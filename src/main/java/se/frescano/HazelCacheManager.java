package se.frescano;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

import com.hazelcast.spring.cache.HazelcastCacheManager;

@Profile("!dev")
@EnableCaching
@Configuration
public class HazelCacheManager extends NexCacheManager{

	public static final Logger logger = LoggerFactory.getLogger(NexCacheManager.class);
	
	@Autowired
	com.hazelcast.core.HazelcastInstance  hazelcastInstance;
	
	@Bean
	@DependsOn("hazelcastInstance")
	public CacheManager cacheManager() {
		logger.info(" initializing the cache with HazelCast");
		HazelcastCacheManager manager = new HazelcastCacheManager(hazelcastInstance);
		//manager.setHazelcastInstance(hazelcastInstance());
		logger.info("existing cache names " +manager.getCacheNames());
        return manager;
	}

	public KeyGenerator keyGenerator() {
		if(logger.isTraceEnabled())logger.trace(" keyGenerator ");
        return null; 
    }

	@Override
	public CacheResolver cacheResolver() {
		if(logger.isTraceEnabled())logger.trace(" CacheResolver ");
		return null;
	}

	@Override
	public CacheErrorHandler errorHandler() {
		if(logger.isTraceEnabled())logger.trace(" errorHandler ");
		return null;
	} 
	
	/*@Bean
	public Config hazelCastConfig() {

		Config config = new Config();
		config.setInstanceName("hazelcast-instance");
		MapConfig cacheSettings = new MapConfig()
				.setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
				.setEvictionPolicy(EvictionPolicy.LFU)
				//.setEvictionPercentage(25)
				.setTimeToLiveSeconds(86400);
		
		config.getMapConfigs().put("nest.series.list",cacheSettings);
		config.getMapConfigs().put("nest.category.map",cacheSettings);
		config.getMapConfigs().put("nest.category.cache",cacheSettings);
		config.getMapConfigs().put("nest.topPanels.list",cacheSettings);
		return config;
	}*/
	
	
	/*@Value("${hazelcast.url}")   
	public String hazelcastUrl;
	@Bean
	public com.hazelcast.core.HazelcastInstance hazelcastInstance() {
		ClientConfig clientConfig = new ClientConfig();
		String hazelcastaddress[] = hazelcastUrl.split(",");
		clientConfig.getNetworkConfig().addAddress(hazelcastaddress);
		clientConfig.getNetworkConfig().setConnectionAttemptLimit(10);
		clientConfig.getNetworkConfig().setConnectionAttemptPeriod(24 * 60);
		clientConfig.getNetworkConfig().setConnectionTimeout(1000);
		HazelcastInstance hazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);
		hazelcastInstance.getLifecycleService().addLifecycleListener(new CustomLifecycleListener());
		hazelcastInstance.addDistributedObjectListener(new CustomDistributedObjectListener());
		hazelcastInstance.getCluster().addMembershipListener(new CustomMembershipListener());
		//hazelcastInstance.getConfig()
		return hazelcastInstance;
	}*/

}

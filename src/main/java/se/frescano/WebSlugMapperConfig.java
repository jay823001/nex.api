package se.frescano;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties
@PropertySource("classpath:web_slugname_map.properties")
public class WebSlugMapperConfig {
 

        private Map<String, String> slug = new HashMap<String, String>();

        public Map<String, String> getSlug() {
            return this.slug;
        }
    
}
package se.frescano;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;
import com.hazelcast.core.DistributedObjectEvent;
import com.hazelcast.core.DistributedObjectListener;
import com.hazelcast.core.LifecycleEvent;
import com.hazelcast.core.LifecycleListener;
import com.hazelcast.core.MemberAttributeEvent;
import com.hazelcast.core.MembershipEvent;
import com.hazelcast.core.MembershipListener;

public class HazelcastCacheConfig /*implements CachingConfigurer*/ {

	/*@Value("${hazelcast.url}") */  
	public String hazelcastUrl;
	
	public static final Logger logger = LoggerFactory.getLogger(HazelcastCacheConfig.class);

	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;

	/*@Override
	public CacheErrorHandler errorHandler() {
		return new CustomCacheErrorHandler();
	}

	@PostConstruct
	public void init(){
		this.hazelcastUrl = applicationpropconfig.getHazelcastUrl();
	}*/
	
	/*@Bean
	@DependsOn("hazelcastInstance")
	public CacheManager cacheManager() {
		System.out.println("Hazelcast adrres"+ hazelcastUrl);

		CacheManager cacheManger = new HazelcastCacheManager(hazelcastInstance);
		Collection<String> names = cacheManger.getCacheNames();
		logger.info(" cache manager initiated , cache names available ");
		for (Iterator<String> iterator = names.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			logger.info(" cache name :: "+string + " "+cacheManger.getCache(string).getName());
		}
		return cacheManger; // (3)
	}*/

	/*HazelcastInstance hazelcastInstance;
	@Bean
	HazelcastInstance hazelcastInstance() {
		ClientConfig clientConfig = new ClientConfig();
		String hazelcastaddress[] = hazelcastUrl.split(",");
		clientConfig.getNetworkConfig().addAddress(hazelcastaddress);
		clientConfig.getNetworkConfig().setConnectionAttemptLimit(10);
		clientConfig.getNetworkConfig().setConnectionAttemptPeriod(24 * 60);
		clientConfig.getNetworkConfig().setConnectionTimeout(1000);
		hazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);
		hazelcastInstance.getLifecycleService().addLifecycleListener(new CustomLifecycleListener());
		hazelcastInstance.addDistributedObjectListener(new CustomDistributedObjectListener());
		hazelcastInstance.getCluster().addMembershipListener(new CustomMembershipListener());
		return hazelcastInstance;
	}*/
	/*@Bean
	public Config hazelCastConfig() {

		Config config = new Config();
		config.setInstanceName("hazelcast-instance");
		MapConfig categoryCache = new MapConfig()
				.setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
				.setEvictionPolicy(EvictionPolicy.LRU)
				.setTimeToLiveSeconds(10);
		config.getMapConfigs().put("nest.series.list",categoryCache);
		return config;
	}*/
	
	/*@Override
	public CacheResolver cacheResolver() {
		System.out.println( " cache resolver ");
		return null;
	}

	@Override
	public KeyGenerator keyGenerator() {
		System.out.println( " KeyGenerator ");
		return null;
	}*/

}
class CustomCacheErrorHandler implements CacheErrorHandler{
	@Override
	public void handleCacheGetError(RuntimeException exception, 
			Cache cache, Object key) {
		System.out.println( " handleCacheGetError " + exception.getMessage() );
	}
	@Override
	public void handleCachePutError(RuntimeException exception, Cache cache, 
			Object key, Object value) {
		System.out.println( " handleCachePutError " + exception.getMessage() );
	}
	@Override
	public void handleCacheEvictError(RuntimeException exception, Cache cache, 
			Object key) {
		System.out.println( " handleCacheEvictError " + exception.getMessage() );
	}
	@Override
	public void handleCacheClearError(RuntimeException exception,Cache cache){
		System.out.println( " handleCacheClearError " + exception.getMessage() );
	}
}
class CustomLifecycleListener implements  LifecycleListener {
	@Override
	public void stateChanged(LifecycleEvent event) {
		System.out.println( " lifecycle change event "+event.getState().name());

	}
}
class CustomDistributedObjectListener  implements DistributedObjectListener {			
	@Override
	public void distributedObjectDestroyed(DistributedObjectEvent event) {
		System.out.println(" event destroyed "+event.getServiceName() + event.getDistributedObject().getName());
	}

	@Override
	public void distributedObjectCreated(DistributedObjectEvent event) {
		System.out.println(" event created "+event.getServiceName() + event.getDistributedObject().getName());

	}
}
class CustomMembershipListener implements MembershipListener {

	@Override
	public void memberRemoved(MembershipEvent membershipEvent) {
		System.out.println( " member Removed " +membershipEvent.getMember().getAddress() );

	}

	@Override
	public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
		System.out.println( " member attribute changed " +memberAttributeEvent.getEventType() + memberAttributeEvent.getKey() +memberAttributeEvent.getMember().getAddress() );

	}

	@Override
	public void memberAdded(MembershipEvent membershipEvent) {
		System.out.println( " member added " +membershipEvent.getMember().getAddress() );


	}
}
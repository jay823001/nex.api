package se.frescano;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("dev")
@EnableCaching
@Configuration
public class LocalCacheManager extends NexCacheManager{

	public static final Logger logger = LoggerFactory.getLogger(NexCacheManager.class);

	@Bean
	public CacheManager cacheManager() {
		logger.info(" initializing the local cache ");
		SimpleCacheManager cacheManager = new SimpleCacheManager();
		 cacheManager.setCaches(Arrays.asList(
		          new ConcurrentMapCache("product.nest.series.list"),
		          new ConcurrentMapCache("product.nest.category.cache"), 
		          new ConcurrentMapCache("product.nest.topPanels.list"),
		          new ConcurrentMapCache("product.nest.category.map"),
		          new ConcurrentMapCache("product.nest.market.cache.data"),
		          new ConcurrentMapCache("product.nest.market.cache.countrycode"),
		          new ConcurrentMapCache("product.nest.market.cache")));
		 
		 System.out.println(cacheManager.getCacheNames());
        return cacheManager;
	}

	public KeyGenerator keyGenerator() {
		if(logger.isTraceEnabled())logger.trace(" keyGenerator ");
        return null; 
    }

	@Override
	public CacheResolver cacheResolver() {
		if(logger.isTraceEnabled())logger.trace(" CacheResolver ");
		return null;
	}

	@Override
	public CacheErrorHandler errorHandler() {
		if(logger.isTraceEnabled())logger.trace(" errorHandler ");
		return null;
	} 

}

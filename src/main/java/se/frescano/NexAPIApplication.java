package se.frescano;

import java.time.Duration;
import java.time.Instant;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import se.frescano.nextory.api.kafka.services.KafkaConsumer;
import se.frescano.nextory.app.request.SpringMessageSourceMessageInterpolator;
import se.frescano.nextory.auth.service.AuthenticationServiceImpl;
import se.frescano.nextory.blueshift.config.BlueShiftConfiguration;
import se.frescano.nextory.dao.MongoDBUtilMorphia;
import se.frescano.nextory.listener.spring.BroadCaster;
import se.frescano.nextory.listener.spring.HttpBroadCaster;
import se.frescano.nextory.util.InMemoryCache;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Configuration 
@EnableSwagger2
@EnableCaching
public class NexAPIApplication /*implements CachingConfigurer*/ {

	public static final Logger logger = LoggerFactory.getLogger(NexAPIApplication.class);
	static ConfigurableApplicationContext _APPLICATIONCONTEXT;
	public static void main(String[] args) {
		Instant start = Instant.now();
		setEnv();
		_APPLICATIONCONTEXT = SpringApplication.run(NexAPIApplication.class, args);
		
		ApplicationPropertiesConfig apt = getBean(ApplicationPropertiesConfig.class);
		KafkaConsumer	kafkaconsumer	= getBean(KafkaConsumer.class);
		AuthenticationServiceImpl	authconfig = getBean( AuthenticationServiceImpl.class);
		BlueShiftConfiguration	blushipfconfig = getBean( BlueShiftConfiguration.class);
		MongoDBUtilMorphia		mongoconfig = getBean( MongoDBUtilMorphia.class);
		
		logger.info(apt.toString());
		logger.info(kafkaconsumer.toString());
		logger.info(authconfig.toString());
		logger.info(blushipfconfig.toString());
		logger.info(mongoconfig.toString());
		
		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toMillis();
		
		logger.info(" Nex Api initialized in {} seconds " ,timeElapsed);
	}
	
	static void setEnv(){
		String env = System.getProperty("ENV");
		if(StringUtils.isBlank(env))
			env = System.getenv("ENV");
		
		
		if(logger.isInfoEnabled())
			logger.info(" environment varaiable from the startup "+env);
		if( StringUtils.equalsIgnoreCase(env, "production") ){
			InMemoryCache.environment	=	"prod";
			InMemoryCache.isProductionEnv = true;
		}else if(StringUtils.equalsIgnoreCase(env, "TEST")){
			InMemoryCache.environment	=	"acpt";
			InMemoryCache.isProductionEnv = false;
		}else if(StringUtils.equalsIgnoreCase(env, "TEST-CLONE")){
			InMemoryCache.environment	=	"acpt1";
			InMemoryCache.isProductionEnv = false;
		}else if(StringUtils.equalsIgnoreCase(env, "DEV") || StringUtils.isBlank(env)){
			InMemoryCache.environment	=	"dev";
			InMemoryCache.isProductionEnv = false;
		}else{
			System.out.println(" No Valid Environment variable , variable can be production/TEST/DEV");
		}
		 System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, InMemoryCache.environment);
	}
	
	private static InitialContext intCntxt;
	
	 public static <T> T getBean(Class<T> clazz) {
			return (T)_APPLICATIONCONTEXT.getBean(clazz);
	 }
	
	
	@Value("${hazelcast.url}")   
	public String hazelcastUrl;
	
	
	@Bean("webclient")	
	public WebClient createWebClient() {
		return WebClient.create();
	}
	
	@Bean
	public Docket swaggerApi(ServletContext servletContext) {
		return new Docket(DocumentationType.SWAGGER_2)
			.pathProvider(new RelativePathProvider(servletContext) {
                @Override
                public String getApplicationBasePath() {
                    return "/product";
                }
            })	
			.select()
			.apis(RequestHandlerSelectors.basePackage("se.frescano"))
			.paths(PathSelectors.regex("/api.*"))
			.build()
			.apiInfo(new ApiInfoBuilder().version("1.0").title("Nextory APP & WEB APIs").description("Documentation for Nextory App & Web api's").build());
	 }
	
	@Bean
	LocalValidatorFactoryBean getLocalValidatorFactoryBean(){
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.setMessageInterpolator(new SpringMessageSourceMessageInterpolator());
		return localValidatorFactoryBean;
	}
	
	public static String lookUp(String contextvariable) {
		try {
			if(!StringUtils.startsWith(contextvariable,"jndi/"))
				return contextvariable;
			
			if( intCntxt == null )
				intCntxt = new InitialContext();
			return (String) intCntxt.lookup("java:comp/env/"+contextvariable);
		} catch (NamingException e) {
			 logger.error("UNABLE TO GET DATABASE CREDNETIALS VIA JNDI. PLEASE CHECK IF THE JNDI IS SET IN SERVER.XML. "
				 		+ "ADD BELOW TAGS WITHIN <GlobalNamingResources></GlobalNamingResources> IN SERVER.XML ",e.getMessage());
				 System.out.println(" <Environment name=\"mysqluser\" value=\"root\"");
				 System.out.println("type=\"java.lang.String\" override=\"false\"/>");
				 System.out.println("<Environment name=\"mysqlpass\" value=\"password\"");
				 System.out.println(" type=\"java.lang.String\" override=\"false\"/> ");
				//e.printStackTrace();
				 System.exit(0);
			}
		return null;
	}
	
	@Bean
	@Profile("!dev")	
	public com.hazelcast.core.HazelcastInstance hazelcastInstance() {
		ClientConfig clientConfig = new ClientConfig();
		String hazelcastaddress[] = hazelcastUrl.split(",");
		clientConfig.getNetworkConfig().addAddress(hazelcastaddress);
		clientConfig.getNetworkConfig().setConnectionAttemptLimit(10);
		clientConfig.getNetworkConfig().setConnectionAttemptPeriod(24 * 60);
		clientConfig.getNetworkConfig().setConnectionTimeout(1000);
		HazelcastInstance hazelcastInstance = HazelcastClient.newHazelcastClient(clientConfig);
		hazelcastInstance.getLifecycleService().addLifecycleListener(new CustomLifecycleListener());
		hazelcastInstance.addDistributedObjectListener(new CustomDistributedObjectListener());
		hazelcastInstance.getCluster().addMembershipListener(new CustomMembershipListener());
		return hazelcastInstance;
	}
			
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	    resolver.setPrefix("/WEB-INF/views/");
	    resolver.setSuffix(".jsp");
	    return resolver;
	}

	@Profile("dev")
	@Bean
	public static PropertySourcesPlaceholderConfigurer properties(){
	  PropertySourcesPlaceholderConfigurer pspc
	    = new PropertySourcesPlaceholderConfigurer();
	  Resource[] resources = new ClassPathResource[ ]
	    { new ClassPathResource( "properties/database-dev.properties" ) };
	  pspc.setLocations( resources );
	  pspc.setIgnoreUnresolvablePlaceholders( true );
	  return pspc;
	}
	
	private BroadCaster broadCaster;
	
	@Bean("broadCast")
	BroadCaster initBroadCaster(){
		broadCaster =new HttpBroadCaster();
		return broadCaster;
	}
	
	@Bean(name = "applicationEventMulticaster")
    public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
        SimpleApplicationEventMulticaster eventMulticaster 
          = new SimpleApplicationEventMulticaster();
         
        eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return eventMulticaster;
    }
	
 	@Bean
 	MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
 		
 		final Tags tags = Tags.of("application", "nex-product");
 		if(System.getenv("HOSTNAME")!=null)
 			Tags.concat(tags,Tags.of("HOSTNAME", System.getenv("HOSTNAME")));
 		return registry -> registry.config().commonTags(tags);
 	}
 	
 

 	/*@Bean
 	Timer authTimer(MeterRegistry registry){
 		return Timer.builder("auth.timer")
 				.publishPercentiles(0.5, 0.95) // median and 95th percentile
 			   .publishPercentileHistogram()
 			   .sla(Duration.ofMillis(100))
 			   .minimumExpectedValue(Duration.ofMillis(1))
 			   .maximumExpectedValue(Duration.ofSeconds(10))
 			  .register(registry);
 	}*/
 	
}

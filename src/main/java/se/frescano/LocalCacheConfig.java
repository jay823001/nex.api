package se.frescano;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;


//@Configuration
//@Profile("dev")

public class LocalCacheConfig /*implements CachingConfigurer */{

	public static final Logger logger = LoggerFactory.getLogger(LocalCacheConfig.class);


	/*@Override
	public CacheErrorHandler errorHandler() {
		return new CustomCacheErrorHandler();
	}*/

	@Bean
	public CacheManager cacheManager() {
		System.out.println("Initializing local cache for the dev env");

		CacheManager cacheManger  = new SimpleCacheManager();
		return cacheManger; // (3)
	}

	/*@Override
	public CacheResolver cacheResolver() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public KeyGenerator keyGenerator() {
		// TODO Auto-generated method stub
		return null;
	}*/

}
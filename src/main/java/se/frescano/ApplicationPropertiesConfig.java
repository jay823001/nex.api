package se.frescano;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

//@PropertySource("classpath:properties/database-${spring.profiles.active}.properties")
@Component("applicationpropconfig")
@RefreshScope
/*Configuration auto refresh can be used only for properties which are directly accessing. 
 * Example : 1. Auto refresh will not work for Mysql object, reason we are initializing DB object by using properties, here property is refreshing not object.
 * 			 2. Auto refresh will work for 'apptusOn' property, we are directly accessing this property in code.
 * 
 * */
public class ApplicationPropertiesConfig {

	/*----Hazelcast config ------start*/
	@Value("${hazelcast.url}")   
	public String hazelcastUrl;
	/*----Hazelcast config ------end*/
	
	@Value("${aptus.hostscheme}") 
	private String	hostscheme;
	
	public String getHazelcastUrl() {
		return hazelcastUrl;
	}

	@Value("${app.supported.versions}") 
	private String	supportedversions;	
	
	private ArrayList<Double> supportedVersionlist;
	
	@Value("${aptus.host}") 
	private String	host;	
	
	@Value("${aptus.didYouMeanUrl}") 
	private String didYouMeanUrl;	
	
	@Value("${aptus.autoCompleteUrlNew}") 
	private String autoCompleteUrlNew;
	
	@Value("${aptus.searchZoneUrl}") 
	private String searchZoneUrl;
	
	@Value("${aptus.username}") 
	private String username;
	
	@Value("${aptus.password}") 
	private String password;
	
	@Value("${aptus.mode}") 
	private String apptusOn;
	
	@Value("${aptus.ClickNotificationUrl}") 
	private String clickNotificationUrl;
	
	@Value("${aptus.privatekey}") 
	private String apptusPrivateKey;
	
	@Value("${aptus.ClickNotificationUrl.nonEsales}") 
	private String nonEsalesClickNotification;
	
	@Value("${aptus.AddtoCartUrl}") 
	private String addToCartUrl;
	
	@Value("${aptus.AddtoCartUrl.nonEsales}") 
	private String addToCartUrlNonEsales;
	
	@Value("${aptus.PaymentUrl}") 
	private String paymentUrl;
	
	@Value("${aptus.nest.series.api}" ) 
	private String nestSeriesUrl;
	
	@Value("${aptus.didYouMeanUrlKids}") 
	private String didYouMeanUrlKids;
	
	@Value("${aptus.bocker}") 
	private String bockerUrl;
	
	@Value("${aptus.ebocker}") 
	private String ebockerUrl;
	
	@Value("${aptus.topListUrl}") 
	private String toplistUrl;
	
	@Value("${aptus.abocker}") 
	private String abockerUrl;
	
	@Value("${aptus.genericUrl}") 
	private String genericUrl;
	
	@Value("${aptus.genericUrlProd}")
	private String genericUrlProd;
	
	@Value("${aptus.RelatedUrl}") 
	private String relatedUrl;
	
	@Value("${aptus.autoCompleteUrlNewKids}") 
	private String autoCompleteUrlNewKids;
	
	@Value("${aptus.parentCategoryUrl}") 
	private String parentCategoryUrl;
	
	@Value("${aptus.categoryZone}") 
	private String categoryZone;
	
	@Value("${aptus.recommendUrl}") 
	private String recommendUrl;
	
	@Value("${aptus.nest.series.api.kids}") 
	private String nestSeriesUrlKids;
	
	@Value("${aptus.recommend.mode}") 
	private String recommendMode;
	
	@Value("${aptus.remove.categories.topBooks}") 
	private String removeCategory;
	
	@Value("${aptus.thoseWhoBought}") 
	private String thoseWhoBought;
	
	@Value("${aptus.recommend.mode.topList}") 
	private String recommendmodetopList;
	
	@Value("${aptus.recommendBasedCustomerUrl}") 
	private String recommendBasedOnCustomer;
	
	@Value("${aptus.topSellers}") 
	private String topSellers;
	
	@Value("${aptus.recommend.mode.topList.child}") 
	private String recommendmodetopListChild;
	
	@Value("${aptus.nest.toppanels.api}")
	private String topPanelUrl;
	
	@Value("${aptus.nest.toppanels.api.new}")
	private String topPanelUrlNew;
	
	@Value("${aptus.web.load.categories.perrequest}")
	private	String webcategorylodperrequest;
	
	/* ---- Kafka category configurations------start*/
	@Value("${nest.nextory.kafka.category.message.key}")
	private String categoryMessageKey;
	
	@Value("${nest.nextory.kafka.category.message.value}")
	private String categoryMessageValue;	
	/* ---- Kafka category configurations------end*/
	
	/* ---- Authentication configurations------start*/
	@Value("${nextory.authentication.service.endpoint}")
	private String AUTHENTICATION_SERVICE;	
	/* ---- Authentication configurations------end*/
	
	@Value("${nextory.blueshift.authentication.service.endpoint}")
	private String BLUESHIFT_AUTHENTICATION_SERVICE;
	
	/* ---- Blueshift configurations------start*/
	@Value("${blueshift.api.url}") 
	private String blueshiftUrl;
	
	@Value("${blueshift.view}") 
	private String blueshiftView;
	
	@Value("${blueshift.add}") 
	private String blueshiftAdd;
	
	@Value("${blueshift.purchase}") 
	private String blueshiftPurchase;
	
	@Value("${blueshift.mode}") 
	private String blueshiftMode;
	
	@Value("${blueshift.EVENT_API_KEY}") 
	private String blueshiftApiKey;
	/* ---- Blueshift configurations------end*/
	
	/* ---- MongoDb configurations------start*/
	@Value("${mongo.user}") 
	private  String mongodbusername;
	
	@Value("${mongo.product.database}") 
	private  String mongoproddatabase;
	
	@Value("${mongo.log.database}") 
	private  String mongologdatabase;
	
	@Value("${mongo.password}") 
	private  String mongodbpassword;
	
	@Value("${mongo.host}") 
	private  String mongoserverAddress;
	
	@Value("${mongo.auth.databases}") 
	private  String mongoauthDatabases;
	/* ---- MongoDb configurations------end*/
	
	/* ---- Mail service configurations------start*/
	@Value("${nextory.mail.from}")
	private String mailfrom;
	
	@Value("${nextory.mail.subject.forgotpassword}")
	private String mailsubject;
	/* ---- Mail service configurations------end*/
	
	/* ---- Web Token configurations------start*/
	@Value("${nextory.jwt.refresh.token.header}")
	private String REFRESH_TOKEN_HEADER;
	
	@Value("${nextory.jwt.access.token.header}")
	private String ACCESS_TOKEN_HEADER;	
	
	@Value("${web.origin.allowed}")
	private  String ALLOWED_ORIGINS;
	
	@Value("${nextory.request.auth.username}")
	private String	nextoryReqAuthUsername;
	@Value("${nextory.request.auth.password}")
	private String	nextoryReqAuthPassword;
	
	/* ---- Web Token configurations------end*/
	
	 @Value("${app.SERVER_URL}")
	 private String SERVER_URL;
	 
	 
	 @Value("${app.SERVER_URL_GENERIC}")
	 private String SERVER_URL_GENERIC;
	 
	 @Value("${app.PHP_IMAGE_RESIZE_URL}")
	 private String	PHP_IMAGE_RESIZE_URL;
	 
	 /* ---- ZENDESK configurations------start*/
	 @Value("${app.ZENDESK_USER_NAME}")
	 private String	ZENDESK_USER_NAME;
	 
	 @Value("${app.ZENDESK_PWD}")
	 private String	ZENDESK_PWD;
	 
	 @Value("${app.ZENDESK_DATE_FORMAT}")
	 private String ZENDESK_DATE_FORMAT;
	 
	 @Value("${app.ZENDESK_SCHEDULE_URL}")
	 private String	ZENDESK_SCHEDULE_URL;
	 
	 @Value("${app.ZENDESK_HOLIDAY_URL}")
	 private String ZENDESK_HOLIDAY_URL;
	 /* ---- ZENDESK configurations------start*/
	 
	 
	 @Value("${library.bookidlist.url}")
	 private String libraryUrl;
	 
	 @Value("${library.bookidlist.maxsize}")
	 private String libraryMaxSize;
	 
	 @Value("${apptus.primaryfacets}")
	 private String primaryfacetslist;
	 
	 @Value("${aptus.topSellers.mode}")
	 private String topSellersMode;
	 
	 
	 
	public String getTopSellersMode() {
		return topSellersMode;
	}

	public void setTopSellersMode(String topSellersMode) {
		this.topSellersMode = topSellersMode;
	}

	public String getDidYouMeanUrl() {
		return didYouMeanUrl;
	}
	
	public String getAutoCompleteUrlNew() {
		return autoCompleteUrlNew;
	}
	public String getSearchZoneUrl() {
		return searchZoneUrl;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getApptusOn() {
		return apptusOn;
	}
	public String getClickNotificationUrl() {
		return clickNotificationUrl;
	}
	public String getApptusPrivateKey() {
		return apptusPrivateKey;
	}
	public String getNonEsalesClickNotification() {
		return nonEsalesClickNotification;
	}
	public String getAddToCartUrl() {
		return addToCartUrl;
	}
	public String getAddToCartUrlNonEsales() {
		return addToCartUrlNonEsales;
	}
	public String getPaymentUrl() {
		return paymentUrl;
	}
	public String getNestSeriesUrl() {
		return nestSeriesUrl;
	}
	public String getDidYouMeanUrlKids() {
		return didYouMeanUrlKids;
	}
	/*public String getPopularSeriesKids() {
		return popularSeriesKids;
	}*/
	/*public String getPopularSeries() {
		return popularSeries;
	}*/
	public String getBockerUrl() {
		return bockerUrl;
	}
	public String getEbockerUrl() {
		return ebockerUrl;
	}
	/*public String getCategoryUrl() {
		return categoryUrl;
	}*/
	/*public String getMenuGroup() {
		return menuGroup;
	}*/
	public String getToplistUrl() {
		return toplistUrl;
	}
	public String getAbockerUrl() {
		return abockerUrl;
	}
	public String getGenericUrl() {
		return genericUrl;
	}
	public String getRelatedUrl() {
		return relatedUrl;
	}
	public String getAutoCompleteUrlNewKids() {
		return autoCompleteUrlNewKids;
	}
	public String getParentCategoryUrl() {
		return parentCategoryUrl;
	}
	public String getCategoryZone() {
		return categoryZone;
	}
	public String getRecommendUrl() {
		return recommendUrl;
	}
	public String getNestSeriesUrlKids() {
		return nestSeriesUrlKids;
	}
	public String getRecommendMode() {
		return recommendMode;
	}
	public String getRemoveCategory() {
		return removeCategory;
	}
	public String getThoseWhoBought() {
		return thoseWhoBought;
	}
	public String getRecommendmodetopList() {
		return recommendmodetopList;
	}
	public String getRecommendBasedOnCustomer() {
		return recommendBasedOnCustomer;
	}
	public String getRecommendmodetopListChild() {
		return recommendmodetopListChild;
	}
	public String getTopPanelUrl() {
		return topPanelUrl;
	}
	
	
	public String getHostscheme() {
		return hostscheme;
	}
	public String getHost() {
		return host;
	}
	
	public String getCategoryMessageKey() {
		return categoryMessageKey;
	}

	public String getCategoryMessageValue() {
		return categoryMessageValue;
	}

	public int	getWebcategorylodperrequest(){
		if( !StringUtils.isBlank( webcategorylodperrequest ))
			return Integer.valueOf( webcategorylodperrequest ).intValue();
		return 1;	
	}
		
	public String getAUTHENTICATION_SERVICE() {
		return AUTHENTICATION_SERVICE;
	}

	public String getBlueshiftUrl() {
		return blueshiftUrl;
	}

	public String getBlueshiftView() {
		return blueshiftView;
	}

	public String getBlueshiftAdd() {
		return blueshiftAdd;
	}

	public String getBlueshiftPurchase() {
		return blueshiftPurchase;
	}

	public String getBlueshiftMode() {
		return blueshiftMode;
	}

	public String getBlueshiftApiKey() {
		return blueshiftApiKey;
	}

	public String getMongodbusername() {
		return mongodbusername;
	}

	public String getMongoproddatabase() {
		return mongoproddatabase;
	}

	public String getMongologdatabase() {
		return mongologdatabase;
	}

	public String getMongodbpassword() {
		return mongodbpassword;
	}

	public String getMongoserverAddress() {
		return mongoserverAddress;
	}

	public String getMongoauthDatabases() {
		return mongoauthDatabases;
	}

	public String getMailfrom() {
		return mailfrom;
	}

	public String getMailsubject() {
		return mailsubject;
	}

	public String getREFRESH_TOKEN_HEADER() {
		return REFRESH_TOKEN_HEADER;
	}
	public void setREFRESH_TOKEN_HEADER(String refreshTokenHeader) {
		REFRESH_TOKEN_HEADER = refreshTokenHeader;
	}

	public String getACCESS_TOKEN_HEADER() {
		return ACCESS_TOKEN_HEADER;
	}
	public void setACCESS_TOKEN_HEADER(String accessTokenHeader) {
		 ACCESS_TOKEN_HEADER = accessTokenHeader;
	}

	public String getSERVER_URL() {
		return SERVER_URL;
	}
	
	

	public String getSERVER_URL_GENERIC() {
		return SERVER_URL_GENERIC;
	}

	public void setSERVER_URL_GENERIC(String sERVER_URL_GENERIC) {
		SERVER_URL_GENERIC = sERVER_URL_GENERIC;
	}

	public String getPHP_IMAGE_RESIZE_URL() {
		return PHP_IMAGE_RESIZE_URL;
	}

	public String getZENDESK_USER_NAME() {
		return ZENDESK_USER_NAME;
	}

	public String getZENDESK_PWD() {
		return ZENDESK_PWD;
	}

	public String getZENDESK_DATE_FORMAT() {
		return ZENDESK_DATE_FORMAT;
	}

	public String getZENDESK_SCHEDULE_URL() {
		return ZENDESK_SCHEDULE_URL;
	}

	public String getZENDESK_HOLIDAY_URL() {
		return ZENDESK_HOLIDAY_URL;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AptusConfiguration [");
		if( hostscheme != null ){
			builder.append("hostscheme=");
			builder.append(hostscheme);
			builder.append(", ");
		}
		if(  host != null ){
			if (host != null) {
				builder.append("host=");
				builder.append(host);
				builder.append(", ");
			}	
		}
		if (didYouMeanUrl != null) {
			builder.append("didYouMeanUrl=");
			builder.append(didYouMeanUrl);
			builder.append(", ");
		}
		
		if (autoCompleteUrlNew != null) {
			builder.append("autoCompleteUrlNew=");
			builder.append(autoCompleteUrlNew);
			builder.append(", ");
		}
		if (searchZoneUrl != null) {
			builder.append("searchZoneUrl=");
			builder.append(searchZoneUrl);
			builder.append(", ");
		}
		if (username != null) {
			builder.append("username=");
			builder.append(username);
			builder.append(", ");
		}
		if (password != null) {
			builder.append("password=");
			builder.append(password);
			builder.append(", ");
		}
		if (apptusOn != null) {
			builder.append("apptusOn=");
			builder.append(apptusOn);
			builder.append(", ");
		}
		if (clickNotificationUrl != null) {
			builder.append("clickNotificationUrl=");
			builder.append(clickNotificationUrl);
			builder.append(", ");
		}
		if (apptusPrivateKey != null) {
			builder.append("apptusPrivateKey=");
			builder.append(apptusPrivateKey);
			builder.append(", ");
		}
		if (nonEsalesClickNotification != null) {
			builder.append("nonEsalesClickNotification=");
			builder.append(nonEsalesClickNotification);
			builder.append(", ");
		}
		if (addToCartUrl != null) {
			builder.append("addToCartUrl=");
			builder.append(addToCartUrl);
			builder.append(", ");
		}
		if (addToCartUrlNonEsales != null) {
			builder.append("addToCartUrlNonEsales=");
			builder.append(addToCartUrlNonEsales);
			builder.append(", ");
		}
		if (paymentUrl != null) {
			builder.append("paymentUrl=");
			builder.append(paymentUrl);
			builder.append(", ");
		}
		if (nestSeriesUrl != null) {
			builder.append("nestSeriesUrl=");
			builder.append(nestSeriesUrl);
			builder.append(", ");
		}
		if (didYouMeanUrlKids != null) {
			builder.append("didYouMeanUrlKids=");
			builder.append(didYouMeanUrlKids);
			builder.append(", ");
		}
		
		if (bockerUrl != null) {
			builder.append("bockerUrl=");
			builder.append(bockerUrl);
			builder.append(", ");
		}
		if (ebockerUrl != null) {
			builder.append("ebockerUrl=");
			builder.append(ebockerUrl);
			builder.append(", ");
		}
		
		if (toplistUrl != null) {
			builder.append("toplistUrl=");
			builder.append(toplistUrl);
			builder.append(", ");
		}
		if (abockerUrl != null) {
			builder.append("abockerUrl=");
			builder.append(abockerUrl);
			builder.append(", ");
		}
		if (genericUrl != null) {
			builder.append("genericUrl=");
			builder.append(genericUrl);
			builder.append(", ");
		}
		if (relatedUrl != null) {
			builder.append("relatedUrl=");
			builder.append(relatedUrl);
			builder.append(", ");
		}
		if (autoCompleteUrlNewKids != null) {
			builder.append("autoCompleteUrlNewKids=");
			builder.append(autoCompleteUrlNewKids);
			builder.append(", ");
		}
		if (parentCategoryUrl != null) {
			builder.append("parentCategoryUrl=");
			builder.append(parentCategoryUrl);
			builder.append(", ");
		}
		if (categoryZone != null) {
			builder.append("categoryZone=");
			builder.append(categoryZone);
			builder.append(", ");
		}
		if (recommendUrl != null) {
			builder.append("recommendUrl=");
			builder.append(recommendUrl);
			builder.append(", ");
		}
		if (nestSeriesUrlKids != null) {
			builder.append("nestSeriesUrlKids=");
			builder.append(nestSeriesUrlKids);
			builder.append(", ");
		}
		if (recommendMode != null) {
			builder.append("recommendMode=");
			builder.append(recommendMode);
			builder.append(", ");
		}
		if (removeCategory != null) {
			builder.append("removeCategory=");
			builder.append(removeCategory);
			builder.append(", ");
		}
		if (thoseWhoBought != null) {
			builder.append("thoseWhoBought=");
			builder.append(thoseWhoBought);
			builder.append(", ");
		}
		if (recommendmodetopList != null) {
			builder.append("recommendmodetopList=");
			builder.append(recommendmodetopList);
			builder.append(", ");
		}
		if (recommendBasedOnCustomer != null) {
			builder.append("recommendBasedOnCustomer=");
			builder.append(recommendBasedOnCustomer);
			builder.append(", ");
		}
		if (recommendmodetopListChild != null) {
			builder.append("recommendmodetopListChild=");
			builder.append(recommendmodetopListChild);
			builder.append(", ");
		}
		if (topPanelUrl != null) {
			builder.append("topPanelUrl=");
			builder.append(topPanelUrl);
			builder.append(", ");
		}
		
		if( webcategorylodperrequest != null ){
			builder.append("webcategorylodperrequest=");
			builder.append(webcategorylodperrequest);
		}
		
		builder.append("]")
		.append(" \n");
		
		builder.append("Web Token Configurations : [");
		if( ACCESS_TOKEN_HEADER != null )
			builder.append( " ACCESS_TOKEN_HEADER =").append( ACCESS_TOKEN_HEADER).append(",");
		if( REFRESH_TOKEN_HEADER != null )
			builder.append( " REFRESH_TOKEN_HEADER =").append( REFRESH_TOKEN_HEADER);
		builder.append("]")
		.append(" \n");
		
		builder.append("HazelCast Configurations : [");
		if( hazelcastUrl != null )
			builder.append( " hazelcastUrl =").append( hazelcastUrl);		
		builder.append("]").append(" \n");
		
		builder.append("ZENDESK Configurations : [");
		if( ZENDESK_USER_NAME != null )
			builder.append( " ZENDESK_USER_NAME =").append( ZENDESK_USER_NAME);	
		if( ZENDESK_PWD != null )
			builder.append( " ZENDESK_PWD =").append( ZENDESK_PWD);		
		if( ZENDESK_SCHEDULE_URL != null )
			builder.append( " ZENDESK_SCHEDULE_URL =").append( ZENDESK_SCHEDULE_URL);
		if( ZENDESK_HOLIDAY_URL != null )
			builder.append( " ZENDESK_HOLIDAY_URL =").append( ZENDESK_HOLIDAY_URL);
		if( ZENDESK_DATE_FORMAT != null )
			builder.append( " ZENDESK_DATE_FORMAT =").append( ZENDESK_DATE_FORMAT);
		
		builder.append("]").append(" \n");
		
		return builder.toString();
	}

	public String getNextoryReqAuthUsername() {
		return nextoryReqAuthUsername;
	}

	public String getNextoryReqAuthPassword() {
		return nextoryReqAuthPassword;
	}

	public void setNextoryReqAuthUsername(String nextoryReqAuthUsername) {
		this.nextoryReqAuthUsername = nextoryReqAuthUsername;
	}

	public void setNextoryReqAuthPassword(String nextoryReqAuthPassword) {
		this.nextoryReqAuthPassword = nextoryReqAuthPassword;
	}

	public String getTopSellers() {
		return topSellers;
	}

	public void setTopSellers(String topSellers) {
		this.topSellers = topSellers;
	}

	public String getTopPanelUrlNew() {
		return topPanelUrlNew;
	}

	public void setTopPanelUrlNew(String topPanelUrlNew) {
		this.topPanelUrlNew = topPanelUrlNew;
	}

	public String getBLUESHIFT_AUTHENTICATION_SERVICE() {
		return BLUESHIFT_AUTHENTICATION_SERVICE;
	}

	public void setBLUESHIFT_AUTHENTICATION_SERVICE(String bLUESHIFT_AUTHENTICATION_SERVICE) {
		BLUESHIFT_AUTHENTICATION_SERVICE = bLUESHIFT_AUTHENTICATION_SERVICE;
	}

	public ArrayList<Double> getSupportedversions() {
		String versions[] =supportedversions.split(",");
		ArrayList<Double> supportedversionslist1 = new ArrayList<Double>();
		 for(String ver:versions)
		 {
			 supportedversionslist1.add(Double.valueOf(ver));
		 }
		 return supportedversionslist1;
	}

	public void setSupportedversions(String supportedversions) {
		this.supportedversions = supportedversions;
		String versions[] =supportedversions.split(",");
		ArrayList<Double> supportedversionslist = new ArrayList<Double>();
		 for(String ver:versions)
		 {
			 supportedversionslist.add(Double.valueOf(ver));
		 }
		 this.supportedVersionlist =supportedversionslist;
	}

	public String getPrimaryfacetslist() {
		return primaryfacetslist;
	}

	public void setPrimaryfacetslist(String primaryfacetslist) {
		this.primaryfacetslist = primaryfacetslist;
	}

	public String getLibraryUrl() {
		return libraryUrl;
	}

	public void setLibraryUrl(String libraryUrl) {
		this.libraryUrl = libraryUrl;
	}

	public String getLibraryMaxSize() {
		return libraryMaxSize;
	}

	public void setLibraryMaxSize(String libraryMaxSize) {
		this.libraryMaxSize = libraryMaxSize;
	}

	public String getGenericUrlProd() {
		return genericUrlProd;
	}

	public void setGenericUrlProd(String genericUrlProd) {
		this.genericUrlProd = genericUrlProd;
	}

	
	

}

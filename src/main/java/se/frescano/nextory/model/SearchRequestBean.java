package se.frescano.nextory.model;

public class SearchRequestBean {

	String format;
	String slug;
	//String subscribe;
	String q;
	Integer pageSize;
	String sortOn;
	Integer pageNumber;
	Boolean category;
	Boolean showall;
	Boolean logReq	= Boolean.FALSE;
		
	
	public Boolean getLogReq() {
		return logReq;
	}
	public void setLogReq(Boolean logReq) {
		this.logReq = logReq;
	}
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public Boolean getShowall() {
		return showall;
	}
	public void setShowall(Boolean showall) {
		this.showall = showall;
	}
	public Boolean getCategory() {
		return category;
	}
	public void setCategory(Boolean category) {
		this.category = category;
	}
	public String getSortOn() {
		return sortOn;
	}
	public void setSortOn(String sortOn) {
		this.sortOn = sortOn;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	/*public String getSubscribe() {
		return subscribe;
	}
	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}*/
	
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	@Override
	public String toString() {
		return "SearchRequestBean [format=" + format + ", slug=" + slug + ", q=" + q
				+ ", pageSize=" + pageSize + ", sortOn=" + sortOn + ", pageNumber=" + pageNumber + ", category="
				+ category + ", showall=" + showall + ", logReq=" + logReq + "]";
	}
	
}	

package se.frescano.nextory.model;

import java.util.ArrayList;
import java.util.List;

import se.frescano.nextory.app.product.response.ProductInformation;

public class SearchResult {

	Integer rows;
	Integer resultSize;
	List<ProductInformation> productlist;
	String title;
	int pageNumber;
	String slugName;
	boolean bookGrpProducts;
	Integer noofbooks;
	String query;
	String didyoumean;
	Integer higherresultSize;
	
	

	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getDidyoumean() {
		return didyoumean;
	}
	public void setDidyoumean(String didyoumean) {
		this.didyoumean = didyoumean;
	}
	public Integer getNoofbooks() {
		return noofbooks;
	}
	public void setNoofbooks(Integer noofbooks) {
		this.noofbooks = noofbooks;
	}
	public boolean isBookGrpProducts() {
		return bookGrpProducts;
	}
	public void setBookGrpProducts(boolean bookGrpProducts) {
		this.bookGrpProducts = bookGrpProducts;
	}
	public Integer getRows() {
		return rows;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public Integer getResultSize() {
		return resultSize;
	}
	public void setResultSize(Integer resultSize) {
		this.resultSize = resultSize;
	}
	public List<ProductInformation> getProductlist() {
		return productlist;
	}
	public void setProductlist(List<ProductInformation> productlist) {
		this.productlist = productlist;
	}
	public void addProduct(ProductInformation productInfo){
		if(this.productlist==null)this.productlist = new ArrayList<ProductInformation>();
		
		this.productlist.add(productInfo);
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSlugName() {
		return slugName;
	}
	public void setSlugName(String slugName) {
		this.slugName = slugName;
	}
	public Integer getHigherresultSize() {
		return higherresultSize;
	}
	public void setHigherresultSize(Integer higherresultSize) {
		this.higherresultSize = higherresultSize;
	}
	@Override
	public String toString() {
		return "SearchResult [rows=" + rows + ", resultSize=" + resultSize + ", productlist=" + productlist + ", title="
				+ title + ", pageNumber=" + pageNumber + ", slugName=" + slugName + ", bookGrpProducts="
				+ bookGrpProducts + ", noofbooks=" + noofbooks + ", query=" + query + ", didyoumean=" + didyoumean
				+ ", higherresultSize=" + higherresultSize + "]";
	}
	
	
}

package se.frescano.nextory.model;

import java.io.Serializable;
import java.util.ArrayList;

import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.mongo.product.model.Ratings;

public class ProductDetails implements Serializable{
	
	private static final long	serialVersionUID	= 17340356270040778L;
	private ProductFormatEnum productType;
	private ProductRelatedDetails ebookDetails;
	private ProductRelatedDetails audioDetails;
	private boolean relatedProductExists ;
	private String seriesName;
	private String sequence;
	private ArrayList<Integer> mainCategoryIds;
	private Ratings ratings;
	/*private String seriesType;


	
	public String getSeriesType() {
		return seriesType;
	}
	public void setSeriesType(String seriesType) {
		this.seriesType = seriesType;
	}*/
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public ProductFormatEnum getProductType() {
		return productType;
	}
	public void setProductType(ProductFormatEnum productType) {
		this.productType = productType;
	}
	public ProductRelatedDetails getEbookDetails() {
		return ebookDetails;
	}
	public void setEbookDetails(ProductRelatedDetails ebookDetails) {
		this.ebookDetails = ebookDetails;
	}
	public ProductRelatedDetails getAudioDetails() {
		return audioDetails;
	}
	public void setAudioDetails(ProductRelatedDetails audioDetails) {
		this.audioDetails = audioDetails;
	}	
	public boolean isRelatedProductExists() {
		return relatedProductExists;
	}
	public void setRelatedProductExists(boolean relatedProductExists) {
		this.relatedProductExists = relatedProductExists;
	}
	public ArrayList<Integer> getMainCategoryIds() {
		return mainCategoryIds;
	}
	public void setMainCategoryIds(ArrayList<Integer> mainCategoryIds) {
		this.mainCategoryIds = mainCategoryIds;
	}
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductDetails [ebookDetails=");
		builder.append(ebookDetails);
		builder.append(", audioDetails=");
		builder.append(audioDetails);
		builder.append("]");
		return builder.toString();
	}
	public Ratings getRatings() {
		return ratings;
	}
	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}
	
	
	

}

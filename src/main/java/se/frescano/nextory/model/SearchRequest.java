package se.frescano.nextory.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import se.frescano.nextory.app.constants.ProductFormatEnum;	

public class SearchRequest implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	ProductFormatEnum productFormat;
	//SubscriptionTypeVO subscriptionType;
	String query;
	/*Integer oldprodid;
	Integer categoryId;*/
	Integer pageNumber;
	Integer windowSize;
	String sortOn;
	//boolean app;
	/*Integer excludeProduct;
	Integer customerid;
	String fieldOn;
	String[] querries;
	boolean manualsort;
	String[] projection = null;*/
	private Integer pageSize =12;
	private String subscribe;
	private String format;
	//private String[] excludePublishers;
	private Integer profileid;
	private String includenotallowedbooks;
	
	/*public SearchRequest() 
	{
	}*/
	
	/*public SearchRequest(boolean app) {
		super();
		//this.app = app;
	}*/
	
	/*public SearchRequest(boolean app,String... projection) {
		//this(app);
		this.projection = projection;
	}*/
	
	
	
	/*public Integer getOldprodid() {
		return oldprodid;
	}

	public void setOldprodid(Integer oldprodid) {
		this.oldprodid = oldprodid;
	}

	public boolean isManualsort() {
		return manualsort;
	}

	public void setManualsort(boolean manualsort) {
		this.manualsort = manualsort;
	}

	public String[] getProjection() {
		return projection;
	}

	public String[] getQuerries() {
		return querries;
	}

	public void setQuerries(String[] querries) {
		this.querries = querries;
	}

	public String getFieldOn() {
		return fieldOn;
	}

	public void setFieldOn(String fieldOn) {
		this.fieldOn = fieldOn;
	}

	
	
	public Integer getCustomerid() {
		return customerid;
	}


	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}


	public Integer getExcludeProduct() {
		return excludeProduct;
	}



	public void setExcludeProduct(Integer excludeProduct) {
		this.excludeProduct = excludeProduct;
	}
*/

	/*public boolean isApp() {
		return app;
	}
	public void setApp(boolean app) {
		this.app = app;
	}*/
	public ProductFormatEnum getProductFormat() {
		return productFormat;
	}
	public void setProductFormat(ProductFormatEnum productFormat) {
		this.productFormat = productFormat;
	}
	/*public SubscriptionTypeVO getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(SubscriptionTypeVO subscriptionType) {
		this.subscriptionType = subscriptionType;
	}*/
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	/*public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}*/
	public Integer getPageNumber() {
		return pageNumber!=null?pageNumber:1;
	}
	public void setPageNumber(Integer pagenumber) {
		this.pageNumber = pagenumber;
	}
	
	public Integer getWindowSize() {
		return windowSize;
	}
	public void setWindowSize(Integer windowSize) {
		this.windowSize = windowSize;
	}
	public String getSortOn() {
		return sortOn;
	}
	public void setSortOn(String sortOn) {
		this.sortOn = sortOn;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}
	
	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}	

	//@Override
	/*public String toString() {
		List<String> lstPublisher=null;		
		return "SearchRequest [productFormat=" + productFormat + ", query="
				+ query + ", oldprodid=" + oldprodid + ", categoryId=" + categoryId + ", pageNumber=" + pageNumber
				+ ", windowSize=" + windowSize + ", sortOn=" + sortOn + ", excludeProduct="
				+ excludeProduct + ", customerid=" + customerid + ", fieldOn=" + fieldOn + ", querries="
				+ Arrays.toString(querries) + ", manualsort=" + manualsort + ", projection="
				+ Arrays.toString(projection) + ", pageSize=" + pageSize + ", subscribe=" + subscribe + ", format="
				+ format +"excludePublishers="+lstPublisher+"]";
	}*/

	/*public Integer getProfileid() {
		return profileid;
	}

	public void setProfileid(Integer profileid) {
		this.profileid = profileid;
	}*/

	public String getIncludenotallowedbooks() {
		return includenotallowedbooks;
	}

	public void setIncludenotallowedbooks(String includenotallowedbooks) {
		this.includenotallowedbooks = includenotallowedbooks;
	}

	
	
	
}
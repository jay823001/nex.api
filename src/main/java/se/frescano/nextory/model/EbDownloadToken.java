package se.frescano.nextory.model;


public class EbDownloadToken implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3082865015939568025L;
	private int id;
	private int libid;
	private String title;
	private Integer format;
	private Integer part;
	private String name;
	private String size;
	private String sizetype;
	private String token;
	private String filechecksum;
	private String elibOrderRef;
	
	public EbDownloadToken() {
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public int getLibid() {
		return libid;
	}



	public void setLibid(int libid) {
		this.libid = libid;
	}



	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getFormat() {
		return this.format;
	}

	public void setFormat(Integer format) {
		this.format = format;
	}

	public Integer getPart() {
		return this.part;
	}

	public void setPart(Integer part) {
		this.part = part;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSizetype() {
		return this.sizetype;
	}

	public void setSizetype(String sizetype) {
		this.sizetype = sizetype;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getFilechecksum() {
		return this.filechecksum;
	}

	public void setFilechecksum(String challenge) {
		this.filechecksum = challenge;
	}

	public String getElibOrderRef() {
		return this.elibOrderRef;
	}

	public void setElibOrderRef(String elibOrderRef) {
		this.elibOrderRef = elibOrderRef;
	}
}

package se.frescano.nextory.model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.mongo.product.model.Ratings;
import se.frescano.nextory.mongo.product.model.RestrictedFor;

public class ProductRelatedDetails implements Serializable{
	
	
	private static final long	serialVersionUID	= 7325424778279554430L;
	
	private int productid;
	private String title;
	private String imageUrl;
	private String coverUrl;
	private String teaserlink;
	private List<String> narattors;
	private List<String> authors;
	private List<String> translators;
	private String language;
	private String isbn;
	private int formattype;
	//private List<FormatIdVO> formatid;
	private ProductFormatEnum productFormat;
	private String productURL;
	private String publisher;
	private Integer publisherid;
	//private Integer libraryId ;
	//private boolean libraryFlag=false;
	private boolean onSale = false;
	private Date firstpublished;
	private float rating;
	private String size;
	private float price;
	private String discription;
	//private boolean bookAllowed;
	//private String bookAllowedFor;
	private String booklengthType;
	private Integer bookLengthValue;
	private Integer aptusid;
	private Ratings ratings;
	private RestrictedFor restrictedfor;
	
	public Integer getBookLengthValue() {
		return bookLengthValue;
	}
	public void setBookLengthValue(Integer bookLengthValue) {
		this.bookLengthValue = bookLengthValue;
	}
	public String getBooklengthType() {
		return booklengthType;
	}
	public void setBooklengthType(String book_length_type) {
		this.booklengthType = book_length_type;
	}
	/*public boolean isBookAllowed() {
		return bookAllowed;
	}
	public void setBookAllowed(boolean bookAllowed) {
		this.bookAllowed = bookAllowed;
	}
	public String getBookAllowedFor() {
		return bookAllowedFor;
	}
	public void setBookAllowedFor(String bookAllowedFor) {
		this.bookAllowedFor = bookAllowedFor;
	}*/
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCoverUrl() {
		return coverUrl;
	}
	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}
	public String getTeaserlink() {
		return teaserlink;
	}
	public void setTeaserlink(String teaserlink) {
		this.teaserlink = teaserlink;
	}
	public List<String> getTranslators() {
		return translators;
	}
	public void setTranslators(List<String> translators) {
		this.translators = translators;
	}
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getFormattype() {
		return formattype;
	}
	public void setFormattype(int formattype) {
		this.formattype = formattype;
		this.productFormat = ProductFormatEnum.getFormat(formattype);
	}
	/*public List<FormatIdVO> getFormatid() {
		return formatid;
	}
	public void setFormatid(List<FormatIdVO> formatid) {
		this.formatid = formatid;
	}*/
	
	public String getProductURL() {
		return productURL;
	}
	public ProductFormatEnum getProductFormat() {
		return productFormat;
	}
	public void setProductFormat(ProductFormatEnum productFormat) {
		this.productFormat = productFormat;
	}
	public void setProductURL(String productURL) {
		this.productURL = productURL;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	/*public Integer getLibraryId() {
		return libraryId;
	}
	public void setLibraryId(Integer libraryId) {
		this.libraryId = libraryId;
	}
	public boolean isLibraryFlag() {
		return libraryFlag;
	}
	public void setLibraryFlag(boolean libraryFlag) {
		this.libraryFlag = libraryFlag;
	}*/
	public boolean isOnSale() {
		return onSale;
	}
	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}
	
	public Date getFirstpublished() {
		return firstpublished;
	}
	public void setFirstpublished(Date firstpublished) {
		this.firstpublished = firstpublished;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	
	public List<String> getNarattors() {
		return narattors;
	}
	public void setNarattors(List<String> narattors) {
		this.narattors = narattors;
	}
	
	public Integer getPublisherid() {
		return publisherid;
	}
	public void setPublisherid(Integer publisherid) {
		this.publisherid = publisherid;
	}
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EbookProductDetails [productid=");
		builder.append(productid);
		builder.append(", title=");
		builder.append(title);
		builder.append(", narattors=");
		builder.append(narattors);
		builder.append(", authors=");
		builder.append(authors);
		builder.append(", isbn=");
		builder.append(isbn);
		builder.append(", formattype=");
		builder.append(formattype);
		builder.append(", productFormat=");
		builder.append(productFormat);
		builder.append(", publisher=");
		builder.append(publisher);
		/*builder.append(", libraryId=");
		builder.append(libraryId);*/
		builder.append(", onSale=");
		builder.append(onSale);
		builder.append(", firstpublished=");
		builder.append(firstpublished);
		builder.append(", price=");
		builder.append(price);
		builder.append(",rating=");
		builder.append(ratings);
		builder.append("]");
		return builder.toString();
	}
	public Integer getAptusid() {
		return aptusid;
	}
	public void setAptusid(Integer aptusid) {
		this.aptusid = aptusid;
	}
	public Ratings getRatings() {
		return ratings;
	}
	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}
	public RestrictedFor getRestrictedfor() {
		return restrictedfor;
	}
	public void setRestrictedfor(RestrictedFor restrictedfor) {
		this.restrictedfor = restrictedfor;
	}
	
	
	
}

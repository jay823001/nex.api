package se.frescano.nextory.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.constants.CONSTANTS;

public class EbCategories implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4573550188356607488L;
	
	@JsonProperty("category_id")
	private Integer categoryId;
	
	@JsonProperty("parent_cat_id")
	private Integer parentCatId;
	
	@JsonProperty("category_name")
	private String categoryName;
	
	@JsonProperty("isactive")
	private Boolean isactive;
	
	@JsonProperty("slug_name")
	private String slugName;
	private Set<EbCategoryGrouping> ebCategoryGroupings = new HashSet<EbCategoryGrouping>(
			0);
	private List<EbCategories> subCategories;
	
	@JsonProperty("popular")
	private Boolean popular;
	 
	private int position;
	
	@JsonProperty("categorytype")
	private CONSTANTS.PROFILE_CATEGORIES categoryType;
	
	
	@JsonProperty("market_id")
	private Integer marketid;
	
	@JsonProperty("primarylanguage")
	private String primarylanguage;
	
	
	@JsonProperty("level")
	private Integer level;
	
	@JsonProperty("apptus_sort_attrib")
	private String apptus_sort_attrib;
	
	public Integer getMarketid() {
		return marketid;
	}
	public void setMarketid(Integer marketid) {
		this.marketid = marketid;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getParentCatId() {
		return parentCatId;
	}
	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Boolean getIsactive() {
		return isactive;
	}
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}
	public String getSlugName() {
		return slugName;
	}
	public void setSlugName(String slugName) {
		this.slugName = slugName;
	}
	public Set<EbCategoryGrouping> getEbCategoryGroupings() {
		return ebCategoryGroupings;
	}
	public void setEbCategoryGroupings(Set<EbCategoryGrouping> ebCategoryGroupings) {
		this.ebCategoryGroupings = ebCategoryGroupings;
	}
	public List<EbCategories> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(List<EbCategories> subCategories) {
		this.subCategories = subCategories;
	}
	public Boolean getPopular() {
		return popular;
	}
	public void setPopular(Boolean popular) {
		this.popular = popular;
	}
	public void addSubCategories(EbCategories subCatId) {
		if(this.subCategories ==null)
			this.subCategories = new ArrayList<EbCategories>();		
		this.subCategories.add(subCatId);		
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public CONSTANTS.PROFILE_CATEGORIES getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = CONSTANTS.PROFILE_CATEGORIES.valueOf(categoryType);
	}
	public String getPrimarylanguage() {
		return primarylanguage;
	}
	public void setPrimarylanguage(String primarylanguage) {
		this.primarylanguage = primarylanguage;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public String getApptus_sort_attrib() {
		return apptus_sort_attrib;
	}
	public void setApptus_sort_attrib(String apptus_sort_attrib) {
		this.apptus_sort_attrib = apptus_sort_attrib;
	}
	@Override
	public String toString() {
		return "EbCategories [categoryId=" + categoryId + ", parentCatId=" + parentCatId + ", categoryName="
				+ categoryName + ", isactive=" + isactive + ", slugName=" + slugName + ", ebCategoryGroupings="
				+ ebCategoryGroupings + ", subCategories=" + subCategories + ", popular=" + popular + ", position="
				+ position + ", categoryType=" + categoryType + "]";
	}
	 
	
}

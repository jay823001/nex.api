package se.frescano.nextory.cache.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SeriesList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4292181664462326882L;
	private ArrayList<Series> datum;

	public ArrayList<Series> getDatum() {
		return datum;
	}

	public void setDatum(ArrayList<Series> datum) {
		this.datum = datum;
	}

	
	
	

}

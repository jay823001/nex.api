package se.frescano.nextory.cache.model;

import java.io.Serializable;

public class Series implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9218171469281131877L;

	private String seriesName;
	
	private Integer displayOrderIndex;
	
	private Integer id;
	
	private Integer order;
	
	private Integer market_id;
	
	
	
	public Integer getMarket_id() {
		return market_id;
	}

	public void setMarket_id(Integer market_id) {
		this.market_id = market_id;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public Integer getDisplayOrderIndex() {
		return displayOrderIndex;
	}

	public void setDisplayOrderIndex(Integer displayOrderIndex) {
		this.displayOrderIndex = displayOrderIndex;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
	
	

}

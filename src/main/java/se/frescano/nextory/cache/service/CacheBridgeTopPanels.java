package se.frescano.nextory.cache.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import se.frescano.nextory.toppanellist.TopPanelList;

@Component
//@CacheConfig(cacheNames={"series"})
public class CacheBridgeTopPanels {
	
	Logger logger = LoggerFactory.getLogger(CacheBridgeTopPanels.class);
	@Autowired
	NextoryHCacheService nextoryHCacheService;
	
	
	@Cacheable(cacheNames={"product.nest.topPanels.list"}) 
	public TopPanelList callNestTopPanelsUrl(String url,String profile,String locale) throws Exception{		
		return nextoryHCacheService.callNestTopPanelsUrl(url, profile, locale);
	}
	
	@CacheEvict(value = "product.nest.topPanels.list", allEntries = true)
	  public void resetAllEntries() {
	    // Intentionally blank
		logger.info("nest.topPanels.list cache removed");
	  }

}

package se.frescano.nextory.cache.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import se.frescano.NestConfiguration;
import se.frescano.nextory.market.model.MarketListVo;
import se.frescano.nextory.restclients.RestException;

@Component
public class CacheBridgeMarket {
	Logger logger = LoggerFactory.getLogger(CacheBridgeMarket.class);
	@Autowired
	private NextoryHCacheService nextoryHCacheService;
	
	@Autowired
	private NestConfiguration	nestConfiguration;
	
	@Cacheable(cacheNames={"product.nest.market.cache"})
	public HashMap<String,Integer> getMarketFromNest() throws Exception, RestException{		
		return nextoryHCacheService.callNestMarketUrlMap( nestConfiguration.getMarkets() );
		
	}
	
	@Cacheable(cacheNames={"product.nest.market.cache.data"})
	public MarketListVo getMarketFromNestList(String locale) throws Exception, RestException{		
		MarketListVo[] data =nextoryHCacheService.callNestMarketUrl( nestConfiguration.getMarkets() );
		MarketListVo vo = new MarketListVo();
		for(MarketListVo marketdata:data)
		{
			if(marketdata.getLocale().equalsIgnoreCase(locale))
			{
				vo= marketdata;
				break;
			}
		}
		return vo;
	}
	
	
	@Cacheable(cacheNames={"product.nest.market.cache.countrycode"})
	public MarketListVo getMarketFromNestBasedOnCountryCode(String countrycode) throws Exception, RestException{		
		MarketListVo[] data =nextoryHCacheService.callNestMarketUrl( nestConfiguration.getMarkets() );
		MarketListVo vo = new MarketListVo();
		for(MarketListVo marketdata:data)
		{
			if(marketdata.getCountryCode().equalsIgnoreCase(countrycode))
			{
				vo= marketdata;
				break;
			}
		}
		return vo;
	}
	
	@CacheEvict(value = {"product.nest.market.cache","product.nest.market.cache.data","product.nest.market.cache.countrycode"}, allEntries = true)
	  public void resetAllEntries() {
	    // Intentionally blank
		logger.info("nest.market.cache cache removed");
	  }
}

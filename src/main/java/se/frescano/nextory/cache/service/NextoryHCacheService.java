package se.frescano.nextory.cache.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import se.frescano.NestConfiguration;
import se.frescano.nextory.cache.model.SeriesList;
import se.frescano.nextory.customer.BookIdData;
import se.frescano.nextory.market.model.MarketListVo;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.toppanellist.TopPanelList;
import se.frescano.nextory.util.Book2GoUtil;

@Service("nextoryHCacheService")
public class NextoryHCacheService {

	Logger logger = LoggerFactory.getLogger(NextoryHCacheService.class);
	
	//AbstractRestClient abstractRestClient;
	
	@Autowired
	private	AbstractRestTempleteClient	abstractresttemplate;
		
	@Autowired
	private NestConfiguration	nestConfiguration;
	
	//public NextoryHCacheService() {
		//abstractRestClient =  new AbstractRestClient();
	//}
	//@Cacheable(value = "nest.category.cache", condition = "#{${spring.profiles.active} != 'dev'}")
	
	//@Cacheable(cacheNames={"nest.category.cache"})
	public Map<Integer,EbCategories> getCategoryCache(String url)
	{
		logger.info("COMING INSIDE Actual layer instead of hazelcast nest.category.cache cache ");
		Map<Integer, EbCategories> categoryMap = null;
		Map<Integer, List<Integer>> categoryIDMap = new HashMap<Integer, List<Integer>>();
		List<EbCategories> categories = loadCategoriesFromNEST(url); 
		if(categories!=null){
			categoryMap = new HashMap<Integer, EbCategories>(); 
			Collections.sort(categories, (c1, c2) -> c1.getParentCatId() - c2.getParentCatId()); 
			for(EbCategories category : categories){
				//logger.info(category.toString());
				categoryMap.put(category.getCategoryId(), category);
				if(category.getParentCatId()!=0){
					EbCategories parent = categoryMap.get(category.getParentCatId());
					if(parent!=null){
						parent.addSubCategories(category); 
						
						List<Integer> subCatIDs = new ArrayList<Integer>();
						if(categoryIDMap.get(parent.getCategoryId())!=null)subCatIDs=categoryIDMap.get(parent.getCategoryId());
						subCatIDs.add(category.getCategoryId());
						
						categoryIDMap.put(parent.getCategoryId(), subCatIDs);
					}
				} 
			}
			
			//sort main categories by position
			List<Map.Entry<Integer, EbCategories>> list =
	                new LinkedList<Map.Entry<Integer, EbCategories>>(categoryMap.entrySet()); // get keys
			 Collections.sort(list, new Comparator<Map.Entry<Integer, EbCategories>>() {
		            public int compare(Map.Entry<Integer, EbCategories> c1,
		                               Map.Entry<Integer, EbCategories> c2) {
		                return (new Integer(c1.getValue().getPosition())).compareTo(new Integer(c2.getValue().getPosition()));
		            }
		      });
			 Map<Integer, EbCategories> sortedCategoryMap = new LinkedHashMap<Integer, EbCategories>();
			 for (Map.Entry<Integer, EbCategories> entry : list) {
				 sortedCategoryMap.put(entry.getKey(), entry.getValue());
		     }
			 
			//InMemoryCache.CATEGORY_CACHE = sortedCategoryMap;
			return sortedCategoryMap;
	
		}
		return categoryMap;
	}
	
	//@Cacheable(value="nest.category.list")
	@SuppressWarnings("unchecked")
	public List<EbCategories> loadCategoriesFromNEST(String categoryurl) {
		 
		List<EbCategories> categories = null;
		try{
			logger.info(" url to get data for nest.category.cache "+ categoryurl);
			if( categoryurl == null )
				throw new Exception("URL for the API to fetch categories from NEST is not defined. Check if NEST_BASE_URL is set in the database properties table ::"+categoryurl);
			
			//Map<String, List<Map>> categoryData = abstractRestClient.get(categoryurl, null, null,Map.class);
			@SuppressWarnings("rawtypes")
			Map<String, List<Map>> categoryData = abstractresttemplate.exchange(categoryurl, HttpMethod.GET, null, null, null, Map.class);			
			if(categoryData!=null && categoryData.get("datum")!=null){
				categories = new ArrayList<EbCategories>();
				for(Map<?, ?> catMap : categoryData.get("datum")){					
					categories.add(Book2GoUtil.objectMapper.convertValue(catMap, EbCategories.class));
				}	
			}
		}catch ( Exception e) {
			//logger.error(" Failed to load the categories from NEST URL !"+categoryurl ,e);
			//throw new Exception(" Failed to load the categories from NEST URL !");
			e.printStackTrace();
		} 
		return categories;
	}
	
	//@Cacheable(cacheNames =  {"nest.category.map"})
	/*public Map<Integer, Integer[]>  getCategoryMap()
	{
		Map<Integer, Integer[]> map = new HashMap<Integer,Integer[]>();
		Map<Integer, EbCategories> categoryMap = null;
		Map<Integer, List<Integer>> categoryIDMap = new HashMap<Integer, List<Integer>>();
		List<EbCategories> categories = loadCategoriesFromNEST( nestConfiguration.getCategories() ); 
		if(categories!=null){
			categoryMap = new HashMap<Integer, EbCategories>(); 
			Collections.sort(categories, (c1, c2) -> c1.getParentCatId() - c2.getParentCatId()); 
			for(EbCategories category : categories){
				//logger.info(category.toString());
				categoryMap.put(category.getCategoryId(), category);
				if(category.getParentCatId()!=0){
					EbCategories parent = categoryMap.get(category.getParentCatId());
					if(parent!=null){
						parent.addSubCategories(category); 
						
						List<Integer> subCatIDs = new ArrayList<Integer>();
						if(categoryIDMap.get(parent.getCategoryId())!=null)subCatIDs=categoryIDMap.get(parent.getCategoryId());
						subCatIDs.add(category.getCategoryId());
						
						categoryIDMap.put(parent.getCategoryId(), subCatIDs);
					}
				} 
			}
			
			//sort main categories by position
			List<Map.Entry<Integer, EbCategories>> list =
	                new LinkedList<Map.Entry<Integer, EbCategories>>(categoryMap.entrySet()); // get keys
			 Collections.sort(list, new Comparator<Map.Entry<Integer, EbCategories>>() {
		            public int compare(Map.Entry<Integer, EbCategories> c1,
		                               Map.Entry<Integer, EbCategories> c2) {
		                return (new Integer(c1.getValue().getPosition())).compareTo(new Integer(c2.getValue().getPosition()));
		            }
		      });
			 Map<Integer, EbCategories> sortedCategoryMap = new LinkedHashMap<Integer, EbCategories>();
			 for (Map.Entry<Integer, EbCategories> entry : list) {
				 sortedCategoryMap.put(entry.getKey(), entry.getValue());
		     }
			 
			//InMemoryCache.CATEGORY_CACHE = sortedCategoryMap;
		for(Integer key: categoryIDMap.keySet()){//convert List<Integer> to Integer[] array
			List<Integer> subCatIDs = categoryIDMap.get(key); 
			//InMemoryCache.CATEGORY_MAP.put(key, subCatIDs.toArray(new Integer[subCatIDs.size()]));
			map.put(key, subCatIDs.toArray(new Integer[subCatIDs.size()]));
		}	
	 
		//sort subcategories based on position
		for(EbCategories category : categories)
			if(category.getSubCategories()!=null && category.getSubCategories().size()>0) 
				Collections.sort(category.getSubCategories(), (s1, s2) -> s1.getPosition() - s2.getPosition()); 
		}
		
		return map;
				
	}*/
	
	
	
	public SeriesList callNestSeriesUrl(String url,String profile,String countrycode) throws Exception,RestException 
	{
		SeriesList serieslist  = abstractresttemplate.exchange(url+countrycode, HttpMethod.GET, null, null, null, SeriesList.class);
		//serieslist = abstractRestClient.get(url+marketid, null, null,SeriesList.class);
		logger.info("COMING INSIDE Actual layer instead of hazelcast nest.series.list cache ");
		return serieslist;
	}
	
	
	public BookIdData callLibraryBooksIdList(String url) throws Exception ,RestException
	{
		try
		{
			BookIdData bookIdList  = abstractresttemplate.exchange(url, HttpMethod.GET, null, null, null, BookIdData.class);
		logger.info("Getting books from library for Recommend for you");
		return bookIdList;
		}
		catch(Exception e)
		{
			logger.error("Exception while getting data from library");
			return new BookIdData();
		}	
	}
	
	
	public TopPanelList callNestTopPanelsUrl(String url,String profile,String locale) throws Exception ,RestException
	{
		TopPanelList topPanelList  = abstractresttemplate.exchange(url, HttpMethod.GET, null, null, null, TopPanelList.class);
		//topPanelList = abstractRestClient.get(url, null, null,TopPanelList.class);
		logger.info("COMING INSIDE Actual layer instead of hazelcast nest.topPanels.list cache ");
		return topPanelList;
	}
	
	public MarketListVo[] callNestMarketUrl(String url) throws Exception, RestException 
	{
		MarketListVo[] marketList  = abstractresttemplate.exchange(url, HttpMethod.GET, null, null, null, MarketListVo[].class);
		//marketList = abstractRestClient.get(url, null, null,MarketListVo[].class);

		/*HashMap<Integer,String> map = new HashMap<Integer,String>();
		if(marketList!=null)
		{
			for(MarketListVo data:marketList)
			{
				map.put(data.getMarket_id(),data.getCountryCode());
			}
		}*/		
		logger.info("COMING INSIDE Actual layer instead of hazelcast nest.market.list cache ");
		return marketList;
	}
	
	
	public HashMap<String,Integer> callNestMarketUrlMap(String url) throws Exception, RestException 
	{
		MarketListVo[] marketList  = null;
		//marketList = abstractRestClient.get(url, null, null,MarketListVo[].class);
		marketList = abstractresttemplate.exchange(url, HttpMethod.GET, null, null, null, MarketListVo[].class);
		HashMap<String,Integer> map = new HashMap<String,Integer>();
		if(marketList!=null)
		{
			for(MarketListVo data:marketList)
			{
				map.put(data.getCountryCode(),data.getMarket_id());
			}
		}
		
		logger.info("COMING INSIDE Actual layer instead of hazelcast nest.market.list cache ");
		return map;
	}
}

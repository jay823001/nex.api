package se.frescano.nextory.cache.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.model.EbCategories;

@Service
public class HazelCastDependentService {

	/*@Autowired 
	CacheBridge cacheBridge;*/
	
	
	
		public EbCategories _getCategory(String slug,Map<Integer,EbCategories> cat){
			//Map<Integer,EbCategories> cat = cacheBridge.getCategoryCache();
			EbCategories filtered =  cat.values().stream().filter(ebCategories -> slug.equalsIgnoreCase(ebCategories.getSlugName()))
					.findFirst().orElse(null);
			return filtered;
		}
		
		public  Integer _getCategoryId(String slug,Map<Integer,EbCategories> cat){
			EbCategories filtered =  _getCategory(slug,cat);
			return filtered!=null?filtered.getCategoryId():0;
		}
		
		public  EbCategories getKidsParentCategory(Map<Integer,EbCategories> cat){
			EbCategories kidsParent = cat.values().stream() 
					.filter(ebcategory -> ebcategory.getParentCatId()==0 && ebcategory.getCategoryType()==CONSTANTS.PROFILE_CATEGORIES.KIDS)
					.findFirst().orElse(null);
			return kidsParent;
		}
		
		public   List<EbCategories> getAllParentCategories(Map<Integer,EbCategories> cat){
			 List<EbCategories> allParents =  
					 cat
					 .values()
					 .stream()
					.filter(ebcategory -> ebcategory.getParentCatId()==0)
					.collect(Collectors.toList());
					
			return allParents;
		}
}

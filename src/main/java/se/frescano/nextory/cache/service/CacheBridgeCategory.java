package se.frescano.nextory.cache.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import se.frescano.NestConfiguration;
import se.frescano.WebSlugMapperConfig;
import se.frescano.nextory.model.EbCategories;

@Component
public class CacheBridgeCategory {
	Logger logger = LoggerFactory.getLogger(CacheBridgeCategory.class);
	@Autowired
	NextoryHCacheService nextoryHCacheService;
	
	@Autowired
	private NestConfiguration	nestconfiguration;
	
	@Autowired
	private WebSlugMapperConfig slugMapper;
	
	/*@Cacheable(cacheNames={"nest.category.cache"})
	public Map<Integer,EbCategories> getCategoryCache(String countrycode){
		String url = CONSTANTS.NEST_BASE_URL+CONSTANTS.NEST_CATEGORIES_URL_v1+countrycode;
		logger.info("Fetch & Cache categories from NEST :::: " + url);
		return nextoryHCacheService.getCategoryCache(url);
	}*/
	
	
	@Cacheable(cacheNames={"product.nest.category.cache"})
	public Map<Integer,EbCategories> getCategoryCache(String countrycode)
	{			
		logger.info("COMING INSIDE Actual layer instead of hazelcast nest.category.cache cache ");
		Map<Integer, EbCategories> categoryMap = null;
		Map<Integer, List<Integer>> categoryIDMap = new HashMap<Integer, List<Integer>>();
		List<EbCategories> categories = nextoryHCacheService.loadCategoriesFromNEST( nestconfiguration.getCategories()+countrycode ); 
		Map<String, String> slugMap = slugMapper.getSlug();
		if(categories!=null){
			categoryMap = new HashMap<Integer, EbCategories>(); 
			Collections.sort(categories, (c1, c2) -> c1.getParentCatId() - c2.getParentCatId()); 
			for(EbCategories category : categories){
				//logger.info(category.toString());
				//replace auto-generated slugname with mapped values
				if(StringUtils.isNotBlank(category.getSlugName()) && slugMap.containsKey(category.getSlugName()))
					category.setSlugName(slugMap.get(category.getSlugName()));
				
				categoryMap.put(category.getCategoryId(), category);
				if(category.getParentCatId()!=0){
					EbCategories parent = categoryMap.get(category.getParentCatId());
					if(parent!=null){
						parent.addSubCategories(category); 
						
						List<Integer> subCatIDs = new ArrayList<Integer>();
						if(categoryIDMap.get(parent.getCategoryId())!=null)subCatIDs=categoryIDMap.get(parent.getCategoryId());
						subCatIDs.add(category.getCategoryId());
						
						categoryIDMap.put(parent.getCategoryId(), subCatIDs);
					}
				} 
			}
			
			//sort main categories by position
			List<Map.Entry<Integer, EbCategories>> list =
	                new LinkedList<Map.Entry<Integer, EbCategories>>(categoryMap.entrySet()); // get keys
			 Collections.sort(list, new Comparator<Map.Entry<Integer, EbCategories>>() {
		            public int compare(Map.Entry<Integer, EbCategories> c1,
		                               Map.Entry<Integer, EbCategories> c2) {
		                return (new Integer(c1.getValue().getPosition())).compareTo(new Integer(c2.getValue().getPosition()));
		            }
		      });
			 Map<Integer, EbCategories> sortedCategoryMap = new LinkedHashMap<Integer, EbCategories>();
			 for (Map.Entry<Integer, EbCategories> entry : list) {
				 sortedCategoryMap.put(entry.getKey(), entry.getValue());
		     }
			 
			//InMemoryCache.CATEGORY_CACHE = sortedCategoryMap;
			return sortedCategoryMap;
	
		}
		return categoryMap;
	}
	
	@CacheEvict(value = "product.nest.category.cache", allEntries = true)
	public void resetAllEntries() {
	    // Intentionally blank
		logger.info("nest.category.cache cache removed");
	}
	
	public List<EbCategories> getCategoriesExcludeParentCategory(Map<Integer,EbCategories> categories, String slugname){
		List<EbCategories> subcategories = new ArrayList<EbCategories>();	
		for(Entry<Integer,EbCategories> data :categories.entrySet()){
			EbCategories d1= data.getValue();
			if(StringUtils.equalsIgnoreCase(d1.getSlugName(), slugname)){				
				subcategories.addAll( d1.getSubCategories() );
			}
		}
		return subcategories;
	}
}

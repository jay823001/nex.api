package se.frescano.nextory.cache.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import se.frescano.nextory.cache.model.SeriesList;
import se.frescano.nextory.restclients.RestException;

@Component
//@CacheConfig(cacheNames={"series"})
public class CacheBridgeSeries {
	
	Logger logger = LoggerFactory.getLogger(CacheBridgeSeries.class);
	@Autowired
	NextoryHCacheService nextoryHCacheService;
	
	
	@Cacheable(cacheNames={"product.nest.series.list"})
	public SeriesList callNestSeriesUrl(String url,String profile, String countrycode) throws Exception,RestException {
		return nextoryHCacheService.callNestSeriesUrl(url, profile,countrycode);
	}

	
	@CacheEvict(value = "product.nest.series.list", allEntries = true)
	  public void resetAllEntries() {
	    // Intentionally blank
		logger.info("nest.series.list cache removed");
	  }
}

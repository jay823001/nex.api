package se.frescano.nextory.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 
 
public class HASHGenerator { 
 static Logger logger=LoggerFactory.getLogger(HASHGenerator.class);
    private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
 
    public static String SHA1(String text) 
    throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
	    MessageDigest md;
	    md = MessageDigest.getInstance("SHA-1");
	    byte[] sha1hash = new byte[40];
	    md.update(text.getBytes("UTF-8"), 0, text.length());
	    sha1hash = md.digest();
	    return convertToHex(sha1hash);
    } 
    public static String MD5CaseSensitive(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException{
    	 MessageDigest md;    	 
 	     md = MessageDigest.getInstance("MD5");
 	     byte[] md5hash = new byte[40];
 	     md.update(text.getBytes("UTF-8"), 0, text.length());
 	     md5hash = md.digest();
 	     return convertToHex(md5hash);
    }
    public static String MD5UpperCase(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException{
   	 String strHash = MD5CaseSensitive(text);
   	 return strHash.toUpperCase();
   }

    /*public static void main(String[] args) throws Exception{
    	StringBuilder steambuild = new StringBuilder();
    	steambuild.append("WFNQlLCH").append("84c1a598-aa19-4f65-bdb0-d11da74ee705")
    	.append("1000006").append("2")
    	.append("1000005").append("1").append(1);
		
		String streamchecksum;
		
		streamchecksum = HASHGenerator.MD5UpperCase(steambuild.toString());
		System.out.println(streamchecksum);
    	
    	String s = "test";
    	String c = HASHGenerator.MD5CaseSensitive(s);
    	if(logger.isDebugEnabled())
    		logger.debug(s +" ---- > "+c);
    	//System.out.println(s +" ---- > "+c);
    	
	}*/
} 


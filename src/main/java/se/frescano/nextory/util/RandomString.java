package se.frescano.nextory.util;

import java.util.Random;

public class RandomString {

   // private static  Logger logger=LoggerFactory.getLogger(RandomString.class);
    private static final String
    charset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String getRandomString(int length) {
    	Random rand = new Random(System.currentTimeMillis());
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i <length; i++) {
    		int pos = rand.nextInt(charset.length());
    		sb.append(charset.charAt(pos));
    	}
    	return sb.toString();
    }
    /*public static void main(String[] args) {
    	if(logger.isDebugEnabled())
    		logger.debug(getRandomString(8));
    	//System.out.println(getRandomString(8));
    }
*/
}


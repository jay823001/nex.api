package se.frescano.nextory.util;

import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import se.frescano.ApplicationPropertiesConfig;

@DependsOn("applicationpropconfig")
@Component("sendMailService")
public class SendMailService {

	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	/*@Value("${nextory.mail.from}")
	private String from;
	@Value("${nextory.mail.subject.forgotpassword}")
	private String subject;*/
	//private boolean debug;
	private String bcc;
	private static final Logger logger = LoggerFactory
			.getLogger(SendMailService.class);

	//private final String testprefix = "";

	/*public JavaMailSenderImpl getMailSender() {
		return mailSender;
	}

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}*/
/*
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}*/

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public void sendMail(String sub, String[] to, String msg, String ovfrom)
			throws Exception {
		sendMailToMultiple(sub, msg, ovfrom, to);
	}
	
	

	public void sendMailToMultiple(String sub, String msg, String sendAs,
			String... receipients) throws Exception {
		// SimpleMailMessage message = new SimpleMailMessage();
		MimeMessage message = mailSender.createMimeMessage();
		if (InMemoryCache.isProductionEnv)
			sub = InMemoryCache.environment + sub;
		message.setSubject(sub);
		message.setFrom(new InternetAddress(applicationpropconfig.getMailfrom()));

		if (StringUtils.isNotBlank(getBcc())) {
			message.addRecipient(Message.RecipientType.BCC,
					new InternetAddress(getBcc()));
		}
		for (int i = 0; i < receipients.length; i++) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					receipients[i]));
		}

		message.setSentDate(new Date());

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(msg.toString(),
				"text/html; charset=ISO-8859-1");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		message.setContent(multipart);
		if (StringUtils.isNotBlank(sendAs))
			message.setReplyTo(InternetAddress.parse(sendAs));
		//mailSender.send(message);
		try{
			mailSender.send(message);
		}catch(MailSendException mse){
			logger.error("Error while sending mail::"+ mse);
			logger.error("Mail Send Exceptions:"+ mse.getMessageExceptions());			
			logger.error("Most specific cause"+mse.getMostSpecificCause());
			
		}catch(MailAuthenticationException mae){
			logger.error("Error while sending mail::"+ mae);
			logger.error("MailAuthenticationException:"+mae.getMessage());
			logger.error("MailAuthenticationException Most specific cause"+mae.getMostSpecificCause());			
		}catch(MailException me){
			logger.error("MailException ::"+me.getMessage());
			logger.error("MailException ::"+me);
			logger.error("MailException - most specific cause::"+me.getMostSpecificCause());
		}
	}


	public void sendMail(String sub, String[] to, String msg) {
		try {
			sendMail(sub, to, msg, applicationpropconfig.getMailfrom());
		} catch (Exception e) {
			logger.error("Exception occured while sending the message ", e);
		}
	}


	public void sendMailAttachment(String[] sReceipients, String sBody,
			String sendAs, String sSubject, String sAttachmentFileName,
			DataSource dataSource) throws Exception {
		MimeMessage message = mailSender.createMimeMessage();
		if (InMemoryCache.isProductionEnv)
			sSubject = InMemoryCache.environment + sSubject;

		message.setSubject(sSubject);
		message.setFrom(new InternetAddress( applicationpropconfig.getMailfrom()));
		if (StringUtils.isNotBlank(getBcc())) {
			message.addRecipient(Message.RecipientType.BCC,
					new InternetAddress(getBcc()));
		}
		for (int i = 0; i < sReceipients.length; i++) {
			String sReceipient = sReceipients[i];
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					sReceipient));
		}
		message.setSentDate(new Date());
		BodyPart messageBodyPart = new MimeBodyPart();
		// messageBodyPart.setText(sbuffer.toString());
		// messageBodyPart.setContent(sBody, "text/html; charset=ISO-8859-1");
		messageBodyPart.setContent(sBody, "text/html; charset=UTF-8");
		MimeBodyPart attachmentPart = new MimeBodyPart();
		attachmentPart.setDataHandler(new DataHandler(dataSource));
		attachmentPart.setFileName(sAttachmentFileName);
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		multipart.addBodyPart(attachmentPart);
		message.setContent(multipart);
		if (StringUtils.isNotBlank(sendAs))
			message.setReplyTo(InternetAddress.parse(sendAs));
		try{
			mailSender.send(message);
		}catch(MailSendException mse){
			logger.error("Error while sending mail::"+ mse);
			logger.error("Mail Send Exceptions:"+ mse.getMessageExceptions());			
			logger.error("Most specific cause"+mse.getMostSpecificCause());
			
		}catch(MailAuthenticationException mae){
			logger.error("Error while sending mail::"+ mae);
			logger.error("MailAuthenticationException:"+mae.getMessage());
			logger.error("MailAuthenticationException Most specific cause"+mae.getMostSpecificCause());			
		}catch(MailException me){
			logger.error("MailException ::"+me.getMessage());
			logger.error("MailException ::"+me);
			logger.error("MailException - most specific cause::"+me.getMostSpecificCause());
		}		
	}

}

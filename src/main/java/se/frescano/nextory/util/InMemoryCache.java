package se.frescano.nextory.util;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS.API_TYPE;
import se.frescano.nextory.app.zendesk.vo.ZendeskDateResponseVO;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.web.api.model.CampaignTypeInfo;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Service("inMemoryCache")
public class InMemoryCache implements ApplicationContextAware{	
		

	@SuppressWarnings("unused")
	private static ApplicationContext 	ac;
	
	public static String contextPath = "";
	private static Logger logger=LoggerFactory.getLogger(InMemoryCache.class);
	public static String salt ="WFNQlLCH";
	//public static int[] preferredFormats = new int[1];
	public static String totalBookCount = null;
	public static String eBookCount = null;
	public static String audioBookCount = null;
	
	public static List<CampaignTypeInfo> campTypes = null;
	public static boolean isProductionEnv = false;
	public static String environment ;	
	public static ZendeskDateResponseVO zendeskDateResponseVO;
	public static DateTime zendeskDate;
	public static Map<String , Integer> subAgeLimits = new HashMap<String , Integer>();
	public static User defaultUser;
	
	@Override
	public void setApplicationContext(ApplicationContext ac) {
		InMemoryCache.ac = ac;
	}
		
	public static int getActiveBookLimit(Double apiVersion){
		Integer		limit	= CONSTANTS.OFFLINE_READER_MIN_BOOK_LIMIT;	
		if( apiVersion == null ){
			return limit;
		}else if( apiVersion < 5.1 ){
			return limit;
		}else if( apiVersion >= 5.1 ){
			limit	= CONSTANTS.OFFLINE_READER_MAX_BOOK_LIMIT;
		}		
		return limit;
	}
	
	public static Integer compareSubscriptionAgeLimit(String subscriptionType, String subscriptionType1){   
		int	orderseq 	= (subAgeLimits.get(subscriptionType) != null)?subAgeLimits.get(subscriptionType):-1;
		int	orderseq_1 	= ( subAgeLimits.get(subscriptionType1) != null)?subAgeLimits.get(subscriptionType1):-1;
		if( orderseq != -1 && orderseq_1 != -1)
			return orderseq - orderseq_1;
		else
			return -1;			
	}

	public static Integer getSubscriptionAgeLimit(String subscriptionType, String countrycode){   
		return subAgeLimits.get(subscriptionType);
	}
	
	public static Double getAPIVersion(String url){
		Double version 	= null;
		Matcher matcher = CONSTANTS.API_PATTERN.matcher(url);
		if (matcher.find() && matcher.groupCount()>1) {
			String verStr = matcher.group(2);
			if(verStr!=null)
				version = Double.parseDouble(verStr);
		}		 
		return version;
	}
	
	public static API_TYPE getAPIType(String url){
		API_TYPE apiType = null; 
		Matcher matcher = CONSTANTS.API_PATTERN.matcher(url);
		if (matcher.find() && matcher.groupCount()>1) {
			String apitype = matcher.group(1);
			
			if(StringUtils.isBlank(apitype))
				apiType = API_TYPE.APP;
			else
				apiType = API_TYPE.valueOf(apitype.toUpperCase());
		}
		return apiType;
	}
	
	public static void sendErrorMsg(HttpServletResponse response, WEBErrorCodeEnum errorCode) {
		GenericAPIResponse apiResponse	=	new GenericAPIResponse(HttpStatus.FORBIDDEN,
				errorCode,errorCode.getMessage() );	 		
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		try {
			response.getWriter().write(Book2GoUtil.objectMapper.writeValueAsString(apiResponse));
		} catch (IOException e) { 
			logger.error("sendErrorMsg::Failed to write error msg ",e.getMessage());
		}
	}
	
}

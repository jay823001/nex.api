package se.frescano.nextory.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.CodeSource;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import datadog.trace.api.CorrelationIdentifier;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.mongo.product.model.Contributors;
import se.frescano.nextory.mongo.product.model.ProductData;
import se.frescano.nextory.mongo.product.model.Relations;

public class Book2GoUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(Book2GoUtil.class);
	private static String ENCODING = "UTF-8";
	private static String SYNC_DATE_TIME_ZONE_PLACE = "Europe/Stockholm";

	private static String SYNC_DATE_INPUT_PATTERN = "MM-dd-yyyy HH:mm:ss Z";
	private static DateTimeFormatter SYNC_DATE_INPUT_FORMAT = DateTimeFormat
			.forPattern(SYNC_DATE_INPUT_PATTERN);
	public static String APP_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss Z";
	private static DateTimeFormatter APP_DATE_FORMAT = DateTimeFormat
			.forPattern(APP_DATE_PATTERN);		

	private static String SYNC_DATE_OUTPUT_PATTERN = "yyyy-MM-dd'T'H:mm:ss.SSSZ";

	public static Locale defaultLocale = new Locale("sv", "SE");
	
	
	public static String APPTUS_DATE_FORMAT		   = "yyyy-MM-dd'T'HH:mm:ssZ";	
	private static SimpleDateFormat apptus_date_formater = new SimpleDateFormat( APPTUS_DATE_FORMAT );
	
	public static final ObjectMapper objectMapper = new ObjectMapper();
	
	private static int index = 0;

	private static String stringTobeReplacedForDetailsURL = "\u0020\u00e1\u00e0\u00e2\u00e5\u00e3\u00e4\u00e6\u0105\u00e7\u0107\u00e9\u00e8\u00ea\u00eb\u0119\u00ed\u00ec\u00ee\u00ef\u0142\u00f1\u0144\u00f3\u00f2\u00f4\u00f8\u00f5\u00f6\u015b\u00df\u00fa\u00f9\u00fb\u00fc\u00ff\u017a\u017c\u003f\u002F";

	public static DateTime baseDate;
	
	static{
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		Calendar cal= Calendar.getInstance();
		cal.set(1990, 1, 1, 0, 0);
		baseDate = new DateTime(cal.getTime());
	}
	
	public static Integer parseString(String str) {
		Integer value = null;
		if (StringUtils.isNotBlank(str)) {
			try {
				value = Integer.parseInt(str);
			} catch (NumberFormatException e) {
				value = null;
			}
		}
		return value;
	}

	public static Long parseStringAsLong(String str) {
		Long value = null;
		if (StringUtils.isNotBlank(str)) {
			try {
				value = Long.parseLong(str);
			} catch (NumberFormatException e) {
				value = null;
			}
		}
		return value;

	}

	public static Float parseStringAsFloat(String str) {
		Float value = null;
		if (StringUtils.isNotBlank(str)) {
			try {
				value = Float.parseFloat(str);
			} catch (NumberFormatException e) {
				value = null;
			}
		}
		return value;

	}

	public static Double parseStringAsDouble(String str) {
		Double value = null;
		if (StringUtils.isNotBlank(str)) {
			try {
				value = Double.parseDouble(str);
				return value;
			} catch (NumberFormatException e) {
				value = null;
			}
		}
		return 0.0;

	}

	/*
	 * This method is not used currently , but however this can be used for checking the class file mismatches to understand the name of the 
	 * jar the class file is getting loaded.
	 */
	public static void test(Class<?> klass){
	    CodeSource codeSource = klass.getProtectionDomain().getCodeSource();
	    if ( codeSource != null) {
	        System.out.println(codeSource.getLocation());
	    }
	}
	
	public static Date getTodaysDate() {
		java.sql.Date sqlDate = new java.sql.Date(
				new java.util.Date().getTime());
		return sqlDate;
	}

	public static Timestamp getTodaystimeStamp() {
		Timestamp sqlDate = new java.sql.Timestamp(
				new java.util.Date().getTime());
		return sqlDate;
	}

	public static Timestamp convertStringToTimeStamp(String value) {
		if (StringUtils.isBlank(value))
			return null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Timestamp sqlDate = null;
		java.util.Date d = null;
		try {
			d = df.parse(value);
			sqlDate = new java.sql.Timestamp(d.getTime());

		} catch (Exception e) {
			String str1 = StringUtils.substringAfter(value, "T");
			if (StringUtils.isBlank(str1))
				value = value + "T00:00:00";
			else {
				int count = StringUtils.countMatches(value, ":");
				if (count == 1)
					value = value + ":00";
				if (count == 0)
					value = value + ":00:00";
			}
			try {
				d = df.parse(value);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			sqlDate = new java.sql.Timestamp(d.getTime());
		}
		return sqlDate;
	}

	

	public static String trimDiscription(String dis) {
		int length = 200;
		if (StringUtils.isNotBlank(dis) && dis.length() > length + 1) {
			String strDesc = dis.substring(0, length);
			int idx = strDesc.lastIndexOf('<');
			if (idx > length - 1)
				return getStrippedDescription(strDesc.substring(0, idx));
			else
				return getStrippedDescription(strDesc);
		}
		return StringUtils.isNotBlank(dis) ? getStrippedDescription(dis) : dis;
	}

	/**
	 * This function is used to stripe the html tags especially while providing
	 * book details to APP. Only considering Line Break <BR>
	 * and rest of tags striped from the description.
	 * 
	 * @param discription
	 * @return
	 */
	public static String stripAppDiscription(String discription) {
		if (StringUtils.isNotBlank(discription)) {
			discription = getStrippedDescription(discription);
		}
		return discription;
	}

	public static String getStrippedDescription(String desc) {
		Whitelist whitelist = Whitelist.none().addTags("br", "p");// NEX-1550(
																	// </p>,<br/>,<br>,<br
																	// />html
																	// tags
																	// should be
																	// replaced
																	// with \n)
		Cleaner cleaner = new Cleaner(whitelist);
		desc = StringEscapeUtils.unescapeHtml3(desc);
		desc = StringEscapeUtils.unescapeHtml4(desc);
		Document origDoc = Jsoup.parse(desc);
		index = desc.indexOf("<a href", index);
		if (index != -1)
			desc = extractHrefFromString(origDoc.body().html());
		desc = desc.replaceAll("</p>|<br/>|<br>|<br />", "#NL");// #NL is used
																// as temporary
																// string
																// because HTML
																// cleaner is
																// ignoring \n
																// but the
																// requirement
																// is to provide
																// \n in the
																// response so
																// before
																// passing the
																// description
																// to HTML
																// cleaner
																// replacing the
																// <br> and </p>
																// tags with #NL
		Document origDoc1 = Jsoup.parse(desc);
		Document cleanDoc = cleaner.clean(origDoc1);
		cleanDoc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
		desc = Jsoup.parse(desc).text();
		return desc.replace("#NL", "\n");// replacing temporary string #NL back
											// to \n
	}

	private static String extractHrefFromString(String desc) {
		String finalDesc = "";
		if (null != desc || !"".equals(desc)) {
			int len = desc.length();
			int a = desc.indexOf("<a href", index);
			if (a != -1) {
				int b = desc.indexOf("</a>", index);
				String a1 = desc.substring(0, a);
				String c1 = desc.substring(b + 4, len);
				String b1 = desc.substring(a, b + 4);
				int indexOfHref = b1.indexOf("\"");
				int endIndex = b1.lastIndexOf("\"");
				String url = b1.substring(indexOfHref + 1, endIndex);
				finalDesc = a1.concat(" ").concat(url).concat(" ").concat(c1);
				index = a;
			} else {
				index = -1;
				return desc;
			}

		} else {
			index = -1;
			return desc;
		}
		return finalDesc;
	}

	

	public static String trimDiscription(String dis, int length) {
		if (StringUtils.isNotBlank(dis) && dis.length() > length) {
			String strDesc = dis.substring(0, length);
			int idx = strDesc.lastIndexOf('<');
			if (idx > (length - 10))
				return getStrippedDescription(strDesc.substring(0, idx));
			else
				return getStrippedDescription(strDesc);
		}
		return StringUtils.isNotBlank(dis) ? getStrippedDescription(dis) : dis;
	}

	

	public static String removeAllSpecial(String string, String newPattern) {
		return string.replaceAll("[^#_\\da-zA-Z������]|(?<!,:)\\s", newPattern);
	}


	public static String getLocalizedString(Locale local, String key) {
		if (key == null)
			return null;
		if (local == null)
			local = defaultLocale;

		ResourceBundle rb = ResourceBundle.getBundle("messages", local);
		if (rb.containsKey(key))
			return rb.getString(key);
		else
			return null;
	}

	public static Locale getLocaleBasedOnString(String localestr) {
		Locale locale = null;
		if (!StringUtils.isBlank(localestr))
			locale = new Locale(localestr);
		return locale;
	}
	
	public static String buildReqContextURL(String sScheme, String sServername,
			int iPort, String sContextPath) {
		StringBuilder sbURL = new StringBuilder();

		sbURL.append(sScheme).append("://").append(sServername);

		// Only add port if not default
		if ("http".equals(sScheme)) {
			if (iPort != 80)
				sbURL.append(":").append(iPort);
		} else if ("https".equals(sScheme)) {
			if (iPort != 443)
				sbURL.append(":").append(iPort);
		}
		sbURL.append(sContextPath);
		return sbURL.toString();

	}

	public static final String ALLOWED_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.!~*'()";


	public static float getPriceExcludingVat(int moms, float orderamount) {
		return (float) (orderamount / (1 + (moms / 100.0f)));
	}

	public static String pointFormatter(float point) {
		int ipoints = (int) point;
		String result = "0";
		if ((point - ipoints == 0))
			result = String.valueOf(ipoints);
		else
			result = String.valueOf(point);
		return result;
	}

	public static String encodeForUrlRedirect(String queryString) {
		try {
			return URLEncoder.encode(queryString, ENCODING);
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported encoding for  " + ENCODING, e);
		}
		return queryString;
	}

	public static Date getNextSubscriptionRunDate(java.util.Date date) {
		Date nextDate = null;
		if (date != null) {
			nextDate = getNextSubscriptionRunDate(date,
					(int) CONSTANTS.SUBSCRIPTION_RUN_DAYS);
		} else
			logger.error("give Date is null  :::" + date);
		return nextDate;
	}

	public static Date getNextSubscriptionRunDate(java.util.Date date,
			int noofdays) {
		Date nextDate = null;
		if (date != null) {
			nextDate = new java.sql.Date(DateUtils.addDays(date, noofdays)
					.getTime());
		} else
			logger.error("give Date is null  :::" + date);
		return nextDate;
	}

	public static boolean IsMember(MemberTypeEnum membertype) {
		return membertype.isMember();
	}

	public static String buildUrl(Integer productid,
			ProductFormatEnum producttype, String author, String isbn,
			String prodtitle, String magTitle) {
		StringBuilder url = new StringBuilder();

		if ((producttype.equals(ProductFormatEnum.E_BOOK))
				|| (producttype.equals(ProductFormatEnum.AUDIO_BOOK))) {
			String title = replaceSwedishcharcters(prodtitle);

			String auth = replaceSwedishcharcters(author);
			url.append(title).append("-").append(auth).append("/").append(isbn);
		}

		return "/bok/" + getFinalUrl(url.toString());
	}

	public static String buildUrl(Integer productid,
			ProductFormatEnum producttype, String author, String isbn,
			String prodtitle, String magTitle, String esalesTicket) {
		StringBuilder url = new StringBuilder();

		if ((producttype.equals(ProductFormatEnum.E_BOOK))
				|| (producttype.equals(ProductFormatEnum.AUDIO_BOOK))) {
			String title = replaceSwedishcharcters(prodtitle);

			String auth = replaceSwedishcharcters(author);
			url.append(title).append("-").append(auth).append("/").append(isbn);
		}

		return "/bok/" + getFinalUrl(url.toString()) + "?esalesticket="
				+ esalesTicket;
	}

	private static String replaceSwedishcharcters(String value) {
		value = StringUtils.replace(StringUtils.lowerCase(value), "-", "");
		value = StringUtils.replaceChars(value,
				stringTobeReplacedForDetailsURL,
				"-aaaaaaaacceeeeeiiiilnnoooooossuuuuyzz");
		return value;
	}


	private static String getFinalUrl(String customurl) {
		return customurl.toLowerCase().replaceAll("[^\\-\\da-zA-Z0-9\\/]", "");
	}


	public static String convertUtilDtToString(java.util.Date date) {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date today = date;
		String getUtilDate = df.format(today);
		return getUtilDate;
	}

	public static java.util.Date convertStringToUtilDtTime(String date) {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date today = null;
		try {
			today = df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return today;
	}

	public static java.util.Date convertStringToDateTimeForLibrarySyncv5(
			String syncDate) {
		DateTime temp = SYNC_DATE_INPUT_FORMAT.withOffsetParsed()
				.parseDateTime(syncDate);
		DateTimeZone fromZone = DateTimeZone.forID(temp.getZone().getID());
		DateTimeZone toZone = DateTimeZone.forID(SYNC_DATE_TIME_ZONE_PLACE);
		DateTime toDateTime = new DateTime(temp.withZoneRetainFields(fromZone))
				.withZone(toZone);
		DateTimeFormatter formatter = DateTimeFormat
				.forPattern(SYNC_DATE_OUTPUT_PATTERN);
		DateTime newDate = formatter.withOffsetParsed().parseDateTime(
				toDateTime.toString());
		return newDate.toDate();
	}

	// Unix time stamp is in seconds and java expects milli seconds
	public static String convertFromUnixTStampForSynch(long syncDate) {
		return convertFromTStampForSynch(syncDate * 1000);
	}

	public static String convertFromUnixTStampForCache(long syncDate) {

		DateTimeZone toZone = DateTimeZone.forID(SYNC_DATE_TIME_ZONE_PLACE);
		String serverTime = APP_DATE_FORMAT.withZone(toZone).print(
				syncDate * 1000);
		return serverTime;

	}

	public static String convertFromTStampForSynch(long syncDate) {
		DateTimeZone toZone = DateTimeZone.forID(SYNC_DATE_TIME_ZONE_PLACE);
		DateTime toDateTime = new DateTime(syncDate).withZone(toZone);
		DateTimeFormatter formatter = DateTimeFormat
				.forPattern(SYNC_DATE_OUTPUT_PATTERN);
		DateTime newDate = formatter.withOffsetParsed().parseDateTime(
				toDateTime.toString());
		String serverTime = SYNC_DATE_INPUT_FORMAT.withZone(toZone).print(
				newDate.getMillis());
		return serverTime;
	}

	public static String convertSecondsIntoTime(Integer seconds) {
		StringBuilder query = new StringBuilder();
		if (seconds != null && seconds.intValue() > 0) {
			int hours = seconds.intValue() / (60 * 60);
			int mins = ((seconds.intValue() / 60) - (hours * 60));

			if (hours > 0) {
				query.append(hours).append(" h ");

			}
			if (mins > 0) {
				query.append(mins).append(" min ");
			}
		}
		return query.toString();

	}


	public static java.util.Date getTodaysUtilDate() {
		java.util.Date utilDate = new java.util.Date(
				new java.util.Date().getTime());
		return utilDate;
	}

	public static String convertDecimalToComma(double decimalValue) {
		Locale locale = new Locale("sv", "SE");
		NumberFormat numberFormat = NumberFormat.getInstance(locale);
		String number = numberFormat.format(decimalValue);
		return number;
	}

	public static String getFormattedBookCount(int bookcount) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(' ');
		DecimalFormat format = new DecimalFormat("#,###", symbols);
		String formatted = String.format("%14s", format.format(bookcount));
		return formatted.trim();
	}

	public static String constructGiftCardFileName(Integer purchasedby,
			Integer purchasedorderno, Integer id) {
		return purchasedby + "_" + purchasedorderno + "_" + id + ".pdf";
	}

	/*public static String getBookedAllowedSubscriptionsNew(
			SubscriptionTypeVO subTypeVO, String allowed_for, String subprice, String countrycode) {
		final String finalsub = StringUtils.isNotBlank(allowed_for) ? allowed_for
				: subprice;
		final SubscriptionTypeVO finalsubVO = InMemoryCache
				.getSubscriptionTypeVO(finalsub,countrycode);

		List<String> allowedSub = InMemoryCache.getActiveSubMap(countrycode).stream()
				.filter(vo -> finalsubVO.getRank() <= vo.getRank())
				.map(SubscriptionTypeVO::getSubName)
				.collect(Collectors.toList());
		return StringUtils.join(allowedSub, ",");
	}*/

/*
	public static boolean isBookAllowedForUserNew(SubscriptionTypeVO subTypeVO,
			String allowed_for, String subprice, String countrycode) {
		if (!StringUtils.isEmpty(allowed_for)) {
			if (subTypeVO.getRank() >= InMemoryCache.getSubscriptionTypeVO(
					allowed_for,countrycode).getRank()) {
				return true;
			}
		} else {
			if (subTypeVO.getRank() >= InMemoryCache.getSubscriptionTypeVO(
					subprice,countrycode).getRank()) {
				return true;
			}
		}
		return false;
	}*/

	/*public static SubscriptionTypeVO getBookAllowedForSubscriptionType(
			String allowed_for, double price, String  countrycode) {

		List<SubscriptionTypeVO> subMap = InMemoryCache.getActiveSubMap(countrycode);

		if (!StringUtils.isEmpty(allowed_for)) {
			SubscriptionTypeVO priceSubvo = InMemoryCache
					.getSubscriptionTypeVO(allowed_for,countrycode);
			if (priceSubvo != null)
				price = priceSubvo.getSubPrice();
		}
		SubscriptionTypeVO least_allowed_Sub = null;
		for (SubscriptionTypeVO subscriptionTypeVO : subMap) {
			if (least_allowed_Sub == null) {
				if (price <= subscriptionTypeVO.getSubPrice())
					least_allowed_Sub = subscriptionTypeVO;
			} else if (subscriptionTypeVO.getRank() < least_allowed_Sub
					.getRank()) {
				if (price <= subscriptionTypeVO.getSubPrice())
					least_allowed_Sub = subscriptionTypeVO;
			}
		}
		return least_allowed_Sub;

	}*/

	public static int calculateDaysToAdd(int qty, float subscriptionAmount,
			float gcMonthlyValue) {
		return (int) Math.round((30 * qty * gcMonthlyValue)
				/ subscriptionAmount);
	}


	public static String dateFormatForVer5(Long seconds) {
		DateFormat df = new SimpleDateFormat(APP_DATE_PATTERN);
		return (seconds != null && seconds > 0) ? df.format(seconds) : "";
	}

	/*public static String dateFormatForVer5(java.util.Date date) {
		DateFormat df = new SimpleDateFormat(APP_DATE_PATTERN);
		return date != null ? df.format(date) : "";
	}*/

	public static Long verifyInputDateFormatForVer5(String dateasStr) {
		try {
			DateTimeFormatter SYNC_DATE_INPUT_FORMAT = DateTimeFormat
					.forPattern(APP_DATE_PATTERN);
			return dateasStr != null ? SYNC_DATE_INPUT_FORMAT.parseDateTime(
					dateasStr).getMillis() : null;
		} catch (Exception e) {

		}
		return null;
	}

	public static String trimBookDiscription(String dis) {
		int length = 200;
		if (StringUtils.isNotBlank(dis) && dis.length() > length + 1) {
			String strDesc = dis.substring(0, length);
			int idx = strDesc.lastIndexOf('<');
			if (idx > length - 1)
				return getStrippedDescription(strDesc.substring(0, idx));
			else
				return getStrippedDescription(strDesc);
		}
		return StringUtils.isNotBlank(dis) ? getStrippedDescription(dis) : dis;
	}

	public static int hoursDifference(Date date1, Date date2) {
		final int MILLI_TO_HOUR = 1000 * 60 * 60;
		return (int) (date1.getTime() - date2.getTime()) / MILLI_TO_HOUR;
	}


	static ObjectMapper mapper = new ObjectMapper();

	public static String toJSON(Object o) {
		String result = "";
		if (o != null) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				mapper.writeValue(out, o);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				result = out.toString("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static float calculateCampaignPrice(float subscriptionPrice,
			byte campaigncodeType, float campaignPrice) {
		float price = 0;
		switch (campaigncodeType) {
		case CONSTANTS.FIXED_CAMPAIGN:
			price = campaignPrice;
			break;
		case CONSTANTS.DISCOUNTED_CAMPAIGN:
			price = subscriptionPrice
					- (subscriptionPrice * (campaignPrice / 100));
			break;
		default:
			price = campaignPrice;
			break;
		}
		return price;
	}

	public static String getActionSource(String shortClassName,
			StackTraceElement stackTraceElement) {
		//String className = stackTraceElement.getClassName();
		String methodName = stackTraceElement.getMethodName();
		Integer lineNumber = stackTraceElement.getLineNumber();
		// String shortClassName="";
		/*
		 * for (int i = 0; i < className.length(); i++) { char c =
		 * className.charAt(i); shortClassName += Character.isUpperCase(c) ? c :
		 * ""; }
		 */
		String shortMethodName = methodName.charAt(0) + "";
		for (int j = 1; j < methodName.length(); j++) {
			char c = methodName.charAt(j);
			shortMethodName += Character.isUpperCase(c) ? c : "";
		}
		// logger.info(className+methodName+lineNumber);
		return shortClassName + "@" + shortMethodName + "_" + lineNumber;
	}

	public static final char EXTENSION_SEPARATOR = '.';

	public static Integer convertDateForMongoLog(java.util.Date date) {
		String tempDate = new DateTime(date).toString(DateTimeFormat
				.forPattern(CONSTANTS.DATEFORMAT_MONGO_LOG));
		return new Integer(tempDate);
	}

	/*public static java.util.Date parseApptusDateString(String date_str){
		java.util.Date	date = null;
		try{
			date = apptus_date_formater.parse(date_str);
		}catch(Exception e){
			e.printStackTrace();
		}
		return date;
	}	*/
	
	
	public static Double getAsDouble(String value){
		try{
			if( StringUtils.isNotBlank( value) && NumberUtils.isNumber(value) )
				return Double.valueOf( value );
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer getAsInteger(String value){
		try{
			if( StringUtils.isNotBlank( value) && NumberUtils.isNumber(value) )
				return Integer.valueOf( value );
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static Relations getSeriesInfo(ProductData pd)
	{
		List<Relations> relations = pd.getRelations();
		Relations seriesInfo = null;
		for (Relations relation : relations)
		{
			if (relation.getRelationtype() == 6)
			{
				seriesInfo = relation;
				break;
			}
		}
		return seriesInfo;
	}

	public static Integer getRelatedProductid(ProductData pd)
	{
		List<Relations> relations = pd.getRelations();
		for (Relations relation : relations)
		{
			if (relation.getRelationtype() == 2 || relation.getRelationtype() == 3)
			{
				return relation.getCbookid();
			}
		}
		return null;
	}
	
	public static List<String> getContributors(ProductData pd, boolean asec, String type)
	{
		List<String> results = new ArrayList<String>();
		List<Contributors> contributors = pd.getContributors();
		for (Contributors contributors2 : contributors)
		{
			if (type.equalsIgnoreCase(contributors2.getRole()))
				results.add(contributors2.getFirstname() + " " + contributors2.getLastname());
		}
		return results;
	}
	
	public static String getStripStartAndEndOfString(String str){
		if( StringUtils.isBlank( str ) )
			return "";
		else{
			str	=	StringUtils.stripStart(str, " ");
			str	=	StringUtils.stripEnd(str, " ");
		}
		return str;	
	}
	
	public static Double getAPIVersion(String url){
		Double version 	= null;
		Matcher matcher = CONSTANTS.API_PATTERN.matcher(url);
		if (matcher.find() && matcher.groupCount()>1) {
			String verStr = matcher.group(3);
		//	logger.info("matcher >> groups :: {}", matcher.toString());
		//	logger.info("matcher >> groups :: {}", matcher.group());
			if(verStr!=null)
				version = Double.parseDouble(verStr);
		}
		 
		return version;
	}
	public static String getAPIBasePath(String url){
		String  basePath 	= null;
		Matcher matcher = CONSTANTS.API_PATTERN.matcher(url);
		if (matcher.find() && matcher.groupCount()>1) {
			basePath = matcher.group();
		}
		 
		return basePath;
	}
	
	public static void setDDTraceIds(){
					try{
						MDC.put("ddTraceID", "ddTraceID:" + String.valueOf(CorrelationIdentifier.getTraceId()));
					    MDC.put("ddSpanID", "ddSpanID:" + String.valueOf(CorrelationIdentifier.getSpanId()));		    
					}catch(Exception e){
						logger.info("Error occured while setting DD trace id's --->");
					}
				}
}

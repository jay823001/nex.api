package se.frescano.nextory.util;

import java.util.Comparator;

import se.frescano.nextory.model.EbCategories;

public class CustomComparator2 implements Comparator<EbCategories> {
    @Override
    public int compare(EbCategories o1, EbCategories o2) {
        return Integer.valueOf(o1.getPosition()).compareTo(Integer.valueOf(o2.getPosition()));
    }
}
package se.frescano.nextory.util;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.app.response.MinifiedProdInfoVO;
import se.frescano.nextory.model.ContributorSlim;
 
public class URLBuilder{
    
	
	public static String suggestProdUrl(ProductInformation productInfo){
		return buildUrl(productInfo);
	}
	
	public static String buildUrl(HttpServletRequest request ,ProductInformation productInfo){
		
		return buildUrl(request.getContextPath(),productInfo);
	}
	public static String buildUrl(String contextPath,ProductInformation productInfo){
		return contextPath+buildUrl(productInfo);
	}
	
	public static String buildUrl(ProductInformation productInfo){
		int i=0;
		StringBuilder authorbuild = new StringBuilder();
		if(productInfo.getAuthors()!=null && productInfo.getAuthors().size()>0){
			Set<String> authors = productInfo.getAuthors();
			if(authors!=null){
				for (Iterator<String> iterator = authors.iterator(); iterator.hasNext();) {
					String author = (String) iterator.next();
					if(i<3) {
						authorbuild.append( i > 0 ? " " : "" ).append(author);
						i++;
					}else
						break ;
				}
			}
			
		}
		//return Book2GoUtil.buildUrl(productInfo.getProductid(),productInfo.getProductFormat().getFormatNo(),authorbuild.toString(),productInfo.getTitle());
		if(productInfo.getTicket() == null)
		return Book2GoUtil.buildUrl(productInfo.getProductid(),productInfo.getProductFormat(), authorbuild.toString(),productInfo.getIsbn(),productInfo.getTitle(),null);
		else
			return Book2GoUtil.buildUrl(productInfo.getProductid(),productInfo.getProductFormat(), authorbuild.toString(),productInfo.getIsbn(),productInfo.getTitle(),null,
					productInfo.getTicket());	
		//public static String buildUrl(Integer productid,ProductFormatEnum producttype, String author,String isbn,String prodtitle,String magTitle){
		//buildUrl(productid, ProductFormatEnum.getFormat(format), author, null, title, null);
	}
	
	    
	/*public static String buildUrl(String contextPath,PartialProductInfo productInfo){
		return contextPath+buildUrl(productInfo);
	}
	public static String buildUrl(PartialProductInfo productInfo){
		return buildUrl(productInfo.getProductid(),
				productInfo.getProducttype(),productInfo.getAuthor(),
				productInfo.getIsbn(),
				productInfo.getTitle(),productInfo.getPaper_name()+productInfo.getMag_display_name(),
				productInfo.getAuthors());
	}*/
		
    /*private static String buildUrl(Integer productid,
			ProductFormatEnum producttype, String author,String isbn,String prodtitle ,
			String magTitle ,List<ContributorSlim> authorlist ){    	
    	String authorname = getAuthor(authorlist, author);    	
    	return Book2GoUtil.buildUrl(productid, producttype, authorname, isbn, prodtitle, magTitle);
    }*/
    
    public static String buildNextoryUrl(Integer productid,
			ProductFormatEnum producttype,List<ContributorSlim> authorlist, String isbn,String prodtitle  ){    	
    	String authorname = getAuthor(authorlist, null);    	
    	return Book2GoUtil.buildUrl(productid, producttype, authorname, isbn, prodtitle, null);
    }
    
   /* private static String getFinalUrl(String customurl){
    	//return customurl.toLowerCase().replaceAll("[^\\-\\da-zA-Z\\/]|(?<!,)\\s", "");
    	return customurl.toLowerCase().replaceAll("[^\\-\\da-zA-Z0-9\\/]","");
    }*/
    
    private static String getAuthor(List<ContributorSlim> authors,String author){
    	int i=0;
    	StringBuilder authorbuild = new StringBuilder();
    	if(authors != null && authors.size()>0){
    		
    		for (Iterator<ContributorSlim> iterator = authors.iterator(); iterator.hasNext();) {
				if(i < 3){
					ContributorSlim contributorSlim = iterator.next();
					authorbuild.append( i > 0 ? " " : "" ).append(contributorSlim.getFirstname()).append(" ").append(contributorSlim.getLastname());
					i++;
				}else
					break ;
			}
    		return authorbuild.toString();
    	}else 
    		return author;
    }
	
	
	public  static String categoryUrlBuild(String url,Integer categoryId , String type, String q){
		StringBuilder buildCatUrl = new StringBuilder();
		buildCatUrl.append(url).append("?");//.append("&q=").append(q);
		buildCatUrl.append("cat=").append(categoryId);
		buildCatUrl.append("&type=").append(type);
		return buildCatUrl.toString();
	}
	
	public static String buildUrl(MinifiedProdInfoVO productInfo){
		int i=0;
		StringBuilder authorbuild = new StringBuilder();
		if(productInfo.getAuthor() != null && productInfo.getAuthor().size()>0){
			List<String> authors = productInfo.getAuthor();
			for (Iterator<String> iterator = authors.iterator(); iterator.hasNext();) {
				String author = (String) iterator.next();
				if(i<3) {
					authorbuild.append( i > 0 ? " " : "" ).append(author);
					i++;
				}else
					break ;
			}
			
		}
		//return Book2GoUtil.buildUrl(productInfo.getIsbn(),productInfo.getFormattype(),authorbuild.toString(),productInfo.getTitle());
		
		return Book2GoUtil.buildUrl(productInfo.getProductid(),ProductFormatEnum.getFormat(productInfo.getFormattype()), authorbuild.toString(),productInfo.getIsbn(),productInfo.getTitle(),null);
		
	}
   
}


package se.frescano.nextory.util;

import java.util.Comparator;

import se.frescano.nextory.app.product.response.BookGroupVOApptus;

public class CustomComparator implements Comparator<BookGroupVOApptus> {
    @Override
    public int compare(BookGroupVOApptus o1, BookGroupVOApptus o2) {
        return o1.getPosition().compareTo(o2.getPosition());
    }
}
package se.frescano.nextory.util;

import org.apache.commons.lang3.StringUtils;

public enum BookGroupViewEnum {

	SERIES("series", "Series bookgroups with pagination"),
	MAIN("main", "Load Toplist & Parent categories as bookgroup's"),
	LEVEL2("level2", "Sub-Categories from Parent categoryid"),
	RELATED_BOOK("related", "Load related book details by bookid"),
	RELATED_BOOK_2("related2", "Load related book details by bookid with authors and narrators"),
	NO_DEFINED("NO_DEFINED", "NO_DEFINED");	
	
	private String	view;
	private String	description;
	
	private BookGroupViewEnum(String view, String	description){
		this.setView(view);
		this.setDescription(description);
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}
	
	public static BookGroupViewEnum getViewName(String name){
		for( BookGroupViewEnum view : values()){
			if( StringUtils.equalsAnyIgnoreCase(name, view.getView() ) )
				return view;			
		}
		return NO_DEFINED;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

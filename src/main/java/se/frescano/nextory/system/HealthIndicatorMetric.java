package se.frescano.nextory.system;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class HealthIndicatorMetric implements HealthIndicator{

	@Autowired
	private HealthCheckService	healthservice;
	
	@Override
	public Health health() {
		int errorCode = check(); // perform some specific health check
        if (errorCode != 0) {
            return Health.down()
              .withDetail("Error Code", errorCode).build();
        }
        return Health.up().build();
	}

	public int check() {
		int status =  healthservice.checkSystemHealth();				
		if(status == HttpServletResponse.SC_OK){
			return 0;
		}
		return 1;
    }
}

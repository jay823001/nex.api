package se.frescano.nextory.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.frescano.nextory.endpoints.SampleEndPoint;

//import com.codahale.metrics.annotation.Timed;


@Controller
//@Profile("!dev")
public class HealthCheckController {

	public static final Logger _logger = LoggerFactory.getLogger(HealthCheckController.class);
	
	@Autowired
	private HealthCheckService	healthservice;
	
	@Autowired
	SampleEndPoint sampleEndPoint;
	
	
	
	@RequestMapping( value = "/howami", method = RequestMethod.GET)
	// @Timed(absolute=true, name="howami")
	public ModelAndView checkSystem(HttpServletRequest req, HttpServletResponse res){						
		if(_logger.isTraceEnabled())_logger.trace("HealthCheckController....checkSystem....Start");
		ModelAndView	mv = null;
		int status =  healthservice.checkSystemHealth();
		
		//sampleEndPoint.handleMessage("up");
		if(status == HttpServletResponse.SC_OK){
			res.setHeader("X-Custom-Header", "alive");
			res.setHeader("X-App-Header", "product");			
			if( System.getenv("MY_POD_NAME") != null )
				res.setHeader("X-Host-Header", System.getenv("MY_POD_NAME"));
		}
		
		if(_logger.isTraceEnabled())_logger.trace("HealthCheckController....checkSystem....End");
		return mv;
	}
	
	
}

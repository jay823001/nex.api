package se.frescano.nextory.system;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
//import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.stereotype.Service;

//import com.codahale.metrics.annotation.Timed;


@Service("healthservice")
public class HealthCheckService {

	public static final Logger _logger = LoggerFactory.getLogger(HealthCheckService.class);
	
	/*@Autowired
	private StartUpDAOImpl	startUpDAO;	*/
	@Autowired
	private HealthCheckDAO	healthcheckdao;
	
	@Autowired
	CacheManager cacheManager;
	//@Autowired
	//HazelcastInstance hazelcastInstance;
	
	/*@Autowired 
    DefaultListableBeanFactory beanFactory;*/

	//@Autowired
	//private CounterService counterService;
	// @Timed(name = "checkSystemHealth")
	public int checkSystemHealth() {
		if(_logger.isTraceEnabled())_logger.trace("HealthCheckService....checkSystemHealth....Start");
		int count = 0;		
		/*Mongo DataSource Check*/
		try{
			healthcheckdao.getMongoProductCount();
		}catch(Exception e){
			count++;
			_logger.error("checkSystemHealth....ERROR...." + e.getMessage() );
		}
		
		/* try hazel cast */
		try{
			cacheManager.getCacheNames();
		}catch (Exception e) {
			count ++;
			_logger.error("checkSystemHealth....ERROR...." + e.getMessage() ,e);
		}

		if(_logger.isTraceEnabled())_logger.trace("HealthCheckService....checkSystemHealth....End");
		
		if( count > 0 ){
			return ( HttpServletResponse.SC_INTERNAL_SERVER_ERROR );
			//counterService.increment("howami.failure");
		}else{
			return ( HttpServletResponse.SC_OK );
			//counterService.increment("howami.success");
		}
		
	}

}

package se.frescano.nextory.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mongodb.MongoException;
import se.frescano.nextory.dao.MongoDBUtilMorphia;

@Repository("healthcheckdao")
public class HealthCheckDAO {

	public static final Logger _logger = LoggerFactory.getLogger(HealthCheckDAO.class);
	
	@Autowired
	private MongoDBUtilMorphia 	nestMongoDBMorphiaUtil ;
		
	public void getMongoProductCount() {		
		try {
			nestMongoDBMorphiaUtil.getConnection().getMongo().getAddress();
		 } catch (MongoException e) {
		       throw e;
		 }		
		//return count;
	}
	
	/*public boolean getElasticSearchProductCount() {		
		return indexSupportDao.check();
		SearchRequest tempReq = new SearchRequest(false);
		tempReq.setQuery("Ana");				
		tempReq.setSubscriptionType(SubscriptionTypeEnum.PREMIUM);
		tempReq.setProductFormat(ProductFormatEnum.E_BOOK);		
		tempReq.setPageNumber(1);
		SearchResult searchResult = searchDAO.getProductsBasedOnKeyword(tempReq, false);
		if(_logger.isTraceEnabled())_logger.trace(": HealthCheckDAO....getElasticSearchProductCount....End");
		return searchResult;
	}*/
}

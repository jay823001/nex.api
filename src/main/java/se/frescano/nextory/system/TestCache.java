package se.frescano.nextory.system;

import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import se.frescano.nextory.model.EbCategories;

@Service
public class TestCache {

	@Cacheable(cacheNames={"nest.category.cache1"})
	public Map<Integer,EbCategories> getCategoryCache(/*String url*/String countrycode)
	{
		HashMap<Integer, EbCategories> hashMap = new HashMap<Integer,EbCategories>();
		EbCategories c = new EbCategories();
		c.setCategoryId(1);
		c.setCategoryName("testing");
		hashMap.put(1, c);
		return hashMap;
	}
}

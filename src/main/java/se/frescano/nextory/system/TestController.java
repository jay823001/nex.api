package se.frescano.nextory.system;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.cache.model.SeriesList;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.cache.service.CacheBridgeSeries;
import se.frescano.nextory.cache.service.CacheBridgeTopPanels;
import se.frescano.nextory.listener.Publisher;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.toppanellist.TopPanelList;

@RestController
@RequestMapping("/api/internal/{version:1.0}")
public class TestController {

	
	@Autowired
	ApplicationPropertiesConfig applicationpropconfig;
	@Autowired
	CacheBridgeCategory cacheBridgeCategory;
	
	@Autowired
	CacheBridgeSeries cacheBridgeSeries;
	
	@Autowired
	CacheBridgeTopPanels cacheBridgeTopPanels;
	
	@Autowired
	CacheBridgeMarket cacheBridgeMarket;
	
	@Autowired
	TestCache testCache;
	
	@Autowired
	Publisher publish;
	
	@GetMapping("/test/subscriptiontype")
	public @ResponseBody SeriesList subscriptiontype() throws Exception, RestException{
		return cacheBridgeSeries.callNestSeriesUrl(applicationpropconfig.getNestSeriesUrl(), "test","SE");
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/test/apptuspublish")
	public @ResponseBody String pusblish() throws Exception{
				
		Map map = new HashMap();
		map.put("market","Sweden");
		map.put("customerKey",68);
		map.put("sellingPrice",null);
		map.put("quantity",null);
		map.put("sessionKey","09093a7d-86d5-44f0-af08-8a705141d112");
		map.put("ticket",null);
		map.put("id","");
		map.put("productKey",544);
		map.put("variantKey","9789143505955");
		map.put("lines",null);
		map.put("token","7c26980b2949be6ceebae1b09706035266e588103818ca63e0c4c5eeb03e0a79");
		//publish_vent.publishEvent(EventBusEnum.APPTUS_NONESALES, map);
		System.out.println(" after publish ");
		return map.toString();
	}
	
	
	@GetMapping("/test/series")
	public @ResponseBody SeriesList test1() throws Exception, RestException{
		return cacheBridgeSeries.callNestSeriesUrl(applicationpropconfig.getNestSeriesUrl(), "test","SE");
	}
	@GetMapping("/test/panel")
	public @ResponseBody TopPanelList test2() throws Exception, RestException{
		return cacheBridgeTopPanels.callNestTopPanelsUrl(applicationpropconfig.getTopPanelUrl(), "", null);
	}
	@GetMapping("/test/ccache")
	public @ResponseBody  Map<Integer, EbCategories> test3(){
		return cacheBridgeCategory.getCategoryCache("SE");
	}
	@Autowired
	CacheManager cacheManager;
	@GetMapping("/test/cachelist")
	public String cache(){
		Collection<String> names = cacheManager.getCacheNames();
		Iterator<String> it = names.iterator();	
		StringBuilder sb = new StringBuilder(cacheManager.getClass().getName());
		sb.append("\r\n");
		while (it.hasNext()) {
			String str = it.next();
			//Cache cache = cacheManager.getCache(str);
			sb.append(str).append("\r\n");
			
		}
		return sb.toString();
	}
	
	@GetMapping("/remove/series")
	public @ResponseBody SeriesList test4(){
		 cacheBridgeSeries.resetAllEntries();
		 return null;
	}
	@GetMapping("/remove/panel")
	public @ResponseBody TopPanelList test5(){
		 cacheBridgeTopPanels.resetAllEntries();
		 return null;
	}
	@GetMapping("/remove/ccache")
	public @ResponseBody  Map<Integer, EbCategories> test6(){
		 cacheBridgeCategory.resetAllEntries();
		 return null;
	}
	@GetMapping("/remove/market")
	public @ResponseBody  Map<Integer, EbCategories> test7(){
		cacheBridgeMarket.resetAllEntries();
		 return null;
	}
	
}

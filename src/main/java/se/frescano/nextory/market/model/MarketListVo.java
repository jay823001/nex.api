package se.frescano.nextory.market.model;

import java.io.Serializable;

public class MarketListVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7149436499895362675L;

	private String countryId;

    private String countryName;

    private String marketName;

    private Integer market_id;

    private String locale;

    private String langLocaleId;

    private String countryCode;

    private String domain;

    private String language;

    private String currency;

    public String getCountryId ()
    {
        return countryId;
    }

    public void setCountryId (String countryId)
    {
        this.countryId = countryId;
    }

    public String getCountryName ()
    {
        return countryName;
    }

    public void setCountryName (String countryName)
    {
        this.countryName = countryName;
    }

    public String getMarketName ()
    {
        return marketName;
    }

    public void setMarketName (String marketName)
    {
        this.marketName = marketName;
    }

    public Integer getMarket_id ()
    {
        return market_id;
    }

    public void setMarket_id (Integer market_id)
    {
        this.market_id = market_id;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getLangLocaleId ()
    {
        return langLocaleId;
    }

    public void setLangLocaleId (String langLocaleId)
    {
        this.langLocaleId = langLocaleId;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getDomain ()
    {
        return domain;
    }

    public void setDomain (String domain)
    {
        this.domain = domain;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [countryId = "+countryId+", countryName = "+countryName+", marketName = "+marketName+", market_id = "+market_id+", locale = "+locale+", langLocaleId = "+langLocaleId+", countryCode = "+countryCode+", domain = "+domain+", language = "+language+", currency = "+currency+"]";
    }
	
	
	
	
	
}

package se.frescano.nextory.spring.method.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.drools.bean.SystemEvent;
import se.frescano.nextory.drools.bean.UserEvent;
import se.frescano.nextory.startup.service.SubscriptionTypeVO;
import se.frescano.nextory.util.Book2GoUtil;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements Serializable{
	
	private static final long serialVersionUID = 2213738407475260589L;
	private Integer customerid;
	private MemberTypeEnum membertype;
	private boolean active; 
	private int member_type_code;
	private int prev_member_type_code;
	
	@JsonIgnore
	private MemberTypeEnum previousMemberType;
	private SubscriptionTypeVO subscription;	
	private int next_subscriptionid;
		
	private Date 	first_loggedin;
	private long	first_loggedin_long;
	private Profile profile; 
	private Profile root_profile;
	private String userCountry;
	private UserAuthToken authToken;
	private Date 	subscribedOn;
	private long	subscribedOn_long;
	private String accountType;
	
	private String currentState;
	//private String previousState;
	private UserEvent userEvent;
	private SystemEvent systemEvent; 
	//private Integer accountId;
	//private NewsLetterStatusEnum subNewsletter;
	//private boolean isactive;
	
	//private ArrayList<Integer> publisherlist;

	//private String countrycode;

	//private Integer marketid
	
	private String countrycode;	
	private Integer[] excludePubs;
	private Warning warning;


	/*public ArrayList<Integer> getPublisherlist() {
		return publisherlist;
	}

	public void setPublisherlist(ArrayList<Integer> publisherlist) {
		this.publisherlist = publisherlist;
	}*/

	/*public Integer getMarketid() {
		return marketid;
	}

	public void setMarketid(Integer marketid) {
		this.marketid = marketid;
	}*/
	
	/*public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}*/

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	/*public String getPreviousState() {
		return previousState;
	}

	public void setPreviousState(String previousState) {
		this.previousState = previousState;
	}*/

	public UserEvent getUserEvent() {
		return userEvent;
	}

	public void setUserEvent(UserEvent userEvent) {
		this.userEvent = userEvent;
	}

	public SystemEvent getSystemEvent() {
		return systemEvent;
	}

	public void setSystemEvent(SystemEvent systemEvent) {
		this.systemEvent = systemEvent;
	}


	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(Long customerid, MemberTypeEnum memType, Long membertypecode) {
		this.customerid = customerid.intValue();
		this.membertype = memType;
		this.member_type_code = membertypecode.intValue();
	}

	/*public String getAdyenShopperRef() {
		return adyenShopperRef;
	}

	public void setAdyenShopperRef(String adyenShopperRef) {
		this.adyenShopperRef = adyenShopperRef;
	}*/

	public int getNext_subscriptionid() {
		return next_subscriptionid;
	}

	public void setNext_subscriptionid(int next_subscriptionid) {
		this.next_subscriptionid = next_subscriptionid;
	}

	/*public boolean isCancelDowngrade() {
		return cancelDowngrade;
	}

	public void setCancelDowngrade(boolean cancelDowngrade) {
		this.cancelDowngrade = cancelDowngrade;
	}*/


	public int getMember_type_code() {
		return member_type_code;
	}

	public void setMember_type_code(int member_type_code) {
		this.member_type_code = member_type_code;
		this.membertype = MemberTypeEnum.getMembertypeEnum(member_type_code);
	}

	/*public boolean isOldMember() {
		return oldMember;
	}

	public void setOldMember(boolean oldMember) {
		this.oldMember = oldMember;
	}*/
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	/*public CreditCardStatusEnum getCardExpiryStatus() {
		return this.cardExpiryStatus;
	}
	
	public void setCardExpiryStatus(CreditCardStatusEnum creditCardStatusEnum) {
		this.cardExpiryStatus = creditCardStatusEnum;
	}
	public boolean isIspasswordreset() {
		return ispasswordreset;
	}
	public void setIspasswordreset(boolean ispasswordreset) {
		this.ispasswordreset = ispasswordreset;
	}*/
	/*public Float getSubpoints() {
		return subpoints;
	}
	public void setSubpoints(Double subpoints) {
		this.subpoints = (subpoints!=null?subpoints.floatValue():0.0f);
	}*/
	
	public MemberTypeEnum getMembertype() {
		return membertype;
	}
	public void setMembertype(MemberTypeEnum membertype) {
		this.membertype = membertype;
		this.member_type_code = membertype.getMemberTypeCode();
	}
	/*public boolean isIsmember() {
		return ismember;
	}
	public void setIsmember(boolean ismember) {
		this.ismember = ismember;
	}*/
	/*public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}*/
	public Integer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	/*public boolean isMailSent() {
		return mailSent;
	}
	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}
	public void setMail_sent_date(Timestamp mail_sent_date) {
		this.mailSent = mail_sent_date!= null ? true :false;
	}*/
	
	
	/*public UserRoleEnum getUserRoleEnum() {
		return userRoleEnum;
	}
	public void setUserRoleEnum(UserRoleEnum userRoleEnum) {
		this.userRoleEnum = userRoleEnum;
	}
	
	public int getSubscriptionid() {
		return subscriptionid;
	}

	public void setSubscriptionid(int subscriptionid) {
		this.subscriptionid = subscriptionid;
	}
*/
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [customerid=");
		builder.append(customerid);
		/*builder.append(", firstname=");
		builder.append(firstname);*/
		/*builder.append(", lastname=");
		builder.append(lastname);
		builder.append(", password=");
		builder.append(password);
		builder.append(", mailId=");
		builder.append(mailId);*/
		builder.append(", membertype=");
		builder.append(membertype);
		/*builder.append(", ispasswordreset=");
		builder.append(ispasswordreset);*/
		/*builder.append(", subpoints=");
		builder.append(subpoints);*/
		builder.append(", mailSent=");
		/*builder.append(mailSent);
		builder.append(", Mobile="+mobileNo);
		builder.append(", mailsent_once = " + mailsent_once );
		builder.append(", token = " + authToken );*/
		builder.append(", profile = " + profile );
		builder.append("]");
		return builder.toString();
	}
	
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + customerid;
		/*result = prime * result
				+ ((firstname == null) ? 0 : firstname.hashCode());*/
		//result = prime * result + giftpoints;
		result = prime * result + ( ( Book2GoUtil.IsMember(membertype ) )? 1231 : 1237);
		/*result = prime * result + (ispasswordreset ? 1231 : 1237);*/
		/*result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((mailId == null) ? 0 : mailId.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());*/		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (customerid != other.customerid)
			return false;
		/*if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;*/
		/*if (giftpoints != other.giftpoints)
			return false;*/
		if (membertype == null) {
			if (other.membertype != null)
				return false;
		} else if (!membertype.equals(other.membertype))
			return false;
		/*if (ispasswordreset != other.ispasswordreset)
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (mailId == null) {
			if (other.mailId != null)
				return false;
		} else if (!mailId.equals(other.mailId))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;*/
		/*if (subpoints == null) {
			if (other.subpoints != null)
				return false;
		} else if (!subpoints.equals(other.subpoints))
			return false;*/
		return true;
	}

	/*public String getRealPassword() {
		return realPassword;
	}

	public void setRealPassword(String realPassword) {
		this.realPassword = realPassword;
	}

	public boolean isIsclearPasswordStored() {
		return isclearPasswordStored;
	}

	public void setIsclearPasswordStored(boolean isclearPasswordStored) {
		this.isclearPasswordStored = isclearPasswordStored;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	*/

	public Date getFirst_loggedin() {
		return first_loggedin;
	}

	public void setFirst_loggedin(Date first_loggedin) {
		this.first_loggedin = first_loggedin;
	}
	
	public int getPrev_member_type_code() {
		return prev_member_type_code;
	}

	public void setPrev_member_type_code(int prev_member_type_code) {
		this.prev_member_type_code = prev_member_type_code;
		if( prev_member_type_code > 0 )
			this.previousMemberType = MemberTypeEnum.getMembertypeEnum(prev_member_type_code);
	}

	public MemberTypeEnum getPreviousMemberType() {
		return previousMemberType;
	}

	public void setPreviousMemberType(MemberTypeEnum previousMemberType) {
		this.previousMemberType = previousMemberType;
		this.prev_member_type_code	= previousMemberType.getMemberTypeCode();
	}

	/*public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAptusid() {
		return aptusid;
	}

	public void setAptusid(String aptusid) {
		this.aptusid = aptusid;
	}*/

	/*public Integer getProfileid() {
		return profileid;
	}

	public void setProfileid(Integer profileid) {
		this.profileid = profileid;
	}*/

	public SubscriptionTypeVO getSubscription() {
		return subscription;
	}

	public void setSubscription(SubscriptionTypeVO subscription) {
		this.subscription = subscription;
	}

	/*public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}*/
	
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/*public String getMailsent_once() {
		return mailsent_once;
	}

	public void setMailsent_once(String mailsent_once) {
		this.mailsent_once = mailsent_once;
	}*/
	public String getUserCountry() {
		return userCountry;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
  
	public UserAuthToken getAuthToken() {
		return authToken;
	}

	public void setAuthToken(UserAuthToken authToken) {
		this.authToken = authToken;
	}

	/*public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Boolean getIsFreeTrial() {
		return isFreeTrial;
	}

	public void setIsFreeTrial(Boolean isFreeTrial) {
		this.isFreeTrial = isFreeTrial;
	}*/

	public Date getSubscribedOn() {
		return subscribedOn;
	}

	public void setSubscribedOn(Date subscribedOn) {
		this.subscribedOn = subscribedOn;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/*public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public NewsLetterStatusEnum getSubNewsletter() {
		return subNewsletter;
	}

	public void setSubNewsletter(NewsLetterStatusEnum subNewsletter) {
		this.subNewsletter = subNewsletter;
	}
	public void setSubNewsletter(String subNewsletter) {
		try{
			this.subNewsletter = NewsLetterStatusEnum.valueOf(subNewsletter);
		}finally{}
		
	}

	public boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}*/

	/*public Account getAccount() {
		Account account = null; 
		if(this.accountId!=null && this.customerid>0){
			account = new Account(new Long(this.customerid), this.accountId);
			account.setOptInApproved(this.subNewsletter==NewsLetterStatusEnum.SUBSCRIBED);
			if(!this.isactive)
				account.setState(States.SUSPENDED);
			else if(this.userEvent==UserEvent.CANCELLED)
				account.setState(States.CANCELED);
			else if(MemberStateEnum.checkIsActiveMember(this.currentState))
				account.setState(States.ACTIVE);
			else
				account.setState(States.INACTIVE);
		}
		
		return account;
	}*/

	public Integer[] getExcludePubs() {
		return excludePubs;
	}

	public void setExcludePubs(Integer[] excludePubs) {
		this.excludePubs = excludePubs;
	}

	public Profile getRoot_profile() {
		return root_profile;
	}

	public void setRoot_profile(Profile root_profile) {
		this.root_profile = root_profile;
	}
	public long getFirst_loggedin_long() {
		return first_loggedin_long;
	}

	public void setFirst_loggedin_long(long first_loggedin_long) {
		this.first_loggedin_long = first_loggedin_long;
		if( first_loggedin_long > 0 )
			this.first_loggedin = new Date( first_loggedin_long );
	}

	public long getSubscribedOn_long() {
		return subscribedOn_long;
	}

	public void setSubscribedOn_long(long subscribedOn_long) {
		this.subscribedOn_long = subscribedOn_long;
		if( subscribedOn_long > 0 )
			this.subscribedOn = new Date( subscribedOn_long );
	}

	
	
	public Warning getWarning() {
		return warning;
	}

	public void setWarning(Warning warning) {
		this.warning = warning;
	}

	//@Override
	public String toStringDeba() {
		return "User [customerid=" + customerid + ", membertype=" + membertype + ", active=" + active
				+ ", member_type_code=" + member_type_code + ", prev_member_type_code=" + prev_member_type_code
				+ ", previousMemberType=" + previousMemberType + ", subscription=" + subscription
				+ ", next_subscriptionid=" + next_subscriptionid + ", first_loggedin=" + first_loggedin
				+ ", first_loggedin_long=" + first_loggedin_long + ", profile=" + profile + ", root_profile="
				+ root_profile + ", userCountry=" + userCountry + ", authToken=" + authToken + ", subscribedOn="
				+ subscribedOn + ", subscribedOn_long=" + subscribedOn_long + ", accountType=" + accountType
				+ ", currentState=" + currentState + ", userEvent=" + userEvent + ", systemEvent=" + systemEvent
				+ ", countrycode=" + countrycode + ", excludePubs=" + Arrays.toString(excludePubs)
				+ ", getCurrentState()=" + getCurrentState() + ", getUserEvent()=" + getUserEvent()
				+ ", getSystemEvent()=" + getSystemEvent() + ", getNext_subscriptionid()=" + getNext_subscriptionid()
				+ ", getMember_type_code()=" + getMember_type_code() + ", isActive()=" + isActive()
				+ ", getMembertype()=" + getMembertype() + ", getCustomerid()=" + getCustomerid() 
				+ ", hashCode()=" + hashCode() + ", getFirst_loggedin()=" + getFirst_loggedin()
				+ ", getPrev_member_type_code()=" + getPrev_member_type_code() + ", getPreviousMemberType()="
				+ getPreviousMemberType() + ", getSubscription()=" + getSubscription() + ", getProfile()="
				+ getProfile() + ", getUserCountry()=" + getUserCountry() + ", getCountrycode()=" + getCountrycode()
				+ ", getAuthToken()=" + getAuthToken() + ", getSubscribedOn()=" + getSubscribedOn()
				+ ", getAccountType()=" + getAccountType() + ", getExcludePubs()=" + Arrays.toString(getExcludePubs())
				+ ", getRoot_profile()=" + getRoot_profile() + ", getFirst_loggedin_long()=" + getFirst_loggedin_long()
				+ ", getSubscribedOn_long()=" + getSubscribedOn_long() + "]";
	}
	
	
}

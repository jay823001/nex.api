package se.frescano.nextory.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

@Service("validationUitls")
public class ValidationUitls {
	
	public  boolean checkIntegers(String field,int maxLen){
		String field1= field.trim();
		if(!strictCheck(field1,maxLen))
			return false;
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(field1);
		//boolean fp = m.matches();
	    return m.matches();
	}
	public  boolean strictCheck(String field,int maxLength){
		if(field !=null && (field.trim().length() >0 && field.trim().length()<=maxLength))
			return true;
		else 
			return false;
	}
	
	public  boolean valueCheck(String field,int maxLength){
		if(field !=null && (field.trim().length() >0 && field.trim().length()<=maxLength))
			return true;
		else 
			return false;
	}
	
	
}

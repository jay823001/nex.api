package se.frescano.nextory.endpoints;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

@Component
public class SampleEndPoint {

	private final Counter counter;
	public SampleEndPoint(MeterRegistry registry) {
		counter = Counter.builder("search").register(registry);
	}
	public void handleMessage(String message) {
		this.counter.increment();
	}
}
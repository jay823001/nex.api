package se.frescano.nextory.endpoints;

import org.springframework.context.ApplicationEvent;

import io.micrometer.core.instrument.Tags;

public class CallState extends ApplicationEvent{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1956757934077820367L;
	private String metricName;
	final long startTime;
	private Tags tags;
	
	private final String uri;
	
	public CallState(String metricName,long startTime,String uri) {
		super(metricName);
		this.startTime = startTime;
		this.metricName  = metricName;
		this.uri = uri;
	}

	public String getMetricName() {
		return metricName;
	}

	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}

	

	public Tags getTags() {
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}

	public long getStartTime() {
		return startTime;
	}

	public String getUri() {
		return uri;
	}
	
	
}


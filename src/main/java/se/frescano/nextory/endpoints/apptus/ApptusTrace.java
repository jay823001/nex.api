package se.frescano.nextory.endpoints.apptus;

import java.time.Instant;

public class ApptusTrace {

	private final Instant timestamp = Instant.now();
	private volatile String panel;
	private volatile Long timeTaken;
	
	public ApptusTrace(String panel/*,Long timeTaken*/) {
		this.panel = panel;
		/*this.timeTaken = timeTaken;*/
	}
	
	public String getPanel() {
		return panel;
	}
	public void setPanel(String panel) {
		this.panel = panel;
	}
	public Long getTimeTaken() {
		return timeTaken;
	}
	public void setTimeTaken(Long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public Instant getTimestamp() {
		return timestamp;
	}
	
	
	
}

package se.frescano.nextory.endpoints.apptus;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

@Endpoint(id = "apptusep")
@Component
public class ApptusEndPoint {

	private int capacity = 50;
	List<ApptusTrace> traces = new ArrayList<>(capacity);
	
	@ReadOperation
	public ApptusTraceDescriptor traces() {
		return new ApptusTraceDescriptor(traces);
	}
	
	
	@WriteOperation
	public void addToTraces(ApptusTrace trace,long millis) {
		synchronized (this.traces) {
			while (this.traces.size() >= this.capacity) {
				this.traces.remove(this.capacity - 1);
			}
			//ApptusTrace trace = new ApptusTrace(panel);
			trace.setTimeTaken(millis - trace.getTimestamp().toEpochMilli());
			this.traces.add(trace);
		}
	}
	
	public static final class ApptusTraceDescriptor {

		private final List<ApptusTrace> traces;

		private ApptusTraceDescriptor(List<ApptusTrace> traces) {
			this.traces = traces;
		}

		public List<ApptusTrace> getTraces() {
			return this.traces;
		}

	}
}

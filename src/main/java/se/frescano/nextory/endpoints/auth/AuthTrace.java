package se.frescano.nextory.endpoints.auth;

import java.time.Instant;

public class AuthTrace {

	private final Instant timestamp = Instant.now();
	private volatile String resp;
	private volatile Long timeTaken;
	
	public AuthTrace(String resp/*,Long timeTaken*/) {
		this.resp = resp;
		/*this.timeTaken = timeTaken;*/
	}

	public String getResp() {
		return resp;
	}

	public void setResp(String resp) {
		this.resp = resp;
	}

	public Long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(Long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public Instant getTimestamp() {
		return timestamp;
	}
	
	
	
	
	
}

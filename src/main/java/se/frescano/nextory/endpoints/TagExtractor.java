package se.frescano.nextory.endpoints;


/*
 * 
 * The interface is used for creating the information for the Spring metrics including the information extracted once the response is received.
 * This will be passed as a parameter to the service where the extraction is required , for example AbstractRestTemplate 
 *  
 */
@FunctionalInterface
public interface TagExtractor<T> {

	CallState extract(T t,long starttime,Throwable ex);

	 
}

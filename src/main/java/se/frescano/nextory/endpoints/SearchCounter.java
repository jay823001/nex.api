package se.frescano.nextory.endpoints;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.MeterRegistry;

@Component
public class SearchCounter extends Counters{
	
	//private final Counter counter;

	public SearchCounter(MeterRegistry registry) {
		//counter = Counter.builder("search").register(registry);
		super(registry,"search");
	}
	public void handleMessage(String message) {
//		increment();
	}
}

package se.frescano.nextory.endpoints;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

public abstract class Counters {
	
	private final Counter counter;
	public Counters(MeterRegistry registry,String countername) {
		counter = Counter.builder(countername).register(registry);
	}
	
	public abstract void handleMessage(String message) ;
	
	protected void increment(){
		counter.increment();
	}
}

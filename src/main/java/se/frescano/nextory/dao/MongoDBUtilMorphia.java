package se.frescano.nextory.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.NexAPIApplication;
 
/**
 * A MongoDB and Morphia Example
 *
 */

@DependsOn("applicationpropconfig")
@Service("nestMongoDBMorphiaUtil")
//@PropertySource("classpath:properties/database-${spring.profiles.active}.properties")
public class MongoDBUtilMorphia {
	private MongoClient mongoClient;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	//@Value("${mongo.user}") 
	private  String user;
	
	//@Value("${mongo.product.database}") 
	private  String proddatabase;
	
	//@Value("${mongo.log.database}") 
	private  String logdatabase;
	
	//@Value("${mongo.password}") 
	private  String password;
	
	//@Value("${mongo.host}") 
	private  String serverAddress;
	
	//@Value("${mongo.auth.databases}") 
	private  String authDatabases;
	
	
	public static TimeZone timeZone = TimeZone.getDefault();
	
	//private static Properties prop = new Properties();
	
	private static final Map<String,Datastore> dataStoreCache = new HashMap<String,Datastore>();

	public MongoDBUtilMorphia() {
		System.setProperty("DEBUG.MONGO", "false");
	}
	
	@PostConstruct
	public void inti(){
		this.user	= applicationpropconfig.getMongodbusername();
		this.proddatabase = applicationpropconfig.getMongoproddatabase();
		this.logdatabase = applicationpropconfig.getMongologdatabase();
		this.password = applicationpropconfig.getMongodbpassword();
		this.serverAddress = applicationpropconfig.getMongoserverAddress();
		this.authDatabases = applicationpropconfig.getMongoauthDatabases();
	}
	
	public String getAuthDatabases() {
		return authDatabases;
	}

	public void setAuthDatabases(String authDatabases) {
		this.authDatabases = authDatabases;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProddatabase() {
		return proddatabase;
	}
	public void setProddatabase(String proddatabase) {
		this.proddatabase = proddatabase;
	}
	public String getLogdatabase() {
		return logdatabase;
	}
	public void setLogdatabase(String logdatabase) {
		this.logdatabase = logdatabase;
	}
	
	
	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	
	@SuppressWarnings("deprecation")
	private MongoClient instantiate(){
		String[] serverAddressList = StringUtils.split(serverAddress, ",");
		List<ServerAddress> addressList = new ArrayList<ServerAddress>();
		MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
	       //build the connection options
	       builder.retryWrites(true).maxConnectionIdleTime(1800000);//set the max wait time in (ms)
	       MongoClientOptions opts = builder.build();
		for (int i = 0; i < serverAddressList.length; i++) {
			String[] serverdetails = StringUtils.split(serverAddressList[i], ":");
			if(serverdetails.length == 2)
				addressList.add(new ServerAddress(serverdetails[0],Integer.parseInt(serverdetails[1])));
			else
				addressList.add(new ServerAddress(serverdetails[0]));
		}
		
		if(StringUtils.isNotBlank(user) && StringUtils.isNotBlank(password)){
			String mongouser = null;
			String mongopass = null;
			mongouser = NexAPIApplication.lookUp(user);
			mongopass = NexAPIApplication.lookUp(password);
			
			/*try {
				InitialContext intCntxt = new InitialContext();
				mongouser = (String)intCntxt.lookup("java:comp/env/"+user);
				mongopass =  (String)intCntxt.lookup("java:comp/env/"+password);
				
			} catch (NamingException e) {
				System.err.println("UNABLE TO GET DATABASE CREDNETIALS VIA JNDI. PLEASE CHECK IF THE JNDI IS SET IN SERVER.XML. "
				 		+ "ADD BELOW TAGS WITHIN <GlobalNamingResources></GlobalNamingResources> IN SERVER.XML ");
				 System.out.println(" <Environment name=\"mongouser\" value=\"nextory\"");
				 System.out.println("type=\"java.lang.String\" override=\"false\"/>");
				 System.out.println("<Environment name=\"mongopass\" value=\"*****\"");
				 System.out.println(" type=\"java.lang.String\" override=\"false\"/> ");
				//e.printStackTrace();
				 System.exit(0);
			}*/
			
			/*final String[] dbtoAuth = StringUtils.split(authDatabases, ",");
    		final List<MongoCredential> mongoCred = new ArrayList<MongoCredential>();
    		for (int i = 0; i < dbtoAuth.length; i++) {
    			mongoCred.add(MongoCredential.createCredential(mongouser, dbtoAuth[i], mongopass.toCharArray()));
			}*/
    		final MongoCredential mongoCred1 = MongoCredential.createCredential(mongouser, "admin", mongopass.toCharArray());
    		mongoClient = new MongoClient(addressList,mongoCred1,opts);
    	}else{
    		mongoClient = new MongoClient(addressList);
    	}
		//mongoClient.setWriteConcern(WriteConcern.SAFE);
		return mongoClient;
	}
	public MongoClient getInstance() {
       if(mongoClient != null){
        	return mongoClient;
        }
		return instantiate();
    }
	
	public  MongoCollection<Document> getMongoCollection(String db,String collectionName) {
		return getInstance().getDatabase(db).getCollection(collectionName);
	}
	
	public  MongoCollection<Document> getMongoCollection(String collectionName) {
		return getInstance().getDatabase(proddatabase).getCollection(collectionName);
	}
	
	public  MongoCollection<Document> getMongoCollectionForNex(String collectionName) {
		return getInstance().getDatabase(proddatabase).getCollection(collectionName);
	}
	
	public  MongoCollection<Document> getMongoCollectionNlob(String collectionName) {
		return getInstance().getDatabase(logdatabase).getCollection(collectionName);
	}
	
	public  org.mongodb.morphia.Datastore getConnection() {
		return getMorphiaDatastore(proddatabase);
	}
	// To be completed
	public AggregateIterable<Document> aggregate(NestAggregationPipeLine pipeline){
		/*try {*/
			//mongoClient = getInstance();
			MongoDatabase mongoDatabase =  getInstance().getDatabase(proddatabase);
			return pipeline.aggregate(mongoDatabase);
		/*} catch (UnknownHostException e) {
			throw new MongoNotAvailableException(e);
		}*/
	}
	
	public  MongoDatabase getMongoConnection(String alternateDB) {
			return mongoClient.getDatabase(alternateDB);
	}
	
	
	public  MongoDatabase getLogMongoConnection() {
		return getInstance().getDatabase(logdatabase);
	}
	
	/*public  MongoCollection<Document> getMongoCollection(String collection) {
		return getMongoConnection(database).getCollection(database);
	}*/
	public  Datastore getMorphiaDatastoreForProduct() {
		return getMorphiaDatastore(proddatabase);
	}
	@SuppressWarnings("deprecation")
	private  Datastore getMorphiaDatastore(String schema) {
		if(dataStoreCache.containsKey(schema))
			return dataStoreCache.get(schema);		
		mongoClient = getInstance();
		mongoClient.setWriteConcern(WriteConcern.FSYNCED);
		Morphia morphia1 = new Morphia();
		morphia1.getMapper().getOptions().setStoreEmpties(true);
		morphia1.getMapper().getOptions().setStoreNulls(true);
		Datastore db = morphia1.createDatastore(mongoClient, schema);
		db.ensureIndexes();
		dataStoreCache.put(schema, db);
		return db;
	}
	
/*	public  Datastore getCollection(String collectionName) throws UnknownHostException
	{
		//MongoCollection<Document> collection =null;				
		Datastore
		return ;		
	}*/
	 
	    /*public static void main( String[] args ) throws UnknownHostException, MongoException {
	 
	     
	    }	*/
	/*@Bean
	 public MongoOperations mongoNxDB() throws Exception { 
		// System.out.println("Creating bean mongoTemplate "+proddatabase);
		    return new MongoTemplate(new SimpleMongoDbFactory(mongoClient, proddatabase));
	 } 
	 
	 @Bean
	 public MongoOperations mongoNlobDB() throws Exception { 
		// System.out.println("Creating bean mongoNlobTemplate "+logdatabase);
		 MongoDbFactory  mongoDbFactory = new SimpleMongoDbFactory(mongoClient, logdatabase);
		//remove _class
		 DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
		MappingMongoConverter converter =	new org.springframework.data.mongodb.core.convert.MappingMongoConverter(dbRefResolver, new MongoMappingContext());
		converter.setTypeMapper(new DefaultMongoTypeMapper(null));
		return new MongoTemplate(mongoDbFactory,converter);
		 
		 //MongoMappingContext mappingContext = new MongoMappingContext();
		 //MongoConverter converter = new MappingMongoConverter(dbRefResolver, mappingContext);
		 //return null;
		 
	 } */
	
	 @Override
	public String toString() {
		StringBuilder	sb = new StringBuilder();
		sb.append("MongoDB configurations [");		
		if( user != null )
			sb.append( "user = " ).append( user ).append(",");
		if( proddatabase != null )
			sb.append(" proddatabase = ").append( proddatabase ).append(",");
		if( logdatabase != null )
			sb.append("logdatabase = ").append( logdatabase ).append(",");
		if( password != null )
			sb.append(" password =").append( password).append(",");
		if( serverAddress != null )
			sb.append( "serverAddress = ").append( serverAddress ).append(",");
		if( authDatabases != null )
			sb.append( "authDatabases = ").append( authDatabases );
		sb.append("]");
		return sb.toString();
	}
}
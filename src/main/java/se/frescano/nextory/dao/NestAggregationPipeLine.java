package se.frescano.nextory.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

public class NestAggregationPipeLine {

	private final List<Document> stages = new ArrayList<Document>();
    
	
	Document out = null;
	String collectionname = null;
	Logger logger = LoggerFactory.getLogger(NestAggregationPipeLine.class);
	public NestAggregationPipeLine(String collectionname) {
		this.collectionname = collectionname;
	}
	
	/*
	public <U> Iterator<U> aggregate(Class<U> target) {
		
		return null;
	}

	
	public <U> Iterator<U> aggregate(Class<U> target, AggregationOptions options) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <U> Iterator<U> aggregate(Class<U> target,
			AggregationOptions options, ReadPreference readPreference) {
		// TODO Auto-generated method stub
		return null;
	}
*/
/*	
	public <U> Iterator<U> aggregate(String collectionName, Class<U> target,
			AggregationOptions options, ReadPreference readPreference) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public NestAggregationPipeLine geoNear(GeoNear geoNear) {
		// TODO Auto-generated method stub
		return null;
	}
*/
	protected AggregateIterable<Document> aggregate(MongoDatabase database){
		if(this.out!=null)
			this.stages.add(out);
		if(logger.isTraceEnabled()){
			String aggregateString = stages.stream().sequential().map(document->document.toJson()).collect(Collectors.joining(",")); 
			logger.trace( ": Aggregation pipe line " + "["+aggregateString+"]");
			
		}
		
		MongoCollection<Document> collection = database.getCollection(collectionname);
		return collection.aggregate(stages).allowDiskUse(true);		
	}
	public NestAggregationPipeLine group(Document groupedDoc) {
		Document document = new Document("$group",groupedDoc);
		stages.add(document);
		return this;
	}
	public NestAggregationPipeLine out(String outputcollection) {
		this.out = new Document("$out", outputcollection);
		return this;
	}
		
	public NestAggregationPipeLine limit(int count) {
		stages.add(new Document("$limit", count));
		return this;
	}

	// this should not be added to the list as this should be the last element in the pipeline array
	public NestAggregationPipeLine lookup(String from, String localField,String foreignField, String as) {
		Document lookupDefinition = Document.parse("{from:'"+from+"',localField:'"+localField+"',foreignField:'"+foreignField+"',as:'"+as+"'}");
		stages.add(new Document("$lookup",lookupDefinition));		
		return this;
	}

	
	public NestAggregationPipeLine match(Document matchdocument) {
		Document document = new Document("$match",matchdocument);
		stages.add(document);
		return this;
	}
		
	public NestAggregationPipeLine project(String... field) {
		Document document = new Document();
		Bson projections = Projections.include(field);
		document.append("$project", projections);
		stages.add(document);
		return null;
	}
	public NestAggregationPipeLine project(String newField, Bson expression,String... field) {
		Document document = new Document();
		Bson projections = Projections.include(field);
		Bson filterProjection = Projections.elemMatch(newField, expression);
		Bson projects = Projections.fields(projections,filterProjection);
		document.append("$project", projects);
		stages.add(document);
		return null;
	}
	public NestAggregationPipeLine projectStr(String projStr) {
		Document project = Document.parse(projStr);
		stages.add(project);
		return null;
	}

	
	public NestAggregationPipeLine skip(int count) {
		stages.add(new Document("$skip", count));
		return this;
	}
	public NestAggregationPipeLine sortDesc(Bson sort) {
		Document sortAs = new Document("$sort",sort);
		stages.add(sortAs);
		return this;
	}	
	
	public NestAggregationPipeLine sortDesc(String... fieldNames) {
		Document sort = new Document("$sort",Sorts.descending(fieldNames));
		stages.add(sort);
		return this;
	}
	
	public NestAggregationPipeLine sortAsc(String... fieldNames) {
		Document sort = new Document("$sort",Sorts.ascending(fieldNames));
		stages.add(sort);
		return this;
	}

	
	public NestAggregationPipeLine unwind(String field) {
		Document unwind = new Document("$unwind","$"+field);
		stages.add(unwind);
		return this;
	}

	public Bson getSortAsc(String fieldNames) {		
		return Sorts.ascending(fieldNames);
	}
	
	public Bson getSortDesc(String fieldNames) {		
		return Sorts.descending(fieldNames);
	}
	
	public NestAggregationPipeLine unwind(String field, Boolean preserveNullandEmptyArrays) {
		Document unwind = new Document("$unwind",new Document("path","$"+field).append("preserveNullAndEmptyArrays", preserveNullandEmptyArrays));
		stages.add(unwind);
		return this;
	}
	
	public NestAggregationPipeLine addFields(String newField, Document fieldValues) {
		Document addFields = new Document("$addFields",new Document("newField",fieldValues));
		stages.add(addFields);
		return this;
	}
	 
	public BasicDBObject filter(String input, String as, Bson condition) {
		BasicDBObject filter = new BasicDBObject("$filter",new BasicDBObject("input",input).append("as", as).append("cond", condition));
		return filter;
	}

	@Override
	public String toString() {
		return "NestAggregationPipeLine [stages=" + stages + ", out=" + out + ", collectionname=" + collectionname
				+ "]";
	}
}

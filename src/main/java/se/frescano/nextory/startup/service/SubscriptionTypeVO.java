package se.frescano.nextory.startup.service;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriptionTypeVO implements Comparable<SubscriptionTypeVO>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5956983061408439071L;
	
	@JsonProperty("subid")
	private int subId;
	
	@JsonProperty("subname")
	private String subName;
	
	@JsonProperty("subprice")
	private float subPrice;
	
	@JsonProperty("sublimit")
	private float subLimit;
	
	@JsonProperty("status")
	private boolean status;
	
	@JsonProperty("subscriptiontype")
	private String type;
	
	@JsonProperty("ranking")
	private Short rank;
	
	@JsonProperty("isfamilysubscription")
	private Boolean isFamilySubscription;
	
	@JsonProperty("maxchildprofiles")
	private Integer maxChildProfiles;
	
	@JsonProperty("maxrootlogin")
	private Integer maxRootLogin;
	
	@JsonProperty("maxchildlogin")
	private Integer maxChildLogin;
	
	@JsonProperty("maxconsumers")
	private Integer	maxconsumers;
		
	private float	offerprice;
	
	/*@JsonProperty("marketid")
	private Integer	marketid;*/
	
	@JsonProperty("countrycode")
	private String	countrycode;
	
	public SubscriptionTypeVO(){
		super();
	}
	/*public SubscriptionTypeVO(int subId, String subName, float subLimit, float subPrice,boolean status) {
		super();
		this.subId = subId;
		this.subName = subName;
		this.subPrice = subPrice;
		this.subLimit = subLimit;
		this.status = status;  
	}*/
	
	public SubscriptionTypeVO(String type, int subId, String subName, float subLimit, float subPrice,boolean status, short rank) {
		super();
		this.type = type;
		this.subId = subId;
		this.subName = subName;
		this.subPrice = subPrice;
		this.subLimit = subLimit;
		this.status = status;
		this.rank =  rank;
		
	}

	public SubscriptionTypeVO(String type, int subId, String subName, float subLimit, 
			float subPrice,boolean status, Boolean isFamilySubscription, Integer maxChildProfiles, short rank) {
		super();
		this.type = type;
		this.subId = subId;
		this.subName = subName;
		this.subPrice = subPrice;
		this.subLimit = subLimit;
		this.status = status; 
		this.rank = rank;
		this.isFamilySubscription = isFamilySubscription;
		this.maxChildProfiles = maxChildProfiles;
	}


	public void setSubId(int subId) {
		this.subId = subId;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public void setSubPrice(float subPrice) {
		this.subPrice = subPrice;
	}

	public void setSubLimit(float subLimit) {
		this.subLimit = subLimit;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	 
	public int getSubId() {
		return subId;
	}



	public String getSubName() {
		return subName;
	}



	public float getSubPrice() {
		return subPrice;
	}



	public float getSubLimit() {
		return subLimit;
	}



	public boolean isStatus() {
		return status;
	}
	@Override
	public int compareTo(SubscriptionTypeVO o) {
		int comapredRank = o.getRank();
		return this.rank - comapredRank;
	}

	public Short getRank() {
		return rank;
	}

	public void setRank(Short rank) {
		this.rank = rank;
	}
		
	public Boolean getIsFamilySubscription() {
		return isFamilySubscription;
	}

	public void setIsFamilySubscription(Boolean isFamilySubscription) {
		this.isFamilySubscription = isFamilySubscription;
	}

	 

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getMaxChildProfiles() {
		return maxChildProfiles;
	}

	public void setMaxChildProfiles(Integer maxChildProfiles) {
		this.maxChildProfiles = maxChildProfiles;
	}

	public Integer getMaxRootLogin() {
		return maxRootLogin;
	}

	public void setMaxRootLogin(Integer maxRootLogin) {
		this.maxRootLogin = maxRootLogin;
	}

	public Integer getMaxChildLogin() {
		return maxChildLogin;
	}

	public void setMaxChildLogin(Integer maxChildLogin) {
		this.maxChildLogin = maxChildLogin;
	}

	public Integer getMaxconsumers() {
		return maxconsumers;
	}

	public void setMaxconsumers(Integer maxconsumers) {
		this.maxconsumers = maxconsumers;
	}
	
	

	public float getOfferprice() {
		return offerprice;
	}

	public void setOfferprice(float offerprice) {
		this.offerprice = offerprice;
	}

	@Override
	public String toString() {
		return "SubscriptionTypeVO [subId=" + subId + ", subName=" + subName + ", subPrice=" + subPrice + ", subLimit="
				+ subLimit + ", status=" + status + ", type=" + type + ", rank=" + rank + ", isFamilySubscription="
				+ isFamilySubscription + ", maxChildProfiles=" + maxChildProfiles + ", maxRootLogin=" + maxRootLogin
				+ ", maxChildLogin=" + maxChildLogin + ", maxconsumers=" + maxconsumers + "]";
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	/*public Integer getMarketid() {
		return marketid;
	}

	public void setMarketid(Integer marketid) {
		this.marketid = marketid;
	}*/

	 
		
}

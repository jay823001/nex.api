package se.frescano.nextory.startup.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.NestConfiguration;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.nest.services.response.NestSubscriptionTypes;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.dao.StartUpDAO;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.web.api.model.CampaignTypeInfo;


@Component
public class StartUpBean /*extends AbstractRestClient */implements ServletContextAware {

	@Autowired
	private StartUpDAO startUpDAO;
	
	@Autowired
	private NestConfiguration	nestConfiguration;

	@Autowired
	private	AbstractRestTempleteClient	abstractresttemplate;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	ServletContext servletContext;

	private static String	PHP_IMAGE_RESIZE_URL = "";
	private static String	SERVER_URL			 = "";
	
	private static final Logger logger = LoggerFactory
			.getLogger(StartUpBean.class);

	public static int[] preferredFormats = new int[1];
	
	
	
	@PostConstruct
	public void initIt() throws Exception {
		
		loadSubscriptionBookAgeLimits( nestConfiguration.getSubscriptiontypes() );	
		initExcludePub();
		logger.info(" -   - - -- - - -After Validating properties  : ");
		if(servletContext!=null)
		loadTotalBookCount();
		SERVER_URL			 = applicationpropconfig.getSERVER_URL();
		PHP_IMAGE_RESIZE_URL = applicationpropconfig.getPHP_IMAGE_RESIZE_URL();		
		
		//load anaonymous user object required for WEB API calls
		initDefaultUser();
	}
	

	private void loadTotalBookCount() {
		Map<ProductFormatEnum, Integer> e2goProdMap = startUpDAO.getE2goProductCount();
		Integer ebokCount = e2goProdMap.get(ProductFormatEnum.E_BOOK);
		Integer audiobokCount = e2goProdMap.get(ProductFormatEnum.AUDIO_BOOK);
		ebokCount = (ebokCount != null ? ebokCount.intValue() : 0);
		audiobokCount = (audiobokCount != null ? audiobokCount.intValue() : 0);
		int totalbookCount = (int)round((double)(ebokCount.intValue() + audiobokCount.intValue()),-3,BigDecimal.ROUND_HALF_DOWN);
		if(ebokCount + audiobokCount < 1000)
			totalbookCount = 1000;
		servletContext.setAttribute(CONSTANTS.TOTAL_BOOKCOUNT,
				Book2GoUtil.getFormattedBookCount(totalbookCount));
		InMemoryCache.totalBookCount = Book2GoUtil
				.getFormattedBookCount(totalbookCount);

		ebokCount = (ebokCount % 100 <= 50) ? ebokCount - (ebokCount % 100)
				: ebokCount + (100 - (ebokCount % 100));

		audiobokCount = (audiobokCount % 100 <= 50) ? audiobokCount
				- (audiobokCount % 100) : audiobokCount
				+ (100 - (audiobokCount % 100));
		InMemoryCache.eBookCount = Book2GoUtil.getFormattedBookCount(ebokCount);
		InMemoryCache.audioBookCount = Book2GoUtil
				.getFormattedBookCount(audiobokCount);
	}

	public static double round(double x, int scale, int roundingMethod) {
        try {
            return (new BigDecimal
                   (Double.toString(x))
                   .setScale(scale, roundingMethod))
                   .doubleValue();
        } catch (NumberFormatException ex) {
            if (Double.isInfinite(x)) {
                return x;          
            } else {
                return Double.NaN;
            }
        }
    }
	
	@PreDestroy
	public void cleanUp() throws Exception {
		if(logger.isTraceEnabled())logger.trace( ": Spring Container is destroy! Customer clean up");
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	private void loadSubscriptionBookAgeLimits(String url) throws Exception{
		try{			
			if( url == null )
				throw new Exception("Subscription Age limit API service is not defined..... in the url "+url);
			NestSubscriptionTypes subscriptions = abstractresttemplate.exchange(url, HttpMethod.GET, null, null, null, NestSubscriptionTypes.class);
			if( subscriptions != null && subscriptions.getSubscriptionTypeList() != null ){											
				for( int i = 0; i < subscriptions.getSubscriptionTypeList().size(); i++ ){				
					InMemoryCache.subAgeLimits.put(subscriptions.getSubscriptionTypeList().get(i).getSubscriptionType(),
							subscriptions.getSubscriptionTypeList().get(i).getSubscriptionSequence());					
				}				
			}else{
				logger.error(" loading Subscription age limit's from NEST System....!"+url);
				throw new Exception("ERROR while loading Subscription age limit's from NEST System....!");
			}
		} catch (Exception e) {
			logger.error(" loading Subscription age limit's from NEST System....!"+url ,e);
			throw new Exception("ERROR while loading Subscription age limit's from NEST System....!");
		}
	}

	private void initExcludePub() throws Exception{
		String nestCampTypeURL = null;
		try {			
			CampaignTypeInfo[] campTypesArr = abstractresttemplate.exchange(nestConfiguration.getCampaigntypes(), HttpMethod.GET, null, null, null, CampaignTypeInfo[].class);			
			InMemoryCache.campTypes = Arrays.asList(campTypesArr);
		} catch (Exception e) {
			logger.error("Failed to load Campaign Types from NEST :: {} ",nestCampTypeURL,e);
			throw new Exception("ERROR loading list of Campaign Types from NEST System  :: "+nestCampTypeURL );
			 
		}
	}
	
	public static String imageResize(String imageurl, Integer hegiht,
			Integer width, Integer quality) {
		return imageResize(imageurl, width, quality, true);
	}

	public static String imageResize(String imageurl, Integer width,
			Integer quality, boolean isApp) {

		String url = PHP_IMAGE_RESIZE_URL;
		StringBuilder imageUrlBuilder = new StringBuilder();		
		imageUrlBuilder.append(SERVER_URL);		
		imageUrlBuilder.append(url).append("/").append(width).append("/");
		imageUrlBuilder.append(StringUtils.isEmpty(imageurl)
				|| "NONE".equalsIgnoreCase(imageurl) ? "photo_na.jpg"
				: StringUtils.trimToEmpty(imageurl));
		return imageUrlBuilder.toString();

	}
	public static void initDefaultUser(){
		User tempUser = new User();
		tempUser.setCustomerid(-1);
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setSubId(7);
		vo.setSubName("FAMILJ");
		vo.setType("PREMIUM");
		vo.setRank((short)3);
		tempUser.setSubscription(vo);
		Profile p = new Profile();
		p.setParentid(-1);
		p.setProfileid(-1);
		p.setIsparent(true);
		p.setCategory(CONSTANTS.PROFILE_CATEGORIES.ALL.name());
		tempUser.setProfile(p);
		tempUser.setCountrycode(CONSTANTS.DEFAULT_COUNTRY);
		tempUser.setMembertype(MemberTypeEnum.VISITOR_NO_ACTION);
		InMemoryCache.defaultUser = tempUser;
	}
}

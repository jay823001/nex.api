package se.frescano.nextory.startup.dao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mongodb.client.MongoCursor;

import se.frescano.nextory.app.constants.ProductActivationStatusEnum;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.dao.MongoDBUtilMorphia;
import se.frescano.nextory.dao.NestAggregationPipeLine;

@Repository("startUpDAO")
public class StartUpDAOImpl implements StartUpDAO  {

	Logger logger = LoggerFactory.getLogger(StartUpDAOImpl.class);
	
	@Autowired
	private MongoDBUtilMorphia nestMongoDBMorphiaUtil ;
	
	
	public Map<ProductFormatEnum, Integer> getE2goProductCount() {
		Map<ProductFormatEnum, Integer> map = new HashMap<ProductFormatEnum, Integer>();
		
		NestAggregationPipeLine	pipeline = new NestAggregationPipeLine("product");
		pipeline.match(new Document("$and", Arrays.asList( new Document("booktype.book_type_id", new Document("$in", Arrays.asList( ProductFormatEnum.E_BOOK.getFormatNo(), ProductFormatEnum.AUDIO_BOOK.getFormatNo()))),
														  new Document("productstatus", ProductActivationStatusEnum.ACTIVE.getDbName()))));
		pipeline.project("_id","booktype.book_type_id");
		pipeline.group(new Document("_id", "$booktype.book_type_id").append("count", new Document("$sum", 1)));
		
		if( logger.isTraceEnabled())logger.trace("Querry for active product's count -->" + pipeline.toString());
		MongoCursor<Document> documentscount = nestMongoDBMorphiaUtil.aggregate(pipeline).iterator();
		while( documentscount.hasNext() ){
			Document	document = documentscount.next();
			Integer		format	 = ( document.get("_id") != null)?document.getInteger("_id"):0;
			if( format ==  ProductFormatEnum.E_BOOK.getFormatNo() ) 
				map.put(ProductFormatEnum.E_BOOK, ( document.get("count") != null)?document.getInteger("count"):0 ) ;
			else if( format ==  ProductFormatEnum.AUDIO_BOOK.getFormatNo() )
				map.put(ProductFormatEnum.AUDIO_BOOK, ( document.get("count") != null)?document.getInteger("count"):0 ) ;			
		}				
		return map;					
	}

 }

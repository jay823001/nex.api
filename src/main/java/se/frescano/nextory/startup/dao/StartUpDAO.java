package se.frescano.nextory.startup.dao;

import java.util.Map;

import se.frescano.nextory.app.constants.ProductFormatEnum;

public interface StartUpDAO{

	public Map<ProductFormatEnum, Integer> getE2goProductCount();
	
}

package se.frescano.nextory.product.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.joda.time.DateTime;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonGenerator;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.ContributorTypeEnum;
import se.frescano.nextory.app.constants.ProductActivationStatusEnum;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.product.request.BookDetailsRequestBean;
import se.frescano.nextory.app.product.response.ReviewAndRatingsJsonWrapper;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.ReviewAndRating;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.dao.MongoDBUtilMorphia;
import se.frescano.nextory.dao.NestAggregationPipeLine;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.model.ProductDetails;
import se.frescano.nextory.model.ProductRelatedDetails;
import se.frescano.nextory.mongo.product.model.Categories;
import se.frescano.nextory.mongo.product.model.ProductData;
import se.frescano.nextory.mongo.product.model.Ratings;
import se.frescano.nextory.mongo.product.model.Relations;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.api.exception.NoProductsAvailableException;

@Repository
public class ProductDao{
	
	@Autowired
	private MongoDBUtilMorphia mongoDBUtilMorphia;
	
	@Autowired
	CacheBridgeCategory cacheBridge1;
		
	public static final Logger _logger = LoggerFactory.getLogger(ProductDao.class);

	public ProductDetails productDetailsFromMongo(Integer bookid, String order_ref, User user, boolean isForDevice,String countrycode)
			throws NoProductsAvailableException
	{
		ArrayList<String> countrylist = new ArrayList<String>();		
		if(countrycode!=null && !countrycode.equalsIgnoreCase(""))
			countrylist.add(countrycode);

		Query<ProductData> query = mongoDBUtilMorphia.getMorphiaDatastoreForProduct().createQuery(ProductData.class);
		query.criteria("_id").equal(bookid).criteria("productstatus") .equal(ProductActivationStatusEnum.ACTIVE.toString());
		
		if(!StringUtils.isBlank(countrycode))
			query.criteria("markets").hasAllOf(countrylist);
			
		query.retrievedFields(true, "_id","isbn","title","booktype","publisheddate","description","contributors","allowedfor","publisher","language",
				"booklength","relations","imageversion","imagewidth","imageheight","subprice","ratings","aptusid","categoryids");
		ProductData pd = query.get();
		if (pd == null)
			throw new NoProductsAvailableException(" no product found with the bookid " + bookid);

		ProductDetails productDetails = new ProductDetails();
		productDetails.setProductType(ProductFormatEnum.getFormat(pd.getBooktype().getBook_type_id()));
		if (_logger.isTraceEnabled())
			_logger.trace("Product found with bookid " + bookid + " type " + productDetails.getProductType());
		if (pd.getBooktype().getBook_type_id() == ProductFormatEnum.AUDIO_BOOK.getFormatNo())
		{
			productDetails.setAudioDetails(getAudioBookProductData(pd, order_ref, user, isForDevice,null));
			setRelatedProductid(productDetails, pd, order_ref, user, isForDevice,null);
		} else
		{
			productDetails.setEbookDetails(getEBookProductData(pd, order_ref, user, isForDevice,null));
			setRelatedProductid(productDetails, pd, order_ref, user, isForDevice,null);
		}
		Relations seriesinfo = Book2GoUtil.getSeriesInfo(pd);
		if (seriesinfo != null)
		{
			if (_logger.isTraceEnabled())
				_logger.trace(": series info data found for bookid " + bookid + " related  " + seriesinfo.getIdentifierid());
			productDetails.setSequence(String.valueOf(seriesinfo.getVolume() != null ? seriesinfo.getVolume() : 0));
			productDetails.setSeriesName(seriesinfo.getIdentifierid());

		}
		List<Integer> categories = pd.getCategoryids();
		ArrayList<Integer> mainCategoriesList = new ArrayList<>();
		if(categories!=null && categories.size()>0)
		{ int ct =0;
			for(Integer cat:categories)
			{
				/*if(StringUtils.isBlank( cat.categorylevel ) )
				{
					mainCategoriesList.add(cat.getCategoryid());
					ct++;
				}
				else if(!StringUtils.isBlank( cat.categorylevel ) && StringUtils.equals(cat.categorylevel, "M") )
				{
					mainCategoriesList.add(cat.getCategoryid());
					ct++;
				}	*/	
				
				EbCategories data = cacheBridge1.getCategoryCache(user.getCountrycode()).get(cat);
				if( (data!=null && data.getParentCatId()!=null && data.getParentCatId().intValue()==0))
				{
					mainCategoriesList.add(cat);
					ct++;
				}
				
			}
			if(ct>0)
			productDetails.setMainCategoryIds(mainCategoriesList);
		}		
		return productDetails;
	}
	
	
	
	public ProductDetails productDetailsFromMongo(String isbn, String order_ref, User user, boolean isForDevice,String countrycode)
			throws NoProductsAvailableException
	{
		ArrayList<String> countrylist = new ArrayList<String>();		
		if(countrycode!=null && !countrycode.equalsIgnoreCase(""))
			countrylist.add(countrycode);

		Query<ProductData> query = mongoDBUtilMorphia.getMorphiaDatastoreForProduct().createQuery(ProductData.class);
		query.criteria("isbn").equal(isbn).criteria("productstatus") .equal(ProductActivationStatusEnum.ACTIVE.toString());
		
		if(!StringUtils.isBlank(countrycode))
			query.criteria("markets").hasAllOf(countrylist);
			
		query.retrievedFields(true, "_id","isbn","title","booktype","publisheddate","description","contributors","allowedfor","publisher","language",
				"booklength","relations","imageversion","imagewidth","imageheight","subprice","ratings","aptusid","categoryids");
		ProductData pd = query.get();
		if (pd == null)
			throw new NoProductsAvailableException(" no product found with the bookid " + isbn);

		ProductDetails productDetails = new ProductDetails();
		productDetails.setProductType(ProductFormatEnum.getFormat(pd.getBooktype().getBook_type_id()));
		if (_logger.isTraceEnabled())
			_logger.trace("Product found with isbn " + isbn + " type " + productDetails.getProductType());
		if (pd.getBooktype().getBook_type_id() == ProductFormatEnum.AUDIO_BOOK.getFormatNo())
		{
			productDetails.setAudioDetails(getAudioBookProductData(pd, order_ref, user, isForDevice,null));
			setRelatedProductid(productDetails, pd, order_ref, user, isForDevice,null);
		} else
		{
			productDetails.setEbookDetails(getEBookProductData(pd, order_ref, user, isForDevice,null));
			setRelatedProductid(productDetails, pd, order_ref, user, isForDevice,null);
		}
		Relations seriesinfo = Book2GoUtil.getSeriesInfo(pd);
		if (seriesinfo != null)
		{
			if (_logger.isTraceEnabled())
				_logger.trace(": series info data found for isbn " + isbn + " related  " + seriesinfo.getIdentifierid());
			productDetails.setSequence(String.valueOf(seriesinfo.getVolume() != null ? seriesinfo.getVolume() : 0));
			productDetails.setSeriesName(seriesinfo.getIdentifierid());

		}
		List<Integer> categories = pd.getCategoryids();
		ArrayList<Integer> mainCategoriesList = new ArrayList<>();
		if(categories!=null && categories.size()>0)
		{ int ct =0;
			for(Integer cat:categories)
			{
				/*if(StringUtils.isBlank( cat.categorylevel ) )
				{
					mainCategoriesList.add(cat.getCategoryid());
					ct++;
				}
				else if(!StringUtils.isBlank( cat.categorylevel ) && StringUtils.equals(cat.categorylevel, "M") )
				{
					mainCategoriesList.add(cat.getCategoryid());
					ct++;
				}	*/	
				
				EbCategories data = cacheBridge1.getCategoryCache(user.getCountrycode()).get(cat);
				if( (data!=null && data.getParentCatId()!=null && data.getParentCatId().intValue()==0))
				{
					mainCategoriesList.add(cat);
					ct++;
				}
				
			}
			if(ct>0)
			productDetails.setMainCategoryIds(mainCategoriesList);
		}		
		return productDetails;
	}
	
	
		
	private void setRelatedProductid(ProductDetails productDetails, ProductData pd, String order_ref, User user,
			boolean isForDevice, String countrycode)
	{
		Integer relProductid = Book2GoUtil.getRelatedProductid(pd);
		if (relProductid != null && relProductid > 0)
		{
			Query<ProductData> query = mongoDBUtilMorphia.getMorphiaDatastoreForProduct()
					.createQuery(ProductData.class);
			query.criteria("_id").equal(relProductid).criteria("productstatus")
					.equal(ProductActivationStatusEnum.ACTIVE.toString());
			query.retrievedFields(true, "_id","isbn","title","booktype","publisheddate","description","contributors","allowedfor","publisher","language",
				"booklength","relations","imageversion","imagewidth","imageheight","subprice","ratings","aptusid");
			ProductData relatedPD = query.get();
			if (relatedPD != null)
			{
				if (_logger.isTraceEnabled())
					_logger.trace(": relProductid data found for isbn " + pd.getIsbn() + " related  " + relProductid);

				if (pd.getBooktype().getBook_type_id() == ProductFormatEnum.AUDIO_BOOK.getFormatNo())
				{
					productDetails.setEbookDetails(getEBookProductData(relatedPD, order_ref, user, isForDevice,countrycode));
				} else
				{
					productDetails.setAudioDetails(getAudioBookProductData(relatedPD, order_ref, user, isForDevice,countrycode));
				}
				productDetails.setRelatedProductExists(true);
			}
		}
	}

	private ProductRelatedDetails getEBookProductData(ProductData pd, String order_ref, User user,
			boolean isForDevice, String countrycode) /* throws SQLException */
	{
		
		ProductRelatedDetails ebookDetails = null;
		ebookDetails = new ProductRelatedDetails();
		//Ratings ratings=new Ratings();
		ebookDetails.setProductid(pd.getBookid());
		ebookDetails.setTitle(pd.getTitle());
		ebookDetails.setFormattype(pd.getBooktype().getBook_type_id());
		String description = pd.getDescription()!=null?pd.getDescription().trim().replace("&lt;", "<").replace("&gt;", ">"):"";
		ebookDetails.setDiscription(description);
		ebookDetails.setLanguage(pd.getLanguage());
		ebookDetails.setPublisher(pd.getPublisher().getPublishername());
		ebookDetails.setPublisherid(pd.getPublisher().getPublisherid());
		if(pd.getPublisher().getRestrictedfor()!=null){
			ebookDetails.setRestrictedfor(pd.getPublisher().getRestrictedfor());
		}
		ebookDetails.setAptusid(pd.getAptusid());
		if(pd.getImageversion()!=null && pd.getImageversion()>0)
		{
			ebookDetails.setCoverUrl(ebookDetails.getProductid() 
					+ "_"+pd.getImageversion()
					+ ".jpg");
		}
		else
		{
			ebookDetails.setCoverUrl(ebookDetails.getProductid() + ".jpg");
		}

		DateTime pudate = new DateTime(pd.getPublisheddate());
		if (pudate.isBefore(Book2GoUtil.baseDate))
		{
			ebookDetails.setFirstpublished(Book2GoUtil.baseDate.toDate());
		} else
			ebookDetails.setFirstpublished(pd.getPublisheddate());

		//This code not used
		/*if (user != null && user.getSubscription() != null)
		{
			if(StringUtils.isNotBlank(pd.getAllowedfor()) || StringUtils.isNotBlank(pd.getSubprice()))
			{
				ebookDetails.setBookAllowedFor(Book2GoUtil.getBookedAllowedSubscriptionsNew(user.getSubscription(),
						pd.getAllowedfor(), pd.getSubprice(),user.getCountrycode()!=null?user.getCountrycode():countrycode));
				ebookDetails.setBookAllowed(StringUtils.contains(ebookDetails.getBookAllowedFor(),user.getSubscription().getSubName()));

			}
			else
				ebookDetails.setBookAllowedFor("");
		} else
		{
			if(StringUtils.isNotBlank(pd.getAllowedfor()) || StringUtils.isNotBlank(pd.getSubprice()))
				ebookDetails.setBookAllowedFor(
					Book2GoUtil.getBookedAllowedSubscriptionsNew(null, pd.getAllowedfor(), pd.getSubprice(),countrycode));
			else
				ebookDetails.setBookAllowedFor("");
		}*/
		ebookDetails.setIsbn(pd.getIsbn());
		ebookDetails.setAuthors(Book2GoUtil.getContributors(pd, true, ContributorTypeEnum.AUTHOR.getDbName()));
		ebookDetails.setNarattors(Book2GoUtil.getContributors(pd, true, ContributorTypeEnum.ILLUSTRATOR.getDbName()));
		ebookDetails.setTranslators(Book2GoUtil.getContributors(pd, true, ContributorTypeEnum.TRANSLATOR.getDbName()));
		/*if (user != null)
		{
			Integer libid = checkProductExitsInLibrary(pd.getBookid(), user);
			ebookDetails.setLibraryFlag(libid != null);
			ebookDetails.setLibraryId(libid);
		}*/
		if(pd.getRatings()!=null){			
			ebookDetails.setRatings(pd.getRatings());
		}else{
			ebookDetails.setRatings(new Ratings((double)CONSTANTS.DEFAULT_PRODUCT_RATING,CONSTANTS.DEFAULT_PRODUCT_RATING_COUNT,(double)CONSTANTS.DEFAULT_PRODUCT_RATING));
		}
		return ebookDetails;
	}

	private ProductRelatedDetails getAudioBookProductData(ProductData pd, String order_ref, User user,
			boolean isForDevice, String  countrycode)
	/* throws SQLException */ {
		ProductRelatedDetails audioBookDetails = null;
		audioBookDetails = new ProductRelatedDetails();
		audioBookDetails.setProductid(pd.getBookid());
		audioBookDetails.setTitle(pd.getTitle());
		audioBookDetails.setFormattype(pd.getBooktype().getBook_type_id());
		String description = pd.getDescription()!=null?pd.getDescription().trim().replace("&lt;", "<").replace("&gt;", ">"):"";
		audioBookDetails.setDiscription(description);
		audioBookDetails.setLanguage(pd.getLanguage());
		audioBookDetails.setPublisher(pd.getPublisher().getPublishername());
		audioBookDetails.setPublisherid( pd.getPublisher().getPublisherid());
		if(pd.getPublisher().getRestrictedfor()!=null)
		{
			audioBookDetails.setRestrictedfor(pd.getPublisher().getRestrictedfor());
		}
		audioBookDetails.setAptusid(pd.getAptusid());		
		if(pd.getImageversion()!=null && pd.getImageversion()>0)
		{
			audioBookDetails.setCoverUrl(audioBookDetails.getProductid() 
					+ "_"+pd.getImageversion()
					+ ".jpg");
		}
		else		
			audioBookDetails.setCoverUrl(audioBookDetails.getProductid() + ".jpg");
		
		audioBookDetails.setBookLengthValue(pd.getBooklength());
		audioBookDetails.setFirstpublished(pd.getPublisheddate());
		
		//This code not used
		/*if (user != null && user.getSubscription() != null)
		{
			if(StringUtils.isNotBlank(pd.getAllowedfor()) || StringUtils.isNotBlank(pd.getSubprice()))
			{
				audioBookDetails.setBookAllowedFor(Book2GoUtil.getBookedAllowedSubscriptionsNew(user.getSubscription(),
						pd.getAllowedfor(), pd.getSubprice(),user.getCountrycode()!=null?user.getCountrycode():countrycode));
				audioBookDetails.setBookAllowed(StringUtils.contains(audioBookDetails.getBookAllowedFor(),user.getSubscription().getSubName()));

			}
			else
				audioBookDetails.setBookAllowedFor("");
		} else
		{
			if(StringUtils.isNotBlank(pd.getAllowedfor()) || StringUtils.isNotBlank(pd.getSubprice()))
				audioBookDetails.setBookAllowedFor(
					Book2GoUtil.getBookedAllowedSubscriptionsNew(null, pd.getAllowedfor(), pd.getSubprice(),countrycode));
			else
				audioBookDetails.setBookAllowedFor("");
		}*/
					
		audioBookDetails.setIsbn(pd.getIsbn());
		audioBookDetails.setAuthors(Book2GoUtil.getContributors(pd, true, ContributorTypeEnum.AUTHOR.getDbName()));
		audioBookDetails.setNarattors(Book2GoUtil.getContributors(pd, true, ContributorTypeEnum.ILLUSTRATOR.getDbName()));
		audioBookDetails.setTranslators(Book2GoUtil.getContributors(pd, true, ContributorTypeEnum.TRANSLATOR.getDbName()));
		
		if(pd.getRatings()!=null)	
			audioBookDetails.setRatings(pd.getRatings());
		else
		  audioBookDetails.setRatings(new Ratings((double)CONSTANTS.DEFAULT_PRODUCT_RATING,CONSTANTS.DEFAULT_PRODUCT_RATING_COUNT,(double)CONSTANTS.DEFAULT_PRODUCT_RATING));
		/*if (user != null)
		{
			Integer libid = checkProductExitsInLibrary(pd.getBookid(), user);
			audioBookDetails.setLibraryFlag(libid != null);
			audioBookDetails.setLibraryId(libid);
		}*/
		return audioBookDetails;
	}

	/*private Integer checkProductExitsInLibrary(int bookid, User user)
	{
		Integer libid=null;
		Datastore datastore= mongoDBUtilMorphia.getMorphiaDatastoreForProduct();
		CustomerProfile profile = datastore.createQuery(CustomerProfile.class)
		.filter("customerid =", user.getCustomerid())
		.filter("profileid =", (user.getProfile() != null)?user.getProfile().getProfileid():0)
		.filter("bookid =", bookid)
		.retrievedFields(true, "libid")
		.get();
		if(profile!=null)
			libid =  profile.getLibid(); 
		return libid;
	}*/
	
	public void getImageForProductBasedOnBookId(Integer bookid,final JsonGenerator jsonGenerator, Map<String, Object> jsonMap){
		Query<ProductData> querry = null;
		Datastore ds =  mongoDBUtilMorphia.getConnection();		
		querry  = ds.createQuery(ProductData.class);		
		querry.criteria("bookid").equal(bookid);
		querry.retrievedFields(true,"coverimage", "updateddate" , "bookid");
		ProductData pd = querry.get();	
		
		if(pd != null)
		{
			jsonMap.put("coverimage", pd.getCoverimage());
			jsonMap.put("lastupdateddate", pd.getUpdateddate().toString());
			jsonMap.put("bookid",bookid);
			jsonMap.put("status","success");
			jsonMap.put("description","Data found");
		}
		else
		{
			jsonMap.put("bookid","NOT A Valid Book ID");
			jsonMap.put("status", "Failed");
			jsonMap.put("description", "No Api found for your query.");
		}
		
	}

	//Review and Ratings
		public ReviewAndRating productRatingDetailsFromMongo(Integer book_id,User user) throws NoProductsAvailableException {
			Document match = new Document("_id.bookid",book_id);		    
			Document groupedDoc = new Document("_id", "$_id.bookid");
			groupedDoc.append("Total_Reviews", new BasicDBObject("$sum", 1));
	    	groupedDoc.append("AvrageRating", new BasicDBObject("$avg", "$ratings"));
		    NestAggregationPipeLine pipeline = new NestAggregationPipeLine("ratings_reviews");
			pipeline.match(match);
			pipeline.group(groupedDoc);
			MongoCursor<Document> documentscount = null;
			ReviewAndRating rating =  new ReviewAndRating(); 
			
			try{
				/** initialize with default values*/
				rating.setBookid(book_id);
				rating.setAvgRatings(0d); 
				rating.setTotalReviews(0);
				rating.setProfileId(user.getProfile() != null ? user.getProfile().getProfileid():0);
				rating.setUserRating(0); 
				
				documentscount = mongoDBUtilMorphia.aggregate(pipeline).iterator();

				if(documentscount != null && documentscount.hasNext()) { 
					Document d = documentscount.next(); 
					rating.setAvgRatings(d.getDouble("AvrageRating"));
					rating.setTotalReviews(d.getInteger("Total_Reviews"));
					
					//get user ratings
					match.append("_id.profileid", user.getProfile() != null ? user.getProfile().getProfileid():0);
					MongoCollection<Document> findColl = mongoDBUtilMorphia.getMongoCollection("ratings_reviews");
					documentscount = findColl.find(match).iterator();
					 
					if(documentscount.hasNext()) {
						Document next = documentscount.next();
						rating.setUserRating(next.getInteger("ratings"));
					}     
				}
				rating.updateWithDefaultValues();
			} catch(Exception nPE) {

				throw new NoProductsAvailableException(" no product found with the isbn " + book_id);
			}

			if (_logger.isTraceEnabled())
				_logger.trace(": found product with isbn " + book_id + " type ");
	
			return rating;
		}

		public void setProductRating(Integer bookid, BookDetailsRequestBean requestBean, User user) throws Exception {

	    	Document doc = new Document("_id",new Document("bookid",bookid).append("profileid",(user.getProfile() != null)?user.getProfile().getProfileid():0));
	    	doc.append("ratings", requestBean.getRating());
	    	doc.append("comments", "");
	    	doc.append("c_date", new Date()); 
	    	try {
	    		MongoCollection<Document> ratingCol =  mongoDBUtilMorphia.getMongoCollection("ratings_reviews"); 
	    		ratingCol.findOneAndUpdate(Filters.eq("_id",doc.get("_id")),new Document("$set",doc), 
	    				new FindOneAndUpdateOptions().upsert(true));
	    		
	    		//update the product collection with avg ratings 
	    		Datastore ds =  mongoDBUtilMorphia.getConnection(); 
	    		Query<ProductData> querry  = ds.createQuery(ProductData.class).filter("_id =", bookid).retrievedFields(true, "ratings"); 
	    		ProductData pd = querry.get();	
	    		Integer totalRatings = 0;
	    		Double avgRatings=0d; 
	    		
	    		if(pd != null ){ 
	    			//calculate the avg rating from rating_reviews collections
    				ReviewAndRating reviewAndRatings = productRatingDetailsFromMongo(bookid, user);
    				totalRatings = reviewAndRatings.getTotalReviews();
    				avgRatings= reviewAndRatings.getAvgRatings();
    				//NEX-4004 code Changes
    				Double error = 1/(Math.sqrt(totalRatings+1));
    				Double ratingsRankings = (3*(error)) + (avgRatings*(1-error));
    				
    				Ratings ratings = new Ratings(avgRatings,totalRatings,ratingsRankings);
	    			UpdateOperations<ProductData> prodUpdateOps = ds.createUpdateOperations(ProductData.class).set("ratings", ratings);
	    			ds.update(ds.createQuery(ProductData.class).filter("_id =", bookid),prodUpdateOps);
	    		}
	    	}catch(Exception ex) {
	    		System.out.println(": error while product rating insertion with isbn " + bookid);
	    		_logger.trace(": error while product rating insertion with isbn " + bookid);
	    	}
		}
		
		
		public ReviewAndRatingsJsonWrapper getBookRateDetails(Integer book_Id,
				BookDetailsRequestBean requestBean, User appuserinfo) throws NoProductsAvailableException {

			if(_logger.isTraceEnabled())
				_logger.trace("ProductDao :: getBookDetails :: start :: bookId" + book_Id);
			ReviewAndRatingsJsonWrapper wrapper = new ReviewAndRatingsJsonWrapper();
			if(appuserinfo!=null) {
				ReviewAndRating reviewAndRatings = productRatingDetailsFromMongo(book_Id, appuserinfo);
				wrapper.setRating(reviewAndRatings);
			}
			return wrapper;
		}					
}
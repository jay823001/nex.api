package se.frescano.nextory.config;




import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


//@Configuration
	 @EnableSwagger2
	 public class SwaggerConfig {

	     @Bean
	     public Docket postsApi() {
	         return new Docket(DocumentationType.SPRING_WEB)
	                 .groupName("public-api")
	                 .apiInfo(apiInfo())
	                 .select()
	                 .apis(RequestHandlerSelectors.any())
	                 .paths(PathSelectors.regex("/api/.*"))
	                 .build();
	     }

	     /*private Predicate<String> postPaths() {
			//return new Predicate<String> ;
	    	 String a = "/adyen/ver1/payment/purchaseFreeTrialMember";
	    	 Predicate<String> p = new Predicate<String>() {
	    		 
				@Override
				public boolean apply(String input) {
					// TODO Auto-generated method stub
					return false;
				}
			};
			p.apply(a);
	    	 return p;
	         return or(
	                java.util.regex("/api/posts.*"),
	                 java.util.regex("/api/comments.*")
	         );
	     }*/

	     private ApiInfo apiInfo() {
	         return new ApiInfoBuilder()
	                 .title("Nextory Back end API")
	                 .description("Nextory API for APP and UI with out input parameters ...")
	                 //.termsOfServiceUrl("http://hantsy.blogspot.com")
	                 .contact(new Contact("nextory back end ", "https://www.nextory.se", "sathish@nextory.se"))
	                 .license("internal")
	                 .version("2.0")
	                 .build();
	     }

	 }


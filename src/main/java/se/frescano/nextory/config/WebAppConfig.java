package se.frescano.nextory.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserInfoMethodArgumentResolver;
import se.frescano.nextory.app.api.spring.method.resolver.CurrentAppHeaderMethodArgumentResolver;
import se.frescano.nextory.app.api.spring.method.resolver.CurrentAppUserTokenMethodArgumentResolver;
import se.frescano.nextory.app.intereceptor.ApiInterceptor;
import se.frescano.nextory.web.api.interceptor.WebAuthInterceptor;
import se.frescano.nextory.web.api.spring.method.resolver.CurrentWebUserMethodArgumentResolver;

@Configuration
@EnableWebMvc
public class WebAppConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

	public static final Logger logger = LoggerFactory.getLogger(WebAppConfig.class);
		
	@Autowired
	private	WebAuthInterceptor	webauthinterceptor;
	
	@Autowired
	private ApiInterceptor		apiinterceptor;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {		
		try {										
			registry.addInterceptor( webauthinterceptor ).addPathPatterns("/api/web/**");
			
			//User Auth-Token authentication interceptor for all APP URL's.
			ArrayList<Double> 	versions = applicationpropconfig.getSupportedversions();
			apiinterceptor.setSupportedVersions( versions );
			registry.addInterceptor( apiinterceptor ).addPathPatterns("/api/**").excludePathPatterns("/api/web/**","/api/internal/**","/api/ext/**","/api/staticdata/**");
									
		} catch (Exception e) {
			logger.error("An exception occured while starting application--->");
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}
	}
	
    @Override 
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/html/trustly_payment_template.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
    
    @Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add( new CurrentWebUserMethodArgumentResolver());
		argumentResolvers.add( new CurrentAppUserTokenMethodArgumentResolver());
		argumentResolvers.add( new AppUserInfoMethodArgumentResolver());		
		argumentResolvers.add( new CurrentAppHeaderMethodArgumentResolver());	
	}

    @Override
	public void configure(WebSecurity security) throws Exception {
		// TODO Auto-generated method stub
		security.ignoring().antMatchers("/**");
	}    
}
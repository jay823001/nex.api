package se.frescano.nextory.apptus.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apptus.esales.connector.CustomerKeyAuthentication;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.listener.Publisher;
import se.frescano.nextory.mongo.product.model.ProductData;
import se.frescano.nextory.notification.NotificationEnum;
import se.frescano.nextory.notification.NotificationEvent;
import se.frescano.nextory.notification.request.ApptusNotificationRequest;
import se.frescano.nextory.spring.method.model.User;

/*
 * Created by Deba
 * Reference : http://springinpractice.com/2008/11/15/aop-101-speeding-up-springs-javamailsenderimpl-with-aop
 * Any new method added to this class should start with the nomenclature of CallAptus..
 * Note: All http rest api methods which are  Asynchronous in nature qualifies to get into this class
 * */

@Service
public class ApptusNotificationApiCall{

	
	@Autowired
	private	ApplicationPropertiesConfig 			applicationpropconfig;
	
	@Autowired
	private Publisher	publisher;
	
	
	@Autowired
	private ApptusServiceHelper	apptusServiceHelper;
	
	/*@Autowired
	private ApptusNotificationService	apptusnotificationservice;	*/
	
	public static final Logger	_logger	= LoggerFactory.getLogger(ApptusNotificationApiCall.class);
	
	/*javax.ws.rs.client.Client  client  = null;
	Client getClient(){
		if(client==null){
			HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(aptusConfiguration.getUsername(), aptusConfiguration.getPassword());
			client = ClientBuilder.newClient( new ClientConfig().register(feature) );
		}
		return client;
	}	*/	
	
	public void CallAptusNotificationWeb(String esalesticket,User user, String profile){		 
		CustomerKeyAuthentication auth = new CustomerKeyAuthentication(applicationpropconfig.getApptusPrivateKey(), String.valueOf(user.getCustomerid()));
		ApptusNotificationRequest	request = new ApptusNotificationRequest("", 
				(( user != null )?String.valueOf((user.getCustomerid() + getDate()).hashCode()): "" + UUID.randomUUID() ), 
				(!StringUtils.isBlank(profile))?profile:null, 
						(user!=null&&user.getCountrycode()!=null)?apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()):
				"Sweden", auth.getToken(), null , null, esalesticket, null, null, null);		   		  
		publisher.publishEvent(new NotificationEvent(NotificationEnum.APPTUS_ESALES, request));
		/*Map<String,Object> postBody = new HashMap<String,Object>();
		   postBody.put("id", "");
		   if(user != null)
		   {
			   String session = String.valueOf((user.getCustomerid() + getDate()).hashCode());
			   postBody.put("sessionKey", session);
		   }
		   else
		   {
			   postBody.put("sessionKey",UUID.randomUUID());
		   }
		   if(!profile.equalsIgnoreCase(""))
			postBody.put("customerKey", profile);
		   
		   postBody.put("market", "Sweden");
		   postBody.put("token", auth.getToken());
		   postBody.put("ticket", esalesticket);
		   postCall(postBody, aptusConfiguration.getClickNotificationUrl());*/
	}
	
	public void CallAptusNotificationWebWithoutEsalesticket(User user,Integer aptusid, String isbn, String profile){			   
		CustomerKeyAuthentication auth = new CustomerKeyAuthentication(applicationpropconfig.getApptusPrivateKey(), String.valueOf(user.getCustomerid()));
		ApptusNotificationRequest	request = new ApptusNotificationRequest("", ( ( user != null )?String.valueOf((user.getCustomerid() + getDate()).hashCode()): "" + UUID.randomUUID() ), 
				(!StringUtils.isBlank(profile))?profile:null, 
						(user!=null&&user.getCountrycode()!=null)?apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()):
							"Sweden", auth.getToken(), "" + aptusid , isbn, null, null, null, null);		   		  
		publisher.publishEvent(new NotificationEvent(NotificationEnum.APPTUS_NONESALES,request));
		
		/*Map<String,Object> postBody = new HashMap<String,Object>();
		postBody.put("id", "");
		if(user != null)
		{
		   String session = String.valueOf((user.getCustomerid() + getDate()).hashCode());
		   postBody.put("sessionKey", session);
		}
		else
		{
		   postBody.put("sessionKey",UUID.randomUUID());
		}
		if(!profile.equalsIgnoreCase(""))
			postBody.put("customerKey", profile);
		postBody.put("market", "Sweden");
		postBody.put("token", auth.getToken());
		postBody.put("productKey", aptusid);
		postBody.put("variantKey", isbn);
		postCall(postBody, aptusConfiguration.getNonEsalesClickNotification());*/
	}
	
	
	public void CallAptusNotificationApp(String esalesticket,User user){		 
		   CustomerKeyAuthentication auth = new CustomerKeyAuthentication(applicationpropconfig.getApptusPrivateKey(), String.valueOf(user.getCustomerid()));
		   
		   ApptusNotificationRequest	request = new ApptusNotificationRequest("", ( ( user.getAuthToken() != null )?"" + user.getAuthToken().getUuid(): "" + UUID.randomUUID() ), 
					(user != null && user.getProfile()!=null && user.getProfile().getProfileid() != null)?"" + user.getProfile().getProfileid():"" + user.getCustomerid(), 
							(user!=null&&StringUtils.isNotBlank(user.getCountrycode()))?apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()):
								"Sweden", auth.getToken(), null , null , esalesticket, null, null, null);		   		  
		   publisher.publishEvent(new NotificationEvent(NotificationEnum.APPTUS_ESALES, request));

		   /* 
			apptusnotificationservice.sendNotification((EventBusEnum.APPTUS_CLICK, aptusConfiguration.getClickNotificationUrl(), request);

		     Map<String,Object> postBody = new HashMap<String,Object>();
		   postBody.put("id", "");
		   postBody.put("sessionKey", ( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): UUID.randomUUID() ));
		   if(user != null && user.getProfile() != null && user.getProfile().getProfileid() != null)
			   postBody.put("customerKey", user.getProfile().getProfileid() );
		   else if( user != null)
			   postBody.put("customerKey", user.getCustomerid());
		   postBody.put("market", "Sweden");
		   postBody.put("token", auth.getToken());
		   postBody.put("ticket", esalesticket);
		   postCall(postBody, aptusConfiguration.getClickNotificationUrl());*/
	}
	
	public void CallAptusNotificationAppWithoutEsalesticket(User user,Integer aptusid, String isbn){			   
		CustomerKeyAuthentication auth = new CustomerKeyAuthentication(applicationpropconfig.getApptusPrivateKey(), String.valueOf(user.getCustomerid()));
		ApptusNotificationRequest	request = new ApptusNotificationRequest("", ( ( user.getAuthToken() != null )?"" + user.getAuthToken().getUuid(): "" + UUID.randomUUID() ), 
					(user != null && user.getProfile()!=null && user.getProfile().getProfileid() != null)? ""+ user.getProfile().getProfileid():"" + user.getCustomerid(), 
							(user!=null&&StringUtils.isNotBlank(user.getCountrycode()))?apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()):
								"Sweden", auth.getToken(), "" + aptusid , "" + isbn , null, null, null, null);		   		  
		publisher.publishEvent(new NotificationEvent(NotificationEnum.APPTUS_NONESALES,request));
		
		//apptusnotificationservice.sendNotification( aptusConfiguration.getNonEsalesClickNotification(), request);
		
		/*Map<String,Object> postBody = new HashMap<String,Object>();
		postBody.put("id", "");
		postBody.put("sessionKey", user.getAuthToken()!=null?user.getAuthToken().getUuid():UUID.randomUUID());
		if(user != null && user.getProfile() != null &&  user.getProfile().getProfileid() != null)
			   postBody.put("customerKey", user.getProfile().getProfileid());
		else if( user != null)
			   postBody.put("customerKey", user.getCustomerid());
		postBody.put("market", "Sweden");
		postBody.put("token", auth.getToken());
		postBody.put("productKey", aptusid);
		postBody.put("variantKey", isbn);
		postCall(postBody, aptusConfiguration.getNonEsalesClickNotification());*/
	}
	
	public void CallAptusPayemntEsales(String token,User user,ProductData pd){			
		CustomerKeyAuthentication auth = new CustomerKeyAuthentication(applicationpropconfig.getApptusPrivateKey(), String.valueOf(user.getCustomerid()));
		ApptusNotificationRequest	request = new ApptusNotificationRequest("", token, 
				( ( user != null && user.getProfile() != null && user.getProfile().getProfileid() != null )?"" + user.getProfile().getProfileid(): "" + user.getCustomerid() ), 				
				(user!=null&&StringUtils.isNotBlank(user.getCountrycode()))?apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()):
					"Sweden", auth.getToken(), null , null, null, null, null, null);		   		  
		
		ArrayList<HashMap<String,Object>> list = new ArrayList<>();
		HashMap<String,Object> innerbody = new HashMap<>();
		innerbody.put("productKey", pd.getAptusid());
		innerbody.put("quantity", 1);		
		innerbody.put("sellingPrice", 0);
		list.add(innerbody);
		request.setLines(list);
		publisher.publishEvent(new NotificationEvent(NotificationEnum.APPTUS_PAYMENT,request));
		
	}	
	
	
	public static String  getDate() {
		Date date = new Date();
		return new SimpleDateFormat("MM-dd-yyyy").format(date);
    }
	
}

package se.frescano.nextory.apptus.service;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.apptus.services.ApptusRelatedBookZoneService;
import se.frescano.nextory.app.apptus.services.RecommendBasedOnCustomerService;
import se.frescano.nextory.app.apptus.services.RecommendZoneService;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.product.response.BooksForBookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.apptus.model.AptusSecond;
import se.frescano.nextory.apptus.model.AptusThird;
import se.frescano.nextory.apptus.model.ProductsAptus;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusAttributes;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse2;
import se.frescano.nextory.apptus.model.SubCategoryFetchVo.CategoryList;
import se.frescano.nextory.apptus.model.SubCategoryFetchVo.Subcategoryzone;
import se.frescano.nextory.apptus.request.GenericPanel;
import se.frescano.nextory.apptus.request.RecommendBasedOnCustomer;
import se.frescano.nextory.apptus.request.RecommendZone;
import se.frescano.nextory.apptus.request.RelatedBookZonePanel;
import se.frescano.nextory.apptus.utils.FilterUtils;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.cache.service.HazelCastDependentService;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.model.ProductRelatedDetails;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.CustomComparator2;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.util.URLBuilder;
import se.frescano.nextory.web.api.response.BookGroupResponse;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.response.UIBook;
import se.frescano.nextory.web.api.response.UIBookGroup;
import se.frescano.nextory.web.api.response.UIBookGroupFinal;
import se.frescano.nextory.web.apptus.request.BockerPanelRequest;
import se.frescano.nextory.web.apptus.request.CategoryZonePanelRequest;
import se.frescano.nextory.web.apptus.request.MainCategoryZonePanelRequest;
import se.frescano.nextory.web.apptus.request.WebGenericPanel;
import se.frescano.nextory.web.apptus.service.WebApptusPanelService;
import se.frescano.nextory.web.apptus.service.WebGenericPanleService;

@Service
public class ApptusService {
	
	@Autowired
	private WebApptusPanelService		webapptuspanelservice;		
	
	@Autowired
	private ApptusServiceHelper 		apptusServiceHelper;
	
	
	@Autowired
	private ApplicationPropertiesConfig 			applicationpropconfig;
	
	@Autowired
	private CacheBridgeCategory 		cacheBridge;
		
	@Autowired
	private HazelCastDependentService	hazelCastDependentService;
	
	@Autowired
	private WebGenericPanleService		webgenericpanelservice;
	
	@Autowired
	private ApptusRelatedBookZoneService	releatedbookzoneservice;

	@Autowired
	private ApptusServiceHelper 		apptusServicehelper;
	
	@Autowired
	private RecommendZoneService		apptusrecommendzoneservice;
	
	@Autowired
	private RecommendBasedOnCustomerService	aptusrecommendbasedoncustomer;
	
	private static final Logger logger = LoggerFactory.getLogger(ApptusService.class);
		
	//Code using
	public GenericAPIResponse callAppropriateUrl(String action, String subscription,Integer windowsize,
			String countrycode, User user)
	{		
		String panelurl ="";
		BockerPanelRequest 	panel = new BockerPanelRequest();
		if(action.equalsIgnoreCase("bocker")){
			panelurl = applicationpropconfig.getBockerUrl() + "_" + countrycode.toLowerCase();			
		}else if(action.equalsIgnoreCase("Ebocker")){	
			panelurl = applicationpropconfig.getEbockerUrl() + "_" + countrycode.toLowerCase();
		}
		else if(action.equalsIgnoreCase("Abocker")){
			panelurl = applicationpropconfig.getAbockerUrl() + "_" + countrycode.toLowerCase();
		}else{
			GenericAPIResponse		response	 = new GenericAPIResponse(); 
			return response;
		}		
		if(windowsize == null){
			panel.setWindow_first("" + 1);
			panel.setWindow_last("" + 50);			
		}else{
			panel.setWindow_first("" + 1);
			panel.setWindow_last("" + windowsize);			
		}
		
		if( !StringUtils.isBlank( countrycode ) && StringUtils.equalsIgnoreCase(countrycode, "SE") ){
			panel.setBookid_filter_new(" NOT " + FilterUtils.buildNyheterFilter(null, "0","0"));
			panel.setBookid_filter( " NOT " + FilterUtils.build_Vi_RekommenderFilter(null, "0","0"));			
		}else if( !StringUtils.isBlank( countrycode ) ){
			panel.setBookid_filter_new(" NOT " + FilterUtils.buildNyheterFilter(countrycode, "0","0"));
			panel.setBookid_filter( " NOT " + FilterUtils.build_Vi_RekommenderFilter(countrycode, "0","0"));			
		}
		Map<String , Integer> map = InMemoryCache.subAgeLimits;		
		if( !StringUtils.isBlank(subscription) )
			panel.setSubscription_filter( " " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ));
		
		if( user!=null &&  !StringUtils.isBlank( user.getUserCountry() ) ){
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry()));
			panel.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry()));
		}
		if(user !=null && user.getExcludePubs()!=null )		
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ));			
		if( !StringUtils.isBlank( countrycode ) )
			panel.setMarket_filter( countrycode );					
						
		String	cat_exclude_filter = apptusServiceHelper.getExcludeCategoryForApptusRequest(countrycode );						
		if( !StringUtils.isBlank( cat_exclude_filter ) )
			panel.setCategory_Exclude_filter(cat_exclude_filter);
		
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(countrycode) );
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey( user ));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey( user ));						
		return getBockerFromApptus(panelurl, panel,-1,action,null,-1,countrycode,user);
	}
	
	//Code using
	public GenericAPIResponse getBockerFromApptus (String panelurl, BockerPanelRequest	panel,Integer formatno, String slug,String subscription,Integer pagenumber,String countrycode, User user)
	{
		Map<String, List<ApptusFinalResponse>> pubDetails = new HashMap<String, List<ApptusFinalResponse>>(); 
		GenericAPIResponse		response	 = new GenericAPIResponse();      
		try {
			pubDetails = webapptuspanelservice.webApptusBookGroupPanelService(panelurl, panel);			
			ArrayList<UIBookGroup>	bookgrouplist 	= new ArrayList<UIBookGroup>();
		    UIBookGroupFinal		bookgroupFinal 	= new UIBookGroupFinal();
		    EbCategories categoryInfo = hazelCastDependentService._getCategory(slug,cacheBridge.getCategoryCache(countrycode));		    	  
		    for(Entry<String, List<ApptusFinalResponse>> entryObj:pubDetails.entrySet()){
		    	UIBookGroup bookgroup = new UIBookGroup();
		    	bookgroup.setBookgroupid(0);
		    	bookgroup.setSlug(entryObj.getKey());
		    	List<ApptusFinalResponse> listofData = entryObj.getValue();		    		  
		    	if(listofData.size()>0){
		    		ApptusAttributes attributes = listofData.get(0).getAttributes();
		    		bookgroup.setIsparentcategory(Boolean.valueOf(attributes.getIsparentcategory()));
			    	bookgroup.setIscategory(Boolean.valueOf(attributes.getIscategory()));
			    	bookgroup.setName(attributes.getName());
			    	if(attributes.getSorton() !=null && !attributes.getSorton().equalsIgnoreCase(""))
			    		bookgroup.setSorton(attributes.getSorton());
			    	if(attributes.getFormattype() == null)
			    		bookgroup.setFormattype(ProductFormatEnum.getFormat(formatno));
			    	else
			    		bookgroup.setFormattype(ProductFormatEnum.getFormat(attributes.getFormattype()));
		    	}
		    	ArrayList<UIBook> listofbooks = new ArrayList<>();
		    	ArrayList<ProductInformation> listofProds = apptusServiceHelper.operateOnAptusSecondSearch(listofData.get(0).getProducts());
		    	List<String> topTitles = new ArrayList<String>(); 
		    	for( ProductInformation	product	: listofProds){		 				//
		    		UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
		 												product.getTitle(),
		 												product.getIsbn(),
		 												product.getCoverUrl(),
		 												URLBuilder.buildUrl(product),
		 												new Long(product.getRelProductidApp()));
		    		book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
		 			book.setAuthors(product.getAuthors());
		 			book.setRelevance(product.getRelevance());
		 			book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
		 			listofbooks.add(book);
		 			if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT )
		 				topTitles.add( book.getIsbn() );		 			
		    	}		    	
		    	bookgroup.setBooks(listofbooks);
		    	bookgrouplist.add(bookgroup);
		    	bookgroupFinal.setToptitles((ArrayList<String>) topTitles);
		    	if(categoryInfo!=null)
		    		bookgroupFinal.setTitle(categoryInfo.getCategoryName());
			}
		    if(slug!= null && pagenumber == 0){
		    	ArrayList<UIBookGroup>	bookgrouplistnew = callAppropriateUrlForParentCategoriesNew(slug, formatno, subscription, bookgrouplist, countrycode ,user);
		    	bookgroupFinal.setBookgroups(bookgrouplistnew);
		    }else
		    	  bookgroupFinal.setBookgroups(bookgrouplist);
		    response.setStatus(HttpStatus.OK);
		    response.setData(bookgroupFinal);
		} catch (Exception e) {
			logger.error("Error while fetching bocker from apptus :::" + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	//Code using
	public GenericAPIResponse getBookGroupFromApptus (String url,BockerPanelRequest panel, Integer formatno, String action, String sorton, String countrycode)
	{
		Map<String, List<ApptusFinalResponse>> pubDetails = new HashMap<String, List<ApptusFinalResponse>>(); 
		GenericAPIResponse		response	 = new GenericAPIResponse();      
		try {
			pubDetails	= webapptuspanelservice.webApptusBookGroupPanelService(url, panel);		    			
		    BookGroupResponse	bookgroupFinal = new BookGroupResponse();		    
		    EbCategories 	ebCategories 	= hazelCastDependentService._getCategory( action,cacheBridge.getCategoryCache(countrycode) );				
			int parentCategory	= ( ebCategories!= null && ebCategories.getParentCatId() != null && ebCategories.getParentCatId() > 0 )?
										ebCategories.getParentCatId():0;										
			bookgroupFinal.setParentid(parentCategory);
			if(parentCategory >0){
				EbCategories p_ebCategories 	= cacheBridge.getCategoryCache(countrycode).get( parentCategory );
				bookgroupFinal.setParentcategoryname(p_ebCategories.getCategoryName());
			}
			if(null != pubDetails && pubDetails.size()>0){
				for(Entry<String, List<ApptusFinalResponse>> entryObj:pubDetails.entrySet()){
					bookgroupFinal.setBookgroupid(0);				    
					List<ApptusFinalResponse> listofData = entryObj.getValue();
				    if(listofData.size()>0){
				    	ApptusAttributes attributes = listofData.get(0).getAttributes();				    	
				    	bookgroupFinal.setName(attributes.getName());					    
				    	if(sorton != null && !sorton.equalsIgnoreCase(""))
				    		bookgroupFinal.setSorton(sorton);
				    	else
				    		bookgroupFinal.setSorton(attributes.getSorton());
					    if(attributes.getFormattype() == null)
					    	bookgroupFinal.setFormattype(ProductFormatEnum.getFormat(formatno));
					    else
					    	bookgroupFinal.setFormattype(ProductFormatEnum.getFormat(attributes.getFormattype()));
				    }
				    ArrayList<UIBook> listofbooks = new ArrayList<>();
				    ArrayList<ProductInformation> listofProds = apptusServiceHelper.operateOnAptusSecondSearch(listofData.get(0).getProducts());
				    List<String> topTitles = new ArrayList<String>(); 
				    for( ProductInformation	product	: listofProds){
				    	//logger.debug(" product.getRelProductid()  : "+product.getIsbn()+"/"+product.getRelProductid() );
				 		UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
				 												product.getTitle(),
				 												product.getIsbn(),
				 												product.getCoverUrl(),
				 												URLBuilder.buildUrl(product),
				 												new Long(product.getRelProductidApp()));
				 		book.setRelevance(product.getRelevance());
				 		book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
				 		book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
				 		listofbooks.add(book);
				 		if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT ){
				 			topTitles.add( book.getIsbn() );
				 		}
				    }				    
				    bookgroupFinal.setBooks(listofbooks);
				    bookgroupFinal.setToptitles((ArrayList<String>) topTitles);
				}
		    }
			else if(ebCategories != null)
				bookgroupFinal.setName(ebCategories.getCategoryName());						    
		    response.setStatus(HttpStatus.OK);
		    response.setData(bookgroupFinal);
		} catch (Exception e) {
			logger.error("Error while fetching bookgroups from apptus :: " + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
		
	public GenericAPIResponse getTopListForCategoriesViewAll (String url,WebGenericPanel panel, Integer formatno, String action, String sorton, String subcatslugname, String countrycode)
	{
		Map<String, List<ApptusFinalResponse>> pubDetails = new HashMap<String, List<ApptusFinalResponse>>(); 
		GenericAPIResponse		response	 = new GenericAPIResponse();      
		try {
			pubDetails = webgenericpanelservice.webApptusBookGroupPanelService(url, panel);		    
		    BookGroupResponse	bookgroupFinal = new BookGroupResponse();
		    EbCategories 		ebCategories 	= hazelCastDependentService._getCategory( action,cacheBridge.getCategoryCache(countrycode) );										
			String parentCategoryName = "";
			bookgroupFinal.setParentid(0);
			if(ebCategories != null)			
				parentCategoryName = ebCategories.getCategoryName();			
		    for(Entry<String, List<ApptusFinalResponse>> entryObj:pubDetails.entrySet()){
		    	bookgroupFinal.setBookgroupid(0);		    		  
		    	List<ApptusFinalResponse> listofData = entryObj.getValue();
		    	if(listofData.size()>0){
		    		ApptusAttributes attributes = listofData.get(0).getAttributes();		    		
		    		bookgroupFinal.setName(parentCategoryName);			    	
		    		if(subcatslugname.equalsIgnoreCase("nyheter")){
		    			if(sorton != null && !sorton.equalsIgnoreCase(""))
		    				bookgroupFinal.setSorton(sorton);
			    		else
			    			bookgroupFinal.setSorton("publishedate");
		    		}else{
		    			if(sorton != null && !sorton.equalsIgnoreCase(""))
		    				bookgroupFinal.setSorton(sorton);
			    		else
			    			bookgroupFinal.setSorton(attributes.getSorton());
		    		}
			    	if(attributes.getFormattype() == null)
			    		bookgroupFinal.setFormattype(ProductFormatEnum.getFormat(formatno));
			    	else
			    		bookgroupFinal.setFormattype(ProductFormatEnum.getFormat(attributes.getFormattype()));
		    	}
		    	ArrayList<UIBook> listofbooks = new ArrayList<>();
		    	ArrayList<ProductInformation> listofProds = apptusServiceHelper.operateOnAptusSecondSearch(listofData.get(0).getProducts());
		    	List<String> topTitles = new ArrayList<String>(); 
		    	for( ProductInformation	product	: listofProds){		 		
		    		UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
		 												product.getTitle(),
		 												product.getIsbn(),
		 												product.getCoverUrl(),
		 												URLBuilder.buildUrl(product),
		 												new Long(product.getRelProductidApp()));
		 			book.setRelevance(product.getRelevance());
		 			book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
		 			book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
		 			listofbooks.add(book);
		 			if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT )
		 				topTitles.add( book.getIsbn() );
		 		}		    	
		    	bookgroupFinal.setBooks(listofbooks);
		    	bookgroupFinal.setToptitles((ArrayList<String>) topTitles);
		    }		    
		    response.setStatus(HttpStatus.OK);
		    response.setData(bookgroupFinal);
		} catch (Exception e) {
			logger.error("Error while fetching toplist :::" + e.getMessage());
			e.printStackTrace();
		}		      
		return response;
	}
	
	//Code Using
	public GenericAPIResponse callViewAllforTopList(String action,Integer formatno,String subcatslugname,
													String sorton,Integer pagenumber,Integer windowsize, 
													String subscription, String countrycode, User user)
	{
		WebGenericPanel	panel = new WebGenericPanel();
		if( !StringUtils.isBlank( countrycode ) )	
			panel.setMarket_filter( countrycode );		
		
		EbCategories categoryInfo = hazelCastDependentService._getCategory(action,cacheBridge.getCategoryCache(countrycode));
		panel.setParent_category_filter( FilterUtils.buildCategoryId(countrycode, "" + categoryInfo.getCategoryId()) );		
			if(StringUtils.isNotBlank(categoryInfo.getPrimarylanguage()))
						{
							String[] b = categoryInfo.getPrimarylanguage().split(",");
							String	language_filter_new  = "";
							if(b!= null && b.length>0){
								byte	count =0;
								for(String aut:b){						
									//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
									if(count ==0)
										language_filter_new = language_filter_new + "language:'"+ aut + "'";
									 else
										 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
									count++;
								 }
							 }
							if( !StringUtils.isBlank( language_filter_new ))
								panel.setLanguage_filter(language_filter_new);
						}
		if(formatno!=null && formatno >= 1)
			panel.setFormat_filter( "" + formatno );			
		
		if(subcatslugname.equalsIgnoreCase("nyheter")){
			if( !StringUtils.isBlank( sorton ) && !StringUtils.equals( sorton, "publisheddate") )
				panel.setRelevance_sort( sorton );			
			else if(StringUtils.equalsAnyIgnoreCase( sorton, "publisheddate") )
				panel.setRelevance_sort("publisheddate desc");			
			else			
				panel.setRelevance_sort("publisheddate desc");			
		}else{
			if( !StringUtils.isBlank( sorton ) && !StringUtils.equals( sorton, "publisheddate") )			
				panel.setRelevance_sort( sorton );			
			else if(sorton.equalsIgnoreCase("publisheddate"))			
				panel.setRelevance_sort("publisheddate desc");			
			else			
				panel.setRelevance_sort("relevance");			
		}
		List<Integer>	windowattr = apptusServiceHelper.getWebWindowStartAndEnd(pagenumber, windowsize);
		panel.setWindow_first( "" + windowattr.get( 0 ));
		panel.setWindow_last( "" + windowattr.get( 1 ));
		Map<String , Integer> map = InMemoryCache.subAgeLimits;
		if(StringUtils.isNotBlank( subscription ) )
			panel.setSubscription_filter( " " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ));			 
		 
		if( user !=null && !StringUtils.isBlank( user.getUserCountry()) )
		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry()));			
		}
		if(user!=null && user.getExcludePubs()!=null)		
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs()));							
		
		String	cat_exclude_filter = apptusServiceHelper.getExcludeCategoryForApptusRequest(countrycode );		
		if(!StringUtils.isBlank( cat_exclude_filter ))
			panel.setCategory_Exclude_filter(cat_exclude_filter);
		
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(countrycode) );
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey(user));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey(user));				
		return getTopListForCategoriesViewAll(applicationpropconfig.getToplistUrl(), panel, formatno,action,sorton,subcatslugname,countrycode);
	}
	
	//Code Using
	public GenericAPIResponse callAppropriateUrlForViewAll(String action,Integer formatno,String subcatslugname,
														   String sorton,Integer pagenumber,Integer windowsize, 
														   String subscription, String countrycode, User user)
	{
	
		BockerPanelRequest	panel = new BockerPanelRequest();								
		if(formatno!=null && formatno >= 1)
			panel.setFormat_filter( "" + formatno );
		if(!StringUtils.isBlank( sorton ) && !StringUtils.equalsIgnoreCase(sorton, "publisheddate") )		
			panel.setRelevance_sort( sorton );
		else if(StringUtils.equalsIgnoreCase(sorton, "publisheddate"))
			panel.setRelevance_sort("publisheddate desc");
		else
			panel.setRelevance_sort("relevance");
		List<Integer>	windowattr = apptusServiceHelper.getWebWindowStartAndEnd(pagenumber, windowsize);
		panel.setWindow_first( "" + windowattr.get( 0 ));
		panel.setWindow_last( "" + windowattr.get( 1 ));		
		Map<String , Integer> map = InMemoryCache.subAgeLimits;
		if(subscription!= null && subscription != "")		 
			panel.setSubscription_filter(" " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ));		 		
		if(user !=null && !StringUtils.isBlank( user.getUserCountry() ) )		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ) );
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));
		}					
		if(user!=null && user.getExcludePubs()!=null)
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ) );
		
		if(subcatslugname.contains("ekommenderar") && countrycode!=null && countrycode.equalsIgnoreCase("SE"))
			panel.setBookid_filter( " NOT " + FilterUtils.build_Vi_RekommenderFilter(null, "0","0"));			
		else if(subcatslugname.contains("ekommenderar") && !StringUtils.isBlank(countrycode) )
			panel.setBookid_filter(" NOT " + FilterUtils.build_Vi_RekommenderFilter(countrycode, "0", "0"));
		
		if(subcatslugname.contains("nyheter") && countrycode!=null  && countrycode.equalsIgnoreCase("SE"))
			panel.setBookid_filter_new( " NOT " + FilterUtils.buildNyheterFilter(null, "0","0"));					
		else if(subcatslugname.contains("nyheter") && !StringUtils.isBlank(countrycode) )
			panel.setBookid_filter_new(" NOT " + FilterUtils.buildNyheterFilter(countrycode, "0","0"));			
		
		panel.setMarket(apptusServiceHelper.getMarketPanelArgument(countrycode));
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey(user));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey(user) );
		
		String	cat_exclude_filter = apptusServiceHelper.getExcludeCategoryForApptusRequest(countrycode);			
		if( !StringUtils.isBlank( cat_exclude_filter ))
			panel.setCategory_Exclude_filter(cat_exclude_filter);
		
		if(!StringUtils.isBlank( countrycode ))
			panel.setMarket_filter(countrycode);					
		
		String panelurl 			  = applicationpropconfig.getGenericUrl() + action + "_"+ countrycode.toLowerCase() + "/"+subcatslugname ;
		return getBookGroupFromApptus(panelurl, panel, formatno,subcatslugname,sorton,countrycode);
	}
		
	public static String  getDate() {
		Date date = new Date();
		return new SimpleDateFormat("MM-dd-yyyy").format(date);
    }
		
	public ArrayList<UIBookGroup>	 callAppropriateUrlForParentCategoriesNew(String action,Integer formatno, String subscription, 
																			  ArrayList<UIBookGroup>	bookgrouplist,String countrycode, User user)
	{		
		List<EbCategories> categoryids = cacheBridge.getCategoriesExcludeParentCategory(cacheBridge.getCategoryCache(countrycode), action);				
		int loopsize	= categoryids.size();
		int counter 	= 0;
		CategoryZonePanelRequest	panel = new CategoryZonePanelRequest();
		Map<String , Integer> map = InMemoryCache.subAgeLimits;		
		if(!StringUtils.isBlank(subscription))
			panel.setSubscription_filter( " " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ));			 		 		
		
		if( user != null && !StringUtils.isBlank( user.getUserCountry() ) ){
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ) );
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ) );
		}			
		if(user !=null && user.getExcludePubs()!=null )
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ) );
		
		if( !StringUtils.isBlank( user.getUserCountry() ) )
			panel.setMarket_filter( countrycode );					
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey( user ));
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey( user ) );
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(countrycode) );		
		panel.setWindow_first( "" + 1 );
		panel.setWindow_last( "" + 50 );
		panel.setRelevance_sort( FilterUtils.buildSortByFilter(null));						
		HashMap<Integer,HashMap<String, List<Subcategoryzone>>> pubDetails1 = new HashMap<Integer,HashMap<String, List<Subcategoryzone>>>();
		int j =1;
		while( counter <= (loopsize-1) ) {
			panel.setCategory_1( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(counter).getCategoryId() ) );
			panel.setCategoryname_1( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(counter).getCategoryId() ) );			
			if((counter+1) <=(loopsize-1)){							
				panel.setCategory_2( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(counter + 1).getCategoryId() ) );
				panel.setCategoryname_2( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(counter + 1).getCategoryId() ) );
			}			
			if((counter+2) <=(loopsize-1)){				
				panel.setCategory_3( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(counter + 2).getCategoryId() ) );
				panel.setCategoryname_3( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(counter + 2).getCategoryId() ) );
			}
			counter = counter + applicationpropconfig.getWebcategorylodperrequest();			
			Map<String, List<Subcategoryzone>> pubDetails = new HashMap<String, List<Subcategoryzone>>(); 
		    try {
		    	pubDetails = webapptuspanelservice.webApptusCategoriesGroup(applicationpropconfig.getCategoryZone(), panel);		    			    			    	
		    	pubDetails1.put(j,(HashMap<String, List<Subcategoryzone>>) pubDetails);
		    	j++;
			} catch (Exception e) {
			    logger.error("Error while fetching parent category :::" + e.getMessage());
				e.printStackTrace();
			}		  
		}		
   	  	for(Entry<Integer,HashMap<String, List<Subcategoryzone>>> xyz:pubDetails1.entrySet()){
   	  		for(Entry<String, List<Subcategoryzone>> entryObj:xyz.getValue().entrySet()){
   	  			UIBookGroup bookgroup = new UIBookGroup();
   	  			bookgroup.setBookgroupid(0);
   	  			bookgroup.setSlug(entryObj.getKey());
   	  			List<Subcategoryzone> listofData = entryObj.getValue();
   	  			List<CategoryList> categoriesname = listofData.get(0).getCategoryList();
   	  			List<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product>  productlist = null;
   	  			if(listofData.size()>1)
   	  				productlist = listofData.get(1).getProducts();
   	  			if(productlist != null){
		   		  if(listofData.size()>0){
		   			  bookgroup.setIsparentcategory(Boolean.valueOf("false"));
		   			  bookgroup.setIscategory(Boolean.valueOf("true"));
		   			  bookgroup.setName(categoriesname.get(0).getDisplayName());
		   			  bookgroup.setSorton("relevance");
		   			  bookgroup.setFormattype(ProductFormatEnum.getFormat(formatno));
		   		  }		   		  
		   		  ArrayList<UIBook> listofbooks = new ArrayList<UIBook>();
		   		  ArrayList<ProductInformation> listofProds = operateOnAptusFourthSearch((ArrayList<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product>)productlist);
		   		  List<String> topTitles = new ArrayList<String>(); 
		   		  for( ProductInformation	product	: listofProds){					
						UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
														product.getTitle(),
														product.getIsbn(),
														product.getCoverUrl(),
														URLBuilder.buildUrl(product),
														new Long(product.getRelProductidApp()));
						book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
						book.setAuthors(product.getAuthors());
						book.setRelevance(product.getRelevance());
						book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
						listofbooks.add(book);
						if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT )
							topTitles.add( book.getIsbn() );								   		  
		   		 }		   		 
		   		 bookgroup.setBooks(listofbooks);
		   		 bookgrouplist.add(bookgroup);
   	  			}   		    		   		
   	  		}
   	  	}   	  
   	  	return bookgrouplist;
	}
		
	public ArrayList<ProductInformation> operateOnAptusFourthSearch(ArrayList<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product> product)
	{
		ArrayList<ProductInformation> productlist = new ArrayList<>();
		//ArrayList<ProductsAptus> product =  pubDetails.getSearchHits().get(0).getProducts();
		for(se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product data:product)
		{
			ProductInformation temp = new ProductInformation();
			temp.setAllowedinlibrary((byte)1);
			String tempAuhor = data.getAttributes().getAuthors();
			if( StringUtils.isNotBlank( tempAuhor ) )
			{
				if(tempAuhor.indexOf("|")!= -1)
				{
					String[] list1 = tempAuhor.split("\\|");
					HashSet<String> authors = new HashSet<String>(list1.length);
					for(String a:list1)
					{
						a = a.replaceAll("\\|", "");
						authors.add(a);
					}
					temp.setAuthors(authors);
				}
				else
				{
					HashSet<String> authors = new HashSet<String>(1);
					authors.add(tempAuhor);
					temp.setAuthors(authors);
				}
			}
			
			if(data.getVariants().get(0).getAttributes().getImageversion()!=null && !data.getVariants().get(0).getAttributes().getImageversion().equalsIgnoreCase(""))
			{
				temp.setCoverUrl(data.getVariants().get(0).getAttributes().getBookid()
						+"_"+data.getVariants().get(0).getAttributes().getImageversion()
						+".jpg");
			}
			else
			{
				temp.setCoverUrl(data.getVariants().get(0).getAttributes().getBookid()+".jpg");
			}			
			temp.setRank((StringUtils.isNotBlank( data.getAttributes().getRank() ))?Integer.parseInt(data.getAttributes().getRank()):0);
			temp.setRelevance(data.getAttributes().getRelevance());
			temp.setTicket(data.getVariants().get(0).getTicket());
			temp.setProductFormat(ProductFormatEnum.getFormat(data.getVariants().get(0).getAttributes().getFormattype()));
			temp.setIsbn(data.getVariants().get(0).getKey());
			temp.setTitle(data.getAttributes().getTitle());
			temp.setProductFormat(ProductFormatEnum.getFormat(data.getVariants().get(0).getAttributes().getFormattype()));
			temp.setProductid(Integer.parseInt(data.getVariants().get(0).getAttributes().getBookid()));
			if(data.getVariants() != null && data.getVariants().size() >1)
			{
				temp.setRelProductid(Integer.parseInt(data.getVariants().get(1).getAttributes().getBookid()));
			}
			else if(data.getVariants().get(0).getAttributes().getRelatedproductid()!= null && 
					Integer.valueOf(data.getVariants().get(0).getAttributes().getRelatedproductid())> 0)
			{
				temp.setRelProductid(Integer.valueOf(data.getVariants().get(0).getAttributes().getRelatedproductid()));
			}
			else
			{
				temp.setRelProductid(0);
			}
			productlist.add(temp);
		}
		return productlist;
	}
	
	public GenericAPIResponse callAppropriateUrlForParentCategoriesPageNumber(String action,Integer formatno, String subscription, 
																				Integer windowsize, String countrycode, User user)
	{
		BockerPanelRequest	panel = new BockerPanelRequest();						
		List<Integer>		windowattr = apptusServiceHelper.getWebWindowStartAndEnd(0, windowsize);
		panel.setWindow_first( "" + windowattr.get(0));
		panel.setWindow_last( "" + windowattr.get(1));					
		EbCategories categoryInfo = hazelCastDependentService._getCategory(action,cacheBridge.getCategoryCache(countrycode));		
		if(formatno!=null && formatno >= 1)
			panel.setFormat_filter("" + formatno );							
		panel.setParent_category_filter( FilterUtils.buildCategoryId(countrycode, "" + categoryInfo.getCategoryId() ));
		
		if(categoryInfo!=null && StringUtils.isNotBlank(categoryInfo.getPrimarylanguage()))
		{
			String[] b = categoryInfo.getPrimarylanguage().split(",");
			String	language_filter_new  = "";
			if(b!= null && b.length>0){
				byte	count =0;
				for(String aut:b){						
					//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
					if(count ==0)
						language_filter_new = language_filter_new + "language:'"+ aut + "'";
					 else
						 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
					count++;
				 }
			 }
			if( !StringUtils.isBlank( language_filter_new ))
				panel.setLanguage_filter(language_filter_new);
		}
		
		Map<String , Integer> map = InMemoryCache.subAgeLimits;		
		if(subscription!= null && subscription != "")
			panel.setSubscription_filter(" " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ));			 		 
		
		if(user != null && !StringUtils.isBlank( user.getUserCountry() ) )
		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));			
		}		
		if(user !=null && user.getExcludePubs()!=null )		
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs()));		
		
		if( !StringUtils.isBlank( countrycode ))		
			panel.setMarket_filter(countrycode);
		panel.setMarket(apptusServiceHelper.getMarketPanelArgument(countrycode));
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey(user));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey(user));				
		return getBockerFromApptus(applicationpropconfig.getParentCategoryUrl()+"_"+countrycode.toLowerCase(), panel, formatno,action, subscription, 1,countrycode,user);
	}
	
	//Code Using
	public GenericAPIResponse	 callAppropriateUrlForParentCategoriesNewPageNumber(String action,Integer formatno, String subscription, 
																				   Integer pagenumber, Integer windowsize,String countrycode, User user)
	{
		GenericAPIResponse		response	 = new GenericAPIResponse();
		ArrayList<UIBookGroup>	bookgrouplist = new ArrayList<>();
		//String url ="";
		List<Integer> categoryids = new ArrayList<>();
		HashMap<String,String> subcategoryslugdata = new HashMap<>();
		EbCategories categoryInfo = hazelCastDependentService._getCategory(action,cacheBridge.getCategoryCache(countrycode));
		if(categoryInfo!=null)
		{
			List<EbCategories> d2 = categoryInfo.getSubCategories();
			Collections.sort(d2,new CustomComparator2());
			categoryids = new ArrayList<>();
			for(EbCategories d3:d2){
				categoryids.add(d3.getCategoryId());
				subcategoryslugdata.put(d3.getCategoryName().trim(), d3.getSlugName().trim());
			}
		}
		int maxsize = ((pagenumber -1) *3)-1;
		int minsize = maxsize -2;
		int loopsize = categoryids.size();

		CategoryZonePanelRequest	panel = new CategoryZonePanelRequest();
		Map<String , Integer> map = InMemoryCache.subAgeLimits;		
		if(formatno!=null && formatno >= 1)		
			panel.setFormat_filter("" + formatno );
		if(subscription!= null && subscription != "")
			panel.setSubscription_filter(" " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ) );			 
		
		if(user!=null && !StringUtils.isBlank( user.getUserCountry() ) ){
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter(user.getUserCountry()));
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ) );
		}				
		if(user !=null && user.getExcludePubs()!=null )
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ));		
		if( !StringUtils.isBlank( countrycode ) )
			panel.setMarket_filter( countrycode );
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(countrycode) );
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey(user));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey(user));
		panel.setRelevance_sort("relevance");
		
		List<Integer>	windowattr = apptusServiceHelper.getWebWindowStartAndEnd(0, windowsize);
		panel.setWindow_first( "" + windowattr.get(0));
		panel.setWindow_last( "" + windowattr.get(1));			
							
		HashMap<Integer,HashMap<String, List<Subcategoryzone>>> pubDetails1 = new HashMap<Integer,HashMap<String, List<Subcategoryzone>>>();
		int j =1;
		if(minsize<=(loopsize-1))
		{					
			panel.setCategory_1(  FilterUtils.buildCategoryId(countrycode, "" + categoryids.get(minsize)));
			panel.setCategoryname_1( FilterUtils.buildCategoryId(countrycode, "" + categoryids.get(minsize)) );
			
						EbCategories cat = cacheBridge.getCategoryCache(countrycode).get(categoryids.get(minsize));
						if(StringUtils.isNotBlank(cat.getPrimarylanguage()))
						{
							String[] b = cat.getPrimarylanguage().split(",");
							String	language_filter_new  = "";
							if(b!= null && b.length>0){
								byte	count =0;
								for(String aut:b){						
									//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
									if(count ==0)
										language_filter_new = language_filter_new + "language:'"+ aut + "'";
									 else
										 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
									count++;
								 }
							 }
							if( !StringUtils.isBlank( language_filter_new ))
								panel.setLanguage_filter_1(language_filter_new);
						}
			
			
			if((minsize+1) <=(loopsize-1))
			{				
				panel.setCategory_2( FilterUtils.buildCategoryId(countrycode , "" +categoryids.get(minsize+1) ) );
				panel.setCategoryname_2( FilterUtils.buildCategoryId( countrycode, "" + categoryids.get(minsize+1) ) );
				
				EbCategories cat1 = cacheBridge.getCategoryCache(countrycode).get(categoryids.get(minsize+1));
				if(StringUtils.isNotBlank(cat1.getPrimarylanguage()))
				{
					String[] b = cat1.getPrimarylanguage().split(",");
					String	language_filter_new  = "";
					if(b!= null && b.length>0){
						byte	count =0;
						for(String aut:b){						
							//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
							if(count ==0)
								language_filter_new = language_filter_new + "language:'"+ aut + "'";
							 else
								 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
							count++;
						 }
					 }
					if( !StringUtils.isBlank( language_filter_new ))
						panel.setLanguage_filter_2(language_filter_new);
				}
			}			
			if((minsize+2) <=(loopsize-1))
			{	
				panel.setCategory_3( FilterUtils.buildCategoryId(countrycode , "" +categoryids.get(minsize+2) ) );
				panel.setCategoryname_3( FilterUtils.buildCategoryId(countrycode , "" +categoryids.get(minsize+2) ) );	
				
				EbCategories cat1 = cacheBridge.getCategoryCache(countrycode).get(categoryids.get(minsize+2));
				if(StringUtils.isNotBlank(cat1.getPrimarylanguage()))
				{
					String[] b = cat1.getPrimarylanguage().split(",");
					String	language_filter_new  = "";
					if(b!= null && b.length>0){
						byte	count =0;
						for(String aut:b){						
							//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
							if(count ==0)
								language_filter_new = language_filter_new + "language:'"+ aut + "'";
							 else
								 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
							count++;
						 }
					 }
					if( !StringUtils.isBlank( language_filter_new ))
						panel.setLanguage_filter_3(language_filter_new);
				}
			}			
			Map<String, List<Subcategoryzone>> pubDetails = new HashMap<String, List<Subcategoryzone>>(); 
		    try {		    	
		    	pubDetails =  webapptuspanelservice.webApptusCategoriesGroup( applicationpropconfig.getCategoryZone(), panel);  		    
		    	pubDetails1.put(j,(HashMap<String, List<Subcategoryzone>>) pubDetails);
		    	j++;
			} catch (Exception e) {
				e.printStackTrace();
			}		     
		}
		
		//convertor to actual object				
   	  	UIBookGroupFinal	bookgroupFinal = new UIBookGroupFinal();   	  	
   	  	for(Entry<Integer,HashMap<String, List<Subcategoryzone>>> xyz:pubDetails1.entrySet())
   	  	{
   	  		for(Entry<String, List<Subcategoryzone>> entryObj:xyz.getValue().entrySet()){
   	  			UIBookGroup bookgroup = new UIBookGroup();
   	  			bookgroup.setBookgroupid(0);   		 
   	  			List<Subcategoryzone> listofData = entryObj.getValue();
   	  			List<CategoryList> categoriesname = listofData.get(0).getCategoryList();
   	  			List<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product>  productlist = null;
   	  			if(listofData.size()>1)
   	  			{
   	  				productlist = listofData.get(1).getProducts();
   	  			}
   	  			List<String> topTitles = new ArrayList<String>(); 
   	  			if(productlist != null)
   	  			{
   	  				if(listofData.size()>0)
   	  				{
   	  					bookgroup.setSlug(subcategoryslugdata.get(categoriesname.get(0).getDisplayName().trim()));
   	  					bookgroup.setIsparentcategory(Boolean.valueOf("false"));
   	  					bookgroup.setIscategory(Boolean.valueOf("true"));
			    		bookgroup.setName(categoriesname.get(0).getDisplayName().trim());
			    		bookgroup.setSorton("relevance");
			    		bookgroup.setFormattype(ProductFormatEnum.getFormat(formatno));		
		   		  }		   		  
		   		 ArrayList<UIBook> listofbooks = new ArrayList<>();
		   		 ArrayList<ProductInformation> listofProds = operateOnAptusFourthSearch((ArrayList<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product>)productlist);		   		 
		   		 for( ProductInformation	product	: listofProds){
						//logger.debug(" product.getRelProductid()  : "+product.getIsbn()+"/"+product.getRelProductid() );
						UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
														product.getTitle(),
														product.getIsbn(),
														product.getCoverUrl(),
														URLBuilder.buildUrl(product),
														new Long(product.getRelProductidApp()));
						book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
						book.setAuthors(product.getAuthors());
						book.setRelevance(product.getRelevance());
						book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
						listofbooks.add(book);
						if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT ){
							topTitles.add( book.getIsbn() );
						}
		   		  
		   		 }
		   		 bookgroup.setToptitles(topTitles);
		   		 bookgroup.setBooks(listofbooks);
		   		 bookgrouplist.add(bookgroup);
   		  }   		 
   	  	  bookgroupFinal.setToptitles((ArrayList<String>) topTitles);
		  if(categoryInfo!=null)    	  
			 bookgroupFinal.setTitle(categoryInfo.getCategoryName());    	  
   	  	}
   	  }
   	  bookgroupFinal.setBookgroups(bookgrouplist);   	  
	  //response.setData(pubDetails1);
   	  response.setStatus(HttpStatus.OK);
	  response.setData(bookgroupFinal);
	  return response;
	}
	
	//Code using
	public GenericAPIResponse callAppropriateUrlForViewAllSubCategories(Integer formatno, String subcatslugname,String sorton,
																		Integer pagenumber,Integer windowsize, String subscription,
																		String countrycode,User user)
	{
	
		WebGenericPanel	panel = new WebGenericPanel();				
		EbCategories subCategoryInfo = hazelCastDependentService._getCategory(subcatslugname,cacheBridge.getCategoryCache(countrycode));				
		panel.setParent_category_filter( FilterUtils.buildCategoryId(countrycode,  "" + subCategoryInfo.getCategoryId() ) );
		
		
		if(StringUtils.isNotBlank(subCategoryInfo.getPrimarylanguage()))
		{
			String[] b = subCategoryInfo.getPrimarylanguage().split(",");
			String	language_filter_new  = "";
			if(b!= null && b.length>0){
				byte	count =0;
				for(String aut:b){						
					//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
					if(count ==0)
						language_filter_new = language_filter_new + "language:'"+ aut + "'";
					 else
						 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
					count++;
				 }
			 }
			if( !StringUtils.isBlank( language_filter_new ))
				panel.setLanguage_filter(language_filter_new);
		}
		
		if(formatno!=null && formatno >= 1)
		panel.setFormat_filter( "" + formatno );			
		
		if( !StringUtils.isBlank( sorton ) && !StringUtils.equalsAnyIgnoreCase( sorton, "publisheddate") )
			panel.setRelevance_sort( sorton );		
		else if(sorton.equalsIgnoreCase("publisheddate"))		
			panel.setRelevance_sort("publisheddate desc");		
		else
			panel.setRelevance_sort("relevance");				
		
		List<Integer>	windowattr = apptusServiceHelper.getWebWindowStartAndEnd(pagenumber, windowsize);
		panel.setWindow_first( "" + windowattr.get( 0 ) );
		panel.setWindow_last( "" + windowattr.get( 1 ) );
										
		Map<String , Integer> map = InMemoryCache.subAgeLimits;		
		if( !StringUtils.isBlank( subscription ) )
			panel.setSubscription_filter( FilterUtils.buildSubscriptionFilter(" " + FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription))));		 
		
		if(user!=null && !StringUtils.isBlank( user.getUserCountry() ) ){
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ) );
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ) );
		}
		
		if(user !=null && user.getExcludePubs()!=null )
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ));
		
		if( user != null && !StringUtils.isBlank( user.getCountrycode() ) )
			panel.setMarket_filter(countrycode);
		
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey(user));
		panel.setMarket(apptusServiceHelper.getMarketPanelArgument(countrycode));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey(user));		
		return getBookGroupFromApptusSubCategories(applicationpropconfig.getToplistUrl(), panel, formatno,subcatslugname,sorton,countrycode);
	}
	
	//Code using
	public GenericAPIResponse getBookGroupFromApptusSubCategories (String url, WebGenericPanel panel, Integer formatno, 
																   String subcatslugname, String sorton,String countrycode)
	{
		Map<String, List<ApptusFinalResponse>> pubDetails = new HashMap<String, List<ApptusFinalResponse>>(); 
		GenericAPIResponse		response	 = new GenericAPIResponse();      
		try {			
			pubDetails = webgenericpanelservice.webApptusBookGroupPanelService(url, panel);					    	 
		    BookGroupResponse	bookgroupFinal 	= new BookGroupResponse();
		    EbCategories 		ebCategories 	= hazelCastDependentService._getCategory( subcatslugname,cacheBridge.getCategoryCache(countrycode));				
		    int parentCategory	= ( ebCategories!= null && ebCategories.getParentCatId() != null && ebCategories.getParentCatId() > 0 )?
										ebCategories.getParentCatId():0;										
			bookgroupFinal.setParentid(parentCategory);
			if(parentCategory >0)
			{
				EbCategories p_ebCategories 	= cacheBridge.getCategoryCache(countrycode).get( parentCategory );
				bookgroupFinal.setParentcategoryname(p_ebCategories.getCategoryName());
			}
			if(null != pubDetails && pubDetails.size()>0)
			{
				for(Entry<String, List<ApptusFinalResponse>> entryObj:pubDetails.entrySet())
				{
					bookgroupFinal.setBookgroupid(0);					
				    List<ApptusFinalResponse> listofData = entryObj.getValue();
				    if(listofData.size()>0)
				    {
				    	ApptusAttributes attributes = listofData.get(0).getAttributes();				    	
				    	bookgroupFinal.setName(ebCategories.getCategoryName());					    		  
				    	if(sorton != null && !sorton.equalsIgnoreCase(""))
				    		bookgroupFinal.setSorton(sorton);
				    	else
				    		bookgroupFinal.setSorton("relevance");
					    if(attributes.getFormattype() == null)
					    	bookgroupFinal.setFormattype(ProductFormatEnum.getFormat(formatno));
					    else
					    	bookgroupFinal.setFormattype(ProductFormatEnum.getFormat(-1));
				    }
				    ArrayList<UIBook> listofbooks = new ArrayList<>();
				    ArrayList<ProductInformation> listofProds = apptusServiceHelper.operateOnAptusSecondSearch(listofData.get(0).getProducts());
				    List<String> topTitles = new ArrayList<String>(); 
				    for( ProductInformation	product	: listofProds){
				    	//logger.debug(" product.getRelProductid()  : "+product.getIsbn()+"/"+product.getRelProductid() );
				 		UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
				 												product.getTitle(),
				 												product.getIsbn(),
				 												product.getCoverUrl(),
				 												URLBuilder.buildUrl(product),
				 												new Long(product.getRelProductidApp()));
				 		book.setRelevance(product.getRelevance());
				 		book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
				 		book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
				 		listofbooks.add(book);
				 		if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT ){
				 			topTitles.add( book.getIsbn() );
				 		}				    		  
				    }				    	
				    bookgroupFinal.setBooks(listofbooks);
				    bookgroupFinal.setToptitles((ArrayList<String>) topTitles);				    		 
				 }
		    }
			else
			{
				bookgroupFinal.setName(ebCategories.getCategoryName());
			}		    
		    response.setStatus(HttpStatus.OK);
		    response.setData(bookgroupFinal);
		} catch (Exception e) {
			logger.error("Error while subcategory  " + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public GenericAPIResponse	 callAppropriateUrlForMainCategoriesPageNumber(String action,Integer formatno, String subscription, 
																			   Integer pagenumber, Integer windowsize,String countrycode,User user)
	{
		GenericAPIResponse		response	 	= new GenericAPIResponse();
		ArrayList<UIBookGroup>	bookgrouplist 	= new ArrayList<>();
		//String url ="";		
		List<Integer> categoryids = new ArrayList<>();
		HashMap<String,String> subcategoryslugdata = new HashMap<>();
		Map<Integer,EbCategories> categoryInfolist = cacheBridge.getCategoryCache(countrycode);
		List<EbCategories> listdata = new ArrayList<EbCategories>();		
		if(categoryInfolist != null)
		{
			for(Entry<Integer,EbCategories> entry:categoryInfolist.entrySet())
			{
				if(entry.getValue().getParentCatId()==0)
				listdata.add(entry.getValue());
			}
			Collections.sort(listdata,new CustomComparator2());			
			for(EbCategories d3:listdata)
			{
				categoryids.add(d3.getCategoryId());
				subcategoryslugdata.put(d3.getCategoryName().trim(), d3.getSlugName().trim());
			}
		}
		int maxsize = ((pagenumber-1) *3)-1;
		int minsize = maxsize -2;
		int loopsize = categoryids.size();
		//int counter =0;
		MainCategoryZonePanelRequest	panel = new MainCategoryZonePanelRequest();
		Map<String , Integer> map = InMemoryCache.subAgeLimits;				
		if(formatno!=null && formatno >= 1)
			panel.setFormat_filter("" + formatno );			
		if( !StringUtils.isBlank( subscription ))		 
			panel.setSubscription_filter( FilterUtils.buildSubscriptionFilter("1", "" + map.get(subscription) ));			 		 
		
		if(user !=null && !StringUtils.isBlank( user.getUserCountry() ) )
		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter(user.getUserCountry() ));
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter(user.getUserCountry() ));			
		}
		
		if(user !=null && user.getExcludePubs()!=null )
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs()));					
		if(!StringUtils.isBlank( countrycode ))
			panel.setMarket_filter(countrycode);			
		panel.setMarket(apptusServiceHelper.getMarketPanelArgument(countrycode));
		panel.setSessionKey( apptusServiceHelper.setWebSessionKey(user));
		panel.setCustomerKey( apptusServiceHelper.setWebCustomerKey(user));				
		List<Integer>	windowattr = apptusServiceHelper.getWebWindowStartAndEnd(0, windowsize);
		panel.setWindow_first("" + windowattr.get( 0 ));
		panel.setWindow_last("" + windowattr.get( 1 ));				
		
		HashMap<Integer,HashMap<String, List<Subcategoryzone>>> pubDetails1 = new HashMap<Integer,HashMap<String, List<Subcategoryzone>>>();
		int j =1;
		if(minsize<=(loopsize-1)){						
			panel.setCategory_1( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(minsize) ) );
			panel.setCategoryname_1( FilterUtils.buildCategoryId(countrycode,  "" + categoryids.get(minsize) ) );
			
			EbCategories cat =categoryInfolist.get(categoryids.get(minsize));
			if(StringUtils.isNotBlank(cat.getPrimarylanguage()))
						{
							String[] b = cat.getPrimarylanguage().split(",");
							String	language_filter_new  = "";
							if(b!= null && b.length>0){
								byte	count =0;
								for(String aut:b){						
									//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
									if(count ==0)
										language_filter_new = language_filter_new + "language:'"+ aut + "'";
									 else
										 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
									count++;
								 }
							 }
							if( !StringUtils.isBlank( language_filter_new ))
								panel.setLanguage_filter_1(language_filter_new);
						}
			//String groupid = apptusServiceHelper.getSortForPromoteDePromote(countrycode, cat.getSlugName());
			String groupid = cat.getApptus_sort_attrib();
			if(StringUtils.isBlank(groupid))
				groupid = apptusServiceHelper.getSortForPromoteDePromote(countrycode, cat.getSlugName());
			if( !StringUtils.isBlank( groupid ) )
				panel.setRelevance_sort_1( groupid +" desc, relevance" );
			else
				panel.setRelevance_sort_1("relevance");		
			
			
			if(StringUtils.isNotBlank(cat.getApptus_sort_attrib()))
			{
				panel.setDpromote_filter_1(" NOT "+cat.getApptus_sort_attrib()+":'-1'");
			}
			
			if((minsize+1) <=(loopsize-1))
			{				
				panel.setCategory_2( FilterUtils.buildCategoryId(countrycode, "" + categoryids.get(minsize+1) ) );
				panel.setCategoryname_2(FilterUtils.buildCategoryId(countrycode, "" + categoryids.get(minsize+1) ) );
				
				EbCategories cat1 =categoryInfolist.get(categoryids.get(minsize+1));
				
				if(StringUtils.isNotBlank(cat1.getPrimarylanguage()))
				{
					String[] b = cat1.getPrimarylanguage().split(",");
					String	language_filter_new  = "";
					if(b!= null && b.length>0){
						byte	count =0;
						for(String aut:b){						
							//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
							if(count ==0)
								language_filter_new = language_filter_new + "language:'"+ aut + "'";
							 else
								 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
							count++;
						 }
					 }
					if( !StringUtils.isBlank( language_filter_new ))
						panel.setLanguage_filter_2(language_filter_new);
				}
				
				//String groupid1 = apptusServiceHelper.getSortForPromoteDePromote(countrycode, cat1.getSlugName());
				String groupid1 = cat1.getApptus_sort_attrib();
				if(StringUtils.isBlank(groupid1))
					groupid1 = apptusServiceHelper.getSortForPromoteDePromote(countrycode, cat1.getSlugName());
				if( !StringUtils.isBlank( groupid1 ) )
					panel.setRelevance_sort_2( groupid1 + " desc, relevance" );
				else
					panel.setRelevance_sort_2("relevance");
				
				if(StringUtils.isNotBlank(cat1.getApptus_sort_attrib()))
				{
					panel.setDpromote_filter_2(" NOT "+cat1.getApptus_sort_attrib()+":'-1'");
				}
			}			
			if((minsize+2) <=(loopsize-1))
			{				
				panel.setCategory_3( FilterUtils.buildCategoryId(countrycode, "" + categoryids.get(minsize+2) ) );
				panel.setCategoryname_3( FilterUtils.buildCategoryId(countrycode, "" + categoryids.get(minsize+2) ) );
				EbCategories cat1 =categoryInfolist.get(categoryids.get(minsize+2));
				
				if(StringUtils.isNotBlank(cat1.getPrimarylanguage()))
				{
					String[] b = cat1.getPrimarylanguage().split(",");
					String	language_filter_new  = "";
					if(b!= null && b.length>0){
						byte	count =0;
						for(String aut:b){						
							//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
							if(count ==0)
								language_filter_new = language_filter_new + "language:'"+ aut + "'";
							 else
								 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
							count++;
						 }
					 }
					if( !StringUtils.isBlank( language_filter_new ))
						panel.setLanguage_filter_3(language_filter_new);
				}
				
				//String groupid1 = apptusServiceHelper.getSortForPromoteDePromote(countrycode, cat1.getSlugName());
				String groupid1 = cat1.getApptus_sort_attrib();
				if(StringUtils.isBlank(groupid1))
					groupid1 = apptusServiceHelper.getSortForPromoteDePromote(countrycode, cat1.getSlugName());
				if( !StringUtils.isBlank( groupid1 ) )
					panel.setRelevance_sort_3(  groupid1 +" desc, relevance" );
				else
					panel.setRelevance_sort_3("relevance");
				
				if(StringUtils.isNotBlank(cat1.getApptus_sort_attrib()))
				{
					panel.setDpromote_filter_3(" NOT "+cat1.getApptus_sort_attrib()+":'-1'");
				}
			}			
			Map<String, List<Subcategoryzone>> pubDetails = new HashMap<String, List<Subcategoryzone>>(); 
		    try {
		    	pubDetails = webapptuspanelservice.webApptusMainCategoriesGroup(applicationpropconfig.getGenericUrl()+"categoryzonemain", panel);		    	
		    	pubDetails1.put(j,(HashMap<String, List<Subcategoryzone>>) pubDetails);
		    	j++;
			} catch (Exception e) {
				e.printStackTrace();
			}		     
		}		
   	  	UIBookGroupFinal	bookgroupFinal = new UIBookGroupFinal();   	  	
   	  	for(Entry<Integer,HashMap<String, List<Subcategoryzone>>> xyz:pubDetails1.entrySet())
   	  	{
   	  		for(Entry<String, List<Subcategoryzone>> entryObj:xyz.getValue().entrySet())
   	  		{
   	  			UIBookGroup bookgroup = new UIBookGroup();
   	  			bookgroup.setBookgroupid(0);
   		 
   	  			List<Subcategoryzone> listofData = entryObj.getValue();
   	  			List<CategoryList> categoriesname = listofData.get(0).getCategoryList();
   	  			List<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product>  productlist = null;
   	  			if(listofData.size()>1)
   	  			{
   	  				productlist = listofData.get(1).getProducts();
   	  			}
   	  			List<String> topTitles = new ArrayList<String>(); 
   	  			if(productlist != null)
   	  			{
   	  				if(listofData.size()>0)
   	  				{
		   			  	  bookgroup.setSlug(subcategoryslugdata.get(categoriesname.get(0).getDisplayName().trim()));
		   			  	  bookgroup.setIsparentcategory(Boolean.valueOf("true"));
			    		  bookgroup.setIscategory(Boolean.valueOf("true"));
			    		  bookgroup.setName(categoriesname.get(0).getDisplayName().trim());
			    		  bookgroup.setSorton("relevance");
			    		  bookgroup.setFormattype(ProductFormatEnum.getFormat(formatno));		
   	  				}		   		  
   	  				ArrayList<UIBook> listofbooks = new ArrayList<>();
   	  				ArrayList<ProductInformation> listofProds = operateOnAptusFourthSearch((ArrayList<se.frescano.nextory.apptus.model.SubCategoryFetchVo.Product>)productlist);		   		 
   	  				for( ProductInformation	product	: listofProds){
						//logger.debug(" product.getRelProductid()  : "+product.getIsbn()+"/"+product.getRelProductid() );
						UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
														product.getTitle(),
														product.getIsbn(),
														product.getCoverUrl(),
														URLBuilder.buildUrl(product),
														new Long(product.getRelProductidApp()));
						book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
						book.setAuthors(product.getAuthors());
						book.setRelevance(product.getRelevance());
						book.setRank(product.getRank() != null ? String.valueOf( product.getRank()):"");
						listofbooks.add(book);
						if( topTitles.size() < CONSTANTS.TOP_TITLES_LIMIT ){
							topTitles.add( book.getIsbn() );
						}
		   		  
   	  				}
   	  				bookgroup.setToptitles(topTitles);
   	  				bookgroup.setBooks(listofbooks);
   	  				bookgrouplist.add(bookgroup);
   	  			}   		 
   	  			bookgroupFinal.setToptitles((ArrayList<String>) topTitles);		
   	  			bookgroupFinal.setTitle( hazelCastDependentService._getCategory(
   	  					subcategoryslugdata.get(categoriesname.get(0).getDisplayName().trim())
   	  					,cacheBridge.getCategoryCache(countrycode)).getCategoryName());			    		
   	  		}
   	  	}
   	  	bookgroupFinal.setBookgroups(bookgrouplist);   	  	  
   	  	response.setStatus(HttpStatus.OK);
   	  	response.setData(bookgroupFinal);
   	  	return response;
	}	
	//end
		
	/*
	 * 
	 * Get Top Books From Apptus
	 */
	public List<ProductInformation> getTopEbookAndAudioBookApptus(String subscriptionType, String countrycode)
	{
		BockerPanelRequest	panel = new BockerPanelRequest();
		List<ProductInformation> topEbookAndAudioBook = new ArrayList<>();
		String url = applicationpropconfig.getBockerUrl() + ( ( StringUtils.isNotBlank( countrycode ) )? "_"+countrycode.toLowerCase() : "");		
		panel.setWindow_first( "1" );
		panel.setWindow_last( "2" );		
		if( StringUtils.isNotBlank( countrycode ) && countrycode.equalsIgnoreCase("SE"))
		{ 
			panel.setBookid_filter_new( " NOT " + FilterUtils.buildNyheterFilter(null, "0","0"));
			panel.setBookid_filter(" NOT " + FilterUtils.build_Vi_RekommenderFilter(null, "0","0"));				
		}else if( StringUtils.isNotBlank( countrycode )  )
		{
			panel.setBookid_filter_new( " NOT " + FilterUtils.buildNyheterFilter(countrycode, "0","0"));
			panel.setBookid_filter(" NOT " + FilterUtils.build_Vi_RekommenderFilter(countrycode, "0","0"));			
		}
			
		Map<String , Integer> submap = InMemoryCache.subAgeLimits;
		if( StringUtils.isNotBlank( subscriptionType ) )		
			panel.setSubscription_filter(" " + FilterUtils.buildSubscriptionFilter("1","" + submap.get(subscriptionType) ) );		
		
		if( StringUtils.isNotBlank( subscriptionType ) )
			panel.setMarket_filter(countrycode);			
		panel.setMarket(apptusServiceHelper.getMarketPanelArgument(countrycode));				
		
		Map<String, List<ApptusFinalResponse>> 	pubDetails = webapptuspanelservice.webApptusBookGroupPanelService(url, panel);
		Map<String,  ArrayList<ProductInformation>> response = new HashMap<String,  ArrayList<ProductInformation>>();
		int i =0;
	    int relprodid =0;
	    for(Entry<String, List<ApptusFinalResponse>> entryObj:pubDetails.entrySet()){
	    	List<ApptusFinalResponse> 		listofData 	= entryObj.getValue();
	    	ArrayList<ProductInformation> 	listofProds = apptusServiceHelper.operateOnAptusSecondSearch(listofData.get(0).getProducts());
	    	int j = 0;
	    	ArrayList<ProductInformation> newlistofProds = new ArrayList<ProductInformation>();
	    	for( ProductInformation product	: listofProds){
	    		if(i ==0 && j==0){
	    			relprodid = product.getRelProductidApp();
	    			if(j==0){
	    				newlistofProds.add(product);
	    				break;
	    			}
	 			}
	    		else
	 			{
	 				if(relprodid ==0 || (relprodid !=0 && relprodid !=product.getProductid()))
	 				{
	 					newlistofProds.add(product);
	 					break;
	 				}
	 			}
	 			j++;	 				
	    	}
	    	if(i ==0)	    	
	    		response.put("topEbooks", newlistofProds);	    	
	    	else
	    	{
	    		response.put("topAbooks", newlistofProds);
	    		break;
	    	}
	    	i++;
	    }		
		for(Entry<String,  ArrayList<ProductInformation>> map:response.entrySet())
		{
			for(ProductInformation book:map.getValue()){
				topEbookAndAudioBook.add(book);
				break;
			}
		}
		return topEbookAndAudioBook;	  
	 }
	
	public List<ProductInformation> getTopNyether(String countrycode, Integer sub){
		List<ProductInformation> topEbookAndAudioBook = new ArrayList<>();				
		BockerPanelRequest 	panel = new BockerPanelRequest();
		String	apptusNyheterBokerURL = applicationpropconfig.getBockerUrl() + ( ( StringUtils.isNotBlank( countrycode ) )? "_"+countrycode.toLowerCase():"");				
		panel.setWindow_first("1");
		panel.setWindow_last("2");			
		if(StringUtils.isNotBlank( countrycode ) && countrycode.equalsIgnoreCase("SE"))
		{
			panel.setBookid_filter_new(" NOT " + FilterUtils.buildNyheterFilter(null, "0","0"));
			panel.setBookid_filter(" NOT " + FilterUtils.build_Vi_RekommenderFilter(null, "0","0"));				
		}else if( StringUtils.isNotBlank( countrycode ) )
		{
			panel.setBookid_filter_new(" NOT " + FilterUtils.buildNyheterFilter(countrycode, "0","0"));
			panel.setBookid_filter(" NOT " + FilterUtils.build_Vi_RekommenderFilter(countrycode, "0","0"));				
		}			
		panel.setMarket(apptusServiceHelper.getMarketPanelArgument(countrycode));
		
		if(sub !=null)
		panel.setSubscription_filter(" " + FilterUtils.buildSubscriptionFilter("1", "" + sub ));
		
		if( !StringUtils.isBlank( countrycode ) )
		panel.setMarket_filter( countrycode );	
		
		Map<String, List<ApptusFinalResponse>> pubDetails = null; 
		Map<String,  ArrayList<ProductInformation>> response = new HashMap<String,  ArrayList<ProductInformation>>(); 
		//GenericAPIResponse		response	 = new GenericAPIResponse();      
		try {
			pubDetails = webapptuspanelservice.webApptusBookGroupPanelService(apptusNyheterBokerURL, panel);
		    int i =1;
		    for(Entry<String, List<ApptusFinalResponse>> entryObj:pubDetails.entrySet())
		    {
		    	List<ApptusFinalResponse> 		listofData = entryObj.getValue();
		    	ArrayList<ProductInformation> listofProds  = apptusServiceHelper.operateOnAptusSecondSearch(listofData.get(0).getProducts());
		    	ArrayList<ProductInformation> newlistofProds = new ArrayList<ProductInformation>();
		    	for( ProductInformation product	: listofProds)
		    	{
		    		if(i==3)
		    		{
		    			newlistofProds.add(product);
		    		}
		    		else
		    		{
		    			break;
		    		}
		 		}
		    	if(i ==3)
		    		response.put("Nyether", newlistofProds);
		    	i++;
		    }
		} catch (Exception e) {
			logger.error("Error while fetching bocker from apptus :::" + e.getMessage());
			e.printStackTrace();
		}		
		for(Entry<String,  ArrayList<ProductInformation>> map:response.entrySet()){
			for(ProductInformation book:map.getValue()){
				topEbookAndAudioBook.add(book);
				//break;
			}
		}
		return topEbookAndAudioBook;	  
	 }

	public List<UIBook> relatedSeriesBooks(String seriesname, Integer apptusid, String countrycode){
		RelatedBookZonePanel	zonepanel 	= apptusServiceHelper.getAptusUrlForRelatedSeries(seriesname, apptusid, countrycode);
		List<UIBook> 			seriesBooks = new ArrayList<UIBook>();		
		AptusThird 				pubDetails 	= null;
		try {
			pubDetails = releatedbookzoneservice.search( zonepanel );
		} catch (Exception e) {					
			e.printStackTrace();
		}
		if(null != pubDetails && null != pubDetails.getSearchHits()){
			AptusSecond pubdetaisl1 = new AptusSecond();
			pubdetaisl1.setSearchHits(pubDetails.getSearchHits());
			ArrayList<ProductInformation> seriesList = apptusServicehelper.operateOnAptusSecondSearch( pubdetaisl1 );			
			if( seriesList != null ){
				for(ProductInformation seriesProdInfo : seriesList){
					if(seriesProdInfo.getSeries().equalsIgnoreCase(seriesname))
					{
						UIBook book = new UIBook();
						book.setBookid(new Long(seriesProdInfo.getProductid()));
						book.setTitle( seriesProdInfo.getTitle() );
						book.setFormattype( seriesProdInfo.getProductFormat() );								
						book.setIsbn(seriesProdInfo.getIsbn());
						book.setAuthors(seriesProdInfo.getAuthors());
						book.setCoverimg(seriesProdInfo.getCoverUrl());
						if(null != seriesProdInfo.getRelProductid())
							book.setRelatedbookid(new Long(seriesProdInfo.getRelProductidApp())); 
						book.setWeburl(URLBuilder.buildUrl(seriesProdInfo));   							
						seriesBooks.add(book);
					}
				}
			}
		}
		return seriesBooks;
	}
	
	public List<UIBook> relatedAutorBooks(List<String> authors, Integer apptusid, List<Integer> excludepublishers, String usercountry, String localecode){		
		List<UIBook> 			authorBooks = new ArrayList<UIBook>();	
		User	user = new User();
		user.setUserCountry(usercountry);
		if( excludepublishers != null ){			
			user.setExcludePubs(excludepublishers.toArray(new Integer[excludepublishers.size()]));
		}		
		
		RelatedBookZonePanel panel = apptusServicehelper.getAptusUrlForRelatedAuthors(authors, apptusid, user, localecode);					
		AptusThird pubDetails = releatedbookzoneservice.search(panel);									
		if(null != pubDetails && null != pubDetails.getSearchHits())
		{
			AptusSecond pubdetaisl1 = new AptusSecond();
			pubdetaisl1.setSearchHits(pubDetails.getSearchHits());							
			ArrayList<ProductInformation> authorList = apptusServicehelper.operateOnAptusSecondSearch( pubdetaisl1 );		
			if( authorList != null ){
				for(ProductInformation authorProdInfo : authorList){
					UIBook book = new UIBook();
					book.setBookid(new Long(authorProdInfo.getProductid()));
					book.setTitle( authorProdInfo.getTitle() );
					book.setFormattype( authorProdInfo.getProductFormat() );							
					book.setIsbn(authorProdInfo.getIsbn());
					book.setAuthors(authorProdInfo.getAuthors());
					book.setCoverimg(authorProdInfo.getCoverUrl());
					if(null != authorProdInfo.getRelProductid())
						book.setRelatedbookid(new Long(authorProdInfo.getRelProductidApp())); 
					book.setWeburl(URLBuilder.buildUrl(authorProdInfo));   
					authorBooks.add(book);
				}
			}
		}
		return authorBooks;
	}
	
	public List<UIBook> recommandedBooks(Integer apptusid, List<String> authors, String localecode) throws UnsupportedEncodingException{		
		List<UIBook> 			books = new ArrayList<UIBook>(0);	
		if( !StringUtils.equalsAnyIgnoreCase( applicationpropconfig.getRecommendMode(), "on") )
			return books;
		
		ProductRelatedDetails	prod = new ProductRelatedDetails();
		prod.setAuthors(authors);
		RecommendZone panel = apptusServicehelper.getAptusUrlForRecommendBooks(apptusid, prod , localecode);
		ApptusFinalResponse2 pubDetails = apptusrecommendzoneservice.genericPanelSearch(panel);				
	    if(pubDetails != null && pubDetails.getRecommendBasedOnProduct() != null && pubDetails.getRecommendBasedOnProduct().get(0) != null
	    	&& pubDetails.getRecommendBasedOnProduct().get(0).getProducts() != null)
	    {
	    	ArrayList<ProductInformation> recommendbooklist = apptusServicehelper.operateOnApptusProduct(
	    			pubDetails.getRecommendBasedOnProduct().get(0).getProducts(),null,0,Double.valueOf("0"),false);
	    	if( recommendbooklist != null ){
	    		for(ProductInformation authorProdInfo : recommendbooklist){
	    			UIBook book = new UIBook();
	  				book.setBookid(new Long(authorProdInfo.getProductid()));
	  				book.setTitle( authorProdInfo.getTitle() );
	  				book.setFormattype( authorProdInfo.getProductFormat() );			  				
	  				book.setIsbn(authorProdInfo.getIsbn());
	  				book.setAuthors(authorProdInfo.getAuthors());
	  				book.setCoverimg(authorProdInfo.getCoverUrl());
	  				if(null != authorProdInfo.getRelProductid())
	  					book.setRelatedbookid(new Long(authorProdInfo.getRelProductidApp())); 
	  				
	  				book.setWeburl(URLBuilder.buildUrl(authorProdInfo));   
	  				books.add(book);
	  			}	  			
	    	}			    	  
	    }
		return books;
	}
	
	
	
	public BooksForBookGroupsApptusResponseWrapper getDataForRecommendZoneForTopList(User user, BooksForBookGroupsApptusResponseWrapper proddetails) throws Exception
	{
		// Recommend books Scenario
		/*if(reqBean.getBookgroupid() != null && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED.getGroupid())
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED.getGroupid()))
		{*/
			RecommendBasedOnCustomer	panelrequest = new RecommendBasedOnCustomer();	
			/*ProductFormatEnum	format = ProductFormatEnum.getFormat( reqBean.getType() );
			if( format == ProductFormatEnum.E_BOOK || format == ProductFormatEnum.AUDIO_BOOK )
				panelrequest.setFormat_filter( "" + format.getFormatNo() );	*/
			//List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd( ( !StringUtils.isBlank( reqBean.getPagenumber() )?Integer.valueOf( reqBean.getPagenumber()):0 ), reqBean.getRows() );			
			panelrequest.setWindow_first("" + 1);
			panelrequest.setWindow_last( "" + 3);
			//panelrequest.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
			if(user.getRoot_profile()!=null && user.getRoot_profile().getProfileid()!=null)
			panelrequest.setCustomerKey( String.valueOf(user.getRoot_profile().getProfileid()));
			if(user.getCountrycode()!=null)
			panelrequest.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
			
			if( !StringUtils.isBlank( user.getUserCountry() ) ){				
				panelrequest.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry()  ));				
				panelrequest.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry()  ));					
			}
			if(user.getExcludePubs()!=null)
			panelrequest.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()));
			if( !StringUtils.isBlank( user.getCountrycode()) )
				panelrequest.setMarket_filter(user.getCountrycode());
			/*String	childfilter = apptusServiceHelper.getChildFilterFromCategorys( user );
			if( !StringUtils.isBlank( childfilter ) )
				panelrequest.setChild_filter(childfilter);*/

			@SuppressWarnings("unused")
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			
			if(user!=null && user.getSubscription()!=null && user.getSubscription().getType()!=null)
			panelrequest.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" +  map.get(user.getSubscription().getType())) );		
			else
				panelrequest.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" +  map.get("PREMIUM")));
			try {
				ArrayList<ProductsAptus> list = aptusrecommendbasedoncustomer.genericPanelSearch(panelrequest);
				if( list.size() > 0 )
				{
					ArrayList<ProductInformation> recommendbooklist = apptusServiceHelper.operateOnApptusProduct(list,null, 10,CONSTANTS.API_VERSION_6_4,true);
					proddetails.setBooks(recommendbooklist);
				}else{
					proddetails.setBooks(Collections.emptyList());
				}
			}
			catch (Exception e) {
				logger.error("Error while fetching Recommend zone" + e.getMessage());
				e.printStackTrace();
			}
		//}
		return proddetails;
	}
	
	public BooksForBookGroupsApptusResponseWrapper getPopularBockerForNewUsers(User user, BooksForBookGroupsApptusResponseWrapper proddetails) throws ParseException
	{
		//String countrycode="";
		GenericPanel	genericpanel = new GenericPanel();			
		String	authors_filter_new = apptusServiceHelper.getExcludeCategoryForApptusRequest( user.getCountrycode() );			
		if( StringUtils.isNotBlank( authors_filter_new ))
			genericpanel.setAuthors_filter_New(authors_filter_new);
	
		genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0) +" desc, relevance" );
		String depromote= apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0);
		
		if(StringUtils.isNotBlank(depromote)){
			String text = " NOT "+ depromote+ ":'-1'";
			genericpanel.setLanguage_filter(text);
		 }
		
		genericpanel.setWindow_first( "" + "1");
		genericpanel.setWindow_last( "" + "3" );
		genericpanel.setMarket(apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
		genericpanel.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
		genericpanel.setCustomerKey( apptusServiceHelper.getApptusCustomerKey(user));				
		if(user.getUserCountry()!=null && !user.getUserCountry().equalsIgnoreCase(""))
		{					
			genericpanel.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));			
			genericpanel.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));			
		}
		if(user.getExcludePubs()!=null)
			genericpanel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()));		
		if(user.getCountrycode()!=null && !user.getCountrycode().equalsIgnoreCase(""))
			genericpanel.setMarket_filter(user.getCountrycode() );
				
		Map<String , Integer> map = InMemoryCache.subAgeLimits;
		
		if(user!=null && user.getSubscription()!=null && user.getSubscription().getType()!=null)
		genericpanel.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType())) );
		else
		genericpanel.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" +  map.get("PREMIUM")));
		
		proddetails = apptusServiceHelper.getBooksFromGenericPanel(genericpanel , user);
		
		return proddetails;
	}
	
	
}

package se.frescano.nextory.apptus.service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.HtmlUtils;

import com.mongodb.client.MongoCursor;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.apptus.services.ApptusGenericPanelAppPreviewService;
import se.frescano.nextory.app.apptus.services.AptusGenericPanelService;
import se.frescano.nextory.app.constants.ApptusBookGroupsEnum;
import se.frescano.nextory.app.constants.ApptusPrmoteDePromoteEnum_DE;
import se.frescano.nextory.app.constants.ApptusPrmoteDePromoteEnum_FI;
import se.frescano.nextory.app.constants.ApptusPrmoteDePromoteEnum_SE;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS.PROFILE_CATEGORIES;
import se.frescano.nextory.app.constants.LibraryStatusEnum;
import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.product.request.BooksForBookGroupRequestBean;
import se.frescano.nextory.app.product.response.BookGroupVOApptus;
import se.frescano.nextory.app.product.response.BooksForBookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.apptus.model.AptusSecond;
import se.frescano.nextory.apptus.model.ProductsAptus;
import se.frescano.nextory.apptus.request.GenericPanel;
import se.frescano.nextory.apptus.request.GenericPanelAppPreviewRequest;
import se.frescano.nextory.apptus.request.RecommendZone;
import se.frescano.nextory.apptus.request.RelatedBookZonePanel;
import se.frescano.nextory.apptus.utils.FilterUtils;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.cache.service.HazelCastDependentService;
import se.frescano.nextory.dao.MongoDBUtilMorphia;
import se.frescano.nextory.dao.NestAggregationPipeLine;
import se.frescano.nextory.market.model.MarketListVo;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.model.ProductRelatedDetails;
import se.frescano.nextory.model.SearchRequest;
import se.frescano.nextory.model.SearchRequestBean;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.service.StartUpBean;
import se.frescano.nextory.startup.service.SubscriptionTypeVO;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.util.URLBuilder;
import se.frescano.nextory.web.api.response.Product;
import se.frescano.nextory.web.api.response.ProductListWithCount;
import se.frescano.nextory.web.apptus.request.WebSearchZonePanelRequest;

@Service
public class ApptusServiceHelper {

	private static final Logger logger = LoggerFactory.getLogger(ApptusServiceHelper.class);
		
	@Autowired
	private AbstractRestTempleteClient	abstractresttemplate;
	
	@Autowired
	private ApplicationPropertiesConfig applicationpropconfig;
		
	@Autowired
	private MongoDBUtilMorphia nextMongoDBUtil;
	
	@Autowired
	private CacheBridgeCategory cacheBridge1;
	
	
	@Autowired
	private CacheBridgeMarket cacheBridgeMarket;
	
	@Autowired
	private HazelCastDependentService	hazelCastDependentService;
	
	@Autowired
	private AptusGenericPanelService aptusgenericpanelservice;
	
	
	@Autowired
	private ApptusGenericPanelAppPreviewService	apppanelreviewservice;
	
	@Autowired
	private Book2GoUtilServiceHelper book2GoUtilServiceHelper;
	
	/*@Autowired
	private PropertiesConfiguration		propertiesconfig;*/
	
	public ApptusServiceHelper() {
		//abstractRestClient =  new AbstractRestClient();
	}
	
	public static void main(String args[]){
		RestTemplate	t = new RestTemplate();
		ResponseEntity<String> response =  t.getForEntity("https://api.esales.apptus.com/clusters/w7F121D72/panels/generic_pannel_app?&arg.parent_category_filter=categoryids_SE:'12'&arg.relevance_sort=s_barnbocker desc, relevance&arg.window_first=1&arg.window_last=12&market=SE&sessionKey=73db4aa1-de27-4744-8057-5e94f5008a8f&customerKey=68&arg.include_filter=include_restriction:'SE' OR include_restriction:'ALL' OR include_restriction:'NIL'&arg.exclude_filter= NOT exclude_restriction:'SE' OR exclude_restriction:'NIL'&arg.market_filter=markets:'SE'&arg.subscription_filter=subscriptionorder:['1','2']", String.class);
		if( response.getStatusCode() == HttpStatus.OK )
			System.out.println( response.getBody() );
	}
	
	public BooksForBookGroupsApptusResponseWrapper getBooksFromGenericPanel(GenericPanel panelrequest, User user,BooksForBookGroupRequestBean reqBean) throws ParseException {		
		Map<String, List<ProductListWithCount>> proDetails  = aptusgenericpanelservice.genericPanelSearch(panelrequest);		
		BooksForBookGroupsApptusResponseWrapper productDetails = new BooksForBookGroupsApptusResponseWrapper();
		if(null != proDetails && proDetails.size()>0)
		{
			for(Entry<String, List<ProductListWithCount>> entryObj:proDetails.entrySet())
	    	{				 
				List<ProductListWithCount> 		listofData 		= entryObj.getValue();
				ArrayList<ProductInformation> 	listofbooks 	= new ArrayList<>();
				 
				 productDetails.setBookCount(listofData.get(0).getCount());				 
				 if( listofData.size()>1 && listofData.get(1).getProducts() != null){
					 List<se.frescano.nextory.web.api.response.Product> listofProds = listofData.get(1).getProducts();
					 if( listofProds != null){
						 List<Integer>	windowstart_end = getWindowStartAndEnd((StringUtils.isBlank( reqBean.getPagenumber()))?0:Integer.valueOf( reqBean.getPagenumber() ), reqBean.getRows());
						 int start = windowstart_end.get(0);
						 listofbooks = operateOnGenericApptusProduct( listofProds, user, start, reqBean.getApiVersion(),false );								    	 
						 productDetails.setBooks(listofbooks);
					 }
				 }
	    	  }			
		}
		return  productDetails;
	}
	
	public BooksForBookGroupsApptusResponseWrapper getDataBasedOnUpcomingTopSellersBooks(Map<String, List<ProductListWithCount>> proDetails,BooksForBookGroupRequestBean reqBean,User user, Boolean upcoming)
	{
		BooksForBookGroupsApptusResponseWrapper productDetails = new BooksForBookGroupsApptusResponseWrapper();
		if(null != proDetails && proDetails.size()>0)
		{
			for(Entry<String, List<ProductListWithCount>> entryObj:proDetails.entrySet())
	    	{				 
				List<ProductListWithCount> 		listofData 		= entryObj.getValue();
				ArrayList<ProductInformation> 	listofbooks 	= new ArrayList<>();
				 
				 productDetails.setBookCount(listofData.get(0).getCount());				 
				 if( listofData.size()>1 && listofData.get(1).getProducts() != null){
					 List<se.frescano.nextory.web.api.response.Product> listofProds = listofData.get(1).getProducts();
					 if( listofProds != null){
						 List<Integer>	windowstart_end = getWindowStartAndEnd((StringUtils.isBlank( reqBean.getPagenumber()))?0:Integer.valueOf( reqBean.getPagenumber() ), reqBean.getRows());
						 int start = windowstart_end.get(0);
						 listofbooks = operateOnGenericApptusProduct( listofProds, user, start, reqBean.getApiVersion(),upcoming );								    	 
						 productDetails.setBooks(listofbooks);
					 }
				 }
	    	  }			
		}
		return  productDetails;
	}
	
	
	public BooksForBookGroupsApptusResponseWrapper getBooksFromGenericPanel(GenericPanel panelrequest, User user) throws ParseException {		
		Map<String, List<ProductListWithCount>> proDetails  = aptusgenericpanelservice.genericPanelSearch(panelrequest);		
		BooksForBookGroupsApptusResponseWrapper productDetails = new BooksForBookGroupsApptusResponseWrapper();
		if(null != proDetails && proDetails.size()>0)
		{
			for(Entry<String, List<ProductListWithCount>> entryObj:proDetails.entrySet())
	    	{				 
				List<ProductListWithCount> 		listofData 		= entryObj.getValue();
				ArrayList<ProductInformation> 	listofbooks 	= new ArrayList<>();
				 
				 productDetails.setBookCount(listofData.get(0).getCount());				 
				 if( listofData.size()>1 && listofData.get(1).getProducts() != null){
					 List<se.frescano.nextory.web.api.response.Product> listofProds = listofData.get(1).getProducts();
					 if( listofProds != null){
						 listofbooks = operateOnGenericApptusProduct( listofProds, user, 0, CONSTANTS.API_VERSION_6_4,false);								    	 
						 productDetails.setBooks(listofbooks);
					 }
				 }
	    	  }			
		}
		return  productDetails;
	}
	//Code using
	public String setWebCustomerKey(User user){
		MemberTypeEnum userEnum = null;
		if( user!= null && user.getMember_type_code() > 0 )
			 userEnum=  MemberTypeEnum.getMembertypeEnum(user.getMember_type_code());
		if(user != null && user.getCustomerid() != null && userEnum!=null && (!MemberTypeEnum.VISITOR_GIFTCARD_BUYER.equals(userEnum)
				 && !MemberTypeEnum.VISITOR_NO_ACTION.equals(userEnum)))
		 
			return (( user.getRoot_profile() != null)?"" + user.getRoot_profile().getProfileid():"0");			 
		 return "0";
	}
	
	//Code using
	public String setWebSessionKey(User user){
		MemberTypeEnum userEnum = null;
		if( user!= null && user.getMember_type_code() > 0 )
			 userEnum=  MemberTypeEnum.getMembertypeEnum(user.getMember_type_code());
		if(user != null && user.getCustomerid() != null && userEnum!=null && (!MemberTypeEnum.VISITOR_GIFTCARD_BUYER.equals(userEnum)
				 && !MemberTypeEnum.VISITOR_NO_ACTION.equals(userEnum)))		 
			return String.valueOf((user.getCustomerid() + getDate() ).hashCode());		
		 return UUID.randomUUID().toString();
	}
	
	@SuppressWarnings("deprecation")
	public ProductInformation getLibCheck(Integer productid, Integer profileid, SubscriptionTypeVO typeEnum,ProductInformation  prod,String countrycode)
	{
		NestAggregationPipeLine	aggregation = new NestAggregationPipeLine("profiles");
		aggregation.match(new Document("profileid", profileid).append("bookid", productid));	
		aggregation.project("libid","status");
		MongoCursor<Document> document = nextMongoDBUtil.aggregate( aggregation ).iterator();
		
		prod.setAvailableinlib(0);//by default		
		if( document.hasNext() ){
			Document	profile = document.next();
			LibraryStatusEnum libstatus = ( profile.get("status") != null )?LibraryStatusEnum.fromDBName( profile.getString("status") ):LibraryStatusEnum.UN_DEFINED;
			if(libstatus == LibraryStatusEnum.OFFLINE_READER)
				prod.setAvailableinlib(2);
			else if ( libstatus == LibraryStatusEnum.INACTIVE)
				prod.setAvailableinlib(1); 
			if( profile.get("libid") != null )prod.setLibid( profile.getInteger("libid") ); 
		}
		return prod;
	}
	
	public static String  getDate() {
		Date date = new Date();
		return new SimpleDateFormat("MM-dd-yyyy").format(date);
    }
	
	
	public Byte getAllowedInLibrary(String usersubscription, Integer booksubscriptionorder)
	{
		Map<String , Integer> map = InMemoryCache.subAgeLimits;
		if(booksubscriptionorder > 0 && booksubscriptionorder.intValue() <= map.get(usersubscription).intValue() )
			return 1;
		else 
			return 0;
	}
	
	//Code using
	public ArrayList<ProductInformation> operateOnApptusProduct(ArrayList<ProductsAptus> product, User user, int start, Double apiversion, Boolean biggerimage)
	{
		ArrayList<ProductInformation> productlist = new ArrayList<>();		
		for(ProductsAptus data:product)
		{
			ProductInformation temp = new ProductInformation();
			temp.setAllowedinlibrary((byte)1);
			String tempAuhor = data.getAttributes().getAuthors();
			HashSet<String> authors =  null;
			if(!StringUtils.isBlank( tempAuhor ) )
			{
				if(tempAuhor.indexOf("|")!= -1)
				{
					String[] list1 = tempAuhor.split("\\|");
					authors = new HashSet<String>(list1.length);
					for(String a:list1)										
						authors.add(StringUtils.replaceAll(a, "\\|", ""));					
				}
				else{
					authors = new HashSet<String>(1);
					authors.add(tempAuhor);					
				}
			}	
			if( authors == null )
				authors	=  new HashSet<String>(0);
			
			temp.setAuthors(authors);
			temp.setR_authors( authors.stream().map(str -> StringUtils.capitalize(str)).collect(Collectors.toCollection(HashSet::new)) );
			
			if(data.getVariants().get(0).getAttributes().getImageversion()!=null && 
					!data.getVariants().get(0).getAttributes().getImageversion().equalsIgnoreCase(""))
			{
				temp.setCoverUrl(data.getVariants().get(0).getAttributes().getBookid()+
						"_"
						+data.getVariants().get(0).getAttributes().getImageversion()
						+ ".jpg");
			}
			else			
				temp.setCoverUrl(data.getVariants().get(0).getAttributes().getBookid()+".jpg");						
			
			 if(apiversion >CONSTANTS.API_VERSION_6_2 && !StringUtils.isBlank( data.getVariants().get(0).getAttributes().getImageheight() ) 
		   		&& !StringUtils.isBlank( data.getVariants().get(0).getAttributes().getImagewidth() )		   		
		   		&& Double.valueOf(data.getVariants().get(0).getAttributes().getImagewidth()) > 0
		   		&& Double.valueOf(data.getVariants().get(0).getAttributes().getImageheight()) > 0)
		   	 {
		   		 Double imagewidth  = Double.valueOf(data.getVariants().get(0).getAttributes().getImagewidth());
		   		 Double imageheight = Double.valueOf(data.getVariants().get(0).getAttributes().getImageheight());
		   		 BigDecimal bd = new BigDecimal(Double.toString(imagewidth/imageheight));
		   		 bd = bd.setScale(4, RoundingMode.HALF_UP);
		   		 temp.setCoverratio(bd.doubleValue());
		   	 }
		   	 else if( apiversion > CONSTANTS.API_VERSION_6_2 ) 		   	 
		   		temp.setCoverratio(new Double("1"));		   	 
			
			 if(data.getVariants().get(0).getAttributes().getAvgrate()!=null)
				 temp.setAvgrate(data.getVariants().get(0).getAttributes().getAvgrate());
			 
			 if(data.getVariants().get(0).getAttributes().getNumberofrates()!=null)
				 temp.setNumberofrates(data.getVariants().get(0).getAttributes().getNumberofrates());
			 				
			temp.setRank(StringUtils.isNotBlank(data.getAttributes().getRank())?Integer.parseInt( data.getAttributes().getRank() ):0);
			temp.setTicket(data.getVariants().get(0).getTicket());
			temp.setProductFormat(ProductFormatEnum.getFormat(data.getVariants().get(0).getAttributes().getFormattype()));
			temp.setIsbn(data.getVariants().get(0).getKey());
			temp.setTitle(data.getAttributes().getTitle());			
			temp.setProductid(Integer.parseInt(data.getVariants().get(0).getAttributes().getBookid()));
			
			if(data.getVariants().get(0).getAttributes().getRelatedproductid()!=null)
			 temp.setRelProductid(data.getVariants().get(0).getAttributes().getRelatedproductid().intValue());			
			String	description = data.getVariants().get(0).getAttributes().getDescription();
			if (StringUtils.isNotBlank( description ))			
				temp.setDescription( Book2GoUtil.getStrippedDescription( Book2GoUtil.trimDiscription( description ) ) );
										
			if(StringUtils.isNotBlank(data.getVariants().get(0).getAttributes().getPublisheddate()))			
				temp.setPubdate( book2GoUtilServiceHelper.dateFormatForVer5( book2GoUtilServiceHelper.parseApptusDateString( data.getVariants().get(0).getAttributes().getPublisheddate() )) );				
			
			if(apiversion<CONSTANTS.API_VERSION_6_3)
				temp.setAvailableinlib(0);			
			
			if(user != null && user.getMembertype().isMember())
			{
				ProductInformation productinfo = new ProductInformation();
			   	productinfo.setRelProductid(data.getVariants().get(0).getAttributes().getRelatedproductid().intValue());
			   	if(apiversion<CONSTANTS.API_VERSION_6_3)
				{
			   		productinfo =getLibCheck(Integer.valueOf(data.getVariants().get(0).getAttributes().getBookid()),
	  				( user.getProfile() != null?user.getProfile().getProfileid():0),user.getSubscription(),productinfo,user.getCountrycode());
			   		if(productinfo.getLibid()!=null){
			   			temp.setLibid(productinfo.getLibid());
				   	   	temp.setAvailableinlib(productinfo.getAvailableinlib());
			   		}
				}			   
			   	temp.setAllowedinlibrary(getAllowedInLibrary(user.getSubscription().getType(), 
			   			data.getVariants().get(0).getAttributes().getSubscriptionorder()));
			   	
			   	if(data.getVariants().get(0).getAttributes().getRelatedsubscriptionorder()!=null 
			   			&& data.getVariants().get(0).getAttributes().getRelatedsubscriptionorder().intValue()>0)
			   	{
			   		temp.setRelatedbookallowedinlibrary(getAllowedInLibrary(user.getSubscription().getType(), 
				   			data.getVariants().get(0).getAttributes().getRelatedsubscriptionorder()));
			   	}else
			   		temp.setRelatedbookallowedinlibrary((byte)0);
			   	
			}
		   	
			if(StringUtils.isNotBlank( data.getAttributes().getSeries() ) )
				temp.setSeries(data.getAttributes().getSeries());
			if(data.getAttributes().getSequenceseries() != null)
				temp.setNumberinseries(Integer.valueOf(data.getAttributes().getSequenceseries()));		   		  
		   	temp.setPosition(start);
		   	
		   	if(biggerimage)
		   		temp.setImageUrl( StartUpBean.imageResize(temp.getCoverUrl(), 0, 500, 100) );
		   	else
		   		temp.setImageUrl( StartUpBean.imageResize(temp.getCoverUrl(), 0, 130, 100) );
		   
		   	if(user!=null && StringUtils.isNotBlank(user.getCountrycode()))
		   	{
			   	StringBuilder webUrl = new StringBuilder(applicationpropconfig.getSERVER_URL_GENERIC()+user.getCountrycode().toLowerCase());
				webUrl.append(URLBuilder.buildUrl(temp));
				temp.setWeburl(webUrl.toString());
		   	}
		   	else
		   	{
		   		StringBuilder webUrl = new StringBuilder(applicationpropconfig.getSERVER_URL());
				webUrl.append(URLBuilder.buildUrl(temp));
				temp.setWeburl(webUrl.toString());
		   	}
			
			start++;			
			productlist.add(temp);
		}		
		return productlist;
	}
	
	public ArrayList<ProductInformation> operateOnGenericApptusProduct(List<Product> listofProds, User user, int start, Double apiversion,Boolean upcoming)
	{
		ArrayList<ProductInformation> productlist = new ArrayList<>();		
		for(se.frescano.nextory.web.api.response.Product product: listofProds){
			ProductInformation temp			= new ProductInformation();
	 		ProductInformation productinfo	= new ProductInformation();
	 		temp.setRelProductid(Integer.valueOf(product.getVariants().get(0).getAttributes().getRelatedproductid()));
			productinfo.setRelProductid(Integer.valueOf(product.getVariants().get(0).getAttributes().getRelatedproductid()));
			
	 		if(apiversion!=null && apiversion < CONSTANTS.API_VERSION_6_3)
	 		{
	 			productinfo= getLibCheck(Integer.valueOf(product.getVariants().get(0).getAttributes().getBookid()),
	    		(user.getProfile() != null)?user.getProfile().getProfileid():0,user.getSubscription(),productinfo, user.getCountrycode());
	 			if(user.getMembertype().isMember())
		   		   temp.setAvailableinlib(productinfo.getAvailableinlib());
			   	else
			   	   temp.setAvailableinlib(0);
		   		   
		   		if(productinfo.getLibid()!=null)
		   			temp.setLibid(productinfo.getLibid());
	 		}	 		
	 		String	p_subscription = product.getVariants().get(0).getAttributes().getSubscriptionorder();
	 		String	p_relatesubscription = product.getVariants().get(0).getAttributes().getRelatedsubscriptionorder();
	 		if( StringUtils.isNotBlank( p_subscription ) && user!=null && user.getSubscription()!=null && user.getSubscription().getType()!=null)
	 			temp.setAllowedinlibrary(getAllowedInLibrary(user.getSubscription().getType(), 
	 				Integer.parseInt( p_subscription )));
	 		else
		   		temp.setAllowedinlibrary((byte)0);
	 			
		   	
		   	if(StringUtils.isNotBlank( p_relatesubscription ) && user!=null && user.getSubscription()!=null && user.getSubscription().getType()!=null)
		   	{
		   		temp.setRelatedbookallowedinlibrary(getAllowedInLibrary(user.getSubscription().getType(), 
		   				Integer.parseInt( p_relatesubscription ) ));
		   	}else
		   		temp.setRelatedbookallowedinlibrary((byte)0);
	 		String tempAuhor = product.getAttributes().getAuthors();
	        HashSet<String> authors = null;
	        if(tempAuhor != null) {
	        	if(tempAuhor.indexOf("|")!= -1)
	        	{
	        		String[] list1 = tempAuhor.split("\\|");
	        		authors = new HashSet<String>(list1.length);
	        		for(String a:list1)		    		        				    		        		
	        			authors.add(StringUtils.replaceAll(a, "\\|", ""));		    		        				    		        		
	        	}else
	        	{
				authors = new HashSet<String>(0);
				authors.add(tempAuhor);		    					
	        	}	        	
			}	 
	        
	        if( authors == null )
	        	authors	=  new HashSet<String>(0);
	        
	        temp.setAuthors(authors);
	        temp.setR_authors( authors.stream().map(str -> StringUtils.capitalize(str)).collect(Collectors.toCollection(HashSet::new)) );
	        
	        String	description = product.getVariants().get(0).getAttributes().getDescription();
			if (StringUtils.isNotBlank( description ))			
				temp.setDescription( Book2GoUtil.getStrippedDescription( Book2GoUtil.trimDiscription( description ) ) );			
			
			temp.setProductid(Integer.valueOf(product.getVariants().get(0).getAttributes().getBookid()));
	   	   	if(StringUtils.isNotBlank( product.getVariants().get(0).getAttributes().getImageversion() ) )
		   	{
	   	   		temp.setCoverUrl(product.getVariants().get(0).getAttributes().getBookid()
			   				+ "_"+ product.getVariants().get(0).getAttributes().getImageversion()
			   				+ ".jpg");  
		   	}
		   	else		   	
		   		temp.setCoverUrl(product.getVariants().get(0).getAttributes().getBookid() + ".jpg");		   	   
		   		 
		   	if(apiversion!=null && apiversion >CONSTANTS.API_VERSION_6_2 && StringUtils.isNotBlank( product.getVariants().get(0).getAttributes().getImageheight() )
		   		&& StringUtils.isNotBlank( product.getVariants().get(0).getAttributes().getImagewidth() )
		   		&& Double.valueOf(product.getVariants().get(0).getAttributes().getImagewidth()) > 0
		   		&& Double.valueOf(product.getVariants().get(0).getAttributes().getImageheight()) > 0)
		   	{
		   		 Double imagewidth = Double.valueOf(product.getVariants().get(0).getAttributes().getImagewidth());
		   		 Double imageheight = Double.valueOf(product.getVariants().get(0).getAttributes().getImageheight());
		   		 BigDecimal bd = new BigDecimal(Double.toString(imagewidth/imageheight));
		   		 bd = bd.setScale(4, RoundingMode.HALF_UP);
		   		 temp.setCoverratio(bd.doubleValue());
		   	}
		   	else if(apiversion!=null && apiversion >CONSTANTS.API_VERSION_6_2 ) 		   	 
		   		temp.setCoverratio(new Double("1"));		   	 
	   	 
		   	 if(product.getVariants().get(0).getAttributes().getAvgrate()!=null)
		   		temp.setAvgrate(product.getVariants().get(0).getAttributes().getAvgrate());
		   	 		   	 
		   	 if(product.getVariants().get(0).getAttributes().getNumberofrates()!=null)
		   		temp.setNumberofrates(product.getVariants().get(0).getAttributes().getNumberofrates());
		   	 	   	   
		   	 if(StringUtils.isNotBlank( product.getVariants().get(0).getAttributes().getPublisheddate() ) )													
		   		temp.setPubdate(book2GoUtilServiceHelper.dateFormatForVer5(book2GoUtilServiceHelper.parseApptusDateString( product.getVariants().get(0).getAttributes().getPublisheddate() )));
		   	 
		   	 if(upcoming && temp.getPubdate()!=null)
		   	 {
		   		 Date pubdate= book2GoUtilServiceHelper.parseApptusDateString( product.getVariants().get(0).getAttributes().getPublisheddate());
		   		 Date now = new Date();
		   		 LocalDate date1 = pubdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		   		 LocalDate date2 = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		   		 //LocalDate date3 = date2.now().plusDays((long) 2);
		   		 long days = ChronoUnit.DAYS.between(date2, date1);
		   		 temp.setNumberofdays(days);
		   	 }
				   	   		   
		   	 temp.setTitle(product.getAttributes().getTitle());
		   	 temp.setProductFormat( ProductFormatEnum.getFormat(product.getVariants().get(0).getAttributes().getFormattype()));	   	   		   	
		   	 temp.setIsbn(product.getVariants().get(0).getKey());		   	 
		   	 if(product.getAttributes().getSequenceseries() != null && !product.getAttributes().getSequenceseries().equalsIgnoreCase(""))
		   		temp.setNumberinseries(Integer.valueOf(product.getAttributes().getSequenceseries()));
		   	 temp.setTicket(product.getVariants().get(0).getTicket());
		   	 temp.setRank((StringUtils.isNotBlank( product.getAttributes().getRank() ))?Integer.parseInt(product.getAttributes().getRank()):0);
		   	 temp.setPosition(start);		   	 
		   	 start++;	   	   
		   	 
		   	temp.setImageUrl( StartUpBean.imageResize(temp.getCoverUrl(), 0, 130, 100) );			   
			if(user!=null && StringUtils.isNotBlank(user.getCountrycode()))
		   	{
			   	StringBuilder webUrl = new StringBuilder(applicationpropconfig.getSERVER_URL_GENERIC()+user.getCountrycode().toLowerCase());
				webUrl.append(URLBuilder.buildUrl(temp));
				temp.setWeburl(webUrl.toString());
		   	}
		   	else
		   	{
		   		StringBuilder webUrl = new StringBuilder(applicationpropconfig.getSERVER_URL());
				webUrl.append(URLBuilder.buildUrl(temp));
				temp.setWeburl(webUrl.toString());
		   	}
			
		   	productlist.add(temp);
		}		
		return productlist;
	}
	
	public BookGroupVOApptus operateOnAptusSeriesDynamic(Entry<String,ArrayList<ProductsAptus>> pubDetails, String viewby)
	{
		BookGroupVOApptus vo = null;
		ArrayList<ProductsAptus> product =  pubDetails.getValue();		
		if(product != null && product.size()> 0 )
		{
			vo = new BookGroupVOApptus();
			vo.setTitle(pubDetails.getKey()); // Temporary fix as this field as stored in lower case in aptus
			vo.setId(ApptusBookGroupsEnum.MAIN_SERIES.getGroupid()+pubDetails.getKey());
			vo.setType(ApptusBookGroupsEnum.TOP_SERIES_TYPE.getGroupid());
			vo.setHaschild(0);
			vo.setAllowsorting(0);
			vo.setShowvolume(1);
			vo.setViewby(viewby);
			vo.setSlugname(ApptusBookGroupsEnum.MAIN_SERIES.getGroupid()+pubDetails.getKey());
			ArrayList<String> covers = new ArrayList<>();
			for(ProductsAptus data:product)
			{				
				if(data.getVariants().get(0).getAttributes().getImageversion()!= null &&
					!data.getVariants().get(0).getAttributes().getImageversion().equalsIgnoreCase("")){
					covers.add(data.getVariants().get(0).getAttributes().getBookid()+
							"_"+data.getVariants().get(0).getAttributes().getImageversion());	
				}else
					covers.add(data.getVariants().get(0).getAttributes().getBookid());				
			}
			vo.setCovers(covers);			
		}
		return vo;
	}		
	
	public RelatedBookZonePanel getAptusUrlForRelatedAuthors(List<String> authors, Integer aptusid, User user, String countrycode )
	{	 
		 //String url = aptusconfig.getRelatedUrl();
		 RelatedBookZonePanel	panel = new RelatedBookZonePanel();
		 panel.setWindow_first("" + 1);
		 panel.setWindow_last("" + 100);
		 panel.setProductKeyFilter( " NOT " + FilterUtils.buildProductKeyFilter( "" + aptusid ));
			 
		 String	series_filter = "";
		 int count=0;
		 if(authors!= null && authors.size()>0){			 
			 for(String aut:authors){				
				 try {
					aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
				} catch (UnsupportedEncodingException e) {					
					e.printStackTrace();
				}
				 //series_filter = series_filter + " " + FilterUtils.buildAuthorFilter( aut );
				 
				 if(count ==0)
					 series_filter = series_filter + "authors:'"+ aut + "'";
					 else
					series_filter = series_filter + " AND authors:'"+ aut + "'";
				 
				count++;
			 }
			 if( !StringUtils.isBlank( series_filter ) )
				 panel.setSeries_filter( series_filter );
		 }
		 
		if(user!=null && !StringUtils.isBlank( user.getUserCountry() ) ){
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));
		}
				
		if(user!=null && user.getExcludePubs()!=null)
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ));
		
		if(!StringUtils.isBlank( countrycode ) )
			panel.setMarket_filter( countrycode );
		panel.setSearch_attributes( "authors" );
		panel.setMarket(getMarketPanelArgument(countrycode));		
		return panel;
	}
	
	//Code using
	public ArrayList<ProductInformation> operateOnAptusSecondSearch(ArrayList<ProductsAptus> product)
	{
		ArrayList<ProductInformation> productlist = new ArrayList<>();
		//ArrayList<ProductsAptus> product =  pubDetails.getSearchHits().get(0).getProducts();
		for(ProductsAptus data:product)
		{
			ProductInformation temp = new ProductInformation();
			temp.setAllowedinlibrary((byte)1);
			String tempAuhor = data.getAttributes().getAuthors();
			if(StringUtils.isNotBlank( tempAuhor ) )
			{
				if(tempAuhor.indexOf("|")!= -1)
				{
					String[] list1 = tempAuhor.split("\\|");
					HashSet<String> authors = new HashSet<String>(list1.length);
					for(String a:list1){
						a = a.replaceAll("\\|", "");
						authors.add(a);
					}
					temp.setAuthors(authors);
				}else
				{
					HashSet<String> authors = new HashSet<String>(1);
					authors.add(tempAuhor);
					temp.setAuthors(authors);
				}
			}			
			if(data.getVariants().get(0).getAttributes().getImageversion()!=null && 
						!data.getVariants().get(0).getAttributes().getImageversion().equalsIgnoreCase(""))
			{
				temp.setCoverUrl(data.getVariants().get(0).getAttributes().getBookid()
						+"_"+data.getVariants().get(0).getAttributes().getImageversion()
							+".jpg");
			}
			else			
				temp.setCoverUrl(data.getVariants().get(0).getAttributes().getBookid()+".jpg");			
			
			temp.setRank((StringUtils.isNotBlank( data.getAttributes().getRank() ))?Integer.parseInt(data.getAttributes().getRank()):0);
			temp.setRelevance(data.getAttributes().getRelevance());
			temp.setTicket(data.getVariants().get(0).getTicket());
			temp.setProductFormat(ProductFormatEnum.getFormat(data.getVariants().get(0).getAttributes().getFormattype()));
			temp.setIsbn(data.getVariants().get(0).getKey());
			temp.setTitle(data.getAttributes().getTitle());
			temp.setProductFormat(ProductFormatEnum.getFormat(data.getVariants().get(0).getAttributes().getFormattype()));
			temp.setProductid(Integer.parseInt(data.getVariants().get(0).getAttributes().getBookid()));
			temp.setDescription(data.getVariants().get(0).getAttributes().getDescription());	
			temp.setRelProductid(data.getVariants().get(0).getAttributes().getRelatedproductid());
			productlist.add(temp);
		}		
		return productlist;
	}
	
	public RelatedBookZonePanel getAptusUrlForRelatedSeries(String seriesname, Integer aptusid, String countrycode)
	{	 
		//String url = aptusconfig.getRelatedUrl();
		RelatedBookZonePanel	panel = new RelatedBookZonePanel();			
		panel.setSeries_filter(" " + FilterUtils.buildSeriesFilter( seriesname ) );
		panel.setProductKeyFilter( " NOT " + FilterUtils.buildProductKeyFilter( "" + aptusid ));		 
		
		if( !StringUtils.isBlank( countrycode ) )
			panel.setMarket_filter( countrycode );
		panel.setSearch_attributes("series");
		panel.setSortBy(" sequenceseries ");
		panel.setMarket( getMarketPanelArgument(countrycode) );
		panel.setWindow_first("1");
		panel.setWindow_last("100");		 
		return panel;
	}
	
	//Code using
	public ArrayList<ProductInformation> operateOnAptusSecondSearch(AptusSecond pubDetails)
	{
		ArrayList<ProductInformation> productlist = new ArrayList<>();
		ArrayList<ProductsAptus> product =  pubDetails.getSearchHits().get(0).getProducts();
		productlist = operateOnApptusProduct(product,null,0, Double.valueOf("0"),false);
		return productlist;
	}
	
	public BooksForBookGroupsApptusResponseWrapper getBooksFromGenericPanelNestPreview(GenericPanelAppPreviewRequest panel,String pagenumber,Integer rows
			,String countrycode) throws ParseException {		
		Map<String, List<ProductListWithCount>> proDetails  = apppanelreviewservice.genericPanelSearch(panel);
		BooksForBookGroupsApptusResponseWrapper productDetails=new BooksForBookGroupsApptusResponseWrapper();
		if(null != proDetails && proDetails.size()>0)
		{
			int end;
   		 	int start;
   		 	if(StringUtils.isNotBlank( pagenumber ) && Integer.valueOf(pagenumber)>=0)
   		 	{
				if(rows != null &&  rows >0)
				{
					 end = (Integer.valueOf(pagenumber) + 1)*rows;
					 start = (end-rows)+1;					
				}
				else
				{
					 end = (Integer.valueOf(pagenumber) + 1)*12;
					 start = (end-12)+1;
				}
   		 	}
   		 else
   		 {
   			 start =0;
   			 end =12;
   		 }
   		 for(Entry<String, List<ProductListWithCount>> entryObj:proDetails.entrySet())
   		 {
   			 List<ProductListWithCount> listofData = entryObj.getValue();
			 ArrayList<ProductInformation> listofbooks = new ArrayList<>();
			 productDetails.setBookCount(listofData.get(0).getCount());
			 if( listofData.size()>1 && listofData.get(1).getProducts() != null){
				 List<se.frescano.nextory.web.api.response.Product> listofProds = listofData.get(1).getProducts();
		    	 if( listofProds != null){
					//int i =0;	
		    		 for( se.frescano.nextory.web.api.response.Product	product	: listofProds)
		    		 {   	   
		    		 		ProductInformation booklist= new ProductInformation();
		    		 		
		    		 		if(product.getVariants()!=null && product.getVariants().size()>1)
		    		 			booklist.setRelProductid(Integer.valueOf(product.getVariants().get(1).getAttributes().getBookid()));

		    		       
		    		       String tempAuhor = product.getAttributes().getAuthors();
		    				if(tempAuhor != null)
		    				{
		    				if(tempAuhor.indexOf("|")!= -1)
		    				{
		    					String[] list1 = tempAuhor.split("\\|");
		    					HashSet<String> authors = new HashSet<String>();
		    					for(String a:list1)
		    					{
		    						a = a.replaceAll("\\|", "");
		    						authors.add(a);
		    					}
		    					booklist.setAuthors(authors);
		    					booklist.setR_authors( authors.stream().map(str -> StringUtils.capitalize(str)).collect(Collectors.toCollection(HashSet::new)) );
		    				}
		    				else
		    				{
		    					HashSet<String> authors = new HashSet<String>();
		    					authors.add(tempAuhor);
		    					booklist.setAuthors(authors);
		    					booklist.setR_authors( authors.stream().map(str -> StringUtils.capitalize(str)).collect(Collectors.toCollection(HashSet::new)) );
		    				}
		    				}
		    				
					       //booklist.setAuthors(product.getAttributes().getAuthors());
					   	   booklist.setDescription(product.getVariants().get(0).getAttributes().getDescription());
					   	   booklist.setProductid(Integer.valueOf(product.getVariants().get(0).getAttributes().getBookid()));
					   	   if(product.getVariants().get(0).getAttributes().getImageversion()!=null &&
					   			!product.getVariants().get(0).getAttributes().getImageversion().equalsIgnoreCase(""))
					   	   {
					   		   booklist.setCoverUrl(product.getVariants().get(0).getAttributes().getBookid() 
					   				+ "_"+product.getVariants().get(0).getAttributes().getImageversion()
					   				+ ".jpg");
					   	   }
					   	   else
					   	   {
					   		   booklist.setCoverUrl(product.getVariants().get(0).getAttributes().getBookid() + ".jpg");
					   	   }
					   	   
					   	   booklist.setImageUrl( StartUpBean.imageResize(booklist.getCoverUrl(), 0, 130, 100) );
					   	   //booklist.setWeburl(null);
					   	   //booklist.setPopularity(Integer.valueOf(product.getVariants().get(0).getAttributes().getPopularity()));
						   	if(product.getVariants().get(0).getAttributes().getPublisheddate()!= null)							
						   		booklist.setPubdate( book2GoUtilServiceHelper.dateFormatForVer5( book2GoUtilServiceHelper.parseApptusDateString( product.getVariants().get(0).getAttributes().getPublisheddate() )) );															
					   	   //booklist.setPubdate(product.getVariants().get(0).getAttributes().getPublisheddate());

					   	   booklist.setTitle(product.getAttributes().getTitle());
					   	   booklist.setProductFormat( ProductFormatEnum.getFormat(
					   	   product.getVariants().get(0).getAttributes().getFormattype()));
					   	   
					
					   	  
					   	   booklist.setIsbn(product.getVariants().get(0).getKey());
					   	   if(product.getAttributes().getSequenceseries() != null && !product.getAttributes().getSequenceseries().equalsIgnoreCase(""))
					   		   booklist.setNumberinseries(Integer.valueOf(product.getAttributes().getSequenceseries()));
					   	   booklist.setTicket(product.getVariants().get(0).getTicket());
					   	   booklist.setRank((StringUtils.isNotBlank( product.getAttributes().getRank() ))?Integer.parseInt(product.getAttributes().getRank()):0);
					   	   
						   	if(countrycode!=null && StringUtils.isNotBlank(countrycode))
						   	{
							   	StringBuilder webUrl = new StringBuilder(applicationpropconfig.getSERVER_URL_GENERIC()+countrycode.toLowerCase());
								webUrl.append(URLBuilder.buildUrl(booklist));
								booklist.setWeburl(webUrl.toString());
						   	}
						   	else
						   	{
						   		StringBuilder webUrl = new StringBuilder(applicationpropconfig.getSERVER_URL());
								webUrl.append(URLBuilder.buildUrl(booklist));
								booklist.setWeburl(webUrl.toString());
						   	}
				   	   
					   	   
					   	   booklist.setPosition(start);
					   	   start++;
					   	   
					   	   listofbooks.add(booklist);
		    			 }
		    	 
		    	 productDetails.setBooks(listofbooks);
				 }
	    	   }
	    	  }
			//response.setData(productDetails);
		}
		return  productDetails;
	}						
	
	public RecommendZone getAptusUrlForRecommendBooks(Integer aptusid,ProductRelatedDetails prodinfo, String countrycode) throws UnsupportedEncodingException
	{	 
		 if(logger.isDebugEnabled()) logger.debug("Apptus ID ::" + aptusid );
		 RecommendZone	panel = new RecommendZone();
		 //String url = aptusconfig.getRecommendUrl(); 
		 panel.setWindow_first( "1" );
		 panel.setWindow_last("100");
		 panel.setFilter("productstatus:'ACTIVE'" );
		 panel.setProductKey("" + aptusid );
		 panel.setMarket(getMarketPanelArgument(countrycode));		 		 
		 if(prodinfo!=null){
			 List<String> authorslist = prodinfo.getAuthors();
			 String	author_filter = "";
			 int count = 0;
			 if(null != authorslist && authorslist.size()>0)
				 for(String authors:authorslist){					
					authors = URLEncoder.encode(HtmlUtils.htmlUnescape(authors),"UTF-8");
					if(count ==0)
						author_filter = author_filter + " NOT authors:'"+ authors + "'";					 
					else
						author_filter = author_filter + " AND NOT authors:'"+ authors + "'";
					 count++;
				}
			}
		 if(!StringUtils.isBlank(countrycode))
			 panel.setMarket_filter(countrycode);				 
		 return panel;
	}	
	
	//Code Using
	public WebSearchZonePanelRequest getAptusUrlForSearchZone(SearchRequestBean externalReq, String field,String countrycode, User user)
	{
		//String url = "https://api.esales.apptus.com/clusters/w7F121D72/panels/search-zone-deba?arg.search_phrase=";
		WebSearchZonePanelRequest	panel = new WebSearchZonePanelRequest();
		panel.setSearchPhase( externalReq.getQ() );
		if(externalReq.getPageNumber() == null || ( externalReq.getPageNumber() != null && externalReq.getPageNumber() <=1 )){
			panel.setWindow_first("1");
			panel.setWindow_last("50");
		}else{
			List<Integer>	windowattr = getWebWindowStartAndEnd( externalReq.getPageNumber(), 50);			
			panel.setWindow_first("" + windowattr.get(0));
			panel.setWindow_last("" + windowattr.get(1));
		}		 
		if(externalReq.getFormat()!= null && externalReq.getFormat() != "")
			panel.setFormat_filter( "" + ProductFormatEnum.getFormatFromTitle(externalReq.getFormat()).getFormatNo() );		 		
		if(user !=null && !StringUtils.isBlank( user.getUserCountry() ) )
		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));
		}
		if(user !=null && user.getExcludePubs()!=null)
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ) );			
		
		if(!StringUtils.isBlank( countrycode ))
			panel.setMarket_filter(countrycode);
		 
		if(StringUtils.equalsIgnoreCase("series", field))		
			panel.setSortBy(" sequenceseries ");		 		 
		if(StringUtils.equalsIgnoreCase("series", field))
			panel.setSearch_attributes(" series ");				 
		else if(StringUtils.equalsIgnoreCase("contributors", field))
			panel.setSearch_attributes(" authors,narrators "); 		
		else
			panel.setSearch_attributes("title,authors,narrators,series");			
		panel.setMarket(getMarketPanelArgument(countrycode));
		panel.setSessionKey( setWebSessionKey(user));
		panel.setCustomerKey( setWebCustomerKey(user));		
		return panel;
	}			
	
	
	public String getParentCategoryFilter(User user){
		EbCategories kidscategories = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));
		String	category_filter = "";
		if(kidscategories != null){
			category_filter = " ("+ FilterUtils.buildCategoryId(user.getCountrycode(), "" + kidscategories.getCategoryId() );
			for(EbCategories sub: kidscategories.getSubCategories()){
				category_filter = category_filter + " OR " + FilterUtils.buildCategoryId(user.getCountrycode(), "" + sub.getCategoryId() );
			}				
			category_filter = category_filter + ") ";
		}	
		return category_filter;
	}
	
	public String getChildFilterFromCategorys(User user){
		String	child_filter = "";
		if(user.getProfile() != null && user.getProfile().getCategory() != null &&
			user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
		{ 
			EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));
			if(kidCategory!= null && kidCategory.getSubCategories() !=null) {
				byte count =0;						  
				for(EbCategories subcategory:kidCategory.getSubCategories()){
					if(count ==0)								 
						child_filter = child_filter + FilterUtils.buildCategoryId(user.getCountrycode(), "" + subcategory.getCategoryId());
					else
						child_filter = child_filter + " OR "+ FilterUtils.buildCategoryId(user.getCountrycode(), "" + subcategory.getCategoryId());
					count++;
				}
			}
		}else{
			EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));
			if(kidCategory!= null && kidCategory.getSubCategories() !=null){
				byte count = 0;
				for(EbCategories subcategory:kidCategory.getSubCategories())
				{
					if( count  ==0)								 
						child_filter = child_filter +  " NOT "+ FilterUtils.buildCategoryId(user.getCountrycode(), "" + subcategory.getCategoryId());								 
					else
						child_filter = child_filter + " AND NOT "+ FilterUtils.buildCategoryId(user.getCountrycode(), "" + subcategory.getCategoryId());
					count++;
				}
			}
		}
		return child_filter;
	}
	
	public List<Integer>	getWindowStartAndEnd(Integer pagnumber, Integer pagesize){
		int	window_start = 0, window_last = 12;
		if(pagesize == null){
			if(pagnumber == null || pagnumber == 0){
				window_start = 1;
				window_last	 = 12;
			}else{
				window_last 	= (pagnumber + 1)* 12;				 
				window_start 	= (window_last -12) +1;				
			 }
		}else if( pagesize != null){
			if(pagnumber == null || pagnumber == 0){
				window_start = 1;
				window_last	 = pagesize;				
			 }else{
				 window_last 	= (pagnumber + 1) * pagesize;				 
				 window_start 	= (window_last - pagesize) +1;					 
			 }
		}
		return Arrays.asList( window_start, window_last);
	}
	
	//Code using
	public List<Integer>	getWebWindowStartAndEnd(Integer pagnumber, Integer pagesize){
		int	window_start = 0, window_last = 50;
		if(pagesize == null){
			if(pagnumber == null || ( pagnumber != null && pagnumber <= 1) ){
				window_start = 1;
				window_last	 = 50;
			}else{
				window_last 	= pagnumber * 50;				 
				window_start 	= (window_last -50) +1;				
			 }
		}else if( pagesize != null){
			if(pagnumber == null || ( pagnumber != null && pagnumber <= 1) ){
				window_start = 1;
				window_last	 = pagesize;				
			 }else{
				 window_last 	= pagnumber * pagesize;				 
				 window_start 	= (window_last - pagesize) +1;					 
			 }
		}
		return Arrays.asList( window_start, window_last);
	}
	
	public List<String> getSubscriptionFilterApptusAttributeName(SearchRequest externalReq, Map<String , Integer> map , User user){
		int userSubRank = map.get(user.getSubscription().getType());
		int ctnew =0, temp = 0, highestSubRank = temp;
		for(Entry<String , Integer>sub:map.entrySet()){
			if(ctnew==0){
				temp = sub.getValue();
				highestSubRank = temp;
			}
			if(highestSubRank<sub.getValue())
				 highestSubRank=sub.getValue();			 
			ctnew++;
		}
		String	subscriptionfilter = "", subscriptionfilter_1 = "";
		if(StringUtils.isBlank( externalReq.getIncludenotallowedbooks() ) || 
		   ( !StringUtils.isBlank( externalReq.getIncludenotallowedbooks() ) && externalReq.getIncludenotallowedbooks().equalsIgnoreCase("0") ) )
		{
			if(userSubRank ==highestSubRank){
				 // write logic to search direclty with only one subscription filter
				subscriptionfilter = FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType())  );
				subscriptionfilter_1 = subscriptionfilter_1 + FilterUtils.SUB_ORDER_FIELD + ":'-1'";
			}else if(userSubRank<highestSubRank){
				 //write logic to search with both subscription filter
				subscriptionfilter = FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType()) );				
				byte ct =0;
				for(int i= userSubRank + 1; i<=highestSubRank; i++){
					if(ct ==0)
						subscriptionfilter_1 =  subscriptionfilter_1 + FilterUtils.buildSubscriptionFilter("" + i) ;
					else
						subscriptionfilter_1 = subscriptionfilter_1 + " OR " + FilterUtils.buildSubscriptionFilter("" + i) ;	 					 
					 ct++;
				 }
			 }else{
				 subscriptionfilter = subscriptionfilter + FilterUtils.buildSubscriptionFilter("-1");
				 subscriptionfilter_1 = subscriptionfilter_1 + FilterUtils.buildSubscriptionFilter("-1");
			 }
		 }else if(!StringUtils.isBlank( externalReq.getIncludenotallowedbooks() ) || externalReq.getIncludenotallowedbooks().equalsIgnoreCase("1"))
		 {
			 if(userSubRank ==highestSubRank) {
				 // write logic to search direclty with only one subscription filter
				 subscriptionfilter = subscriptionfilter + FilterUtils.buildSubscriptionFilter("-1");
				 subscriptionfilter_1 = subscriptionfilter_1 + FilterUtils.buildSubscriptionFilter("-1");
			 }else if(userSubRank<highestSubRank)
			 {
				 //write logic to search with both subscription filter				
				 subscriptionfilter_1 = subscriptionfilter_1+  FilterUtils.buildSubscriptionFilter("-1");				 				
				 int ct =0;
				 for(int i=userSubRank+1;i<=highestSubRank;i++){
					 if(ct ==0)
						 subscriptionfilter = subscriptionfilter + FilterUtils.buildSubscriptionFilter("" + i);
					 else
						 subscriptionfilter = subscriptionfilter + " OR " + FilterUtils.buildSubscriptionFilter("" + i);					 
					 ct++;
				 }
			 }else{
				 subscriptionfilter = subscriptionfilter + FilterUtils.buildSubscriptionFilter("-1");
				 subscriptionfilter_1 = subscriptionfilter_1 + FilterUtils.buildSubscriptionFilter("-1");
			 }
		 }		
		return Arrays.asList( subscriptionfilter, subscriptionfilter_1);
	}
	
	//Code Using
	public String getSortForPromoteDePromote(String countrycode,String slugname)
	{
		String groupid ="";
		if(countrycode!=null && !countrycode.equalsIgnoreCase("") && countrycode.equalsIgnoreCase("SE"))
		{
			ApptusPrmoteDePromoteEnum_SE enumdata = ApptusPrmoteDePromoteEnum_SE.getRelatedGroupName(slugname);
			if(null != enumdata && null != enumdata.getGroupid())
			groupid = enumdata.getGroupid();
		}
		else if(countrycode!=null && !countrycode.equalsIgnoreCase("") && countrycode.equalsIgnoreCase("FI"))
		{
			ApptusPrmoteDePromoteEnum_FI enumdata = ApptusPrmoteDePromoteEnum_FI.getRelatedGroupName(slugname);
			if(null != enumdata && null != enumdata.getGroupid())
			groupid = enumdata.getGroupid();
		}
		else if(countrycode!=null && !countrycode.equalsIgnoreCase("") && countrycode.equalsIgnoreCase("DE"))
		{
			ApptusPrmoteDePromoteEnum_DE enumdata = ApptusPrmoteDePromoteEnum_DE.getRelatedGroupName(slugname);
			if(null != enumdata && null != enumdata.getGroupid())
			groupid = enumdata.getGroupid();
		}
		
		return groupid;
	}
	
	public String getSortForPromoteDePromoteTopBooks(String countrycode,Integer format)
	{
		String groupid ="";
		if(!StringUtils.isBlank( countrycode ) && countrycode.equalsIgnoreCase("SE"))
		{
			if(format.intValue() ==1)
				groupid = ApptusPrmoteDePromoteEnum_SE.TOP_EBOOK.getGroupid();
			else if(format.intValue() ==2)
				groupid = ApptusPrmoteDePromoteEnum_SE.TOP_ABOOK.getGroupid();
			else if(format.intValue() ==0)
				groupid = ApptusPrmoteDePromoteEnum_SE.TOP_BOOK.getGroupid();
		}
		else if(!StringUtils.isBlank( countrycode ) && countrycode.equalsIgnoreCase("FI"))
		{
			if(format.intValue() ==1)
				groupid = ApptusPrmoteDePromoteEnum_FI.TOP_EBOOK.getGroupid();
			else if(format.intValue() ==2)
				groupid = ApptusPrmoteDePromoteEnum_FI.TOP_ABOOK.getGroupid();
			else if(format.intValue() ==0)
				groupid = ApptusPrmoteDePromoteEnum_FI.TOP_BOOK.getGroupid();
		}
		else if(!StringUtils.isBlank( countrycode ) && countrycode.equalsIgnoreCase("DE"))
		{
			if(format.intValue() ==1)
				groupid = ApptusPrmoteDePromoteEnum_DE.TOP_EBOOK.getGroupid();
			else if(format.intValue() ==2)
				groupid = ApptusPrmoteDePromoteEnum_DE.TOP_ABOOK.getGroupid();
			else if(format.intValue() ==0)
				groupid = ApptusPrmoteDePromoteEnum_DE.TOP_BOOK.getGroupid();
		}	
		return groupid;
	}
	
	public String	getApptusSessionKey(User user){
		return (user.getAuthToken()!=null?user.getAuthToken().getUuid():UUID.randomUUID().toString());
	}
	
	public String getApptusCustomerKey(User user){
		if(user!=null &&  user.getMembertype()!= null && (!MemberTypeEnum.VISITOR_GIFTCARD_BUYER.equals(user.getMembertype()) &&
				!MemberTypeEnum.VISITOR_NO_ACTION.equals(user.getMembertype())))
		{
			 if(user != null && user.getProfile() != null && user.getProfile().getProfileid() != null)
				 return "" + user.getProfile().getProfileid();
			 else if( user != null)
				return "" + user.getCustomerid();
		}
		return null;
	}
	
	//Code using
	public String getIncludeCategoryForApptusRequest(String countrycode){
		String	category_filter = "";
		EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache( countrycode ));
		
		if(kidCategory!= null && kidCategory.getSubCategories() !=null){
			byte 	count =0;			
			for(EbCategories subcategory:kidCategory.getSubCategories()){
				if(count ==0 )
					category_filter = category_filter + FilterUtils.buildCategoryId( countrycode, "" + subcategory.getCategoryId());
				else
					category_filter = category_filter + " OR " + FilterUtils.buildCategoryId( countrycode, "" + subcategory.getCategoryId());								
				count++;
			}			
		}
		return category_filter;
	}
	
	//Code using
	public String getExcludeCategoryForApptusRequest(String countrycode){
		EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(countrycode));
		byte count =0;
		String	cat_exclude_filter = "";
		if(kidCategory!= null && kidCategory.getSubCategories() !=null){								  
			for(EbCategories subcategory:kidCategory.getSubCategories()){
				if(count ==0)				 
					cat_exclude_filter = cat_exclude_filter + " NOT " + FilterUtils.buildCategoryId(countrycode, "" + subcategory.getCategoryId());				 
				else
					cat_exclude_filter = cat_exclude_filter + " AND NOT " + FilterUtils.buildCategoryId(countrycode, "" + subcategory.getCategoryId());
				count++;
			 }			
		}
		
		String tobe_removecategorylist = applicationpropconfig.getRemoveCategory();
		ArrayList<Integer> categoryidlist = new ArrayList<Integer>();
		if(tobe_removecategorylist.indexOf(",") ==-1)
			categoryidlist.add(Integer.valueOf( tobe_removecategorylist ) );			
		else{
			String list[] = tobe_removecategorylist.split(",");
			for(String a:list){
				categoryidlist.add(Integer.valueOf(a));
			}
		}
		for(Integer categoryid:categoryidlist){
			if(count == 0 )
				cat_exclude_filter = cat_exclude_filter + " NOT " + FilterUtils.buildCategoryId(countrycode, "" + categoryid);							
			else			
				cat_exclude_filter = cat_exclude_filter + " AND NOT " + FilterUtils.buildCategoryId(countrycode, "" + categoryid);			
		}	
		return cat_exclude_filter;
	}
	
	public String getMarketPanelArgument(String countrycode)
	{
		String market = "";
		
		try {
			MarketListVo vo = cacheBridgeMarket.getMarketFromNestBasedOnCountryCode(countrycode);
			market=vo.getMarketName();
		} catch (RestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(StringUtils.isBlank(market))
		{
			if(StringUtils.isNotBlank(countrycode) && countrycode.equalsIgnoreCase("SE"))
				market= "Sweden";
			else if(StringUtils.isNotBlank(countrycode) && countrycode.equalsIgnoreCase("FI"))
				market= "Finland";
		}
		
		return market;
	}
}

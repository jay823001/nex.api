package se.frescano.nextory.apptus.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Service;

@Service
public class Book2GoUtilServiceHelper {

	public  String APPTUS_DATE_FORMAT		   = "yyyy-MM-dd'T'HH:mm:ssZ";	
	public  String APP_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss Z";
	
	
	public  java.util.Date parseApptusDateString(String date_str){
		java.util.Date	date = null;
		SimpleDateFormat apptus_date_formater = new SimpleDateFormat( APPTUS_DATE_FORMAT );
		try{
			date = apptus_date_formater.parse(date_str);
		}catch(Exception e){
			e.printStackTrace();
		}
		return date;
	}
	
	public  String dateFormatForVer5(java.util.Date date) {
		DateFormat df = new SimpleDateFormat(APP_DATE_PATTERN);
		return date != null ? df.format(date) : "";
	}
	
	public  String dateFormatForVer5(Long seconds) {
		DateFormat df = new SimpleDateFormat(APP_DATE_PATTERN);
		return (seconds != null && seconds > 0) ? df.format(seconds) : "";
	}
}

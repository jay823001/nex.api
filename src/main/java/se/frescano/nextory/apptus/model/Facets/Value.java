
package se.frescano.nextory.apptus.model.Facets;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "text",
    "ticket",
    "count",
    "selected",
    "displayname"
})

@JsonFilter("facetvalue_filter")
public class Value {

    @JsonProperty("text")
    private String text;
    @JsonProperty("ticket")
    private String ticket;
    @JsonProperty("count")
    private long count;
    @JsonProperty("selected")
    private boolean selected;
    @JsonProperty("displayname")
    private String categoryname;
    
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    public Value withText(String text) {
        this.text = text;
        return this;
    }

    @JsonProperty("ticket")
    public String getTicket() {
        return ticket;
    }

    @JsonProperty("ticket")
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Value withTicket(String ticket) {
        this.ticket = ticket;
        return this;
    }

    @JsonProperty("count")
    public long getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(long count) {
        this.count = count;
    }

    public Value withCount(long count) {
        this.count = count;
        return this;
    }

    @JsonProperty("selected")
    public boolean isSelected() {
        return selected;
    }

    @JsonProperty("selected")
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Value withSelected(boolean selected) {
        this.selected = selected;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Value withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @JsonProperty("displayname")
	public String getCategoryname() {
		return categoryname;
	}

    @JsonProperty("displayname")
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

    
    
    

}

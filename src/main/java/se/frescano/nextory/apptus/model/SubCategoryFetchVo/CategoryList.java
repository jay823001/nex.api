
package se.frescano.nextory.apptus.model.SubCategoryFetchVo;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tree",
    "key",
    "ticket",
    "parentKey",
    "displayName",
    "attributes"
})
public class CategoryList {

    @JsonProperty("tree")
    private String tree;
    @JsonProperty("key")
    private String key;
    @JsonProperty("ticket")
    private String ticket;
    @JsonProperty("parentKey")
    private String parentKey;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("attributes")
    private CategoryListAttributes attributes;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("tree")
    public String getTree() {
        return tree;
    }

    @JsonProperty("tree")
    public void setTree(String tree) {
        this.tree = tree;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("ticket")
    public String getTicket() {
        return ticket;
    }

    @JsonProperty("ticket")
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    @JsonProperty("parentKey")
    public String getParentKey() {
        return parentKey;
    }

    @JsonProperty("parentKey")
    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("attributes")
    public CategoryListAttributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(CategoryListAttributes attributes) {
        this.attributes = attributes;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

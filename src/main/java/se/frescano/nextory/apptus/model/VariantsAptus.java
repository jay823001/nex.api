package se.frescano.nextory.apptus.model;

public class VariantsAptus {
	
	private String key;
	private String ticket;
	AttributesAptus attributes;
	
	
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public AttributesAptus getAttributes() {
		return attributes;
	}
	public void setAttributes(AttributesAptus attributes) {
		this.attributes = attributes;
	}
	@Override
	public String toString() {
		return "VariantsAptus [key=" + key + ", ticket=" + ticket + ", attributes=" + attributes + "]";
	}
	
	
	

}

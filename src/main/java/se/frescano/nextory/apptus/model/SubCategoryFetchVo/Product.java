
package se.frescano.nextory.apptus.model.SubCategoryFetchVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "key",
    "ticket",
    "variants",
    "attributes",
    "categoryReferenceAttributes"
})
public class Product {

    @JsonProperty("key")
    private String key;
    @JsonProperty("ticket")
    private String ticket;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("attributes")
    private ProductAttributes attributes;
    @JsonProperty("categoryReferenceAttributes")
    private CategoryReferenceAttributes categoryReferenceAttributes;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("ticket")
    public String getTicket() {
        return ticket;
    }

    @JsonProperty("ticket")
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @JsonProperty("attributes")
    public ProductAttributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(ProductAttributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("categoryReferenceAttributes")
    public CategoryReferenceAttributes getCategoryReferenceAttributes() {
        return categoryReferenceAttributes;
    }

    @JsonProperty("categoryReferenceAttributes")
    public void setCategoryReferenceAttributes(CategoryReferenceAttributes categoryReferenceAttributes) {
        this.categoryReferenceAttributes = categoryReferenceAttributes;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

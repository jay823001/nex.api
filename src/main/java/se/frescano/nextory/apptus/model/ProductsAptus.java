package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

public class ProductsAptus {
	
	private String key;
	private String ticket;
	ArrayList<VariantsAptus> variants = new ArrayList<VariantsAptus>();
	AttributesProductAptus attributes = new AttributesProductAptus();
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public ArrayList<VariantsAptus> getVariants() {
		return variants;
	}
	public void setVariants(ArrayList<VariantsAptus> variants) {
		this.variants = variants;
	}
	public AttributesProductAptus getAttributes() {
		return attributes;
	}
	public void setAttributes(AttributesProductAptus attributes) {
		this.attributes = attributes;
	}
	@Override
	public String toString() {
		return "ProductsAptus [key=" + key + ", ticket=" + ticket + ", variants=" + variants + ", attributes="
				+ attributes + "]";
	}
	
	
	

}

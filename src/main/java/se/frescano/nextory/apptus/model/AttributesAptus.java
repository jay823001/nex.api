package se.frescano.nextory.apptus.model;

public class AttributesAptus {

	private String formattype;
	private String providerproductid;
	private String bookid;
	private String productstatus;
	private String popularity;
	private String publisheddate;
	private Integer Nyheter_pos;
	private Integer vi_rekommenderar_pos;
	private Integer relatedproductid;
	private String description;
	private String imageversion;
	private String imageheight;
	private String imagewidth;
	private Double avgrate;
	private Integer numberofrates;
	private Integer relatedsubscriptionorder;
	private Integer subscriptionorder;
	
	
	
	
	public Double getAvgrate() {
		return avgrate;
	}
	public void setAvgrate(Double avgrate) {
		this.avgrate = avgrate;
	}
	public Integer getNumberofrates() {
		return numberofrates;
	}
	public void setNumberofrates(Integer numberofrates) {
		this.numberofrates = numberofrates;
	}
	public String getImageheight() {
		return imageheight;
	}
	public void setImageheight(String imageheight) {
		this.imageheight = imageheight;
	}
	public String getImagewidth() {
		return imagewidth;
	}
	public void setImagewidth(String imagewidth) {
		this.imagewidth = imagewidth;
	}
	public String getImageversion() {
		return imageversion;
	}
	public void setImageversion(String imageversion) {
		this.imageversion = imageversion;
	}
	public String getFormattype() {
		return formattype;
	}
	public void setFormattype(String formattype) {
		this.formattype = formattype;
	}
	public String getProviderproductid() {
		return providerproductid;
	}
	public void setProviderproductid(String providerproductid) {
		this.providerproductid = providerproductid;
	}
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public String getProductstatus() {
		return productstatus;
	}
	public void setProductstatus(String productstatus) {
		this.productstatus = productstatus;
	}
	public String getPopularity() {
		return popularity;
	}
	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}
	public String getPublisheddate() {
		return publisheddate;
	}
	public void setPublisheddate(String publisheddate) {
		this.publisheddate = publisheddate;
	}
	public Integer getNyheter_pos() {
		return Nyheter_pos;
	}
	public void setNyheter_pos(Integer nyheter_pos) {
		Nyheter_pos = nyheter_pos;
	}
	public Integer getVi_rekommenderar_pos() {
		return vi_rekommenderar_pos;
	}
	public void setVi_rekommenderar_pos(Integer vi_rekommenderar_pos) {
		this.vi_rekommenderar_pos = vi_rekommenderar_pos;
	}
	public Integer getRelatedproductid() {
		return relatedproductid;
	}
	public void setRelatedproductid(Integer relatedproductid) {
		this.relatedproductid = relatedproductid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "AttributesAptus [formattype=" + formattype + ", providerproductid=" + providerproductid + ", bookid="
				+ bookid + ", productstatus=" + productstatus + ", popularity=" + popularity + ", publisheddate="
				+ publisheddate + ", Nyheter_pos=" + Nyheter_pos + ", vi_rekommenderar_pos=" + vi_rekommenderar_pos
				+ ", relatedproductid=" + relatedproductid + ", description=" + description + "]";
	}
	public Integer getRelatedsubscriptionorder() {
		return relatedsubscriptionorder;
	}
	public void setRelatedsubscriptionorder(Integer relatedsubscriptionorder) {
		this.relatedsubscriptionorder = relatedsubscriptionorder;
	}
	public Integer getSubscriptionorder() {
		return subscriptionorder;
	}
	public void setSubscriptionorder(Integer subscriptionorder) {
		this.subscriptionorder = subscriptionorder;
	}

	
	
	
}

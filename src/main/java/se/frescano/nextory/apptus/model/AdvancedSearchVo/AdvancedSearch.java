
package se.frescano.nextory.apptus.model.AdvancedSearchVo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import se.frescano.nextory.apptus.model.SearchHitCount;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "autocomplete",
    "autocompleteAuthor",
    "productSuggestions",
    "searchHitCount",
    //"searchHitsWithCount",
    "productSuggestionsSeries",
    "autocompleteNarrators",
    "autocompleteSeries",
    "categorySuggestions",
    "promotionalListSuggestions"
})
public class AdvancedSearch 
{

   

	@Override
	public String toString() {
		return "AdvancedSearch [autocomplete=" + autocomplete + ", autocompleteAuthor=" + autocompleteAuthor
				+ ", productSuggestions=" + productSuggestions + ", searchHitCount=" + searchHitCount
				+ ", productSuggestionsSeries=" + productSuggestionsSeries + ", autocompleteNarrators="
				+ autocompleteNarrators + ", autocompleteSeries=" + autocompleteSeries + ", categorySuggestions="
				+ categorySuggestions + "]";
	}

	@JsonProperty("autocomplete")
    private List<Autocomplete> autocomplete = null;
    @JsonProperty("autocompleteAuthor")
    private List<AutocompleteAuthor> autocompleteAuthor = null;
    @JsonProperty("productSuggestions")
    private List<ProductSuggestion> productSuggestions = null;
    @JsonProperty("searchHitCount")
    private List<SearchHitCount> searchHitCount = null;
    /*@JsonProperty("searchHitsWithCount")
    private List<SearchHitsWithCount> searchHitsWithCount = null;*/
    @JsonProperty("productSuggestionsSeries")
    private List<ProductSuggestion> productSuggestionsSeries = null;
    @JsonProperty("autocompleteNarrators")
    private List<AutocompleteNarrator> autocompleteNarrators = null;
    @JsonProperty("autocompleteSeries")
    private List<AutocompleteNarrator> autocompleteSeries = null;
    @JsonProperty("categorySuggestions")
    private List<categorySuggestions> categorySuggestions = null;
    @JsonProperty("promotionalListSuggestions")
    private List<categorySuggestions> promotionalListSuggestions = null;
    

    @JsonProperty("autocomplete")
    public List<Autocomplete> getAutocomplete() {
        return autocomplete;
    }

    @JsonProperty("autocomplete")
    public void setAutocomplete(List<Autocomplete> autocomplete) {
        this.autocomplete = autocomplete;
    }

    @JsonProperty("autocompleteAuthor")
    public List<AutocompleteAuthor> getAutocompleteAuthor() {
        return autocompleteAuthor;
    }

    @JsonProperty("autocompleteAuthor")
    public void setAutocompleteAuthor(List<AutocompleteAuthor> autocompleteAuthor) {
        this.autocompleteAuthor = autocompleteAuthor;
    }

    @JsonProperty("productSuggestions")
    public List<ProductSuggestion> getProductSuggestions() {
        return productSuggestions;
    }

    @JsonProperty("productSuggestions")
    public void setProductSuggestions(List<ProductSuggestion> productSuggestions) {
        this.productSuggestions = productSuggestions;
    }
    
    @JsonProperty("productSuggestionsSeries")
    public List<ProductSuggestion> getProductSuggestionsSeries() {
        return productSuggestionsSeries;
    }

    @JsonProperty("productSuggestionsSeries")
    public void setProductSuggestionsSeries(List<ProductSuggestion> productSuggestionsSeries) {
        this.productSuggestionsSeries = productSuggestionsSeries;
    }

    
    
    /*@JsonProperty("searchHitsWithCount")
    public List<SearchHitsWithCount> getSearchHitsWithCount() {
        return searchHitsWithCount;
    }

    @JsonProperty("searchHitsWithCount")
    public void setSearchHitsWithCount(List<SearchHitsWithCount> searchHitsWithCount) {
        this.searchHitsWithCount = searchHitsWithCount;
    }*/

    
	@JsonProperty("autocompleteNarrators")
    public List<AutocompleteNarrator> getAutocompleteNarrators() {
        return autocompleteNarrators;
    }

	@JsonProperty("searchHitCount")
    public List<SearchHitCount> getSearchHitCount() {
		return searchHitCount;
	}

	@JsonProperty("searchHitCount")
	public void setSearchHitCount(List<SearchHitCount> searchHitCount) {
		this.searchHitCount = searchHitCount;
	}

	@JsonProperty("autocompleteNarrators")
    public void setAutocompleteNarrators(List<AutocompleteNarrator> autocompleteNarrators) {
        this.autocompleteNarrators = autocompleteNarrators;
    }

	@JsonProperty("autocompleteSeries")
	public List<AutocompleteNarrator> getAutocompleteSeries() {
		return autocompleteSeries;
	}

	@JsonProperty("autocompleteSeries")
	public void setAutocompleteSeries(List<AutocompleteNarrator> autocompleteSeries) {
		this.autocompleteSeries = autocompleteSeries;
	}

	@JsonProperty("categorySuggestions")
	public List<categorySuggestions> getCategorySuggestions() {
		return categorySuggestions;
	}

	@JsonProperty("categorySuggestions")
	public void setCategorySuggestions(List<categorySuggestions> categorySuggestions) {
		this.categorySuggestions = categorySuggestions;
	}

	@JsonProperty("promotionalListSuggestions")
	public List<categorySuggestions> getPromotionalListSuggestions() {
		return promotionalListSuggestions;
	}

	@JsonProperty("promotionalListSuggestions")
	public void setPromotionalListSuggestions(List<categorySuggestions> promotionalListSuggestions) {
		this.promotionalListSuggestions = promotionalListSuggestions;
	}
	
	

}

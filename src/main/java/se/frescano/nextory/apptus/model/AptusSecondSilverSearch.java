package se.frescano.nextory.apptus.model;

import java.util.List;

public class AptusSecondSilverSearch {
	
	private List<SearchHitCount> searchHitCount ;

	private List<SearchHits> searchHits;
	
	private List<SearchHitCount> searchHitCountHigher ;

	public List<SearchHitCount> getSearchHitCount() {
		return searchHitCount;
	}

	public void setSearchHitCount(List<SearchHitCount> searchHitCount) {
		this.searchHitCount = searchHitCount;
	}

	public List<SearchHits> getSearchHits() {
		return searchHits;
	}

	public void setSearchHits(List<SearchHits> searchHits) {
		this.searchHits = searchHits;
	}

	public List<SearchHitCount> getSearchHitCountHigher() {
		return searchHitCountHigher;
	}

	public void setSearchHitCountHigher(List<SearchHitCount> searchHitCountHigher) {
		this.searchHitCountHigher = searchHitCountHigher;
	}

	@Override
	public String toString() {
		return "AptusSecondSilverSearch [searchHitCount=" + searchHitCount + ", searchHits=" + searchHits
				+ ", searchHitCountHigher=" + searchHitCountHigher + "]";
	}


}

package se.frescano.nextory.apptus.model;

public class Corrections {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Corrections [text=" + text + "]";
	}
	
	
}

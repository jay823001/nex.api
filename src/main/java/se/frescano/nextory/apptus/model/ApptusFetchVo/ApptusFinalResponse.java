package se.frescano.nextory.apptus.model.ApptusFetchVo;

import java.util.ArrayList;

import se.frescano.nextory.apptus.model.ProductsAptus;

public class ApptusFinalResponse {
	
	public String name;
	
	public String displayName;
	
	public ArrayList<ProductsAptus> products;
	
	public ApptusAttributes attributes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public ArrayList<ProductsAptus> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<ProductsAptus> products) {
		this.products = products;
	}

	public ApptusAttributes getAttributes() {
		return attributes;
	}

	public void setAttributes(ApptusAttributes attributes) {
		this.attributes = attributes;
	}
	
	

}

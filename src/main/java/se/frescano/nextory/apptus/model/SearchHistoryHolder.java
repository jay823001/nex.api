package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

import se.frescano.nextory.apptus.model.SearchHistoryVo.SearchHistory;

public class SearchHistoryHolder {

	private ArrayList<SearchHistory> recentSearches;

	public ArrayList<SearchHistory> getRecentSearches() {
		return recentSearches;
	}

	public void setRecentSearches(ArrayList<SearchHistory> recentSearches) {
		this.recentSearches = recentSearches;
	}

	
	
	
}

package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

public class AutoComplete {

	
	private String name;
	
	private String displayname;
	
	private String description;
	
	private ArrayList<Completions> completions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Completions> getCompletions() {
		return completions;
	}

	public void setCompletions(ArrayList<Completions> completions) {
		this.completions = completions;
	}
	
	
	
}


package se.frescano.nextory.apptus.model.SubCategoryFetchVo;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "authors",
    "coverimage",
    "product_key",
    "rank",
    "relevance",
    "title"
})
public class ProductAttributes {

    @JsonProperty("authors")
    private String authors;
    @JsonProperty("coverimage")
    private String coverimage;
    @JsonProperty("product_key")
    private String productKey;
    @JsonProperty("rank")
    private String rank;
    @JsonProperty("relevance")
    private String relevance;
    @JsonProperty("title")
    private String title;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("authors")
    public String getAuthors() {
        return authors;
    }

    @JsonProperty("authors")
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    @JsonProperty("coverimage")
    public String getCoverimage() {
        return coverimage;
    }

    @JsonProperty("coverimage")
    public void setCoverimage(String coverimage) {
        this.coverimage = coverimage;
    }

    @JsonProperty("product_key")
    public String getProductKey() {
        return productKey;
    }

    @JsonProperty("product_key")
    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    @JsonProperty("rank")
    public String getRank() {
        return rank;
    }

    @JsonProperty("rank")
    public void setRank(String rank) {
        this.rank = rank;
    }

    @JsonProperty("relevance")
    public String getRelevance() {
        return relevance;
    }

    @JsonProperty("relevance")
    public void setRelevance(String relevance) {
        this.relevance = relevance;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

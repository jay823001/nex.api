package se.frescano.nextory.apptus.model;

public class SearchHitCount {

	public int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "SearchHitCount [count=" + count + "]";
	}
	
	
}

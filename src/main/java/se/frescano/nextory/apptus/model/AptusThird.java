package se.frescano.nextory.apptus.model;

import java.util.List;

public class AptusThird {
	

	private List<SearchHits> searchHits;


	public List<SearchHits> getSearchHits() {
		return searchHits;
	}

	public void setSearchHits(List<SearchHits> searchHits) {
		this.searchHits = searchHits;
	}

	@Override
	public String toString() {
		return "AptusSecond [ searchHits=" + (searchHits != null ? searchHits:" ") + "]";
	}
}

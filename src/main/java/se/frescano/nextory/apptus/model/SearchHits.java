package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

public class SearchHits {

	ArrayList<ProductsAptus> products = new ArrayList<ProductsAptus>();
	
	public ArrayList<ProductsAptus> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<ProductsAptus> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "SearchHits [products=" + products + "]";
	}
	
}


package se.frescano.nextory.apptus.model.ApptusFetchVo;

public class ApptusAttributes {

	public String name;
	
	public Integer sortorder;
	
	public String sorton;
	
	public String iscategory;
	
	public String isparentcategory;
	
	public Integer formattype;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSortorder() {
		return sortorder;
	}

	public void setSortorder(Integer sortorder) {
		this.sortorder = sortorder;
	}

	public String getSorton() {
		return sorton;
	}

	public void setSorton(String sorton) {
		this.sorton = sorton;
	}

	public String getIscategory() {
		return iscategory;
	}

	public void setIscategory(String iscategory) {
		this.iscategory = iscategory;
	}

	public String getIsparentcategory() {
		return isparentcategory;
	}

	public void setIsparentcategory(String isparentcategory) {
		this.isparentcategory = isparentcategory;
	}

	public Integer getFormattype() {
		return formattype;
	}

	public void setFormattype(Integer formattype) {
		this.formattype = formattype;
	}
	
	
}

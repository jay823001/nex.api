package se.frescano.nextory.apptus.model;

public class AttributesProductAptus {

	
	private String authors;
	private String title;
	private String coverimage;
	private String series;
	private String rank;
	private String relevance;
	private Integer sequenceseries;
	
	
	public String getCoverimage() {
		return coverimage;
	}
	public void setCoverimage(String coverimage) {
		this.coverimage = coverimage;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public String getAuthors() {
		return authors;
	}
	public void setAuthors(String authors) {
		this.authors = authors;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getRelevance() {
		return relevance;
	}
	public void setRelevance(String relevance) {
		this.relevance = relevance;
	}
	public Integer getSequenceseries() {
		return sequenceseries;
	}
	public void setSequenceseries(Integer sequenceseries) {
		this.sequenceseries = sequenceseries;
	}
	@Override
	public String toString() {
		return "AttributesProductAptus [authors=" + authors + ", title=" + title + ", coverimage=" + coverimage
				+ ", series=" + series + ", rank=" + rank + ", relevance=" + relevance + ", sequenceseries="
				+ sequenceseries + "]";
	}
	
	
	
}

package se.frescano.nextory.apptus.model;

public class Completions {

	
	private String text;
	
	private String ticket;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
	
}

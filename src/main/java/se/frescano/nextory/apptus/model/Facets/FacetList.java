
package se.frescano.nextory.apptus.model.Facets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "attribute",
    "isRange",
    "values",
    "range",
    "primaryfacet"
})
public class FacetList {

    @JsonProperty("attribute")
    private String attribute;
    @JsonProperty("isRange")
    private boolean isRange;
    @JsonProperty("values")
    private List<Value> values = null;
    @JsonProperty("range")
    private Range range;
    @JsonProperty("primaryfacet")
    private Boolean primaryfacet;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("attribute")
    public String getAttribute() {
        return attribute;
    }

    @JsonProperty("attribute")
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public FacetList withAttribute(String attribute) {
        this.attribute = attribute;
        return this;
    }

    @JsonProperty("isRange")
    public boolean isIsRange() {
        return isRange;
    }

    @JsonProperty("isRange")
    public void setIsRange(boolean isRange) {
        this.isRange = isRange;
    }

    public FacetList withIsRange(boolean isRange) {
        this.isRange = isRange;
        return this;
    }

    @JsonProperty("values")
    public List<Value> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<Value> values) {
        this.values = values;
    }

    public FacetList withValues(List<Value> values) {
        this.values = values;
        return this;
    }

    @JsonProperty("range")
    public Range getRange() {
        return range;
    }

    @JsonProperty("range")
    public void setRange(Range range) {
        this.range = range;
    }

    public FacetList withRange(Range range) {
        this.range = range;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FacetList withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @JsonProperty("primaryfacet")
	public Boolean getPrimaryfacet() {
		return primaryfacet;
	}

	@JsonProperty("primaryfacet")
	public void setPrimaryfacet(Boolean primaryfacet) {
		this.primaryfacet = primaryfacet;
	}

	@Override
	public String toString() {
		return "FacetList [attribute=" + attribute + ", isRange=" + isRange + ", values=" + values + ", range=" + range
				+ ", primaryfacet=" + primaryfacet + ", additionalProperties=" + additionalProperties + "]";
	}
	
	
}

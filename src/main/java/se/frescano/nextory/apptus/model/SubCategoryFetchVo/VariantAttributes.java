
package se.frescano.nextory.apptus.model.SubCategoryFetchVo;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Nyheter_pos",
    "baseprice",
    "bookid",
    "booklength",
    "description",
    "formattype",
    "popularity",
    "productstatus",
    "providerproductid",
    "publisheddate",
    "relatedproductid",
    "subscriptionorder",
    "variant_key",
    "vi_rekommenderar_pos",
    "imageversion"
})
public class VariantAttributes {

    @JsonProperty("Nyheter_pos")
    private String nyheterPos;
    @JsonProperty("baseprice")
    private String baseprice;
    @JsonProperty("bookid")
    private String bookid;
    @JsonProperty("booklength")
    private String booklength;
    @JsonProperty("description")
    private String description;
    @JsonProperty("formattype")
    private String formattype;
    @JsonProperty("popularity")
    private String popularity;
    @JsonProperty("productstatus")
    private String productstatus;
    @JsonProperty("providerproductid")
    private String providerproductid;
    @JsonProperty("publisheddate")
    private String publisheddate;
    @JsonProperty("relatedproductid")
    private String relatedproductid;
    @JsonProperty("subscriptionorder")
    private String subscriptionorder;
    @JsonProperty("variant_key")
    private String variantKey;
    @JsonProperty("vi_rekommenderar_pos")
    private String viRekommenderarPos;
    @JsonProperty("imageversion")
    private String imageversion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Nyheter_pos")
    public String getNyheterPos() {
        return nyheterPos;
    }

    @JsonProperty("Nyheter_pos")
    public void setNyheterPos(String nyheterPos) {
        this.nyheterPos = nyheterPos;
    }

    @JsonProperty("baseprice")
    public String getBaseprice() {
        return baseprice;
    }

    @JsonProperty("baseprice")
    public void setBaseprice(String baseprice) {
        this.baseprice = baseprice;
    }

    @JsonProperty("bookid")
    public String getBookid() {
        return bookid;
    }

    @JsonProperty("bookid")
    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    @JsonProperty("booklength")
    public String getBooklength() {
        return booklength;
    }

    @JsonProperty("booklength")
    public void setBooklength(String booklength) {
        this.booklength = booklength;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("formattype")
    public String getFormattype() {
        return formattype;
    }

    @JsonProperty("formattype")
    public void setFormattype(String formattype) {
        this.formattype = formattype;
    }

    @JsonProperty("popularity")
    public String getPopularity() {
        return popularity;
    }

    @JsonProperty("popularity")
    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    @JsonProperty("productstatus")
    public String getProductstatus() {
        return productstatus;
    }

    @JsonProperty("productstatus")
    public void setProductstatus(String productstatus) {
        this.productstatus = productstatus;
    }

    @JsonProperty("providerproductid")
    public String getProviderproductid() {
        return providerproductid;
    }

    @JsonProperty("providerproductid")
    public void setProviderproductid(String providerproductid) {
        this.providerproductid = providerproductid;
    }

    @JsonProperty("publisheddate")
    public String getPublisheddate() {
        return publisheddate;
    }

    @JsonProperty("publisheddate")
    public void setPublisheddate(String publisheddate) {
        this.publisheddate = publisheddate;
    }

    @JsonProperty("relatedproductid")
    public String getRelatedproductid() {
        return relatedproductid;
    }

    @JsonProperty("relatedproductid")
    public void setRelatedproductid(String relatedproductid) {
        this.relatedproductid = relatedproductid;
    }

    @JsonProperty("subscriptionorder")
    public String getSubscriptionorder() {
        return subscriptionorder;
    }

    @JsonProperty("subscriptionorder")
    public void setSubscriptionorder(String subscriptionorder) {
        this.subscriptionorder = subscriptionorder;
    }

    @JsonProperty("variant_key")
    public String getVariantKey() {
        return variantKey;
    }

    @JsonProperty("variant_key")
    public void setVariantKey(String variantKey) {
        this.variantKey = variantKey;
    }

    @JsonProperty("vi_rekommenderar_pos")
    public String getViRekommenderarPos() {
        return viRekommenderarPos;
    }

    @JsonProperty("vi_rekommenderar_pos")
    public void setViRekommenderarPos(String viRekommenderarPos) {
        this.viRekommenderarPos = viRekommenderarPos;
    }
    @JsonProperty("imageversion")
    public String getImageversion() {
		return imageversion;
	}

    @JsonProperty("imageversion")
	public void setImageversion(String imageversion) {
		this.imageversion = imageversion;
	}
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

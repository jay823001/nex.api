package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

public class AutoCompleteHolder {

	private ArrayList<AutoComplete> autocomplete;

	public ArrayList<AutoComplete> getAutocomplete() {
		return autocomplete;
	}

	public void setAutocomplete(ArrayList<AutoComplete> autocomplete) {
		this.autocomplete = autocomplete;
	}
	
	
}

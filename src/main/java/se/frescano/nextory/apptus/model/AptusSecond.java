package se.frescano.nextory.apptus.model;

import java.util.List;

public class AptusSecond {
	
	private List<SearchHitCount> searchHitCount ;

	private List<SearchHits> searchHits;

	public List<SearchHitCount> getSearchHitCount() {
		return searchHitCount;
	}

	public void setSearchHitCount(List<SearchHitCount> searchHitCount) {
		this.searchHitCount = searchHitCount;
	}

	public List<SearchHits> getSearchHits() {
		return searchHits;
	}

	public void setSearchHits(List<SearchHits> searchHits) {
		this.searchHits = searchHits;
	}

	@Override
	public String toString() {
		return "AptusSecond [searchHitCount=" + (searchHitCount != null?searchHitCount:" ") + ", searchHits=" + (searchHits != null ? searchHits:" ") + "]";
	}
}

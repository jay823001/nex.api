package se.frescano.nextory.apptus.model.ApptusFetchVo;

import java.util.ArrayList;

public class ApptusFinalResponse4 {
	
	ArrayList<ApptusFinalResponse> recommendBasedOnCustomer;

	public ArrayList<ApptusFinalResponse> getRecommendBasedOnCustomer() {
		return recommendBasedOnCustomer;
	}

	public void setRecommendBasedOnCustomer(ArrayList<ApptusFinalResponse> recommendBasedOnCustomer) {
		this.recommendBasedOnCustomer = recommendBasedOnCustomer;
	}

}

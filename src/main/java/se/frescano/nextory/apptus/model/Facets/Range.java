
package se.frescano.nextory.apptus.model.Facets;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "min",
    "max",
    "minSelected",
    "maxSelected"
})
public class Range {

    @JsonProperty("min")
    private String min;
    @JsonProperty("max")
    private String max;
    @JsonProperty("minSelected")
    private String minSelected;
    @JsonProperty("maxSelected")
    private String maxSelected;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("min")
    public String getMin() {
        return min;
    }

    @JsonProperty("min")
    public void setMin(String min) {
        this.min = min;
    }

    public Range withMin(String min) {
        this.min = min;
        return this;
    }

    @JsonProperty("max")
    public String getMax() {
        return max;
    }

    @JsonProperty("max")
    public void setMax(String max) {
        this.max = max;
    }

    public Range withMax(String max) {
        this.max = max;
        return this;
    }

    @JsonProperty("minSelected")
    public String getMinSelected() {
        return minSelected;
    }

    @JsonProperty("minSelected")
    public void setMinSelected(String minSelected) {
        this.minSelected = minSelected;
    }

    public Range withMinSelected(String minSelected) {
        this.minSelected = minSelected;
        return this;
    }

    @JsonProperty("maxSelected")
    public String getMaxSelected() {
        return maxSelected;
    }

    @JsonProperty("maxSelected")
    public void setMaxSelected(String maxSelected) {
        this.maxSelected = maxSelected;
    }

    public Range withMaxSelected(String maxSelected) {
        this.maxSelected = maxSelected;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Range withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}

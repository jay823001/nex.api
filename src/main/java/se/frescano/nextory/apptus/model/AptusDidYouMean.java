package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

public class AptusDidYouMean {
	
	ArrayList<DidYouMean> didYouMean;

	public ArrayList<DidYouMean> getDidYouMean() {
		return didYouMean;
	}

	public void setDidYouMean(ArrayList<DidYouMean> didYouMean) {
		this.didYouMean = didYouMean;
	}

	@Override
	public String toString() {
		return "AptusDidYouMean [didYouMean=" + didYouMean + "]";
	}
	
	

}

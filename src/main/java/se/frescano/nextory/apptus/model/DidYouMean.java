package se.frescano.nextory.apptus.model;

import java.util.ArrayList;

public class DidYouMean {

	ArrayList<Corrections> corrections;

	public ArrayList<Corrections> getCorrections() {
		return corrections;
	}

	public void setCorrections(ArrayList<Corrections> corrections) {
		this.corrections = corrections;
	}

	@Override
	public String toString() {
		return "DidYouMean [corrections=" + corrections + "]";
	}
	
	
}

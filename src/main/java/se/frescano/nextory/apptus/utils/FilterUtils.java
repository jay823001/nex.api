package se.frescano.nextory.apptus.utils;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

/*
 * 
 * Utility class to build the Apptus filter fields, Any field which is used for filtering will use this helper class 
 * for building the filter field.
 * 
 * If the varargs receives one value then it will be set as {field}:'SE' else {field}:['SE','FI']
 * 
 */
public class FilterUtils {

	//List of fields in the Apptus.
	public final static String SUB_ORDER_FIELD="subscriptionorder";
	final static String MARKET_FIELD="markets";
	final static String LANGUAGE_FIELD="language";
	final static String FORMAT_FIELD="formattype";
	final static String PRODUCT_KEY="product_key";
	final static String AUTHOR_FIELD="authors";
	final static String NYHETER_POS="Nyheter";
	final static String SERIES_FILETER="series";
	final static String CATEGORY_ID	  ="categoryids";
	final static String PUBLISHER_ID	  ="publisherid";
	final static String VI_REKOMMENDERAR_POS="vi_rekommenderar";
	final static String prefix ="'";
	final static String suffix ="'";
	final static String	include_country_filter_str = "include_restriction:''{0}'' OR include_restriction:''ALL'' OR include_restriction:''NIL''";
	final static String	exclude_country_filter_str = " NOT exclude_restriction:''{0}'' OR exclude_restriction:''NIL''";
	
	public static String buildSubscriptionFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(SUB_ORDER_FIELD,filters).toString(); 
	}
	
	public static String buildSeriesFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(SERIES_FILETER,filters).toString(); 
	}
	
	public static String buildCategoryId(String countrycode, String ...filters){
		if(filters==null)
			return null;
		return builder(CATEGORY_ID + ( !StringUtils.isBlank( countrycode )?"_" + countrycode : "" ),filters).toString(); 
	}
	
	public static String buildNyheterFilter(String countrycode, String ...filters){
		if(filters==null)
			return null;
		return builder(NYHETER_POS + (!StringUtils.isBlank(countrycode)?"_" + countrycode :"") + "_pos" ,filters).toString(); 
	}
	
	public static String build_Vi_RekommenderFilter(String countrycode, String ...filters){
		if(filters==null)
			return null;
		return builder(VI_REKOMMENDERAR_POS + (!StringUtils.isBlank(countrycode)?"_" + countrycode :"") + "_pos" ,filters).toString();		
	}
	
	public static String buildMarketFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(MARKET_FIELD,filters).toString(); 
	}
	
	public static String buildLanguageFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(LANGUAGE_FIELD,filters).toString(); 
	}
	
	public static String buildProductKeyFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(PRODUCT_KEY,filters).toString(); 
	}
	
	public static String buildAuthorFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(AUTHOR_FIELD,filters).toString(); 
	}
	
	public static String buildFormatFilter(String ...filters){
		if(filters==null)
			return null;
		return builder(FORMAT_FIELD,filters).toString(); 
	}
	
	static String builder(String field , String ...filters ){
		StringBuilder sb = new StringBuilder(field).append(":");
		byte filterslen = (byte) filters.length; 
		if( filterslen > 1){ sb.append('['); };
		for (byte i = 0; i < filterslen; i++) {
			sb.append(prefix).append(filters[i]).append(suffix);
			if(filterslen > i+1)
				sb.append(",");
		}
		if(filterslen > 1){ sb.append(']'); };
		
		return sb.toString();
	}
	
	
	static String builder(String field , char ...filters ){
		StringBuilder sb = new StringBuilder(field).append(":");
		byte filterslen = (byte) filters.length; 
		if( filterslen > 1){ sb.append('['); };
		for (byte i = 0; i < filterslen; i++) {
			sb.append(prefix).append(filters[i]).append(suffix);
			if(filterslen > i+1)
				sb.append(",");
		}
		if(filterslen > 1){ sb.append(']'); };
		
		return sb.toString();
	}
	
	public static String buildCountryIncludeFilter(String country){		
		if(country == null)
			return null;
		else			
			return MessageFormat.format(include_country_filter_str, country); 
	}
	
	public static String buildCountryExcludeFilter(String country){		
		if(country == null)
			return null;
		else			
			return MessageFormat.format(exclude_country_filter_str, country); 
	}
	
	public static String buildPublisherExclusionFilter(Integer[] publisherlist){
		StringBuilder sb = new StringBuilder();		
		byte i =0;
		if(publisherlist!=null){			
			for(Integer publisher : publisherlist){
            	if(i ==0)
            		sb.append(" NOT " + PUBLISHER_ID + ":" + prefix + publisher + suffix);            		 
            	else
            		sb.append(" AND NOT " + PUBLISHER_ID+ ":" + prefix + publisher+ suffix);            		
            	i++;
            }
		}
		return ( i > 0)?sb.toString():null;
	}
	
	public static String buildSortByFilter(String sorton){
		if(!StringUtils.isBlank( sorton ) && sorton.contains("pubdate"))
			return "publisheddate desc";
		else if(!StringUtils.isBlank( sorton ) && sorton.contains("title"))
			return "title asc";	
		else if(!StringUtils.isBlank( sorton ) && sorton.contains("ratingsranking"))
			return "ratingsranking desc";	
		else if(!StringUtils.isBlank( sorton ) && sorton.contains("authors"))
			return "authors asc";	
		else
			return "relevance";				
	}
	
	public static void main(String[] args) {
		//System.out.println(buildSubscriptionFilter('1','2','3'));
		//System.out.println(buildMarketFilter("SE","FI"));	
		System.out.println( buildCountryIncludeFilter("FI"));  
	}
}

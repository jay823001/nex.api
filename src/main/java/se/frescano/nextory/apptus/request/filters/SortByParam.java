package se.frescano.nextory.apptus.request.filters;

public interface SortByParam {

	final String	SORT_BY				= "arg.sort_by";
	
	public void setSortBy(String sort_by);
}

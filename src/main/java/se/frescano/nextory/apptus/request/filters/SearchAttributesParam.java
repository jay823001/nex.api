package se.frescano.nextory.apptus.request.filters;

public interface SearchAttributesParam {

	final String SEARCH_ATTRIBUTES="arg.search_attributes";
	
	public void setSearch_attributes(String search_attributes);
}

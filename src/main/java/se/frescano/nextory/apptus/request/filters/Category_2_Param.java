package se.frescano.nextory.apptus.request.filters;

public interface Category_2_Param {

	final String	CATEGORY_2	= "arg.category_2";
	
	public void setCategory_2(String value);
}

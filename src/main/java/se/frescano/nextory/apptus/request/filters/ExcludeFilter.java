package se.frescano.nextory.apptus.request.filters;

public interface ExcludeFilter {

	final String EXCLUDE_FILTER ="arg.exclude_filter";
	
	public void setExclude_filter(String exclude_filter);
}

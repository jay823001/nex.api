package se.frescano.nextory.apptus.request.filters;

public interface ParentCategoryFilter {

	final String	PARENT_CATEGOEY_FILTER = "arg.parent_category_filter";
	
	public void setParent_category_filter(String value);
}

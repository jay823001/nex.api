package se.frescano.nextory.apptus.request.filters;

public interface CategoryExcludefilter {

	final String	CATEGORY_EXCLUDE_FILTER	= "arg.cat_exclude_filter";
	
	public void setCategory_Exclude_filter(String cat_exclude_filter);
}

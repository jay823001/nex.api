package se.frescano.nextory.apptus.request.filters;

public interface ROOT_CATEGORY {

	final String	ROOT_CATEGORY	= "arg.root_category";
	
	public void setROOT_CATEGORY(String value);
}

package se.frescano.nextory.apptus.request.filters;

public interface Categoryname_3_Param {

	final String	CATEGORY_NAME_3	= "arg.categoryname_3";
	
	public void setCategoryname_3(String value);
}

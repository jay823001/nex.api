package se.frescano.nextory.apptus.request.filters;

public interface AuthorsFilterNew {

	final String	AUTHORS_FILETR 		= "arg.authors_filter_new";
	
	public void setAuthors_filter_New(String value) ;

}

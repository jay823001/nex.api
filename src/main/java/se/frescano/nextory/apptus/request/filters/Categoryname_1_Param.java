package se.frescano.nextory.apptus.request.filters;

public interface Categoryname_1_Param {

	final String	CATEGORY_NAME_1	= "arg.categoryname_1";
	
	public void setCategoryname_1(String value);
}

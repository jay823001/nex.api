package se.frescano.nextory.apptus.request.filters;

public interface LocaleFilter {

	final String	LOCALE 				= "arg.locale";
	
	public void setLocale(String value);
}

package se.frescano.nextory.apptus.request.filters;

public interface RelevanceSort_1Param {

	final String	RELEVANCE_SORT_1		= "arg.relevance_sort_1";
	
	public void setRelevance_sort_1(String relevance_sort_1) ;
}

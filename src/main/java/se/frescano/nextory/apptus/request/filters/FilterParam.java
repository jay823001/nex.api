package se.frescano.nextory.apptus.request.filters;

public interface FilterParam {

	final String	FILTER	= "arg.filter";
	
	public void setFilter(String filter);
}

package se.frescano.nextory.apptus.request.filters;

public interface RelevanceSort_3Param {

	final String	RELEVANCE_SORT_3 		= "arg.relevance_sort_3";
	
	public void setRelevance_sort_3(String relevance_sort_3);
}

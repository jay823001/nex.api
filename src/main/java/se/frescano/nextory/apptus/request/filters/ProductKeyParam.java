package se.frescano.nextory.apptus.request.filters;

public interface ProductKeyParam {

	final String	PRODUCT_KEY			= "arg.product_key";
	
	public void setProductKey(String product_key);
}

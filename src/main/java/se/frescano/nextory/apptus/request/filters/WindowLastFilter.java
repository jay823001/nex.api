package se.frescano.nextory.apptus.request.filters;

public interface WindowLastFilter {

	final String WINDOW_LAST	 = "arg.window_last";
	
	public void setWindow_last(String string);
}

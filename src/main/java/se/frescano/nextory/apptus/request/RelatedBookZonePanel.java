package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.ProductKeyFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SeriesFilter;
import se.frescano.nextory.apptus.request.filters.SortByParam;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class RelatedBookZonePanel extends ApptusPanelBean implements ExcludeFilter, FacetsParam, IncludeFilter, LocaleFilter, 
																	 MarketFilter, ProductKeyFilter, PublisherFilter,
																	 SearchAttributesParam, SearchPhraseParam, SelectedCategoryParam,
																	 SeriesFilter, SortByParam, VariantsPerProductParam{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9089249275854515935L;
	

	//private	static final String	exclude_filter 		= "arg.exclude_filter";
	//private	static final String	facets 				= "arg.facets";
	//private	static final String	include_filter 		= "arg.include_filter";
	//private	static final String	locale 				= "arg.locale";
	//private	static final String	market_filter 		= "arg.market_filter";
	//private	static final String  product_key_filter = "arg.product_key_filter";
	//private	static final String	 publisher_filter 	= "arg.publisher_filter";
	//private	static final String	search_attributes 	= "arg.search_attributes";
	//private	static final String	search_phrase 		= "arg.search_phrase";
	//private	static final String	selected_category 	= "arg.selected_category";
	//private	static final String	series_filter 		= "arg.series_filter";
	//private	static final String	sort_by 	= "arg.sort_by";
	//private	static final String	variants_per_product= "arg.variants_per_product";
	
	@Override
	public void setExclude_filter(String exclude_filter) {
		set(EXCLUDE_FILTER,exclude_filter);
	}

	@Override
	public void setFacets(String facets) {		
		set(FACETS, facets);
	}

	@Override
	public void setInclude_filter(String include_filter) {
		set(INCLUDE_FILTER,include_filter);
	}

	@Override
	public void setLocale(String locale) {		
		set(LOCALE, locale);
	}

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( market_filter ));		
	}
	
	@Override
	public void setProductKeyFilter(String productkey) {
		set(PRODUCT_KEY_FILTER, productkey);		
	}
	
	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}
	
	@Override
	public void setSearch_attributes(String search_attributes) {		
		set(SEARCH_ATTRIBUTES, search_attributes);
	}
	
	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
	
	@Override
	public void setSelected_category(String selected_category) {		
		set(SELECTED_CATEGORY, selected_category);
	}
	
	@Override
	public void setSeries_filter(String series_filter) {
		set(SERIES_FILTER, series_filter);
	}
	
	@Override
	public void setVariants_per_product(String variants_per_product) {	
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}
	
	@Override
	public void setSortBy(String sort_by) {	
		set(SORT_BY, sort_by);
	}
}

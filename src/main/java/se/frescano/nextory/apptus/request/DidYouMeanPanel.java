package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.FilterParam;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;

public class DidYouMeanPanel extends ApptusPanelBean implements SearchPhraseParam, FilterParam, SearchAttributesParam{

	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 4842597998818651642L;

	//private	static	final String	SEARCH_PHRASE = "arg.search_phrase";
	/*private	static	final String	filter = "arg.filter";*/
	//private	static	final String	search_attributes = "arg.search_attributes";	
	
	public DidYouMeanPanel(String search_phrase){
		setSearchPhase(search_phrase);
	}
	
	@Override
	public void setSearchPhase(String string){
		set(SEARCH_PHRASE,string);
	}

	@Override
	public void setFilter(String filter) {
		set(FILTER,filter);
	}

	@Override
	public void setSearch_attributes(String string) {
		set(SEARCH_ATTRIBUTES,string);
	}
		
}

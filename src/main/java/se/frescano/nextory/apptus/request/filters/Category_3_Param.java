package se.frescano.nextory.apptus.request.filters;

public interface Category_3_Param {

	final String	CATEGORY_3	= "arg.category_3";
	
	public void setCategory_3(String value);
}

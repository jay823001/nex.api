package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;

public class DidYouMeanForKidsPanel extends ApptusPanelBean implements SearchPhraseParam{


	private static final long serialVersionUID = 4842597998818651642L;
	
	public DidYouMeanForKidsPanel(String search_phrase){
		this.setSearchPhase(search_phrase);
	}
	
	@Override
	public void setSearchPhase(String string){
		set(SEARCH_PHRASE,string);
	}

}

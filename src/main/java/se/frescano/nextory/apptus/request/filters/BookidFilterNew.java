package se.frescano.nextory.apptus.request.filters;

public interface BookidFilterNew {

	final String	BOOKID_FILTER_NEW 	= "arg.bookid_filter_new";
	
	public void setBookid_filter_new(String value);
}

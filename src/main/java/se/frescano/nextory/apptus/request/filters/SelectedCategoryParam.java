package se.frescano.nextory.apptus.request.filters;

public interface SelectedCategoryParam {

	final String	SELECTED_CATEGORY 	= "arg.selected_category";
	
	public void setSelected_category(String value);
}

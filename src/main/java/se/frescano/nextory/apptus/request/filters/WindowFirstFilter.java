package se.frescano.nextory.apptus.request.filters;

public interface WindowFirstFilter{

	final String WINDOW_FIRST = "arg.window_first";
	
	public void setWindow_first(String string) ;
}

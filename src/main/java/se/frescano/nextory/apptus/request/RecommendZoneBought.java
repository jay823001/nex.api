package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.AuthorsFilter;
import se.frescano.nextory.apptus.request.filters.CategoryFilter;
import se.frescano.nextory.apptus.request.filters.ChildFilter;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.ProductKeyParam;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SeriesFilter;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class RecommendZoneBought extends ApptusPanelBean implements AuthorsFilter, CategoryFilter, ExcludeFilter, 
																	ChildFilter, FormatFilter, IncludeFilter, MarketFilter,
																	ProductKeyParam, PublisherFilter, SeriesFilter, 
																	SubscriptionFilter, VariantsPerProductParam{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8687053937173691570L;
	
	@Override
	public void setAuthors_filter(String value) {
		set(AUTHORS_FILETR, value);
	}
	
	public void setCategory_filter(String category_filter) {		
		set(CATEGORY_FILTER, category_filter);
	}

	@Override
	public void setChild_filter(String child_filter) {		
		set(CHILD_FILTER, child_filter);
	}

	@Override
	public void setExclude_filter(String exclude_filter) {		
		set(EXCLUDE_FILTER, exclude_filter);
	}

	@Override
	public void setFormat_filter(String format_filter) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter(format_filter));		
	}

	@Override
	public void setInclude_filter(String include_filter) {		
		set(INCLUDE_FILTER, include_filter);
	}

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter(market_filter));						
	}

	@Override
	public void setProductKey(String product_key) {
		set(PRODUCT_KEY, product_key);
	}
	
	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSeries_filter(String series_filter) {		
		set(SERIES_FILTER, series_filter);
	}

	@Override
	public void setSubscription_filter(String subscription_filter) {		
		set(SUBSCRIPTION_FILTER, subscription_filter);
	}

	@Override
	public void setVariants_per_product(String variants_per_product) {		
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}
	
}

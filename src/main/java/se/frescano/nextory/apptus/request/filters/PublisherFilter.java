package se.frescano.nextory.apptus.request.filters;

public interface PublisherFilter {

	final String PUBLISHER_FILTER="arg.publisher_filter";
	
	public void setPublisher_filter(String publisher_filter);
}

package se.frescano.nextory.apptus.request.filters;

public interface SeriesFilter {

	final String	SERIES_FILTER 		= "arg.series_filter";
	
	public void setSeries_filter(String series_filter) ;
}

package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.BookidFilterNew;
import se.frescano.nextory.apptus.request.filters.ChildFilter;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.RelatedBookidFilter;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class RecommendBasedOnCustomer extends ApptusPanelBean implements ChildFilter,ExcludeFilter, FacetsParam, FormatFilter,
																		 IncludeFilter, MarketFilter,PublisherFilter,
																		 SelectedCategoryParam, SubscriptionFilter, VariantsPerProductParam,BookidFilterNew
																		 ,RelatedBookidFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6867662713371261727L;

	//private static final String	child_filter 		= "arg.child_filter";
	//private static final String	exclude_filter 		= "arg.exclude_filter";
	//private static final String	facets 				= "arg.facets";
	//private static final String	format_filter 		= "arg.format_filter";
	//private static final String	include_filter 		= "arg.include_filter";
	//private static final String	market_filter 		= "arg.market_filter";
	//private static final String	publisher_filter 	= "arg.publisher_filter";
	//private static final String	selected_category 	= "arg.selected_category";
	//private static final String	subscription_filter = "arg.subscription_filter";
	//private static final String	variants_per_product = "arg.variants_per_product";
		
	@Override
	public void setChild_filter(String child_filter) {		
		set(CHILD_FILTER, child_filter);
	}

	@Override
	public void setExclude_filter(String exclude_filter) {		
		set(EXCLUDE_FILTER, exclude_filter);
	}

	@Override
	public void setFormat_filter(String format_filter) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( format_filter));		
	}

	@Override
	public void setInclude_filter(String include_filter) {		
		set(INCLUDE_FILTER, include_filter);
	}

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( market_filter));		
	}

	@Override
	public void setFacets(String facets) {
		set(FACETS,facets);
	}
	
	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSelected_category(String selected_category) {		
		set(SELECTED_CATEGORY, selected_category);
	}

	@Override
	public void setSubscription_filter(String subscription_filter) {		
		set(SUBSCRIPTION_FILTER, subscription_filter);
	}

	@Override
	public void setVariants_per_product(String variants_per_product) {		
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}

	@Override
	public void setBookid_filter_new(String value) {
		set(BOOKID_FILTER_NEW, value);
	}

	@Override
	public void setRelated_Bookid_filter(String value) {
		set(RELATED_BOOKID_FILTER, value);
		
	}

}

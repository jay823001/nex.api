package se.frescano.nextory.apptus.request.filters;

public interface FacetsParam {

	final String	FACETS 				= "arg.facets";
	
	public void setFacets(String value);
	
}

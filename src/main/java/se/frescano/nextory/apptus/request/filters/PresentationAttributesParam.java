package se.frescano.nextory.apptus.request.filters;

public interface PresentationAttributesParam {

	final String	PRESENTATION_ATTRIBUTES = "arg.presentation_attributes";
	
	public void setPresentation_attributes(String value);
	
}

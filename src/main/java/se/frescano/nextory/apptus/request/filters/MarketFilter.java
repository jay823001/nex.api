package se.frescano.nextory.apptus.request.filters;

public interface MarketFilter {

	final String MARKET_FILTER ="arg.market_filter"; 
	
	public void setMarket_filter(String market_filter);
}

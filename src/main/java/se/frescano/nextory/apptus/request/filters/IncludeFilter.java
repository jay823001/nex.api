package se.frescano.nextory.apptus.request.filters;

public interface IncludeFilter {

	final String INCLUDE_FILTER ="arg.include_filter";
	
	public void setInclude_filter(String include_filter);
}

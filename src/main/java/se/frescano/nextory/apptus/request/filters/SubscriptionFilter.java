package se.frescano.nextory.apptus.request.filters;

public interface SubscriptionFilter {

	final String	SUBSCRIPTION_FILTER = "arg.subscription_filter";
	
	public void setSubscription_filter(String value) ;
}

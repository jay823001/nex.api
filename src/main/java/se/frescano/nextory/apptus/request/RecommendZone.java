package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.AuthorsFilter;
import se.frescano.nextory.apptus.request.filters.CategoryFilter;
import se.frescano.nextory.apptus.request.filters.ChildFilter;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FilterParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.ProductKeyFilter;
import se.frescano.nextory.apptus.request.filters.ProductKeyParam;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SeriesFilter;
import se.frescano.nextory.apptus.request.filters.SortByParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class RecommendZone extends ApptusPanelBean implements AuthorsFilter, ExcludeFilter, FacetsParam, 
															  IncludeFilter, LocaleFilter, MarketFilter,
															  ProductKeyParam, ProductKeyFilter, SeriesFilter, 
															  SortByParam, FilterParam, PublisherFilter,
															  SearchAttributesParam, SearchPhraseParam,CategoryFilter,
															  SelectedCategoryParam, VariantsPerProductParam,ChildFilter,FormatFilter,SubscriptionFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8687053937173691570L;

	//private static final String	exclude_filter 		= "arg.exclude_filter";
	//private static final String	facets		 		= "arg.facets";
	//private static final String	include_filter 		= "arg.include_filter";
	//private static final String	locale 				= "arg.locale";
	//private static final String	market_filter 		= "arg.market_filter";
	//private static final String	product_key			= "arg.product_key";
	//private static final String	product_key_filter	= "arg.product_key_filter";
	//private static final String	publisher_filter 	= "arg.publisher_filter";
	//private static final String	search_attributes 	= "arg.search_attributes";
	//private static final String	search_phrase		= "arg.search_phrase";
	//private static final String	selected_category	= "arg.selected_category";
	/*private static final String	series_filter 		= "arg.series_filter";*/
	/*private static final String	sort_by				= "arg.sort_by";*/
	//private static final String	variants_per_product = "arg.variants_per_product";
	//private static final String	filter	= "arg.filter";
	//private static final String	author_filter		= "arg.author_filter";
	
	@Override
	public void setAuthors_filter(String value) {
		set(AUTHORS_FILETR, value);
	}
	
	@Override
	public void setFilter(String filter) {		
		set(FILTER, filter);
	}
	
	@Override
	public void setProductKey(String product_key) {		
		set(PRODUCT_KEY, product_key);
	}
	
	@Override
	public void setExclude_filter(String exclude_filter) {		
		set(EXCLUDE_FILTER, exclude_filter);
	}
	
	@Override
	public void setFacets(String facets) {		
		set(FACETS, facets);
	}

	@Override
	public void setInclude_filter(String include_filter) {		
		set(INCLUDE_FILTER, include_filter);
	}
	
	@Override
	public void setLocale(String locale) {		
		set(LOCALE, locale);
	}
	
	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter(market_filter));						
	}
	
	@Override
	public void setProductKeyFilter(String product_key_filter) {
		set(PRODUCT_KEY_FILTER, product_key_filter);
	}
	
	@Override
	public void setSearch_attributes(String search_attributes) {
		set(SEARCH_ATTRIBUTES, search_attributes);		
	}
	
	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
	
	@Override
	public void setSelected_category(String value) {
		set(SELECTED_CATEGORY, value);
	}
	
	@Override
	public void setSortBy(String sort_by) {
		set(SORT_BY, sort_by);
	}

	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSeries_filter(String series_filter) {		
		set(SERIES_FILTER, series_filter);
	}

	@Override
	public void setVariants_per_product(String variants_per_product) {		
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}

	@Override
	public void setSubscription_filter(String subscription_filter) {		
		set(SUBSCRIPTION_FILTER, subscription_filter);
	}

	@Override
	public void setFormat_filter(String format_filter) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter(format_filter));		
	}
	
	@Override
	public void setChild_filter(String child_filter) {		
		set(CHILD_FILTER, child_filter);
	}

	@Override
	public void setCategory_filter(String category_filter) {		
		set(CATEGORY_FILTER, category_filter);
	}
	
}

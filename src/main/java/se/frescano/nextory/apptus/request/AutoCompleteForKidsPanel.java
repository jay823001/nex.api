package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.ChildCategoryParam;

public class AutoCompleteForKidsPanel extends AutoCompletePanel implements ChildCategoryParam{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7987311620610810072L;

	@Override
	public void setChild_category_filter(String string) {
		set(CHILD_CATEGORY_FILTER,string);
	}
	
}

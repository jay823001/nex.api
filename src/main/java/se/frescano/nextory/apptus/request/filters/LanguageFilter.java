package se.frescano.nextory.apptus.request.filters;

public interface LanguageFilter {

	final String LANGUAGE_FILTER ="arg.language_filter"; 
	
	public void setLanguage_filter(String language_filter);
}

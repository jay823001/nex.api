package se.frescano.nextory.apptus.request.filters;

public interface Category_1_Param {

	final String	CATEGORY_1	= "arg.category_1";
	
	public void setCategory_1(String value);
}

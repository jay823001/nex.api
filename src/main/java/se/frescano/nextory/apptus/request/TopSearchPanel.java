package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class TopSearchPanel extends ApptusPanelBean implements ExcludeFilter, FacetsParam, IncludeFilter, LocaleFilter, 
MarketFilter, FormatFilter, PublisherFilter, SubscriptionFilter{
	
	private static final long serialVersionUID = -7987311620610810072L;

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( market_filter ));		
		
	}

	@Override
	public void setLocale(String value) {
		set(LOCALE, value);
		
	}

	@Override
	public void setInclude_filter(String include_filter) {
		set(INCLUDE_FILTER,include_filter);
		
	}

	@Override
	public void setFacets(String value) {
		set(FACETS, value);
		
	}

	@Override
	public void setExclude_filter(String exclude_filter) {
		set(EXCLUDE_FILTER,exclude_filter);
		
	}

	@Override
	public void setSubscription_filter(String value) {
		set(SUBSCRIPTION_FILTER, value);
		
	}

	@Override
	public void setPublisher_filter(String publisher_filter) {
		set(PUBLISHER_FILTER, publisher_filter);
		
	}

	@Override
	public void setFormat_filter(String value) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( value ));			
		
	}

	
	
	
}

package se.frescano.nextory.apptus.request.filters;

public interface LanguageFilter_2 {

	final String LANGUAGE_FILTER_2 ="arg.language_filter_2"; 
	
	public void setLanguage_filter_2(String language_filter);
}

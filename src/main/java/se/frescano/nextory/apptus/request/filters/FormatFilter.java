package se.frescano.nextory.apptus.request.filters;

public interface FormatFilter {

	final String	FORMAT_FILTER 		= "arg.format_filter";
	
	public void setFormat_filter(String value) ;
}

package se.frescano.nextory.apptus.request.filters;

public interface SearchPhraseParam {

	final String	SEARCH_PHRASE = "arg.search_phrase";
	
	public void setSearchPhase(String string);
}

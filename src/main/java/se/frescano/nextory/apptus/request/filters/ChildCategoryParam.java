package se.frescano.nextory.apptus.request.filters;

public interface ChildCategoryParam {

	final String CHILD_CATEGORY_FILTER ="arg.child_category";
	
	public void setChild_category_filter(String string);
}

package se.frescano.nextory.apptus.request.filters;

public interface Subscription_1Filter {

	final String SUBSCRIPTION_FILTER_1 = "arg.subscription_filter_1";
	
	public void setSubscription_filter_1(String string) ;
}

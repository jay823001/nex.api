package se.frescano.nextory.apptus.request.filters;

public interface RelatedBookidFilter {

	final String	RELATED_BOOKID_FILTER 	= "arg.relatedbook_filter";
	
	public void setRelated_Bookid_filter(String value);
}

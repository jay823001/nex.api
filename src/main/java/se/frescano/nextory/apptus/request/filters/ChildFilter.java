package se.frescano.nextory.apptus.request.filters;

public interface ChildFilter {

	final String	CHILD_FILTER 		= "arg.child_filter";
	
	public void setChild_filter(String value);
}

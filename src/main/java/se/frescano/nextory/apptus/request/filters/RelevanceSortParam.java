package se.frescano.nextory.apptus.request.filters;

public interface RelevanceSortParam {

	final String	RELEVANCE_SORT 		= "arg.relevance_sort";
	
	public void setRelevance_sort(String value);
}

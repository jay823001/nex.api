package se.frescano.nextory.apptus.request.filters;

public interface ProductKeyFilter {

	final String	PRODUCT_KEY_FILTER	= "arg.product_key_filter";
	
	public void setProductKeyFilter(String product_key_filter) ;
}

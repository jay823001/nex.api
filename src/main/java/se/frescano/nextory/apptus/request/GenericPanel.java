package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.AuthorsFilterNew;
import se.frescano.nextory.apptus.request.filters.BookidFilterNew;
import se.frescano.nextory.apptus.request.filters.ChildFilter;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LanguageFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.ParentCategoryFilter;
import se.frescano.nextory.apptus.request.filters.PresentationAttributesParam;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.RelevanceSortParam;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class GenericPanel extends ApptusPanelBean implements AuthorsFilterNew, BookidFilterNew, ChildFilter, 
															ExcludeFilter, FacetsParam, FormatFilter, 
															IncludeFilter, LocaleFilter, MarketFilter, LanguageFilter,
															ParentCategoryFilter, PresentationAttributesParam, PublisherFilter,
															RelevanceSortParam, SearchAttributesParam, SearchPhraseParam,
															SelectedCategoryParam, SubscriptionFilter, VariantsPerProductParam{

	private static final long serialVersionUID = 2973998317290305232L;
	

	@Override
	public void setInclude_filter(String value) {
		set(INCLUDE_FILTER,value);
	}

	@Override
	public void setLocale(String value) {		
		set(LOCALE, value);
	}

	@Override
	public void setMarket_filter(String value) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( value ));		
	}

	@Override
	public void setLanguage_filter(String value) {
		set(LANGUAGE_FILTER, value);		
	}
	@Override
	public void setParent_category_filter(String value) {		
		set(PARENT_CATEGOEY_FILTER, value);
	}

	@Override
	public void setPresentation_attributes(String value) {		
		set(PRESENTATION_ATTRIBUTES, value);
	}

	@Override
	public void setPublisher_filter(String value) {		
		set(PUBLISHER_FILTER, value);
	}

	@Override
	public void setRelevance_sort(String value) {		
		set(RELEVANCE_SORT, value);
	}

	@Override
	public void setSearch_attributes(String value) {		
		set(SEARCH_ATTRIBUTES, value);
	}
	
	@Override
	public void setSelected_category(String value) {		
		set(SELECTED_CATEGORY, value);
	}

	@Override
	public void setSubscription_filter(String value) {		
		set(SUBSCRIPTION_FILTER, value);
	}

	@Override
	public void setVariants_per_product(String value) {	
		set(VARIANTS_PER_PRODUCT, value);
	}

	@Override
	public void setAuthors_filter_New(String value) {
		set(AUTHORS_FILETR, value);
	}

	@Override
	public void setBookid_filter_new(String value) {
		set(BOOKID_FILTER_NEW, value);
	}

	@Override
	public void setChild_filter(String value) {
		set(CHILD_FILTER, value);
	}

	@Override
	public void setExclude_filter(String value) {
		set(EXCLUDE_FILTER,value);
	}

	@Override
	public void setFacets(String value) {
		set(FACETS, value);
	}
	
	@Override
	public void setFormat_filter(String value) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( value ));			
	}

	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
}

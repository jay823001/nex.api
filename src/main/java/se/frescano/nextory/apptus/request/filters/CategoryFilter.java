package se.frescano.nextory.apptus.request.filters;

public interface CategoryFilter {
	
	final String	CATEGORY_FILTER 	= "arg.category_filter";
	
	public void setCategory_filter(String category_filter);

}

package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.ParentCategoryFilter;
import se.frescano.nextory.apptus.request.filters.PresentationAttributesParam;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SortByParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.Subscription_1Filter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class SearchPanel extends ApptusPanelBean implements ExcludeFilter, FacetsParam, FormatFilter,
															IncludeFilter,LocaleFilter, MarketFilter,
															ParentCategoryFilter,PresentationAttributesParam, PublisherFilter,
															SearchAttributesParam, SearchPhraseParam,SelectedCategoryParam,
														 	SortByParam,SubscriptionFilter, VariantsPerProductParam,
														 	Subscription_1Filter{
	
	//String panelname;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5405868843959237610L;

	
	public SearchPanel(String window_first,String window_last) {
		this.setWindow_first( window_first );
		this.setWindow_last( window_last );
	}

	@Override
	public void setExclude_filter(String string) {
		this.set(EXCLUDE_FILTER,string);
	}

	@Override
	public void setFacets(String string) {
		this.set(FACETS,string);		
	}

	@Override
	public void setFormat_filter(String string) {
		this.set(FORMAT_FILTER, FilterUtils.buildFormatFilter(string));			
	}

	@Override
	public void setInclude_filter(String string) {
		this.set(INCLUDE_FILTER,string);
	}

	@Override
	public void setLocale(String string) {
		this.set(LOCALE,string);
	}

	@Override
	public void setMarket_filter(String string) {
		this.set(MARKET_FILTER, FilterUtils.buildMarketFilter(string));
	}

	@Override
	public void setParent_category_filter(String string) {
		this.set(PARENT_CATEGOEY_FILTER,string);
	}

	@Override
	public void setPublisher_filter(String string) {
		this.set(PUBLISHER_FILTER,string);
	}

	@Override
	public void setSelected_category(String string) {
		this.set(SELECTED_CATEGORY, string);
	}
	
	@Override
	public void setSubscription_filter(String string) {
		this.set(SUBSCRIPTION_FILTER, string );
	}

	@Override
	public void setSubscription_filter_1(String string) {
		this.set(SUBSCRIPTION_FILTER_1, string );
	}

	@Override
	public void setVariants_per_product(String string) {
		this.set(VARIANTS_PER_PRODUCT, string);
	}

	@Override
	public void setSortBy(String sort_by) {
		set(SORT_BY, sort_by);
	}

	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}

	@Override
	public void setSearch_attributes(String search_attributes) {
		set(SEARCH_ATTRIBUTES, search_attributes);
	}

	@Override
	public void setPresentation_attributes(String value) {
		set(PRESENTATION_ATTRIBUTES,value);
	}
		

}

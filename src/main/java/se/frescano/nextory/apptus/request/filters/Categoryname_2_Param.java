package se.frescano.nextory.apptus.request.filters;

public interface Categoryname_2_Param {

	final String	CATEGORY_NAME_2	= "arg.categoryname_2";
	
	public void setCategoryname_2(String value);
}

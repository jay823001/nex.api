package se.frescano.nextory.apptus.request;

import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPrefixParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class AutoCompletePanel extends ApptusPanelBean implements ExcludeFilter, IncludeFilter, 
																  MarketFilter, PublisherFilter,
																  SearchAttributesParam, SearchPrefixParam{
	
	private static final long serialVersionUID = -7987311620610810072L;

	@Override
	public void setExclude_filter(String exclude_filter) {
		set(AutoCompletePanel.EXCLUDE_FILTER,exclude_filter);
	}
	
	@Override
	public void setInclude_filter(String include_filter) {
		set(AutoCompletePanel.INCLUDE_FILTER,include_filter);
	}
	
	@Override
	public void setMarket_filter(String market_filter) {
		set(AutoCompletePanel.MARKET_FILTER, FilterUtils.buildMarketFilter(market_filter));		
	}

	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(AutoCompletePanel.PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSearch_attributes(String search_attributes) {		
		set(AutoCompletePanel.SEARCH_ATTRIBUTES, search_attributes);
	}
	
	@Override
	public void setSearchPrefix(String search_prifix) {
		set(SEARCH_PREFIX, search_prifix);	
		/*try {
			if( !StringUtils.isBlank( search_prifix ))
				set(AutoCompletePanel.SEARCH_PREFIX, URLEncoder.encode(search_prifix,CONSTANTS.UTF_8_CHARSET));	
			else
				set(AutoCompletePanel.SEARCH_PREFIX,null);
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}*/
	}	
	
	
}

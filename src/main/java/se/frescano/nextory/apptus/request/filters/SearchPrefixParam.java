package se.frescano.nextory.apptus.request.filters;

public interface SearchPrefixParam {

	final String SEARCH_PREFIX="arg.search_prefix";
	
	public void setSearchPrefix(String search_prifix);
}

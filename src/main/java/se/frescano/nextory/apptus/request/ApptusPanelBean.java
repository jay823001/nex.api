package se.frescano.nextory.apptus.request;

import org.springframework.util.LinkedMultiValueMap;

import se.frescano.nextory.apptus.request.filters.WindowFirstFilter;
import se.frescano.nextory.apptus.request.filters.WindowLastFilter;

@SuppressWarnings("serial")
public abstract class ApptusPanelBean extends LinkedMultiValueMap<String,String> implements WindowFirstFilter, WindowLastFilter{
	
	final String prefix ="'";
	final String suffix ="'";
	
	private static final String MARKET	   	 = "market";
	private static final String SESSIONKEY 	 = "sessionKey";
	private static final String CUSTOMERKEY  = "customerKey";
	
	public void setMarket(String string) {
		set(MARKET, string );
	}
	
	public void setCustomerKey(String string) {
		set(CUSTOMERKEY, string);
	}
	
	public void setSessionKey(String string){
		set(SESSIONKEY, string);
	}
	
	public void setAsString(String key, String value) {
		value = prefix+value+suffix;
		super.set(key, value);
	}
	
	@Override
	public void setWindow_first(String string) {
		set(WINDOW_FIRST, string);
	}
	
	@Override
	public void setWindow_last(String string) {
		set(WINDOW_LAST, string);
	}
}

package se.frescano.nextory.apptus.request.filters;

public interface RelevanceSort_2Param {

	final String	RELEVANCE_SORT_2 		= "arg.relevance_sort_2";
	
	public void setRelevance_sort_2(String relevance_sort_2);
}

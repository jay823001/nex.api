package se.frescano.nextory.apptus.request.filters;

public interface VariantsPerProductParam {

	final String	VARIANTS_PER_PRODUCT	= "arg.variants_per_product";
	
	public void setVariants_per_product(String value);
}

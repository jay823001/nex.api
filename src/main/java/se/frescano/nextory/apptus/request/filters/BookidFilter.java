package se.frescano.nextory.apptus.request.filters;

public interface BookidFilter {

	final String	BOOKID_FILTER		= "arg.bookid_filter";
	
	public void setBookid_filter(String bookid_filter);
}

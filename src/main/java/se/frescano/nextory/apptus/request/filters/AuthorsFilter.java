package se.frescano.nextory.apptus.request.filters;

public interface AuthorsFilter {

	final String	AUTHORS_FILETR 		= "arg.authors_filter";
	
	public void setAuthors_filter(String value) ;

}

package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class RestrictedFor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1574435026214132010L;

	private ArrayList<String> countrycodes;
	
	private String flag;

	public ArrayList<String> getCountrycodes() {
		return countrycodes;
	}

	public void setCountrycodes(ArrayList<String> countrycodes) {
		this.countrycodes = countrycodes;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
		
}


package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;
import java.util.Date;

import org.mongodb.morphia.annotations.Transient;

public class FixedPriceData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9061925349104884204L;

	@Transient
	Integer productid;
	
	double fixedprice;
	
	String status;
	
	Date startdate;
	
	Date enddate;
	
	Date createddate;
	
	boolean pubprice;

	
	public Integer getProductid() {
		return productid;
	}

	public void setProductid(Integer productid) {
		this.productid = productid;
	}

	public double getFixedprice() {
		return fixedprice;
	}

	public void setFixedprice(double fixedprice) {
		this.fixedprice = fixedprice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public boolean ispubprice() {
		return pubprice;
	}

	public void setpubprice(boolean ispubprice) {
		this.pubprice = ispubprice;
	}
	
	public int hashcode()
	{
		int hash=0;
		hash = this.createddate.hashCode();
		return hash;
		
	}

}

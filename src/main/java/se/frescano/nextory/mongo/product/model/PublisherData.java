package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class PublisherData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6548733521500403292L;
	private int publisherid;
	private String publishername;
	private int distributorid;
	private String distributorname;
	public RestrictedFor restrictedfor;
	
	public RestrictedFor getRestrictedfor() {
		return restrictedfor;
	}
	public void setRestrictedfor(RestrictedFor restrictedfor) {
		this.restrictedfor = restrictedfor;
	}
	public int getPublisherid() {
		return publisherid;
	}
	public void setPublisherid(int publisherid) {
		this.publisherid = publisherid;
	}
	
	public String getPublishername() {
		return publishername;
	}
	
	public void setPublishername(String publishername) {
		this.publishername = publishername;
	}
	
	public int getDistributorid() {
		return distributorid;
	}
	
	public void setDistributorid(int distributorid) {
		this.distributorid = distributorid;
	}
	
	public String getDistributorname() {
		return distributorname;
	}
	
	public void setDistributorname(String distributorname) {
		this.distributorname = distributorname;
	}


}

package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value="product",noClassnameStored = true)
public class ProductData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6291052148476586256L;

	@Id
	public Integer bookid;
	
	/*public Integer provider_productid;*/
	
	public String isbn;
	
	public String title;
	
	public String subtitle;
	
	public String language;
	
	public String description;
	
	public String adminoverride;
	
	@Embedded 
	public PublisherData publisher;
	
	@Embedded 
	public BookType booktype;
	
	public String productstatus;
	
	public String imagename;
	
	public String coverimage;
	
	public Integer booklength;
	
	public String filedownloadsource;
	
	public Date filedownloaddate;
	
	public String fileredownloadsource;
	
	public Date fileredownloaddate;
	
	public String statusatpublisher;
	
	public Date originalpublicationdate;
	
	public Date publisheddate;
	
	public Integer filesize;
	
	public double netprice;
	
	public Date netpriceupdatedon;
	
	public Date createddate;
	
	public Date updateddate;
	
	@Embedded
	public List<Contributors> contributors;
	
	@Embedded
	public List<Relations> relations;
	
	public List<Integer> categoryids;
	
	/*@Embedded
	public List<Categories> categories;*/
	
	/*@Embedded
	public List<Formats> formats;*/
	/*
	@Embedded
	public Pricelog pricelog;*/
	
	@Embedded
	public List<CampaignPriceData> campaignpricelist; 
	
	@Embedded
	public List<FixedPriceData> fixedpricelist;
	
	/*@Embedded
	public PriceCalculation pricecalculation;*/
	
	private String allowedfor;
	
	private String subprice;
	
	public Integer aptusid;
	
	@Embedded
	private Ratings ratings;
	
	private Integer imageversion;
	
	private String imageheight;
	
	private String imagewidth;
	
	private ArrayList<String> markets;	
	 
	public String getImageheight() {
		return imageheight;
	}

	public void setImageheight(String imageheight) {
		this.imageheight = imageheight;
	}

	public String getImagewidth() {
		return imagewidth;
	}

	public void setImagewidth(String imagewidth) {
		this.imagewidth = imagewidth;
	}

	public Integer getImageversion() {
		return imageversion;
	}

	public void setImageversion(Integer imageversion) {
		this.imageversion = imageversion;
	}

	public Integer getAptusid() {
		return aptusid;
	}

	public void setAptusid(Integer aptusid) {
		this.aptusid = aptusid;
	}

	public String getSubprice() {
		return subprice;
	}

	public void setSubprice(String subprice) {
		this.subprice = subprice;
	}
	
	public String getAllowedfor() {
		return allowedfor;
	}

	public void setAllowedfor(String allowedfor) {
		this.allowedfor = allowedfor;
	}
/*
	public PriceCalculation getPricecalculation() {
		return pricecalculation;
	}

	public void setPricecalculation(PriceCalculation pricecalculation) {
		this.pricecalculation = pricecalculation;
	}*/

	public Integer getBookid() {
		return bookid;
	}

	public void setBookid(Integer bookid) {
		this.bookid = bookid;
	}

/*	public Integer getProvider_productid() {
		return provider_productid;
	}

	public void setProvider_productid(Integer provider_productid) {
		this.provider_productid = provider_productid;
	}*/

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PublisherData getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherData publisher) {
		this.publisher = publisher;
	}

	public BookType getBooktype() {
		return booktype;
	}

	public void setBooktype(BookType booktype) {
		this.booktype = booktype;
	}

	public String getProductstatus() {
		return productstatus;
	}

	public void setProductstatus(String productStatus) {
		this.productstatus = productStatus;
	}

	public String getImagename() {
		return imagename;
	}

	public void setImagename(String imagename) {
		this.imagename = imagename;
	}

	public String getCoverimage() {
		return coverimage;
	}

	public void setCoverimage(String coverimage) {
		this.coverimage = coverimage;
	}

	public Integer getBooklength() {
		return booklength;
	}

	public void setBooklength(Integer booklength) {
		this.booklength = booklength;
	}

	public String getFiledownloadsource() {
		return filedownloadsource;
	}

	public void setFiledownloadsource(String filedownloadsource) {
		this.filedownloadsource = filedownloadsource;
	}

	public Date getFiledownloaddate() {
		return filedownloaddate;
	}

	public void setFiledownloaddate(Date filedownloaddate) {
		this.filedownloaddate = filedownloaddate;
	}

	public String getFileredownloadsource() {
		return fileredownloadsource;
	}

	public void setFileredownloadsource(String fileredownloadsource) {
		this.fileredownloadsource = fileredownloadsource;
	}

	public Date getFileredownloaddate() {
		return fileredownloaddate;
	}

	public void setFileredownloaddate(Date fileredownloaddate) {
		this.fileredownloaddate = fileredownloaddate;
	}

	public String getStatusatpublisher() {
		return statusatpublisher;
	}

	public void setStatusatpublisher(String statusatpublisher) {
		this.statusatpublisher = statusatpublisher;
	}

	public Date getOriginalpublicationdate() {
		return originalpublicationdate;
	}

	public void setOriginalpublicationdate(Date originalpublicationdate) {
		this.originalpublicationdate = originalpublicationdate;
	}

	public Date getPublisheddate() {
		return publisheddate;
	}

	public void setPublisheddate(Date publishedDate) {
		this.publisheddate = publishedDate;
	}

	public Integer getFilesize() {
		return filesize;
	}

	public void setFilesize(Integer filesize) {
		this.filesize = filesize;
	}

	public double getNetprice() {
		return netprice;
	}

	public void setNetprice(double netprice) {
		this.netprice = netprice;
	}

	public Date getNetpriceupdatedon() {
		return netpriceupdatedon;
	}

	public void setNetpriceupdatedon(Date netpriceupdatedon) {
		this.netpriceupdatedon = netpriceupdatedon;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}

	public List<Contributors> getContributors() {
		return contributors;
	}

	public void setContributors(List<Contributors> contributors) {
		this.contributors = contributors;
	}

	public List<Relations> getRelations() {
		return relations;
	}

	public void setRelations(List<Relations> relations) {
		this.relations = relations;
	}

	/*public List<Formats> getFormats() {
		return formats;
	}

	public void setFormats(List<Formats> formats) {
		this.formats = formats;
	}*/

	/*public List<Categories> getCategories() {
		return categories;
	}

	public void setCategories(List<Categories> categories) {
		this.categories = categories;
	}*/

/*
	public Pricelog getPricelog() {
		return pricelog;
	}

	public void setPricelog(Pricelog pricelog) {
		this.pricelog = pricelog;
	}*/

	public List<CampaignPriceData> getCampaignpricelist() {
		return campaignpricelist;
	}

	public void setCampaignpricelist(List<CampaignPriceData> campaignpricelist) {
		this.campaignpricelist = campaignpricelist;
	}

	public List<FixedPriceData> getFixedpricelist() {
		return fixedpricelist;
	}

	public void setFixedpricelist(List<FixedPriceData> fixedpricelist) {
		this.fixedpricelist = fixedpricelist;
	}

	public String getAdminoverride() {
		return adminoverride;
	}

	public void setAdminoverride(String adminoverride) {
		this.adminoverride = adminoverride;
	}

	public Document toMongoDocument() {
		
		 Document obj=new Document();
		  obj.put("price",this.netprice);
		  obj.put("isbn",this.isbn);
		  obj.put("allowed_for",this.allowedfor!= null ?  this.allowedfor: null);
		  obj.put("price_updatedon",this.netpriceupdatedon != null ? new Date(this.netpriceupdatedon.getTime()) : null);
		  obj.put("formatname",this.booktype.getBook_type());
		  obj.put("publisher",publisher.getPublishername());
		  obj.put("publisherid",publisher.getPublisherid());
		  obj.put("productStatus",this.productstatus);
		  
		  return  obj;
	}

	public Ratings getRatings() {
		return ratings;
	}

	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}

	public ArrayList<String> getMarkets() {
		return markets;
	}

	public void setMarkets(ArrayList<String> markets) {
		this.markets = markets;
	}

	public List<Integer> getCategoryids() {
		return categoryids;
	}

	public void setCategoryids(List<Integer> categoryids) {
		this.categoryids = categoryids;
	}	
	
	
}
package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Relations implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1649363751554939335L;
	public int relationtype;
	public String identifiertype;
	public String identifierid;
	public String relationtypename;
	public String createdby;
	public Integer cbookid;
	public Integer volume;
	
	public int getRelationtype() {
		return relationtype;
	}
	public void setRelationtype(int relationtype) {
		this.relationtype = relationtype;
	}
	public String getIdentifiertype() {
		return identifiertype;
	}
	public void setIdentifiertype(String identifiertype) {
		this.identifiertype = identifiertype;
	}
	public String getIdentifierid() {
		return identifierid;
	}
	public void setIdentifierid(String identifierid) {
		this.identifierid = identifierid;
	}
	public String getRelationtypename() {
		return relationtypename;
	}
	public void setRelationtypename(String relationtypename) {
		this.relationtypename = relationtypename;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Integer getCbookid() {
		return cbookid;
	}
	public void setCbookid(Integer cbookid) {
		this.cbookid = cbookid;
	}
	public Integer getVolume() {
		return volume;
	}
	public void setVolume(Integer volume) {
		this.volume = volume;
	}
	
	
	
}

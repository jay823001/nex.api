package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class BookType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1057991997367670555L;

	private int book_type_id;
	
	private String book_type;

	public int getBook_type_id() {
		return book_type_id;
	}

	public void setBook_type_id(int book_type_id) {
		this.book_type_id = book_type_id;
	}

	public String getBook_type() {
		return book_type;
	}

	public void setBook_type(String book_type) {
		this.book_type = book_type;
	}
	
	

}

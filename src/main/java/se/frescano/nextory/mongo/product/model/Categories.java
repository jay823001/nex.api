package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Categories implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3422929280991414342L;
	public int categoryid;
	public String categoryname;
	public String slugname;
	public String categorylevel;
	
	
	public String getCategorylevel() {
		return categorylevel;
	}
	public void setCategorylevel(String categorylevel) {
		this.categorylevel = categorylevel;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	public String getSlugname() {
		return slugname;
	}
	public void setSlugname(String slugname) {
		this.slugname = slugname;
	}
	
	
	
}

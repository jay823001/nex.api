package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Ratings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1098517733037678778L;
	private Double avgrating;
	private Integer totalratings;
	private Double ratingsranking;
	@JsonIgnore
	private Date updateddate;
	
	public Ratings() {
	 
	}
	
	public Ratings(Double avgrating, Integer totalratings, Double ratingsranking) {
		super();
		this.avgrating = avgrating;
		this.totalratings = totalratings;
		this.ratingsranking = ratingsranking;
		this.updateddate = new Date();
	}

	public Double getAvgrating() {
		return avgrating;
	}

	public void setAvgrating(Double avgrating) {
		this.avgrating = avgrating;
	}

	public Integer getTotalratings() {
		return totalratings;
	}

	public void setTotalratings(Integer totalratings) {
		this.totalratings = totalratings;
	}

	public Date getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}
	
	public Double getRatingsranking() {
		return ratingsranking;
	}

	public void setRatingsranking(Double ratingsranking) {
		this.ratingsranking = ratingsranking;
	}
	 

}

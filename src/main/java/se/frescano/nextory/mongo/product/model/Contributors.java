package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;
import java.util.Date;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Contributors implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6239496287226915391L;
	public String role;
	public String firstname;
	public String lastname;
	public Date lastupdatedon;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getLastupdatedon() {
		return lastupdatedon;
	}
	public void setLastupdatedon(Date lastupdatedon) {
		this.lastupdatedon = lastupdatedon;
	}
	
	
	
}

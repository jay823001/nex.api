package se.frescano.nextory.mongo.product.model;

import java.io.Serializable;
import java.util.Date;

public class CampaignPriceData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6294545377315006650L;

	double campaignprice;
	
	Date campaignstartdate;
	
	Date campaignenddate;
	
	String status;
	
	String description;
	
	Date createddate;
	
	Date updateddate;

	public double getCampaignprice() {
		return campaignprice;
	}

	public void setCampaignprice(double campaignprice) {
		this.campaignprice = campaignprice;
	}

	public Date getCampaignstartdate() {
		return campaignstartdate;
	}

	public void setCampaignstartdate(Date campaignstartdate) {
		this.campaignstartdate = campaignstartdate;
	}

	public Date getCampaignenddate() {
		return campaignenddate;
	}

	public void setCampaignenddate(Date campaignenddate) {
		this.campaignenddate = campaignenddate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}
	
	public int hashcode()
	{
		int hash=0;
		hash = this.createddate.hashCode();
		return hash;
		
	}
}

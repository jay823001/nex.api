package se.frescano.nextory.web.apptus.request;

import se.frescano.nextory.apptus.request.ApptusPanelBean;
import se.frescano.nextory.apptus.request.filters.BookidFilter;
import se.frescano.nextory.apptus.request.filters.BookidFilterNew;
import se.frescano.nextory.apptus.request.filters.CategoryExcludefilter;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LanguageFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.ParentCategoryFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.RelevanceSortParam;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class BockerPanelRequest extends ApptusPanelBean implements BookidFilter, BookidFilterNew, CategoryExcludefilter,
																	ExcludeFilter, FacetsParam, FormatFilter,
																	IncludeFilter, LocaleFilter,MarketFilter,
																	ParentCategoryFilter, PublisherFilter,SearchAttributesParam,
																	SearchPhraseParam,SelectedCategoryParam, SubscriptionFilter,
																	VariantsPerProductParam, RelevanceSortParam,LanguageFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2604598604925139983L;
		
	@Override
	public void setRelevance_sort(String relevance_sort) {
		set(RELEVANCE_SORT, relevance_sort);		
	}
	
	@Override
	public void setBookid_filter(String bookid_filter) {
		set( BOOKID_FILTER, bookid_filter);		
	}
	
	@Override
	public void setBookid_filter_new(String bookid_filter_new) {
		set(BOOKID_FILTER_NEW, bookid_filter_new);		
	}
	
	@Override
	public void setCategory_Exclude_filter(String cat_exclude_filter) {
		set(CATEGORY_EXCLUDE_FILTER, cat_exclude_filter);		
	}

	@Override
	public void setExclude_filter(String exclude_filter) {
		set(EXCLUDE_FILTER,exclude_filter);
	}

	@Override
	public void setFacets(String facets) {		
		set(FACETS, facets);
	}

	@Override
	public void setFormat_filter(String format_filter) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( format_filter ));			
	}

	@Override
	public void setInclude_filter(String include_filter) {
		set(INCLUDE_FILTER,include_filter);
	}

	@Override
	public void setLocale(String locale) {		
		set(LOCALE, locale);
	}

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( market_filter ));		
	}

	@Override
	public void setParent_category_filter(String parent_category_filter) {		
		set(PARENT_CATEGOEY_FILTER, parent_category_filter);
	}

	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSearch_attributes(String search_attributes) {		
		set(SEARCH_ATTRIBUTES, search_attributes);
	}

	@Override
	public void setSelected_category(String selected_category) {		
		set(SELECTED_CATEGORY, selected_category);
	}

	@Override
	public void setSubscription_filter(String subscription_filter) {		
		set(SUBSCRIPTION_FILTER, subscription_filter);
	}

	@Override
	public void setVariants_per_product(String variants_per_product) {	
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}

	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
	
	@Override
	public void setLanguage_filter(String language_filter){
		set(LANGUAGE_FILTER, language_filter);
	}
}

package se.frescano.nextory.web.apptus.request;

import se.frescano.nextory.apptus.request.ApptusPanelBean;
import se.frescano.nextory.apptus.request.filters.Category_1_Param;
import se.frescano.nextory.apptus.request.filters.Category_2_Param;
import se.frescano.nextory.apptus.request.filters.Category_3_Param;
import se.frescano.nextory.apptus.request.filters.Categoryname_1_Param;
import se.frescano.nextory.apptus.request.filters.Categoryname_2_Param;
import se.frescano.nextory.apptus.request.filters.Categoryname_3_Param;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LanguageFilter_1;
import se.frescano.nextory.apptus.request.filters.LanguageFilter_2;
import se.frescano.nextory.apptus.request.filters.LanguageFilter_3;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.RelevanceSortParam;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class CategoryZonePanelRequest extends ApptusPanelBean implements Category_1_Param, Categoryname_1_Param, Category_2_Param,
																		 Category_3_Param, Categoryname_2_Param, Categoryname_3_Param,
																		 ExcludeFilter, FacetsParam,FormatFilter,
																		 IncludeFilter, LocaleFilter,MarketFilter,
																		 LanguageFilter_1,LanguageFilter_2,LanguageFilter_3,
																		 PublisherFilter,RelevanceSortParam,SearchAttributesParam,
																		 SearchPhraseParam, SelectedCategoryParam, SubscriptionFilter,
																		 VariantsPerProductParam{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2816261979104062277L;
	
	
	@Override
	public void setLanguage_filter_1(String language_filter) {
		set(LANGUAGE_FILTER_1,language_filter);
	}
	
	@Override
	public void setLanguage_filter_2(String language_filter) {
		set(LANGUAGE_FILTER_2,language_filter);
	}
	
	@Override
	public void setLanguage_filter_3(String language_filter) {
		set(LANGUAGE_FILTER_3,language_filter);
	}
	
	@Override
	public void setCategory_1(String value) {
		set(CATEGORY_1,value);
	}
	
	@Override
	public void setCategoryname_1(String value) {
		set(CATEGORY_NAME_1,value);
	}
	
	@Override
	public void setCategory_2(String value) {
		set(CATEGORY_2,value);
	}
	
	@Override
	public void setCategoryname_2(String value) {
		set(CATEGORY_NAME_2,value);
	}	
	
	@Override
	public void setCategory_3(String value) {
		set(CATEGORY_3,value);
	}
	
	@Override
	public void setCategoryname_3(String value) {
		set(CATEGORY_NAME_3,value);
	}
	
	@Override
	public void setExclude_filter(String value) {
		set(EXCLUDE_FILTER,value);
	}

	@Override
	public void setFacets(String value) {		
		set(FACETS, value);
	}

	@Override
	public void setFormat_filter(String value) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( value ));			
	}

	@Override
	public void setInclude_filter(String value) {
		set(INCLUDE_FILTER,value);
	}

	@Override
	public void setLocale(String value) {		
		set(LOCALE, value);
	}

	@Override
	public void setMarket_filter(String value) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( value ));		
	}

	@Override
	public void setPublisher_filter(String value) {		
		set(PUBLISHER_FILTER, value);
	}

	@Override
	public void setSearch_attributes(String value) {		
		set(SEARCH_ATTRIBUTES, value);
	}

	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
	
	@Override
	public void setSelected_category(String value) {		
		set(SELECTED_CATEGORY, value);
	}

	@Override
	public void setSubscription_filter(String value) {		
		set(SUBSCRIPTION_FILTER, value);
	}

	@Override
	public void setVariants_per_product(String value) {	
		set(VARIANTS_PER_PRODUCT, value);
	}
	
	@Override
	public void setRelevance_sort(String value) {
		set(RELEVANCE_SORT, value);		
	}
	
}

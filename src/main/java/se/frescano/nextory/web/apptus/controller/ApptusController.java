package se.frescano.nextory.web.apptus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.apptus.service.ApptusService;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.web.api.request.BockerSpecificRequest;
import se.frescano.nextory.web.api.request.CategorySpecificRequest;
import se.frescano.nextory.web.api.request.CategoryViewAllRequest;
import se.frescano.nextory.web.api.request.SearchBooksRequestBean;
import se.frescano.nextory.web.api.request.SearchSuggestRequestBean;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.service.ProductInfoService;
import se.frescano.nextory.web.api.spring.method.resolver.WebAPIUser;
import se.frescano.nextory.web.api.util.WEBAPIURLCONSTANTS;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;
import se.frescano.nextory.web.api.validator.BockerSpecificValidator;
import se.frescano.nextory.web.api.validator.CategorySpecificValidator;
import se.frescano.nextory.web.api.validator.CategoryViewAllValidator;
import se.frescano.nextory.web.api.validator.SearchBooksRequestBeanValidator;
import se.frescano.nextory.web.api.validator.SearchSuggestRequestBeanValidator;


@RestController
@RequestMapping("/api/web/{version:4.0|5.0}")
public class ApptusController {
	
	@Autowired
	private	ApptusService	apptusService;
	
	@Autowired
	private CategorySpecificValidator categorySpecificValidator;
	
	@Autowired
	private BockerSpecificValidator bockerSpecificValidator;
	
	@Autowired
	private CategoryViewAllValidator categoryViewAllValidator;
	
	@Autowired
	private	ProductInfoService	productinfoservice;
	
	@Autowired
	CacheBridgeCategory cacheBridgeCategory;
	

	@Autowired
	private SearchBooksRequestBeanValidator searchRequestValidator;
	
	@Autowired
	private SearchSuggestRequestBeanValidator suggestRequestValidator;

	
	private static final Logger logger = LoggerFactory.getLogger(ApptusController.class);
		
	
	@RequestMapping(value =  { WEBAPIURLCONSTANTS.BOCKER_FIRST_SCREEN}, method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public GenericAPIResponse getBockerDetails(@WebAPIUser User user, BockerSpecificRequest bockerSpecificRequest,
			@PathVariable("version") Double version, HttpServletRequest request,
			BindingResult result){	
		if(version!=null && version>=CONSTANTS.WEB_API_VERSION_3_0 && bockerSpecificRequest!=null)
			bockerSpecificRequest.setAuthkey((String) request.getAttribute(CONSTANTS.WEB_AUTH_KEY));
		
		bockerSpecificValidator.validate(bockerSpecificRequest, result); 
		if(result.hasErrors()){  
			return new GenericAPIResponse(HttpStatus.BAD_REQUEST,
										  WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION,  result.getFieldErrors());
		}
		GenericAPIResponse details = new GenericAPIResponse();
		try{
			String  countrycode = (String) request.getAttribute(CONSTANTS.COUNTRY_CODE);
			//to diplay home page toplist categories
			details = apptusService.callAppropriateUrl("bocker",bockerSpecificRequest.getSubscription(),
					bockerSpecificRequest.getWindowsize(),countrycode,user);
		}catch (APIException e){
			logger.error("get bocker Details failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}catch (Exception e){
			logger.error("get bocker Details failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}		
		return details;
	 }
	
	@RequestMapping(value = { WEBAPIURLCONSTANTS.CATEGORY_VIEW_ALL_LEVEL3}, method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public GenericAPIResponse getViewAllCategories( @WebAPIUser User user, CategoryViewAllRequest categoryViewAllRequest,
			@PathVariable("version") Double version, HttpServletRequest request,
			BindingResult result)
		{
		/*String slugname = "";
		int formatno =0;*/
		if(version!=null && version>=CONSTANTS.WEB_API_VERSION_3_0 && categoryViewAllRequest!=null)
			categoryViewAllRequest.setAuthkey((String) request.getAttribute(CONSTANTS.WEB_AUTH_KEY));
		
		categoryViewAllValidator.validate(categoryViewAllRequest, result); 
		if(result.hasErrors()){  
			return new GenericAPIResponse(HttpStatus.BAD_REQUEST,
										  WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION,  result.getFieldErrors());
		}			
		GenericAPIResponse details = new GenericAPIResponse();
		try{						
			String  countrycode = (String) request.getAttribute(CONSTANTS.COUNTRY_CODE);
			//fetch toplist viewall for main categories Level 2
			if((categoryViewAllRequest.getSubcatslug().contains("top") || categoryViewAllRequest.getSubcatslug().contains("nyheter"))
					&& !categoryViewAllRequest.getCatslug().equalsIgnoreCase("bocker")
					&& !categoryViewAllRequest.getCatslug().equalsIgnoreCase("e-bocker")
					&& !categoryViewAllRequest.getCatslug().equalsIgnoreCase("ljudbocker"))
			{
				
				details = apptusService.callViewAllforTopList(categoryViewAllRequest.getCatslug(), 
						categoryViewAllRequest.getFormat(), categoryViewAllRequest.getSubcatslug(), 
						categoryViewAllRequest.getSorton(), categoryViewAllRequest.getPagenumber(), categoryViewAllRequest.getWindowsize(), 
						categoryViewAllRequest.getSubscription(),countrycode,user);
			}
			
			
			//TOP TIER TOP LIST View All level 1
			else if(categoryViewAllRequest.getCatslug().equalsIgnoreCase("bocker")
					|| categoryViewAllRequest.getCatslug().equalsIgnoreCase("e-bocker")
					|| categoryViewAllRequest.getCatslug().equalsIgnoreCase("ljudbocker"))
			{
			details = apptusService.callAppropriateUrlForViewAll(categoryViewAllRequest.getCatslug(), 
						categoryViewAllRequest.getFormat(), categoryViewAllRequest.getSubcatslug(), 
						categoryViewAllRequest.getSorton(), categoryViewAllRequest.getPagenumber(), categoryViewAllRequest.getWindowsize(), 
						categoryViewAllRequest.getSubscription(),countrycode,user);
			}
						
			//subcategories viewall of main categoryies  level 3
			else{
				details = apptusService.callAppropriateUrlForViewAllSubCategories(categoryViewAllRequest.getFormat(), categoryViewAllRequest.getSubcatslug(), 
								categoryViewAllRequest.getSorton(), categoryViewAllRequest.getPagenumber(), categoryViewAllRequest.getWindowsize(), 
								categoryViewAllRequest.getSubscription(),countrycode,user);
			}			
		}catch (APIException e){
			logger.error("get categoryViewAll failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);	
		}catch (Exception e){
			logger.error("get categoryViewAll failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}		
		return details;
	 }
			
	@RequestMapping(value={ WEBAPIURLCONSTANTS.SEACRH},  method = RequestMethod.GET,produces = "application/json")
	public GenericAPIResponse searchBooks(@WebAPIUser User user,@Valid SearchBooksRequestBean	requestBean,
			@PathVariable("version") Double version, HttpServletRequest request,
			 BindingResult results){
		if(logger.isTraceEnabled())logger.trace("searchBooks....request...." + requestBean.toString());
		if(version!=null && version>=CONSTANTS.WEB_API_VERSION_3_0 && requestBean!=null)
			requestBean.setAuthkey((String) request.getAttribute(CONSTANTS.WEB_AUTH_KEY));
		
		searchRequestValidator.validate(requestBean, results); 
		if(results.hasErrors()){  
			return new GenericAPIResponse(HttpStatus.BAD_REQUEST,
										  WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION, 
										  results.getFieldErrors());
		} 
		GenericAPIResponse		response	= new GenericAPIResponse();				
		try{			
			String  countrycode = (String) request.getAttribute(CONSTANTS.COUNTRY_CODE);
			response = productinfoservice.searchBooks( requestBean,countrycode,user);			
		}catch(Exception e){
			logger.error("SearchBooks failed. " + e.getMessage() );			
			response	=	new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, 
													WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}				
		return response;
	}
	
	@RequestMapping(value={ WEBAPIURLCONSTANTS.AUTOCOMPLETE},  method = RequestMethod.GET,produces = "application/json")
	public  GenericAPIResponse searchSuggests(@WebAPIUser User user,@Valid SearchSuggestRequestBean	requestBean,
			@PathVariable("version") Double version, BindingResult results,HttpServletRequest request){
		if(logger.isTraceEnabled())logger.trace("searchSuggests....request...." + requestBean.toString());
		if(version!=null && version>=CONSTANTS.WEB_API_VERSION_3_0 && requestBean!=null)
			requestBean.setAuthkey((String) request.getAttribute(CONSTANTS.WEB_AUTH_KEY));
		
		suggestRequestValidator.validate(requestBean, results); 
		if(results.hasErrors()){  
			return new GenericAPIResponse(HttpStatus.BAD_REQUEST,
										  WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION,  results.getFieldErrors());
		} 
		GenericAPIResponse		response	= new GenericAPIResponse();	
		//User user = null;
		try{ 			
			String  countrycode = (String) request.getAttribute(CONSTANTS.COUNTRY_CODE);			
			response = productinfoservice.searchSuggestedBooks( requestBean,countrycode,user);			
		}catch(Exception e){
			logger.error("SearchSuggests failed. " + e.getMessage() );
			response	=	new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, 
													WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}			
		return response;
	}
	
	@RequestMapping(value = { WEBAPIURLCONSTANTS.MAIN_CATEGORY_LEVEL2_SCREEN}, method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public GenericAPIResponse getParentCategoriesDetailsPageNumberWise( @WebAPIUser User user, CategorySpecificRequest categorySpecificRequest,
			@PathVariable("version") Double version, HttpServletRequest request,
			BindingResult result)
		{
		/*String slugname = "";
		int formatno =0;*/
		if(version!=null && version>=CONSTANTS.WEB_API_VERSION_3_0 && categorySpecificRequest!=null)
			categorySpecificRequest.setAuthkey((String) request.getAttribute(CONSTANTS.WEB_AUTH_KEY));
		
		categorySpecificValidator.validate(categorySpecificRequest, result); 
		if(result.hasErrors()){  
			return new GenericAPIResponse(HttpStatus.BAD_REQUEST,
										  WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION,  result.getFieldErrors());
		}
		GenericAPIResponse details = new GenericAPIResponse();
		
		try{			
			String  countrycode = (String) request.getAttribute(CONSTANTS.COUNTRY_CODE);
			//fetch toplist of main categories 
			if(categorySpecificRequest.getPagenumber() != null && categorySpecificRequest.getPagenumber() == 1)
			{
				details = apptusService.callAppropriateUrlForParentCategoriesPageNumber(categorySpecificRequest.getCategoryslug(),
						categorySpecificRequest.getFormat(),
						categorySpecificRequest.getSubscription(), categorySpecificRequest.getWindowsize(),countrycode,user);
			}			
			//fetch subcategory list of main categories
			else
			{
				details = apptusService.callAppropriateUrlForParentCategoriesNewPageNumber(categorySpecificRequest.getCategoryslug()
						,categorySpecificRequest.getFormat(), categorySpecificRequest.getSubscription(), 
						categorySpecificRequest.getPagenumber(),categorySpecificRequest.getWindowsize(),countrycode,user);
			}
			
		}catch (APIException e){
			logger.error("get categorySpecificLoad failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);	
		}catch (Exception e){
			logger.error("get categorySpecificLoad failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}		
		return details;
	 }
	
	@RequestMapping(value = { WEBAPIURLCONSTANTS.LEVEL1_SCREEN_BOCKER_EBOCKER_ABOCKER}, method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public GenericAPIResponse getMainCategoriesDetailsPageNumberWise( @WebAPIUser User user, CategorySpecificRequest categorySpecificRequest,
			@PathVariable("version") Double version, HttpServletRequest request,
			BindingResult result)
		{		
		if(result.hasErrors()){  
			return new GenericAPIResponse(HttpStatus.BAD_REQUEST,
										  WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION,  result.getFieldErrors());
		}
		GenericAPIResponse details = new GenericAPIResponse();		
		try{			
			String  countrycode = (String) request.getAttribute( CONSTANTS.COUNTRY_CODE );
			if(categorySpecificRequest.getPagenumber() != null && categorySpecificRequest.getPagenumber().intValue() == 1)
			{
				//fetch toplist and categories bocker 
				if(categorySpecificRequest.getFormat() == null || categorySpecificRequest.getFormat().intValue() == -1)				
					details = apptusService.callAppropriateUrl("bocker",categorySpecificRequest.getSubscription(),
							categorySpecificRequest.getWindowsize(),countrycode,user);				
				//fetch toplist and categories Ebocker 
				else if(categorySpecificRequest.getFormat() != null && categorySpecificRequest.getFormat().intValue() == 1)
				
					details = apptusService.callAppropriateUrl("Ebocker",categorySpecificRequest.getSubscription(),
							categorySpecificRequest.getWindowsize(),countrycode,user);
				//fetch toplist and categories Abocker 
				else if(categorySpecificRequest.getFormat() != null && categorySpecificRequest.getFormat().intValue() == 2)				
					details = apptusService.callAppropriateUrl("Abocker",categorySpecificRequest.getSubscription(),
							categorySpecificRequest.getWindowsize(),countrycode,user);				
			}
				
			if(categorySpecificRequest.getPagenumber() != null && categorySpecificRequest.getPagenumber().intValue() > 1)
			{
				details = apptusService.callAppropriateUrlForMainCategoriesPageNumber(categorySpecificRequest.getCategoryslug()
						,categorySpecificRequest.getFormat(), categorySpecificRequest.getSubscription(), 
						categorySpecificRequest.getPagenumber(),categorySpecificRequest.getWindowsize(),countrycode,user);
			}
			
		}catch (APIException e){
			logger.error("get MaincategorySpecificLoadStep failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);	
		}catch (Exception e){
			logger.error("get MaincategorySpecificLoadStep failed. ", e);
			details = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}		
		return details;
	 }
		
}

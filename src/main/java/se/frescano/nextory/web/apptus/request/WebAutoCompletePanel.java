package se.frescano.nextory.web.apptus.request;

import se.frescano.nextory.apptus.request.ApptusPanelBean;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPrefixParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class WebAutoCompletePanel extends ApptusPanelBean implements ExcludeFilter, IncludeFilter, MarketFilter,
																	 PublisherFilter,SearchAttributesParam, SearchPrefixParam{

	private static final long serialVersionUID = -7987311620610810072L;
		
	@Override
	public void setExclude_filter(String exclude_filter) {
		set(EXCLUDE_FILTER,exclude_filter);
	}
	
	@Override
	public void setInclude_filter(String include_filter) {
		set(INCLUDE_FILTER,include_filter);
	}
	
	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter(market_filter));		
	}

	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSearch_attributes(String search_attributes) {		
		set(SEARCH_ATTRIBUTES, search_attributes);
	}
	
	@Override
	public void setSearchPrefix(String search_prifix) {
		set(SEARCH_PREFIX, search_prifix);	
	}
		
}

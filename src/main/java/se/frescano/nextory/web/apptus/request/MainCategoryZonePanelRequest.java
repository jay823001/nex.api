package se.frescano.nextory.web.apptus.request;

import se.frescano.nextory.apptus.request.ApptusPanelBean;
import se.frescano.nextory.apptus.request.filters.Category_1_Param;
import se.frescano.nextory.apptus.request.filters.Category_2_Param;
import se.frescano.nextory.apptus.request.filters.Category_3_Param;
import se.frescano.nextory.apptus.request.filters.Categoryname_1_Param;
import se.frescano.nextory.apptus.request.filters.Categoryname_2_Param;
import se.frescano.nextory.apptus.request.filters.Categoryname_3_Param;
import se.frescano.nextory.apptus.request.filters.DpromoteFilter1;
import se.frescano.nextory.apptus.request.filters.DpromoteFilter2;
import se.frescano.nextory.apptus.request.filters.DpromoteFilter3;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LanguageFilter_1;
import se.frescano.nextory.apptus.request.filters.LanguageFilter_2;
import se.frescano.nextory.apptus.request.filters.LanguageFilter_3;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.RelevanceSort_1Param;
import se.frescano.nextory.apptus.request.filters.RelevanceSort_2Param;
import se.frescano.nextory.apptus.request.filters.RelevanceSort_3Param;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SubscriptionFilter;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class MainCategoryZonePanelRequest extends ApptusPanelBean implements Category_1_Param, Categoryname_1_Param, Category_2_Param,
																			 Category_3_Param, Categoryname_2_Param, Categoryname_3_Param,
																			 ExcludeFilter, FacetsParam,FormatFilter,
																			 IncludeFilter,LocaleFilter,MarketFilter,
																			 LanguageFilter_1,LanguageFilter_2,LanguageFilter_3,
																			 DpromoteFilter1,DpromoteFilter2,DpromoteFilter3,
																			 PublisherFilter, RelevanceSort_1Param,RelevanceSort_2Param, RelevanceSort_3Param,
																			 SearchAttributesParam, SearchPhraseParam, SelectedCategoryParam, SubscriptionFilter, VariantsPerProductParam{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2816261979104062277L;
	
	
	@Override
	public void setLanguage_filter_1(String language_filter) {
		set(LANGUAGE_FILTER_1,language_filter);
	}
	
	@Override
	public void setLanguage_filter_2(String language_filter) {
		set(LANGUAGE_FILTER_2,language_filter);
	}
	
	@Override
	public void setLanguage_filter_3(String language_filter) {
		set(LANGUAGE_FILTER_3,language_filter);
	}
	
	@Override
	public void setCategory_1(String category_1) {
		set(CATEGORY_1,category_1);
	}
	@Override
	public void setCategoryname_1(String categoryname_1) {
		set(CATEGORY_NAME_1,categoryname_1);
	}
	
	@Override
	public void setCategory_2(String category_2) {
		set(CATEGORY_2,category_2);
	}
	
	@Override
	public void setCategoryname_2(String categoryname_2) {
		set(CATEGORY_NAME_2,categoryname_2);
	}	
	
	@Override
	public void setCategory_3(String category_3) {
		set(CATEGORY_3,category_3);
	}
	
	@Override
	public void setCategoryname_3(String categoryname_3) {
		set(CATEGORY_NAME_3,categoryname_3);
	}
	
	@Override
	public void setExclude_filter(String exclude_filter) {
		set(EXCLUDE_FILTER,exclude_filter);
	}

	@Override
	public void setFacets(String facets) {		
		set(FACETS, facets);
	}

	@Override
	public void setFormat_filter(String format_filter) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( format_filter ));			
	}

	@Override
	public void setInclude_filter(String include_filter) {
		set(INCLUDE_FILTER,include_filter);
	}

	@Override
	public void setLocale(String locale) {		
		set(LOCALE, locale);
	}

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( market_filter ));		
	}

	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}

	@Override
	public void setSearch_attributes(String search_attributes) {		
		set(SEARCH_ATTRIBUTES, search_attributes);
	}

	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
	
	@Override
	public void setSelected_category(String selected_category) {		
		set(SELECTED_CATEGORY, selected_category);
	}

	@Override
	public void setSubscription_filter(String subscription_filter) {		
		set(SUBSCRIPTION_FILTER, subscription_filter);
	}

	@Override
	public void setVariants_per_product(String variants_per_product) {	
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}
	
	@Override
	public void setRelevance_sort_1(String relevance_sort_1) {
		set(RELEVANCE_SORT_1, relevance_sort_1);		
	}
	
	@Override
	public void setRelevance_sort_2(String relevance_sort_2) {
		set(RELEVANCE_SORT_2, relevance_sort_2);		
	}
	
	@Override
	public void setRelevance_sort_3(String relevance_sort_3) {
		set(RELEVANCE_SORT_3, relevance_sort_3);		
	}
	
	@Override
	public void setDpromote_filter_1(String dpromote_filter){
		set(Dpromote_FILTER_1, dpromote_filter);	
	}
	
	@Override
	public void setDpromote_filter_2(String dpromote_filter){
		set(Dpromote_FILTER_2, dpromote_filter);	
	}
	
	@Override
	public void setDpromote_filter_3(String dpromote_filter){
		set(Dpromote_FILTER_3, dpromote_filter);	
	}
}

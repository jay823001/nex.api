package se.frescano.nextory.web.apptus.request;

import se.frescano.nextory.apptus.request.ApptusPanelBean;
import se.frescano.nextory.apptus.request.filters.ExcludeFilter;
import se.frescano.nextory.apptus.request.filters.FacetsParam;
import se.frescano.nextory.apptus.request.filters.FormatFilter;
import se.frescano.nextory.apptus.request.filters.IncludeFilter;
import se.frescano.nextory.apptus.request.filters.LocaleFilter;
import se.frescano.nextory.apptus.request.filters.MarketFilter;
import se.frescano.nextory.apptus.request.filters.PresentationAttributesParam;
import se.frescano.nextory.apptus.request.filters.PublisherFilter;
import se.frescano.nextory.apptus.request.filters.SearchAttributesParam;
import se.frescano.nextory.apptus.request.filters.SearchPhraseParam;
import se.frescano.nextory.apptus.request.filters.SelectedCategoryParam;
import se.frescano.nextory.apptus.request.filters.SortByParam;
import se.frescano.nextory.apptus.request.filters.VariantsPerProductParam;
import se.frescano.nextory.apptus.utils.FilterUtils;

public class WebSearchZonePanelRequest extends ApptusPanelBean implements ExcludeFilter, FacetsParam, FormatFilter,
																		 IncludeFilter, LocaleFilter, MarketFilter,
																		 PresentationAttributesParam,PublisherFilter,SearchAttributesParam,
																		 SearchPhraseParam,SelectedCategoryParam,SortByParam,VariantsPerProductParam{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8304509667094175941L;
	
	
	@Override
	public void setExclude_filter(String exclude_filter) {
		set(EXCLUDE_FILTER,exclude_filter);
	}

	@Override
	public void setFacets(String facets) {		
		set(FACETS, facets);
	}

	@Override
	public void setFormat_filter(String format_filter) {
		set(FORMAT_FILTER, FilterUtils.buildFormatFilter( format_filter ));			
	}

	@Override
	public void setInclude_filter(String include_filter) {
		set(INCLUDE_FILTER,include_filter);
	}

	@Override
	public void setLocale(String locale) {		
		set(LOCALE, locale);
	}

	@Override
	public void setMarket_filter(String market_filter) {
		set(MARKET_FILTER, FilterUtils.buildMarketFilter( market_filter ));		
	}
	
	@Override
	public void setPresentation_attributes(String presentation_attributes) {
		set(PRESENTATION_ATTRIBUTES, presentation_attributes);		
	}
	
	@Override
	public void setSearch_attributes(String search_attributes) {		
		set(SEARCH_ATTRIBUTES, search_attributes);
	}

	@Override
	public void setSearchPhase(String string) {
		set(SEARCH_PHRASE, string);
	}
	
	@Override
	public void setSelected_category(String selected_category) {		
		set(SELECTED_CATEGORY, selected_category);
	}
	
	@Override
	public void setSortBy(String sort_by) {
		set(SORT_BY, sort_by);
	}
	
	@Override
	public void setVariants_per_product(String variants_per_product) {	
		set(VARIANTS_PER_PRODUCT, variants_per_product);
	}
	
	@Override
	public void setPublisher_filter(String publisher_filter) {		
		set(PUBLISHER_FILTER, publisher_filter);
	}
}

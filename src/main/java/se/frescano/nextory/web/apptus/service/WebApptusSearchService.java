package se.frescano.nextory.web.apptus.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import reactor.core.publisher.Mono;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.AptusSecond;
import se.frescano.nextory.web.apptus.request.WebSearchZonePanelRequest;

@Service
public class WebApptusSearchService {
	
	private Logger logger = LoggerFactory.getLogger(WebApptusSearchService.class);
	
	@Autowired
	private	WebClient webclient;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder 	searchUriBuilder = null;
		 	
	//@PostConstruct
	public UriComponentsBuilder init(){
		return searchUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path( applicationpropconfig.getSearchZoneUrl() );	
	}
	public AptusSecond search(WebSearchZonePanelRequest panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();
		logger.info("APPTUS WEB SEARCH REQUEST--->" + uriComponents.toUri());	 				
		try{
			Mono<AptusSecond> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AptusSecond.class);
			return result.block();
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);
			/*e.printStackTrace();*/
		}
		//result1.subscribe(TestSubscribe::handleResponse);
		return null;
	}

}

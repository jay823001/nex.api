package se.frescano.nextory.web.apptus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.request.APIController;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.web.api.response.GenericAPIResponse;


@RestController
@RequestMapping(value={"/api/internal/{version:1.0}",APIJSONURLCONSTANTS.INT_API_V2_BASE})
public class ApptusInternalController extends APIController{
		
	@Autowired
	private ApptusApiService apptusApiService;	
	
	@RequestMapping(value = "/preview/list/details", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getBookForBookGroupPreview(@RequestParam("bookgroupid") String bookgroupid,
				@RequestParam("pagenumber") String pagenumber,@RequestParam("rows") Integer rows,
				@RequestParam("subscriptiontype") Integer subtype,@RequestParam("countrycode") String countrycode) 
	{
		GenericAPIResponse details = new GenericAPIResponse();		
		try {
			details.setData(apptusApiService.getBooksForBookGroupForNestPreview(bookgroupid, pagenumber, rows, subtype,countrycode) );
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return getJsonMappingfilter(details, "productdetails_filter", APIController.json_booksofbookgroupfilter_exclude_gt_6_2);
	}

}

package se.frescano.nextory.web.apptus.service;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import reactor.core.publisher.Mono;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.AutoCompleteHolder;
import se.frescano.nextory.apptus.model.Completions;
import se.frescano.nextory.web.apptus.request.WebAutoCompletePanel;

@Service
public class WebApptusAutoCompleteService {

private Logger logger = LoggerFactory.getLogger(WebApptusAutoCompleteService.class);
	
	@Autowired
	private	WebClient webclient;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder uriBuilder = null;
		
	//@PostConstruct
	public UriComponentsBuilder init(){
		return uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getGenericUrl() + "autocompletesuggestionweb");
	}
	
	public ArrayList<Completions> search(WebAutoCompletePanel panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS AUTOCOMPLETE REQUEST--->" + uriComponents.toUri());	 				
		try{
			Mono<AutoCompleteHolder> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AutoCompleteHolder.class);	
			AutoCompleteHolder	responseresult = result.block();
			if( responseresult != null && responseresult.getAutocomplete() != null &&
					responseresult.getAutocomplete().get(0) != null && responseresult.getAutocomplete().get(0) != null )
				return responseresult.getAutocomplete().get(0).getCompletions();
			return new ArrayList<Completions>(0);
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUriString(), e);			
		}
		return new ArrayList<Completions>(0);
	}
}

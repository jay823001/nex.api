package se.frescano.nextory.web.apptus.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import reactor.core.publisher.Mono;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse;
import se.frescano.nextory.apptus.model.SubCategoryFetchVo.Subcategoryzone;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.apptus.request.BockerPanelRequest;
import se.frescano.nextory.web.apptus.request.CategoryZonePanelRequest;
import se.frescano.nextory.web.apptus.request.MainCategoryZonePanelRequest;

@Service
public class WebApptusPanelService {

	private Logger logger = LoggerFactory.getLogger(WebApptusPanelService.class);
	
	@Autowired
	private	WebClient webclient;
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	//private UriComponentsBuilder uriBuilder = null;
	
	public Map<String, List<ApptusFinalResponse>> webApptusBookGroupPanelService(String url, BockerPanelRequest panel){				
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() );
		UriComponents uriComponents = uriBuilder.path(url).replaceQueryParams(panel).build();

		logger.info("APPTUS BOOKGROUPS REQUEST--->" + uriComponents.toUri());	 				
		try{
			Mono<String> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(String.class);						
			return Book2GoUtil.objectMapper.readValue(result.block(),new TypeReference<Map<String, List<ApptusFinalResponse>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching book groups results for URL -->" + uriComponents.toUriString(), e);
			/*e.printStackTrace();*/
		}
		//result1.subscribe(TestSubscribe::handleResponse);
		return null;
	}
	
	public Map<String, List<Subcategoryzone>> webApptusCategoriesGroup(String url, CategoryZonePanelRequest panel){				
		
		//UriComponents uriComponents = uriBuilder.path(url).replaceQueryParams(panel).build();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() );
		UriComponents uriComponents = uriBuilder.path(url).replaceQueryParams(panel).build();
		
		logger.info("APPTUS CATEGORIES REQUEST--->" + uriComponents.toUri());	 				
		try{
			Mono<String> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(String.class);						
			return Book2GoUtil.objectMapper.readValue(result.block(),new TypeReference<Map<String, List<Subcategoryzone>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching categories book groups results for URL -->" + uriComponents.toUriString(), e);
			/*e.printStackTrace();*/
		}
		//result1.subscribe(TestSubscribe::handleResponse);
		return null;
	}
	
	public Map<String, List<Subcategoryzone>> webApptusMainCategoriesGroup(String url, MainCategoryZonePanelRequest panel){				
		
		//UriComponents uriComponents = uriBuilder.path(url).replaceQueryParams(panel).build();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() );
		UriComponents uriComponents = uriBuilder.path(url).replaceQueryParams(panel).build();
		
		logger.info("APPTUS Main Category REQUEST--->" + uriComponents.toUri());	 				
		try{
			Mono<String> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(String.class);						
			return Book2GoUtil.objectMapper.readValue(result.block(),new TypeReference<Map<String, List<Subcategoryzone>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching categories book groups results for URL -->" + uriComponents.toUriString(), e);
			/*e.printStackTrace();*/
		}
		//result1.subscribe(TestSubscribe::handleResponse);
		return null;
	}
}

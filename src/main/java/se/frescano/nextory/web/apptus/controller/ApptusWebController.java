package se.frescano.nextory.web.apptus.controller;

import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.hazelcast.map.impl.querycache.event.sequence.SubscriberSequencerProvider;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserToken;
import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.ApptusBookGroupsEnum;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS.API_TYPE;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.exception.TokenNotValidException;
import se.frescano.nextory.app.product.request.BooksForBookGroupRequestBean;
import se.frescano.nextory.app.product.request.MainJsonRequestBean;
import se.frescano.nextory.app.product.request.SearchJsonApptusRequestBean;
import se.frescano.nextory.app.product.response.AdvancedSearchWrapper;
import se.frescano.nextory.app.product.response.BooksForBookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.product.response.SearchWithFacetsApptusResponseWrapper;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.BookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.response.JsonWrapper;
import se.frescano.nextory.apptus.service.ApptusService;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.service.SubscriptionTypeVO;
import se.frescano.nextory.util.BookGroupViewEnum;
import se.frescano.nextory.web.api.controller.WebAPIBaseController;
import se.frescano.nextory.web.api.exception.WEBAPIValidationException;
import se.frescano.nextory.web.api.request.WebApiRequestBean;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.service.ProductInfoService;
import se.frescano.nextory.web.api.spring.method.resolver.WebAPIUser;
import se.frescano.nextory.web.api.util.WEBAPIURLCONSTANTS;
import se.frescano.nextory.web.api.validator.BockerSpecificValidator;
import se.frescano.nextory.web.api.validator.BookGroupsRequestValidator;
import se.frescano.nextory.web.api.validator.BooksBookGroupRequestValidator;
import se.frescano.nextory.web.api.validator.CategorySpecificValidator;
import se.frescano.nextory.web.api.validator.CategoryViewAllValidator;
import se.frescano.nextory.web.api.validator.SearchBooksRequestBeanValidator;
import se.frescano.nextory.web.api.validator.SearchSuggestRequestBeanValidator;
import springfox.documentation.annotations.ApiIgnore;

@Api(value = "Product Web APIs ", description = "New Product APIs for WEB ")
@RestController
public class ApptusWebController extends WebAPIBaseController{
	
	/*@Autowired
	private	ApptusService	apptusService;*/
	
	@Autowired
	private	ApptusApiService	apptusApiService;
	
	/*@Autowired
	private CategorySpecificValidator categorySpecificValidator;
	
	@Autowired
	private BockerSpecificValidator bockerSpecificValidator;
	
	@Autowired
	private CategoryViewAllValidator categoryViewAllValidator;
	
	@Autowired
	private	ProductInfoService	productinfoservice;*/
	

	@Autowired
	private SearchBooksRequestBeanValidator searchRequestValidator;
	
	@Autowired
	private SearchSuggestRequestBeanValidator suggestRequestValidator;
	
	@Autowired
	private BookGroupsRequestValidator bookGroupsRequestValidator;

	@Autowired
	private BooksBookGroupRequestValidator booksRequestValidator;
	
	
	private static final Logger logger = LoggerFactory.getLogger(ApptusController.class);
	
	@ApiOperation(value = "Get bookgroups API for WEB", nickname = "groupsGet", notes = "All the top level bookgroups having sub-categories uses this API to fetch child groups", 
    		response = APIJsonWrapper.class, authorizations = {@Authorization(value = "auth")}, tags={  })
	@ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Success", response = APIJsonWrapper.class),
            @ApiResponse(code = 400, message = "Bad Request", response = APIJsonWrapper.class) })
	@RequestMapping(value =	{ WEBAPIURLCONSTANTS.BOOK_GROUPS,WEBAPIURLCONSTANTS.BOOK_GROUPS_SLUGNAME}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<JsonWrapper> mainApi( 
									@ApiIgnore
									@ModelAttribute(CONSTANTS.DEFAULT_INPUTS) WebApiRequestBean defaultRequestBean,
									@ApiIgnore
										@WebAPIUser User user, 
											 HttpServletRequest request, 
											 @ApiParam(value = " WEB API version ", required=true, defaultValue="6.0")
											 @PathVariable("version") Double version,
											 @ApiParam(value = "Slugnames required only for categories at all levels", required=false)
											 @PathVariable("slugname") Optional<String> slugname,
											 @ApiParam(value = "Market specific locale ", required=true, defaultValue="sv_SE", allowableValues="sv_SE, fi_FI, de_DE")
											 @RequestHeader String locale,
											 @ApiParam(required = true, name = "bookgrouprequest", value = "view & bookgroupid values for the request")
											  MainJsonRequestBean reqBean, BindingResult result) throws TokenNotValidException, APIException, Exception{
		
		String bookgroupid = null;
		if(slugname.isPresent()){//slugname to bookgroupid mapping
			
			bookgroupid = apptusApiService.getBookGroupidBySlug(slugname.get(), defaultRequestBean.getCountryCode());
			/*if(bookgroupid!=null && bookgroupid.startsWith(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()))
				reqBean.setView(BookGroupViewEnum.MAIN.getView());
			else if(bookgroupid!=null && bookgroupid.startsWith(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()))*/
				
			if(bookgroupid!=null){
				reqBean.setBookgroupid(bookgroupid);
				reqBean.setView(BookGroupViewEnum.LEVEL2.getView());
			}
			else
			{
				if(reqBean.getView().equals(BookGroupViewEnum.RELATED_BOOK.getView()))
				{
					reqBean.setIsbn(slugname.get());
				}
				
			}
		}
		bookGroupsRequestValidator.validate(reqBean, result);
	     
	    if (result.hasErrors())
	        throw new WEBAPIValidationException(result.getFieldErrors());
	    
		
		APIJsonWrapper apiJSONResponse; 
		BookGroupsApptusResponseWrapper wrapper = apptusApiService.getBookGroupsDetails( reqBean, user, version);
		
		if(wrapper!=null && wrapper.getBookGroups()!=null)
			wrapper.getBookGroups().stream().forEach(bg ->bg.setServiceType(API_TYPE.WEB));
		
		apiJSONResponse =  new APIJsonWrapper(wrapper);
		 
		
		return new ResponseEntity<JsonWrapper>(apiJSONResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value =	{ WEBAPIURLCONSTANTS.BOOKS_FOR_BOOKGROUPS,WEBAPIURLCONSTANTS.BOOKS_FOR_BOOKGROUPS_SLUGNAME }, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> booksforbookgroup( 
									@ModelAttribute(CONSTANTS.DEFAULT_INPUTS) WebApiRequestBean defaultRequestBean,
										@WebAPIUser User user, 
											 HttpServletRequest request, 
											 @PathVariable("version") Double version,
											 @PathVariable("slugname") Optional<String> slugname,
											 BooksForBookGroupRequestBean reqBean, BindingResult result) throws TokenNotValidException, APIException, Exception{
		String bookgroupid = null;
		if(slugname.isPresent()){//slugname to bookgroupid mapping
			bookgroupid = apptusApiService.getBookGroupidBySlug(slugname.get(), defaultRequestBean.getCountryCode());
			if(bookgroupid!=null)
				reqBean.setBookgroupid(bookgroupid);
			else
				reqBean.setBookgroupid(slugname.get());
		}		
		booksRequestValidator.validate(reqBean, result);
	     
	    if (result.hasErrors())
	        throw new WEBAPIValidationException(result.getFieldErrors());
		
		 
		BooksForBookGroupsApptusResponseWrapper wrapper = apptusApiService.getBooksForBookGroup(reqBean, user, version);
		MappingJacksonValue apiJSONResponse =   getFiltered(new APIJsonWrapper(wrapper),"productdetails_filter",books_exclude_filter);
		
		return new ResponseEntity<Object>(apiJSONResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value =	{ WEBAPIURLCONSTANTS.BOOK_SEARCH}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object	> searchBooks( 
									@ModelAttribute(CONSTANTS.DEFAULT_INPUTS) WebApiRequestBean defaultRequestBean,
										@WebAPIUser User user, 
											 HttpServletRequest request, 
											 @PathVariable("version") Double version,
											 SearchJsonApptusRequestBean reqBean, BindingResult result) throws TokenNotValidException, APIException, Exception{
				
		searchRequestValidator.validate_v2(reqBean, result);
	     
	    if (result.hasErrors())
	        throw new WEBAPIValidationException(result.getFieldErrors());
	     
		 
		BooksForBookGroupsApptusResponseWrapper wrapper = apptusApiService.getBooksFromKeywordSearchDevice(reqBean,version,user);	
		MappingJacksonValue apiJSONResponse =   getFiltered(new APIJsonWrapper(wrapper),"productdetails_filter",searchbooks_exclude_filter);
		 
		return new ResponseEntity<Object>(apiJSONResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value =	{ WEBAPIURLCONSTANTS.BOOK_SEARCH_FACETS}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> searchBookswithFacets( 
									@ModelAttribute(CONSTANTS.DEFAULT_INPUTS) WebApiRequestBean defaultRequestBean,
										@WebAPIUser User user, 
											 HttpServletRequest request, 
											 @PathVariable("version") Double version,
											 SearchJsonApptusRequestBean reqBean, BindingResult result) throws TokenNotValidException, APIException, Exception{
				
		searchRequestValidator.validate_facets(reqBean, result);
	     
	    if (result.hasErrors())
	        throw new WEBAPIValidationException(result.getFieldErrors());
	    
		SearchWithFacetsApptusResponseWrapper wrapper = apptusApiService.getBooksFromSearchWithFactes(reqBean,version,user);				
		MappingJacksonValue apiJSONResponse =   getFiltered(new APIJsonWrapper(wrapper),"productdetails_filter",searchbooks_exclude_facets_filter);
		 
		return new ResponseEntity<Object>(apiJSONResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value =	{ WEBAPIURLCONSTANTS.BOOK_ONLY_FACETS}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> onlyFacets( 
									@ModelAttribute(CONSTANTS.DEFAULT_INPUTS) WebApiRequestBean defaultRequestBean,
										@WebAPIUser User user, 
											 HttpServletRequest request, 
											 @PathVariable("version") Double version,
											 SearchJsonApptusRequestBean reqBean, BindingResult result) throws TokenNotValidException, APIException, Exception{
				
		searchRequestValidator.validate_only_facets(reqBean, result);
	     
	    if (result.hasErrors())
	        throw new WEBAPIValidationException(result.getFieldErrors());
	  
		SearchWithFacetsApptusResponseWrapper wrapper = apptusApiService.getOnlyFactes(reqBean,version,user);			
		MappingJacksonValue apiJSONResponse =   getFiltered(new APIJsonWrapper(wrapper),"productdetails_filter",searchbooks_exclude_facets_filter);
		 
		return new ResponseEntity<Object>(apiJSONResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value =	{ WEBAPIURLCONSTANTS.BOOK_SEARCH_ADVANCED}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> advancedSearchBooks( 
									@ModelAttribute(CONSTANTS.DEFAULT_INPUTS) WebApiRequestBean defaultRequestBean,
										@WebAPIUser User user, 
											 HttpServletRequest request, 
											 @PathVariable("version") Double version,
											 SearchJsonApptusRequestBean reqBean, BindingResult result) throws TokenNotValidException, APIException, Exception{
				
		suggestRequestValidator.validate_v2(reqBean, result);
	     
	    if (result.hasErrors())
	        throw new WEBAPIValidationException(result.getFieldErrors());
	    
		AdvancedSearchWrapper wrapper = apptusApiService.getAdvancedSearch(reqBean,version,user);			
		MappingJacksonValue apiJSONResponse =   getFiltered(new APIJsonWrapper(wrapper),"productdetails_filter",searchbooks_exclude_facets_filter);
		 
		return new ResponseEntity<Object>(apiJSONResponse, HttpStatus.OK);
	}


	public ApptusApiService getApptusApiService() {
		return apptusApiService;
	}


	public SearchBooksRequestBeanValidator getSearchRequestValidator() {
		return searchRequestValidator;
	}


	public SearchSuggestRequestBeanValidator getSuggestRequestValidator() {
		return suggestRequestValidator;
	}


	public BookGroupsRequestValidator getBookGroupsRequestValidator() {
		return bookGroupsRequestValidator;
	}


	public BooksBookGroupRequestValidator getBooksRequestValidator() {
		return booksRequestValidator;
	}


	public void setApptusApiService(ApptusApiService apptusApiService) {
		this.apptusApiService = apptusApiService;
	}


	public void setSearchRequestValidator(SearchBooksRequestBeanValidator searchRequestValidator) {
		this.searchRequestValidator = searchRequestValidator;
	}


	public void setSuggestRequestValidator(SearchSuggestRequestBeanValidator suggestRequestValidator) {
		this.suggestRequestValidator = suggestRequestValidator;
	}


	public void setBookGroupsRequestValidator(BookGroupsRequestValidator bookGroupsRequestValidator) {
		this.bookGroupsRequestValidator = bookGroupsRequestValidator;
	}


	public void setBooksRequestValidator(BooksBookGroupRequestValidator booksRequestValidator) {
		this.booksRequestValidator = booksRequestValidator;
	}
		
}

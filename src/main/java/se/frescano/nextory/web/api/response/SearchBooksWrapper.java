package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.List;

public class SearchBooksWrapper implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7105117332744194655L;
	private String searchon;
	private String	esalesticket;
	private String	keyword;
	private Integer	bookcount;
	private List<UIBook> books;
	private List<String> toptitles;
	
	public SearchBooksWrapper(){
		
	}
	
	public SearchBooksWrapper(String searchon, String	esalesticket, String	keyword, Integer	bookcount,
			List<UIBook> books,
			List<String> toptitles){
		this.searchon	= searchon;
		this.esalesticket	= esalesticket;
		this.keyword	= keyword;
		this.bookcount	= bookcount;
		this.books	 = books;
		this.toptitles = toptitles;
	}
	
	public String getSearchon() {
		return searchon;
	}
	public void setSearchon(String searchon) {
		this.searchon = searchon;
	}
	public String getEsalesticket() {
		return esalesticket;
	}
	public void setEsalesticket(String esalesticket) {
		this.esalesticket = esalesticket;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Integer getBookcount() {
		return bookcount;
	}
	public void setBookcount(Integer bookcount) {
		this.bookcount = bookcount;
	}
	public List<UIBook> getBooks() {
		return books;
	}
	public void setBooks(List<UIBook> books) {
		this.books = books;
	}

	public List<String> getToptitles() {
		return toptitles;
	}

	public void setToptitles(List<String> toptitles) {
		this.toptitles = toptitles;
	}
		
}

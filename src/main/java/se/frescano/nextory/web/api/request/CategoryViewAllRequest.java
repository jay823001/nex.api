package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class CategoryViewAllRequest extends AuthKeyRequestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3961491505865906943L;

	/**
	 * 
	 */
	private String catslug;
	
	private String subcatslug;
	
	private String sorton;

	private Integer pagenumber;
	
	private Integer format;
	
	private Integer windowsize;
	
	private String toplist;
	
	private String subscription;

	public String getCatslug() {
		return catslug;
	}

	public void setCatslug(String catslug) {
		this.catslug = catslug;
	}

	public String getSubcatslug() {
		return subcatslug;
	}

	public void setSubcatslug(String subcatslug) {
		this.subcatslug = subcatslug;
	}

	public String getSorton() {
		return sorton;
	}

	public void setSorton(String sorton) {
		this.sorton = sorton;
	}

	public Integer getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(Integer pagenumber) {
		this.pagenumber = pagenumber;
	}

	public Integer getFormat() {
		return format;
	}

	public void setFormat(Integer format) {
		this.format = format;
	}

	public Integer getWindowsize() {
		return windowsize;
	}

	public void setWindowsize(Integer windowsize) {
		this.windowsize = windowsize;
	}

	public String getToplist() {
		return toplist;
	}

	public void setToplist(String toplist) {
		this.toplist = toplist;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	@Override
	public String toString() {
		return "CategoryViewAllRequest [catslug=" + catslug + ", subcatslug=" + subcatslug + ", sorton=" + sorton
				+ ", pagenumber=" + pagenumber + ", format=" + format + ", windowsize=" + windowsize + ", toplist="
				+ toplist + ", subscription=" + subscription + "]";
	}
	
	
	 
}

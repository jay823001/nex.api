package se.frescano.nextory.web.api.exception;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.exception.TokenNotValidException;
import se.frescano.nextory.app.response.ErrorJsonWrapper;
import se.frescano.nextory.web.api.response.GenericAPIResponse;

import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;


@ControllerAdvice(basePackages="se.frescano.nextory.web.apptus.controller")
@ComponentScan
@RestController
public class WEBAPIExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(WEBAPIExceptionHandler.class);
	
	 
	 
	
	//@ExceptionHandler(MethodArgumentNotValidException.class)
	/* @Override
	  public final ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, 
			  HttpHeaders headers, HttpStatus status, WebRequest request) {
		 GenericAPIResponse errorDetails = new GenericAPIResponse(HttpStatus.BAD_REQUEST,
					WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION, ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	
	//@ExceptionHandler(MissingServletRequestParameterException.class)
	 @Override
	  public final ResponseEntity<Object> handleMissingServletRequestParameter (MissingServletRequestParameterException ex, 
			  HttpHeaders headers, HttpStatus status, WebRequest request) {
		 GenericAPIResponse errorDetails = new GenericAPIResponse(HttpStatus.BAD_REQUEST,
					WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION, ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }*/
	
	/** All BAD REQUEST ERROR RESPONSE - 400 */
	@ExceptionHandler(ConstraintViolationException.class)
	  public final ResponseEntity<Object> handleConstraintViolation (ConstraintViolationException ex, WebRequest request) {
		ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INPUT_MISSING, ex.getConstraintViolations());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	
	@Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, 
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INPUT_MISSING, ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(ValidationException.class)
	  public final ResponseEntity<Object> handleValidationException (ValidationException ex, WebRequest request) {
		
		ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INPUT_MISSING, ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	@ExceptionHandler(TokenNotValidException.class)
	  public final ResponseEntity<Object> handleTokenNotValidException (TokenNotValidException ex, WebRequest request) {
		
		ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INVALID_TOKEN, ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	
	
	@ExceptionHandler(WEBAPIValidationException.class)
	  public final ResponseEntity<Object> handleWEBAPIValidationException (WEBAPIValidationException ex, WebRequest request) {
		
		ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INPUT_MISSING, ex.getErrorFields());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	

	/** All SERVER ERROR RESPONSE - 500 */	
	 @ExceptionHandler(Throwable.class)
	    public ResponseEntity<Object> handleDefaultException(Throwable ex) {
		 
		 ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INTERNAL_ERROR, ex.getMessage());
		    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	 }
	 @ExceptionHandler(RuntimeException.class)
	    public ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {
		 
		 ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INTERNAL_ERROR, ex.getMessage());
		    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	 }
	 
	 @ExceptionHandler(APIException.class)
	  public final ResponseEntity<Object> handleAPIException (APIException ex, WebRequest request) {
		
		ErrorJsonWrapper errorDetails = new ErrorJsonWrapper(ErrorCodes.INTERNAL_ERROR, ex.getMessage());
	    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
}

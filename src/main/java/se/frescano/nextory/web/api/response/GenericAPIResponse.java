package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.frescano.nextory.web.api.model.WebAPIResponseError;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@JsonInclude(Include.NON_NULL)
public class GenericAPIResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8783183119744295674L;
	private int status;
	private Object data;
	private WebAPIResponseError error;
	 
	 
	public GenericAPIResponse() { 
	}
	 
	public GenericAPIResponse(HttpStatus status, Object response){
		super();
		//this.data =new WebAPIResponseData();
		this.status = status.value();
		this.data = response;
	}

	public GenericAPIResponse(HttpStatus status, WebAPIResponseError error){
		super();
		this.status = status.value();
		this.error = error;
	}
	
	public GenericAPIResponse(HttpStatus status, WEBErrorCodeEnum error){
		super();
		this.status = status.value();
		this.error = new WebAPIResponseError(error);
	}


	/*public GenericAPIResponse(HttpStatus status, WEBErrorCodeEnum error, String message) {
		super();
		this.status = status.value();
		this.error = new WebAPIResponseError(error.getCode(), error.getMessage()+ (( message != null )? ", " + message : ""));
	}*/

	public GenericAPIResponse(HttpStatus status, WEBErrorCodeEnum error, List<FieldError> errorFields) {
		super();
		this.status = status.value();
		this.error = new WebAPIResponseError(error, errorFields);
	}
	/*public GenericAPIResponse(HttpStatus status, WEBErrorCodeEnum error, StackTraceElement[] trace) {
		super();
		this.status = status.value();
		if(trace!=null && trace.length>0)
			this.error = new WebAPIResponseError(error, trace[0].toString());
	}*/
	public GenericAPIResponse(HttpStatus status, WEBErrorCodeEnum error, Exception e) {
		super();
		this.status = status.value();
		StackTraceElement[] trace = e.getStackTrace(); 
		if(trace!=null && trace.length>1)
			this.error = new WebAPIResponseError(error, e.getMessage()+" at "+trace[0].toString());
		else
			this.error = new WebAPIResponseError(error, e.getMessage());
	}
	
	public GenericAPIResponse(HttpStatus status, WEBErrorCodeEnum error, String details) {
		super();
		this.status = status.value(); 
		this.error = new WebAPIResponseError(error, details);
	}
	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}

	public void setStatus(HttpStatus httpStatus) {
		this.status = httpStatus.value();
	}


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public WebAPIResponseError getError() {
		return error;
	}


	public void setError(WebAPIResponseError error) {
		this.error = error;
	}
	 
	 
}

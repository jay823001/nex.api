package se.frescano.nextory.web.api.response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"categoryids"
})
public class CategoryReferenceAttributes {
	
	
	
	@JsonProperty("categoryids")
	private List<ParentCategoryVo> categoryids = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("categoryids")
	public List<ParentCategoryVo> getCategoryids() {
	return categoryids;
	}

	@JsonProperty("categoryids")
	public void setCategoryids(List<ParentCategoryVo> categoryids) {
	this.categoryids = categoryids;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

	

}

package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class BookGroupsRequest extends RequestParamBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1571035221430824228L;
	 
	private Long bookgroupid;
	private String subscription;	
	private String category_slug;
	 
 
	 
	public Long getBookgroupid() {
		return bookgroupid;
	}
	public void setBookgroupid(Long bookgroupid) {
		this.bookgroupid = bookgroupid;		 
	}	
	 
	 
	
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	public String getCategory_slug() {
		return category_slug;
	}
	public void setCategory_slug(String category_slug) {
		this.category_slug = category_slug;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BookGroupsRequest [");
		if (bookgroupid != null) {
			builder.append("bookgroupid=");
			builder.append(bookgroupid);
			builder.append(", ");
		}
		if (subscription != null) {
			builder.append("subscription=");
			builder.append(subscription);
			builder.append(", ");
		}
		if (category_slug != null) {
			builder.append("category_slug=");
			builder.append(category_slug);
		}
		builder.append("]");
		return builder.toString();
	}
	 
}

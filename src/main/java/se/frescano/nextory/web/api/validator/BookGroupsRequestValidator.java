package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.product.request.MainJsonRequestBean;
import se.frescano.nextory.web.api.request.BookGroupsRequest;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class BookGroupsRequestValidator extends AuthKeyValidator implements Validator {

	 
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return MainJsonRequestBean.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		MainJsonRequestBean requestBean = (MainJsonRequestBean)target;
		//if(bookGroup.getAuthkey()!=null && !bookGroup.getAuthkey().isEmpty())
    	//validateKey(bookGroup.getAuthkey(), errors, false);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "view", "error.view", ErrorCodes.INPUT_MISSING.name());
		
		
	}

}

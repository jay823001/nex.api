package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class RequestParamBean extends AuthKeyRequestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6872217397099812127L;
	
	//private String  subscription;
	private String 	format;
	private String 	sorton;
	private Integer	pagenumber;
	private Integer	windowsize;
	/*public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}*/
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getSorton() {
		return sorton;
	}
	public void setSorton(String sorton) {
		this.sorton = sorton;
	}
	public Integer getPagenumber() {
		return pagenumber;
	}
	public void setPagenumber(Integer pagenumber) {
		this.pagenumber = pagenumber;
	}
	public Integer getWindowsize() {
		return windowsize;
	}
	public void setWindowsize(Integer windowsize) {
		this.windowsize = windowsize;
	}
	
	
}

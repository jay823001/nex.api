package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class BockerSpecificRequest extends AuthKeyRequestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8820825932748051064L;

	/**
	 * 
	 */
	
	private String subscription;
	
	private Integer windowsize;


	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	
	
	public Integer getWindowsize() {
		return windowsize;
	}

	public void setWindowsize(Integer windowsize) {
		this.windowsize = windowsize;
	}

	@Override
	public String toString() {
		return "BockerSpecificRequest [subscription=" + subscription + "]";
	}

	
	
	

	 
}

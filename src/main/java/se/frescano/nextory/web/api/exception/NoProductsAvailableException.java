package se.frescano.nextory.web.api.exception;

public class NoProductsAvailableException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7827313253361461747L;

	public NoProductsAvailableException(Exception e) {
		super(e);
	}

	public NoProductsAvailableException(String string) {
		super(string);
	}
	
}

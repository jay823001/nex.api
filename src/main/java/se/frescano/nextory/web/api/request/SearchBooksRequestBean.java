package se.frescano.nextory.web.api.request;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class SearchBooksRequestBean extends RequestParamBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7382906151908500808L;

	 
	private String	type;
	 
	private String keyword;
	 
	//private String[] excludePublishers;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		
		try {
			this.keyword = URLDecoder.decode(keyword, StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.keyword = keyword;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "SearchBooksRequestBean [type=" + type + ", keyword=" + keyword + "]";
	}	 
		
}

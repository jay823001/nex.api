package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class UIBookGroupFinal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6051604864899939009L;

	/**
	 * 
	 */
	//private static final long serialVersionUID = 6914916839038932982L;
	
	private String title;
	
	private ArrayList<UIBookGroup> bookgroups;
	
	private ArrayList<String> toptitles;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<UIBookGroup> getBookgroups() {
		return bookgroups;
	}

	public void setBookgroups(ArrayList<UIBookGroup> bookgroups) {
		this.bookgroups = bookgroups;
	}

	public ArrayList<String> getToptitles() {
		return toptitles;
	}

	public void setToptitles(ArrayList<String> toptitles) {
		this.toptitles = toptitles;
	}

	/*public static long getSerialversionuid() {
		return serialVersionUID;
	}*/
	
	
	
}

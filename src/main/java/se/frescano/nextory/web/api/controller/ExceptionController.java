package se.frescano.nextory.web.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import se.frescano.nextory.web.api.exception.InSecuredRequestException;
import se.frescano.nextory.web.api.response.GenericAPIResponse;

@ControllerAdvice
public class ExceptionController {
	 @ExceptionHandler(value = InSecuredRequestException.class)
	  public GenericAPIResponse exception(InSecuredRequestException exception) {
	       return new GenericAPIResponse(HttpStatus.BAD_REQUEST, HttpStatus.UNAUTHORIZED);
	  }
}

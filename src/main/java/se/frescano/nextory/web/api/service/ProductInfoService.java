package se.frescano.nextory.web.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonGenerator;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.apptus.services.ApptusRelatedBookZoneService;
import se.frescano.nextory.app.apptus.services.DidyouMeanPanelService;
import se.frescano.nextory.app.apptus.services.RecommendZoneService;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.apptus.model.AptusSecond;
import se.frescano.nextory.apptus.model.AutoCompleteHolder;
import se.frescano.nextory.apptus.model.Completions;
import se.frescano.nextory.apptus.request.DidYouMeanPanel;
import se.frescano.nextory.apptus.service.ApptusNotificationApiCall;
import se.frescano.nextory.apptus.service.ApptusServiceHelper;
import se.frescano.nextory.apptus.utils.FilterUtils;
import se.frescano.nextory.blueshift.config.BlueShiftConfiguration;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.listener.Publisher;
import se.frescano.nextory.model.SearchRequestBean;
import se.frescano.nextory.model.SearchResult;
import se.frescano.nextory.product.dao.ProductDao;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.URLBuilder;
import se.frescano.nextory.web.api.request.SearchBooksRequestBean;
import se.frescano.nextory.web.api.request.SearchSuggestRequestBean;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.response.SearchBooksWrapper;
import se.frescano.nextory.web.api.response.SearchSuggestionWrapper;
import se.frescano.nextory.web.api.response.UIBook;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;
import se.frescano.nextory.web.apptus.request.WebAutoCompletePanel;
import se.frescano.nextory.web.apptus.request.WebSearchZonePanelRequest;
import se.frescano.nextory.web.apptus.service.WebApptusAutoCompleteService;
import se.frescano.nextory.web.apptus.service.WebApptusSearchService;

@Service
public class ProductInfoService {
	private static final Logger logger = LoggerFactory.getLogger(ProductInfoService.class);
			
	@Autowired
	private	ProductDao productDao;
	
	@Autowired
	private ApplicationPropertiesConfig applicationpropconfig;
	
	
	@Autowired
	private ApptusServiceHelper apptusServicehelper;
		
	@Autowired
	ApptusNotificationApiCall notificationForApptus;
	
	@Autowired
	CacheBridgeMarket cacheBridgeMarket;
	
	@Autowired
	private ApptusRelatedBookZoneService	releatedbookzoneservice;
	
	@Autowired
	private RecommendZoneService			apptusrecommendzoneservice;
	
	@Autowired
	private DidyouMeanPanelService			apptusdidyoumeanservice;
	
	@Autowired
	private WebApptusSearchService			webapptussearchservice;
	
	@Autowired
	private WebApptusAutoCompleteService	webautocompleteservice;
	
	/*@Autowired
	private EventBusPublisher	publisher_event;*/
	
	@Autowired
	private Publisher publisher;
	@Autowired
	private BlueShiftConfiguration 		 blueShiftConfiguration;
	
			
	//Code Using
	public GenericAPIResponse searchBooks(SearchBooksRequestBean requestBean, String  countrycode, User user) {		
		GenericAPIResponse		response	 = null;
		if( StringUtils.equalsIgnoreCase( requestBean.getType(), "series") )
			response	=	searchBooksByKeyword(requestBean,countrycode,user);
		else if( StringUtils.equalsIgnoreCase( requestBean.getType(), "contributor") )
			response	=	searchBooksByContributor(requestBean,countrycode,user);
		else if( StringUtils.equalsIgnoreCase( requestBean.getType(), "search") )
			response	=	searchBooksByKeyword(requestBean,countrycode,user);				
		return response;
	}
	
	//Code Using
	@SuppressWarnings("unused")
	public GenericAPIResponse searchSuggestedBooks( SearchSuggestRequestBean requestBean, String countrycode, User user ) throws Exception {
		
		SearchResult 			searchResult = null;
		GenericAPIResponse		response	 = new GenericAPIResponse();			
		AutoCompleteHolder pubDetails = new AutoCompleteHolder();
		try{														
			ArrayList<Completions> completions = CallAutoCompleteNew(requestBean.getKeyword(), countrycode, user);
			response.setStatus( HttpStatus.OK.value());
			if(null != completions )
			{				
				for(Completions comp:completions )
				{
					if(comp.getText().indexOf("|")!=-1)				
						comp.setText(comp.getText().replaceAll("\\|", ", "));					
				}
				response.setData( new SearchSuggestionWrapper(requestBean.getKeyword(), completions));	
			}
			else
			response.setData( new SearchSuggestionWrapper(requestBean.getKeyword(), completions));				
		}catch(Exception e){
			throw new Exception( e.getMessage() );
		}		
		return response;
	}
	
	//Code Using
	public GenericAPIResponse searchBooksByContributor(SearchBooksRequestBean requestBean,String countrycode,User user ){
		
		SearchResult 			searchResult = null;
		GenericAPIResponse		response	 = new GenericAPIResponse();			
		List<String> 			toptiltes	 = new ArrayList<String>(3);
		try{
			requestBean.setKeyword( HtmlUtils.htmlEscape(requestBean.getKeyword(), "UTF-8") );			
			if(!applicationpropconfig.getApptusOn().equalsIgnoreCase("on")){
				//searchResult = getContributorSearchResults(requestBean, false);
			}else
				searchResult = getSearchResultsForAptus(requestBean,requestBean.getType(), false, true,countrycode,user);
			List<UIBook>	books 	= new ArrayList<UIBook>();
			if( searchResult != null && searchResult.getProductlist() != null && searchResult.getProductlist().size() > 0 ){
				for( ProductInformation	product	: searchResult.getProductlist()){
					UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
												product.getTitle(),
												product.getIsbn(),
												product.getCoverUrl(),
												URLBuilder.buildUrl(product),
												(product.getRelProductid() != null)?Long.valueOf( product.getRelProductidApp()):0);
					book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
					books.add(book);
					if( toptiltes.size() < CONSTANTS.TOP_TITLES_LIMIT )
						toptiltes.add( book.getIsbn() );
				}				
			}
			response.setStatus( HttpStatus.OK.value());
			response.setData( new SearchBooksWrapper(requestBean.getType(), "XXXXXX", requestBean.getKeyword(), (books.size() > 0 ? books.size():0), books, toptiltes));		
		}catch(Exception e){
			response	= new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION);
		}
		
		return response;
	}
	
	//Code using
	public GenericAPIResponse searchBooksByKeyword(SearchBooksRequestBean requestBean, String countrycode,User user) {
		SearchResult 			searchResult = null;
		GenericAPIResponse		response	 = new GenericAPIResponse();	
		List<String>			toptitles	 = new ArrayList<String>(3);
		try{			
			requestBean.setKeyword( HtmlUtils.htmlEscape(requestBean.getKeyword(), "UTF-8") );
			if(!applicationpropconfig.getApptusOn().equalsIgnoreCase("on")){
								
			}else{
				searchResult = getSearchResultsForAptus(requestBean, requestBean.getType(), false, true,countrycode,user);
			}
			
			List<UIBook>	books 	= new ArrayList<UIBook>();
			if( searchResult != null && searchResult.getProductlist() != null && searchResult.getProductlist().size() > 0 ){
				for( ProductInformation	product	: searchResult.getProductlist()){
					UIBook	book	= new UIBook(Long.valueOf( String.valueOf( product.getProductid() ) ),												
												product.getTitle(),
												product.getIsbn(),
												product.getCoverUrl(),
												URLBuilder.buildUrl(product),
												(product.getRelProductid() != null)?Long.valueOf( product.getRelProductidApp() ):0);
					book.setFormattype( ( product.getProductFormat().getFormatNo() == 1 )?ProductFormatEnum.E_BOOK:ProductFormatEnum.AUDIO_BOOK );
					books.add(book);
					if( toptitles.size() < CONSTANTS.TOP_TITLES_LIMIT ){
						toptitles.add( book.getIsbn() );
					}
				}				
			}
			response.setStatus( HttpStatus.OK.value());
			String query = requestBean.getKeyword();
			if(searchResult != null && org.apache.commons.lang3.StringUtils.isNotBlank(searchResult.getQuery())){
				query = searchResult.getQuery();
			}
			if(logger.isDebugEnabled())logger.debug(" Query modified as Did you mean "+query);
			response.setData( new SearchBooksWrapper(requestBean.getType(), "XXXXXX", 
					query, (books.size() > 0 ? books.size():0), books, toptitles));	
			
		}catch(Exception e){
			e.printStackTrace();
			response	= new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, 
												 WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}		
		return response;
	}
	
	//Code Using
	public SearchResult getSearchResultsForAptus(SearchBooksRequestBean requestBean, String field, boolean typeAhead, boolean logReq,
			String countrycode, User user){		
		SearchResult 	searchResult = null;
		//HashMap<String, Double> map = productDao.getBasePriceForSubscription();
		// All the search requests should be logged in MONGO hence
		// retrieving previous log for the search query
		try {
			SearchRequestBean	bean	=	new SearchRequestBean();
			bean.setQ( requestBean.getKeyword() );
			//bean.setSubscribe( requestBean.getSubscription() );
			bean.setFormat( requestBean.getFormat() );
			bean.setPageNumber( requestBean.getPagenumber() );
			bean.setPageSize( requestBean.getWindowsize() );
			bean.setSortOn( requestBean.getSorton() );
			            	
            WebSearchZonePanelRequest 	panel		= apptusServicehelper.getAptusUrlForSearchZone(bean, field,countrycode,user);			
			AptusSecond 				pubDetails	= webapptussearchservice.search( panel );			
			if(pubDetails.getSearchHits() != null && pubDetails.getSearchHits().size() > 0){
				ArrayList<ProductInformation> productlist = apptusServicehelper.operateOnAptusSecondSearch( pubDetails );
				ArrayList<ProductInformation> productlistSuggest = new ArrayList<>();
				searchResult = new SearchResult();
				if(requestBean.getType().equalsIgnoreCase("search"))
				{					
					int size =0;					
					searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount()+size);	
				}
				else
				{
					searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());
				}
				productlistSuggest.addAll(productlist);				
				searchResult.setProductlist(productlistSuggest);
				searchResult.setQuery(requestBean.getKeyword());
				
			}else if(requestBean.getPagenumber()!=null && requestBean.getPagenumber().intValue() <=1){ // we should not call the didyoumean all the time .. plan to change this ...
				DidYouMeanPanel panel1 = new DidYouMeanPanel(requestBean.getKeyword());				
				panel1.setMarket(apptusServicehelper.getMarketPanelArgument( countrycode ));				
				String didYouMean = apptusdidyoumeanservice.search(panel1);
				if( StringUtils.isNotBlank( didYouMean ) ){					
					requestBean.setKeyword( didYouMean );
					bean.setQ( didYouMean );
					panel = apptusServicehelper.getAptusUrlForSearchZone(bean, field, countrycode,user);
					AptusSecond pubDetailsNew	= webapptussearchservice.search( panel );
					if(pubDetailsNew.getSearchHits() != null && pubDetailsNew.getSearchHits().size() > 0){
						ArrayList<ProductInformation> productlist = apptusServicehelper.operateOnAptusSecondSearch(pubDetailsNew);
						searchResult = new SearchResult();
						searchResult.setResultSize(pubDetailsNew.getSearchHitCount().get(0).getCount());
						searchResult.setProductlist(productlist);
						searchResult.setQuery( didYouMean );
					}
				}
			}
		} catch (Exception e) {
			logger.error("getSearchResultsForAptus failed. " + e.getMessage());
		}		
		return searchResult;
	}
   	
	//Code Using
	public ArrayList<Completions> CallAutoCompleteNew(String query, String countrycode,User user) throws Exception{
		// String tempurl = config.getGenericUrl()+"autocompletesuggestionweb?";		
		WebAutoCompletePanel	panel = new WebAutoCompletePanel();
		if(user!=null && !StringUtils.isBlank( user.getUserCountry() ))
		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter(user.getUserCountry())); 
			panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter(user.getUserCountry())); 				
		}
		if(user!=null && user.getExcludePubs()!=null )
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ) );				
		if( !StringUtils.isBlank( countrycode ))
			panel.setMarket_filter(countrycode);				
		panel.setMarket(apptusServicehelper.getMarketPanelArgument(countrycode));
		panel.setSessionKey(apptusServicehelper.setWebSessionKey(user));
		panel.setCustomerKey(apptusServicehelper.setWebCustomerKey(user));		
		panel.setSearchPrefix( query );
		panel.setWindow_first("1");
		panel.setWindow_last("10");			  				
		return webautocompleteservice.search(panel);
	}
	
	//Code Using
	public void getImageForProductBasedOnBookId(Integer bookid,final JsonGenerator jsonGenerator, Map<String, Object> jsonMap) {
		productDao.getImageForProductBasedOnBookId(bookid,jsonGenerator,jsonMap);	
	}
	
	//Code Using
	public HashMap<String, Integer> getAllMarketFromCacheBridge() throws Exception, RestException{
		return cacheBridgeMarket.getMarketFromNest();
	}	
}

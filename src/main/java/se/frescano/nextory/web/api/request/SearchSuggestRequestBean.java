package se.frescano.nextory.web.api.request;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import javax.validation.constraints.NotNull;

public class SearchSuggestRequestBean extends AuthKeyRequestBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8497555531382011030L;
	@NotNull
	private String keyword;
   // private String[] excludePublishers;

	public String getKeyword() {
		return keyword;
	}


	public void setKeyword(String keyword) {
		
		try {
			this.keyword = URLDecoder.decode(keyword, StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.keyword = keyword;
		}
	}

	@Override
	public String toString() {
		return "SearchSuggestRequestBean [keyword=" + keyword + "]";
	}
}

package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import se.frescano.nextory.web.api.request.CategorySpecificRequest;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class CategorySpecificValidator extends AuthKeyValidator implements Validator { 
	
	@Override
	public boolean supports(Class<?> clazz) {
		 
		return CategorySpecificRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CategorySpecificRequest categorySpecificRequest = (CategorySpecificRequest)target; 
       // if(bookRequest.getAuthkey()!=null && !bookRequest.getAuthkey().isEmpty())
    	validateKey(categorySpecificRequest.getAuthkey(), errors,false);
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors,"categoryslug", "categoryslug.required", WEBErrorCodeEnum.INPUT_MISSING.name());
		 
	}

}

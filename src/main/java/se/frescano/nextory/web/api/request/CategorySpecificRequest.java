package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class CategorySpecificRequest extends AuthKeyRequestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8720663727152384060L;

	/**
	 * 
	 */
	private String categoryslug;
	
	private Integer format;
	
	private String subscription;
	
	private Integer pagenumber;
	
	private Integer windowsize;

	public String getCategoryslug() {
		return categoryslug;
	}

	public void setCategoryslug(String categoryslug) {
		this.categoryslug = categoryslug;
	}

	public Integer getFormat() {
		return format;
	}

	public void setFormat(Integer format) {
		this.format = format;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	
	
	public Integer getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(Integer pagenumber) {
		this.pagenumber = pagenumber;
	}

	
	
	public Integer getWindowsize() {
		return windowsize;
	}

	public void setWindowsize(Integer windowsize) {
		this.windowsize = windowsize;
	}

	@Override
	public String toString() {
		return "CategorySpecificRequest [categoryslug=" + categoryslug + ", format=" + format + ", subscription="
				+ subscription + ", pagenumber=" + pagenumber + "]";
	}
	
	

	 
}

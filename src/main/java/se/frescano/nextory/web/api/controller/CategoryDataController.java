package se.frescano.nextory.web.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.service.CategoryDataService;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@RestController
public class CategoryDataController extends WebAPIBaseController{

	private static final Logger logger = LoggerFactory.getLogger(CategoryDataController.class);
	
	@Autowired
	private CategoryDataService	categoryservice;
	
	@RequestMapping(value="/categories",  method = RequestMethod.GET,produces = "application/json")
	public GenericAPIResponse loadCategories(HttpServletRequest request){		
		GenericAPIResponse		response	= new GenericAPIResponse();		
		try{
			String countrycode = (String) request.getAttribute("countrycode");
			if(StringUtils.isBlank(countrycode))
				countrycode="SE";
			response	=	categoryservice.loadCategoriesList(countrycode);		
			if( logger.isTraceEnabled())logger.trace("Category list...." + response.getData() );
		}catch(Exception e){
			logger.error("loadCategories failed. " + e.getMessage() );
			response	=	new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR,
													WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, e);
		}				
		return response;
	}
}

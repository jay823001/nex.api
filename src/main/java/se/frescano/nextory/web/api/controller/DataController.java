package se.frescano.nextory.web.api.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.product.response.BooksForBookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.app.request.APIController;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.JsonWrapper;
import se.frescano.nextory.apptus.service.ApptusService;
import se.frescano.nextory.auth.rest.response.TokenExtractResponse;
import se.frescano.nextory.auth.service.AuthenticationServiceImpl;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.web.api.response.TopBookVO;
import se.frescano.nextory.web.api.response.UIBook;
import se.frescano.nextory.web.api.service.ProductInfoService;

@RestController
@RequestMapping(value = {"/api/staticdata", "/api/ext/product/{version:1.1}"})
public class DataController extends APIController{

	private static final Logger logger = LoggerFactory.getLogger(DataController.class);
	
	@Autowired
	private	ProductInfoService	productinfoservice;
	
	@Autowired
	private	AuthenticationServiceImpl		authenticationservice;
		
	@Autowired
	private ApptusService		apptusservice;
	
	@RequestMapping(value = "/NewList", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	 public List<TopBookVO> getNewList(@RequestParam("countrycode")String countrycode){		 
		 List<TopBookVO>	list	= new ArrayList<TopBookVO>();
		 try{
			 
			 Map<String , Integer> submap = InMemoryCache.subAgeLimits;
				List<Map.Entry<String , Integer>> listNew =
		                new LinkedList<Map.Entry<String , Integer>>(submap.entrySet());
				
				Collections.sort(listNew, new Comparator<Map.Entry<String , Integer>>() {
		            public int compare(Map.Entry<String , Integer> c1,
		                               Map.Entry<String , Integer> c2) {
		                return (new Integer(c1.getValue())).compareTo(new Integer(c2.getValue()));
		            }
		      });
				
			
				for(Entry<String , Integer> map:listNew)
				{	
				
					 List<ProductInformation> apiresp = apptusservice.getTopNyether(countrycode,map.getValue());
					 if( apiresp != null && apiresp.size() > 0 ){
							for(ProductInformation info : apiresp){
								TopBookVO	vo = new TopBookVO(
										info.getProductid(),
										info.getTitle(),
										info.getIsbn(),
										info.getCoverUrl(),
										info.getRelProductidApp(),
										info.getProductFormat().getTitle(),
										info.getDescription());
								list.add(vo);
							}
						}	
				}
		 }catch (APIException e){
			 list = new ArrayList<TopBookVO>(0);
			 logger.info("getNewList failed. " + e.getMessage() );
		 }catch (Exception e){
			 list = new ArrayList<TopBookVO>(0);
			 logger.info("getNewList failed. " + e.getMessage() );
		 }		 
		 return list;
	 }
	 
	@RequestMapping(value = "/apptus/topEbookAndAudioBook", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String, List<TopBookVO>> gettopMarketEbookAndAudioBookApptus(){		
			
			Map<String, List<TopBookVO>> mapOfList = new HashMap<String, List<TopBookVO>>();
			try{
				
				HashMap<String, Integer> marketlist=productinfoservice.getAllMarketFromCacheBridge();
				
				Map<String , Integer> submap = InMemoryCache.subAgeLimits;
				List<Map.Entry<String , Integer>> listNew =
		                new LinkedList<Map.Entry<String , Integer>>(submap.entrySet());
				
				Collections.sort(listNew, new Comparator<Map.Entry<String , Integer>>() {
		            public int compare(Map.Entry<String , Integer> c1,
		                               Map.Entry<String , Integer> c2) {
		                return (new Integer(c1.getValue())).compareTo(new Integer(c2.getValue()));
		            }
		      });
				
				for (Map.Entry<String, Integer> entry : marketlist.entrySet()) {
			        //System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
					List<TopBookVO>	list	= new ArrayList<TopBookVO>();
					for(Entry<String , Integer> map:listNew)
					{
						List<ProductInformation> apiresp = apptusservice.getTopEbookAndAudioBookApptus(map.getKey(),entry.getKey());
						if( apiresp != null && apiresp.size() > 0 )
						{
							for(ProductInformation info : apiresp)
							{
								TopBookVO	vo = new TopBookVO(
										info.getProductid(),
										info.getTitle(),
										info.getIsbn(),
										info.getCoverUrl(),
										info.getRelProductidApp(),
										info.getProductFormat().getTitle(),
										info.getDescription());
								list.add(vo);
							}
						}
					}
					
					mapOfList.put(entry.getKey(), list);
				}
			}catch (APIException e){
				logger.error("gettopEbookAndAudioBook failed.  " + e.getMessage() );
				 mapOfList = new HashMap<String, List<TopBookVO>>(0);
			} catch (Exception e) {
				logger.error("gettopEbookAndAudioBook failed. " + e.getMessage());
				mapOfList = new HashMap<String, List<TopBookVO>>(0);
			}		
			return mapOfList;
	 }
	 
	@RequestMapping(value = "/relatedSeriesbooks", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getRelatedseriesBooks(@RequestParam("seriesname")String seriesname, 
										@RequestParam("apptusid")Integer apptusid, 
										@RequestParam("localecode")String country){		 
		List<UIBook>	books = new ArrayList<UIBook>(0);
		try{
			books  = apptusservice.relatedSeriesBooks(seriesname, apptusid, country);
		}catch(Exception e){
			logger.error("getRelatedseriesBooks failed.  " , e);
		}
		return books;
	}
	
	@RequestMapping(value = "/relatedAuthorbooks", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getRelatedAuthorBooks(@RequestParam( name = "authors", required = false)List<String> authors, 
										@RequestParam("apptusid")Integer apptusid, 
										@RequestParam(name = "excludepublishers", required = false)List<Integer> excludepublishers, 
										@RequestParam( name = "usercountry", required=false)String usercountry,
										@RequestParam("localecode")String localecode){		 
		List<UIBook>	books = new ArrayList<UIBook>(0);
		try{
			books  = apptusservice.relatedAutorBooks(authors, apptusid, excludepublishers, usercountry, localecode);
		}catch(Exception e){
			logger.error("getRelatedseriesBooks failed.  " , e);
		}
		return books;
	}
	
	@RequestMapping(value = "/recommandedbooks", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getRecommandedBooks(@RequestParam( name = "authors", required = false)List<String> authors, 
									  @RequestParam("apptusid")Integer apptusid, 																				
									  @RequestParam("localecode")String localecode){		 
		List<UIBook>	books = new ArrayList<UIBook>(0);
		try{
			books  = apptusservice.recommandedBooks( apptusid, authors, localecode);
		}catch(Exception e){
			logger.error("getRelatedseriesBooks failed.  " , e);
		}
		return books;
	}
	
	@RequestMapping(value = "/recommandedbooksBasedOnCustomer", method = RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getRecommnedBooksBasedOnCustomer(@RequestParam("customerid")Integer customerid){		 
		BooksForBookGroupsApptusResponseWrapper	books = new BooksForBookGroupsApptusResponseWrapper();
		try{
			TokenExtractResponse aut_response =authenticationservice.authenticateBlueShiftUser(customerid);
			User user = aut_response.getUserinfo();
			if(user!=null)
			 books  = apptusservice.getDataForRecommendZoneForTopList(user,books);
			if(books!=null && (books.getBooks()==null || books.getBooks().size()==0) )
				books = apptusservice.getPopularBockerForNewUsers(user,books);
		}catch(Exception e){
			logger.error("getRecommnedBooksBasedOnCustomer failed.  " , e);
		}
		 //return new APIJsonWrapper(books,CONSTANTS.API_VERSION_6_4);
		 return getJsonMappingfilter(new APIJsonWrapper(books,CONSTANTS.API_VERSION_6_4), "productdetails_filter", null);
	}
	
	
	 @RequestMapping(value = "/trustly/template", method = RequestMethod.GET)
	 public ModelAndView trustlyPaymentTemplate() {
		 ModelAndView	mv = new ModelAndView("trustly_payment_template");
		 return mv;
	 }	
	 
}

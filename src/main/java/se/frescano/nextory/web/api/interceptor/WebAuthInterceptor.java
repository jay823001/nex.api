package se.frescano.nextory.web.api.interceptor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Base64;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS.WEB_TOKEN_TYPE;
import se.frescano.nextory.auth.exception.AuthenticationException;
import se.frescano.nextory.auth.rest.request.SERVICE_TYPE;
import se.frescano.nextory.auth.rest.request.TokenExtractReqBean;
import se.frescano.nextory.auth.rest.response.TokenExtractResponse;
import se.frescano.nextory.auth.service.AuthenticationServiceImpl;
import se.frescano.nextory.error.handlers.WebErrorHandler;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.web.api.exception.InSecuredRequestException;
import se.frescano.nextory.web.api.exception.WebApiException;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@DependsOn("applicationpropconfig")
@Service
public class WebAuthInterceptor extends HandlerInterceptorAdapter implements WebErrorHandler{
	private static final Logger logger = LoggerFactory.getLogger(WebAuthInterceptor.class);
 	  
    //private URL[] targetOrigin;
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	public static final String AUTHENTICATION_HEADER = "Authorization";
	
	/**
     * Name of the custom HTTP header used to transmit the CSRF token and also to prefix 
     * the CSRF cookie for the expected backend service
     */ 
	
	@Autowired
	private	AuthenticationServiceImpl		authenticationservice;
 
	public WebAuthInterceptor() throws ServletException {
		super(); 		 		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//Data Dog tracing
		Book2GoUtil.setDDTraceIds();
		
		String authCredentials 	= request.getHeader(AUTHENTICATION_HEADER);
		String inputURL 		= request.getRequestURI();
		//Double version 			= getWebAPIVersion(request.getRequestURI());
		Double version 			= Book2GoUtil.getAPIVersion(request.getRequestURI());
		String error = "";
		String accessToken  = request.getHeader(applicationpropconfig.getACCESS_TOKEN_HEADER());
		String refreshToken = request.getHeader(applicationpropconfig.getREFRESH_TOKEN_HEADER());
	    Boolean forceAuth =  Boolean.TRUE; 
	    String countrycode = null;
	    String locale = request.getHeader(CONSTANTS.LOCALE_PARAMNAME);
		if(locale==null)
			locale = request.getParameter(CONSTANTS.LOCALE_PARAMNAME);
				
		//WEB_TOKEN_EXCLUDE_URL 
		inputURL = inputURL.substring(inputURL.lastIndexOf("/"));
		WEBErrorCodeEnum errorCode = WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION;
		TokenExtractResponse	token_response 	= null;
		
		/*JWT Authentication for web ver 3.0 and above only*/
		 //if(!Arrays.asList(CONSTANTS.WEB_TOKEN_EXCLUDE_URL).contains(inputURL)){
		 try{
			// if its not production env skip the  authentication
			if (InMemoryCache.isProductionEnv) 
				authenticationservice.authenticateBasicAuth(authCredentials,applicationpropconfig.getNextoryReqAuthUsername(),applicationpropconfig.getNextoryReqAuthPassword()); 					
			
			//validate locale
			countrycode = authenticationservice.validateLocale(version, locale, SERVICE_TYPE.WEB, request); 
			
		    TokenExtractReqBean tokenRequest = new TokenExtractReqBean( SERVICE_TYPE.WEB, version,accessToken,refreshToken, CONSTANTS.SALT, forceAuth, locale,countrycode);
			
		    
		    request.setAttribute(CONSTANTS.APIVERSION, version);
		    if ( StringUtils.isNotBlank( accessToken )) { 
		    	verifyOrigin(request,response, WEB_TOKEN_TYPE.ACCESS);
		    	token_response = authenticationservice.authenticateUser(tokenRequest,request);
		    	
		    	if(token_response!=null){
			    	User			userinfo = token_response.getUserinfo();
					if(userinfo!=null && !tokenRequest.getCountrycode().equals(userinfo.getCountrycode()))
						throw new WebApiException(WEBErrorCodeEnum.INPUT_LOCALE_INVALID, "locale mismatch");
			    	request.setAttribute(CONSTANTS.USER_INFO, token_response.getUserinfo());
					if(token_response.getTokeninfo()!=null)
						request.setAttribute(CONSTANTS.WEB_AUTH_KEY, token_response.getTokeninfo().getUuid());
					
				} 	        
		    }  
		    
	 	}catch(WebApiException e){
			error = e.getErrorCodes().getMessage();	
			errorCode = e.getErrorCodes();
		} catch(AuthenticationException e){
			error = e.getMessage();
			errorCode = WEBErrorCodeEnum.AUTHENTICATION_FALIED_EXCEPTION;
			if(error==null)error=errorCode.getMessage();
		}catch(Exception e){
			error = (e.getMessage() == null)?WEBErrorCodeEnum.INVALID_AUTH_TOKEN.getMessage():e.getMessage();
		}
			   		
		 
		 
		 if( StringUtils.isNotBlank( error ) ){
		    	logger.error("Token error :: {} ",error);
		    	sendErrorMsg(response,errorCode );
		    	return false;
		 }									
		return super.preHandle(request, response, handler);
	}

	/*public boolean authenticate(String authCredentials) {

		if (null == authCredentials)
			return false;
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		boolean authenticationStatus = "nextoryui".equals(username) && "tobedecided".equals(password);
		return authenticationStatus;
	}*/
	
	 
	
	private void verifyOrigin(HttpServletRequest httpReq, HttpServletResponse httpResp, WEB_TOKEN_TYPE tokenType) throws InSecuredRequestException, MalformedURLException{
		/* STEP 1: Verifying Same Origin with Standard Headers */
        //Try to get the source from the "Origin" header
        String source = httpReq.getHeader("Origin");
        String reason = null;
        if (StringUtils.isBlank(source)) {
            //If empty then fallback on "Referer" header
            source = httpReq.getHeader("Referer");
            //If this one is empty too then we trace the event and we block the request (recommendation of the article)...
            if (StringUtils.isBlank(source)) {
            	reason = "Error: ORIGIN and REFERER request headers are both absent/empty so we block the request !";
            	logger.error(reason);
                throw new InSecuredRequestException(reason); 
            }
        }       
               
        /* STEP 2: Verifying CSRF token using "Double Submit Cookie" approach */
        //If CSRF token cookie is absent from the request then we provide one in response but we stop the process at this stage.
        //Using this way we implement the first providing of token
        if(WEB_TOKEN_TYPE.REFRESH==tokenType){
	        Cookie tokenCookie = null;
	        Cookie[] cookies = httpReq.getCookies() ;
	        if (cookies != null) {  
	            tokenCookie = Arrays.stream(httpReq.getCookies()).filter(c -> c.getName().equals(applicationpropconfig.getREFRESH_TOKEN_HEADER())).findFirst().orElse(null); 
	        	 /*  for(Cookie cookie:cookies)
	        		   logger.info("Cookie Token :: {}/{}/{}/{}", cookie.getName(),tokenCookie.getValue(),cookie.getDomain(),cookie.getPath());*/ 
	        } 
	      
	        String tokenFromHeader = httpReq.getHeader(applicationpropconfig.getREFRESH_TOKEN_HEADER());  
	        if(!StringUtils.isBlank(tokenFromHeader)){
	            //If the cookie is present then we pass to validation phase
	            //Get token from the custom HTTP header (part under control of the requester)
	        	//logger.info("Checking cookie token :: {}", tokenCookie.getValue());
	            //If empty then we trace the event and we block the request
	            if (tokenCookie == null || StringUtils.isBlank(tokenCookie.getValue())) {
	            	reason = "Error: Token provided via Cookie is absent/empty so we block the request !";
	                logger.error(reason);
	                throw new InSecuredRequestException(reason); 
	            } else  if (!tokenFromHeader.equals(tokenCookie.getValue())) {
	                //Verify that token from header and one from cookie are the same
	                //Here is not the case so we trace the event and we block the request
	            	reason = "Error: Token provided via HTTP Header and via Cookie are not equals so we block the request !";
	            	logger.error(reason);
	                throw new InSecuredRequestException(reason); 
	            } 
	        } 
        }  
	}

	public void setAuthenticationservice(AuthenticationServiceImpl authService) {
		authenticationservice = authService;
		
	}

	public ApplicationPropertiesConfig getApplicationpropconfig() {
		return applicationpropconfig;
	}

	public void setApplicationpropconfig(ApplicationPropertiesConfig applicationpropconfig) {
		this.applicationpropconfig = applicationpropconfig;
	}	
 	
}

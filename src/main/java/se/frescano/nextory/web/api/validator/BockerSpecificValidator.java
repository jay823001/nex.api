package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import se.frescano.nextory.web.api.request.BockerSpecificRequest;

@Component
public class BockerSpecificValidator extends AuthKeyValidator implements Validator { 
	
	@Override
	public boolean supports(Class<?> clazz) {
		 
		return BockerSpecificRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BockerSpecificRequest bockerSpecificRequest = (BockerSpecificRequest)target; 
       // if(bookRequest.getAuthkey()!=null && !bookRequest.getAuthkey().isEmpty())
    	validateKey(bockerSpecificRequest.getAuthkey(), errors,false);
    	//ValidationUtils.rejectIfEmptyOrWhitespace(errors,"categoryslug", "categoryslug.required", WEBErrorCodeEnum.INPUT_MISSING.name());
		 
	}

}

package se.frescano.nextory.web.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.web.api.request.AuthKeyRequestBean;
import se.frescano.nextory.web.api.request.WebApiRequestBean;
import se.frescano.nextory.web.api.util.WEBAPIURLCONSTANTS;

//@CrossOrigin
@RequestMapping(value={WEBAPIURLCONSTANTS.WEB_API_V1_BASE,WEBAPIURLCONSTANTS.WEB_API_V2_BASE})
public abstract class WebAPIBaseController {
	
	public static SimpleFilterProvider	books_exclude_filter 	= new SimpleFilterProvider().addFilter("productdetails_filter", SimpleBeanPropertyFilter.serializeAllExcept("availableinlib"));
	public static SimpleFilterProvider	searchbooks_exclude_filter 		= new SimpleFilterProvider().addFilter("productdetails_filter", SimpleBeanPropertyFilter.serializeAllExcept("availableinlib"));
	public static SimpleFilterProvider	searchbooks_exclude_facets_filter 		= 
			new SimpleFilterProvider().addFilter("productdetails_filter", SimpleBeanPropertyFilter.serializeAllExcept("availableinlib")
					).addFilter("facets_filter", SimpleBeanPropertyFilter.serializeAllExcept("name",
						    "ticket",
						    "path",
						    "description",
						    "displayName",
						    "attributes",
						    "resultType"))
			.addFilter("facetvalue_filter", SimpleBeanPropertyFilter.serializeAllExcept("ticket"));
	

	public void setDefaults(AuthKeyRequestBean requestbean, Double apiversion, HttpServletRequest request){
		if( requestbean != null ){
			requestbean.setApiversion(apiversion);				
			if( apiversion >= CONSTANTS.WEB_API_VERSION_3_0 && StringUtils.isBlank( requestbean.getAuthkey() )  
				&& request.getAttribute( CONSTANTS.WEB_AUTH_KEY ) != null )
			requestbean.setAuthkey( (String)request.getAttribute( CONSTANTS.WEB_AUTH_KEY ) );
		}
			
	}
	
	@ModelAttribute(CONSTANTS.DEFAULT_INPUTS)
	public WebApiRequestBean getCountryCode(@PathVariable("version") Double version, HttpServletRequest request){
		WebApiRequestBean defaultRequest = null;
		String countryCode = CONSTANTS.DEFAULT_COUNTRY;
		if(request.getAttribute(CONSTANTS.COUNTRYCODE_ATTRIBUTENAME)!=null)
			countryCode = (String)request.getAttribute(CONSTANTS.COUNTRYCODE_ATTRIBUTENAME);
		defaultRequest = new  WebApiRequestBean(version, countryCode);
		return defaultRequest;
	}
	
	public MappingJacksonValue getFiltered(Object responsebean, String filter_id, SimpleFilterProvider filter){		
		MappingJacksonValue mapping = new MappingJacksonValue(responsebean);
		if( filter != null )
			mapping.setFilters(filter);
		else
			mapping.setFilters(new SimpleFilterProvider().addFilter(filter_id, SimpleBeanPropertyFilter.serializeAll()));			
		return mapping;
	}
	
}

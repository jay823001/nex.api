package se.frescano.nextory.web.api.response;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"authors",
"authorsreverse",
"coverimage",
"language",
"product_key",
"publisher",
"rank",
"relevance",
"sequenceseries",
"series",
"seriesAsit",
"title"
})
public class ProductListAttributes {
		
    @JsonProperty("authors")
	private String authors;
	@JsonProperty("authorsreverse")
	private String authorsreverse;
	@JsonProperty("coverimage")
	private String coverimage;
	@JsonProperty("language")
	private String language;
	@JsonProperty("product_key")
	private String productKey;
	@JsonProperty("publisher")
	private String publisher;
	@JsonProperty("rank")
	private String rank;
	@JsonProperty("relevance")
	private String relevance;
	@JsonProperty("sequenceseries")
	private String sequenceseries;
	@JsonProperty("series")
	private String series;
	@JsonProperty("seriesAsit")
	private String seriesAsit;
	@JsonProperty("title")
	private String title;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("authors")
	public String getAuthors() {
	return authors;
	}

	@JsonProperty("authors")
	public void setAuthors(String authors) {
	this.authors = authors;
	}

	@JsonProperty("authorsreverse")
	public String getAuthorsreverse() {
	return authorsreverse;
	}

	@JsonProperty("authorsreverse")
	public void setAuthorsreverse(String authorsreverse) {
	this.authorsreverse = authorsreverse;
	}

	@JsonProperty("coverimage")
	public String getCoverimage() {
	return coverimage;
	}

	@JsonProperty("coverimage")
	public void setCoverimage(String coverimage) {
	this.coverimage = coverimage;
	}

	@JsonProperty("language")
	public String getLanguage() {
	return language;
	}

	@JsonProperty("language")
	public void setLanguage(String language) {
	this.language = language;
	}

	@JsonProperty("product_key")
	public String getProductKey() {
	return productKey;
	}

	@JsonProperty("product_key")
	public void setProductKey(String productKey) {
	this.productKey = productKey;
	}

	@JsonProperty("publisher")
	public String getPublisher() {
	return publisher;
	}

	@JsonProperty("publisher")
	public void setPublisher(String publisher) {
	this.publisher = publisher;
	}

	@JsonProperty("rank")
	public String getRank() {
	return rank;
	}

	@JsonProperty("rank")
	public void setRank(String rank) {
	this.rank = rank;
	}

	@JsonProperty("relevance")
	public String getRelevance() {
	return relevance;
	}

	@JsonProperty("relevance")
	public void setRelevance(String relevance) {
	this.relevance = relevance;
	}

	@JsonProperty("sequenceseries")
	public String getSequenceseries() {
	return sequenceseries;
	}

	@JsonProperty("sequenceseries")
	public void setSequenceseries(String sequenceseries) {
	this.sequenceseries = sequenceseries;
	}

	@JsonProperty("series")
	public String getSeries() {
	return series;
	}

	@JsonProperty("series")
	public void setSeries(String series) {
	this.series = series;
	}

	@JsonProperty("seriesAsit")
	public String getSeriesAsit() {
	return seriesAsit;
	}

	@JsonProperty("seriesAsit")
	public void setSeriesAsit(String seriesAsit) {
	this.seriesAsit = seriesAsit;
	}

	@JsonProperty("title")
	public String getTitle() {
	return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
	this.title = title;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

	
}

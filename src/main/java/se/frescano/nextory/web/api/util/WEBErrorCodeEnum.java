package se.frescano.nextory.web.api.util;

public enum WEBErrorCodeEnum {

	/*----GENDREAL ERROR'S----*/
	INTERNAL_SERVER_EXCEPTION(100,"INTERNAL_SERVER_EXCEPTION"),
	INPUT_VALIDATION_EXCEPTION(101,"INPUT_VALIDATION_EXCEPTION"),
	
	INVALID_AUTH_TOKEN(1000,"INVALID_AUTH_TOKEN"),
	NO_DATA_FOUND(1001,"NO_DATA_FOUND"),
	INPUT_MISSING(1002,"INPUT_MISSING"),
	INPUT_FORMAT_INVALID(1003,"INPUT_FORMAT_INVALID"),
	INPUT_PASSWORD_FORMAT_INVALID(1004,"INPUT_PASSWORD_FORMAT_INVALID"),
	INPUT_LOCALE_MISSING(1005,"locale is mandatory"),
	INPUT_LOCALE_INVALID(1006,"locale is not valid"),
	INPUT_LOCALE_UNSUPPORTED(1007,"locale is not supported"),
	
	/*----BOOKER PAGE ERROR'S----*/	
	BOOKGROUP_SLUGNAME_MISSTING(2001,"BOOKGROUP_SLUGNAME_MISSTING"),
	BOOKGROUP_SLUGNAME_NOT_VALID(2000,"BOOKGROUP_SLUGNAME_NOT_VALID"),	
	
	/*LOGIN PAGE ERROR'S*/	
	AUTHENTICATION_FALIED_EXCEPTION(3000,"AUTHENTICATION_FALIED_EXCEPTION"),
	USER_NOT_FOUND(3001,"USER_NOT_FOUND"),
	USER_DISABLED(3002,"USER_DISABLED"),
	INVALID_FORGOT_PASSWORD_LINK(3003,"INVALID_FORGOT_PASSWORD_LINK"),
	
	/*CAMPAIGN PAGE ERROR'S*/
	INVALID_CAMPAIGN_CODE(4000,"INVALID_CAMPAIGN_CODE"),
	
	/*GIFTCARD_PAGE ERROR'S*/
	INVALID_GIFTCARD_CODE(5000,"INVALID_GIFTCARD_CODE"),
	
	/*REGISTRATION PAGE ERROR'S*/	
	USER_ALREADY_EXSISTS(6000,"USER_ALREADY_EXSISTS"),
	
	/*SUBSCRIPTION CHANGE ERROR'S*/	
	SUBSCRIPTION_NOT_FOUND(7000,"SUBSCRIPTION_NOT_FOUND"),
	
	/*PAYMENT ERROR'S*/
	PAYMENT_FAILED_EXCEPTION(8000,"PAYMENT_FAILED_EXCEPTION"),
		
	/*JWT Token Errors*/
	ACCESS_TOKEN_EXPIRED(9000, "TOKEN_EXPIRED"),
	
	/*tele2 delete*/
	EXTERNAL_USER_DELETE_NOT_SUCCESS(2080 ,"UNSUCCESSFUL");
	
	/*
	
	INVALID_KEY(101, "Invalid Key"),
	KEY_EXPIRED(102,"Key Expired"),
	INVALID_USERNAME_PASSWORD(103, "Invalid username / password"),
	INVALID_INPUT_DATA(104,"Invalid input data"),
	MANDATORY_INPUTS_MISSING(105,"Mandatory input data is missing"),
	CATEGORY_NOT_FOUND(106, "Category not found"),
	PRODUCT_NOT_AVAILABLE(107, "Product/Book not found"),
	INVALID_CAMPAIGN_VOUCHER_CODE(108, "INVALID_CAMPAIGN_VOUCHER_CODE"),
	SERVER_EXCEPTION(109,"Unknown exception occurred"),
	USER_NOT_FOUND(110, "USER_NOT_FOUND"),
	INVALID_GIFTCARD_VOUCHER_CODE(111, "INVALID_GIFTCARD_VOUCHER_CODE"),
	USER_CREATION_FAILED(112, "Unable to create user due to unknown error"),
	NO_PRODUCTS_FOR_BOOKGROUP(113, "No more products available for bookgroup"),
	CUSTOMER_EXISTS(114, "Customer already registered"),
	PAYMENT_FAILED(115,"Payment got failed"),
	INPUT_PARAMETER_VALIDATION_FAILED(116,"Validation of input parameters failed"), 
	AUTH_KEY_CREATION_FAILED(117, "Failed to create auth key"),
	SQL_EXCEPTION(118, "Database sql exception occured"),
	GIFTCARD_DETAILS_NOT_AVAILABLE(119,"Gift card details are not available"),
	INVALID_ORDER_NO(120,"Order no is invalid or doesn't exists"),
	INVALID_PASSWORD(121,"INVALID_PASSWORD"),  
	INVALID_EMAIL_FORMAT(122,"INVALID_EMAIL_FORMAT"),
	ORDER_ALREADY_EXISTS(123,"Order no already exists"),
	ORDER_CREATION_FAILED(124,"Failed to create order"),
	INPUT_EMPTY(125,"Input is missing or empty"), 
	GIFTCARD_REDEEM_CUSTOMER_EXISTS(126, "GIFTCARD_REDEEM_CUSTOMER_EXISTS"),
	INPUT_DATA_REQUIRED(127,"INPUT_DATA_REQUIRED"),
	GIFTCARD_QUANTIY_REQUIRED(128,"GIFTCARD_QUANTIY_REQUIRED"),
	TOPLIST_CATEGORY_BOTH_SLUG_MISSING(129,"Atleast one of the slug(toplist_slug or category_slug) is required "),
	CONFIRM_PASSWORD_MISMATCH(130,"Password & confirm password is not matching"),
	EXISTING_NEW_SUBSCRIPTION_SAME(131,"Existing subscription & new subscription  to update should not be same"),
	INVALID_SUBSCRIPTION(132,"Either this subscription doesn't exists or is inactive state"),
	SUBSCRIPTION_NOT_FOUND(133,"SUBSCRIPTION_NOT_FOUND"),
	USER_MEMBERSHIP_NOT_VALID(134,"USER_MEMBERSHIP_NOT_VALID");*/
	private int code;
	private String message;
	
	private WEBErrorCodeEnum(int code, String message){
		this.code	= code;
		this.message	= message;
	}
	private WEBErrorCodeEnum(String message){ 
		this.message	= message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	  
	
}

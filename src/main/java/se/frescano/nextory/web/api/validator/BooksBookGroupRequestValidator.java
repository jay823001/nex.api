package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import se.frescano.nextory.app.product.request.BooksForBookGroupRequestBean;
import se.frescano.nextory.web.api.request.BookGroupRequestBean;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class BooksBookGroupRequestValidator extends AuthKeyValidator implements Validator {

	 
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return BooksForBookGroupRequestBean.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BooksForBookGroupRequestBean books = (BooksForBookGroupRequestBean)target; 
    	 
    	/*if(StringUtils.isEmpty(bookGroup.getToplist_slug()) && StringUtils.isEmpty(bookGroup.getCategory_slug()) )
    	errors.rejectValue("toplist_slug", "slug.required", WEBErrorCodeEnum.BOOKGROUP_SLUGNAME_MISSTING.name());*/
	}

}

package se.frescano.nextory.web.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@ControllerAdvice
@Component
public class GlobalExceptionHandler {
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
	public GenericAPIResponse handleMissingParams(MissingServletRequestParameterException ex) {
	    String name = ex.getParameterName();
	  //  System.out.println(name + " parameter is missing");
	    GenericAPIResponse apiResponse = new GenericAPIResponse(HttpStatus.BAD_REQUEST, 
	    		WEBErrorCodeEnum.INPUT_VALIDATION_EXCEPTION,  name+" is required" );
	    return apiResponse;
	    // Actual exception handling
	}
    
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
	public GenericAPIResponse handleNullPointerException(NullPointerException ne) { 
	    GenericAPIResponse apiResponse = new GenericAPIResponse(HttpStatus.INTERNAL_SERVER_ERROR, 
	    		WEBErrorCodeEnum.INTERNAL_SERVER_EXCEPTION, ne );
	    return apiResponse;
	    
	}
    
}

package se.frescano.nextory.web.api.exception;

import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

public class WebApiException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3807959623866535321L;

	private WEBErrorCodeEnum	errorCodes;
	
	
	public WebApiException(){
		super();
	}
	
	public WebApiException(String message){
		super(message);
	}
	public WebApiException(WEBErrorCodeEnum errorcode,String message){
		super(message);
		this.errorCodes	 = errorcode;		
	}
	
	public WebApiException(WEBErrorCodeEnum errorcode){
		this.errorCodes	 = errorcode;		
	}

	public WEBErrorCodeEnum getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(WEBErrorCodeEnum errorCodes) {
		this.errorCodes = errorCodes;
	}
}

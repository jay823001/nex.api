package se.frescano.nextory.web.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.web.api.service.ProductInfoService;

@RestController
public class ProductController {

	@Autowired
	private ProductInfoService productService;
	
	@RequestMapping(value={APIJSONURLCONSTANTS.FETCH_IMAGE, APIJSONURLCONSTANTS.INT_API_V2_BASE+APIJSONURLCONSTANTS.FETCH_IMAGE},method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String,Object> getProductInfoBasedOnBookId(@RequestParam("bookid") String bookid, HttpServletRequest request, HttpServletResponse response){
	   Map<String,Object> jsonMap=new HashMap<String,Object>();
   		if(bookid !=null && !bookid.equalsIgnoreCase(""))
		{
   			String bookidNew[] = bookid.split("_");
   			Integer bookidtobeworkedupon = Integer.valueOf(bookidNew[0]);
   			productService.getImageForProductBasedOnBookId(bookidtobeworkedupon, null,jsonMap);
		}
		else
		{
			jsonMap.put("bookid","INPUT MISSING");
			jsonMap.put("status", "Failed");
			jsonMap.put("description", "No Api found for your query.");
		}						
   		return jsonMap;
	}

}

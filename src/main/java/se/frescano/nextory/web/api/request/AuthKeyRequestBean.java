package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class AuthKeyRequestBean extends WebApiRequestBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3684180788849468159L;
	private String authkey;
	private String serverdetails;
	
	public String getAuthkey() {
		return authkey;
	}
	public void setAuthkey(String authkey) {
		this.authkey = authkey;
	}
	public String getServerdetails() {
		return serverdetails;
	}
	public void setServerdetails(String serverdetails) {
		this.serverdetails = serverdetails;
	}

}

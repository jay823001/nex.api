package se.frescano.nextory.web.api.request;

import java.io.Serializable;

public class BookGroupRequestBean extends RequestParamBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1080324335512508794L;
	
	/*@NotNull*/
	private String	bookgroupid;
	private String  toplist_slug;
	private String  category_slug;
	private String[] excludePublishers; 
	
	public String getBookgroupid() {
		return bookgroupid;
	}
	public void setBookgroupid(String bookgroupid) {
		this.bookgroupid = bookgroupid;
	}
	public String getCategory_slug() {
		return category_slug;
	}
	public void setCategory_slug(String category_slug) {
		this.category_slug = category_slug;
	}
	public String getToplist_slug() {
		return toplist_slug;
	}
	public void setToplist_slug(String toplist_slug) {
		this.toplist_slug = toplist_slug;
	}
	
	public String[] getExcludePublishers() {
		return excludePublishers;
	}
	public void setExcludePublishers(String[] excludePublishers) {
		this.excludePublishers = excludePublishers;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BookGroupRequestBean [");
		if (bookgroupid != null) {
			builder.append("bookgroupid=");
			builder.append(bookgroupid);
			builder.append(", ");
		}
		if (toplist_slug != null) {
			builder.append("toplist_slug=");
			builder.append(toplist_slug);
			builder.append(", ");
		}
		if (category_slug != null) {
			builder.append("category_slug=");
			builder.append(category_slug);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	  
}

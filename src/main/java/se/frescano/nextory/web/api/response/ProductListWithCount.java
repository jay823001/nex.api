package se.frescano.nextory.web.api.response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"name",
"ticket",
"path",
"displayName",
"description",
"attributes",
"resultType",
"count",
"products"
})
public class ProductListWithCount {



	@JsonProperty("name")
	private String name;
	@JsonProperty("ticket")
	private String ticket;
	@JsonProperty("path")
	private String path;
	@JsonProperty("displayName")
	private String displayName;
	@JsonProperty("description")
	private String description;
	@JsonProperty("attributes")
	private Attributes attributes;
	@JsonProperty("resultType")
	private String resultType;
	@JsonProperty("count")
	private Integer count;
	@JsonProperty("products")
	private List<Product> products = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("name")
	public String getName() {
	return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
	this.name = name;
	}

	@JsonProperty("ticket")
	public String getTicket() {
	return ticket;
	}

	@JsonProperty("ticket")
	public void setTicket(String ticket) {
	this.ticket = ticket;
	}

	@JsonProperty("path")
	public String getPath() {
	return path;
	}

	@JsonProperty("path")
	public void setPath(String path) {
	this.path = path;
	}

	@JsonProperty("displayName")
	public String getDisplayName() {
	return displayName;
	}

	@JsonProperty("displayName")
	public void setDisplayName(String displayName) {
	this.displayName = displayName;
	}

	@JsonProperty("description")
	public String getDescription() {
	return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
	this.description = description;
	}

	@JsonProperty("attributes")
	public Attributes getAttributes() {
	return attributes;
	}

	@JsonProperty("attributes")
	public void setAttributes(Attributes attributes) {
	this.attributes = attributes;
	}

	@JsonProperty("resultType")
	public String getResultType() {
	return resultType;
	}

	@JsonProperty("resultType")
	public void setResultType(String resultType) {
	this.resultType = resultType;
	}

	@JsonProperty("count")
	public Integer getCount() {
	return count;
	}

	@JsonProperty("count")
	public void setCount(Integer count) {
	this.count = count;
	}

	@JsonProperty("products")
	public List<Product> getProducts() {
	return products;
	}

	@JsonProperty("products")
	public void setProducts(List<Product> products) {
	this.products = products;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

	
}

package se.frescano.nextory.web.api.response;

import java.io.Serializable;

public class UICategoryVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7843051705898762785L;
	
	private Integer categoryid;
	private Integer parentcatid;
	private String categoryname;
	private Boolean isactive;
	private String slugname;
	private Boolean popular;
	private int position;
	
	public UICategoryVO(){
		
	}
	
	public UICategoryVO(Integer categoryId, Integer parentCatId, String categoryName, Boolean isactive, String slugName,
			 Boolean popular, int position){
		this.categoryid 	= categoryId;
		this.parentcatid	= parentCatId;
		this.categoryname 	= categoryName;
		this.isactive		= isactive;
		this.slugname		= slugName;
		this.popular		= popular;
		this.position		= position;
	}

	public Integer getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(Integer categoryid) {
		this.categoryid = categoryid;
	}

	public Integer getParentcatid() {
		return parentcatid;
	}

	public void setParentcatid(Integer parentcatid) {
		this.parentcatid = parentcatid;
	}

	public String getCategoryname() {
		return categoryname;
	}

	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public String getSlugname() {
		return slugname;
	}

	public void setSlugname(String slugname) {
		this.slugname = slugname;
	}

	public Boolean getPopular() {
		return popular;
	}

	public void setPopular(Boolean popular) {
		this.popular = popular;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	 
	
}

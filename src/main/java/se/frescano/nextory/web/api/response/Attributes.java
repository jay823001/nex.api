package se.frescano.nextory.web.api.response;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"Nyheter_pos",
"baseprice",
"bookid",
"booklength",
"createddate",
"description",
"filesize",
"formattype",
"originalpublicationdate",
"popularity",
"productstatus",
"providerproductid",
"publisheddate",
"relatedproductid",
"relatedsubscriptionorder",
"subscriptionorder",
"variant_key",
"vi_rekommenderar_pos",
"narrators",
"narratorsreverse",
"imageversion",
"imageheight",
"imagewidth",
"avgrate",
"numberofrates"

})
public class Attributes {

	@JsonProperty("Nyheter_pos")
	private String nyheterPos;
	@JsonProperty("baseprice")
	private String baseprice;
	@JsonProperty("bookid")
	private String bookid;
	@JsonProperty("booklength")
	private String booklength;
	@JsonProperty("createddate")
	private String createddate;
	@JsonProperty("description")
	private String description;
	@JsonProperty("filesize")
	private String filesize;
	@JsonProperty("formattype")
	private String formattype;
	@JsonProperty("originalpublicationdate")
	private String originalpublicationdate;
	@JsonProperty("popularity")
	private String popularity;
	@JsonProperty("productstatus")
	private String productstatus;
	@JsonProperty("providerproductid")
	private String providerproductid;
	@JsonProperty("publisheddate")
	private String publisheddate;
	@JsonProperty("relatedproductid")
	private String relatedproductid;
	@JsonProperty("relatedsubscriptionorder")
	private String relatedsubscriptionorder;
	@JsonProperty("subscriptionorder")
	private String subscriptionorder;
	@JsonProperty("variant_key")
	private String variantKey;
	@JsonProperty("vi_rekommenderar_pos")
	private String viRekommenderarPos;
	@JsonProperty("narrators")
	private String narrators;
	@JsonProperty("narratorsreverse")
	private String narratorsreverse;
	@JsonProperty("imageversion")
	private String imageversion;
	@JsonProperty("imageheight")
	private String imageheight;
	@JsonProperty("imagewidth")
	private String imagewidth;
	@JsonProperty("avgrate")
	private Double avgrate;
	@JsonProperty("numberofrates")
	private Integer numberofrates;
	
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Nyheter_pos")
	public String getNyheterPos() {
	return nyheterPos;
	}

	@JsonProperty("Nyheter_pos")
	public void setNyheterPos(String nyheterPos) {
	this.nyheterPos = nyheterPos;
	}

	@JsonProperty("baseprice")
	public String getBaseprice() {
	return baseprice;
	}

	@JsonProperty("baseprice")
	public void setBaseprice(String baseprice) {
	this.baseprice = baseprice;
	}

	@JsonProperty("bookid")
	public String getBookid() {
	return bookid;
	}

	@JsonProperty("bookid")
	public void setBookid(String bookid) {
	this.bookid = bookid;
	}

	@JsonProperty("booklength")
	public String getBooklength() {
	return booklength;
	}

	@JsonProperty("booklength")
	public void setBooklength(String booklength) {
	this.booklength = booklength;
	}

	@JsonProperty("createddate")
	public String getCreateddate() {
	return createddate;
	}

	@JsonProperty("createddate")
	public void setCreateddate(String createddate) {
	this.createddate = createddate;
	}

	@JsonProperty("description")
	public String getDescription() {
	return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
	this.description = description;
	}

	@JsonProperty("filesize")
	public String getFilesize() {
	return filesize;
	}

	@JsonProperty("filesize")
	public void setFilesize(String filesize) {
	this.filesize = filesize;
	}

	@JsonProperty("formattype")
	public String getFormattype() {
	return formattype;
	}

	@JsonProperty("formattype")
	public void setFormattype(String formattype) {
	this.formattype = formattype;
	}

	@JsonProperty("originalpublicationdate")
	public String getOriginalpublicationdate() {
	return originalpublicationdate;
	}

	@JsonProperty("originalpublicationdate")
	public void setOriginalpublicationdate(String originalpublicationdate) {
	this.originalpublicationdate = originalpublicationdate;
	}

	@JsonProperty("popularity")
	public String getPopularity() {
	return popularity;
	}

	@JsonProperty("popularity")
	public void setPopularity(String popularity) {
	this.popularity = popularity;
	}

	@JsonProperty("productstatus")
	public String getProductstatus() {
	return productstatus;
	}

	@JsonProperty("productstatus")
	public void setProductstatus(String productstatus) {
	this.productstatus = productstatus;
	}

	@JsonProperty("providerproductid")
	public String getProviderproductid() {
	return providerproductid;
	}

	@JsonProperty("providerproductid")
	public void setProviderproductid(String providerproductid) {
	this.providerproductid = providerproductid;
	}

	@JsonProperty("publisheddate")
	public String getPublisheddate() {
	return publisheddate;
	}

	@JsonProperty("publisheddate")
	public void setPublisheddate(String publisheddate) {
	this.publisheddate = publisheddate;
	}

	@JsonProperty("relatedproductid")
	public String getRelatedproductid() {
	return relatedproductid;
	}

	@JsonProperty("relatedproductid")
	public void setRelatedproductid(String relatedproductid) {
	this.relatedproductid = relatedproductid;
	}

	@JsonProperty("relatedsubscriptionorder")
	public String getRelatedsubscriptionorder() {
	return relatedsubscriptionorder;
	}

	@JsonProperty("relatedsubscriptionorder")
	public void setRelatedsubscriptionorder(String relatedsubscriptionorder) {
	this.relatedsubscriptionorder = relatedsubscriptionorder;
	}

	@JsonProperty("subscriptionorder")
	public String getSubscriptionorder() {
	return subscriptionorder;
	}

	@JsonProperty("subscriptionorder")
	public void setSubscriptionorder(String subscriptionorder) {
	this.subscriptionorder = subscriptionorder;
	}

	@JsonProperty("variant_key")
	public String getVariantKey() {
	return variantKey;
	}

	@JsonProperty("variant_key")
	public void setVariantKey(String variantKey) {
	this.variantKey = variantKey;
	}

	@JsonProperty("vi_rekommenderar_pos")
	public String getViRekommenderarPos() {
	return viRekommenderarPos;
	}

	@JsonProperty("vi_rekommenderar_pos")
	public void setViRekommenderarPos(String viRekommenderarPos) {
	this.viRekommenderarPos = viRekommenderarPos;
	}

	@JsonProperty("narrators")
	public String getNarrators() {
	return narrators;
	}

	@JsonProperty("narrators")
	public void setNarrators(String narrators) {
	this.narrators = narrators;
	}

	@JsonProperty("narratorsreverse")
	public String getNarratorsreverse() {
	return narratorsreverse;
	}

	@JsonProperty("narratorsreverse")
	public void setNarratorsreverse(String narratorsreverse) {
	this.narratorsreverse = narratorsreverse;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

	@JsonProperty("imageversion")
	public String getImageversion() {
		return imageversion;
	}

	@JsonProperty("imageversion")
	public void setImageversion(String imageversion) {
		this.imageversion = imageversion;
	}

	@JsonProperty("imageheight")
	public String getImageheight() {
		return imageheight;
	}

	@JsonProperty("imageheight")
	public void setImageheight(String imageheight) {
		this.imageheight = imageheight;
	}

	@JsonProperty("imagewidth")
	public String getImagewidth() {
		return imagewidth;
	}

	@JsonProperty("imagewidth")
	public void setImagewidth(String imagewidth) {
		this.imagewidth = imagewidth;
	}

	@JsonProperty("avgrate")
	public Double getAvgrate() {
		return avgrate;
	}

	@JsonProperty("avgrate")
	public void setAvgrate(Double avgrate) {
		this.avgrate = avgrate;
	}

	@JsonProperty("numberofrates")
	public Integer getNumberofrates() {
		return numberofrates;
	}

	@JsonProperty("numberofrates")
	public void setNumberofrates(Integer numberofrates) {
		this.numberofrates = numberofrates;
	}
	
	
}

package se.frescano.nextory.web.api.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@JsonInclude(Include.NON_EMPTY)
public class WebAPIResponseError implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8670652394049809749L;
	private int code;
	private String name;
	private String msg;
	private Object details;
	
	public WebAPIResponseError(WEBErrorCodeEnum error) {
		super();
		this.code = error.getCode();
		//this.name = error.toString();
		this.msg = error.getMessage(); 
	}
	public WebAPIResponseError(WEBErrorCodeEnum error, String details) {
		super();
		this.code = error.getCode();
		//this.name = error.toString();
		this.msg = error.getMessage(); 
		this.details = details;
	}
	
	public WebAPIResponseError(WEBErrorCodeEnum error, List<FieldError> details) {
		super();
		this.code = error.getCode();
		this.msg = error.getMessage();
		//this.name = error.toString();
		Map<String, String> fieldList = new HashMap<String, String>(); 
		for(FieldError fieldError : details)
			fieldList.put(fieldError.getField(), fieldError.getDefaultMessage());
		this.details = fieldList;
	}
	
	
	
	public WebAPIResponseError() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getCode() {
		return code;
	}
	public void setCode(int errorCode) {
		this.code = errorCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String message) {
		this.msg = message;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}

package se.frescano.nextory.web.api.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.mongo.product.model.PublisherData;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CampaignTypeInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3299835444692307398L;

	@JsonProperty("campaigntypeid")
	private Integer campaignTypeId;
	
	@JsonProperty("campaigntypename")
	private String campaignTypeName;
	
	@JsonProperty("campaigntypestatus")
	private Boolean campaignTypeStatus;
	
	private List<PublisherData> publishers;
	public Integer getCampaignTypeId() {
		return campaignTypeId;
	}
	public void setCampaignTypeId(Integer campaignTypeId) {
		this.campaignTypeId = campaignTypeId;
	}
	public String getCampaignTypeName() {
		return campaignTypeName;
	}
	public void setCampaignTypeName(String campaignTypeName) {
		this.campaignTypeName = campaignTypeName;
	}
	public List<PublisherData> getPublishers() {
		return publishers;
	}
	public void setPublishers(List<PublisherData> publishers) {
		this.publishers = publishers;
	}
	public Boolean getCampaignTypeStatus() {
		return campaignTypeStatus;
	}
	public void setCampaignTypeStatus(Boolean campaignTypeStatus) {
		this.campaignTypeStatus = campaignTypeStatus;
	}

}

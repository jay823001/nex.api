package se.frescano.nextory.web.api.request;

public class WebApiRequestBean {
	
	
	private Double	apiversion;
	protected String countryCode;
	
	public WebApiRequestBean(){
		super();
	}
	public WebApiRequestBean(Double apiversion, String countryCode) {
		super();
		this.apiversion = apiversion;
		this.countryCode = countryCode;
	}

	public Double getApiversion() {
		return apiversion;
	}

	public void setApiversion(Double apiversion) {
		this.apiversion = apiversion;
	}
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public void setDefaults(WebApiRequestBean defaultBean) {
		if(defaultBean!=null){
			this.countryCode = defaultBean.getCountryCode();
			this.apiversion = defaultBean.getApiversion();
		}
	}

}

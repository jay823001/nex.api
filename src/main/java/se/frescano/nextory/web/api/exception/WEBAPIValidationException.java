package se.frescano.nextory.web.api.exception;

import java.util.List;

import org.springframework.validation.FieldError;

import se.frescano.nextory.web.api.exception.WebApiException;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

public class WEBAPIValidationException extends WebApiException {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1659317346994583665L;
	private List<FieldError> errorFields;
	
	public WEBAPIValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	public WEBAPIValidationException() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WEBAPIValidationException(List<FieldError> errorFields) {
		super();
		this.errorFields = errorFields;
	}
	public WEBAPIValidationException(WEBErrorCodeEnum errorEnum) {
		super();
		this.setErrorCodes(errorEnum);
	}
	public List<FieldError> getErrorFields() {
		return errorFields;
	}

	public void setErrorFields(List<FieldError> errorFields) {
		this.errorFields = errorFields;
	}
}

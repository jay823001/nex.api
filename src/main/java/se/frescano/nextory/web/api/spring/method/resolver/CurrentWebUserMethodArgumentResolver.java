package se.frescano.nextory.web.api.spring.method.resolver;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.InMemoryCache;


public class CurrentWebUserMethodArgumentResolver implements HandlerMethodArgumentResolver {
	private Logger logger = LoggerFactory.getLogger(CurrentWebUserMethodArgumentResolver.class);
	//private static final Logger logger = LoggerFactory.getLogger(CurrentWebUserMethodArgumentResolver.class);
     public boolean supportsParameter(MethodParameter methodParameter) {
         return
             methodParameter.getParameterAnnotation(WebAPIUser.class) != null
             && methodParameter.getParameterType().equals(User.class);
    }

    
    public Object resolveArgument(MethodParameter methodParameter,
                        ModelAndViewContainer mavContainer,
                        NativeWebRequest webRequest,
                        WebDataBinderFactory binderFactory) throws Exception {
    	
    	
         if (this.supportsParameter(methodParameter)) { 
        	// String authkey = null;
        	 HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
        	 Double webAPIVersion = (Double)request.getAttribute(CONSTANTS.APIVERSION);
        	/* if(version!=null && version>=CONSTANTS.WEB_API_VERSION_3_0)
        		 authkey = (String) request.getAttribute(CONSTANTS.WEB_AUTH_KEY);
        	 else {
        		 authkey = webRequest.getParameter(CONSTANTS.WEB_AUTH_KEY);
        	 }        	 
        	User userObj = null; 
        	try{
         		userObj = CiperTokenService.decryptWebAuthKey(authkey, version);
         	}catch(Exception e){
         		return new GenericAPIResponse(HttpStatus.BAD_REQUEST, WEBErrorCodeEnum.INVALID_AUTH_TOKEN,  e);
         	}  */ 
            //if(logger.isTraceEnabled())logger.trace(" ");
        	 User userObj = (User)request.getAttribute(CONSTANTS.USER_INFO);
        	 
        	 if(webAPIVersion!=null && webAPIVersion >=CONSTANTS.WEB_API_VERSION_6_0 && userObj==null){
        		 userObj = InMemoryCache.defaultUser;
        		 String countryCode = CONSTANTS.DEFAULT_COUNTRY;
        			if(request.getAttribute(CONSTANTS.COUNTRYCODE_ATTRIBUTENAME)!=null)
        				countryCode = (String)request.getAttribute(CONSTANTS.COUNTRYCODE_ATTRIBUTENAME);
        			userObj.setCountrycode(countryCode);	
        		/* if(userObj==null)
        					throw new  APIException(ErrorCodes.INTERNAL_ERROR, "User Object is null");*/
        	 }	 
        		 
        	 return userObj;
         } else {
             return WebArgumentResolver.UNRESOLVED;
         }
    }
    
    	
	/*private Double getWebAPIVersion(String url){
		Double version 	= null;
		String urlPrefix = "/api/web/";
		if( StringUtils.contains(url, urlPrefix) ){
			url	= StringUtils.substring( url, (StringUtils.indexOf(url, (urlPrefix)) + (urlPrefix.length())), (url.length() - 1));
			url	= StringUtils.substring( url, 0,StringUtils.indexOf(url, "/"));
			 
			try{
				version	= Double.parseDouble(url); 
			}catch(Exception e){
				version = null;
				if(logger.isDebugEnabled())logger.error("web url version error ! ",e); 
			}			
		}
		return version;
	}*/
}

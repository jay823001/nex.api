package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import se.frescano.nextory.web.api.request.CategoryViewAllRequest;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class CategoryViewAllValidator extends AuthKeyValidator implements Validator { 
	
	@Override
	public boolean supports(Class<?> clazz) {
		 
		return CategoryViewAllRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CategoryViewAllRequest categorySpecificRequest = (CategoryViewAllRequest)target; 
       // if(bookRequest.getAuthkey()!=null && !bookRequest.getAuthkey().isEmpty())
    	validateKey(categorySpecificRequest.getAuthkey(), errors,false);
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors,"catslug", "categoryslug.required", WEBErrorCodeEnum.INPUT_MISSING.name());
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors,"subcatslug", "Subcategoryslug.required", WEBErrorCodeEnum.INPUT_MISSING.name());
		 
	}

}

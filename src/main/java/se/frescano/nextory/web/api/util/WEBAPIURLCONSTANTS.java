package se.frescano.nextory.web.api.util;

public class WEBAPIURLCONSTANTS {

	public static final String WEB_API_V1_BASE = "/api/web/{version:4.0|5.0}";
	public static final String WEB_API_V2_BASE = "/api/web/product/{version:6.0}";
	
	public static final String BOCKER_FIRST_SCREEN ="/bocker";
	
	public static final String CATEGORY_VIEW_ALL_LEVEL3 = "/categoryViewAll";
	
	public static final String BOOK_DETAILS ="/book";
	
	public static final String SEACRH = "/search";
	
	public static final String AUTOCOMPLETE ="/suggest";
	
	public static final String MAIN_CATEGORY_LEVEL2_SCREEN ="/categorySpecificLoadStep";
	
	public static final String LEVEL1_SCREEN_BOCKER_EBOCKER_ABOCKER = "/MaincategorySpecificLoadStep";
	
	/**New set of APIs inherited from APP APIs */
	public static final String BOOK_GROUPS="/groups";
	public static final String BOOKS_FOR_BOOKGROUPS="/booksforbookgroup";
	public static final String BOOK_SEARCH="/searchbooks";
	public static final String BOOK_SEARCH_FACETS="/searchbooksfacets";
	public static final String BOOK_ONLY_FACETS="/facets";
	public static final String BOOK_SEARCH_ADVANCED="/advancedsearchbooks";
	public static final String BOOK_GROUPS_SLUGNAME="/groups/{slugname}";
	public static final String BOOKS_FOR_BOOKGROUPS_SLUGNAME="/booksforbookgroup/{slugname}";
}

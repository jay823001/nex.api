package se.frescano.nextory.web.api.response;

import java.util.List;

public class CategoryMasterWrapper {

	private List<UICategoryVO> categories;
	
	public CategoryMasterWrapper(){
		
	}
	
	public CategoryMasterWrapper(List<UICategoryVO> categories){
		this.categories	= categories;
	}

	public List<UICategoryVO> getCategories() {
		return categories;
	}

	public void setCategories(List<UICategoryVO> categories) {
		this.categories = categories;
	}
}

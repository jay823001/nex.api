package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import se.frescano.nextory.apptus.model.Completions;

public class SearchSuggestionWrapper  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6544212788975228068L;
	private	String keyword;
	private List<UIBook> books;
	private ArrayList<Completions> completions;
	
	
	public SearchSuggestionWrapper(){
		
	}
	
	public SearchSuggestionWrapper(String keyword, List<UIBook> books){
		this.keyword	= keyword;
		this.books	= books;
	}
	
	public SearchSuggestionWrapper(String keyword, ArrayList<Completions> completions){
		this.keyword	= keyword;
		this.completions	= completions;
	}
	
	

	public ArrayList<Completions> getCompletions() {
		return completions;
	}

	public void setCompletions(ArrayList<Completions> completions) {
		this.completions = completions;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<UIBook> getBooks() {
		return books;
	}

	public void setBooks(List<UIBook> books) {
		this.books = books;
	}
		
}

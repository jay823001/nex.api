package se.frescano.nextory.web.api.response;

public class TopBookVO {

	private int 	productid;
	private String 	title;
	private String 	isbn;
	private String 	coverUrl;
	private Integer relProductid;
	private String 	productFormat;
	private String description;
	
	public TopBookVO(int 	productid, 
					 String 	title, 
					 String 	isbn, 
					 String 	coverUrl, 
					 Integer relProductid, 
					 String 	productFormat, 
					 String description){
		this.productid	= productid;
		this.title		= title;
		this.isbn		= isbn;
		this.coverUrl	= coverUrl;
		this.relProductid	= relProductid;
		this.productFormat = productFormat;
		this.description	= description;
	}
	
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getCoverUrl() {
		return coverUrl;
	}
	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}
	public Integer getRelProductid() {
		return relProductid;
	}
	public void setRelProductid(Integer relProductid) {
		this.relProductid = relProductid;
	}
	public String getProductFormat() {
		return productFormat;
	}
	public void setProductFormat(String productFormat) {
		this.productFormat = productFormat;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
		
}

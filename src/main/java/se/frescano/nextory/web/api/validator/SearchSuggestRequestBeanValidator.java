package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import se.frescano.nextory.app.product.request.SearchJsonApptusRequestBean;
import se.frescano.nextory.web.api.request.SearchSuggestRequestBean;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class SearchSuggestRequestBeanValidator extends AuthKeyValidator implements Validator {
 
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return SearchSuggestRequestBean.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SearchSuggestRequestBean bookGroup = (SearchSuggestRequestBean)target; 
    	validateKey(bookGroup.getAuthkey(), errors, false);
    	 
    	 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "keyword", "error.keyword", WEBErrorCodeEnum.INPUT_MISSING.name()); 
    	
	}
	
	public void validate_v2(Object target, Errors errors) {
		SearchJsonApptusRequestBean bookGroup = (SearchJsonApptusRequestBean)target; 
    	 
    	/* ValidationUtils.rejectIfEmptyOrWhitespace(errors, "keyword", "error.keyword", WEBErrorCodeEnum.INPUT_MISSING.name());*/ 
    	
	}
	
}

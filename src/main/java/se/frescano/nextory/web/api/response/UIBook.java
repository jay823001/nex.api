package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.model.ContributorSlim;
import se.frescano.nextory.mongo.product.model.Ratings;


@JsonInclude(Include.NON_NULL)
public class UIBook implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2779412232016353403L;
	private Long bookid;
	private Long libid;
	private String coverimg;
	private List<ContributorSlim> contributors; 
	private Boolean pdfReader;
	private String status;
	private String purchasetype;
	private Boolean streamingavailable;
	private Long relatedbookid;
	private UIBook relatedbook;
	
	private Long productid;
	/*private Integer format;*/
	private Boolean allowedinlibrary; 
	private String isbn;
	private String title;  
	private String weburl;
	Set<String> authors;
	List<String> narattors;
	List<String> translators;
	private String publisher;
	private String description;
	private ProductFormatEnum	formattype;
	/*@JsonFormat(shape=JsonFormat.Shape.STRING, timezone="UTC", pattern=CONSTANTS.SHORT_DATE_FORMAT)*/
	private String publishdate;
	private Integer publishyear;
	private String bookallowedfor;
	private String language;
	private String sequence;
	private String seriesname;
	private String rank;
	private String relevance;
	private Integer bookLengthValue;
	private Ratings ratings;
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getSeriesname() {
		return seriesname;
	}

	public void setSeriesname(String seriesname) {
		this.seriesname = seriesname;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Set<String> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<String> authors) {
		this.authors = authors;
	}

	public Long getProductid() {
		return productid;
	}

	public void setProductid(Long productid) {
		this.productid = productid;
	}

 

	/*public void setFormat(Integer format) {
		this.format = format;
	}*/

	public Boolean getAllowedinlibrary() {
		return allowedinlibrary;
	}

	public void setAllowedinlibrary(Boolean allowedinlibrary) {
		this.allowedinlibrary = allowedinlibrary;
	}

  
	

	public UIBook() {
		// TODO Auto-generated constructor stub
	}

	public UIBook(Long bookid, String titile, String isbn, String coverimg, String weburl, Long relatedbookid) {
		this.bookid	= bookid;
		//this.format	= format;
		this.title	= titile;
		this.isbn	= isbn;
		this.coverimg	= coverimg;
		this.weburl	= weburl;
		this.relatedbookid	= relatedbookid;
	}
	
	public Long getBookid() {
		return bookid;
	}

	public void setBookid(Long bookid) {
		this.bookid = bookid;
	}

	public Long getLibid() {
		return libid;
	}

	public void setLibid(Long libid) {
		this.libid = libid;
	}

	public String getCoverimg() {
		return coverimg;
	}

	public void setCoverimg(String coverimg) {
		this.coverimg = coverimg;
	}

	public List<ContributorSlim> getContributors() {
		return contributors;
	}

	public void setContributors(List<ContributorSlim> contributors) {
		this.contributors = contributors;
	}

 

	public Boolean getPdfReader() {
		return pdfReader;
	}

	public void setPdfReader(Boolean pdfReader) {
		this.pdfReader = pdfReader;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPurchasetype() {
		return purchasetype;
	}

	public void setPurchasetype(String purchasetype) {
		this.purchasetype = purchasetype;
	}

	public Boolean getStreamingavailable() {
		return streamingavailable;
	}

	public void setStreamingavailable(Boolean streamingavailable) {
		this.streamingavailable = streamingavailable;
	}

	public Long getRelatedbookid() {
		return relatedbookid;
	}

	public void setRelatedbookid(Long relatedbookid) {
		this.relatedbookid = relatedbookid;
	}
	public String toString(){
	 
		return "UIBook [bookid='"+bookid+"', libid="+libid+", coverimg="+coverimg+" , contributors="+contributors+", "
				+ ", pdfReader="+pdfReader+", status="+status+", purchasetype="+purchasetype+","
						+ " streamingavailable="+streamingavailable+", relatedbookid="+relatedbookid+"]";
	}

	 

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getWeburl() {
		return weburl;
	}

	public void setWeburl(String weburl) {
		this.weburl = weburl;
	}

	/*public Integer getFormat() {
		return format;
	}*/

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPublishdate() {
		return publishdate;
	}

	public void setPublishdate(String publishdate) {
		this.publishdate = publishdate;
	}

	public String getBookallowedfor() {
		return bookallowedfor;
	}

	public void setBookallowedfor(String bookallowedfor) {
		this.bookallowedfor = bookallowedfor;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public ProductFormatEnum getFormattype() {
		return formattype;
	}

	public void setFormattype(ProductFormatEnum formattype) {
		this.formattype = formattype;
	}

	public Integer getPublishyear() {
		return publishyear;
	}

	public void setPublishyear(Integer publishyear) {
		this.publishyear = publishyear;
	}

	public List<String> getNarattors() {
		return narattors;
	}

	public void setNarattors(List<String> narattors) {
		this.narattors = narattors;
	}

	public List<String> getTranslators() {
		return translators;
	}

	public void setTranslators(List<String> translators) {
		this.translators = translators;
	}

	public UIBook getRelatedbook() {
		return relatedbook;
	}

	public void setRelatedbook(UIBook relatedbook) {
		this.relatedbook = relatedbook;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getRelevance() {
		return relevance;
	}

	public void setRelevance(String relevance) {
		this.relevance = relevance;
	}
	
	public Integer getBookLengthValue() {
		return bookLengthValue;
	}

	public void setBookLengthValue(Integer bookLengthValue) {
		this.bookLengthValue = bookLengthValue;
	}

	public Ratings getRatings() {
		return ratings;
	}

	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}
	
	
}

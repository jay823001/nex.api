package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import se.frescano.nextory.app.product.request.SearchJsonApptusRequestBean;
import se.frescano.nextory.web.api.request.SearchBooksRequestBean;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class SearchBooksRequestBeanValidator extends AuthKeyValidator implements Validator {

	 
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return SearchBooksRequestBean.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SearchBooksRequestBean bookGroup = (SearchBooksRequestBean)target; 
    	validateKey(bookGroup.getAuthkey(), errors, false);
    	
    	 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "error.type", WEBErrorCodeEnum.INPUT_MISSING.name()); 
    	 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "keyword", "error.keyword", WEBErrorCodeEnum.INPUT_MISSING.name()); 
    	
	}
	
	 
	public void validate_v2(Object target, Errors errors) {
		SearchJsonApptusRequestBean bookGroup = (SearchJsonApptusRequestBean)target; 
    	//validateKey(bookGroup.getAuthkey(), errors, false);
    	
    /*	 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "error.type", WEBErrorCodeEnum.INPUT_MISSING.name()); 
    	 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "keyword", "error.keyword", WEBErrorCodeEnum.INPUT_MISSING.name());*/ 
    	
	}
	public void validate_facets(Object target, Errors errors) {
		SearchJsonApptusRequestBean bookGroup = (SearchJsonApptusRequestBean)target; 
    	
	}
	public void validate_only_facets(Object target, Errors errors) {
		SearchJsonApptusRequestBean bookGroup = (SearchJsonApptusRequestBean)target; 
    	
	}
}

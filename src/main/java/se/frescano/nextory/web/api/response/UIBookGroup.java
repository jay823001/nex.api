package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.frescano.nextory.app.constants.ProductFormatEnum;

@JsonInclude(Include.NON_EMPTY)
public class UIBookGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6914916839038932982L;
	
	private Integer bookgroupid;
	private String name;
	private String slug;
	private Boolean iscategory;
	private Boolean isparentcategory;
	private ProductFormatEnum formattype;
	private String displaymode;
	private String sorton;
	private Integer	parentid;
	private String parentcategoryname;	
	private List<UIBook> books;
	private List<String> toptitles;
	
	
	public UIBookGroup() {
		// TODO Auto-generated constructor stub
	}
	

	public UIBookGroup(Integer bookgroupid, String name, String slug, Boolean iscategory, Boolean isparentcategory) {
		super();
		this.bookgroupid=bookgroupid;
		this.name = name;
		this.slug = slug;
		this.iscategory = iscategory;
		this.isparentcategory = isparentcategory; 
	}
	public UIBookGroup(Integer bookgroupid, String name, String slug, Boolean iscategory, Boolean isparentcategory, String sorton) {
		super();
		this.bookgroupid=bookgroupid;
		this.name = name;
		this.slug = slug;
		this.iscategory = iscategory;
		this.isparentcategory = isparentcategory; 
		this.sorton = sorton;
	}

	public UIBookGroup(Integer bookgroupid, String name, String slug, String mode) {
		// TODO Auto-generated constructor stub
		super();
		this.bookgroupid=bookgroupid;
		this.name = name;
		this.slug = slug;
		this.displaymode=mode;
	}

	public UIBookGroup(Integer bookgroupid, String name, Integer parentid, List<UIBook> books){
		super();
		this.bookgroupid=bookgroupid;
		this.name	= name;
		this.parentid	= parentid;
		this.books	= books;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Boolean getIscategory() {
		return iscategory;
	}

	public void setIscategory(Boolean iscategory) {
		this.iscategory = iscategory;
	}

	public Boolean getIsparentcategory() {
		return isparentcategory;
	}

	public void setIsparentcategory(Boolean isparentcategory) {
		this.isparentcategory = isparentcategory;
	}

	 

	public List<UIBook> getBooks() {
		return books;
	}

	public void setBooks(List<UIBook> books) {
		this.books = books;
	}
	
	public String toString(){
		
		return "UIBookGroup=[id="+bookgroupid+", name='"+name+"', slug="+slug+", iscategory="+iscategory+" , isParentcategory="+isparentcategory+", books="+books+" ]";
	}


	public Integer getBookgroupid() {
		return bookgroupid;
	}


	public void setBookgroupid(Integer bookgroupid) {
		this.bookgroupid = bookgroupid;
	}


	public String getDisplaymode() {
		return displaymode;
	}


	public void setDisplaymode(String displaymode) {
		this.displaymode = displaymode;
	}


	public String getSorton() {
		return sorton;
	}


	public void setSorton(String sorton) {
		this.sorton = sorton;
	}


	public ProductFormatEnum getFormattype() {
		return formattype;
	}


	public void setFormattype(ProductFormatEnum formattype) {
		this.formattype = formattype;
	}


	public Integer getParentid() {
		return parentid;
	}


	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}


	public String getParentcategoryname() {
		return parentcategoryname;
	}


	public void setParentcategoryname(String parentcategoryname) {
		this.parentcategoryname = parentcategoryname;
	}


	public List<String> getToptitles() {
		return toptitles;
	}


	public void setToptitles(List<String> toptitles) {
		this.toptitles = toptitles;
	}
	
}

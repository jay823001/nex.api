package se.frescano.nextory.web.api.response;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.frescano.nextory.app.constants.ProductFormatEnum;

@JsonInclude(Include.NON_EMPTY)
public class BookGroupResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3921803842062894106L;

	/**
	 * 
	 */
	//private static final long serialVersionUID = 6914916839038932982L;
	private Integer bookgroupid;
	
	private String name;
	
	private ProductFormatEnum formattype;
	
	private String sorton;
	
	private Integer parentid;
	
	private String parentcategoryname;
	
	private ArrayList<UIBook> books;
	
	private ArrayList<String> toptitles;

	public Integer getBookgroupid() {
		return bookgroupid;
	}

	public void setBookgroupid(Integer bookgroupid) {
		this.bookgroupid = bookgroupid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductFormatEnum getFormattype() {
		return formattype;
	}

	public void setFormattype(ProductFormatEnum formattype) {
		this.formattype = formattype;
	}

	public String getSorton() {
		return sorton;
	}

	public void setSorton(String sorton) {
		this.sorton = sorton;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getParentcategoryname() {
		return parentcategoryname;
	}

	public void setParentcategoryname(String parentcategoryname) {
		this.parentcategoryname = parentcategoryname;
	}

	public ArrayList<UIBook> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<UIBook> books) {
		this.books = books;
	}

	public ArrayList<String> getToptitles() {
		return toptitles;
	}

	public void setToptitles(ArrayList<String> toptitles) {
		this.toptitles = toptitles;
	}

	
	
	
}

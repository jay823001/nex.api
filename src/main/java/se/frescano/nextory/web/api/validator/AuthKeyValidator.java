package se.frescano.nextory.web.api.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import se.frescano.nextory.web.api.request.AuthKeyRequestBean;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@Component
public class AuthKeyValidator implements Validator {
 
	//private static final Logger logger = LoggerFactory.getLogger(AuthKeyValidator.class);
	
	@Override
	public void validate(Object target, Errors errors) { 
		//AuthKeyRequestBean authKeyBean = (AuthKeyRequestBean)target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authkey", "error.authkey", WEBErrorCodeEnum.INPUT_MISSING.name());  
        
	}
	
	public void validateKey(String authkey,Errors errors, Boolean isRequired){
		try {
			if(isRequired && (authkey==null || authkey.isEmpty())){
				errors.rejectValue("authkey", "authkey.empty", WEBErrorCodeEnum.INPUT_MISSING.name());
				return;
			}else if(!isRequired && (authkey==null || authkey.isEmpty()))
				return;							
		} catch (Exception e) {			
			errors.rejectValue("authkey", "authkey.invalid", WEBErrorCodeEnum.INVALID_AUTH_TOKEN.name());
		} 
	}

	@Override
	public boolean supports(Class<?>  clazz) {
		// TODO Auto-generated method stub
		return AuthKeyRequestBean.class.isAssignableFrom(clazz);
	}

}

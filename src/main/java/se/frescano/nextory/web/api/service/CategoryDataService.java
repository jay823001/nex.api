package se.frescano.nextory.web.api.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.web.api.response.CategoryMasterWrapper;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.response.UICategoryVO;

@Service("categoryservice")
public class CategoryDataService {

	@Autowired
	private CacheBridgeCategory	cachebridecategory;
	
	public GenericAPIResponse loadCategoriesList(String countrycode) throws Exception {
		
		GenericAPIResponse	response 	= new GenericAPIResponse();
		List<UICategoryVO> categories	= new ArrayList<UICategoryVO>();
		try{
			Iterator<?>	it	= cachebridecategory.getCategoryCache(countrycode).entrySet().iterator();
			while( it.hasNext() ){
				@SuppressWarnings("unchecked")
				Entry<Integer, EbCategories> entry = (Entry<Integer, EbCategories>) it.next();
				EbCategories	category = entry.getValue();
				categories.add( new UICategoryVO(category.getCategoryId(), 
												category.getParentCatId(), 
												category.getCategoryName(),  
												category.getIsactive(),
												category.getSlugName(),
												category.getPopular(),
												category.getPosition()));
			}
			response = new GenericAPIResponse(HttpStatus.OK, new CategoryMasterWrapper( categories ));
		}catch(Exception e){
			throw new Exception( e.getMessage() );
		}
		
		return response;
	}
}

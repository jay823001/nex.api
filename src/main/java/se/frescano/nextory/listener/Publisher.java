package se.frescano.nextory.listener;

import org.springframework.context.ApplicationEvent;

public interface Publisher {

	
	//public void publishSpringEvent(ApplicationEvent event);
	public void publishEvent(ApplicationEvent event);
	/*public void publishEvent(EventBusEnum apptusEsales,
			Map<String,Object> request);*/
	
}

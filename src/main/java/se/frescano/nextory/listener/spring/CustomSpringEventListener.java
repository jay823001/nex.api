package se.frescano.nextory.listener.spring;

import java.util.concurrent.TimeUnit;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.Logger;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.blueshift.config.BlueShiftConfiguration;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.notification.NotificationEnum;
import se.frescano.nextory.notification.NotificationEvent;

/*
 * Any Application Event posted in the Spring listener framework will call the onApplicationEvent method.
 * 
 */
@Component
public class CustomSpringEventListener implements ApplicationListener<ApplicationEvent> {
	
	
	@Autowired
	MeterRegistry meterRegistry;
	@Autowired
	ApplicationPropertiesConfig	 	applicationpropconfig;
	@Autowired
	BlueShiftConfiguration 	blueShiftConfiguration;
	private final BroadCaster broadCaster;
	static final Logger logger = (Logger) LoggerFactory.getLogger(CustomSpringEventListener.class);
	
	public CustomSpringEventListener(BroadCaster broadCaster) {
 		this.broadCaster = broadCaster;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 * We are interested in only two types of Events , CallState and NotificationEvent
	 */
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
    	
    	//System.out.println( " event received "+event.toString());
        if(event instanceof CallState)
    		sendMetrics((CallState)event);
    	else if(event instanceof NotificationEvent)
    		publishEvent((NotificationEvent)event);
    	
    	
    }
    
    
    public void publishEvent(NotificationEvent nexEvent) {
    	if(!filterInterest1(nexEvent)) return;
    	long start = System.currentTimeMillis();
    	broadCaster.broadCastEvent(nexEvent);
    	long elapsed = System.currentTimeMillis() - start;
    	if(logger.isDebugEnabled())logger.debug("Elapsed time: {} ms" , elapsed );
    }
    boolean filterInterest1(NotificationEvent myEvent) {
        if (myEvent != null && myEvent.getNotificationType() != null) {
        	
        	boolean		notify	= false;
        	if( NotificationEnum.isApptusNotification( myEvent.getNotificationType() ) )
        		notify	= ( applicationpropconfig.getApptusOn().equalsIgnoreCase("on") )?true:false;
        	else if(NotificationEnum.isBlueShiftNotification( myEvent.getNotificationType() ) )
        		notify	= ( blueShiftConfiguration.getBlueshiftMode().equalsIgnoreCase("on") )?true:false;
        	
        	if( notify )
        		System.out.println( " received at  "+System.currentTimeMillis());
            return notify;
        }
        return false;
    }
    
    void sendMetrics(CallState event){
    	logger.trace("Received spring custom event - " + event.getMetricName());
    	Timer.builder(event.getMetricName())
        .tags(event.getTags())
        .description("timer of webclient operations")
        .register(meterRegistry)
        .record(meterRegistry.config().clock().monotonicTime() - event.getStartTime(), TimeUnit.NANOSECONDS);
    }
}

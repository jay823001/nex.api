package se.frescano.nextory.listener.spring;

import se.frescano.nextory.notification.NotificationEvent;

public interface BroadCaster {
	
	
	public void broadCastEvent(NotificationEvent event);

}

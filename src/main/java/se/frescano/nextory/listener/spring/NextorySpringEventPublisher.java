package se.frescano.nextory.listener.spring;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import se.frescano.nextory.listener.Publisher;

@Component
public class NextorySpringEventPublisher implements Publisher{

	private final ApplicationEventPublisher applicationEventPublisher;
	public NextorySpringEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
	@Override
	public void publishEvent(ApplicationEvent event) {
		//throw new RuntimeException(" not supported use EventBus event handler ");
		applicationEventPublisher.publishEvent(event);
	}
	
}

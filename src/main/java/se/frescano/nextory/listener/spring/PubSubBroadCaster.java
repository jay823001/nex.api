package se.frescano.nextory.listener.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.frescano.nextory.notification.NotificationEvent;

//@Component
public class PubSubBroadCaster implements BroadCaster {

	static final Logger logger = LoggerFactory.getLogger(PubSubBroadCaster.class);
	@Override
	public void broadCastEvent(NotificationEvent event) {
		
		logger.info(" broad casted the message {}, {}" + event.getNotificationType(),event.getPayload());
	}

}

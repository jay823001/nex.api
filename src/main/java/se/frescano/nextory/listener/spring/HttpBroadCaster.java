package se.frescano.nextory.listener.spring;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.blueshift.config.BlueShiftConfiguration;
import se.frescano.nextory.notification.NotificationEvent;

//@Component
public class HttpBroadCaster implements BroadCaster {
	
	@Autowired
	private	ApplicationPropertiesConfig applicationpropconfig;
	
	@Autowired
	private BlueShiftConfiguration		blueshiftconfiguration;
	

	static final Logger logger = LoggerFactory.getLogger(HttpBroadCaster.class);
	@Override
	public void broadCastEvent(NotificationEvent event) {
		transmit(event);
		logger.info(" broad casted the message {}, {}" + event.getNotificationType(),event.getPayload());
	}

	
	
	private void transmit(NotificationEvent event){
		switch (event.getNotificationType()) {
		case APPTUS_ESALES:
			notifyApptus(applicationpropconfig.getClickNotificationUrl(), event.getPayload());
			break;
		case APPTUS_NONESALES:
			notifyApptus(applicationpropconfig.getNonEsalesClickNotification(), event.getPayload());
			break;
		case APPTUS_ADDTOCART:
			notifyApptus(applicationpropconfig.getAddToCartUrl(), event.getPayload());
			break;
		case APPTUS_ADDTOCART_NONESALES:
			notifyApptus(applicationpropconfig.getAddToCartUrlNonEsales(), event.getPayload());
			break;
		case APPTUS_PAYMENT:
			notifyApptus(applicationpropconfig.getPaymentUrl(), event.getPayload());
			break;
		case BLUE_SHIFT:
			notifyBlueShift(blueshiftconfiguration.getBlueshiftUrl(), event.getPayload() );
			break;
		default:
			break;
		}
	}
	/*@Autowired
	WebClient webclient;
	*/
	void notifyApptus(String url,Map<String, Object> payload){
		try {
			
			URI uri = new URIBuilder("https://api.esales.apptus.com"+url).build();
			Mono<ClientResponse> result = WebClient.create().post().uri(uri)
				.body(BodyInserters.fromPublisher(Mono.just(payload),Map.class))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.acceptCharset(Charset.forName(CONSTANTS.UTF_8_CHARSET))
				.exchange();
			ClientResponse clientResponse = result.block();
			HttpStatus status =clientResponse.statusCode();
			if(status.is2xxSuccessful())				
				logger.debug("Notification sucecssfull");
			else if(status.is4xxClientError() || status.is5xxServerError() ){
				logger.warn(" Notification failed with status code {} and the message {} ",status,clientResponse.statusCode().getReasonPhrase());
			}else
				logger.info(" Notification failed with status code {} ",status);
		} catch (URISyntaxException e) {
			logger.warn(" Notification failed to transmit to the url {} ", url );
		}	
	}
	
	void notifyBlueShift(String url,Map<String, Object> payload){
		try {
			URI uri = new URIBuilder(url).build();
			Mono<ClientResponse> result = WebClient.create().post().uri(uri)
				.body(BodyInserters.fromPublisher(Mono.just(payload),Map.class))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)							
				.header("Authorization", "Basic " + Base64Utils.encodeToString((blueshiftconfiguration.getBlueshiftApiKey() + ":" + "").getBytes(Charset.forName(CONSTANTS.AUTHORIZATION_US_ASCII))) )			
				.accept(MediaType.APPLICATION_JSON)
				.acceptCharset(Charset.forName(CONSTANTS.UTF_8_CHARSET))
				.exchange();
			ClientResponse clientResponse = result.block();
			HttpStatus status =clientResponse.statusCode();
			if(status.is2xxSuccessful())				
				logger.debug("Notification sucecssfull");
			else if(status.is4xxClientError() || status.is5xxServerError() ){
				logger.warn(" Notification failed with status code {} and the message {} ",status,clientResponse.statusCode().getReasonPhrase());
			}else
				logger.info(" Notification failed with status code {} ",status);
		} catch (URISyntaxException e) {
			logger.warn(" Notification failed to transmit to the url {} ", url );
		}
	}
	
}

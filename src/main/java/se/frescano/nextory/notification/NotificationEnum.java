package se.frescano.nextory.notification;

import org.apache.commons.lang3.StringUtils;

public enum NotificationEnum {
	BLUE_SHIFT,
	APPTUS_ESALES,
	APPTUS_NONESALES,
	APPTUS_ADDTOCART,
	APPTUS_ADDTOCART_NONESALES,
	APPTUS_PAYMENT,
	AUTH_ENDPOINT
	;
		
	public static boolean isApptusNotification(NotificationEnum event){		
		if( event != null && StringUtils.startsWith(event.name(), "APPTUS") )
			return true;		
		return false;
	}
	
	public static boolean isBlueShiftNotification(NotificationEnum event){		
		if( event != null && StringUtils.startsWith(event.name(), "BLUE_SHIFT") )
			return true;		
		return false;
	}
}

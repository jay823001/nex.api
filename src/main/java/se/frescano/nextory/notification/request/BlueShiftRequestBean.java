package se.frescano.nextory.notification.request;

import java.util.HashMap;

public class BlueShiftRequestBean extends HashMap<String,Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1928001679272511837L;

	private static final String	customer_id	= "customer_id";
	private static final String	products	= "products";
	private static final String	sku	= "sku";
	private static final String	event	= "event";
	
	public BlueShiftRequestBean(String	customer_id, String isbn, String event){
		this.setCustomerid(customer_id);
		this.setProducts(isbn);
		this.setEvent(event);
	}
	
	public void setCustomerid(String customer_id){
		put(BlueShiftRequestBean.customer_id, customer_id);
	}
	
	public void setProducts(String isbn){
		HashMap<String,String> productlist = new HashMap<String,String>();
		productlist.put(BlueShiftRequestBean.sku, isbn);
		put(BlueShiftRequestBean.products, productlist);
	}
	
	public void setEvent(String event){
		put(BlueShiftRequestBean.event, event);
	}
}

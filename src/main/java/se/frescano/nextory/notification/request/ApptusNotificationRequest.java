package se.frescano.nextory.notification.request;

import java.util.ArrayList;
import java.util.HashMap;

public class ApptusNotificationRequest extends HashMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -695782836909804425L;
	private static final String	ID	= "id";
	private static final String	SESSION_KEY		= "sessionKey";
	private static final String	CUSTOMER_KEY	= "customerKey";
	private static final String	MARKET			= "market";
	private static final String	TOKEN			= "token";
	private static final String	PRODUCT_KEY		= "productKey";
	private static final String	VARIANT_KEY		= "variantKey";
	private static final String	TICKET			= "ticket";
	private static final String	QUANTIITY		= "quantity";
	private static final String	SELLING_PRICE	= "sellingPrice";
	private static final String	LINES			= "lines";
	
	public ApptusNotificationRequest(String id, String sessionKey, String customerKey, String market, String token, String productkey, String variantkey
									, String ticket, String quality, String sellingprice,ArrayList<HashMap<String,Object>> lines){
		this.setId(id);
		this.setSessionKey(sessionKey);
		this.setCustomerKey(customerKey);
		this.setMarket(market);
		this.setToken(token);
		this.setProductKey(productkey);
		this.setVariantKey(variantkey);
		this.setTicket(ticket);
		this.setQuantity(quality);
		this.setSettingPrice(sellingprice);
		this.setLines(lines);
	}
	
	public void setId(String string){
		this.put(ID, string);
	}
	
	public void setMarket(String string){
		this.put(MARKET, string);
	}
	
	public void setToken(String string){
		this.put(TOKEN, string);
	}
	
	public void setProductKey(String string){
		this.put(PRODUCT_KEY, string);
	}
	
	public void setVariantKey(String string){
		this.put(VARIANT_KEY, string);
	}
	
	public void setTicket(String string){
		this.put(TICKET, string);
	}

	public void setQuantity(String string){
		this.put(QUANTIITY, string);
	}
	
	public void setSettingPrice(String string){
		this.put(SELLING_PRICE, string);
	}
	
	public void setLines(ArrayList<HashMap<String,Object>> lines){
		this.put(LINES, lines);
	}
		
	public void setCustomerKey(String customerKey) {
		this.put(CUSTOMER_KEY, customerKey);
	}


	public void setSessionKey(String sessionKey) {
		this.put(SESSION_KEY, sessionKey);
	}

}

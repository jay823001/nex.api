package se.frescano.nextory.notification;

import java.util.Map;

import org.springframework.context.ApplicationEvent;


/*
 * 
 * To use the spring listeners , the pojo must implement Spring's applicationevent.
 * This will be interpreted by the CustomSpringEventListener
 * 
 */
public class NotificationEvent extends ApplicationEvent {

	private static final long serialVersionUID = 3970761107529983780L;
	private final Map<String,Object> payload;
	private final NotificationEnum notificationType;
	public NotificationEvent(NotificationEnum notificationType,Map<String,Object> payload) {
		super(notificationType);
		this.payload = payload;
		this.notificationType = notificationType;
	}
	
	public NotificationEnum getNotificationType() {
		return notificationType;
	}

	public Map<String, Object> getPayload() {
		return payload;
	}
	
	
	
}


package se.frescano.nextory.api.kafka.services;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.cache.service.CacheBridgeSeries;
import se.frescano.nextory.cache.service.CacheBridgeTopPanels;

@Service
@DependsOn("applicationpropconfig")
public class KafkaConsumer {
	private static final Logger log = LoggerFactory.getLogger(KafkaConsumer.class);
  
	@Autowired
	CacheBridgeCategory cacheBridge;
	
	@Autowired
	CacheBridgeSeries cacheBridgeSeries;
	
	@Autowired
	CacheBridgeMarket cacheBridgeMarket;
	
	@Autowired
	CacheBridgeTopPanels cacheBridgeTopPanels;
	
	@Autowired
	private ApplicationPropertiesConfig applicationpropconfig;
	
	/*@Value("${nest.nextory.kafka.category.message.key}")*/
	private String categoryMessageKey;
	
	/*@Value("${nest.nextory.kafka.category.message.value}")*/
	private String categoryMessageValue;
	
	@PostConstruct
	public void inti(){
		this.categoryMessageKey 	= applicationpropconfig.getCategoryMessageKey();
		this.categoryMessageValue 	= applicationpropconfig.getCategoryMessageValue();
	}
	
	@KafkaListener(topics="${nest.nextory.kafka.topic}")
    public void processMessage(@Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String messageKey, @Payload String messageVal) {
		log.info("Kafka Listner received message [Key,value] = '[{},{}]'", messageKey,messageVal);
		if(StringUtils.isNotBlank(messageKey) && StringUtils.isNotEmpty(messageVal)){
			if(StringUtils.isNotBlank(categoryMessageKey) && StringUtils.isNotBlank(categoryMessageValue) 
					&& categoryMessageKey.equals(messageKey) && categoryMessageValue.equals(messageVal))
				cacheBridge.resetAllEntries();
			else if(StringUtils.equals(messageKey, "SERIES") && StringUtils.equals(messageVal, "SERIES"))
				cacheBridgeSeries.resetAllEntries();
			else if(StringUtils.equals(messageKey, "MARKETS") && StringUtils.equals(messageVal, "MARKETS"))
				cacheBridgeMarket.resetAllEntries();
			else if(StringUtils.equals(messageKey, "PANELS") && StringUtils.equals(messageVal, "PANELS"))
				cacheBridgeTopPanels.resetAllEntries();
				
		} 
    }
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Kafka Configurations : [");
		if( categoryMessageKey != null ){
			builder.append("categoryMessageKey = ").append( categoryMessageKey).append(" , ");
		}
		
		if( categoryMessageValue != null ){
			builder.append("categoryMessageValue = ").append( categoryMessageValue).append(" , ");
		}
		
		builder.append("]");
		return builder.toString();
	}
	 
}

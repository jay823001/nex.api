package se.frescano.nextory.restclients;

import io.opentracing.propagation.TextMap;

import java.util.Iterator;
import java.util.Map;

import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;

public class DataDogClientHttpHeadersInjectAdapter implements TextMap{

	private final RequestHeadersSpec<?> reqHeaderSpec;
	
	  public DataDogClientHttpHeadersInjectAdapter(final RequestHeadersSpec<?> reqHeaderSpec) {
		 
	    this.reqHeaderSpec = reqHeaderSpec ;
	    
	  }

	  @Override
	  public void put(final String key, final String value) {
		  if( reqHeaderSpec != null )
			  reqHeaderSpec.header(key, value);
	  }

	  @Override
	  public Iterator<Map.Entry<String, String>> iterator() {
	    throw new UnsupportedOperationException("This class should be used only with tracer#inject()");
	  }
}

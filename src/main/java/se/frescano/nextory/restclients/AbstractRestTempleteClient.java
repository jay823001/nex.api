package se.frescano.nextory.restclients;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;

import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.propagation.Format;
import io.opentracing.util.GlobalTracer;
import reactor.core.publisher.Mono;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.datadog.trace.AppTracingEndpoint;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.listener.Publisher;

@Service("abstractresttemplate")
public class AbstractRestTempleteClient {

	//@Autowired
	private	WebClient	webClient;
	
	@PostConstruct
	public void init(){		
		webClient = WebClient.create();		
	}
	
	@Autowired
	Publisher publisher;
	
	public <T> T  exchange(String url, HttpMethod method, Class<T> responseEntity) throws  Exception{
		return exchange(url, method, null,null,null,responseEntity);
	}
	public <T> T  exchange(String url, HttpMethod method, Object body, Class<T> responseEntity) throws Exception{
		return exchange(url, method, body,null,null,responseEntity);
	}
	public <T> T  exchange(String url, HttpMethod method, Class<T> responseEntity,Function<T,CallState> tagEx) throws Exception{
		return exchange(url, method, null,null,null,responseEntity,null);
	}
	
	public <T> T  exchange(String url, HttpMethod method, Class<T> responseEntity,TagExtractor<T> tagEx) throws Exception{
		return exchange(url, method, null,null,null,responseEntity,tagEx);
	}
	public <T> T  exchange(String url, HttpMethod method, Object body, String username, String password, Class<T> responseEntity) throws Exception{
		return exchange(url, method, body, username, password, responseEntity, null);
	}
	/*public <T> T  getExchange(UriComponents uri, Class<T> responseEntity,TagExtractor<T> tagex) throws Exception{
		
		return exchange(url,  responseEntity, tagex);
	}*/
	
	
	public <T> T  exchange(String url, HttpMethod method, Object body, String username, String password, Class<T> responseEntity,TagExtractor<T> tagEx) throws Exception{
		T t=null;
		long starttime =System.currentTimeMillis();
		try {
			if(method.matches(HttpMethod.GET.name())){
				t = getexchange(url,  body, username, password, responseEntity);
			}else if(method.matches(HttpMethod.POST.name()) ){
				t = postexchange(url,  body, username, password, responseEntity);
			}	
			if(tagEx!=null)publisher.publishEvent(tagEx.extract((T)t,starttime,null));
		}catch (RestException|URISyntaxException e) {
			if(tagEx!=null)publisher.publishEvent(tagEx.extract(null, starttime, e));
			throw new Exception(e);
		}
		return (T)t;
	}
	
	@AppTracingEndpoint
	private <T> T  getexchange(String url,  Object body, String username, String password, Class<T> responseEntity) throws Exception{
		//URI uri = new URIBuilder(url).build();
	//	uri.getPath();
		Tracer tracer  = GlobalTracer.get();
		try (Scope scope = tracer.scopeManager().active()) {
			final Span span = scope.span();
		 
			RequestHeadersSpec<?> reqHeaderSpec = webClient.get().uri(url);
			if( !StringUtils.isBlank(username) && !StringUtils.isBlank(password))
				reqHeaderSpec.header("Authorization", "Basic " + Base64Utils.encodeToString((username + ":" + password).getBytes(Charset.forName(CONSTANTS.AUTHORIZATION_US_ASCII))));
			
			 tracer.inject(span.context(),
				Format.Builtin.HTTP_HEADERS,
				new DataDogClientHttpHeadersInjectAdapter(reqHeaderSpec));
			
			Mono<T> response = reqHeaderSpec.retrieve()				
			.onStatus(HttpStatus::isError  , clientResponse ->Mono.error(new RestException(clientResponse.statusCode(),clientResponse.statusCode().toString())))
			.bodyToMono(responseEntity);
			
			return response.block();
		}
		finally
		{}	
	}
	
	@AppTracingEndpoint
	private <T> T  postexchange(String url,  Object body, String username, String password, Class<T> responseEntity) throws Exception{
		URI uri = new URIBuilder(url).build();
		Tracer tracer  = GlobalTracer.get();
		try (Scope scope = tracer.scopeManager().active()) {
			final Span span = scope.span();
		 
			RequestHeadersSpec<?> reqHeaderSpec = webClient.post().uri(uri)				
					.body(BodyInserters.fromObject(body));
			
			if( !StringUtils.isBlank(username) && !StringUtils.isBlank(password))
				reqHeaderSpec.header("Authorization", "Basic " + Base64Utils.encodeToString((username + ":" + password).getBytes(Charset.forName(CONSTANTS.AUTHORIZATION_US_ASCII))));
			
			tracer.inject(span.context(),
			Format.Builtin.HTTP_HEADERS,
			new DataDogClientHttpHeadersInjectAdapter(reqHeaderSpec));
			
			Mono<T> response = reqHeaderSpec.retrieve()				
					.onStatus(HttpStatus::isError  , clientResponse ->Mono.error(new RestException(clientResponse.statusCode(),clientResponse.statusCode().toString())))
					.bodyToMono(responseEntity);
					
			return response.block();
		
		}finally{
			}
	}

		
}

package se.frescano.nextory.restclients;

import org.springframework.http.HttpStatus;

public class RestException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3066783944855128336L;
	final HttpStatus status;
	public RestException(org.springframework.http.HttpStatus httpStatus,String message) {	
		super(message);
		this.status  =httpStatus;
	}
	public HttpStatus getStatus() {
		return status;
	}
	
	
}

package se.frescano.nextory.restclients;

public class ClientNotInitializedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5950424776475884547L;

	public ClientNotInitializedException(String message) {
		super(message);
	}
}

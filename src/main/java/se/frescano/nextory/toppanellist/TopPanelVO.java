package se.frescano.nextory.toppanellist;

import java.io.Serializable;

public class TopPanelVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3613143729860555882L;
	private String groupid;
	private String title;
	private Integer position;
	private String listimageurl;
	private Integer listid;
	private String description;
	private String label;
	private String listimageurllarger;
	private Integer darktext;
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getListimageurllarger() {
		return listimageurllarger;
	}
	public void setListimageurllarger(String listimageurllarger) {
		this.listimageurllarger = listimageurllarger;
	}
	public Integer getListid() {
		return listid;
	}
	public void setListid(Integer listid) {
		this.listid = listid;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public String getListimageurl() {
		return listimageurl;
	}
	public void setListimageurl(String listimageurl) {
		this.listimageurl = listimageurl;
	}
	public Integer getDarktext() {
		return darktext;
	}
	public void setDarktext(Integer darktext) {
		this.darktext = darktext;
	}
	
	
}

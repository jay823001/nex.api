package se.frescano.nextory.toppanellist;

import java.io.Serializable;
import java.util.List;

public class TopPanelList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8042819358071690034L;
	private List<TopPanelVO> topPanelList;
	public String status;
	public int code;
	public String description;
	
	
	
	public List<TopPanelVO> getTopPanelList() {
		return topPanelList;
	}
	public void setTopPanelList(List<TopPanelVO> topPanelList) {
		this.topPanelList = topPanelList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}

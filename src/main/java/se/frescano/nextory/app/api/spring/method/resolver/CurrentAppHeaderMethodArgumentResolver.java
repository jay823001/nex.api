package se.frescano.nextory.app.api.spring.method.resolver;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.request.APIRequestBean;
import se.frescano.nextory.app.request.APITokenRequestBean;

public class CurrentAppHeaderMethodArgumentResolver implements HandlerMethodArgumentResolver{

	//private static final Logger logger = LoggerFactory.getLogger(CurrentAppUserTokenMethodArgumentResolver.class);
	
	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {		
		return methodParameter.getParameterAnnotation(AppRequestBeanInfo.class) != null && methodParameter.getParameterType().equals(APITokenRequestBean.class);
	}

	@Override
	public Object resolveArgument(MethodParameter methodParameter, 
								  ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
								  WebDataBinderFactory binderFactory) throws Exception {
		HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
		
		APITokenRequestBean bean = new APITokenRequestBean();
		bean.setAppId(request.getHeader("appId"));
		bean.setDeviceid(request.getHeader("deviceid"));
		bean.setLocale(request.getHeader("locale"));
		bean.setModel(request.getHeader("model"));
		bean.setOsinfo(request.getHeader("osinfo"));
		bean.setVersion(request.getHeader("version"));

		
		if (this.supportsParameter(methodParameter))												     	        	
			return bean;
        else {
            return WebArgumentResolver.UNRESOLVED;
        }
	}
		
}

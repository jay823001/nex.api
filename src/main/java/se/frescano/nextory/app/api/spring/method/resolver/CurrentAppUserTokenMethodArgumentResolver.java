package se.frescano.nextory.app.api.spring.method.resolver;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.request.UserAuthToken;

public class CurrentAppUserTokenMethodArgumentResolver implements HandlerMethodArgumentResolver{

	//private static final Logger logger = LoggerFactory.getLogger(CurrentAppUserTokenMethodArgumentResolver.class);
	
	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {		
		return methodParameter.getParameterAnnotation(AppUserToken.class) != null && methodParameter.getParameterType().equals(UserAuthToken.class);
	}

	@Override
	public Object resolveArgument(MethodParameter methodParameter, 
								  ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
								  WebDataBinderFactory binderFactory) throws Exception {
		HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
		if (this.supportsParameter(methodParameter))												     	        	
			return request.getAttribute(CONSTANTS.AUTH_TOKEN);
        else {
            return WebArgumentResolver.UNRESOLVED;
        }
	}
		
}

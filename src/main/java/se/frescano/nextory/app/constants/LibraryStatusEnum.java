package se.frescano.nextory.app.constants;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public enum LibraryStatusEnum {
	
	E2GO_ORDERED("E2GO_ORDERED","not ordered in elib"),
	INACTIVE("INACTIVE", "inactive"),
	@Deprecated
	ACTIVE("ACTIVE", "active"),
	@Deprecated
	U_DOWNLOAD("U_DOWNLOAD" ,"book available for unlimited download"),
	U_INACTIVE("U_INACTIVE","book not available for unlimited download"),
	OFFLINE_READER("OFFLINE_READER","available for download"),
	@Deprecated
	REFUND("REFUND","refund"),
	DELETED("DELETED","deleted"),
	@Deprecated
	UN_DEFINED("UN_DEFINED",null),
	REMOVED("REMOVED","removed from library");
	
	private String dbName;
	private String desc;
	
	private LibraryStatusEnum(String name, String desc) {
		this.dbName = name;
		this.desc = desc;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String name) {
		this.dbName = name;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	static LibraryStatusEnum[] values = values();
	
	
	public static LibraryStatusEnum fromDBName(String name) {
		if(StringUtils.isBlank(name))return UN_DEFINED;
		
		for (int i = 0; i < values.length; i++) {
			LibraryStatusEnum libraryStatusEnum = values[i];
			if(name.equalsIgnoreCase(libraryStatusEnum.getDbName())){
				return libraryStatusEnum;
			}
		}
		return UN_DEFINED;
	}

	public static List<String> getNonActiveProfileStatus(){
		
		return Arrays.asList(OFFLINE_READER.name(),DELETED.name());
	}

}

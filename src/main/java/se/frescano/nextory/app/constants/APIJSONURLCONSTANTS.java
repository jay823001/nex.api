package se.frescano.nextory.app.constants;

public class APIJSONURLCONSTANTS {

	
	/** Generic API*/
	public static final String BUSINESS_HOURS="/businesshours";
	public static final String SALT_URL = "/salt";
	public static final String BOOK_COUNT_API = "/book-count";
	public static final String PING_VER = "/server-status";
		
	/** version starts from 6.0 Added as part of Apptus re-strcuture of code*/
	public static final String APTTUS_GROUPS="/groups";
	public static final String APPTUS_BOOKS="/booksforbookgroup";
	
	public static final String APPTUS_SEARCH="/searchbooks";
	public static final String APPTUS_SEARCH_FACETS="/searchbooksfacets";
	public static final String APPTUS_ONLY_FACETS="/facets";
	public static final String APPTUS_ADVANCED_SEARCH="/advancedsearchbooks";
	public static final String APPTUS_REPORT_CLICKS="/reportclick";
	public static final String AUTOCOMPLETE="/autocomplete";
	public static final String SEARCH_HISTORY="/searchhistory";
	public static final String TOP_SEARCHES="/topsearches";
	public static final String FETCH_IMAGE="/fetch/productimage";
	
	/** Book Rating Related API*/
	public static final String BOOK_RATINGS = "/ratebook";
	
	public static final Double APP_API_VER_6_4 = 6.4;
	public static final Double APP_API_VER_6_5 = 6.5;
//	public static final Double APP_API_VER_6_6 = 6.6;
	//public static final Double[] API_VERSIONS = {APP_API_VER_6_4,APP_API_VER_6_5};
	
	public static final String[] SECURE_API = {APTTUS_GROUPS, APPTUS_BOOKS, APPTUS_SEARCH, APPTUS_SEARCH_FACETS,APPTUS_ONLY_FACETS,APPTUS_ADVANCED_SEARCH,AUTOCOMPLETE,
			SEARCH_HISTORY,TOP_SEARCHES,APPTUS_REPORT_CLICKS};
	
	public static final String APP_API_V1_BASE = "/api/{api_version}";
	public static final String APP_API_V2_BASE = "/api/app/catalogue/{api_version}";
	public static final String INT_API_V2_BASE = "/api/int/catalogue/{version}";
	
}

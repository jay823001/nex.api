package se.frescano.nextory.app.constants;

import se.frescano.nextory.util.Book2GoUtil;

public enum BookStatusEnum {
	
	COMPLETED(2,"Marked as completed"),
	STARTED(1 , "book read partially"),
	NOTSTARTED(0 ,"book not read"),
	STARTED_AND_NOTSTARTED(3 ,"book read partially and book not read");
	
	private int code;
	private String desc;
	
	private BookStatusEnum(int code , String desc){
		this.code = code;
		this.desc = desc ;
	}
	
	
	static BookStatusEnum[] values = values();
	
	
	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public static BookStatusEnum fromDBName(int dbValue) {
		for (int i = 0; i < values.length; i++) {
			BookStatusEnum statusEnum = values[i];
			if(dbValue == statusEnum.getCode()){
				return statusEnum;
			}
		}
		return null;
	}
	
	public static BookStatusEnum getStatusEnum(String codevalue){
		Integer code = Book2GoUtil.parseString(codevalue);
		if (code == null )
			return null;		
		
		for (BookStatusEnum value : values) {
			if (code.intValue() == value.getCode())
				return value;
		}
		return null;		
	}
	
}

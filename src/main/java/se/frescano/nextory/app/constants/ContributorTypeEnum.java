package se.frescano.nextory.app.constants;

public enum ContributorTypeEnum {
	
	AUTHOR("Author"),ILLUSTRATOR("Narrator"),TRANSLATOR("Translator");
	
	private String dbName;
	
	private ContributorTypeEnum(String name) {
		this.dbName = name;
	}
	
	public String getDbName() {
		return dbName;
	}
	
	public void setDbName(String name) {
		this.dbName = name;
	}
}

package se.frescano.nextory.app.constants;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public enum MemberTypeEnum {
	//(int memberTypeCode, Integer[]  allowedMemCodes,Integer previousMemCode, boolean member, boolean oldMember)
	VISITOR_NO_ACTION		 	(100002, new Integer []{101000,102000,103000,101009,102009,103009},100000,false,false),
	VISITOR_GIFTCARD_BUYER		(101001,null,101001,false,false),
	NONMEMBER_PIECE_BUYER		(100003,null,null,false,false),
	E2GO_NONMEMBER				(100004,null,null,false,false),
	NONMEMBER_PREVIOUS_EMPLOYEE	(100005,null,null,false,false),

	FREE_TRIAL_MEMBER			(203002,new Integer []{103002,203004,403002,403004},203002,true ,false ),
	FREE_GIFTCARD_NOCARDINFO	(201002,new Integer []{101002,201004,401002},201002,true ,false ),
	FREE_CAMPAIGN_MEMBER		(202002,new Integer []{102002, 202004, 402002},202002,true ,false ),
	FREE_GIFTCARD_MEMBER		(201007,null,201007,true ,false ),
	FREE_GIFTCARD_UPGRADE_MEMBER(201010,null,null,true ,false ),
	FREE_GIFTCARD_CANCELLED		(201003,null,null,true ,false),
	FREE_CAMPAIGN_CANCELLED		(202003,null,null,true ,false ),
	FREE_CARD_EXPRIYDUE			(205006,new Integer []{201006,202006,203006},null,true ,false ),
	FREE_TRANSACTION_FAILED     (203011,new Integer []{202011,201011,203011},null,true,false),
	
	FREE_TRIAL_PARKED(203014,new Integer []{203014,203021},203014,true,false),
    FREE_TRIAL_CANCELLED_PARKED(203017,null,203017,true,false),
    FREE_CAMPAIGN_PARKED(202014,new Integer []{202014,202021},202014,true,false),
    FREE_CAMPAIGN_CANCELLED_PARKED(202017,null,202017,true,false),
    FREE_GIFTCARD_PARKED(201014,new Integer []{201014,201021},201014,true,false),
    FREE_GIFTCARD_CANCELLED_PARKED(201017,null,201017,true,false),
    FREE_TRANSACTION_FAILED_PARKED(203025,new Integer []{203025,203032},203025,true,false),
    FREE_TRANSACTION_FAILED_CANCELLED_PARKED(203028,null,203028,true,false),
    

    MEMBER_PAYING                (304001,new Integer []{201008, 203008,202008, 301008,302008,304008,404004,304004,404008,404002,405004},304001,true ,false ),
	MEMBER_CARD_EXPIRYDUE		(305006,new Integer []{304006,301006,302006,205008},null,true ,false ),
	
	MEMBER_GIFTCARD_EXISTING	(301002,new Integer []{301004},304001,true ,false ),
	MEMBER_CAMPAIGN_EXISTING	(302002,new Integer []{302004},304001,true ,false ),
	MEMBER_EMPLOYEE				(300000,null,null,true ,false ),

	MEMBER_PAYING_CANCELLED		(304003,new Integer []{304003},null,true ,false ),
	MEMBER_GIFTCARD_CANCELLED 	(301003,null,null,true ,false ),
	MEMBER_CAMPAIGN_CANCELLED	(302003,null,null,true ,false ),
	MEMBER_TRANSACTION_FAILED   (304011,new Integer []{301011,302011,304011,305011},null,true,false),
	MEMBER_FIXED_PRICE_CAMPAIGN (302012,new Integer []{302012,403012,401012,402012,404012,405012,102012,202012},null,true,false),
	MEMBER_DISCOUNTED_PRICE_CAMPAIGN(302013,new Integer []{302013,403013,401013,402013,404013,405013,102013,202013},null,true,false),
	MEMBER_FIXED_CAMPAIGN_TRANSACTION_FAILED(302014,new Integer []{302023},null,true,false),
	MEMBER_DISCOUNTED_CAMPAIGN_TRANSACTION_FAILED(302015,new Integer []{302024},null,true,false),
	
	MEMBER_PAYING_PARKED(304014,new Integer []{304014,304021},304014,true,false),
	MEMBER_PAYING_CANCELLED_PARKED(304017,new Integer []{302017,301017},304017,true,false),
	MEMBER_TRANSACTION_FAILED_PARKED(304025,new Integer []{304025,304032},304025,true,false),	
	MEMBER_FIXED_PRICE_CAMPAIGN_PARKED(302026,null,304026,true,false),
	MEMBER_DISCOUNTED_PRICE_CAMPAIGN_PARKED(302027,null,304027,true,false),
	MEMBER_TRANSACTION_FAILED_CANCELLED_PARKED(304028,null,304028,true,false),
	MEMBER_FIXED_CAMPAIGN_TRANSACTION_FAILED_PARKED(302028,new Integer []{302028},302028,true,false),
	MEMBER_DISCOUNTED_CAMPAIGN_TRANSACTION_FAILED_PARKED(302029,new Integer []{302029},302029,true,false),
	MEMBER_GIFTCARD_EXISTING_PARKED(301016,new Integer []{301016},301016,true ,false ),
	MEMBER_CAMPAIGN_EXISTING_PARKED(302016,new Integer []{302016},302016,true ,false ),
    
	NONMEMBER_PREVIOUS_TRIAL	(403005,new Integer []{203005,203003},null,false,true ),
	NONMEMBER_PREVIOUS_GIFTCARD	(401005,new Integer []{201005},null,false,true ),
	NONMEMBER_PREVIOUS_CAMPAIGN	(402005,new Integer []{202005},null,false,true ),
	NONMEMBER_PREVIOUS_MEMBER	(404005,new Integer []{301005,302005,304005},304001,false,true ),
	NONMEMBER_CARD_EXPIRED		(405006,new Integer []{205005,305005},null,false,true ),
	NONMEMBER_TRANSACTION_FAILED(404011,new Integer []{304016,302019,302020},null,false,true);
	
	final static MemberTypeEnum[] values = values();
	
	
	private int memberTypeCode;
	private Integer[] allowedMemCodes;
	private Integer previousMemCode;
	private boolean member ;
	private boolean oldMember;

	

	public int getMemberTypeCode() {
		return memberTypeCode;
	}
	public void setMemberTypeCode(int memberTypeCode) {
		this.memberTypeCode = memberTypeCode;
	}
	public Integer[] getAllowedMemCodes() {
		return allowedMemCodes;
	}
	public void setAllowedMemCodes(Integer[] allowedMemCodes) {
		this.allowedMemCodes = allowedMemCodes;
	}
	public static MemberTypeEnum[] getValues() {
		return values;
	}
	public boolean isMember() {
		return member;
	}

	public void setMember(boolean member) {
		this.member = member;
	}
	

	public boolean isOldMember() {
		return oldMember;
	}

	public void setOldMember(boolean oldMember) {
		this.oldMember = oldMember;
	}
	

	public Integer getPreviousMemCode() {
		return previousMemCode;
	}
	public void setPreviousMemCode(Integer previousMemCode) {
		this.previousMemCode = previousMemCode;
	}
	private MemberTypeEnum(int memberTypeCode, Integer[]  allowedMemCodes,Integer previousMemCode, boolean member, boolean oldMember) {
		this.memberTypeCode = memberTypeCode;
		this.allowedMemCodes = allowedMemCodes;
		this.previousMemCode = previousMemCode ;
		this.member = member;
		this.oldMember = oldMember;
	}
	
	public static MemberTypeEnum getName(String name) {
		if(StringUtils.isBlank(name))
			return null;
		
		for (MemberTypeEnum value : values) {
			if (name.equals(value.name()))
				return value;
		}
		return null;
	}

	public static MemberTypeEnum getMembertypeEnum(int membertypeCode){
		for (MemberTypeEnum value : values) {
			if (value.getMemberTypeCode() == membertypeCode)
				return value;
		}
		return null;
	}
	
	public static MemberTypeEnum getMemeberFromAllowMemTypes(int membertypeCode){
		for (MemberTypeEnum value : values) {
			if (value.getAllowedMemCodes() !=null){
				Integer[] allowedValues = value.getAllowedMemCodes();
				List<Integer> array = Arrays.asList(allowedValues);
				if(array.contains(membertypeCode))
					return value;
			}
				
		}
		return null;
		
	}

	public boolean isCancelledMember() {
		
		return this.name().endsWith("CANCELLED") || this.name().endsWith("CANCELLED_PARKED");
	}
	
	public boolean isParkedMember() {
		
		return this.name().endsWith("PARKED");
	}

}


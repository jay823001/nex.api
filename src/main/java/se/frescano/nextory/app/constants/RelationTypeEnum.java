package se.frescano.nextory.app.constants;



public enum RelationTypeEnum {
	
	C_EBOOK(2,"correspondingEbook"),
	C_AUDIOBOOK(3,"correspondingAudiobook"),
	SERIES(6,"seriesBook");
	
	private int relationtype;
	private String relationname;
	
	private static RelationTypeEnum[] relationTypes = values();
	
	
	
	private RelationTypeEnum(int relationtype,String relationname){
		this.relationtype = relationtype;
		this.relationname = relationname;
	}




	public int getRelationtype() {
		return relationtype;
	}




	public void setRelationtype(int relationtype) {
		this.relationtype = relationtype;
	}




	public String getRelationname() {
		return relationname;
	}




	public void setRelationname(String relationname) {
		this.relationname = relationname;
	}
	
	public static RelationTypeEnum getFromRelationType(int relationtype){
		for(RelationTypeEnum relationType : relationTypes){
			if(relationtype==relationType.getRelationtype())
				return relationType;
		}
		return null;
	}
	public static Integer getOtherRelationType(int relationtype){
		RelationTypeEnum relationType = getFromRelationType(relationtype);
		if(relationType==RelationTypeEnum.C_AUDIOBOOK)
			return RelationTypeEnum.C_EBOOK.getRelationtype();
		else if(relationType==RelationTypeEnum.C_EBOOK)
			return RelationTypeEnum.C_AUDIOBOOK.getRelationtype();
		return null;
	}
	
	
}

package se.frescano.nextory.app.constants;

import org.apache.commons.lang3.StringUtils;

import se.frescano.nextory.util.Book2GoUtil;

public enum ProductProviderEnum {
	
	E2GO(1),ELIB(2),NEST(4),UN_DEFINED(-1);
	
	private int id;
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	static ProductProviderEnum[] values = values();
	

	public static ProductProviderEnum resolveEnum(String name){
		if(StringUtils.isNotBlank(name)){	// Additional check for null -Sathish
			for (ProductProviderEnum value : values) {
				if (StringUtils.equals(name,value.name()))
					return value;
			}
		}	
		return UN_DEFINED;
	}
	public static ProductProviderEnum getId(int id){
		for (ProductProviderEnum value : values) {
			if (value.getId()== id)
				return value;
		}
		return UN_DEFINED;
	}
	
	public static ProductProviderEnum getId(String id){
		Integer producttype = Book2GoUtil.parseString(id);
		if(producttype != null )
			return getId(producttype);
		return UN_DEFINED;
		
	}
	
	private ProductProviderEnum(int id){
		this.id = id;
		
	}
	
}

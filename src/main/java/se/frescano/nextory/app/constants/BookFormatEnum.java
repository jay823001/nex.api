package se.frescano.nextory.app.constants;

public enum BookFormatEnum {

	E_BOOK(1, "E_BOOK"), 
	AUDIO_BOOK(2, "AUDIO_BOOK");
	
	private int	formatId;
	private String formatName;
	
	private BookFormatEnum(int formatId, String formatName)
	{
		this.formatId = formatId;
		this.formatName = formatName;
	}
	
	static BookFormatEnum[] values = values();

	public static BookFormatEnum getFormat(int format)
	{
		for (BookFormatEnum value : values)
		{
			if (value.getFormatId() == format)
				return value;
		}
		return null;
	}

	public int getFormatId() {
		return formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public static Integer getOtherFormat(int format){
		BookFormatEnum formatEnum = getFormat(format);
		if(formatEnum == BookFormatEnum.E_BOOK)
			return BookFormatEnum.AUDIO_BOOK.getFormatId();
		else if(formatEnum == BookFormatEnum.AUDIO_BOOK){
			return BookFormatEnum.E_BOOK.getFormatId();
		}
		return null;
	}
	public static Integer getTargetRelationType(int formatid){
		BookFormatEnum formatEnum = getFormat(formatid);
		if(formatEnum == BookFormatEnum.E_BOOK)
			return RelationTypeEnum.C_AUDIOBOOK.getRelationtype();
		else if(formatEnum == BookFormatEnum.AUDIO_BOOK)
			return RelationTypeEnum.C_EBOOK.getRelationtype();
		return null;
			
	}
	
}

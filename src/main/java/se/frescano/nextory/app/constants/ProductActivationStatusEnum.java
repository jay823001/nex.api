package se.frescano.nextory.app.constants;

public enum ProductActivationStatusEnum
{

	ACTIVE("ACTIVE"), 
	P_INACTIVE("P_INACTIVE"),
	P_DELETED("P_DELETED"),
	P_DEFERRED("P_DEFERRED"),
	P_UPCOMING("P_UPCOMING"),
	HIGH_PRICE("HIGH_PRICE"),
	NO_FORMAT("NO_FORMAT"),
	A_INACTIVE("A_INACTIVE"),
	ERROR("ERROR"),
	A_OMITTED("A_OMITTED"),
	DATA_MISSING("DATA_MISSING"),
	PARKED("PARKED"),
	UNKNOWN("UNKNOWN"),
	L_INACTIVE("L_INACTIVE"),
	DELETED("DELETED");

	private String dbName;

	private ProductActivationStatusEnum(String name)
	{
		this.dbName = name;
	}

	public String getDbName()
	{
		return dbName;
	}

	public void setDbName(String name)
	{
		this.dbName = name;
	}

	static ProductActivationStatusEnum[] values = values();

	public static ProductActivationStatusEnum convertElibStatus(String elibStatus, boolean isUpcoming)
	{

		if ("active".equalsIgnoreCase(elibStatus))
		{
			return ACTIVE;
		}
		else if (("inactive".equalsIgnoreCase(elibStatus) && isUpcoming) || "UPCOMING".equalsIgnoreCase(elibStatus))
		{
			return P_UPCOMING;
		}
		else if ("inactive".equalsIgnoreCase(elibStatus) && !isUpcoming)
		{
			return P_INACTIVE;
		}
		else if ("deleted".equalsIgnoreCase(elibStatus))
		{
			return P_DELETED;
		}else if ("parked".equalsIgnoreCase(elibStatus))
		{
			return PARKED;
		}
		else
			return UNKNOWN;
	}

}

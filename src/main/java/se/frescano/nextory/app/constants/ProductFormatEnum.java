package se.frescano.nextory.app.constants;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.util.Book2GoUtil;
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ProductFormatEnum {

	E_BOOK(1,"E-bok"),
	AUDIO_BOOK(2,"Ljudbok"),
	GIFTCARD(3,"Presentkort"),
	SUBSCRIPTION(4,"Prenumeration"),
	E_MAGAZINE(5,"E-magasin"),
	CCARD_AUTHORIZED(6,"CCARD_AUTHORIZED"),
	TRAIL_MEMBER(7,"TRAIL_MEMBER"),
	UPGRADE_SUBSCRIPTION(8,"UPGRADE_SUBSCRIPTION"),
	UN_DEFINED(-1,"Undefined");
	
	private int formatNo;
	private String title;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	static ProductFormatEnum[] values = values();
	public static ProductFormatEnum getFormat(Integer format){
		if(format == null)
			return UN_DEFINED;
		
		for (ProductFormatEnum value : values) {
			if (value.getFormatNo() == format)
				return value;
		}
		return UN_DEFINED;
	}
	public static ProductFormatEnum getFormat(String format){
		Integer producttype = Book2GoUtil.parseString(format);
		if(producttype != null )
			return getFormat(producttype);
		return UN_DEFINED;		
	}
	public static ProductFormatEnum getFormatFromTitle(String format){
	
		if (format == null )
			return UN_DEFINED;		
		
		for (ProductFormatEnum value : values) {
			if (format.equalsIgnoreCase(value.getTitle()))
				return value;
		}
		return UN_DEFINED;		
	}
	
	
	private ProductFormatEnum(int formatno,String title){
		this.formatNo = formatno;
		this.title = title;
	}
	@JsonProperty(value="format")
	public int getFormatNo() {
		return formatNo;
	}
	public void setFormatNo(int formatno) {
		this.formatNo = formatno;
	}
	
	
	
}

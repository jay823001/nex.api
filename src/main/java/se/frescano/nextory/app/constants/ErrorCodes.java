package se.frescano.nextory.app.constants;

import se.frescano.nextory.util.Book2GoUtil;

public enum ErrorCodes {

	OK(0, "ok"),

	INPUT_MISSING(1000, "Input missing or invalid","api.INPUT_MISSING"),
	INPUT_LOCALE_MISSING(1017,"locale is mandatory","api.INPUT_LOCALE_MISSING"),
	INPUT_LOCALE_INVALID(1018,"Locale invalid","api.INPUT_LOCALE_INVALID"),
	INTERNAL_ERROR(2000, "Internal error","api.INTERNAL_ERROR"),
	API_DEPRECATED(2001, "Unsupported version","api.API_DEPRECATED"),
	MAINTENANCE_MODE(2002, "System under maintenance","api.MAINTENANCE_MODE"),
	
	AUTHENTICATION_ERROR(3000, "Du har inget abonnemang, kontakta kundservice","api.AUTHENTICATION_ERROR"),
	//TEST_CODE(10000," NEXTORY TEST ERROR CODE"),
	INVALID_TOKEN(3001, "Invalid Token","api.INVALID_TOKEN"),
	INVALID_USER(3012, "Invalid User","api.INVALID_USER"),
	USER_EXIST(3014, "User is already exist","api.USER_EXIST"),
	USERNAME_TAKEN(3002, "Username taken","api.USERNAME_TAKEN"), 
	USERNAME_LENGTH(3003, "Username not in required length","api.USERNAME_LENGTH"),
	PASSWORD_LENGTH(3004,"Password not in required length","api.PASSWORD_LENGTH"),
	CHECKSUM_MISMATCH(3005,"Check sum mismatch","api.CHECKSUM_MISMATCH"),
	INACTIVE_USER(3006, "Inactive User","api.INACTIVE_USER"),
	USER_NOT_FOUND(3007, "Denna e-postadress finns inte registrerad hos Nextory","api.USER_NOT_FOUND"), 
	DUPLICATE_LOGIN_ERROR(3008,"User already loggedin from other device","api.DUPLICATE_LOGIN_ERROR"),
	DEVICE_NOT_FOUND(3009,"Either user logged from other device or inactive token","api.DEVICE_NOT_FOUND"),
	TOKEN_MISMATCH_FOUND(3010, "token mismatch found","api.TOKEN_MISMATCH_FOUND"),
	FORGOT_PASSWORD_MAIL_INVALID(3011, "User not available","api.FORGOT_PASSWORD_MAIL_INVALID"),
	
	// ITEM_UNKNOWN(4000," Item unknown/missing "),
	DOWNLOAD_URL_ERROR(4001, " Could not generate download url ","api.DOWNLOAD_URL_ERROR"),
	CATEGORY_NOT_FOUND(4007, " Category Not Found","api.CATEGORY_NOT_FOUND"),
	SEARCH_RETURN_NO_DATA(4009, " Search returned no data ","api.SEARCH_RETURN_NO_DATA"), 
	NO_ITEM_WITH_FORMAT_AVAILALABLE(4008, " No Item found with this format ","api.NO_ITEM_WITH_FORMAT_AVAILALABLE"),

	ORDER_FAILED(5000, "Order failed","api.ORDER_FAILED"),
	CART_BOOK_ALREADYPURCHASED(5001,"Book already purchased","api.CART_BOOK_ALREADYPURCHASED"),
	// POINTS_NOT_AVAILABLE(5002,"User donot have enough points to complete this order"),
	USER_A_MEMBER(5003, "User is already a member","api.USER_A_MEMBER"),
	CANCELLED_MEMBER(5008,"user already cancelled membership","api.CANCELLED_MEMBER"), 
	USER_NONMEMBER(5009,"user not a member","api.USER_NONMEMBER"),
	// USER_NOT_A_MEMBER(5004,"User is not a member"),
	PURCHASE_ORDER_NOT_ALLOWED(5005,"User already member.Purchase orders not allowed","api.PURCHASE_ORDER_NOT_ALLOWED"), 
	UNLIMITED_ORDER_NOT_ALLOWED(5006, "User is not a member","api.UNLIMITED_ORDER_NOT_ALLOWED"), 
	GIFTCARD_PURCHASE_NOT_ALLOWED(5007,"Giftcard purchase not allowed for memeber","api.GIFTCARD_PURCHASE_NOT_ALLOWED"),
	LIBRARY_BOOK_NOT_FOUND(6000, "product not found in library","api.LIBRARY_BOOK_NOT_FOUND"),
	INVALID_PRODUCT(6001, "piece/active/refund status products cannot be deleted","api.INVALID_PRODUCT"),
	LIBRARY_SETTING_ACTION_ERROR(6002, "action not found","api.LIBRARY_SETTING_ACTION_ERROR"), 
	LIBRARY_PURCHASE_DETAILS_NOTFOUND(6003,"order details not found ","api.LIBRARY_PURCHASE_DETAILS_NOTFOUND"), 
	LIBRARY_OFFINE_READER_MAX_REACHED_ERROR(6004, "max offline reader count reached","api.LIBRARY_OFFINE_READER_MAX_REACHED_ERROR"), 
	LIBRARY_USER_NOT_MEMBER(6005, "user not a member","api.LIBRARY_USER_NOT_MEMBER"),
	LIBRARY_BOOK_IN_E2GOORDERSTATE(6006,"Please activate the book before downloading","api.LIBRARY_BOOK_IN_E2GOORDERSTATE"),
	LIBRARY_BOOK_IN_INACTIVATESTATE(6007, "Please activate the book before downloading","api.LIBRARY_BOOK_IN_INACTIVATESTATE"),
	LIBRARY_PUB_NOT_ACTIVE(6008, "The publisher is not active ","api.LIBRARY_PUB_NOT_ACTIVE"), 
	LIBRARY_BOOK_EXISTS(6009," Book already added to the library ","api.LIBRARY_BOOK_EXISTS"),
	LIBRARY_MODIFIED_ERROR(6010,"Customer library modified","api.LIBRARY_MODIFIED_ERROR"),
	LIBRARY_NOT_OWNED_BY_USER_ERROR(6011,"Customer not a owner of the library item ","api.LIBRARY_NOT_OWNED_BY_USER_ERROR"),

	LIBRARY_BOOK_SETTING_NOT_FOUND(7000,"library detail book setting not found","api.LIBRARY_BOOK_SETTING_NOT_FOUND"), 
	LIBRARY_SYNC_NOT_DONE(7001, "library sync not done","api.LIBRARY_SYNC_NOT_DONE"),
	LIBRARY_BOOK_NOT_AVAILABLE_FOR_USER(
			7002, "Denna bok ingår inte I ditt abonnemang","api.LIBRARY_BOOK_NOT_AVAILABLE_FOR_USER"),
	///NO_BANNER_FOUND(8000, "no banners found","api.NO_BANNER_FOUND"), 
	//USER PROFILE UPDATE
	MOBILE_N0_NOTVALID(9000, "Input missing or invalid","api.MOBILE_N0_NOTVALID"),
	FIRSTNAME_LENGTH(9001,"First Name length invalid","api.FIRSTNAME_LENGTH"),
	AFTERNAME_LENGTH(9002,"Last Name length invalid","api.AFTERNAME_LENGTH"),
	//Empty password changes
	EMPTY_PASSOWRD(9003,"Password cannot be empty","api.PASSWORD_MISSING"),

	//error msg
	//API_VER_NOT_AVAILABLE(9004,"Api version not available","api.APP_VERSION_NOT_AVAILABLE");
	API_VER_NOT_AVAILABLE(9004,"Uppdatera appen och få tillgång till Nextorys nya funktioner.","api.API_VER_NOT_AVAILABLE"),	
	
	EMPTY_LOGINKEY(9005,"Login key cannot be empty","api.LOGINKEY_MISSING"),
	EMPTY_SIGNUPDETAILS(9006,"Login details cannot be empty","api.SIGNUPDETILS_MISSING"),
	/**Family Accounts */
	PROFILENAME_LENGTH(10001,"Ogiltigt profilnamn","api.PROFILENAME_LENGTH"), 
	PROFILE_CATEGORY_INVALID(10002,"Ett fel uppstod, vänligen försök igen.","api.PROFILE_CATEGORY_INVALID"),
	PROFILE_LIMIT_EXCEEDED(10003, "Du kan inte skapa fler profiler.","api.PROFILE_LIMIT_EXCEEDED"),
	PROFILE_NAME_EXISTS(10004, "Profilnamnet finns redan.","api.PROFILE_NAME_EXISTS"),
	PROFILE_LOGINKEY_FAILED(10005,"Ett fel uppstod vid profil-login, vänligen försök igen","api.PROFILE_LOGINKEY_FAILED"),
	PROFILE_LOGINKEY_INVALID(10006, "Ett fel uppstod vid login, vänligen försök igen.","api.PROFILE_LOGINKEY_INVALID"),
	PROFILE_CREATION_NOTALLOWED(10007, "Du kan inte skapa profiler med ditt abonnemang.","api.PROFILE_CREATION_NOTALLOWED"),
	PROFILE_DELETE_NOTALLOWED(10008, "Huvudprofilen går inte att radera.","api.PROFILE_DELETE_NOTALLOWED"),
	PROFILE_NOT_FOUND(10009,"Profilen existerar inte","api.PROFILE_NOT_FOUND"),
	PROFILE_ACTION_INVALID(10010,"Det går inte att utföra denna åtgärd.","api.PROFILE_ACTION_INVALID"),
	SUBSCRIPTION_INACTIVE(10011,"Det har uppstått ett fel med ditt abonnemangsform, vänligen kontakta kundtjänst.","api.SUBSCRIPTION_INACTIVE"),
	PROFILE_NOT_ACTIVE(10012,"Profilen är inte längre aktiv.","api.PROFILE_NOT_ACTIVE"),
	PROFILE_ACTION_NOTALLOWED(10013,"Går ej att utföra med Barnprofil.","api.PROFILE_ACTION_NOTALLOWED"),
	UNAUTHORIZED_API_ACCESS(10014, "Ett fel har inträffat logga in igen.","api.UNAUTHORIZED_API_ACCESS"),
	COLORINDEX_RANGE(10015, "Fel uppstod vid val av färg","api.COLORINDEX_RANGE"),
	
	/** Personalized error messages in case of session logout*/
	
	SESSION_ACCOUNT_CANCELLED(1001, "You have been logged out as your Membership got cancelled","api.ACCOUNT_CANCELLED"),
	/*SESSION_ADMIN_CANCELLED(1002, "You have been logged out as Admin cancelled your subscription","api.ADMIN_CANCELLED"),*/
	SESSION_DOWNLOAD_EXCEEDED(1002, "You have been logged out as your daily/monthly download limit was exceeded","api.DOWNLOAD_EXCEEDED"),
	SESSION_SUBSCRIPTION_DOWNGRADE(1003, "You have been logged out as you have downgraded your subscription to {#subscription name}","api.SUBSCRIPTION_DOWNGRADE"),
	SESSION_SUBSCRIPTION_UPGRADE(1004, "You have been logged out as you have upgraded  your subscription to {#subscription name}","api.SUBSCRIPTION_UPGRADE"),
	SESSION_CONCURRENT_NONFAM(1005, "You have been logged out as you have already logged in to { #count } other devices","api.CONCURRENT_NONFAM"),
	SESSION_CONCURRENT_FAM(1006, "Your current session expired as you have login to {#count} other devices","api.CONCURRENT_FAM"),
	SESSION_PROFILE_REMOVED(1007, "You have been logged out as your profile was deleted","api.PROFILE_REMOVED"),
	SESSION_PASSWORD_UPDATED(1008, "You have been logged out as your account password was changed","api.PASSWORD_UPDATED"),
	/*below 2 error codes are used only for internal purpose and is not exposed to API*/
	TOKEN_REMOVED(1009, "Token is removed due to concurrent logins","api.INVALID_TOKEN"),
	TOKEN_DIRTY(1010, "Token is set as dirty due to subscription or membership changes","api.INVALID_TOKEN"),
	USER_ADD_VISITOR(1011, "Item can't be added to My List, since user is a Visitor  ","api.USER_ADD_VISITOR"), 
	USER_ACT_VISITOR(1012, "Item can't be activated, since user is a Visitor ","api.USER_ACT_VISITOR"), 
	NON_MMBER_USER_ADD(1013, "Item can't be added to My List, since user is a Non-Member ","api.NON_MMBER_USER_ADD"), 
	NON_MMBER_USER_ACTIVE(1014, "Item can't be activated, since user is a Non-Member  ","api.NON_MMBER_USER_ACTIVE"),
	SESSION_ACCOUNTTYPE_CHANGED(1015, "account type has been changed ","api.ACCOUNTTYPE_CHANGED"),
	SESSION_ACCOUNTTYPE_SUBSCRIPTION_CHANGED(1016, "account type and subscription has been changed ","api.ACCOUNTTYPE_SUBSCRIPTION_CHANGED");

	private int errorcode = 0;
	private String message = "";
	private String i18key = "NOT_IMPLEMENTED";
	private boolean isError = true;
	private ErrorCodes(int errorCode, String message, String i18key) {
		this.errorcode = errorCode;
		this.message = message;
		this.i18key = i18key;
		this.isError = true;
	}
	private ErrorCodes(int errorCode, String message) {
		this.errorcode = errorCode;
		this.message = message;
		this.i18key = "NOT_IMPLEMENTED";
		this.isError = true;
	}
	private ErrorCodes(int errorCode, String message, boolean isError) {
		this.errorcode = errorCode;
		this.message = message;
		this.i18key = null;
		this.isError = isError;
	}
	public int getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
	public String getMessage() {
		String i18message = Book2GoUtil.getLocalizedString(null, i18key);
		return i18message==null?message:i18message;
	}
	/*public void setMessage(String message) {
		this.message = message;
	}*/
	
	public String getI18key() {
		return i18key;
	}
	public void setI18key(String i18key) {
		this.i18key = i18key;
	}
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}

	public static ErrorCodes	getErrorCodeDetails(int errorcode){
		for(ErrorCodes code : values()){
			if( code.getErrorcode() == errorcode )
				return code;
		}
		return ErrorCodes.INVALID_TOKEN;
	}
}

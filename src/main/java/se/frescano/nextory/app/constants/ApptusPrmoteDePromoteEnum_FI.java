package se.frescano.nextory.app.constants;

import java.util.HashMap;

public enum ApptusPrmoteDePromoteEnum_FI {
	
	TOP_EBOOK("topplista-e-bocker" , "s_topE_FI"),
	TOP_ABOOK("topplista-ljudbocker" , "s_topA_FI"),
	TOP_BOOK("topplista-bocker" , "s_topB_FI"),

	DEKKARIT("dekkarit_1","s_FI_1"),
	JANNITYS("jannitys_1","s_FI_2"),
	LASTENKIRJAT("lastenkirjat_1","s_FI_3"),
	FEELGOOD_FI("feelgood_1","s_FI_4"),
	ELAMAKERRAT_MUISTELMAT("elamakerrat_muistelmat_1","s_FI_5"),
	KAUNOKIRJALLISUUS("kaunokirjallisuus_1","s_FI_6"),
	FANTASIA_SCIENCEFICTION("fantasia_sciencefiction_1","s_FI_7"),
	EROTIIKKA("erotiikka_1","s_FI_8"),
	HENKILOKOHTAINEN_KASVU("henkilokohtainen_kasvu_1","s_FI_9"),
	TIETOKIRJAT("tietokirjat_1","s_FI_10"),
	YHTEISKUNTA_POLITIIKKA("yhteiskunta_politiikka_1","s_FI_11"),
	SARJAKUVAT_HUUMORI("sarjakuvat_huumori_1","s_FI_12"),
	ELAMANTAPAKIRJALLISUUS("elamantapakirjallisuus_1","s_FI_13");
	
	private String groupname;
	private String groupid;
	//private int ordering;
	static ApptusPrmoteDePromoteEnum_FI[] values = values();

	
	public static ApptusPrmoteDePromoteEnum_FI getRelatedGroupName(String groupname){
		for (ApptusPrmoteDePromoteEnum_FI value : values) {
			if (value.getGroupname().equals(groupname))
				return value;
		}
		return null;
	}
	
	
	public static ApptusPrmoteDePromoteEnum_FI getRelatedGroup(String groupid){
		for (ApptusPrmoteDePromoteEnum_FI value : values) {
			if (value.getGroupid().equals(groupid))
				return value;
		}
		return null;
	}
	
	public HashMap<String,String> getData()
	{
		HashMap<String,String> map = new HashMap<>();
		for(ApptusPrmoteDePromoteEnum_FI val:values)
		{
			map.put(val.getGroupname(),val.getGroupid());
		}
		return map;
	}
	
	private ApptusPrmoteDePromoteEnum_FI(String groupname, String groupid){
		//this.ordering = ordering;
		this.groupname = groupname;
		this.groupid = groupid;
	}
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	
	/*public int getOrdering() {
		return ordering;
	}
	public void setOrdering(int ordering) {	
		this.ordering = ordering;
	}*/
	
	
	

}
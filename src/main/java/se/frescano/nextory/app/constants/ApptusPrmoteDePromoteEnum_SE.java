package se.frescano.nextory.app.constants;

import java.util.HashMap;

public enum ApptusPrmoteDePromoteEnum_SE {
	
	TOP_EBOOK("topplista-e-bocker" , "s_topE"),
	TOP_ABOOK("topplista-ljudbocker" , "s_topA"),
	TOP_BOOK("topplista-bocker" , "s_topB"),
	DECKARE("deckare_1" , "s_SE_1"),
	SPANNING("spanning_1","s_SE_2"),
	BARNBOCKER("barnbocker_1" , "s_SE_3"),
	FEELGOOD("feelgood_1","s_SE_4"),
	BIOGRAFIER_MEMOARER("biografier_memoarer_1" , "s_SE_5"),
	SKONLITTERATUR("skonlitteratur_1","s_SE_6"),
	Tonar_AND_Young_Adult("tonar_youngadult_1","s_SE_7"),
	FANTASY_SCIENCEFICTION("fantasy_sciencefiction_1" , "s_SE_8"),
	EROTIK("erotik_1" , "s_SE_9"),
	PERSONLIG_UTVECKLING("personlig_utveckling_1","s_SE_10"),
	FAKTA("fakta_1" , "s_SE_11"),
	SAMHALLE_POLITIK("samhalle_politik_1","s_SE_12"),
	SERIER_HUMOR("serier_humor_1","s_SE_13"),
	LIVSSTIL("livsstil_1","s_SE_14");
	//BOOK_TIPS("vi-rekommenderar" , "s_bookTips"),
	
	//TOP_TIER_TOP_LIST_TopA("TOP_TIER_TOP_LIST_TopA" , "tttl_TopA");
	
	private String groupname;
	private String groupid;
	//private int ordering;
	static ApptusPrmoteDePromoteEnum_SE[] values = values();

	
	public static ApptusPrmoteDePromoteEnum_SE getRelatedGroupName(String groupname){
		for (ApptusPrmoteDePromoteEnum_SE value : values) {
			if (value.getGroupname().equals(groupname))
				return value;
		}
		return null;
	}
	
	
	public static ApptusPrmoteDePromoteEnum_SE getRelatedGroup(String groupid){
		for (ApptusPrmoteDePromoteEnum_SE value : values) {
			if (value.getGroupid().equals(groupid))
				return value;
		}
		return null;
	}
	
	public HashMap<String,String> getData()
	{
		HashMap<String,String> map = new HashMap<>();
		for(ApptusPrmoteDePromoteEnum_SE val:values)
		{
			map.put(val.getGroupname(),val.getGroupid());
		}
		return map;
	}
	
	private ApptusPrmoteDePromoteEnum_SE(String groupname, String groupid){
		//this.ordering = ordering;
		this.groupname = groupname;
		this.groupid = groupid;
	}
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	
	/*public int getOrdering() {
		return ordering;
	}
	public void setOrdering(int ordering) {	
		this.ordering = ordering;
	}*/
	
	
	

}

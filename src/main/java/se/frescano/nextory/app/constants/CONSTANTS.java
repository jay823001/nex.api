package se.frescano.nextory.app.constants;

import java.util.regex.Pattern;

public class CONSTANTS
{
	public static final String APIVERSION = "APIVERSION";
	public static String USER_AUTH_TOKEN	 	= "USER_AUTH_TOKEN";
	public static final String SALT = "WFNQlLCH";
	public static String MANUAL_SORT_PREFIX 	= "manualsort.";
	public static String MANUAL_SORT_SUFFIX ="_pos";

	public static final String AUTHORIZATION 		= "Authorization";
	public static final String AUTHORIZATION_BASIC	= "Basic ";
	public static final String AUTHORIZATION_US_ASCII	= "US-ASCII";
	public static final String UTF_8_CHARSET		= "UTF-8";
	public static final String COUNTRY_CODE			= "countrycode";
	public static final String FACEBOOK_APP_ID							= "315331678539858";
	
	public static final String SYNCH_SETTINGS_BEAN	= "SYNCH_SETTINGS_BEAN";
	
	public static final String SESSION_GIFTCARD						   = "PRESENT_KORT";
	public static final String SESSION_NEW_USER						= "SESSION_NEW_USER";

	public static final String LOGGEDIN_USER							  = "LOGGED_IN_USER";
	public static final String SESSION_SHOPPING_CART_TRANS_KEY			= "trans_key";
	public static final String SESSION_SHOPPING_CART_INVALID_SESSION	  = "invalidsession";
	
	public static final String SESSION_PASSWORD_RESET_KEY				 = "PASSWORD_RESET_KEY";
	public static final String SESSION_CARD_EXPIRY_SOON				   = "CARD_EXPIRY_SOON";

	public static final String REQUEST_ORDER_REF						  = "e2goref";
	public static final String URL_PARAMETER_NAME						 = "urlParameterName";

	public static final int	USER_PERSONAL_NO_LEN					   = 10;
	public static final String AUTH_TOKEN_EXTENSTION_DELIMITER 		   = "V_";

	public static final int	USER_MOBILE_AND_CELL_MIN_LEN			   = 7;
	public static final int	USER_MOBILE_AND_CELL_MAX_LEN			   = 12;

	public static final int	USER_PASSWORD_MAXLEN					   = 25;
	public static final int	USER_PASSWORD_MINLEN					   = 4;
	
	public static final int	MYPAGES_SORTBY_INVOICE					 = 4;
	public static final int	MYPAGES_SORTBY_PRICE					   = 5;

	public static final int	USER_POSTCODE_MINLEN					   = 4;
	public static final int	USER_CITY_MAXLEN						   = 45;
	public static final String COOKIENAME								 = "carthash";
	public static final String SESSION_SHOPPINGCART_CAMPAGIN_CODE		 = "CAMPAGIN_CODE";

	public static final String TD_COOKIENAME							  = "TRADEDOUBLER";

	public static final String UPDATED_BY								 = "USER";

	public static final String MEMBERSHIP_SUBSCRIPTION					= "Prenumeration";
	public static final int	VAT_PROD_OTHER							 = 99;

	public static final int	PRODUCT_LIMIT_NORMAL_PAGES				 = 5;
	public static final int	PRODUCT_LIMIT_HOME_PAGE					= 10;
	public static final int	PRODUCT_LIMIT_DEVICE					   = 50;
	public static final int	GENERIC_TOP_PRODUCTS					   = 2;

	public static final int	API_RESULT_LIMIT						   = 50;

	public static final int	ALL_FORMAT								 = 0;

	public static final int	DEFAULT_PAGENUMBER						 = 1;
	public static final int	DEFAULT_PAGESIZE_WEB					   = 12;
	public static final int	DEFAULT_PAGESIZE_DEVICE					= 12;

	public static final int	QUANTITY								   = 1;

	public static final String DATEFORMAT								 = "yyyy/MM/dd HH:mm:ss";
	
	public static final String PAYNOVA_CURRENCY_KEY					   = "paynova.currency";
	public static final String PAYNOVA_LANG_KEY						   = "paynova.language";
	public static final String PAYMENT_FAILED_EXCEPTION				   = "payment.failed.exception";
	public static final String PAYMENT_DATA_TAMPERED_EXCEPTION			= "payment.data.tampered.exception";

	public static final String GIFT_CARD_SUBJECT						  = "Giftcard(s) vocher Details";
	public static final String INVOICE_SUBJECT							= "Invoice Details - Book2go";
	public static final String REQUEST_CHANNEL							= "AJAX";

	public static final int	PRODUCT_PRICE_CAT_AUTOMATTED_ASSOCIATION	= 0;
	public static final int	PRODUCT_PRICE_CAT_MANUAL_ASSOCIATION		= 1;
	public static final int	PRODUCT_PRICE_MANUAL_OVERRIDE			   = 2;
	public static final String PRODUCT_PRICE_MANUAL_CATEGORY			   = "MA";

	public static final String SESSION_USER_ID							 = "CUSTOMER_ID";
	public static final String SESSION_GENERATED_HASHKEY				   = "HASH_KEY";

	public static final int	SYSPROP_SUBMENU_BOOK2GO					 = 1;
	public static final int	SYSPROP_SUBMENU_IMPORT					  = 2;
	public static final int	SYSPROP_SUBMENU_BANK						= 3;
	public static final int	SYSPROP_SUBMENU_CMS						 = 4;
	public static final int	SYSPROP_SUBMENU_OTHER					   = 5;
	public static final int	SYSPROP_SUBMENU_CATEGORY					= 6;
	public static final int	SYSPROP_SUBMENU_POPULAR_EBOOK			   = 7;
	public static final int	SYSPROP_SUBMENU_POPULAR_AUDIO			   = 8;
	public static final int	SYSPROP_SUBMENU_NEW_EBOOK				   = 9;
	public static final int	SYSPROP_SUBMENU_BANNERS					 = 10;
	public static final int	SYSPROP_SUBMENU_BANNERUPLOAD				= 11;
	public static final int	SYSPROP_SUBMENU_TEST_MAIL				   = 12;
	public static final int	SYSPROP_SUBMENU_CACHE					   = 13;
	public static final int	DEFAULT_SUBSCRIPTION_ID					= 6;

	public static final String AUDIO_BOOKS								 = "ljudbocker";
	public static final String E_BOOKS									 = "e-bocker";
	public static final String GIFTCARD_NAME							   = "Presentkort";

	public static final String ELIB_SHOP_ORDER_FAILED_NOTIFICATION_SUBJECT = "Elib shop order failed Notification";

	//COMMON
	//Used to store internal coding key and value 
	public static final String TEMPLATE_NAME							   = "TEMPLATE_NAME";
	public static final String EMAIL_SUBJECT							   = "EMAIL_SUBJECT";
	public static final String USER_EMAIL_ID							   = "USER_EMAIL_ID";
	public static final String USER_LIBRARY_LINK_URI					   = "USER_LIBRARY_LINK_URI";
	public static final String EMAIL_HEADER_IMG_URI						= "EMAIL_HEADER_IMG_URI";
	public static final String PRODUCT_IMG_URI							 = "PRODUCT_IMG_URI";
	public static final String MAGAZINE_IMG_URL_PART					   = "MAGAZINE_IMG_URL_PART";
	public static final String ADMIN_MSG								   = "ADMIN_MSG";
	public static final String CUSTOMER_EMAIL_ID						   = "CUSTOMER_EMAIL_ID";
	public static final String CUSTOMER_MSG								= "CUSTOMER_MSG";
	public static final String EMAIL_TEMPLATE							  = "EMAIL_TEMPLATE";
	public static final String MEMBERSHIP_ACTIVE_DATE					  = "MEMBERSHIP_ACTIVE_DATE";
	public static final String PRESENTKORT_PURCHASE_AMOUNT				 = "PRESENTKORT_PURCHASE_AMOUNT";
	// Used to store the Template fields
	public static final String FIRST_NAME								  = "FIRST_NAME";
	public static final String USER_LIBRARY_LINK						   = "USER_LIBRARY_LINK";
	public static final String BOOK2GO_URL								 = "BOOK2GO_URL";
	public static final String BOOK2GO_CONTEXT_URL						 = "BOOK2GO_CONTEXT_URL";
	public static final String POPULAR_PRODUCTS							= "POPULAR_PRODUCTS";
	public static final String EMAIL_HEADER_IMG							= "EMAIL_HEADER_IMG";
	public static final String PRODUCT_IMG_PATH							= "PRODUCT_IMG_PATH";
	public static final String MAGAZINE_IMG_PATH						   = "MAGAZINE_IMG_PATH";
	public static final String SUBSCRPTION_REMAIN_DAYS 	   				= "SUBSCRPTION_REMAIN_DAYS";

	public static final String NUMBER_UTIL								 = "NUMBER_UTIL";
	public static final String GIFT_CARD_IN_ORDER						  = "GIFT_CARD_IN_ORDER";	
	public static final String IMAGE_RESIZE_URL							= "IMAGE_RESIZE_URL";
	//TELL YOUR FRIEND 
	public static final String MESSAGE_BEAN								= "MESSAGE_BEAN";
	//SYSTEM ADMIN EXCEPTION BEAN
	public static final String EXCEPTION_BEAN							  = "EXCEPTION_BEAN";
	public static final String ERROR_ID									= "ERROR_ID";
	//FOGOT PASSWORD EMAIL
	public static final String FORGOT_PASSWORD_URL						 = "FORGOT_PASSWORD_URL";

	//ORDER email
	public static final String ORDER_DATA								  = "ORDER_DATA";

	//BOOKCHECK EMAIL
	public static final String USER_LOGIN_LINK							 = "USER_LOGIN_LINK";
	public static final String USER_LOGIN_LINK_URI						 = "USER_LOGIN_LINK_URI";

	public static final String USER_SUBSCRIPTION_POINT					 = "USER_SUBSCRIPTION_POINT";
	public static final String GIFTCARD_TEMPLATE_1_PDF_URI				 = "GIFTCARD_TEMPLATE_1_PDF_URI";
	public static final String GIFTCARD_TEMPLATE_3_PDF_URI				 = "GIFTCARD_TEMPLATE_3_PDF_URI";
	public static final String GIFTCARD_TEMPLATE_6_PDF_URI				 = "GIFTCARD_TEMPLATE_6_PDF_URI";
	public static final String GIFTCARD_TEMPLATE_X_PDF_URI				 = "GIFTCARD_TEMPLATE_X_PDF_URI";
	public static final String GIFTCARD_ATTACHMENT_FILE_NAME			   = "GIFTCARD_ATTACHMENT_FILE_NAME";
	public static final String GIFTCARD_ATTACHMENT_FILE_PATH			   = "GIFTCARD_ATTACHMENT_FILE_PATH";
	public static final String GIFTCARD_FOLDER_NAME						= "GIFTCARD_FOLDER_NAME";
	public static final String SUBSCRIPTION_MONTHS						 = "SUBSCRIPTION_MONTHS";

	public static final String MEMBERSHIP_NAME							 = "MEMBERSHIP_NAME";
	public static final String ROLLBACK_CUSTOMER_PURCHSAE_LIST			 = "ROLLBACK_CUSTOMER_PURCHSAE_LIST";
	public static final String FAILED_CUSTOMER_PURCHSAE_PRODUCT			= "FAILED_CUSTOMER_PURCHSAE_PRODUCT";

	public static final String MAIL_SUBJECT_ELIB_TRANSACATION_FAILED	   = "Elib Shop Order Failed";

	public static final String SHORT_DATE_FORMAT						   = "yyyy-MM-dd";

	public static final float  SUBSCRIPTION_RUN_DAYS					   = 30;
	public static final float  TRAIL_SUBSCRIPTION_RUN_DAYS				 = 7;
	public static final float  VOUCHER_SUBSCRIPTION_RUN_DAYS			   = 90;

	public static final String BOOKCHEQUE_SESSION_DATA					 = "BOOKCHEQUE_SESSION_DATA";
	public static final String BOOKCHEQUE_SESSION_PRODUCTID				= "BOOKCHEQUE_SESSION_PRODUCTID";
	public static final int	OFFLINE_READER_MIN_BOOK_LIMIT			   	= 3;
	public static final int	OFFLINE_READER_MAX_BOOK_LIMIT	   			= 20;
	public static final int	DEFAULT_WINDOWSIZE						  = 12;
	public static final String REFERRER_TOKEN							  = "REFERRER_TOKEN";

	public static final String LIBRARY_SYNC_DEFAULT_DATE				   = "01-01-2014 00:00:00 +0200";
	public static final String AUTH_TOKEN	= "AUTH_TOKEN";
	public static final String USER_INFO	= "USER_INFO";
	/* Payex vat constants .Please contact you superior before changing the value */
	public static final int	VAT_FOR_PURCHASE							= 2500;
	public static final int	VAT_FOR_AGREEMENT						   = 0;
	/* Payex vat constants .Please contact you superior before changing the value */

	/*
	 *  MONGODB CONSTANTS 
	 */

	public static final String EMAIL_TEMPLATE_BANNER_URL				   = "EMAIL_TEMPLATE_BANNER_URL";

	public static final String MONGO_E2GO_SUBSCRIPTION_CHANGE_LOG			   = "subscription_change_log";
	public static final String MONGO_E2GO_SUBSCRIPTION_DAILY_LOG				 = "subscription_daily_log";
	public static final String MONGO_E2GO_LIBRARY_READ_PERCENTAGE_LOG		   = "app_library_read_percentage_log";
	
	
	public static final String MONGO_NEXT_MEMBERSHIP_CHANGE_LOG  = "nx_membership_change_log";
	public static final String MONGO_NEXT_MEMBERSHIP_DAILY_LOG  = "nx_membership_daily_log";
	public static final String MONGO_NEXT_READNING_BEHAVIOUR_LOG = "nx_reading_behaviour_log";
	public static final String MONGO_NEXT_LIBRARY_BEHAVIOUR_LOG  = "nx_library_behaviour_log";
	public static final String MONGO_NEXT_CUSTOMER_SEARCH_BEHAVIOUR_LOG = "nx_customer_search_behaviour_log";
	public static final String MONGO_NEXTB_LIBRARY_BEHAVIOUR_LOG  = "nextory_price_behaviour_log";
	public static final String MONGO_LAST_MEMBERSHIP_LOG  = "nx_last_membership_log";
	public static final String MONGO_NEXT_DAILY_JOBS_LOG = "nx_customer_daily_jobs_log";
	public static final String MONGO_NEXT_SUBSCRIPTION_MASTER_LOG = "nx_subscription_master_log";
	public static final String MONGO_NEXT_PRODUCT_ALLOWED_FOR_LOG = "nx_product_allowed_for_log";
	public static final String MONGO_NEXT_LIBRARY_BOOK_BEHAVIOUR_LOG  = "nx_library_book_behaviour_log";
	public static final String MONGO_NEXT_BOOK_ACTIVATE_DEACTIVATE_LOG = "nx_book_activation_deactivation_log";
	//NEX-730
	public static final String MONGO_NEXT_APP_DEVICE_LOG = "nx_app_device_log";
	public static final String MONGO_NEXT_MEMBERSHIP_CHANGE_REDUCED_LOG = "nx_last_membership_log";
	public static final String MONGO_NEXT_INACTIVE_BOOK_READNING_BEHAVIOUR_LOG = "nx_inactive_book_reading_behaviour_log";
	

	public static final String TOTAL_BOOKCOUNT							 = "TOTAL_BOOKCOUNT";
	public static final String E_BOOKCOUNT 								="E_BOOKCOUNT";
	public static final String AUDIO_BOOKCOUNT 							="AUDIO_BOOKCOUNT";
	
	public static final String RECRUIT_FRIEND_FREE_DAYS 				="RECRUIT_FRIEND_FREE_DAYS";
	public static final String PASSWORD="password";
	public static final String IGNORE_ACTUAL_PASSWORD="PWDIGNORE";
	public static final String USER_PASSWORD="76C3123DBEDEC1E13F920DB346B9C5BC";

	public static final String MONGO_REPORT_LIBRARY_PUBLISHER_LOG = "report_library_publisher_log";
	//public static final String PASSWORD_PATTERN="^[a-zA-Z0-9_@#$%-]*$";
	public static final String PASSWORD_PATTERN="^[a-zA-Z0-9_@#$%-]*$";
	public static final String PASSWORD_PATTERN_SWEDISH = "[ÅåÄäÖö\\s]";
	public static final String FACEBOOK_LINK				 = "FACEBOOK_LINK";
	public static final String TWITTER_LINK				 = "TWITTER_LINK";
	public static final String INSTAGRAM_LINK				 = "INSTAGRAM_LINK";
	
	public static final String PRODUCTLIST_MANUAL="MANUAL";
	public static final String PRODUCTLIST_AUTO="AUTO";

	//For menu highlighting
	public static final int KONTO_LOGGA_IN_MENU_ITEM	= 1;
	public static final int EBOOK_MENU_ITEM		        = 2;
	public static final int AUDIOBOOK_MENU_ITEM		    = 3;
	public static final int TOPLIST_SLIDE_WINDOW_SIZE = 50;
	public static final String PROVIDER = "provider";
	public static final String RECURRING ="RECURRING";
	public static final String CURRENCY = "SEK";
	public static final String SUBID = "subId";
	public static final String FLOW = "flow";
	public static final String DIRECT_ACTIVATION_FLOW = "direct_activation_flow";
	public static final String CAMP_BEAN = "paymentBean";
	public static final String GIFT_BEAN ="giftCardBean";
	public static final String MONGO_NEXT_EMAIL_LOGS = "nx_customer_email_logs";
	public static final String MAIL_SENDER_NAME							   = "MAIL_SENDER_NAME";
	public static final String APP_LIBRARY_SYNC_SETTING_LOG = "app_library_sync_setting_log";
	public static final String APP_LIBRARY_SYNC_BOOKMARKS_LOG = "app_library_sync_bookmarks_log";
	public static final String APP_LIBRARY_SYNC_HIGHLIGHT_LOG = "app_library_sync_highlight_log";
	public static final String APP_LIBRARY_SYNC_PAGING_LOG = "app_library_sync_paging_log";
	/*public static final String CAMPAIGN_BG_NAME_EXT = "Campaigns_BG.jpg";*/
	//public static final int MAX_ALLOWED_SUBSCRIPTION_DAYS = 29;
	//new template version
	public static final String RULEMAILER_UPGRADE_SUBSCRIPTION_MAIL_TRIGGER_V2 = "Subscription upgrade";
	public static final String RULEMAILER_DOWNGRADE_SUBSCRIPTION_MAIL_TRIGGER_V2 = "Subscription downgrade";
	
	public static final String SEARCH_UPDATED = "UPDATED";
	public static final String SEARCH_FAILED = "FAILED";
	public static final String SEARCH_SUCCESS = "SUCCESS";
	//public static final String APIVERSION	= "APIVERSION";
	public static final Double[]	DOWNWORD_COMPATIBILITY = new Double[]{5.0};
	public static final int			DOWNWORD_COMPATIBILITY_BOOKS_LIMIT = 3;
	public static final Double[]	FEATURE_COMPATIBILITY = new Double[]{5.1};
	public static final int			FEATURE_COMPATIBILITY_BOOKS_LIMIT = 10;
	public static final String 		AUTHORREADDING_PLACEHOLDER 	="Författaruppläsning";
	public static final String 		TOPLIST_BOCKER_SLUG_NAME = "topplista-bocker";
	public static final String 		TOPLIST_E_BOCKER_SLUG_NAME = "topplista-e-bocker";
	public static final String 		BOOKTIPS_SLUG_NAME = "vi-rekommenderar";
	public static final String 		LINK 					= "link";
	public static final float DISCOUNTED_CAMPAIGN_PERCENTAGE=50;
	
	public static final byte FIXED_CAMPAIGN=1;
	public static final byte DISCOUNTED_CAMPAIGN=2;	
	public static final String TOPLIST_BOOKGROUP_TYPE = "topplista";
	public static final String CATEGORY_BOOKGROUP_TYPE = "category";
	public static final String WEB_API_SERVICE_TYPE 	= "FETCH_BOOKS_OF_BOOKGROUP";
	public static final String SORT_POPULARITY 	= "popularity";
	public static final String SORT_PUBLISH_DATE 	= "pubdate";
	public static final String SORT_MANUAL 	= "manualsort";
	public static final String VALIDATE_FORGOT_PASSWORD 	= "VALIDATE_FORGOT_PASSWORD";
	public static final String PROCESS_FORGOT_PASSWORD 		= "PROCESS_FORGOT_PASSWORD";
	public static final String	CANCELLED_PARKED			= "CANCELLED_PARKED";
	
	/**Validtion constraints for customer info*/
	public static final int		USER_INPUT_MIN_LEN										= 3;
	public static final int		USER_FIRSTNAME_LEN										= 45;
	public static final int		USER_LASTNAME_LEN										= 45;
	public static final int		USER_ADDRESS1_LEN										= 100;
	public static final int		USER_ADDRESS2_LEN										= 100;
	public static final int		USER_POSTCODE_MAXLEN									= 5;
	public static final int		USER_EMAIL_LEN											= 50;
	public static final int		USER_TELEPHONE_MAXLEN									= 10;
	public static final int		USER_CELLPHONE_NO_LEN									= 10;
	public static final String PASSWORD_ALLOWED_PATTERN="^[a-zA-Z0-9_@#$%-ÅåÄäÖö\\s]*$";
	public static final int		TOP_TITLES_LIMIT										= 3;
	
	public static final Integer DEVICE_NON_MEMBER_NOTATION = 1;
	public static final Integer DEVICE_MEMBER_NOTATION = 2;
	public static final Integer DEVICE_CANCELLED_MEMBER_NOTATION = 3;
	 
	 public static final Integer DEVICE_MEMBER_NOTATION_6_0 = 1;
	 public static final Integer DEVICE_CANCELLED_MEMBER_NOTATION_6_0 = 2;
	 public static final Integer DEVICE_NON_MEMBER_NOTATION_6_0 = 3;
	 public static final Integer DEVICE_VISITOR_6_0 = 4;
	 
	
	 
	 //public static final double API_VERSION_5_0=5.0;
	 //public static final double API_VERSION_5_1=5.1;
	/* public static final double API_VERSION_5_2=5.2;
	 public static final double API_VERSION_5_3=5.3;
	 public static final double API_VERSION_5_4=5.4;
	 public static final double API_VERSION_5_5=5.5;
	 public static final double API_VERSION_5_6=5.6; 
	 public static final double API_VERSION_5_7=5.7; 
	 public static final double API_VERSION_6_0=6.0; 
	 public static final double API_VERSION_6_1=6.1;*/ 
	 public static final double API_VERSION_6_2=6.2;
	 public static final double API_VERSION_6_3=6.3;
	 public static final double API_VERSION_6_4=6.4;
	 
	 public static final String UNDEFINED 	= "UNDEFINED";
	 public static enum CUSTOM_SUBSCRIPTION {LAST, ASK};
	 
	 /**Family account profile*/
	public static final int		USER_PROFILENAME_LEN										= 45; 
	public static final int		PROFILE_CATEGORY_LEN										= 20;
	public static enum PROFILE_CATEGORIES {
		ALL, KIDS;
		public String toString() {return name().toLowerCase();}
		 
	};
	public static enum ACCOUNT_TYPE { 
		//old account_type_code & new code as parameters, 0 means not used 
		VISITOR(DEVICE_NON_MEMBER_NOTATION, DEVICE_VISITOR_6_0),
		MEMBER(DEVICE_MEMBER_NOTATION,DEVICE_MEMBER_NOTATION_6_0), 
		NON_MEMBER(DEVICE_NON_MEMBER_NOTATION, DEVICE_NON_MEMBER_NOTATION_6_0),
		CANCELLED_MEMBER(DEVICE_CANCELLED_MEMBER_NOTATION, DEVICE_CANCELLED_MEMBER_NOTATION_6_0);
		int account_type_code;
		int account_type_code_v6;
		
		private ACCOUNT_TYPE(int account_type_code, int account_type_code_v6) {
			this.account_type_code = account_type_code;
			this.account_type_code_v6 = account_type_code_v6;
		}
		
		public int getAccountTypeCode(Double apiVersion){
			return this.account_type_code_v6;
		}
	};
	
	public static enum PROFILE_STATUS {
		ACTIVE, INACTIVE,DELETED;
			public String toString() {return name().toLowerCase();}
	};
	public static enum TOKEN_TYPE {LT,PT}; 
	public static enum PROFILE_ALLOWED_ACTION {
		ADD,EDIT,DELETE;
		public String toString() {return name().toLowerCase();}
	};
	/*public static String[] LT_TOKEN_ALLOWED_API = {
								APIJSONURLCONSTANTS.LOGIN_URL, APIJSONURLCONSTANTS.LOGOUT_URL,
								APIJSONURLCONSTANTS.MODIFY_PROFILE, APIJSONURLCONSTANTS.LIST_PROFILES
							};*/
	public static String[] FAMILY_COLOR_CODES = {"#e7be2b", "#8484b0", "#d4524c", "#a69e7d", "#f39200", "#ccdc7c", "#679840", "#667a85", "#009fe3"};
	
	//public static String NEST_BASE_URL;//value is loaded from database properties in startupbean
	//public static final String NEST_SUBSCRIPTIONTYPE_URL = "/app/ver2/fetch/subscriptiontype";
	//public static final String NEST_CATEGORIES_URL = "/api/categories";
	//public static final String NEST_MARKETS_URL = "/api/1.0/markets";
	//public static final String NEST_CATEGORIES_URL_v1 = "/api/1.0/categories?countrycode=";
	//public static final String NEST_DISTRIBUTOR_URL = "/app/ver2/fetch/distributorlist";
	public static final String DELIMITER = "|";
	
	//public static final String WEB_ACCESS_URL = "/app/changesubscription/auth?websitekey=";
	public static final String DATEFORMAT_MONGO_LOG = "yyyyMMdd";

	public static final Integer DEFAULT_VISITOR_PROFILE_ID = -1;
	public static final Integer  DEFAULT_VISITOR_SUBSCRIPTION_ID = 6;//default Guld subscription
	//public static enum USER_ROLES {WEB,APP}; 
	public static final double WEB_API_VERSION_3_0=3.0;
	public static final String[] WEB_TOKEN_EXCLUDE_URL = {"/login"};
	public static  enum WEB_TOKEN_TYPE{ACCESS, REFRESH};
	public static final String WEB_AUTH_KEY = "authkey";
	
	public static final int DEFAULT_PRODUCT_RATING = 3;
	public static final int DEFAULT_PRODUCT_RATING_COUNT = 1;//no of product ratings  
	
	public static final String DEFAULT_LOCALE = "sv_SE";
	//public static final String MARKET_ID_ATTRIBUTENAME = "marketid";
	//public static final String COUNTRY_CODE_ATTRIBUTENAME = "countrycode";
	public static final String LOCALE_PARAMNAME = "locale";
	public static final Pattern 	API_PATTERN = Pattern.compile("/api(/web|/app|/int|/ext)?(/catalogue|/product)?/(\\d.\\d)");
	public static enum API_TYPE {WEB,APP,EXT,INT}; 
	public static final double WEB_API_VERSION_4_0	=	4.0;
	public static final double WEB_API_VERSION_6_0	=	6.0;
	public static final String DEFAULT_INPUTS = "webDefaults";
	public static final String COUNTRYCODE_ATTRIBUTENAME = "countrycode";
	public static final String DEFAULT_COUNTRY = "SE";
	//public static final Double[] WEB_API_VERSIONS = {WEB_API_VERSION_3_0, WEB_API_VERSION_4_0};
	//public static final String NEST_CAMP_TYPE_URI = "/api/1.0/getcampaigntypes";
	
	///APPTUS CONSTANTS
	//public static final String RELATED_BOOK_SERIES_TITLE = "Böcker i samma serie";
	
}

package se.frescano.nextory.app.constants;

public enum  TopPanelSubCatEnum {

	
	TopE(1, "Populära e-böcker"), 
	TopA(2, "Populära ljudböcker"),
	Nyheter_SE(3, "Nyheter"),
	Boktips(4,"Boktips"),
	RecommendBasedOnCustomer(5,"Rekommenderas för dig"),
	Nyheter_FI(6, "Uutuudet"),
	Top_Sellers(7,"Most Read Books");
	
	private int	formatId;
	private String formatName;
	
	private TopPanelSubCatEnum(int formatId, String formatName)
	{
		this.formatId = formatId;
		this.formatName = formatName;
	}
	
	static TopPanelSubCatEnum[] values = values();

	public static TopPanelSubCatEnum getFormat(int format)
	{
		for (TopPanelSubCatEnum value : values)
		{
			if (value.getFormatId() == format)
				return value;
		}
		return null;
	}

	public int getFormatId() {
		return formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
}

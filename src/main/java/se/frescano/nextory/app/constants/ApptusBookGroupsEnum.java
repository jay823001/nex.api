package se.frescano.nextory.app.constants;

public enum ApptusBookGroupsEnum {
	
	MAIN_SERIES("MAIN SERIES" , "s_"),
	ID_SEPERATOR("ID_SEPERATOR" , "_"),
	TOP_TIER_CATEGORIES("TOP_TIER_CATEGORIES" , "ttc_"),
	TOP_TIER_TOP_LIST_Nyther("TOP_TIER_TOP_LIST_Nyther" , "tttl_Nyheter"),
	TOP_TIER_TOP_LIST_BookTips("TOP_TIER_TOP_LIST_BookTips" , "tttl_BookTips"),
	TOP_TIER_TOP_LIST_Dynamic("TOP_TIER_TOP_LIST_Dynamic" , "tttl_dynamic"),
	TOP_TIER_TOP_LIST_RecommendBasedCustomer("TOP_TIER_TOP_LIST_Recommend" , "tttl_Rec"),
	TOP_TIER_TOP_LIST_TopSeller("TOP_TIER_TOP_LIST_TopSeller" , "tttl_topSeller"),
	TOP_TIER_TOP_LIST_UPCOMING("TOP_TIER_TOP_LIST_UPCOMING" , "tttl_upcoming"),
	TOP_TIER_TOP_LIST_TopE("TOP_TIER_TOP_LIST_TopE" , "tttl_TopE"),
	SUB_CATEGORY("SUB_CATEGORY" , "sc_"),
	SUB_CATEGORY_TOP_LIST("SUB_CATEGORY_TOP_LIST" , "stl_"),
	GENERIC_TOPE("GENERIC_TOPE" , "TopE"),
	GENERIC_TOPA("GENERIC_TOPA" , "TopA"),
	GENERIC_Nyheter("GENERIC_Nyheter" , "Nyheter"),
	SUB_CATEGORY_TOP_TIER_CATEGORY("SUB_CATEGORY_TOP_TIER_CATEGORY" , "stl_ttc_"),
	RELATED_BOOK_GROUP("RELATED_BOOK_GROUP" , "r_"),
	SEARCH_GROUP("RELATED_BOOK_GROUP" , "search_"),
	RELATED_BOOK_GROUP_SERIES("RELATED_BOOK_GROUP_SERIES" , "series"),
	RELATED_BOOK_GROUP_AUTHORS("RELATED_BOOK_GROUP_AUTHORS" , "authors"),
	RELATED_BOOK_GROUP_NARRATORS("RELATED_BOOK_GROUP_NARRATORS" , "narrators"),
	RELATED_BOOK_GROUP_RECOMMEND("RELATED_BOOK_GROUP_RECOMMEND" , "rec"),
	TOP_TIER_TOP_LIST_TYPE("TOP_TIER_TOP_LIST_TYPE" , "Top-Tier Top-List"),
	Promotion_LIST_TYPE("Promotion_LIST_TYPE" , "Promotion"),
	TOP_TIER_TOP_CATEGORY_TYPE("TOP_TIER_TOP_CATEGORY_TYPE" , "Top-Tier Category"),
	SUB_CATEGORY_TOP_LIST_TYPE("SUB_CATEGORY_TOP_LIST_TYPE" , "Sub-Top-List"),
	SUB_CATEGORY_TOP_CATEGORY_TYPE("SUB_CATEGORY_TOP_CATEGORY_TYPE" , "Sub-Category"),
	TOP_SERIES_TYPE("TOP_SERIES_TYPE" , "Series"),
	RELATED_TYPE("RELATED_TYPE" , "Related"),
	RELATED_RECOMMEND_URL_TO_BE_FIRED("RELATED_RECOMMEND_URL_TO_BE_FIRED" , ""),
	TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED("TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED" , "tttl_Rec"),
	EMPTY_DATA_TO_BE_RETURNED_RESPONSE("EMPTY_DATA_TO_BE_RETURNED_RESPONSE" , "emptydata"),
	NOT_VALID_BOOK_GROUP_ID("NOT_VALID_BOOK_GROUP_ID" , "NotValidBookGroupId"),
	TOP_TIER_TOP_LIST_TopB("TOP_TIER_TOP_LIST_TopB" , "tttl_TopB"),
	TOP_TIER_TOP_LIST_TopA("TOP_TIER_TOP_LIST_TopA" , "tttl_TopA");
	
	private String groupname;
	private String groupid;
	//private int ordering;
	static ApptusBookGroupsEnum[] values = values();
	
	public static ApptusBookGroupsEnum getRelatedGroup(String groupid){
		for (ApptusBookGroupsEnum value : values) {
			if (value.getGroupid().equals(groupid))
				return value;
		}
		return null;
	}
	
	private ApptusBookGroupsEnum(String groupname, String groupid){
		//this.ordering = ordering;
		this.groupname = groupname;
		this.groupid = groupid;
	}
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	
	/*public int getOrdering() {
		return ordering;
	}
	public void setOrdering(int ordering) {	
		this.ordering = ordering;
	}*/
	
	
	

}

package se.frescano.nextory.app.constants;

import java.util.HashMap;

public enum ApptusPrmoteDePromoteEnum_DE {
	
	TOP_EBOOK("topplista-e-bocker" , "s_topE_DE"),
	TOP_ABOOK("topplista-ljudbocker" , "s_topA_DE"),
	TOP_BOOK("topplista-bocker" , "s_topB_DE");
	/*Nyheter("Nyheter" , "s_nyheter_DE"),
	BOOK_TIPS("vi-rekommenderar" , "s_bookTips_DE"),
	BOOKS_IN_ENGLISH("books_in_english" , "s_books_in_english_DE"),
	BOOKS_IN_SWEDISH("bocker_pa_svenska","s_books_in_swedish_DE");*/
	
	private String groupname;
	private String groupid;
	//private int ordering;
	static ApptusPrmoteDePromoteEnum_DE[] values = values();

	
	public static ApptusPrmoteDePromoteEnum_DE getRelatedGroupName(String groupname){
		for (ApptusPrmoteDePromoteEnum_DE value : values) {
			if (value.getGroupname().equals(groupname))
				return value;
		}
		return null;
	}
	
	
	public static ApptusPrmoteDePromoteEnum_DE getRelatedGroup(String groupid){
		for (ApptusPrmoteDePromoteEnum_DE value : values) {
			if (value.getGroupid().equals(groupid))
				return value;
		}
		return null;
	}
	
	public HashMap<String,String> getData()
	{
		HashMap<String,String> map = new HashMap<>();
		for(ApptusPrmoteDePromoteEnum_DE val:values)
		{
			map.put(val.getGroupname(),val.getGroupid());
		}
		return map;
	}
	
	private ApptusPrmoteDePromoteEnum_DE(String groupname, String groupid){
		//this.ordering = ordering;
		this.groupname = groupname;
		this.groupid = groupid;
	}
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	
	/*public int getOrdering() {
		return ordering;
	}
	public void setOrdering(int ordering) {	
		this.ordering = ordering;
	}*/
	
	
	

}
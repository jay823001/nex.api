package se.frescano.nextory.app.constants;

import java.util.HashMap;

public enum ApptusPrmoteDePromoteEnum_FI_Translated {
	
	TOP_EBOOK("topplista-e-bocker" , "s_topE_FI"),
	TOP_ABOOK("topplista-ljudbocker" , "s_topA_FI"),
	Nyheter("Nyheter_FI" , "s_nyheter_FI"),
	TOP_BOOK("topplista-bocker" , "s_topB_FI"),
	BOOK_TIPS("vi-rekommenderar_FI" , "s_bookTips_FI"),
	DICK_JANNITYS("dick_jannitys" , "s_dick_jannitys_FI"),
	LASTEN_KIRJAT("lasten_kirjat" , "s_lasten_kirjat_FI"),
	ROMAANIT_FIKTIO("romaanit_fiktio" , "s_romaanit_fiktio_FI"),
	ELAMAKERRAT_MUISTELMAT("elamakerrat_muistelmat" , "s_elamakerrat_muistelmat_FI"),
	Henkilokohtaistakehitysta_Terveys_Koulutus("henkilokohtaistakehitysta_terveys_koulutus" , "s_henkilokohtaistakehitysta_terveys_koulutus_FI"),
	Teini_Nuoretaikuiset("teini_nuoretaikuiset" , "s_teini_nuoretaikuiset_FI"),
	Tosiasiat("tosiasiat" , "s_tosiasiat_FI"),
	Erotiikka_Romantiikka("erotiikka_romantiikka" , "s_erotiikka_romantiikka_FI"),
	Huumori_Sarjakuvasarja("huumori_sarjakuvasarja" , "s_huumori_sarjakuvasarja_FI"),
	Luettavissa("luettavissa" , "s_luettavissa_FI"),
	Business("business" , "s_business_FI"),
	Fantasia_Sciencefiction("fantasia_sciencefiction" , "s_fantasia_sciencefiction_FI"),
	Kielet_Sanakirjat("kielet_sanakirjat" , "s_kielet_sanakirjat_FI"),
	BOOKS_IN_ENGLISH("books_in_english" , "s_books_in_english_FI");
	//BOOK_TIPS("vi-rekommenderar" , "s_bookTips"),
	
	//TOP_TIER_TOP_LIST_TopA("TOP_TIER_TOP_LIST_TopA" , "tttl_TopA");
	
	private String groupname;
	private String groupid;
	//private int ordering;
	static ApptusPrmoteDePromoteEnum_FI_Translated[] values = values();

	
	public static ApptusPrmoteDePromoteEnum_FI_Translated getRelatedGroupName(String groupname){
		for (ApptusPrmoteDePromoteEnum_FI_Translated value : values) {
			if (value.getGroupname().equals(groupname))
				return value;
		}
		return null;
	}
	
	
	public static ApptusPrmoteDePromoteEnum_FI_Translated getRelatedGroup(String groupid){
		for (ApptusPrmoteDePromoteEnum_FI_Translated value : values) {
			if (value.getGroupid().equals(groupid))
				return value;
		}
		return null;
	}
	
	public HashMap<String,String> getData()
	{
		HashMap<String,String> map = new HashMap<>();
		for(ApptusPrmoteDePromoteEnum_FI_Translated val:values)
		{
			map.put(val.getGroupname(),val.getGroupid());
		}
		return map;
	}
	
	private ApptusPrmoteDePromoteEnum_FI_Translated(String groupname, String groupid){
		//this.ordering = ordering;
		this.groupname = groupname;
		this.groupid = groupid;
	}
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	
	/*public int getOrdering() {
		return ordering;
	}
	public void setOrdering(int ordering) {	
		this.ordering = ordering;
	}*/
	
	
	

}

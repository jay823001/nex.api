package se.frescano.nextory.app.exception;

public class E2goJDBCException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5119991610219583806L;
	public E2goJDBCException(Throwable th){
		this(th,null);
	}
	public E2goJDBCException(Throwable th,String message){
		super(message,th);
	}
}

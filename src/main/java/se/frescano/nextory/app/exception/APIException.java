package se.frescano.nextory.app.exception;

import se.frescano.nextory.app.constants.ErrorCodes;

public class APIException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3913487654484813199L;
	ErrorCodes errorCodes;
	
	/*public APIException(){
		super();
	}*/
	
	/*public APIException(String message){
		super(message);
	}*/
	public APIException(ErrorCodes errorCodes){
		super(errorCodes.getMessage());
		this.errorCodes = errorCodes;
	}
	public APIException(String message,ErrorCodes errorCodes){
		super(message);
		this.errorCodes = errorCodes;
	}
	public APIException(ErrorCodes errorCodes,String message){
		super(message);
		this.errorCodes = errorCodes;
	}

	public ErrorCodes getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(ErrorCodes errorCodes) {
		this.errorCodes = errorCodes;
	}
	
}

package se.frescano.nextory.app.exception;

import java.util.ArrayList;
import java.util.List;

import se.frescano.nextory.app.constants.ErrorCodes;

public class TokenNotValidException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4687482775770423186L;
	ErrorCodes errorCodes;
	private List<String> args;
	
	public TokenNotValidException(){
		super();
	}
	
	public TokenNotValidException(ErrorCodes errorCodes){
		super(errorCodes.getMessage());
		this.errorCodes = errorCodes;
	}
	public TokenNotValidException(String message,ErrorCodes errorCodes){
		super(message);
		this.errorCodes = errorCodes;
	}
	 
	public ErrorCodes getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(ErrorCodes errorCodes) {
		this.errorCodes = errorCodes;
	}

	public List<String> getArgs() {
		return args;
	}

	public void setArgs(List<String> args) {
		this.args = args;
	}
	public void addArgs(String arg) {
		if(this.args==null)
			this.args = new ArrayList<String>();
		this.args.add(arg);
	}

	@Override
	public String toString() {
		return "TokenNotValidException [errorCodes=" + errorCodes + ", args=" + args + "]"; 
	}
}

package se.frescano.nextory.app.exception;

public class APIDataOverRideException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6328772196133268569L;

	public APIDataOverRideException(String message){
		super(message);
	}
}

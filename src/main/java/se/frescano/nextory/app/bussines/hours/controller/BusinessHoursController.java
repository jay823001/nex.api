package se.frescano.nextory.app.bussines.hours.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.nextory.app.api.spring.method.resolver.AppRequestBeanInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserToken;
import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.exception.TokenNotValidException;
import se.frescano.nextory.app.request.APIController;
import se.frescano.nextory.app.request.APITokenRequestBean;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.JsonWrapper;
import se.frescano.nextory.app.zendesk.service.ZendeskScheduleService;
import se.frescano.nextory.app.zendesk.vo.ZendeskDateResponseVO;
import se.frescano.nextory.spring.method.model.User;

@RestController
public class BusinessHoursController extends APIController{
	
	Logger logger = LoggerFactory.getLogger(BusinessHoursController.class);
	
	@Autowired
	private  ZendeskScheduleService zendeskScheduleService;
	
	@Autowired
	private Validator validator;
	//@RequestMapping(value={APIJSONURLCONSTANTS.V5.BUSINESS_HOURS,APIJSONURLCONSTANTS.V5_1.BUSINESS_HOURS}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	@RequestMapping(value={APIJSONURLCONSTANTS.BUSINESS_HOURS}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody JsonWrapper fetchBusinessHours(@AppUserToken UserAuthToken authtoken,@AppUserInfo User appuserinfo, @PathVariable("api_version")Double apiVersion, 
			APITokenRequestBean reqBean, BindingResult result,HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean ) throws Exception{
		
		setDefaultParams(reqBean, apiVersion, authtoken, appuserinfo,tokenbean);
		
		Set<ConstraintViolation<APITokenRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		
		/*ErrorCodes errorCodes = validate(logger,reqBean,result,request);
		if(!errorCodes.equals(ErrorCodes.OK)){
			if(logger.isInfoEnabled())logger.info(" DeviceSearchController  fetchBusinessHours , validation failed ");
			return handleErrorPageAsBean(errorCodes,reqBean.getLocale());
		}*/
		try{	
			ZendeskDateResponseVO zendeskResponse = zendeskScheduleService.getBusinessHours();
			return (zendeskResponse !=null  ? new APIJsonWrapper(zendeskResponse):handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale()));			
		}catch(TokenNotValidException e){
			//if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("DeviceSearchController -> fetchBusinessHours ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		} catch (Exception e) {
			logger.error("Error while fetching the nextory business hours "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
			//throw e;
		}
	}

}

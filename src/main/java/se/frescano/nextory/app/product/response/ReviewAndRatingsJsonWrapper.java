package se.frescano.nextory.app.product.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import se.frescano.nextory.app.request.APIWrapperBean;
import se.frescano.nextory.app.response.ReviewAndRating;

public class ReviewAndRatingsJsonWrapper extends APIWrapperBean {
	
	@JsonIgnore
	private ReviewAndRating rating = null;
	
	@JsonIgnore
	boolean andriod ;
	
	@JsonProperty( value = "avgrate")
	public Double	getAvgRatings(){
		if( this.rating != null && this.rating.getAvgRatings() != null )
			return this.rating.getAvgRatings();
		else
			return 0.0;
	}
	
	@JsonProperty( value = "numberofrates")
	public int	getNumberofrates(){
		if( this.rating != null )
			return this.rating.getTotalReviews();
		else
			return 0;
	}
	
	public ReviewAndRating getRating() {
		return rating;
	}

	public void setRating(ReviewAndRating rating) {
		this.rating = rating;
	}

	public boolean isAndriod() {
		return andriod;
	}

	public void setAndriod(boolean andriod) {
		this.andriod = andriod;
	}

}

package se.frescano.nextory.app.product.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import se.frescano.nextory.app.request.APIPaginationBean;

public class AutoCompleteJsonRequestBean extends APIPaginationBean{

	@NotNull(message="api.INPUT_MISSING")
	@Length(min=1,message="api.INPUT_MISSING")
	private String keyword ;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	

	
}

package se.frescano.nextory.app.product.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DownloadFileVo {

	@JsonProperty( value= "url")
	private String	url;
	
	@JsonProperty( value= "formatid")
	private int		formatid;
	
	@JsonProperty( value= "duration")
	private String	duration;
	
	@JsonProperty( value= "sizeinbytes")
	private int		sizeinbytes;
	
	@JsonProperty( value= "cheksum")
	private String	cheksum;
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getFormatid() {
		return formatid;
	}
	public void setFormatid(int formatid) {
		this.formatid = formatid;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public int getSizeinbytes() {
		return sizeinbytes;
	}
	public void setSizeinbytes(int sizeinbytes) {
		this.sizeinbytes = sizeinbytes;
	}
	public String getCheksum() {
		return cheksum;
	}
	public void setCheksum(String cheksum) {
		this.cheksum = cheksum;
	}
	
	
}

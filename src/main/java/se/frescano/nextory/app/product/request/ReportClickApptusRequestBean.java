package se.frescano.nextory.app.product.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import se.frescano.nextory.app.request.APITokenRequestBean;

public class ReportClickApptusRequestBean extends APITokenRequestBean{

	@NotNull(message="api.INPUT_MISSING")
	@Digits(integer=10,fraction=0,message="api.INPUT_MISSING")
	private Integer id ;
	
	
	@NotNull(message="api.INPUT_MISSING")
	@Length(min=1,message="api.INPUT_MISSING")
	private String esalesticket ;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getEsalesticket() {
		return esalesticket;
	}


	public void setEsalesticket(String esalesticket) {
		this.esalesticket = esalesticket;
	}

	
}

package se.frescano.nextory.app.product.response;


public class SeriesInfoBean {
	private Integer productid;
	private String seriesName;
	private Integer sequence;
	//private List<PartialProductInfo> seriesBooksList;
	
	
	
	/*public List<PartialProductInfo> getSeriesBooksList() {
		return seriesBooksList;
	}
	public void setSeriesBooksList(List<PartialProductInfo> seriesBooksList) {
		this.seriesBooksList = seriesBooksList;
	}*/
	public Integer getProductid() {
		return productid;
	}
	public void setProductid(Integer productid) {
		this.productid = productid;
	}
	public String getSeriesName() {
		return seriesName;
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
}
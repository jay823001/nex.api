package se.frescano.nextory.app.product.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;


import se.frescano.nextory.app.request.APIWrapperBean;
import se.frescano.nextory.apptus.model.AdvancedSearchVo.categorySuggestions;

@JsonInclude(Include.NON_NULL)
public class AdvancedSearchWrapper extends APIWrapperBean {
	
	
	@JsonProperty( value = "autoComplete")
	ArrayList<String> autoComplete;
	
	@JsonProperty( value = "autoCompleteAuthors")
	ArrayList<AutoCompleteSeries> autoCompleteAuthors;
		
	@JsonProperty( value = "productSuggestions")
	List<ProductInformation> productSuggestions;
	
	@JsonProperty( value = "searchCount")
	private Integer searchCount =0;
	
	@JsonProperty( value = "productSuggestionsSeries")
	List<ProductInformation> productSuggestionsSeries;
	
	@JsonProperty( value = "autoCompleteNarrators")
	ArrayList<AutoCompleteSeries> autoCompleteNarrators;
	
	@JsonProperty( value = "autoCompleteSeries")
	ArrayList<AutoCompleteSeries> autoCompleteSeries;
	
	@JsonProperty( value = "categorySuggestions")
	ArrayList<AutoCompleteSeries> categorySuggestions;
	
	@JsonProperty( value = "promotionalListSuggestions")
	ArrayList<AutoCompleteSeries> promotionalListSuggestions;
	

	@JsonIgnore
	private Double apiVersion;

	public List<ProductInformation> getProductSuggestions() {
		return productSuggestions;
	}

	public void setProductSuggestions(List<ProductInformation> productSuggestions) {
		this.productSuggestions = productSuggestions;
	}

	public List<ProductInformation> getProductSuggestionsSeries() {
		return productSuggestionsSeries;
	}

	public void setProductSuggestionsSeries(List<ProductInformation> productSuggestionsSeries) {
		this.productSuggestionsSeries = productSuggestionsSeries;
	}

	public ArrayList<String> getAutoComplete() {
		return autoComplete;
	}

	public void setAutoComplete(ArrayList<String> autoComplete) {
		this.autoComplete = autoComplete;
	}

	public ArrayList<AutoCompleteSeries> getAutoCompleteAuthors() {
		return autoCompleteAuthors;
	}

	public void setAutoCompleteAuthors(ArrayList<AutoCompleteSeries> autoCompleteAuthors) {
		this.autoCompleteAuthors = autoCompleteAuthors;
	}

	public ArrayList<AutoCompleteSeries> getAutoCompleteNarrators() {
		return autoCompleteNarrators;
	}

	public void setAutoCompleteNarrators(ArrayList<AutoCompleteSeries> autoCompleteNarrators) {
		this.autoCompleteNarrators = autoCompleteNarrators;
	}

	public Integer getSearchCount() {
		return searchCount;
	}

	public void setSearchCount(Integer searchCount) {
		this.searchCount = searchCount;
	}

	public Double getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(Double apiVersion) {
		this.apiVersion = apiVersion;
	}

	public ArrayList<AutoCompleteSeries> getAutoCompleteSeries() {
		return autoCompleteSeries;
	}

	public void setAutoCompleteSeries(ArrayList<AutoCompleteSeries> autoCompleteSeries) {
		this.autoCompleteSeries = autoCompleteSeries;
	}

	public ArrayList<AutoCompleteSeries> getCategorySuggestions() {
		return categorySuggestions;
	}

	public void setCategorySuggestions(ArrayList<AutoCompleteSeries> categorySuggestions) {
		this.categorySuggestions = categorySuggestions;
	}

	public ArrayList<AutoCompleteSeries> getPromotionalListSuggestions() {
		return promotionalListSuggestions;
	}

	public void setPromotionalListSuggestions(ArrayList<AutoCompleteSeries> promotionalListSuggestions) {
		this.promotionalListSuggestions = promotionalListSuggestions;
	}
	
	
	
}

package se.frescano.nextory.app.product.request;

import javax.validation.constraints.NotNull;

import se.frescano.nextory.app.request.APITokenRequestBean;


public class FetchProductRequestBean extends APITokenRequestBean{
	
	
	@NotNull(message="api.INPUT_MISSING")	
	String idFields;
	
	
	public String getIdFields() {
		return idFields;
	}
	public void setIdFields(String idFields) {
		this.idFields = idFields;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FetchProductRequestBean [getAppId()=");
		builder.append(getAppId());
		builder.append(", getIdFields()=");
		builder.append(getIdFields());
		builder.append(", getVersion()=");
		builder.append(getVersion());
		builder.append(", getModel()=");
		builder.append(getModel());
		builder.append(", getClass()=");
		builder.append(getClass());
		builder.append(", hashCode()=");
		builder.append(hashCode());
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

	
}

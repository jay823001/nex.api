package se.frescano.nextory.app.product.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;


import se.frescano.nextory.app.request.APIWrapperBean;

//@JsonSerialize(using=BookListApptusConvertor.class)
@JsonInclude(Include.NON_NULL)
public class BooksForBookGroupsApptusResponseWrapper extends APIWrapperBean {
	
		
	@JsonProperty( value = "books")
	List<ProductInformation> books;

	@JsonProperty( value = "bookcount")
	private Integer bookCount =0;
	
	@JsonProperty( value = "notallowedbookcount")
	private Integer higherBookCount = 0;
	 
	@JsonIgnore
	private Double apiVersion;
	
	
	

	public Double getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(Double apiVersion) {
		this.apiVersion = apiVersion;
	}

	public Integer getHigherBookCount() {
		return higherBookCount;
	}

	public void setHigherBookCount(Integer higherBookCount) {
		this.higherBookCount = higherBookCount;
	}

	public List<ProductInformation> getBooks() {
		return books;
	}

	public void setBooks(List<ProductInformation> books) {
		this.books = books;
	}

	public Integer getBookCount() {
		return bookCount;
	}

	public void setBookCount(Integer bookCount) {
		this.bookCount = bookCount;
	}
	
	
	
	
	
	
}

package se.frescano.nextory.app.product.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class AutoCompleteSeries
{			
		
	/*App response feilds*/
	@JsonProperty( value = "id")
	private String id;	
	
	@JsonProperty( value = "title")
	private String terms;
	
	@JsonProperty(value ="position")
	private Integer position;
	
	@JsonProperty(value ="allowsorting")
	private Integer allowsorting;
	
	@JsonProperty(value ="showvolume")
	private Integer showvolume;
	
	@JsonProperty(value ="haschild")
	private Integer haschild;
	
	@JsonProperty(value ="type")
	private String type;

	@JsonProperty(value ="parentid")
	private String parentId;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getAllowsorting() {
		return allowsorting;
	}

	public void setAllowsorting(Integer allowsorting) {
		this.allowsorting = allowsorting;
	}

	public Integer getShowvolume() {
		return showvolume;
	}

	public void setShowvolume(Integer showvolume) {
		this.showvolume = showvolume;
	}

	public Integer getHaschild() {
		return haschild;
	}

	public void setHaschild(Integer haschild) {
		this.haschild = haschild;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	

	
	
}

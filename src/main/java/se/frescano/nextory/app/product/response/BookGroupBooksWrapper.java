package se.frescano.nextory.app.product.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.request.APIWrapperBean;

//@JsonSerialize(using =BookListConvertor.class)
public class BookGroupBooksWrapper extends APIWrapperBean
{

	@JsonProperty( value = "books")
	List<ProductInformation> books;

	@JsonProperty( value = "bookcount")
	Integer bookCount;
	
	@JsonIgnore
	boolean sortOnVolume;
	/*Double requestAppVersion;
	boolean positionRequired;
	
	public boolean isPositionRequired() {
		return positionRequired;
	}

	public void setPositionRequired(boolean positionRequired) {
		this.positionRequired = positionRequired;
	}

	public Double getRequestAppVersion() {
		return requestAppVersion;
	}

	public void setRequestAppVersion(Double requestAppVersion) {
		this.requestAppVersion = requestAppVersion;
	}*/

	public List<ProductInformation> getBooks()
	{
		return books;
	}

	public void setBooks(List<ProductInformation> books)
	{
		this.books = books;
	}

	public Integer getBookCount()
	{
		return bookCount;
	}

	public void setBookCount(Integer bookCount)
	{
		this.bookCount = bookCount;
	}

	public boolean isSortOnVolume() {
		return sortOnVolume;
	}

	public void setSortOnVolume(boolean sortOnVolume) {
		this.sortOnVolume = sortOnVolume;
	}

}

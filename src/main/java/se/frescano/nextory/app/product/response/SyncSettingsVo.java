package se.frescano.nextory.app.product.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SyncSettingsVo {

	@JsonProperty(value = "completed")
	private	int		completed;
	
	@JsonProperty( value = "currentpercentage", defaultValue = "")
	private String	currentpercentage;
	
	@JsonProperty( value = "position", defaultValue = "")
	private String	position;
	
	@JsonProperty( value = "syncdate", defaultValue = "")
	//@JsonSerialize( using = AppDateSerializer.class)
	private String	syncdate;
	
	@JsonProperty( value = "cfi", defaultValue = "")
	private String	cfi;
	
	@JsonProperty( value = "idref", defaultValue = "")
	private String	idref;
	
	
	public int getCompleted() {
		return completed;
	}
	public void setCompleted(int completed) {
		this.completed = completed;
	}
	public String getCurrentpercentage() {
		return currentpercentage;
	}
	public void setCurrentpercentage(String currentpercentage) {
		this.currentpercentage = currentpercentage;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getSyncdate() {
		return syncdate;
	}
	public void setSyncdate(String syncdate) {
		this.syncdate = syncdate;
	}
	public String getCfi() {
		return cfi;
	}
	public void setCfi(String cfi) {
		this.cfi = cfi;
	}
	public String getIdref() {
		return idref;
	}
	public void setIdref(String idref) {
		this.idref = idref;
	}
	
	
	
}

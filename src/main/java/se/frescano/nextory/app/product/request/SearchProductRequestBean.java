package se.frescano.nextory.app.product.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import se.frescano.nextory.app.request.APIRequestBean;

public class SearchProductRequestBean extends APIRequestBean{
	
	
	@NotNull(message="api.INPUT_MISSING")
	public String bookType;
	
	@NotNull(message="api.INPUT_MISSING")
	@Length(min=1,message="api.INPUT_MISSING")
	private String query ;
	
	

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	
	

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchProductRequestBean [getQuery()=");
		builder.append(getQuery());
		builder.append(", getAppId()=");
		builder.append(getAppId());
		builder.append(", getBookType()=");
		builder.append(getBookType());
		builder.append(", getVersion()=");
		builder.append(getVersion());
		builder.append(", getModel()=");
		builder.append(getModel());
		builder.append(", getClass()=");
		builder.append(getClass());
		builder.append(", hashCode()=");
		builder.append(hashCode());
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

	
	
}

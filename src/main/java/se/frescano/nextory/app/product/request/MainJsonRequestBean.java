package se.frescano.nextory.app.product.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import se.frescano.nextory.app.request.APITokenRequestBean;

@ApiModel(value = "bookgrouprequest", description = "Bookgroup request model")
public class MainJsonRequestBean extends APITokenRequestBean{

	/*@NotNull(message="api.INPUT_MISSING")
	@Length(min=1,message="api.INPUT_MISSING")*/
	@ApiModelProperty(value = "Valid bookgroupid. This is required in the cases other than view=main")
	private String bookgroupid ;

	@ApiModelProperty(value = "Valid bookid. This is required for related authors & narrators for a book")
	private Integer bookid;
	
	@ApiModelProperty(value = "Valid isbn. This is required for related authors & narrators for a isbn when slug name is isbn")
	private String isbn ;
	
	@ApiModelProperty(value = "Number of records required in the response ", required=true)
	private Integer pagesize;
	
	@ApiModelProperty(value = "Page number ", required=true)
	private String pagenumber;
	
	@ApiModelProperty(value = " view to identify the Bookgroup type", required=true, allowableValues="main, series, related, related2, level2")
	private String view;
	
	public String getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}

	public String getBookgroupid() {
		return bookgroupid;
	}

	public void setBookgroupid(String bookgroupid) {
		this.bookgroupid = bookgroupid;
	}

	public Integer getBookid() {
		return bookid;
	}

	public void setBookid(Integer bookid) {
		this.bookid = bookid;
	}

	public Integer getPagesize() {
		return pagesize;
	}

	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	

	
	
}

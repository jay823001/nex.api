package se.frescano.nextory.app.product.request;

import se.frescano.nextory.app.request.APITokenRequestBean;

public class BooksForBookGroupRequestBean extends APITokenRequestBean{

	/*@NotNull(message="api.INPUT_MISSING")
	@Length(min=1,message="api.INPUT_MISSING")*/
	private String bookgroupid ;
	
	private Integer rows;
	
	private String pagenumber;
	
	private String sort;
	
	private Integer type;
	
	private String authorname;
	
	
	public String getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}

	public String getBookgroupid() {
		return bookgroupid;
	}

	public void setBookgroupid(String bookgroupid) {
		this.bookgroupid = bookgroupid;
	}


	public Integer getPagesize() {
		return rows;
	}

	public void setPagesize(Integer pagesize) {
		this.rows = pagesize;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAuthorname() {
		return authorname;
	}

	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

	
	
}

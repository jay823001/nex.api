package se.frescano.nextory.app.product.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import se.frescano.nextory.app.request.APITokenRequestBean;

public class SearchJsonApptusRequestBean extends APITokenRequestBean{

	@NotNull(message="api.INPUT_MISSING")
	@Length(min=1,message="api.INPUT_MISSING")
	private String query ;
	
	//@Digits(integer=1,fraction=0,message="api.INPUT_MISSING")
	private String type;
	
	String sort;
	
	String pagesize;
	
	//@Digits(integer=3,fraction=0,message="api.INPUT_MISSING")
	String pagenumber ;
	
	String includenotallowedbooks;
	
	String language_facet;

	String subscriptionorder_facet;
	
	String avgrate_Min_facet;
	
	String avgrate_Max_facet;
	
	String categoryids_facet;
	
	String seriesAsit_facet;
	
	String authors_facet;
	
	String formattype_facet;

	public String getIncludenotallowedbooks() {
		return includenotallowedbooks;
	}

	public void setIncludenotallowedbooks(String includenotallowedbooks) {
		this.includenotallowedbooks = includenotallowedbooks;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getPagesize() {
		return pagesize;
	}

	public void setPagesize(String pagesize) {
		this.pagesize = pagesize;
	}

	public String getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}

	public String getLanguage_facet() {
		return language_facet;
	}

	public void setLanguage_facet(String language_facet) {
		this.language_facet = language_facet;
	}

	public String getSubscriptionorder_facet() {
		return subscriptionorder_facet;
	}

	public void setSubscriptionorder_facet(String subscriptionorder_facet) {
		this.subscriptionorder_facet = subscriptionorder_facet;
	}

	public String getAvgrate_Min_facet() {
		return avgrate_Min_facet;
	}

	public void setAvgrate_Min_facet(String avgrate_Min_facet) {
		this.avgrate_Min_facet = avgrate_Min_facet;
	}

	public String getAvgrate_Max_facet() {
		return avgrate_Max_facet;
	}

	public void setAvgrate_Max_facet(String avgrate_Max_facet) {
		this.avgrate_Max_facet = avgrate_Max_facet;
	}

	public String getCategoryids_facet() {
		return categoryids_facet;
	}

	public void setCategoryids_facet(String categoryids_facet) {
		this.categoryids_facet = categoryids_facet;
	}

	

	public String getSeriesAsit_facet() {
		return seriesAsit_facet;
	}

	public void setSeriesAsit_facet(String seriesAsit_facet) {
		this.seriesAsit_facet = seriesAsit_facet;
	}

	public String getAuthors_facet() {
		return authors_facet;
	}

	public void setAuthors_facet(String authors_facet) {
		this.authors_facet = authors_facet;
	}

	public String getFormattype_facet() {
		return formattype_facet;
	}

	public void setFormattype_facet(String formattype_facet) {
		this.formattype_facet = formattype_facet;
	}
	
	
}

package se.frescano.nextory.app.product.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import se.frescano.nextory.app.constants.CONSTANTS.API_TYPE;
import se.frescano.nextory.app.serializers.BookGroupSerializer;

@JsonSerialize(using = BookGroupSerializer.class)
public class BookGroupVOApptus implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4987239699505868187L;
	
	private String id;

	private String type;
		
	private Integer contenttype;
	
	private String title;
	
	private Integer haschild;
	
	private List<String> covers;
	
	private Integer position;
	
	private String parentid;

	private Integer allowsorting;
	
	private Integer showvolume;
	
	private String imageurl;
	
	private String imagelargeurl;
	
	private String description;
	
	private String label;
	
	private Boolean promotionalList;
	
	private String	viewby;
	
	private Boolean darktext;
	
	private String slugname;
	
	private API_TYPE serviceType;

	public Boolean getPromotionalList() {
		return promotionalList;
	}

	public void setPromotionalList(Boolean promotionalList) {
		this.promotionalList = promotionalList;
	}

	public String getImagelargeurl() {
		return imagelargeurl;
	}

	public void setImagelargeurl(String imagelargeurl) {
		this.imagelargeurl = imagelargeurl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getAllowsorting() {
		return allowsorting;
	}

	public void setAllowsorting(Integer allowsorting) {
		this.allowsorting = allowsorting;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getContenttype() {
		return contenttype;
	}

	public void setContenttype(Integer contenttype) {
		this.contenttype = contenttype;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getHaschild() {
		return haschild;
	}

	public void setHaschild(Integer haschild) {
		this.haschild = haschild;
	}

	public List<String> getCovers() {
		return covers;
	}

	public void setCovers(List<String> covers) {
		this.covers = covers;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getShowvolume() {
		return showvolume;
	}

	public void setShowvolume(Integer showvolume) {
		this.showvolume = showvolume;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getViewby() {
		return viewby;
	}

	public void setViewby(String viewby) {
		this.viewby = viewby;
	}

	public Boolean getDarktext() {
		return darktext;
	}

	public void setDarktext(Boolean darktext) {
		this.darktext = darktext;
	}

	public String getSlugname() {
		return slugname;
	}

	public void setSlugname(String slugname) {
		this.slugname = slugname;
	}

	public API_TYPE getServiceType() {
		return serviceType;
	}

	public void setServiceType(API_TYPE serviceType) {
		this.serviceType = serviceType;
	}

	
}


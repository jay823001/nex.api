package se.frescano.nextory.app.product.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.request.APIWrapperBean;
import se.frescano.nextory.app.response.MinifiedProdInfoVO;

//@JsonSerialize(using=BookDetailsApptusJsonConvertor.class)
@JsonInclude(Include.NON_NULL)
public class BookDetailsApptusJsonWrapper extends APIWrapperBean{

	@JsonProperty(value = "books")
	private MinifiedProdInfoVO book = null;
	
	@JsonIgnore
	private boolean andriod ;

	public MinifiedProdInfoVO getBook() {
		return book;
	}

	public void setBook(MinifiedProdInfoVO book) {
		this.book = book;
	}

	public boolean isAndriod() {
		return andriod;
	}

	public void setAndriod(boolean andriod) {
		this.andriod = andriod;
	}
	
	
}

package se.frescano.nextory.app.product.request;

import javax.validation.constraints.NotNull;

import se.frescano.nextory.app.request.APITokenRequestBean;

public class BookDetailsRequestBean extends APITokenRequestBean{

	@NotNull(message="api.INPUT_MISSING")	
	String id;
	
	String esalesticket;
	
//	@NotNull(message="api.INPUT_MISSING")
	//@Max(5)
	Integer rating;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEsalesticket() {
		return esalesticket;
	}

	public void setEsalesticket(String esalesticket) {
		this.esalesticket = esalesticket;
	}
	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}	
	
}

package se.frescano.nextory.app.product.response;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.constants.ProductFormatEnum;

@JsonInclude(Include.NON_NULL)
@JsonFilter("productdetails_filter")
public class ProductInformation
{			
		
	/*App response feilds*/
	@JsonProperty( value = "id")
	private int productid;	
	
	@JsonProperty( value = "title")
	private String title;
	
	@JsonProperty( value = "type")
	private int	format	= 0;	
	
	@JsonProperty( value = "imageurl")
	private String imageUrl;
	
	@JsonProperty( value = "weburl")
	private String weburl;
	
	@JsonProperty( value = "descriptionbrief")
	private String description	= "";
	
	@JsonIgnore
	private Integer relProductid;
	
	@JsonProperty( value = "numberinseries")
	private Integer numberinseries;
		
	@JsonProperty( value = "pubdate")
	private String pubdate;
	
	@JsonProperty( value = "authors")
	private Set<String> r_authors;
	
	@JsonProperty(value = "availableinlib")
	private Integer availableinlib;
	
	@JsonProperty(value = "libid")
	private Integer libid;
	
	@JsonProperty(value = "allowedinlibrary")	
	private byte 	allowedinlibrary;
	
	@JsonProperty(value = "relatedbookallowedinlibrary")
	private byte relatedbookallowedinlibrary;
	
	@JsonProperty(value = "coverratio")
	private Double coverratio;
	
	@JsonProperty( value = "esalesticket")
	private String ticket;
	
	@JsonProperty( value = "rank")
	private Integer rank	= 0;
	
	@JsonProperty( value = "position")
	private Integer position;
	
	@JsonProperty( value = "numberofrates")
	private Integer numberofrates;
	
	@JsonProperty( value = "avgrate")
	private Double avgrate;
	
	@JsonProperty( value = "numberofdays")
	private Long numberofdays;
	
	@JsonIgnore
	private ProductFormatEnum productFormat;
	
	@JsonIgnore
	private Set<String> authors;
	
	@JsonProperty(value = "isbn")
	private String isbn;
	
	@JsonIgnore
	private String coverUrl;
	
	@JsonIgnore
	private String author = "";
	
	@JsonIgnore
	private ProductFormatEnum relproducttype;	
		
	@JsonIgnore
	private Integer booktypeid;
	@JsonIgnore
	private Integer publishedYear;
	@JsonIgnore
	private String language;
	@JsonIgnore
	private String publishername;
	@JsonIgnore
	private Date firstpublished;
	@JsonIgnore
	private Integer relbooktypeid;
	@JsonIgnore	
	private String productURL;
	@JsonIgnore
	private String relevance;
	@JsonIgnore
	private String series;
	
	public byte getRelatedbookallowedinlibrary() {
		return relatedbookallowedinlibrary;
	}
	public void setRelatedbookallowedinlibrary(byte relatedbookallowedinlibrary) {
		this.relatedbookallowedinlibrary = relatedbookallowedinlibrary;
	}
	public Set<String> getR_authors() {
		return r_authors;
	}
	public void setR_authors(Set<String> r_authors) {
		this.r_authors = r_authors;
	}
	public String getWeburl() {
		return weburl;
	}
	public void setWeburl(String weburl) {
		this.weburl = weburl;
	}
	
	public Double getAvgrate() {
		return avgrate;
	}
	public void setAvgrate(Double avgrate) {
		this.avgrate = avgrate;
	}
	public Integer getNumberofrates() {
		return numberofrates;
	}
	public void setNumberofrates(Integer numberofrates) {
		this.numberofrates = numberofrates;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getNumberinseries() {
		return numberinseries;
	}
	public void setNumberinseries(Integer numberinseries) {
		this.numberinseries = numberinseries;
	}

	public Integer getPublishedYear()
	{
		return publishedYear;
	}

	public void setPublishedYear(Integer publishedYear)
	{
		this.publishedYear = publishedYear;
	}

	public ProductInformation(int count)
	{
	}

	public ProductInformation()
	{
		super();
	}

	public String getPubdate()
	{
		return pubdate;
	}

	public Integer getAvailableinlib()
	{
		return availableinlib;
	}

	public Integer getLibid()
	{
		return libid;
	}

	

	public String getDescription()
	{
		return description;
	}

	public void setPubdate(String pubdate)
	{
		this.pubdate = pubdate;
	}
/*
	public void setPopularity(PopularityMap<String, Integer> popularity)
	{
		this.popularity = popularity;
	}*/

	public void setAvailableinlib(Integer availableinlib)
	{
		this.availableinlib = availableinlib;
	}

	public void setLibid(Integer libid)
	{
		this.libid = libid;
	}
	

	public void setDescription(String description)
	{
		this.description = description;
	}

	/*public int getProvider_productid()
	{
		return provider_productid;
	}

	public void setProvider_productid(int provider_productid)
	{
		this.provider_productid = provider_productid;
	}*/

	public String getCoverUrl()
	{
		return coverUrl;
	}

	public void setCoverUrl(String coverUrl)
	{
		this.coverUrl = coverUrl;
	}

	@JsonIgnore
	public Integer getRelProductidApp()
	{
		return relProductid;
	}
	
	@JsonProperty( value = "relatedbookid")
	public Object getRelProductid()
	{
		if(this.relProductid!=null && this.relProductid>0)
		return relProductid;
		else
			return "";
	}

	public void setRelProductid(Integer relProductid)
	{
		this.relProductid = relProductid;
	}

	public int getProductid()
	{
		return productid;
	}

	public void setProductid(int productid)
	{
		this.productid = productid;
	}

	public String getTitle()
	{
		return title;
	}

	
	public void setTitle(String title)
	{
		this.title = title;
	}

	public ProductFormatEnum getProductFormat()
	{
		return productFormat;
	}

	public void setProductFormat(ProductFormatEnum productFormat)
	{
		this.productFormat = productFormat;
		if( productFormat != null )
			this.format	= productFormat.getFormatNo();
	}

	public Set<String> getAuthors()
	{
		return authors;
	}

	public void setAuthors(Set<String> authors)
	{
		this.authors = authors;
	}

	public String getAuthor()
	{
		if (authors == null || authors.isEmpty())
			return "";
		Iterator<String> iterator = authors.iterator();
		return iterator.next();
	}

	public void addAuthor(List<String> author)
	{
		if (this.authors == null)
			authors = new HashSet<String>();
		authors.addAll(author);

	}

	public void addAuthor(String author)
	{
		if (this.authors == null)
			authors = new HashSet<String>();
		authors.add(author);
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(String isbn)
	{
		this.isbn = isbn;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public void appendAuthor(String author)
	{
		this.author += StringUtils.isNotBlank(this.author) ? ", " + author : author;
	}

	/*public Integer getBookid()
	{
		return bookid;
	}

	public void setBookid(Integer bookid)
	{
		this.bookid = bookid;
	}*/

	public Integer getBooktypeid()
	{
		return booktypeid;
	}

	public void setBooktypeid(Integer booktypeid)
	{
		this.booktypeid = booktypeid;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	public String getPublishername()
	{
		return publishername;
	}

	public void setPublishername(String publishername)
	{
		this.publishername = publishername;
	}

	public Date getFirstpublished()
	{
		return firstpublished;
	}

	public void setFirstpublished(Date firstpublished)
	{
		this.firstpublished = firstpublished;
	}

	public Integer getRelbooktypeid()
	{
		return relbooktypeid;
	}

	public void setRelbooktypeid(Integer relbooktypeid)
	{
		this.relbooktypeid = relbooktypeid;
	}

	public ProductFormatEnum getRelproducttype()
	{
		return relproducttype;
	}

	public void setRelproducttype(ProductFormatEnum relproducttype)
	{
		this.relproducttype = relproducttype;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public String getProductURL()
	{
		return productURL;
	}

	public void setProductURL(String productURL)
	{
		this.productURL = productURL;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getRelevance() {
		return relevance;
	}

	public void setRelevance(String relevance) {
		this.relevance = relevance;
	}
		
	public Double getCoverratio() {
		return coverratio;
	}
	public void setCoverratio(Double coverratio) {
		this.coverratio = coverratio;
	}
	
	public int getFormat() {
		return format;
	}
	
	public byte getAllowedinlibrary() {
		return allowedinlibrary;
	}
	public void setAllowedinlibrary(byte allowedinlibrary) {
		this.allowedinlibrary = allowedinlibrary;
	}
	public Long getNumberofdays() {
		return numberofdays;
	}
	public void setNumberofdays(Long numberofdays) {
		this.numberofdays = numberofdays;
	}
	
	@Override
	public String toString() {
		return "ProductInformation [productid=" + productid + ", title="
				+ title + ", productFormat=" + productFormat + ", authors=" + authors + ", isbn=" + isbn
				+ ", relProductid=" + relProductid + ", coverUrl=" + coverUrl + ", author=" + author
				+ ", relproducttype=" + relproducttype + ", pubdate=" + pubdate + "," + ", availableinlib=" + availableinlib + ", libid=" + libid + ", allowedinlibrary=" + allowedinlibrary
				+ ", description=" + description + ", booktypeid=" + booktypeid + ", publishedYear=" + publishedYear
				+ ", language=" + language + ", publishername=" + publishername + ", firstpublished=" + firstpublished
				+ ", relbooktypeid=" + relbooktypeid + ", imageUrl=" + imageUrl + ", productURL=" + productURL
				+ ", ticket=" + ticket + ", rank=" + rank + ", relevance=" + relevance + ", numberinseries="
				+ numberinseries + ", position=" + position + "]";
	}

	
	
}

package se.frescano.nextory.app.product.response;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.request.APIWrapperBean;

//@JsonSerialize(using =AutoCompleteConvertor.class)
public class AutoCompleteWrapper extends APIWrapperBean
{

	@JsonProperty( value = "terms")
	ArrayList<String> terms;

	public ArrayList<String> getTerms() {
		return terms;
	}

	public void setTerms(ArrayList<String> terms) {
		this.terms = terms;
	}
		
}

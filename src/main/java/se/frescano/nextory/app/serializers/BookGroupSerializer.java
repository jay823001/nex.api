package se.frescano.nextory.app.serializers;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import se.frescano.nextory.app.constants.ApptusBookGroupsEnum;
import se.frescano.nextory.app.constants.CONSTANTS.API_TYPE;
import se.frescano.nextory.app.product.response.BookGroupVOApptus;
import se.frescano.nextory.startup.service.StartUpBean;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.BookGroupViewEnum;

public class BookGroupSerializer extends JsonSerializer<BookGroupVOApptus>{

	private 	Logger	logger	= LoggerFactory.getLogger( BookGroupSerializer.class);
	@Override
	public void serialize(BookGroupVOApptus entry, JsonGenerator generator,
			SerializerProvider s_provider) throws IOException {
		if( StringUtils.equals( entry.getViewby(), BookGroupViewEnum.SERIES.getView() ) )
			writeSeriesBookGroupJSON( entry, generator );	
		else if( StringUtils.equals( entry.getViewby(), BookGroupViewEnum.MAIN.getView() ) )
			writeLevel1Categories( entry, generator );
		else if( StringUtils.equals( entry.getViewby(), BookGroupViewEnum.LEVEL2.getView() ) )
			writeLevel2Categories( entry, generator );	
		else if( StringUtils.equals( entry.getViewby(), BookGroupViewEnum.RELATED_BOOK.getView() ) )
			writeRelatedBookDetails( entry, generator );
		else if( StringUtils.equals( entry.getViewby(), BookGroupViewEnum.RELATED_BOOK_2.getView() ) )
			writeRelatedBookDetails( entry, generator );
			
	}
	
	public void writeSeriesBookGroupJSON(BookGroupVOApptus entry, JsonGenerator generator){
		try{
			generator.writeStartObject();			
			generator.writeStringField("id",entry.getId());
			generator.writeStringField("type",entry.getType());
			generator.writeStringField("title",Book2GoUtil.getStrippedDescription(entry.getTitle()));
			generator.writeNumberField("haschild", entry.getHaschild());						
			
			List<String> covers = entry.getCovers();
	        generator.writeFieldName("covers");
	        generator.writeStartArray();
			if(covers!=null && covers.size() >0) {
				for (Iterator<String> iterator2 = covers.iterator(); iterator2.hasNext();) {
					generator.writeString(StartUpBean.imageResize(String.valueOf(iterator2.next())+ ".jpg", 0, 130, 100));
				}
			}
			generator.writeEndArray();
			
			generator.writeNumberField("position", entry.getPosition());			
			generator.writeNumberField("allowsorting", entry.getAllowsorting());			
			if(entry.getShowvolume()!=null)
				generator.writeNumberField("showvolume", entry.getShowvolume());	
			
			if(entry.getServiceType()!=null && entry.getServiceType() == API_TYPE.WEB && entry.getSlugname()!=null)
				generator.writeStringField("slugname",entry.getSlugname());
			
			generator.writeEndObject();
		}catch(Exception e){
			logger.error("An error occured while serializing bookgroup for --> " + entry.getTitle());
		}		
	}
	
	public void writeLevel1Categories(BookGroupVOApptus entry, JsonGenerator generator){
		try{
			generator.writeStartObject();						
			generator.writeStringField("id",entry.getId());
			generator.writeStringField("type",entry.getType());
			generator.writeStringField("title",Book2GoUtil.getStrippedDescription(entry.getTitle()));
			generator.writeNumberField("haschild", entry.getHaschild());
			generator.writeNumberField("position", entry.getPosition());
			generator.writeNumberField("allowsorting", entry.getAllowsorting());
			
			if(entry.getPromotionalList())
			{
				if(!StringUtils.isBlank( entry.getImageurl() ))
				generator.writeStringField("imageurl",entry.getImageurl());
			
				if(!StringUtils.isBlank( entry.getImagelargeurl() ))
					generator.writeStringField("imagelargeurl",entry.getImagelargeurl());
				
				if( !StringUtils.isBlank( entry.getDescription() ) )
				generator.writeStringField("description",entry.getDescription());
				
				//Note Below else block is written because of Hard Coding Issue in App
				if( !StringUtils.isBlank( entry.getLabel() ) )
					generator.writeStringField("label",entry.getLabel());
				else 
					generator.writeStringField("label","");
				
				if(entry.getType().equalsIgnoreCase(ApptusBookGroupsEnum.Promotion_LIST_TYPE.getGroupid()))
					generator.writeNumberField("darktext", entry.getDarktext()?1:0);
					
			}
			if(entry.getServiceType()!=null && entry.getServiceType() == API_TYPE.WEB && entry.getSlugname()!=null)
				generator.writeStringField("slugname",entry.getSlugname());
				
			generator.writeEndObject();
		}catch(Exception e){
			logger.error("An error occured while serializing bookgroup for --> " + entry.getTitle());
		}		
	}
	
	public void writeLevel2Categories(BookGroupVOApptus entry, JsonGenerator generator){
		try{
			generator.writeStartObject();			
			generator.writeStringField("parentid",entry.getParentid());
			generator.writeStringField("id",entry.getId());
			generator.writeStringField("type",entry.getType());
			generator.writeStringField("title",Book2GoUtil.getStrippedDescription(entry.getTitle()));
			generator.writeNumberField("haschild", entry.getHaschild());
			generator.writeNumberField("position", entry.getPosition());
			generator.writeNumberField("allowsorting", entry.getAllowsorting());
			
			if(entry.getServiceType()!=null && entry.getServiceType() == API_TYPE.WEB && entry.getSlugname()!=null)
				generator.writeStringField("slugname",entry.getSlugname());
			
			generator.writeEndObject();
		}catch(Exception e){
			logger.error("An error occured while serializing bookgroup for --> " + entry.getTitle());
		}		
	}

	public void writeRelatedBookDetails(BookGroupVOApptus entry, JsonGenerator generator){
		try{
			generator.writeStartObject();						
			generator.writeStringField("id",entry.getId());
			generator.writeStringField("type",entry.getType());
			generator.writeStringField("title",Book2GoUtil.getStrippedDescription(entry.getTitle()));
			generator.writeNumberField("haschild", entry.getHaschild());
			generator.writeNumberField("position", entry.getPosition());
			generator.writeNumberField("allowsorting", entry.getAllowsorting());
			
			if(entry.getShowvolume()!=null && entry.getShowvolume()>0)
				generator.writeNumberField("showvolume", entry.getShowvolume());	
			
			if(entry.getServiceType()!=null && entry.getServiceType() == API_TYPE.WEB && entry.getSlugname()!=null)
				generator.writeStringField("slugname",entry.getSlugname());
			
			generator.writeEndObject();
		}catch(Exception e){
			logger.error("An error occured while serializing bookgroup for --> " + entry.getTitle());
		}		
	}
}

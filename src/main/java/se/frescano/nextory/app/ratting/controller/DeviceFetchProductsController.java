package se.frescano.nextory.app.ratting.controller;

import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;

import se.frescano.nextory.app.api.spring.method.resolver.AppRequestBeanInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserToken;
import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.exception.TokenNotValidException;
import se.frescano.nextory.app.product.request.BookDetailsRequestBean;
import se.frescano.nextory.app.product.response.ReviewAndRatingsJsonWrapper;
import se.frescano.nextory.app.request.APIController;
import se.frescano.nextory.app.request.APITokenRequestBean;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.JsonWrapper;
import se.frescano.nextory.product.dao.ProductDao;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.InMemoryCache;

@RestController
public class DeviceFetchProductsController extends APIController implements ServletContextAware
{

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private Validator validator;

	Logger logger = LoggerFactory.getLogger(DeviceFetchProductsController.class);

	public void setServletContext(ServletContext servletContext)
	{
		InMemoryCache.contextPath = servletContext.getContextPath();
	}
	
	// Review and Ratings
	@RequestMapping(value =	{ APIJSONURLCONSTANTS.BOOK_RATINGS}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public JsonWrapper getBookRatings(@AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,HttpServletRequest request,
										BookDetailsRequestBean requestBean,	BindingResult result,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
		setDefaultParams(requestBean, apiVersion, authtoken, user,tokenbean);
		ReviewAndRatingsJsonWrapper wrapper = null;
		if (logger.isTraceEnabled())
			logger.trace(" DeviceFetchProductsController  :: getProductRatingDetails :: start :: requestBean ::: " + requestBean);			
		try
		{ 		
			Set<ConstraintViolation<BookDetailsRequestBean>> violations = validator.validate(requestBean);
		    if (violations != null && violations.size() > 0)
		    {
		        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, requestBean.getLocale());
		    }
			Integer book_Id = Book2GoUtil.parseString(requestBean.getId());
			Integer rating = requestBean.getRating();			
			if (rating == null || rating > 5 )
			{
				if (logger.isTraceEnabled()) logger.trace(" DeviceFetchProductsController  getProductDetails , validation failed "+book_Id + " - "+rating);
				return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING,apiVersion,requestBean.getLocale());
			}
			productDao.setProductRating(book_Id, requestBean, user);			
			wrapper = productDao.getBookRateDetails(book_Id, requestBean, user);
			if (wrapper != null && wrapper.getRating() != null)
			{
				wrapper.setStatus(ErrorCodes.OK.getMessage());
				wrapper.setAndriod("200".equals(requestBean.getAppId()));
				if (logger.isTraceEnabled())
					logger.trace( ": DeviceFetchProductsController  :: getProductDetails1 :: end :: wrapperBean ::: "
							+ wrapper);
				//return new APIJsonWrapper(wrapper,apiVersion);
				return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
						?new APIJsonWrapper(wrapper,apiVersion,user.getWarning()):new APIJsonWrapper(wrapper,apiVersion));
			} 
		} catch (TokenNotValidException e)
		{
			if( logger.isTraceEnabled() )
				logger.trace( "User with token (" + requestBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,requestBean.getLocale());
		} catch (APIException e)
		{
			logger.error("@@@@@@@@@@@@@@@@@@@@@@@ DeviceLibraryController -> Bannner Details   ", e);
			return handleErrorPageAsBean(
			e.getErrorCodes() != null ? e.getErrorCodes() : ErrorCodes.AUTHENTICATION_ERROR,apiVersion,requestBean.getLocale());
		}
		return null;
	}
			
}
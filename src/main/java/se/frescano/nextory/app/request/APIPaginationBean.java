package se.frescano.nextory.app.request;

import javax.validation.constraints.Digits;

public class APIPaginationBean extends APITokenRequestBean{

	@Digits(integer=10,fraction=0,message="api.INPUT_MISSING")
	String pagenumber;
	
	String sort;
	
	@Digits(integer=2,fraction=0,message="api.INPUT_MISSING")
	String rows = "12";

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
		
}

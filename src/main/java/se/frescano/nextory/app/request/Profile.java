package se.frescano.nextory.app.request;

import java.io.Serializable;
import java.util.Date;

public class Profile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6721346676299176741L;
	
	private Integer profileid;
	private Integer parentid;
	private Boolean isparent;
	private String profilename;
	private Integer colorindex;
	private String category;
	
	//@JsonSerialize(using = AppDateSerializer.class)
	//@JsonDeserialize(using = AppDateDeSerializer.class)
	private Date createddate;
	private long createddate_long;
	private String loginkey;
	private String status;
	
	public Profile() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Profile(Integer customerid, String profilename, Integer colorindex, String category, Boolean isParent) {
		// TODO Auto-generated constructor stub
		this.parentid = customerid;
		this.profilename = profilename;
		this.colorindex = colorindex;
		this.category = category;
		this.isparent = isParent;
		
	}
	public Integer getProfileid() {
		return profileid;
	}
	public void setProfileid(Integer profileid) {
		this.profileid = profileid;
	}
	public Integer getParentid() {
		return parentid;
	}
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
	public Boolean getIsparent() {
		return isparent;
	}
	public void setIsparent(Boolean isparent) {
		this.isparent = isparent;
	}
	public String getProfilename() {
		return profilename;
	}
	public void setProfilename(String profilename) {
		this.profilename = profilename;
	}
 
	public String getCategory() {
		if(category!=null)
			return category.toLowerCase();
		else
			return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public String getLoginkey() {
		return loginkey;
	}
	public void setLoginkey(String loginkey) {
		this.loginkey = loginkey;
	}
	public String getStatus() {
		if(status!=null)
			return status.toLowerCase();
		else
			return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Profile [profileid=" + profileid + ", parentid=" + parentid + ", isparent=" + isparent
				+ ", profilename=" + profilename + ", colorindex=" + colorindex + ", category=" + category
				+ ", createddate=" + createddate + ", loginkey=" + loginkey + ", status=" + status + "]";
	}

	public Integer getColorindex() {
		return colorindex;
	}

	public void setColorindex(Integer colorindex) {
		this.colorindex = colorindex;
	}

	public long getCreateddate_long() {
		return createddate_long;
	}

	public void setCreateddate_long(long createddate_long) {
		this.createddate_long = createddate_long;
		if( createddate_long > 0 )
			this.createddate = new Date( createddate_long );
		
	}

}

package se.frescano.nextory.app.request;

import java.util.Locale;

import javax.validation.MessageInterpolator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

public class SpringMessageSourceMessageInterpolator implements
		MessageInterpolator, MessageSourceAware, InitializingBean {

	private MessageSource messageSource;

	public String interpolate(String messageTemplate, Context context) {
		if(StringUtils.startsWith(messageTemplate, "api."))
			return StringUtils.removeStart(messageTemplate, "api.");
		return messageSource.getMessage(messageTemplate, new Object[] {},Locale.getDefault());
	}

	public String interpolate(String messageTemplate, Context context,Locale locale) {
		if(StringUtils.startsWith(messageTemplate, "api."))
			return StringUtils.removeStart(messageTemplate, "api.");
		return messageSource.getMessage(messageTemplate, new Object[] {},locale);
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public void afterPropertiesSet() throws Exception {
		if (messageSource == null) {
			throw new IllegalStateException(
					"MessageSource was not injected, could not initialize "
							+ this.getClass().getSimpleName());
		}
	}

}

package se.frescano.nextory.app.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class APIWrapperBean {

//	@XStreamAlias("stat")
//	@XStreamAsAttribute
	@JsonIgnore
	String status;
	
	@JsonIgnore
	String locale;
	
	@JsonIgnore
	Double apiVersion;
	

	public Double getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(Double version) {
		this.apiVersion = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	
}

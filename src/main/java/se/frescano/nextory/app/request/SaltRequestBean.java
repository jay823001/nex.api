package se.frescano.nextory.app.request;


public class SaltRequestBean extends APIRequestBean{

	
	
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SaltRequestBean [getAppId()=");
		builder.append(getAppId());
		builder.append(", getVersion()=");
		builder.append(getVersion());
		builder.append(", getModel()=");
		builder.append(getModel());
		builder.append(", getClass()=");
		builder.append(getClass());
		builder.append(", hashCode()=");
		builder.append(hashCode());
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
}

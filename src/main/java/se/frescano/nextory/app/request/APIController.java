package se.frescano.nextory.app.request;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.error.handlers.AppErrorHandler;
import se.frescano.nextory.spring.method.model.User;
@RequestMapping(value =	{ APIJSONURLCONSTANTS.APP_API_V1_BASE, APIJSONURLCONSTANTS.APP_API_V2_BASE})
public class APIController implements AppErrorHandler{
	
	@Autowired
	LocalValidatorFactoryBean validator;
	//private static final Logger logger = LoggerFactory.getLogger(APIController.class);	 
	
	public static SimpleFilterProvider	json_booksofbookgroupfilter_exclude_gt_6_2 	= new SimpleFilterProvider().addFilter("productdetails_filter", 
			SimpleBeanPropertyFilter.serializeAllExcept("availableinlib","isbn"));
	public static SimpleFilterProvider	json_searchbooksfilter_exclude_gt_6_2 		= new SimpleFilterProvider().addFilter("productdetails_filter", 
			SimpleBeanPropertyFilter.serializeAllExcept("availableinlib","isbn"));
	public static SimpleFilterProvider	json_searchbooksfilter_exclude_facets_fields 		= 
			new SimpleFilterProvider().addFilter("productdetails_filter", SimpleBeanPropertyFilter.serializeAllExcept("availableinlib","isbn")
					).addFilter("facets_filter", SimpleBeanPropertyFilter.serializeAllExcept("name",
						    "ticket",
						    "path",
						    "description",
						    "displayName",
						    "attributes",
						    "resultType"))
			.addFilter("facetvalue_filter", SimpleBeanPropertyFilter.serializeAllExcept("ticket"));
	
	protected void setDefaultParams(APITokenRequestBean requestbean, Double apiVersion, UserAuthToken authtoken, User appuserinfo,
			APITokenRequestBean headersdata){
		requestbean.setApiVersion(apiVersion);
		if( authtoken != null && authtoken.getApiVersion() == null )authtoken.setApiVersion(apiVersion);
		requestbean.setToken( (authtoken != null)?authtoken.getUuid():null);
		requestbean.setAuthToken(authtoken);
		if( appuserinfo != null )
			appuserinfo.setAuthToken(authtoken);
		
		if(StringUtils.isBlank(requestbean.getAppId()))
		{
			requestbean.setAppId(headersdata.getAppId());
		}
		if(StringUtils.isBlank(requestbean.getDeviceid()))
		{
			requestbean.setDeviceid(headersdata.getDeviceid());
		}
		if(StringUtils.isBlank(requestbean.getLocale()))
		{
			requestbean.setLocale(headersdata.getLocale());
		}
		if(StringUtils.isBlank(requestbean.getModel()))
		{
			requestbean.setModel(headersdata.getModel());
		}
		if(StringUtils.isBlank(requestbean.getOsinfo()))
		{
			requestbean.setOsinfo(headersdata.getOsinfo());
		}
		if(StringUtils.isBlank(requestbean.getVersion()))
		{
			requestbean.setVersion(headersdata.getVersion());
		}
	}
	
	public void setHeaderParams(APIRequestBean requestbean,APITokenRequestBean headersdata)
	{
		if(StringUtils.isBlank(requestbean.getAppId()))
		{
			requestbean.setAppId(headersdata.getAppId());
		}
		if(StringUtils.isBlank(requestbean.getDeviceid()))
		{
			requestbean.setDeviceid(headersdata.getDeviceid());
		}
		if(StringUtils.isBlank(requestbean.getLocale()))
		{
			requestbean.setLocale(headersdata.getLocale());
		}
		if(StringUtils.isBlank(requestbean.getModel()))
		{
			requestbean.setModel(headersdata.getModel());
		}
		if(StringUtils.isBlank(requestbean.getOsinfo()))
		{
			requestbean.setOsinfo(headersdata.getOsinfo());
		}
		if(StringUtils.isBlank(requestbean.getVersion()))
		{
			requestbean.setVersion(headersdata.getVersion());
		}
	}
	protected ErrorCodes validate(Logger logger,APIRequestBean apiRequestBean,BindingResult result,HttpServletRequest request){
		//String url = request.getRequestURI();
		apiRequestBean.setVer2(true);
		ErrorCodes errorcodes =  checkMandatoryFieldsForVer2(apiRequestBean, logger);
		if(!errorcodes.equals(ErrorCodes.OK))
			return errorcodes;
		validator.validate(apiRequestBean, result);
		if(!result.hasErrors())
			return ErrorCodes.OK;
		else{
			FieldError erro = result.getFieldError();
			String errorCode = erro.getDefaultMessage();
			logger.warn(" field "+erro.getField() +" missing ");
			return ErrorCodes.valueOf(errorCode); 
		}
	}
		
	public  boolean valueCheck(String field,int maxLength){
		return valueCheck(field, 1,maxLength);
	}
	public  boolean valueCheck(String field,int minlength,int maxLength){
		if(field !=null && (field.trim().length() >=minlength && field.trim().length()<=maxLength))
			return true;
		else 
			return false;
	}
	public  boolean isMailIdValid(String field,int maxLen){
		String field1=field.trim();
		if(!valueCheck(field1,maxLen))
	    	  return false;
		  //Pattern p = Pattern.compile(".+@.+\\.[a-zA-Z]+");
		  //Pattern p = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		  Pattern p = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	      Matcher m = p.matcher(field1);
	      return m.matches();
	}
	
	
	protected ErrorCodes validateToken(APITokenRequestBean requestBean) {
		if(StringUtils.isBlank(requestBean.getToken()) )
			throw new APIException("Token required ", ErrorCodes.INPUT_MISSING);
		return ErrorCodes.OK;
	}
	private ErrorCodes checkMandatoryFieldsForVer2(APIRequestBean apiRequestBean,Logger logger){
		if(StringUtils.isBlank(apiRequestBean.getVersion()) || StringUtils.isBlank(apiRequestBean.getModel())){
			logger.warn("field App version info missing ");
			return ErrorCodes.INPUT_MISSING;
		}
		return ErrorCodes.OK;
	}
		 
	public Object getJsonMappingfilter(Object responsebean, String filetr_id, SimpleFilterProvider filter){		
		MappingJacksonValue mapping = new MappingJacksonValue(responsebean);
		if( filter != null )
			mapping.setFilters(filter);
		else
			mapping.setFilters(new SimpleFilterProvider().addFilter(filetr_id, SimpleBeanPropertyFilter.serializeAll()));			
		return mapping;
	}
}

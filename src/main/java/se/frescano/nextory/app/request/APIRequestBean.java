package se.frescano.nextory.app.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiParam;







public abstract class APIRequestBean {

	//float apiVersionInfo;
	@ApiParam(hidden = true)
	@NotNull(message="api.INPUT_MISSING")
	String version;
	@ApiParam(hidden = true)
	@NotNull(message="api.INPUT_MISSING")
	String model;
	@ApiParam(hidden = true)
	@NotNull(message="api.INPUT_MISSING")
	String deviceid;
	
	@ApiParam(hidden = true)
	@NotNull(message="api.INPUT_MISSING")
	@Digits(integer=3,fraction=0,message="api.INPUT_MISSING")
	String appId;
	@ApiParam(hidden = true)
	//@NotNull(message="api.INPUT_MISSING")
	@Ignore
	private String locale;
	
	
	@NotNull(message="api.INPUT_MISSING")
	@ApiParam(hidden = true)
	String osinfo;
	
	//@XStreamOmitField
	@Ignore
	@ApiParam(hidden = true)
	boolean ver2;
	
//	@Ignore
	//@NotNull(message="api.INPUT_MISSING")
	@ApiParam(hidden = true)
	Double apiVersion;
	
	/*public float getApiVersionInfo() {
		return apiVersionInfo;
	}

	public void setApiVersionInfo(float apiVersionInfo) {
		this.apiVersionInfo = apiVersionInfo;
	}*/
	/*@Digits(integer=2,fraction=0,message="api.INPUT_MISSING")
	String rows = "12";*/

	/*private String token;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}*/
	public String getVersion() {
		return version;
	}

	public void setVersion(String appver) {
		this.version = appver;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public boolean isVer2() {
		return ver2;
	}

	public void setVer2(boolean isVer2) {
		this.ver2 = isVer2;
	}
	/*public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}
*/	public String getAppId() {
		return appId;
	}
	public void setAppId(String app_id) {
		this.appId = app_id;
	}

	public String getDeviceid() {	
		 return deviceid;
		//return StringUtils.isBlank(deviceid)?model+"_"+appId:deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getOsinfo() {
		return osinfo;
	}

	public void setOsinfo(String osInfo) {
		this.osinfo = osInfo;
	}
	 
	
	public Double getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(Double apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
}

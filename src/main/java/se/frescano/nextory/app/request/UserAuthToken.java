package se.frescano.nextory.app.request;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiParam;
import se.frescano.nextory.app.constants.CONSTANTS.ACCOUNT_TYPE;
import se.frescano.nextory.app.constants.CONSTANTS.TOKEN_TYPE;
import se.frescano.nextory.util.Book2GoUtil;
import springfox.documentation.annotations.ApiIgnore;

@JsonInclude(JsonInclude.Include.NON_NULL) 
public class UserAuthToken implements Serializable{
	/**
	 * 
	 */
	//private static final Logger logger = LoggerFactory.getLogger(UserAuthToken.class);
	@ApiParam(hidden = true)
	private static final long serialVersionUID = 2950056215066551677L;
	@ApiParam(hidden = true)
	private String uuid; 
	@ApiParam(hidden = true)
	private TOKEN_TYPE type;
	@ApiParam(hidden = true)
	private String deviceid;
	@ApiParam(hidden = true)
	private Integer customerid;
	@ApiParam(hidden = true)
	private Integer profileid;
	@ApiParam(hidden = true)
	private Integer subscriptionid;
	
	@ApiParam(hidden = true)
	private long createdDate_long;
	@ApiParam(hidden = true)
	private Date createdDate;
	@ApiParam(hidden = true)
	private Double apiVersion;
	
	//@JsonDeserialize(using = AppDateDeSerializer.class)
	@ApiParam(hidden = true)
	private Date pwd_updatedDate;
	@ApiParam(hidden = true)
	private long		pwd_updatedDate_long;
	@ApiParam(hidden = true)
	private ACCOUNT_TYPE accountType;
	
	/*private TokenNotValidException errorCode;
	private String hashedPass;*/
	 
	public UserAuthToken() {  }
	
	/*public  UserAuthToken(String encodedToken) throws TokenNotValidException{
		 String[] tokenStr = encodedToken.split("\\"+CONSTANTS.DELIMITER);
		 if(tokenStr!=null && tokenStr.length>0){
			 try{
				 this.uuid = tokenStr[0];
				 this.customerid=Integer.parseInt(tokenStr[1]);
				 this.deviceid = tokenStr[2];
				 this.createdDate = new Date(Long.parseLong(tokenStr[3]));
				 
				 if(tokenStr.length>4 && tokenStr.length<6) //LT will have up to 5 info
					 this.subscriptionid = Integer.parseInt(tokenStr[4]);
				 else if(tokenStr.length>4){//PT
					this.profileid=Integer.parseInt(tokenStr[4]);
					this.subscriptionid=Integer.parseInt(tokenStr[5]);
					this.type = TOKEN_TYPE.valueOf(tokenStr[6]);
					//this.hashedPass=tokenStr[7];
				 }
				 
			 }catch (Exception e) {
				logger.debug("Failed Token info :: {}", Arrays.asList(tokenStr));
				logger.error("Token decoding failed :: ", e); 
				throw new TokenNotValidException("Token decoding failed",ErrorCodes.INVALID_TOKEN);
			} 
		 } 
	 }*/
	 /*public UserAuthToken(String uuid, String type, String deviceid, Integer customerid, Integer profileid,
				Integer subscriptionid, String hashPass) {
			super();
			this.uuid = uuid;
			this.type = TOKEN_TYPE.valueOf(type);
			this.deviceid = deviceid;
			this.customerid = customerid;
			this.profileid = profileid;
			this.subscriptionid = subscriptionid;
			this.createdDate=new Date();
			this.pwd_updatedDate=new Date();
			//if(hashPass!=null)this.hashedPass=hashPass.substring(hashPass.length()-5);
		}*/

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
 
	public TOKEN_TYPE getType() {
		return type;
	}

	public void setType(TOKEN_TYPE type) {
		this.type = type;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public Integer getSubscriptionid() {
		return subscriptionid;
	}

	public void setSubscriptionid(Integer subscriptionid) {
		this.subscriptionid = subscriptionid;
	}

	public Integer getProfileid() {
		return profileid;
	}

	public void setProfileid(Integer profileid) {
		this.profileid = profileid;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	 /*public String encodeToken(){
		 String delimit = CONSTANTS.DELIMITER;
		 return this.uuid+delimit+this.customerid+delimit+this.deviceid+delimit+
				 this.createdDate.getTime()+delimit+this.profileid+delimit+this.subscriptionid+delimit+this.type.name();
				 //+delimit+this.hashedPass;
	 }*/

	

	public Double getApiVersion() {
		return apiVersion;
	}

	@Override
	public String toString() {
		return "UserAuthToken [uuid=" + uuid + ", type=" + type + ", deviceid=" + deviceid + ", customerid="
				+ customerid + ", profileid=" + profileid + ", subscriptionid=" + subscriptionid + ", createdDate_long="
				+ createdDate_long + ", createdDate=" + createdDate + ", apiVersion=" + apiVersion
				+ ", pwd_updatedDate=" + pwd_updatedDate + ", pwd_updatedDate_long=" + pwd_updatedDate_long
				+ ", accountType=" + accountType + "]";
	}

	public void setApiVersion(Double apiVersion) {
		this.apiVersion = apiVersion;
	}

	public Date getPwd_updatedDate() {
		return pwd_updatedDate;
	}

	public void setPwd_updatedDate(Date pwd_updatedDate) {
		this.pwd_updatedDate = pwd_updatedDate;
	}

	public ACCOUNT_TYPE getAccountType() {
		return accountType;
	}

	public void setAccountType(ACCOUNT_TYPE accountType) {
		this.accountType = accountType;
	}

	public long getCreatedDate_long() {
		return createdDate_long;
	}

	public void setCreatedDate_long(long createdDate_long) {
		this.createdDate_long = createdDate_long;
		if( createdDate_long > 0 )
			this.createdDate = new Date(createdDate_long);
	}

	public long getPwd_updatedDate_long() {
		return pwd_updatedDate_long;
	}

	public void setPwd_updatedDate_long(long pwd_updatedDate_long) {
		this.pwd_updatedDate_long = pwd_updatedDate_long;
		if( pwd_updatedDate_long > 0 )
			this.pwd_updatedDate = new Date(pwd_updatedDate_long);
	}
 
	 
}

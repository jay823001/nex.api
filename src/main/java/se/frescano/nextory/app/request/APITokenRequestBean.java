package se.frescano.nextory.app.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
public class APITokenRequestBean extends APIRequestBean {
	
	@ApiParam(hidden = true)
	private String token;
	
	@Ignore
	@ApiParam(hidden = true)
	private Double apiVersion = 0.0;
	
	@Ignore
	private UserAuthToken authToken;
	
	
	@Ignore
	Logger logger = LoggerFactory.getLogger(APITokenRequestBean.class); 
	
	/*public Double getApiVersion() {
		return apiVersion;
	}
	public void setApiVersion(Double apiVersion) {
		this.apiVersion = apiVersion;
	}*/
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;

		/*if(deviceid!=null &&  authToken==null && !TokenService.isTokenUUID(token)){
			//decrypt & convert to object
			 authToken= TokenService.decryptToken(token, deviceid);
			 if(authToken!=null)this.token=authToken.getUuid();
		 }else{
			 authToken = new UserAuthToken();
			 authToken.setUuid(token);
		 }
		 if(authToken!=null)authToken.setApiVersion(apiVersion);*/
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("APITokenRequestBean [");
		if (token != null) {
			builder.append("token=");
			builder.append(token);
			builder.append(", ");
		}
		if (apiVersion != null) {
			builder.append("apiVersion=");
			builder.append(apiVersion);
			builder.append(", ");
		}
		if (version != null) {
			builder.append("version=");
			builder.append(version);
			builder.append(", ");
		}
		if (model != null) {
			builder.append("model=");
			builder.append(model);
			builder.append(", ");
		}
		if (deviceid != null) {
			builder.append("deviceid=");
			builder.append(deviceid);
			builder.append(", ");
		}
		if (appId != null) {
			builder.append("appId=");
			builder.append(appId);
			builder.append(", ");
		}
		if (osinfo != null) {
			builder.append("osinfo=");
			builder.append(osinfo);
			builder.append(", ");
		}
		builder.append("ver2=");
		builder.append(ver2);
		builder.append("]");
		return builder.toString();
	}
	public UserAuthToken getAuthToken() {
		return authToken;
	}
	 
	public void setAuthToken(UserAuthToken authToken) {
		this.authToken = authToken;
	}
	
}

package se.frescano.nextory.app.request;
//$Id: Ignore.java 
/*
* 
*/


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
* 
* Accepts any type.
*
* @author Sathish Yellanty
*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD) //can use in Field only.
public @interface Ignore {
	
	
	
}


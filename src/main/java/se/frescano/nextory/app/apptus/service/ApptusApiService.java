package se.frescano.nextory.app.apptus.service;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.apptus.esales.connector.ArgMap;
import com.apptus.esales.connector.CloudConnector;
import com.apptus.esales.connector.DynamicPage;
import com.apptus.esales.connector.MissingSubpanelException;
import com.apptus.esales.connector.PanelContent;
import com.apptus.esales.connector.Result.Product;
import com.apptus.esales.connector.Result.Variant;
import com.apptus.esales.connector.Subpanel;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.apptus.services.ApptusAutoCompleteForKidsService;
import se.frescano.nextory.app.apptus.services.ApptusAutoCompleteService;
import se.frescano.nextory.app.apptus.services.ApptusRelatedBookZoneService;
import se.frescano.nextory.app.apptus.services.AptusAdvancedSearchService;
import se.frescano.nextory.app.apptus.services.AptusOnlyFacetsService;
import se.frescano.nextory.app.apptus.services.AptusSearchFacetsService;
import se.frescano.nextory.app.apptus.services.AptusSearchService;
import se.frescano.nextory.app.apptus.services.DidyouMeanForKidsPanelService;
import se.frescano.nextory.app.apptus.services.DidyouMeanPanelService;
import se.frescano.nextory.app.apptus.services.RecommendBasedOnCustomerService;
import se.frescano.nextory.app.apptus.services.RecommendZoneBroughtService;
import se.frescano.nextory.app.apptus.services.RecommendZoneService;
import se.frescano.nextory.app.apptus.services.SearchHistoryService;
import se.frescano.nextory.app.apptus.services.TopSearchService;
import se.frescano.nextory.app.apptus.services.TopSellerService;
import se.frescano.nextory.app.apptus.services.UpCommingService;
import se.frescano.nextory.app.constants.ApptusBookGroupsEnum;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS.PROFILE_CATEGORIES;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.constants.TopPanelSubCatEnum;
import se.frescano.nextory.app.product.request.AutoCompleteJsonRequestBean;
import se.frescano.nextory.app.product.request.BooksForBookGroupRequestBean;
import se.frescano.nextory.app.product.request.MainJsonRequestBean;
import se.frescano.nextory.app.product.request.ReportClickApptusRequestBean;
import se.frescano.nextory.app.product.request.SearchJsonApptusRequestBean;
import se.frescano.nextory.app.product.response.AdvancedSearchWrapper;
import se.frescano.nextory.app.product.response.AutoCompleteSeries;
import se.frescano.nextory.app.product.response.AutoCompleteWrapper;
import se.frescano.nextory.app.product.response.BookGroupVOApptus;
import se.frescano.nextory.app.product.response.BooksForBookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.app.product.response.SearchWithFacetsApptusResponseWrapper;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.BookGroupsApptusResponseWrapper;
import se.frescano.nextory.apptus.model.AptusSecond;
import se.frescano.nextory.apptus.model.AptusSecondSilverSearch;
import se.frescano.nextory.apptus.model.AptusSecondSilverSearchWithFacets;
import se.frescano.nextory.apptus.model.AptusThird;
import se.frescano.nextory.apptus.model.AttributesAptus;
import se.frescano.nextory.apptus.model.AttributesProductAptus;
import se.frescano.nextory.apptus.model.Completions;
import se.frescano.nextory.apptus.model.Phrases;
import se.frescano.nextory.apptus.model.ProductsAptus;
import se.frescano.nextory.apptus.model.VariantsAptus;
import se.frescano.nextory.apptus.model.AdvancedSearchVo.AdvancedSearch;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse2;
import se.frescano.nextory.apptus.model.ApptusFetchVo.categoryList;
import se.frescano.nextory.apptus.model.Facets.Facet;
import se.frescano.nextory.apptus.model.Facets.FacetList;
import se.frescano.nextory.apptus.model.Facets.Value;
import se.frescano.nextory.apptus.request.AdvancedSearchZone;
import se.frescano.nextory.apptus.request.AutoCompleteForKidsPanel;
import se.frescano.nextory.apptus.request.AutoCompletePanel;
import se.frescano.nextory.apptus.request.DidYouMeanForKidsPanel;
import se.frescano.nextory.apptus.request.DidYouMeanPanel;
import se.frescano.nextory.apptus.request.GenericPanel;
import se.frescano.nextory.apptus.request.GenericPanelAppPreviewRequest;
import se.frescano.nextory.apptus.request.RecommendBasedOnCustomer;
import se.frescano.nextory.apptus.request.RecommendZone;
import se.frescano.nextory.apptus.request.RelatedBookZonePanel;
import se.frescano.nextory.apptus.request.SearchHistoryPanel;
import se.frescano.nextory.apptus.request.SearchPanel;
import se.frescano.nextory.apptus.request.TopSearchPanel;
import se.frescano.nextory.apptus.request.TopSellers;
import se.frescano.nextory.apptus.request.UpComming;
import se.frescano.nextory.apptus.service.ApptusNotificationApiCall;
import se.frescano.nextory.apptus.service.ApptusServiceHelper;
import se.frescano.nextory.apptus.utils.FilterUtils;
import se.frescano.nextory.blueshift.config.BlueShiftConfiguration;
import se.frescano.nextory.cache.model.Series;
import se.frescano.nextory.cache.model.SeriesList;
import se.frescano.nextory.cache.service.CacheBridgeCategory;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.cache.service.CacheBridgeSeries;
import se.frescano.nextory.cache.service.CacheBridgeTopPanels;
import se.frescano.nextory.cache.service.HazelCastDependentService;
import se.frescano.nextory.cache.service.NextoryHCacheService;
import se.frescano.nextory.customer.BookIdData;
import se.frescano.nextory.dao.MongoDBUtilMorphia;
import se.frescano.nextory.listener.Publisher;
import se.frescano.nextory.model.EbCategories;
import se.frescano.nextory.model.ProductDetails;
import se.frescano.nextory.model.ProductRelatedDetails;
import se.frescano.nextory.model.SearchRequest;
import se.frescano.nextory.model.SearchResult;
import se.frescano.nextory.model.SearchResultWithFacets;
import se.frescano.nextory.mongo.product.model.ProductData;
import se.frescano.nextory.notification.NotificationEnum;
import se.frescano.nextory.notification.NotificationEvent;
import se.frescano.nextory.notification.request.BlueShiftRequestBean;
import se.frescano.nextory.product.dao.ProductDao;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.toppanellist.TopPanelList;
import se.frescano.nextory.toppanellist.TopPanelVO;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.BookGroupViewEnum;
import se.frescano.nextory.util.CustomComparator;
import se.frescano.nextory.util.CustomComparator2;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.web.api.response.ProductListWithCount;

@Service
public class ApptusApiService {

	@Autowired
	private ApplicationPropertiesConfig applicationpropconfig;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	MongoDBUtilMorphia mongoDBUtilMorphia;
		
	@Autowired
	ApptusServiceHelper apptusServiceHelper;
	
	@Autowired
	ApptusNotificationApiCall notificationForApptus;
		
	@Autowired
	CacheBridgeCategory cacheBridge1;
	
	@Autowired
	CacheBridgeSeries cacheBridgeSeries;
	
	@Autowired
	CacheBridgeTopPanels cacheBridgeTopPanels;
	
	@Autowired
	HazelCastDependentService	hazelCastDependentService;
	
	@Autowired
	CacheBridgeMarket cacheBridgeMarket;	
	
	@Autowired
	private	AptusSearchService	aptussearchservice;
	
	@Autowired
	private	AptusSearchFacetsService	aptussearchfacetsservice;
	
	@Autowired
	private AptusOnlyFacetsService aptusOnlyFacetsService;
	
	@Autowired
	private AptusAdvancedSearchService aptusAdvancedSearchService;
	
	@Autowired
	private RecommendZoneBroughtService	aptusrecommendzonebrought;
	
	@Autowired
	private RecommendBasedOnCustomerService	aptusrecommendbasedoncustomer;
	
	@Autowired
	private TopSellerService topSellerService;
	
	@Autowired
	private DidyouMeanPanelService			aptusdidyoumeanservice;
	
	@Autowired
	private DidyouMeanForKidsPanelService	aptusdidyoumeanforkidsservice;
	
	@Autowired
	private ApptusAutoCompleteForKidsService autocompleteforkidsservice;
	
	@Autowired
	private ApptusAutoCompleteService		 autocompleteservice;
	
	@Autowired
	private SearchHistoryService searchHistoryService;
	
	@Autowired
	private ApptusRelatedBookZoneService	releatedbookzoneservice;
		
	@Autowired
	private BlueShiftConfiguration 		 blueShiftConfiguration;
	
	@Autowired
	private UpCommingService upCommingService;
	
	@Autowired
	private Publisher	publisher;
	
	@Autowired
	private NextoryHCacheService hservice;
	
	@Autowired
	private RecommendZoneService		apptusrecommendzoneservice;
	
	@Autowired
	private TopSearchService topSearchService;
	
	Logger logger = LoggerFactory.getLogger(ApptusApiService.class);
	
	
	public BookGroupsApptusResponseWrapper getBookGroupsDetails(MainJsonRequestBean reqBean, User user, Double apiVersion) throws Exception{
		
		BookGroupsApptusResponseWrapper	wrapper = new BookGroupsApptusResponseWrapper();
		BookGroupViewEnum	viewtype = BookGroupViewEnum.getViewName( reqBean.getView() );
		List<BookGroupVOApptus> bookGroups = new ArrayList<BookGroupVOApptus>();
		switch( viewtype ){
			case SERIES :
				if( !StringUtils.isBlank( reqBean.getPagenumber() ) && 
					(reqBean.getPagesize() != null && reqBean.getPagesize() >0 ) )
				{								
					wrapper = getPopularSeriesBasedOnPageNumber(reqBean,user,apiVersion,wrapper);				
					wrapper.setStatus(ErrorCodes.OK.getMessage());
					wrapper.setApiversion(apiVersion);				
				}
				break;
			case MAIN:				
				bookGroups = getLevel1CategoryData(reqBean, user, apiVersion);
				wrapper.setBookGroups(bookGroups);
				wrapper.setStatus(ErrorCodes.OK.getMessage());
				wrapper.setApiversion(apiVersion);
				break;
			case LEVEL2 :
				if( !StringUtils.isBlank( reqBean.getBookgroupid() ) ){
					bookGroups = getLevel2CategoryData(reqBean, user, apiVersion);					
					wrapper.setBookGroups(bookGroups);
					wrapper.setStatus(ErrorCodes.OK.getMessage());
					wrapper.setApiversion(apiVersion);				
				}
				break;
			case RELATED_BOOK:
				if( (reqBean.getBookid() != null && reqBean.getBookid()>0) ||
						StringUtils.isNotBlank(reqBean.getIsbn())){
					bookGroups = getRelatedProduct(reqBean, user, apiVersion);
					wrapper.setBookGroups(bookGroups);
					wrapper.setStatus(ErrorCodes.OK.getMessage());
					wrapper.setApiversion(apiVersion);				
				}
				break;
			case RELATED_BOOK_2:
				if(reqBean.getBookid() != null && reqBean.getBookid()>0 ){
					bookGroups = getRelatedProduct2(reqBean, user, apiVersion);
					wrapper.setBookGroups(bookGroups);
					wrapper.setStatus(ErrorCodes.OK.getMessage());
					wrapper.setApiversion(apiVersion);				
				}
				break;
				
		default:
				reqBean.setView( BookGroupViewEnum.MAIN.getView());
				bookGroups = getLevel1CategoryData(reqBean, user, apiVersion);
				wrapper.setBookGroups(bookGroups);
				wrapper.setStatus(ErrorCodes.OK.getMessage());
				wrapper.setApiversion(apiVersion);
			break;
		}	
		return wrapper;
	}
	public BookGroupsApptusResponseWrapper getPopularSeriesBasedOnPageNumber(MainJsonRequestBean reqBean, User user, Double apiVersion, 
			BookGroupsApptusResponseWrapper wrapper) throws Exception 
	{	
		UserAuthToken authToken = reqBean.getAuthToken();
		Profile		  profile 	= user.getProfile();		
		try
		{
			CloudConnector eSales = CloudConnector.getOrCreate("esales://"+applicationpropconfig.getUsername()+":"+applicationpropconfig.getPassword());
			DynamicPage dynamicPage = eSales.session(String.valueOf(authToken), String.valueOf(profile!=null&&profile.getProfileid()!=null ?
					profile.getProfileid():user.getCustomerid())
					, apptusServiceHelper.getMarketPanelArgument(user.getCountrycode())).dynamicPage("popular-series-dynamic");
			int pageSize = reqBean.getPagesize();
			int end =0, start =0;			
			String urlForSeries = applicationpropconfig.getNestSeriesUrl();
			ArrayList<BookGroupVOApptus> volist = new ArrayList<>();
			SeriesList serieslist = new SeriesList();
			try{
				if(user.getProfile() != null && user.getProfile().getCategory() != null &&
						user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
				{
					urlForSeries = applicationpropconfig.getNestSeriesUrlKids();
					serieslist = cacheBridgeSeries.callNestSeriesUrl(urlForSeries,PROFILE_CATEGORIES.KIDS.name(),user.getCountrycode());
				}
				else			
					serieslist = cacheBridgeSeries.callNestSeriesUrl(urlForSeries,PROFILE_CATEGORIES.ALL.name(),user.getCountrycode());			
		 	
				//Loop Starts from api call 
				ArrayList<Series> series = serieslist.getDatum();
				HashMap<String,Integer> seriesmap = new HashMap<>();		 	
				if(series != null && series.size()> 0){
					if( reqBean.getPagenumber() == null || reqBean.getPagenumber().equalsIgnoreCase("") ){		 			
						wrapper.setSize(series.size());
						for(Series ser:series){
							if(ser.getDisplayOrderIndex() != null && ser.getDisplayOrderIndex()>0){
								Subpanel bookPanel = Subpanel.create(ser.getSeriesName(), "/popularseriesdynamic");				 		
								bookPanel.addArgument("series_filter", FilterUtils.buildSeriesFilter( ser.getSeriesName() ) );
				 				dynamicPage.addSubpanel(bookPanel);
				 				seriesmap.put(ser.getSeriesName(), ser.getOrder());
							}
						}
					}else{
						end 	= (Integer.valueOf(reqBean.getPagenumber())==0?1: (Integer.valueOf(reqBean.getPagenumber())+1))* pageSize;
						start	= end - pageSize;				 	
						for(int i = start; (i < end && (series.size()-1) >= i ); i++){
							wrapper.setSize(series.size());
							Series ser = series.get(i);
							if(ser.getDisplayOrderIndex() != null && ser.getDisplayOrderIndex() > 0){
								Subpanel bookPanel = Subpanel.create(ser.getSeriesName(), "/popularseriesdynamic");					 		
								bookPanel.addArgument("series_filter", FilterUtils.buildSeriesFilter( ser.getSeriesName() ) );
								dynamicPage.addSubpanel(bookPanel);
								seriesmap.put(ser.getSeriesName(), ser.getOrder());
							}			 			
						}						
					}
				}		 			 
				ArgMap dynamicPageArguments = new ArgMap();
				dynamicPageArguments.put("window_first", "1");
				dynamicPageArguments.put("window_last", "3");
				dynamicPageArguments.put("subscription_filter", 
					FilterUtils.buildSubscriptionFilter("1","" + InMemoryCache.subAgeLimits.get(user.getSubscription().getType()) ));
				if( !StringUtils.isBlank( user.getUserCountry() ) )
				{
					dynamicPageArguments.put("include_filter", FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ) );
					dynamicPageArguments.put("exclude_filter", FilterUtils.buildCountryExcludeFilter(user.getUserCountry() ) );
				}
				if(!StringUtils.isBlank( user.getUserCountry() ))				
					dynamicPageArguments.put("market_filter", FilterUtils.buildMarketFilter( user.getCountrycode() ) );
			
				if(user.getExcludePubs()!=null)									
					dynamicPageArguments.put("publisher_filter", FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs()));			
			
				PanelContent content = dynamicPage.retrieveContent(dynamicPageArguments);
				HashMap<String,ArrayList<ProductsAptus>> data = new HashMap<>();			
				ArrayList<ProductsAptus> productlist = new ArrayList<>();
				String serieslocal = "";
				for(PanelContent contents :content.subpanels())
				{				
					if(contents.subpanels() != null && contents.subpanels().size()>0)
					{
						for(PanelContent datacontent1:contents.subpanels())
						{						
							for(PanelContent  datacontent:datacontent1.subpanels())
							{
								if(datacontent.hasResult()){
									for(Product pd:datacontent.resultAsProducts())
									{
										ProductsAptus p1 = new ProductsAptus();
										p1.setTicket(pd.getTicket());
										p1.setKey(pd.toString());
										AttributesProductAptus a1 = new AttributesProductAptus();
										a1.setAuthors(String.valueOf(pd.getAttribute("authors")));
										a1.setTitle(String.valueOf(pd.getAttribute("title")));
										a1.setCoverimage(String.valueOf(pd.getAttribute("coverimage")));
										a1.setSeries(String.valueOf(pd.getAttribute("seriesAsit")));
										serieslocal = String.valueOf(pd.getAttribute("seriesAsit"));
										a1.setRank(String.valueOf(pd.getAttribute("rank")));
										a1.setRelevance(String.valueOf(pd.getAttribute("relevance")));
										a1.setSequenceseries(Integer.valueOf(String.valueOf(pd.getAttribute("sequenceseries"))));
										p1.setAttributes(a1);
										ArrayList<VariantsAptus> variantlist = new ArrayList<>();
										for(Variant variant:pd.getVariants())
										{
											VariantsAptus variantnew = new VariantsAptus();
											AttributesAptus attributes = new AttributesAptus();
											attributes.setBookid(String.valueOf(variant.getAttribute("bookid")));
											attributes.setDescription(String.valueOf(variant.getAttribute("description")));
											attributes.setFormattype(String.valueOf(variant.getAttribute("formattype")));
											attributes.setImageversion(String.valueOf(variant.getAttribute("imageversion")));
											//attributes.setNyheter_pos(Integer.valueOf(String.valueOf(variant.getAttribute("Nyheter_pos"))));
											attributes.setPopularity(String.valueOf(variant.getAttribute("popularity")));
											attributes.setProductstatus(String.valueOf(variant.getAttribute("productstatus")));
											attributes.setProviderproductid(String.valueOf(variant.getAttribute("providerproductid")));
											attributes.setPublisheddate(String.valueOf(variant.getAttribute("publisheddate")));
											attributes.setRelatedproductid(Integer.valueOf(String.valueOf(variant.getAttribute("relatedproductid"))));
											//attributes.setVi_rekommenderar_pos(Integer.valueOf(String.valueOf(variant.getAttribute("vi_rekommenderar_pos"))));
											variantnew.setKey(variant.toString());
											variantnew.setTicket(variant.getTicket());
											variantnew.setAttributes(attributes);
											variantlist.add(variantnew);
										}
										p1.setVariants(variantlist);
										productlist.add(p1);									
									}															
									data.put(serieslocal, productlist);
									productlist = new ArrayList<>(); 
								}
							}
						}					
					}
				}			
				for(Entry<String,ArrayList<ProductsAptus>> operateOn:data.entrySet())
				{
					BookGroupVOApptus vo = apptusServiceHelper.operateOnAptusSeriesDynamic(operateOn, reqBean.getView() );
					if(null != seriesmap.get(vo.getTitle()))
					{
						int position = seriesmap.get(vo.getTitle());
						vo.setPosition((position-1));					
						volist.add(vo);
					}
				}			
				Collections.sort(volist, new CustomComparator());
				for(BookGroupVOApptus vo : volist){
					vo.setPosition(start); 
					start++;
				}			
			}catch (MissingSubpanelException e) {
				e.printStackTrace();
				logger.trace("No data found..." + e.getMessage()  );
			}
			//wrapper.setStart(start);
			wrapper.setBookGroups(volist);
			return wrapper;			
		}catch(Exception e)
		{
			if(logger.isErrorEnabled())
				logger.error("Error while fetching series from NEST" + e);			
			throw new Exception(e);
		}		
	}

	public ArrayList<BookGroupVOApptus> getLevel2CategoryData(MainJsonRequestBean reqBean, User user,Double apiVersion) {
	
		ArrayList<BookGroupVOApptus> volist = new ArrayList<>();
		try
		{
			Map<Integer,EbCategories> categories= new HashMap<Integer,EbCategories>();
			Locale locale 	 = Book2GoUtil.getLocaleBasedOnString(reqBean.getLocale());
			if( !StringUtils.isBlank( user.getCountrycode() ) )
				categories =cacheBridge1.getCategoryCache(user.getCountrycode());
		
			if(reqBean.getBookgroupid().indexOf(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()) != -1)
			{
				String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
				EbCategories maincategory = categories.get( Integer.valueOf(idlist[1]) );
				
				if(maincategory != null && maincategory.getCategoryId()!= null 
						&& maincategory.getSubCategories()!= null && maincategory.getCategoryId()>0 && maincategory.getParentCatId()==0)
				{					
					BookGroupVOApptus voNews = new BookGroupVOApptus();
					voNews.setParentid(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+maincategory.getCategoryId());
					voNews.setId(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST.getGroupid()+
								ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+maincategory.getCategoryId()+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.GENERIC_Nyheter.getGroupid());
					voNews.setType(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST_TYPE.getGroupid());
					/*if(StringUtils.isNotBlank(user.getCountrycode()) && user.getCountrycode().equalsIgnoreCase("FI"))
					 voNews.setTitle(TopPanelSubCatEnum.Nyheter_FI.getFormatName());
					else
					 voNews.setTitle(TopPanelSubCatEnum.Nyheter_SE.getFormatName());*/	
					voNews.setTitle(Book2GoUtil.getLocalizedString( locale, "product.News."+user.getCountrycode()));
					voNews.setHaschild(0);
					voNews.setPosition(0);
					voNews.setAllowsorting(0);
					voNews.setViewby(reqBean.getView());
					volist.add(voNews);	
					
					int position = 1;
					
					if(applicationpropconfig.getTopSellersMode().equalsIgnoreCase("on"))
					{
						BookGroupVOApptus topsellers = new BookGroupVOApptus();
						topsellers.setParentid(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+maincategory.getCategoryId());
						topsellers.setId(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopSeller.getGroupid()+
									ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+maincategory.getCategoryId());
						topsellers.setType(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST_TYPE.getGroupid());	
						topsellers.setTitle(Book2GoUtil.getLocalizedString( locale, "product.top.sellers."+user.getCountrycode()));
						topsellers.setHaschild(0);
						topsellers.setPosition(1);
						topsellers.setAllowsorting(0);
						topsellers.setViewby(reqBean.getView());
						volist.add(topsellers);	
						position++;
					}
					
					
					List<EbCategories> list = maincategory.getSubCategories();
					Collections.sort(list, new CustomComparator2());
					
					for(EbCategories subcategory:list)
					{
						BookGroupVOApptus vo = new BookGroupVOApptus();
						vo.setParentid(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+maincategory.getCategoryId());
						vo.setId(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+subcategory.getCategoryId());
						vo.setType(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_CATEGORY_TYPE.getGroupid());
						vo.setTitle(subcategory.getCategoryName());
						if(apiVersion>CONSTANTS.API_VERSION_6_4)
						{
							if(subcategory.getSubCategories()!=null && subcategory.getSubCategories().size()>0)
								vo.setHaschild(1);
							else
							   vo.setHaschild(0);
						}
						else
						{
							vo.setHaschild(0);
						}
						vo.setPosition( position );
						vo.setAllowsorting(1);
						vo.setViewby(reqBean.getView());
						vo.setSlugname(subcategory.getSlugName());
						position++;
						volist.add(vo);
					}					
				}
				else if(maincategory != null && maincategory.getCategoryId()!= null 
						&& maincategory.getSubCategories()!= null && maincategory.getCategoryId()>0 && maincategory.getParentCatId()>0)
				{
					BookGroupVOApptus voNews = new BookGroupVOApptus();
					voNews.setParentid(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+maincategory.getCategoryId());
					voNews.setId(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST.getGroupid()+
								ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+maincategory.getCategoryId()+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.GENERIC_Nyheter.getGroupid());
					voNews.setType(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST_TYPE.getGroupid());
					/*if(StringUtils.isNotBlank(user.getCountrycode()) && user.getCountrycode().equalsIgnoreCase("FI"))
					 voNews.setTitle(TopPanelSubCatEnum.Nyheter_FI.getFormatName());
					else
					 voNews.setTitle(TopPanelSubCatEnum.Nyheter_SE.getFormatName());*/	
					voNews.setTitle(Book2GoUtil.getLocalizedString( locale, "product.News."+user.getCountrycode()));
					voNews.setHaschild(0);
					voNews.setPosition(0);
					voNews.setAllowsorting(0);
					voNews.setViewby(reqBean.getView());
					volist.add(voNews);					
					List<EbCategories> list = maincategory.getSubCategories();
					Collections.sort(list, new CustomComparator2());
					int position = 1;
					for(EbCategories subcategory:list)
					{
						BookGroupVOApptus vo = new BookGroupVOApptus();
						vo.setParentid(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+maincategory.getCategoryId());
						vo.setId(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+subcategory.getCategoryId());
						vo.setType(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_CATEGORY_TYPE.getGroupid());
						vo.setTitle(subcategory.getCategoryName());
						if(apiVersion>CONSTANTS.API_VERSION_6_4)
						{
							if(subcategory.getSubCategories()!=null && subcategory.getSubCategories().size()>0)
								vo.setHaschild(1);
							else
							   vo.setHaschild(0);
						}
						else
						{
							vo.setHaschild(0);
						}
						vo.setPosition( position );
						vo.setAllowsorting(1);
						vo.setViewby(reqBean.getView());
						vo.setSlugname(subcategory.getSlugName());
						position++;
						volist.add(vo);
					}					
				}
			}
			return volist;
		}catch(Exception e){
			logger.error("Error while fetching category data :: " + e);			
			return volist;
		}
	}
	
	public ArrayList<BookGroupVOApptus> getLevel1CategoryData(MainJsonRequestBean reqBean, User user, Double apiVersion) throws Exception {		
		ArrayList<BookGroupVOApptus> volist = new ArrayList<>();		
		try{
			if(user!=null && user.getProfile() != null && user.getProfile().getCategory() != null &&
					user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))			
				volist = getLevel1DataForChild(reqBean,apiVersion,user);			
			else			
				volist = getLevel1DataForParent(reqBean,apiVersion,user);					
			return volist;
		}
		catch(Exception e)
		{
			logger.error("Error while fetching level1 data ::" + e);			
			return volist;
		}
	}
	
	public ArrayList<BookGroupVOApptus> getLevel1DataForChild(MainJsonRequestBean reqBean, Double apiVersion, User user)
	{
		ArrayList<BookGroupVOApptus> volist = new ArrayList<>();
		
		EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));
		
		if(kidCategory != null && kidCategory.getSubCategories() != null){
			int i = 0;
			String url = applicationpropconfig.getTopPanelUrlNew();
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			url = url + "&profile=CHILD&subscriptionorder="+map.get(user.getSubscription().getType());
			url = url +"&country="+user.getCountrycode();
			TopPanelList topPanelList = new TopPanelList();
			try {
				topPanelList = cacheBridgeTopPanels.callNestTopPanelsUrl(url, "CHILD",user.getCountrycode());
			} catch (Exception e) {			
				e.printStackTrace();
			}
			for(TopPanelVO vo:topPanelList.getTopPanelList()){
				BookGroupVOApptus voRecomendCustomer = new BookGroupVOApptus();
				voRecomendCustomer.setId(vo.getGroupid());
				voRecomendCustomer.setType(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TYPE.getGroupid());
				voRecomendCustomer.setTitle(vo.getTitle());
				voRecomendCustomer.setHaschild(0);
				voRecomendCustomer.setPosition(i);
				voRecomendCustomer.setAllowsorting(0);
				voRecomendCustomer.setViewby( reqBean.getView());
				
				if(StringUtils.isNotBlank(vo.getGroupid()) && vo.getGroupid().contains("dynamic"))
				{
					Boolean description =false;
					Boolean image =false;
					
					if(vo.getListimageurl()!=null && !vo.getListimageurl().equalsIgnoreCase(""))
						{
							voRecomendCustomer.setImageurl(vo.getListimageurl());
							image = true;
						}
					
					if(vo.getLabel()!=null && !vo.getLabel().equalsIgnoreCase(""))
						{
							voRecomendCustomer.setLabel(vo.getLabel());
						}
					
					if(vo.getDescription()!=null && !vo.getDescription().equalsIgnoreCase(""))
						{
							voRecomendCustomer.setDescription(vo.getDescription());
							description= true;
						}
					
					if(vo.getListimageurllarger()!=null && !vo.getListimageurllarger().equalsIgnoreCase(""))
						{
							voRecomendCustomer.setImagelargeurl(vo.getListimageurllarger());
							image = true;
						}
					
					if(vo.getDarktext()!=null && (vo.getDarktext().intValue()==0||vo.getDarktext().intValue()==1))
						voRecomendCustomer.setDarktext(vo.getDarktext().intValue()==1?Boolean.TRUE:Boolean.FALSE);
					else
						voRecomendCustomer.setDarktext(Boolean.TRUE);
					
					voRecomendCustomer.setPromotionalList(true);
					
					if(apiVersion > CONSTANTS.API_VERSION_6_3 && description && image)
					voRecomendCustomer.setType(ApptusBookGroupsEnum.Promotion_LIST_TYPE.getGroupid());
				}
				else
				{
					voRecomendCustomer.setPromotionalList(false);
				}
				
				volist.add(voRecomendCustomer);
				i++;
			}
			List<EbCategories> list = kidCategory.getSubCategories();
					Collections.sort(list, new CustomComparator2());
			for(EbCategories subcategory:list){
				BookGroupVOApptus vo = new BookGroupVOApptus();				
				vo.setId(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+subcategory.getCategoryId());
				vo.setType(ApptusBookGroupsEnum.TOP_TIER_TOP_CATEGORY_TYPE.getGroupid());
				vo.setTitle(subcategory.getCategoryName());
				vo.setHaschild(0);
				vo.setPosition(i);
				vo.setAllowsorting(1);
				vo.setViewby( reqBean.getView());
				vo.setPromotionalList(false);
				i++;
				volist.add(vo);					
			}
		}		
		return volist;
	}
	
	
	public ArrayList<BookGroupVOApptus> getLevel1DataForParent(MainJsonRequestBean reqBean, Double apiVersion, User user)
	{
		ArrayList<BookGroupVOApptus> volist = new ArrayList<>();
		Map<Integer,EbCategories> categories =cacheBridge1.getCategoryCache(user.getCountrycode());		
		if(categories != null)
		{
				int i =0;
				String url = applicationpropconfig.getTopPanelUrlNew();
				Map<String , Integer> map = InMemoryCache.subAgeLimits;
				url = url + "&profile=PARENT&subscriptionorder="+map.get(user.getSubscription().getType());				
				url = url +"&country="+user.getCountrycode();
				TopPanelList topPanelList = new TopPanelList();
				try {
					topPanelList = cacheBridgeTopPanels.callNestTopPanelsUrl(url, "PARENT",user.getCountrycode());
				} catch (Exception e) {				
					e.printStackTrace();
				}
				if( topPanelList != null ){
					for(TopPanelVO vo:topPanelList.getTopPanelList())
					{
						BookGroupVOApptus voRecomendCustomer = new BookGroupVOApptus();					
						voRecomendCustomer.setId(vo.getGroupid());
						voRecomendCustomer.setType(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TYPE.getGroupid());
						voRecomendCustomer.setTitle(vo.getTitle());
						voRecomendCustomer.setHaschild(0);
						voRecomendCustomer.setPosition(i);
						voRecomendCustomer.setAllowsorting(0);
						voRecomendCustomer.setViewby( reqBean.getView());
						
						if(StringUtils.isNotBlank(vo.getGroupid()) && vo.getGroupid().contains("dynamic"))
						{
							Boolean description =false;
							Boolean image =false;
							
							if(vo.getListimageurl()!=null && !vo.getListimageurl().equalsIgnoreCase(""))
								{
									voRecomendCustomer.setImageurl(vo.getListimageurl());
									image = true;
								}
							
							if(vo.getLabel()!=null && !vo.getLabel().equalsIgnoreCase(""))
								{
									voRecomendCustomer.setLabel(vo.getLabel());
								}
							
							if(vo.getDescription()!=null && !vo.getDescription().equalsIgnoreCase(""))
								{
									voRecomendCustomer.setDescription(vo.getDescription());
									description= true;
								}
							
							if(vo.getListimageurllarger()!=null && !vo.getListimageurllarger().equalsIgnoreCase(""))
								{
									voRecomendCustomer.setImagelargeurl(vo.getListimageurllarger());
									image = true;
								}
							
							if(vo.getDarktext()!=null && (vo.getDarktext().intValue()==0||vo.getDarktext().intValue()==1))
								voRecomendCustomer.setDarktext(vo.getDarktext().intValue()==1?Boolean.TRUE:Boolean.FALSE);
							else
								voRecomendCustomer.setDarktext(Boolean.TRUE);
							
							voRecomendCustomer.setPromotionalList(true);
							
							if(apiVersion > CONSTANTS.API_VERSION_6_3 && description && image)
							voRecomendCustomer.setType(ApptusBookGroupsEnum.Promotion_LIST_TYPE.getGroupid());
						}
						else
						{
							voRecomendCustomer.setPromotionalList(false);
						}
						volist.add(voRecomendCustomer);
						i++;
					}
				}
								
				for(Entry<Integer,EbCategories> subcategory:categories.entrySet())
				{
					if(subcategory.getValue().getParentCatId() == 0 && subcategory.getValue().getSubCategories() != null)
					{
						BookGroupVOApptus vo = new BookGroupVOApptus();						
						vo.setId(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+subcategory.getValue().getCategoryId());
						vo.setType(ApptusBookGroupsEnum.TOP_TIER_TOP_CATEGORY_TYPE.getGroupid());
						vo.setTitle(subcategory.getValue().getCategoryName());
						vo.setHaschild(1);
						vo.setPosition(i);
						vo.setAllowsorting(1);
						vo.setViewby( reqBean.getView());
						vo.setPromotionalList(false);
						vo.setSlugname(subcategory.getValue().getSlugName());
						i++;
						volist.add(vo);
					}
				}
		}		
		return volist;
	}
	
	public ArrayList<BookGroupVOApptus> getRelatedProduct2(MainJsonRequestBean reqBean, User user, Double apiVersion){
				
		ArrayList<BookGroupVOApptus> volist = new ArrayList<>();		
		try
		{
			Locale locale 	 = Book2GoUtil.getLocaleBasedOnString(reqBean.getLocale());
			if(reqBean.getBookid() > 0)
			{		
				ProductDetails productDetails = productDao.productDetailsFromMongo(reqBean.getBookid(), null, user, true, user.getCountrycode());							
				if(productDetails != null)
				{					
					if (StringUtils.isNotBlank(productDetails.getSeriesName())){
						BookGroupVOApptus vo = new BookGroupVOApptus();
						vo.setId(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+reqBean.getBookid()+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_SERIES.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setTitle(Book2GoUtil.getLocalizedString( locale, "product.related.book.in.same.series."+user.getCountrycode()));
						vo.setSlugname(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+reqBean.getBookid()+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_SERIES.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setHaschild(0);
						vo.setPosition(0);
						vo.setAllowsorting(0);
						vo.setViewby( reqBean.getView() );
						vo.setShowvolume(1);
						volist.add(vo);
					}else{
						Integer aptusid;
						List<String> authors = new ArrayList<String>();
						if (productDetails.getProductType().getFormatNo() == ProductFormatEnum.AUDIO_BOOK.getFormatNo()){
							authors = productDetails.getAudioDetails().getAuthors();
							aptusid = productDetails.getAudioDetails().getAptusid();
						} else{
							authors = productDetails.getEbookDetails().getAuthors();
							aptusid = productDetails.getEbookDetails().getAptusid();
						}				
						if (authors.size() > 0) {
							String[] authorsArray = new String[authors.size()];
							authors.stream().parallel().map(String::toLowerCase).collect(Collectors.toList())
								.toArray(authorsArray);
												
							RelatedBookZonePanel panel 		= apptusServiceHelper.getAptusUrlForRelatedAuthors(authors, aptusid,user,user.getCountrycode());
							AptusThird 			pubDetails 	= releatedbookzoneservice.search( panel );
							if (null != pubDetails && null != pubDetails.getSearchHits()) {
								AptusSecond pubdetaisl1 = new AptusSecond();
								pubdetaisl1.setSearchHits(pubDetails.getSearchHits());							
								ArrayList<ProductInformation> authorList = apptusServiceHelper
									.operateOnAptusSecondSearch(pubdetaisl1);
								if (authorList != null) {
									BookGroupVOApptus vo = new BookGroupVOApptus();
									vo.setId(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid() + reqBean.getBookid()
										+ ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()
										+ ApptusBookGroupsEnum.RELATED_BOOK_GROUP_AUTHORS.getGroupid());
									vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
									vo.setSlugname(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid() + reqBean.getBookid()
										+ ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()
										+ ApptusBookGroupsEnum.RELATED_BOOK_GROUP_AUTHORS.getGroupid());
									vo.setTitle(Book2GoUtil.getLocalizedString( locale, "product.related.book.in.same.authors."+user.getCountrycode()));
									vo.setHaschild(0);
									vo.setPosition(0);
									vo.setAllowsorting(1);
									vo.setViewby( reqBean.getView() );
									vo.setShowvolume(0);
									volist.add(vo);
								}
							}
						}
					}
			
					if(applicationpropconfig.getRecommendMode().equalsIgnoreCase("on"))
					{
						BookGroupVOApptus vo = new BookGroupVOApptus();
						vo.setId(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+reqBean.getBookid()+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_RECOMMEND.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setTitle(Book2GoUtil.getLocalizedString( locale, "product.related.book.in.rec."+user.getCountrycode()));
						//vo.setTitle("Rekommenderade bÃƒÂ¶cker");
						vo.setSlugname(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+reqBean.getBookid()+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_RECOMMEND.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setHaschild(0);
						vo.setPosition(1);
						vo.setAllowsorting(0);
						vo.setViewby( reqBean.getView() );
						vo.setShowvolume(0);
						volist.add(vo);
					}
					
					
					if(apiVersion>CONSTANTS.API_VERSION_6_4)
					{
						List<String> authors = new ArrayList<String>();
						List<String> narrators = new ArrayList<String>();
						if (productDetails.getProductType().getFormatNo() == ProductFormatEnum.AUDIO_BOOK.getFormatNo()){
							authors = productDetails.getAudioDetails().getAuthors();
							narrators = productDetails.getAudioDetails().getNarattors();
						} else{
							authors = productDetails.getEbookDetails().getAuthors();
							narrators = productDetails.getEbookDetails().getNarattors();
						}
						
						if(authors!=null && authors.size()>0)
						{
							int i =0;
							for(String aut:authors)
							{
								if(StringUtils.isNotBlank(aut))
								{
								BookGroupVOApptus vo = new BookGroupVOApptus();
								vo.setId("search_authors_"+aut);
								vo.setType("Authors");
								vo.setTitle(aut);
								vo.setSlugname("search_authors_"+aut);
								vo.setHaschild(0);
								vo.setPosition(i);
								vo.setAllowsorting(0);
								vo.setViewby( reqBean.getView() );
								vo.setShowvolume(0);
								volist.add(vo);
								i++;
								}
							}
						}
						
						if(narrators!=null && narrators.size()>0)
						{
							int i =0;
							for(String aut:narrators)
							{
								if(StringUtils.isNotBlank(aut))
								{
								BookGroupVOApptus vo = new BookGroupVOApptus();
								vo.setId("search_narrators_"+aut);
								vo.setType("Narrators");
								vo.setTitle(aut);
								vo.setSlugname("search_narrators_"+aut);
								vo.setHaschild(0);
								vo.setPosition(i);
								vo.setAllowsorting(0);
								vo.setViewby( reqBean.getView() );
								vo.setShowvolume(0);
								volist.add(vo);
								i++;
								}
							}
						}
					}
					
				}			
			}		
			return volist;
		}
		catch(Exception e)
		{
			logger.error("Error while fetching related products ::: " + e);
			//System.out.println(e);
			return volist;
		}
	}
	
	
	public ArrayList<BookGroupVOApptus> getRelatedProduct(MainJsonRequestBean reqBean, User user, Double apiVersion){
		
		ArrayList<BookGroupVOApptus> volist = new ArrayList<>();		
		try
		{
			Locale locale 	 = Book2GoUtil.getLocaleBasedOnString(reqBean.getLocale());
			ProductDetails productDetails = null;
			Integer bookid=null;
				if(reqBean.getBookid()!=null && reqBean.getBookid() > 0 )
				{		
					 productDetails = productDao.productDetailsFromMongo(reqBean.getBookid(), null, user, true, user.getCountrycode());		
				}
				else if(StringUtils.isNotBlank(reqBean.getIsbn()))
				{
			 		productDetails = productDao.productDetailsFromMongo(reqBean.getIsbn(), null, user, true, user.getCountrycode());	
				}
			
				if(productDetails != null)
				{			
					if (productDetails.getProductType().getFormatNo() == ProductFormatEnum.AUDIO_BOOK.getFormatNo()){
						bookid = productDetails.getAudioDetails().getProductid();
					} else{
						bookid = productDetails.getEbookDetails().getProductid();
					}
					
					
					if (StringUtils.isNotBlank(productDetails.getSeriesName())){
						BookGroupVOApptus vo = new BookGroupVOApptus();
						vo.setId(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+(reqBean.getBookid()!=null?reqBean.getBookid():bookid)+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_SERIES.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setTitle(Book2GoUtil.getLocalizedString( locale, "product.related.book.in.same.series."+user.getCountrycode()));
						vo.setSlugname(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+(reqBean.getBookid()!=null?reqBean.getBookid():bookid)+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_SERIES.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setHaschild(0);
						vo.setPosition(0);
						vo.setAllowsorting(0);
						vo.setViewby( reqBean.getView() );
						vo.setShowvolume(1);
						volist.add(vo);
					}else{
						Integer aptusid;
						List<String> authors = new ArrayList<String>();
						if (productDetails.getProductType().getFormatNo() == ProductFormatEnum.AUDIO_BOOK.getFormatNo()){
							authors = productDetails.getAudioDetails().getAuthors();
							aptusid = productDetails.getAudioDetails().getAptusid();
						} else{
							authors = productDetails.getEbookDetails().getAuthors();
							aptusid = productDetails.getEbookDetails().getAptusid();
						}				
						if (authors.size() > 0) {
							String[] authorsArray = new String[authors.size()];
							authors.stream().parallel().map(String::toLowerCase).collect(Collectors.toList())
								.toArray(authorsArray);
												
							RelatedBookZonePanel panel 		= apptusServiceHelper.getAptusUrlForRelatedAuthors(authors, aptusid,user,user.getCountrycode());
							AptusThird 			pubDetails 	= releatedbookzoneservice.search( panel );
							if (null != pubDetails && null != pubDetails.getSearchHits()) {
								AptusSecond pubdetaisl1 = new AptusSecond();
								pubdetaisl1.setSearchHits(pubDetails.getSearchHits());							
								ArrayList<ProductInformation> authorList = apptusServiceHelper
									.operateOnAptusSecondSearch(pubdetaisl1);
								if (authorList != null) {
									BookGroupVOApptus vo = new BookGroupVOApptus();
									vo.setId(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid() + (reqBean.getBookid()!=null?reqBean.getBookid():bookid)
										+ ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()
										+ ApptusBookGroupsEnum.RELATED_BOOK_GROUP_AUTHORS.getGroupid());
									vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
									vo.setSlugname(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid() + (reqBean.getBookid()!=null?reqBean.getBookid():bookid)
										+ ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()
										+ ApptusBookGroupsEnum.RELATED_BOOK_GROUP_AUTHORS.getGroupid());
									vo.setTitle(Book2GoUtil.getLocalizedString( locale, "product.related.book.in.same.authors."+user.getCountrycode()));
									vo.setHaschild(0);
									vo.setPosition(0);
									vo.setAllowsorting(1);
									vo.setViewby( reqBean.getView() );
									vo.setShowvolume(0);
									volist.add(vo);
								}
							}
						}
					}
			
					if(applicationpropconfig.getRecommendMode().equalsIgnoreCase("on"))
					{
						BookGroupVOApptus vo = new BookGroupVOApptus();
						vo.setId(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+(reqBean.getBookid()!=null?reqBean.getBookid():bookid)+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_RECOMMEND.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setTitle(Book2GoUtil.getLocalizedString( locale, "product.related.book.in.rec."+user.getCountrycode()));
						//vo.setTitle("Rekommenderade bÃƒÂ¶cker");
						vo.setSlugname(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()+(reqBean.getBookid()!=null?reqBean.getBookid():bookid)+
								ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid()+ApptusBookGroupsEnum.RELATED_BOOK_GROUP_RECOMMEND.getGroupid());
						vo.setType(ApptusBookGroupsEnum.RELATED_TYPE.getGroupid());
						vo.setHaschild(0);
						vo.setPosition(1);
						vo.setAllowsorting(0);
						vo.setViewby( reqBean.getView() );
						vo.setShowvolume(0);
						volist.add(vo);
					}
					
				}			
					
			return volist;
		}
		catch(Exception e)
		{
			logger.error("Error while fetching related products ::: " + e);
			//System.out.println(e);
			return volist;
		}
	}
	
	public BooksForBookGroupsApptusResponseWrapper getBooksForBookGroupForNestPreview(String bookgroupid,String pagenumber, Integer rows, Integer subtype,String countrycode) throws Exception
	{		
		GenericPanelAppPreviewRequest panel = getApptusUrlForNestPreview(bookgroupid, pagenumber, rows, subtype,countrycode);
		return apptusServiceHelper.getBooksFromGenericPanelNestPreview(panel, pagenumber, rows,countrycode);
	}
		
	public BooksForBookGroupsApptusResponseWrapper getBooksForBookGroup(BooksForBookGroupRequestBean reqBean, User user, Double apiVersion) throws Exception
	{					
		List<Object> m_results = getApptusBooksForBookGroupCriteria(reqBean,apiVersion,user);
		GenericPanel panelrequest = ( m_results.get( 0 ) != null)?(GenericPanel)m_results.get( 0 ):null;
		String 		 url 		  = ( m_results.get( 1 ) != null)?(String)m_results.get( 1 ):null;
		BooksForBookGroupsApptusResponseWrapper proddetails = new BooksForBookGroupsApptusResponseWrapper();
		if(panelrequest == null && url != null && url.equalsIgnoreCase(ApptusBookGroupsEnum.RELATED_RECOMMEND_URL_TO_BE_FIRED.getGroupid()))
			proddetails = getDataForRecommendZoneForApp(reqBean,apiVersion,user,proddetails);
		else if(panelrequest == null && url != null && url.equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED.getGroupid()))
			proddetails = getDataForRecommendZoneForTopList(reqBean,apiVersion,user,proddetails);
		else if(panelrequest == null && url != null && url.equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopSeller.getGroupid()))
			proddetails = getDataForTopSellers(reqBean,apiVersion,user,proddetails);
		else if(panelrequest == null && url != null && url.equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_UPCOMING.getGroupid()))
			proddetails = getDataForUpComing(reqBean,apiVersion,user,proddetails);
		else if(panelrequest == null  && url != null && url.equalsIgnoreCase(ApptusBookGroupsEnum.EMPTY_DATA_TO_BE_RETURNED_RESPONSE.getGroupid()))
			proddetails.setBooks(new ArrayList<ProductInformation>(0));		
		else if(panelrequest == null && url != null && url.equalsIgnoreCase(ApptusBookGroupsEnum.NOT_VALID_BOOK_GROUP_ID.getGroupid())){
			proddetails = new BooksForBookGroupsApptusResponseWrapper();
			proddetails.setBookCount(0);
			proddetails.setBooks(new ArrayList<ProductInformation>(0));
		}else
			proddetails = apptusServiceHelper.getBooksFromGenericPanel(panelrequest , user, reqBean);
		
		if(proddetails.getBooks() == null || proddetails.getBooks().size()<=0)
		{
			proddetails = new BooksForBookGroupsApptusResponseWrapper();
			proddetails.setBookCount(0);
			proddetails.setBooks(new ArrayList<ProductInformation>());
		}
		//Top E books and Top A books format wrong scenario handle the flow same as above code
		return proddetails;
	}
		
	public BooksForBookGroupsApptusResponseWrapper getDataForRecommendZoneForApp(BooksForBookGroupRequestBean reqBean, Double apiVersion,User user
			,BooksForBookGroupsApptusResponseWrapper proddetails) throws Exception
	{
		// Recommend books Scenario
		if(!StringUtils.isBlank( reqBean.getBookgroupid() ) 
			&& reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid())
			&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP_RECOMMEND.getGroupid()))
		{
			String idlist[] 	= reqBean.getBookgroupid().split(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid());
			String idlist1[] 	= idlist[1].split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			//String countrycode = cacheBridgeMarket.getMarketFromNest().get(user.getCountrycode());
			Integer bookid = Integer.valueOf(idlist1[0]);
			ProductDetails productDetails = productDao.productDetailsFromMongo(bookid, null, user, false,user.getCountrycode());
			ProductRelatedDetails prodInfo = null;
			ProductRelatedDetails prodInfoForAuthors = null;
			//ProductRelatedDetails prodRelatedInfo = null;
			if(productDetails != null){
				if(productDetails.getProductType().getFormatNo()==ProductFormatEnum.E_BOOK.getFormatNo())
					prodInfo = productDetails.getEbookDetails();					
				else
					prodInfo = productDetails.getAudioDetails();
			}
			RecommendZone	panelrequet	= new RecommendZone();
			ProductFormatEnum	format	= ProductFormatEnum.getFormat( reqBean.getType() );
			if( format == ProductFormatEnum.E_BOOK ||  format == ProductFormatEnum.AUDIO_BOOK )			
				panelrequet.setFormat_filter( "" + format.getFormatNo());
			panelrequet.setProductKey( ( prodInfo != null )?"" + prodInfo.getAptusid() : "" );	
			List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd((StringUtils.isBlank( reqBean.getPagenumber()))?0:Integer.valueOf( reqBean.getPagenumber() ), reqBean.getRows());
			panelrequet.setWindow_first( "" + windowstart_end.get(0));
			panelrequet.setWindow_last( "" + windowstart_end.get(1) );
			panelrequet.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
			panelrequet.setCustomerKey( apptusServiceHelper.getApptusCustomerKey(user));
			panelrequet.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
			if( !StringUtils.isBlank( user.getUserCountry() ) ){							
				panelrequet.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));				
				panelrequet.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));
			}
			panelrequet.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()) );
			if( !StringUtils.isBlank( user.getUserCountry() ) )
				panelrequet.setMarket_filter( user.getUserCountry() );			
			String	child_filter = apptusServiceHelper.getChildFilterFromCategorys( user );
			if( !StringUtils.isBlank( child_filter ))
				panelrequet.setChild_filter(child_filter);
			if(productDetails != null){
				if(productDetails.getProductType().getFormatNo()==ProductFormatEnum.E_BOOK.getFormatNo())
					prodInfoForAuthors = productDetails.getEbookDetails();
				else
					prodInfoForAuthors = productDetails.getAudioDetails();
				if(prodInfoForAuthors!=null){
					List<String> authorslist = prodInfoForAuthors.getAuthors();
					String	 author_filter = "";
					byte count =0;
					if(null != authorslist && authorslist.size()>0){
						for(String authors:authorslist)	{
							authors = URLEncoder.encode(HtmlUtils.htmlUnescape(authors),CONSTANTS.UTF_8_CHARSET);							
							if(count ==0)
								author_filter = author_filter + " NOT authors:'"+ authors + "'";
							else
								author_filter = author_filter + " AND NOT authors:'"+ authors + "'";
							count++;
						}
						if( !StringUtils.isBlank( author_filter ))
							panelrequet.setAuthors_filter(author_filter);
					}
				}
				if(productDetails.getMainCategoryIds()!=null && productDetails.getMainCategoryIds().size()>0){
					String	category_filter = "";
					byte count =0;
					for(Integer cat:productDetails.getMainCategoryIds()){
						if(count == 0)
							category_filter = category_filter + FilterUtils.buildCategoryId( user.getCountrycode(), "" + cat);
						else
							category_filter = category_filter + " OR "+ FilterUtils.buildCategoryId( user.getCountrycode(), "" + cat);
						count++;
					}
					if( !StringUtils.isBlank( category_filter ))
						panelrequet.setCategory_filter(category_filter);
				}
				if(productDetails.getSeriesName()!=null && !productDetails.getSequence().equalsIgnoreCase("")){
					productDetails.setSeriesName(URLEncoder.encode(HtmlUtils.htmlUnescape(productDetails.getSeriesName()),"UTF-8"));
					panelrequet.setSeries_filter( " NOT seriesAsit:'"+productDetails.getSeriesName()+"'" + " AND NOT series:'"+productDetails.getSeriesName()+"'" );					
				}													
			}
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			panelrequet.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType()) ) );						
			try {
				ApptusFinalResponse2 apptus_results = apptusrecommendzoneservice.genericPanelSearch(panelrequet);
				
				if( apptus_results != null && apptus_results.getRecommendBasedOnProduct()!=null &&
						apptus_results.getRecommendBasedOnProduct().get(0).getProducts() != null){
					ArrayList<ProductInformation> recommendbooklist = apptusServiceHelper.operateOnApptusProduct(
							apptus_results.getRecommendBasedOnProduct().get(0).getProducts(),user, windowstart_end.get( 0 ),apiVersion,false);
					proddetails.setBooks(recommendbooklist);
				}else
					proddetails.setBooks( Collections.emptyList());
			}catch (Exception e) {
				logger.error("Error while fetching Recommend zone" + e.getMessage());
				e.printStackTrace();
			}
		}
		//String recommendurl = productService.getAptusUrlForRecommendBooks(aptusid);
		return proddetails;
	}
		
	public List<Object> getApptusBooksForBookGroupCriteria(BooksForBookGroupRequestBean reqBean, Double apiVersion,User user) throws Exception {		
		
		String url	=	null;
		boolean issortrequired = true;
		boolean isformatrequired = true;
		boolean isUrlRequired = true;
		boolean isUrlRequiredRecommend = false;
		boolean isUrlRequiredTopSeller = false;
		boolean overrideurl = false;
		boolean validBookGroupId = false;
		boolean isUrlRequiredUpcoming =false;
		//String countrycode="";
		GenericPanel	genericpanel = new GenericPanel();
		
		if( !StringUtils.isBlank( reqBean.getBookgroupid() ) && reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.MAIN_SERIES.getGroupid()) 
			&& reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.MAIN_SERIES.getGroupid()) )
		{
			String seriesname[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String name = seriesname[1];
			genericpanel.setAuthors_filter_New("series:'" + name + "'");
			//URLEncoder.encode(name,"UTF-8")+ "'" );			
			genericpanel.setSearch_attributes("series");
			genericpanel.setRelevance_sort("sequenceseries");
			issortrequired = false;
			validBookGroupId = true;			
		}
		else if( !StringUtils.isBlank( reqBean.getBookgroupid() ) && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()) 
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()))
		{
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist[1];
			genericpanel.setParent_category_filter( FilterUtils.buildCategoryId(user.getCountrycode(), "" + id ));	
			EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(id));
			if(cat !=null && StringUtils.isNotBlank(cat.getPrimarylanguage()))
			{
				String[] b = cat.getPrimarylanguage().split(",");
				String	language_filter_new  = "";
				if(b!= null && b.length>0){
					byte	count =0;
					for(String aut:b){						
						aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
						if(count ==0)
							language_filter_new = language_filter_new + "language:'"+ aut + "'";
						 else
							 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
						count++;
					 }
				 }
				if( !StringUtils.isBlank( language_filter_new ))
					genericpanel.setLanguage_filter(language_filter_new);
			}

			validBookGroupId = true;			
			if( !StringUtils.isBlank( reqBean.getSort() ) ){
				if(reqBean.getSort().contains("default")){					
					//String groupid = apptusServiceHelper.getSortForPromoteDePromote(user.getCountrycode(), cat.getSlugName());	
					String groupid = (cat !=null && cat.getApptus_sort_attrib()!=null)?cat.getApptus_sort_attrib():"";
					if(StringUtils.isBlank(groupid))
					 groupid = apptusServiceHelper.getSortForPromoteDePromote(user.getCountrycode(), cat.getSlugName());
					if(StringUtils.isBlank(groupid))
						groupid = apptusServiceHelper.getSortForPromoteDePromote(user.getCountrycode(), cat.getSlugName());
					if( !StringUtils.isBlank(groupid) )
						genericpanel.setRelevance_sort( groupid + " desc, relevance" );					
					else
						genericpanel.setRelevance_sort("relevance");
					issortrequired = false;
				}
			}
			else{
				//EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(id));				
				//String groupid = apptusServiceHelper.getSortForPromoteDePromote(user.getCountrycode(), cat.getSlugName());
				String groupid = (cat !=null && cat.getApptus_sort_attrib()!=null)?cat.getApptus_sort_attrib():"";
				if(StringUtils.isBlank(groupid))
					 groupid = apptusServiceHelper.getSortForPromoteDePromote(user.getCountrycode(), cat.getSlugName());
				if( !StringUtils.isBlank( groupid ))					
					genericpanel.setRelevance_sort( groupid + " desc, relevance" );
				else
					genericpanel.setRelevance_sort("relevance");	
				issortrequired = false;
			}
			
			String depromote = (cat !=null && cat.getApptus_sort_attrib()!=null)?cat.getApptus_sort_attrib():"";
			if(StringUtils.isNotBlank(depromote)){
				String authors_filter_new = " NOT "+ depromote+ ":'-1'";
				genericpanel.setAuthors_filter_New(authors_filter_new);
			 }
			
				
			
		}
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() ) && reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_Nyther.getGroupid()))
		{
			if( !StringUtils.isBlank( user.getCountrycode() ) && user.getCountrycode().equalsIgnoreCase("SE")){
				genericpanel.setBookid_filter_new(" NOT Nyheter_pos:['0','0']" );	
				genericpanel.setRelevance_sort( "Nyheter_pos" );				
			}else if(user.getCountrycode()!=null && !user.getCountrycode().equalsIgnoreCase("")){
				genericpanel.setBookid_filter_new(" NOT Nyheter"+"_"+user.getCountrycode()+"_pos:['0','0']");
				genericpanel.setRelevance_sort( "Nyheter"+"_"+user.getCountrycode()+"_pos");				
			}			 
			issortrequired = false;
			validBookGroupId = true;
		}
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() )
				&& reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_BookTips.getGroupid()))
		{
			if(!StringUtils.isBlank( user.getCountrycode() ) && user.getCountrycode().equalsIgnoreCase("SE")){
				genericpanel.setBookid_filter_new(" NOT vi_rekommenderar_pos:['0','0']" );	
				genericpanel.setRelevance_sort("vi_rekommenderar_pos");				
			}
			else if( !StringUtils.isBlank( user.getCountrycode() )){
				genericpanel.setBookid_filter_new( " NOT vi_rekommenderar"+"_"+user.getCountrycode()+"_pos:['0','0']");				
				genericpanel.setRelevance_sort( "vi_rekommenderar"+"_"+user.getCountrycode()+"_pos");				
			}
			issortrequired = false;
			validBookGroupId = true;
		}
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() )
				&& reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_Dynamic.getGroupid()))
		{
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist[2];
			genericpanel.setParent_category_filter( "listids:'L"+id+"'" );
			genericpanel.setRelevance_sort( "category_key('category_rank_values,L"+id+"') asc ");
			if(user.getProfile() != null && user.getProfile().getCategory() != null &&
					user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
			{ 
				EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));				
				if(kidCategory!= null && kidCategory.getSubCategories() !=null){
				   byte	count =0;
				   String	author_filter = "";				   
				   for(EbCategories subcategory:kidCategory.getSubCategories())
					{
						 if(count == 0)
							 author_filter = author_filter  + FilterUtils.buildCategoryId( user.getCountrycode(), "" + subcategory.getCategoryId());
						 else
							 author_filter = author_filter + " OR " + FilterUtils.buildCategoryId( user.getCountrycode(), "" + subcategory.getCategoryId());							 
						 count++;
					 }
				   if( !StringUtils.isBlank( author_filter ))
					   genericpanel.setAuthors_filter_New(author_filter);
				 }
			}			
			issortrequired = false;
			validBookGroupId = true;
		}
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() )
				&& reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RecommendBasedCustomer.getGroupid()))		
			isUrlRequiredRecommend = true;
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() )
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopSeller.getGroupid()))		
			isUrlRequiredTopSeller = true;
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() )
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_UPCOMING.getGroupid()))		
			isUrlRequiredUpcoming = true;
		else if(reqBean.getBookgroupid()!=null && 
				(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid())||
				 reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid())||
				 reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopB.getGroupid())
			))
		{
			int format =0;
			if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()))
				format =1;
			else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid())) 
				format=2;
			if( format > 0 ){
				genericpanel.setFormat_filter( "" + format );
				isformatrequired 	= false;
			}							
			issortrequired 		= false;
			if(user.getProfile() != null && user.getProfile().getCategory() != null &&
				user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
			{ 
				String	category_filter = apptusServiceHelper.getIncludeCategoryForApptusRequest( user.getCountrycode() );				
				if( !StringUtils.isBlank( category_filter ))
						genericpanel.setAuthors_filter_New( category_filter );				
			}else 
			{							
				String	authors_filter_new = apptusServiceHelper.getExcludeCategoryForApptusRequest( user.getCountrycode() );			
				if( StringUtils.isNotBlank( authors_filter_new ))
					genericpanel.setAuthors_filter_New(authors_filter_new);
			}	
			
			if((reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()) 
				&& reqBean.getType() != null && reqBean.getType()==2)
				|| (reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid()) 
				&& reqBean.getType() != null && reqBean.getType()==1))
			{
				overrideurl = true;
			}
			if(!StringUtils.isBlank( reqBean.getSort() ) ){
				
				if(reqBean.getSort().contains("default"))
				{
					if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()))
						genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 1)+" desc, relevance");
					else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid()))
						genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 2)+" desc, relevance" );
					else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopB.getGroupid()))
						genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0)+" desc, relevance" );
					issortrequired = false;
				}
				
				if(reqBean.getSort().contains("default")){
					if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()))
						genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 1) + " desc, relevance");		
					else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid()))
						genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 2) + " desc, relevance" );
					else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopB.getGroupid()))
						genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0) + " desc, relevance" );						
					issortrequired = false;
				}
			}else 
			{
				if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()))
					genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 1)+" desc, relevance" );
				else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid()))
					genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 2)+" desc, relevance" );
				else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopB.getGroupid()))
					genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0)+" desc, relevance" );
				
				if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()))
					genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 1) + " desc, relevance" );					
				else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid()))
					genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 2) +" desc, relevance" );
				else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopB.getGroupid()))
					genericpanel.setRelevance_sort( apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0) +" desc, relevance" );					
				issortrequired = false;
			}
			validBookGroupId = true;
			
			String depromote = "";
			if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopE.getGroupid()))
				depromote= apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 1);
			else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopA.getGroupid()))
				depromote= apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 2);
			else if(reqBean.getBookgroupid().equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopB.getGroupid()))
				depromote= apptusServiceHelper.getSortForPromoteDePromoteTopBooks(user.getCountrycode(), 0);
			
			if(StringUtils.isNotBlank(depromote)){
				String authors_filter_new = " NOT "+ depromote+ ":'-1'";
				genericpanel.setLanguage_filter(authors_filter_new);
			 }
			
		}
		//stl_ttc_12_TopE"
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() ) && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()) 
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()))
		{
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid());
			String idlist2[] = idlist[1].split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist2[0];
			genericpanel.setParent_category_filter( FilterUtils.buildCategoryId( user.getCountrycode(), "" + id));	
			EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(id));
			if(StringUtils.isNotBlank(cat.getPrimarylanguage()))
			{
				String[] b = cat.getPrimarylanguage().split(",");
				String	language_filter_new  = "";
				if(b!= null && b.length>0){
					byte	count =0;
					for(String aut:b){						
						aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
						if(count ==0)
							language_filter_new = language_filter_new + "language:'"+ aut + "'";
						 else
							 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
						count++;
					 }
				 }
				if( !StringUtils.isBlank( language_filter_new ))
					genericpanel.setLanguage_filter(language_filter_new);
			}
			validBookGroupId = true;
		}
		else if(!StringUtils.isBlank( reqBean.getBookgroupid() ) && 
				reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST.getGroupid()) 
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_LIST.getGroupid())
				&& (reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPE.getGroupid()) ||
					reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPA.getGroupid()) ||
					reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_Nyheter.getGroupid()) ))
		{
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_TIER_CATEGORY.getGroupid());
			String idlist2[] = idlist[1].split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist2[0];
			EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(id));
			genericpanel.setParent_category_filter( FilterUtils.buildCategoryId( user.getCountrycode(), "" + id) );					
			int format =0;
			if(reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPE.getGroupid()))
				format =1;
			else if(reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPA.getGroupid()))
				format=2;
			else if(reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPA.getGroupid()))
				isformatrequired = true;
			if(format > 0){
				genericpanel.setFormat_filter( "" + format );				
				isformatrequired = false;
			}else{
				genericpanel.setRelevance_sort("publisheddate desc");				
				issortrequired = false;
			}
			if((reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPE.getGroupid()) 
				&& reqBean.getType() != null && reqBean.getType()==2)
				|| (reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.GENERIC_TOPA.getGroupid()) 
				&& reqBean.getType() != null && reqBean.getType()==1))
			overrideurl = true;
			
			validBookGroupId = true;
			if(StringUtils.isNotBlank(cat.getPrimarylanguage()))
			{
				String[] b = cat.getPrimarylanguage().split(",");
				String	language_filter_new  = "";
				if(b!= null && b.length>0){
					byte	count =0;
					for(String aut:b){						
						aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
						if(count ==0)
							language_filter_new = language_filter_new + "language:'"+ aut + "'";
						 else
							 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
						count++;
					 }
				 }
				if( !StringUtils.isBlank( language_filter_new ))
					genericpanel.setLanguage_filter(language_filter_new);
			}
		}
		else if( !StringUtils.isBlank( reqBean.getBookgroupid() ) && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid())
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid()))
		{
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.RELATED_BOOK_GROUP.getGroupid());
			String idlist1[] = idlist[1].split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String bookid = idlist1[0];
			
			ProductDetails productDetails = productDao.productDetailsFromMongo(Integer.valueOf(bookid), "", user, true,user.getCountrycode());			
			ProductRelatedDetails prodInfo = null;
			if(productDetails != null)
			{
				if(productDetails.getProductType().getFormatNo()==ProductFormatEnum.E_BOOK.getFormatNo())
					prodInfo = productDetails.getEbookDetails();					
				else
					prodInfo = productDetails.getAudioDetails();													
			}
			if(user.getProfile() != null && user.getProfile().getCategory() != null &&
				user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
			{ 
				String	child_filter = apptusServiceHelper.getIncludeCategoryForApptusRequest( user.getCountrycode() );
				if(StringUtils.isNotBlank(child_filter))
					genericpanel.setChild_filter(child_filter);				
			}
			if(idlist1[1].contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP_SERIES.getGroupid()))
			{
				String seriesname = productDetails.getSeriesName();
				genericpanel.setSearch_attributes( "series" );
				genericpanel.setAuthors_filter_New( FilterUtils.buildSeriesFilter( seriesname ) );
				//NEX-4358 code changes
				//genericpanel.setBookid_filter_new( " NOT product_key:'"+prodInfo.getAptusid()+"'");
				genericpanel.setRelevance_sort("sequenceseries");				
				issortrequired = false;
			}
			
			else if(idlist1[1].contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP_AUTHORS.getGroupid()))
			{
				List<String> authors = new ArrayList<String>();
				if (productDetails.getProductType().getFormatNo() == ProductFormatEnum.AUDIO_BOOK.getFormatNo()){
					authors = productDetails.getAudioDetails().getAuthors();
				} else{
					authors = productDetails.getEbookDetails().getAuthors();
				}
				genericpanel.setSearch_attributes( "authors" );
				genericpanel.setBookid_filter_new( " NOT product_key:'"+prodInfo.getAptusid()+"'");				
				String	authors_filter_new  = "";
				if(authors!= null && authors.size()>0){
					byte	count =0;
					for(String aut:authors){						
						aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
						if(count ==0)
							authors_filter_new = authors_filter_new + "authors:'"+ aut + "'";
						 else
							authors_filter_new = authors_filter_new + " AND authors:'"+ aut + "'";							
						count++;
					 }
				 }
				if( !StringUtils.isBlank( authors_filter_new ))
					genericpanel.setAuthors_filter_New(authors_filter_new);
			}
			else if(idlist1[1].contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP_RECOMMEND.getGroupid()))
				isUrlRequired = false;						
			validBookGroupId = true;			
		}
		else if( !StringUtils.isBlank( reqBean.getBookgroupid() ) && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.SEARCH_GROUP.getGroupid())
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.SEARCH_GROUP.getGroupid()))
		{
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.SEARCH_GROUP.getGroupid());
			String idlist1[] = idlist[1].split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			
			 if(idlist[1].contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP_AUTHORS.getGroupid()))
			{
				genericpanel.setSearch_attributes( "authors,authorsreverse" );
				String	authors_filter_new  = "";
				if(StringUtils.isNotBlank(idlist1[1]))
				//authors_filter_new = authors_filter_new + "authors:'"+ idlist1[1] + "'";
				authors_filter_new = authors_filter_new + idlist1[1];
				if( !StringUtils.isBlank( authors_filter_new ))
					genericpanel.setSearchPhase(authors_filter_new);
			}
			 
			else if(idlist[1].contains(ApptusBookGroupsEnum.RELATED_BOOK_GROUP_NARRATORS.getGroupid()))
				{
					genericpanel.setSearch_attributes( "narrators,narratorsreverse" );
					String	authors_filter_new  = "";
					if(StringUtils.isNotBlank(idlist1[1]))
					//authors_filter_new = authors_filter_new + "narrators:'"+ idlist1[1] + "'";
					authors_filter_new = authors_filter_new + idlist1[1];
					if( !StringUtils.isBlank( authors_filter_new ))
						genericpanel.setSearchPhase(authors_filter_new);
				}
			 
			 validBookGroupId = true;	 
		
		}
		if(isformatrequired && reqBean.getType()!= null && reqBean.getType() >0 && reqBean.getType()<3)
		{
			try{
				ProductFormatEnum format = ProductFormatEnum.getFormat(reqBean.getType());
				genericpanel.setFormat_filter( "" + format.getFormatNo() );			
			}catch(Exception e){
				throw e;
			}
		}		
		if(issortrequired && !StringUtils.isBlank( reqBean.getSort() ) ){
			if(reqBean.getSort().contains("default"))
				genericpanel.setRelevance_sort("relevance");			
			else if(reqBean.getSort().contains("pubdate"))
				genericpanel.setRelevance_sort("publisheddate desc");			
			else if(reqBean.getSort().contains("title"))			
				genericpanel.setRelevance_sort("title");
			else if(reqBean.getSort().contains("ratingsranking"))			
				genericpanel.setRelevance_sort("ratingsranking desc");
			else 		
				genericpanel.setRelevance_sort("relevance");		
		}else if(issortrequired && StringUtils.isBlank( reqBean.getSort() ))
			genericpanel.setRelevance_sort("relevance");
		
		List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd((StringUtils.isBlank( reqBean.getPagenumber()))?0:Integer.valueOf( reqBean.getPagenumber() ), reqBean.getRows());
		genericpanel.setWindow_first( "" + windowstart_end.get(0));
		genericpanel.setWindow_last( "" + windowstart_end.get(1) );
		genericpanel.setMarket(apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
		genericpanel.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
		genericpanel.setCustomerKey( apptusServiceHelper.getApptusCustomerKey(user));				
		if(user.getUserCountry()!=null && !user.getUserCountry().equalsIgnoreCase(""))
		{					
			genericpanel.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));			
			genericpanel.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));			
		}
		if(user.getExcludePubs()!=null)
			genericpanel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()));		
		if(user.getCountrycode()!=null && !user.getCountrycode().equalsIgnoreCase(""))
			genericpanel.setMarket_filter(user.getCountrycode() );
				
		Map<String , Integer> map = InMemoryCache.subAgeLimits;
		genericpanel.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType())) );
		
		if(!isUrlRequired){
			url = ApptusBookGroupsEnum.RELATED_RECOMMEND_URL_TO_BE_FIRED.getGroupid();
			genericpanel = null;
		}
		if(overrideurl){
			url = ApptusBookGroupsEnum.EMPTY_DATA_TO_BE_RETURNED_RESPONSE.getGroupid();
			genericpanel = null;
		}
		if(!validBookGroupId){
			url = ApptusBookGroupsEnum.NOT_VALID_BOOK_GROUP_ID.getGroupid();
			genericpanel = null;
		}
		if(isUrlRequiredRecommend){
			url = ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED.getGroupid();
			genericpanel = null;
		}		
		if(isUrlRequiredTopSeller){
			url = ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopSeller.getGroupid();
			genericpanel = null;
		}
		if(isUrlRequiredUpcoming){
			url = ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_UPCOMING.getGroupid();
			genericpanel = null;
		}	
		//if(logger.isInfoEnabled()) logger.info("Apptus URL :: " + url);		
		return Arrays.asList( genericpanel, url);
	}
	
	
	public GenericPanelAppPreviewRequest getApptusUrlForNestPreview(String bookgroupid,String pagenumber, Integer rows, Integer type,String countrycode)
	{
		GenericPanelAppPreviewRequest	panel = new GenericPanelAppPreviewRequest();
		if(bookgroupid!=null 
				&& bookgroupid.equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_Nyther.getGroupid()))
		{
			 if(!StringUtils.isBlank( countrycode ) && countrycode.equalsIgnoreCase("SE"))
			 {
				 panel.setBookid_filter_new( " NOT " + FilterUtils.buildNyheterFilter(null, "0","0") );	
				 panel.setRelevance_sort("Nyheter_pos");				 
			 }else if( !StringUtils.isBlank( countrycode ) )
			 {
				 panel.setBookid_filter_new( " NOT " + FilterUtils.buildNyheterFilter(countrycode, "0","0") );	
				 panel.setRelevance_sort("Nyheter_"+countrycode+"_pos");					
			 }			
		}else if(bookgroupid!=null 
				&& bookgroupid.equalsIgnoreCase(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_BookTips.getGroupid()))
		{
			 if(!StringUtils.isBlank( countrycode ) && countrycode.equalsIgnoreCase("SE"))
			 {
				 panel.setBookid_filter_new( " NOT " + FilterUtils.build_Vi_RekommenderFilter(null, "0","0"));				 
				 panel.setRelevance_sort("vi_rekommenderar_pos");
			 }
			 else if( !StringUtils.isBlank( countrycode ) ){
				 panel.setBookid_filter_new( " NOT " + FilterUtils.build_Vi_RekommenderFilter(countrycode, "0","0"));				 
				 panel.setRelevance_sort("vi_rekommenderar_"+countrycode+"_pos");
			 }
		}else if(bookgroupid!=null 
				&& bookgroupid.startsWith(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_Dynamic.getGroupid()))
		{
			String idlist[] = bookgroupid.split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist[2];
			panel.setParent_category_filter("listids:'L"+id+"'" );
			panel.setRelevance_sort("category_key('category_rank_values,L"+id+"') asc ");
		}
		Integer pagesize = rows;
		List<Integer>	windowattr	= apptusServiceHelper.getWindowStartAndEnd( ( !StringUtils.isBlank( pagenumber ) && Integer.valueOf(pagenumber)>=0)?Integer.valueOf(pagenumber):0, pagesize);
		panel.setWindow_first( "" + windowattr.get( 0 ));
		panel.setWindow_last( "" + windowattr.get( 1 ) );
		panel.setSubscription_filter( FilterUtils.buildSubscriptionFilter("1","" + type));
		if( !StringUtils.isBlank( countrycode ))
			panel.setMarket_filter(countrycode);			
		return panel;
	}
	
	public SearchWithFacetsApptusResponseWrapper getBooksFromSearchWithFactes(SearchJsonApptusRequestBean reqBean, Double apiVersion,User user)
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: start :: BookGroupBooksWrapper ::: "+reqBean);		
		SearchRequest searchRequest = new SearchRequest();		
		String	qStr	=	reqBean.getQuery();
		if( qStr != null ){			
			reqBean.setQuery(Book2GoUtil.getStripStartAndEndOfString( qStr ));
		}
		searchRequest.setQuery(reqBean.getQuery());
		Integer pagenumber = Book2GoUtil.parseString(reqBean.getPagenumber());
		Integer rows = Book2GoUtil.parseString(reqBean.getPagesize());
		ProductFormatEnum productformat = ProductFormatEnum.getFormat(reqBean.getType());
		searchRequest.setPageNumber(pagenumber != null ? pagenumber.intValue() : 0);
		searchRequest.setSortOn(reqBean.getSort());
		searchRequest.setWindowSize(rows != null ? rows.intValue() : 12);
		searchRequest.setProductFormat(productformat);
		searchRequest.setIncludenotallowedbooks(reqBean.getIncludenotallowedbooks()!=null?reqBean.getIncludenotallowedbooks():"");
		SearchResultWithFacets result = new SearchResultWithFacets();
		Profile profile = user.getProfile();
		if(null != profile && null != profile.getCategory()){			
			if(StringUtils.equalsIgnoreCase(profile.getCategory(), PROFILE_CATEGORIES.KIDS.name()))		
					result =  getSilverSearchResultsWithFacetsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.KIDS.name(),user,reqBean);
			else	
					result =  getSilverSearchResultsWithFacetsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user,reqBean);			
		}else			
			result =  getSilverSearchResultsWithFacetsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user,reqBean);
		
		SearchWithFacetsApptusResponseWrapper wrapper = new SearchWithFacetsApptusResponseWrapper();
		if(result!=null){
			wrapper.setBooks(result.getProductlist());
			wrapper.setBookCount(result.getResultSize());
			wrapper.setHigherBookCount(result.getHigherresultSize());
			wrapper.setApiVersion(reqBean.getApiVersion());
			//wrapper.setFacets(result.getFacets());
		}
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: Stop :: BookGroupBooksWrapper ::: "+result.getResultSize());
		return wrapper;
	}
	
	
	public SearchWithFacetsApptusResponseWrapper getOnlyFactes(SearchJsonApptusRequestBean reqBean, Double apiVersion,User user)
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: start :: BookGroupBooksWrapper ::: "+reqBean);		
		SearchRequest searchRequest = new SearchRequest();		
		String	qStr	=	reqBean.getQuery();
		if( qStr != null ){			
			reqBean.setQuery(Book2GoUtil.getStripStartAndEndOfString( qStr ));
		}
		searchRequest.setQuery(reqBean.getQuery());
		Integer pagenumber = Book2GoUtil.parseString(reqBean.getPagenumber());
		Integer rows = Book2GoUtil.parseString(reqBean.getPagesize());
		ProductFormatEnum productformat = ProductFormatEnum.getFormat(reqBean.getType());
		searchRequest.setPageNumber(pagenumber != null ? pagenumber.intValue() : 0);
		searchRequest.setSortOn(reqBean.getSort());
		searchRequest.setWindowSize(rows != null ? rows.intValue() : 12);
		searchRequest.setProductFormat(productformat);
		searchRequest.setIncludenotallowedbooks(reqBean.getIncludenotallowedbooks()!=null?reqBean.getIncludenotallowedbooks():"");
		SearchResultWithFacets result = new SearchResultWithFacets();
		Profile profile = user.getProfile();
		if(null != profile && null != profile.getCategory()){			
			if(StringUtils.equalsIgnoreCase(profile.getCategory(), PROFILE_CATEGORIES.KIDS.name()))		
					result =  getOnlyFacetsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.KIDS.name(),user,reqBean);
			else	
					result =  getOnlyFacetsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user,reqBean);			
		}else			
			result =  getOnlyFacetsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user,reqBean);
		
		SearchWithFacetsApptusResponseWrapper wrapper = new SearchWithFacetsApptusResponseWrapper();
		if(result!=null){
			//wrapper.setBooks(result.getProductlist());
			wrapper.setBookCount(result.getResultSize());
			//wrapper.setHigherBookCount(result.getHigherresultSize());
			wrapper.setApiVersion(reqBean.getApiVersion());
			wrapper.setFacets(result.getFacets());
		}
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: Stop :: BookGroupBooksWrapper ::: "+result.getResultSize());
		return wrapper;
	}
	
	
	public BooksForBookGroupsApptusResponseWrapper getBooksFromKeywordSearchDevice(SearchJsonApptusRequestBean reqBean, Double apiVersion,User user)
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: start :: BookGroupBooksWrapper ::: "+reqBean);		
		SearchRequest searchRequest = new SearchRequest();		
		String	qStr	=	reqBean.getQuery();
		if( qStr != null ){			
			reqBean.setQuery(Book2GoUtil.getStripStartAndEndOfString( qStr ));
		}
		searchRequest.setQuery(reqBean.getQuery());
		Integer pagenumber = Book2GoUtil.parseString(reqBean.getPagenumber());
		Integer rows = Book2GoUtil.parseString(reqBean.getPagesize());
		ProductFormatEnum productformat = ProductFormatEnum.getFormat(reqBean.getType());
		searchRequest.setPageNumber(pagenumber != null ? pagenumber.intValue() : 0);
		searchRequest.setSortOn(reqBean.getSort());
		searchRequest.setWindowSize(rows != null ? rows.intValue() : 12);
		searchRequest.setProductFormat(productformat);
		searchRequest.setIncludenotallowedbooks(reqBean.getIncludenotallowedbooks()!=null?reqBean.getIncludenotallowedbooks():"");
		SearchResult result = new SearchResult();
		Profile profile = user.getProfile();
		if(null != profile && null != profile.getCategory()){			
			if(StringUtils.equalsIgnoreCase(profile.getCategory(), PROFILE_CATEGORIES.KIDS.name()))		
					result =  getSilverSearchResultsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.KIDS.name(),user);
			else	
					result =  getSilverSearchResultsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user);			
		}else			
			result =  getSilverSearchResultsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user);
		
		BooksForBookGroupsApptusResponseWrapper wrapper = new BooksForBookGroupsApptusResponseWrapper();
		if(result!=null){
			wrapper.setBooks(result.getProductlist());
			wrapper.setBookCount(result.getResultSize());
			wrapper.setHigherBookCount(result.getHigherresultSize());
			wrapper.setApiVersion(reqBean.getApiVersion());
		}
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: Stop :: BookGroupBooksWrapper ::: "+result.getResultSize());
		return wrapper;
	}
	
	public AdvancedSearchWrapper getAdvancedSearch(SearchJsonApptusRequestBean reqBean, Double apiVersion,User user)
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: start :: BookGroupBooksWrapper ::: "+reqBean);		
		SearchRequest searchRequest = new SearchRequest();	
		AdvancedSearchWrapper wrapper = new AdvancedSearchWrapper();
		String	qStr	=	reqBean.getQuery();
		if( qStr != null ){			
			reqBean.setQuery(Book2GoUtil.getStripStartAndEndOfString( qStr ));
		}
		searchRequest.setQuery(reqBean.getQuery());
		Integer pagenumber = Book2GoUtil.parseString(reqBean.getPagenumber());
		Integer rows = Book2GoUtil.parseString(reqBean.getPagesize());
		ProductFormatEnum productformat = ProductFormatEnum.getFormat(reqBean.getType());
		searchRequest.setPageNumber(pagenumber != null ? pagenumber.intValue() : 0);
		searchRequest.setSortOn(reqBean.getSort());
		searchRequest.setWindowSize(rows != null ? rows.intValue() : 12);
		searchRequest.setProductFormat(productformat);
		//searchRequest.setIncludenotallowedbooks(reqBean.getIncludenotallowedbooks()!=null?reqBean.getIncludenotallowedbooks():"");

		Profile profile = user.getProfile();
		if(null != profile && null != profile.getCategory()){			
			if(StringUtils.equalsIgnoreCase(profile.getCategory(), PROFILE_CATEGORIES.KIDS.name()))		
				wrapper =  getAdvancedSearchResultsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.KIDS.name(),user);
			else	
				wrapper =  getAdvancedSearchResultsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user);			
		}else			
			wrapper =  getAdvancedSearchResultsForAptus(searchRequest,apiVersion,PROFILE_CATEGORIES.ALL.name(),user);
		
		
		wrapper.setApiVersion(reqBean.getApiVersion());
		
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getAdvancedSearch :: Stop :: AdvancedSearchWrapper ::: "+wrapper);
		return wrapper;
	}
	
	
	public String CallDidYouMean(String query, String profileCategory) throws Exception
	{
		String pubDetails1 = null; 
		if(profileCategory.equalsIgnoreCase(PROFILE_CATEGORIES.ALL.name()))
			pubDetails1 = aptusdidyoumeanservice.search(new DidYouMeanPanel(query));					
		else
			pubDetails1	= aptusdidyoumeanforkidsservice.search(new DidYouMeanForKidsPanel(query));			
		 return pubDetails1;
	}
	
	
	public void reportClicksToApptus(ReportClickApptusRequestBean reqBean, Double apiVersion, User appuserinfo)
	{
		if (logger.isTraceEnabled())logger.trace("ApptusApiService  :: getBooksFromKeywordSearchDevice :: start :: BookGroupBooksWrapper ::: "+reqBean);		
		Query<ProductData> query = mongoDBUtilMorphia.getMorphiaDatastoreForProduct().createQuery(ProductData.class);
		query.criteria("_id").equal(Integer.valueOf(reqBean.getId()));
		query.retrievedFields(true, "aptusid", "isbn");
		ProductData pd = query.get();
						
		if( StringUtils.isNotBlank( reqBean.getEsalesticket() ) 
			&& appuserinfo !=null && appuserinfo.getProfile() != null 
			&& appuserinfo.getProfile().getProfileid()>0 )									
			notificationForApptus.CallAptusNotificationApp(reqBean.getEsalesticket(),appuserinfo);				
		else
			notificationForApptus.CallAptusNotificationAppWithoutEsalesticket(appuserinfo, pd.getAptusid(), pd.getIsbn());	
		
		//reportClicksForBlueShift(pd,appuserinfo);
	}
	
	
	public void reportClicksForBlueShift(ProductData pd, User user)
	{
		BlueShiftRequestBean	request = null;
		try{
			//Blusshift code notofication Deba
			if(user!=null && pd!=null 
				&& user.getCustomerid()!=null && StringUtils.isNotBlank( pd.getIsbn() ) ){
				request = new BlueShiftRequestBean(user.getCustomerid().toString(), pd.getIsbn(), applicationpropconfig.getBlueshiftView());
				publisher.publishEvent(new NotificationEvent(NotificationEnum.BLUE_SHIFT, request));								
			}			
		}catch(Exception e){
			logger.error("An error occured while sending notification to Buleshit with data -->" + request, e);
		}
	}
	
	public SearchResult getSilverSearchResultsForAptus(SearchRequest externalReq, Double apiVersion, String profileCategory,User user)
	{
		SearchResult searchResult = null;
		SearchPanel panelrequest = null;
		AptusSecondSilverSearch pubDetails = null;
		if (StringUtils.isNotBlank(externalReq.getQuery()))
		{
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			 try {
				 int window = externalReq.getWindowSize() == null?12:externalReq.getWindowSize();				
				 int start = ((externalReq.getPageNumber() == null ?(1*window):((externalReq.getPageNumber()+1)*window)) - window) +1;
				 panelrequest = getUrlForAptusAppSilverSearch(externalReq, map, apiVersion, profileCategory, user);				   				 
				 pubDetails 	= aptussearchservice.search(panelrequest);				  
				 if(pubDetails != null && ( pubDetails.getSearchHits() != null && pubDetails.getSearchHits().size() > 0)
					||( pubDetails.getSearchHitCountHigher().size() > 0 && pubDetails.getSearchHitCountHigher().get(0).getCount() > 0) )
				 {
					 searchResult = new SearchResult();						
					 if(null != pubDetails.getSearchHits())
					 {							
						 ArrayList<ProductInformation> productlist = apptusServiceHelper.operateOnApptusProduct(pubDetails.getSearchHits().get(0).getProducts(), user, start, apiVersion,false);
						 //This code commented, to make use of "apptusServiceHelper.operateOnApptusProduct()" method
						 //ArrayList<ProductInformation> productlist = operateOnAptusSecondSilverSearch(pubDetails,externalReq,apiVersion,user);
						 searchResult.setProductlist(productlist);
					 }						
					 searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());
					 searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount());
				}else
				{
					String didYouMean = CallDidYouMean(externalReq.getQuery(),profileCategory);
					if( StringUtils.isNotBlank( didYouMean ) )
					{							
						externalReq.setQuery( didYouMean );
						panelrequest = getUrlForAptusAppSilverSearch(externalReq,map,apiVersion,profileCategory,user);
						pubDetails = aptussearchservice.search(panelrequest);							
						if(pubDetails !=null && pubDetails.getSearchHits() != null && pubDetails.getSearchHits().size() > 0 ||
							(pubDetails !=null && pubDetails.getSearchHitCountHigher().size()>0 && pubDetails.getSearchHitCountHigher().get(0).getCount() >0))
						{
							searchResult = new SearchResult();	
							if(null != pubDetails.getSearchHits())
							 {							
								 ArrayList<ProductInformation> productlist = apptusServiceHelper.operateOnApptusProduct(pubDetails.getSearchHits().get(0).getProducts(), user, start, apiVersion,false);
								 //This code commented, to make use of "apptusServiceHelper.operateOnApptusProduct()" method
								 //ArrayList<ProductInformation> productlist = operateOnAptusSecondSilverSearch(pubDetails,externalReq,apiVersion,user);
								 searchResult.setProductlist(productlist);
							 }	
							searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());							
							searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount());
						}
						else if(pubDetails !=null && pubDetails.getSearchHits() == null && pubDetails.getSearchHitCount().get(0).count > 0 ||
									(pubDetails !=null && pubDetails.getSearchHitCountHigher().size()>0 && pubDetails.getSearchHitCountHigher().get(0).getCount() >0))
						{
							searchResult = new SearchResult();
							searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());
							searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount());
						}
						else						
							searchResult = new SearchResult();							
					}
					else					
						searchResult = new SearchResult();						
				}
				pubDetails=null;
			} catch (JsonParseException e) {			
				e.printStackTrace();
			} catch (JsonMappingException e) {			
				e.printStackTrace();
			} catch (IOException e) {			
				e.printStackTrace();
			}catch (Exception e) {			
				e.printStackTrace();
			}
		} else
			searchResult = new SearchResult();						
		return searchResult;
	}
	
	
	public  List<FacetList> operateonFacets(List<Facet> facets, User user)
	{
		List<Facet> newdata = new ArrayList<Facet>();
		for(Facet data:facets)
		{
			Facet datafacet = new Facet();
			List<FacetList> newfacetlist = new ArrayList<FacetList>();
			for(FacetList datalist:data.getFacetList())
			{
				if(datalist.getAttribute().startsWith("categoryids"))
				{
					FacetList datalistnew = new FacetList();
					datalistnew.setAttribute("categoryids");
					datalistnew.setIsRange(datalist.isIsRange());
					List<Value> val = new ArrayList<Value> ();
					for(Value value: datalist.getValues())
					{
						if(!value.getText().equalsIgnoreCase("root"))
						{
							EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(value.getText()));
							if(cat != null && cat.getParentCatId() != null && cat.getParentCatId().intValue() == 0)
							{
								value.setCategoryname(cat.getCategoryName());
								val.add(value);
							}
						}
					}
					datalistnew.setValues(val);
					if(applicationpropconfig.getPrimaryfacetslist().contains(datalistnew.getAttribute()))
						datalistnew.setPrimaryfacet(Boolean.TRUE);
					else
						datalistnew.setPrimaryfacet(Boolean.FALSE);
					if(val.size()>0)
					newfacetlist.add(datalistnew);
					
				}
				else
				{
					if(applicationpropconfig.getPrimaryfacetslist().contains(datalist.getAttribute()))
						datalist.setPrimaryfacet(Boolean.TRUE);
					else
						datalist.setPrimaryfacet(Boolean.FALSE);
					newfacetlist.add(datalist);
				}
				
			}
			datafacet.setFacetList(newfacetlist);
			newdata.add(datafacet);
		}
		
		
		return newdata.get(0)!=null && newdata.get(0).getFacetList()!=null? newdata.get(0).getFacetList():new ArrayList<FacetList>();
		
	}
	
	public SearchResultWithFacets getSilverSearchResultsWithFacetsForAptus(SearchRequest externalReq, Double apiVersion, String profileCategory,User user
			,SearchJsonApptusRequestBean reqbean)
	{
		SearchResultWithFacets searchResult = null;
		SearchPanel panelrequest = null;
		AptusSecondSilverSearchWithFacets pubDetails = null;
		if (StringUtils.isNotBlank(externalReq.getQuery()))
		{
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			 try {
				 int window = externalReq.getWindowSize() == null?12:externalReq.getWindowSize();				
				 int start = ((externalReq.getPageNumber() == null ?(1*window):((externalReq.getPageNumber()+1)*window)) - window) +1;
				 panelrequest = getUrlForAptusAppSilverSearch(externalReq, map, apiVersion, profileCategory, user);	
				 
				 //write code to add facets arguments.
				 String facetsfilter = getfacetsfilter( reqbean,  user);
				 if(StringUtils.isNotBlank(facetsfilter))
				  panelrequest.setFacets(facetsfilter);
				 
				 pubDetails 	= aptussearchfacetsservice.search(panelrequest);				  
				 if(pubDetails != null && ( pubDetails.getSearchHits() != null && pubDetails.getSearchHits().size() > 0)
					||( pubDetails.getSearchHitCountHigher().size() > 0 && pubDetails.getSearchHitCountHigher().get(0).getCount() > 0) )
				 {
					 searchResult = new SearchResultWithFacets();						
					 if(null != pubDetails.getSearchHits())
					 {							
						 ArrayList<ProductInformation> productlist = apptusServiceHelper.operateOnApptusProduct(
								 pubDetails.getSearchHits().get(0).getProducts(), user, start, apiVersion,false);
						 //This code commented, to make use of "apptusServiceHelper.operateOnApptusProduct()" method
						 //ArrayList<ProductInformation> productlist = operateOnAptusSecondSilverSearch(pubDetails,externalReq,apiVersion,user);
						 searchResult.setProductlist(productlist);
					 }						
					 searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());
					 searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount());
					 /*if(pubDetails.getFacets()!=null)
					 searchResult.setFacets(operateonFacets(pubDetails.getFacets(),user));*/
				}else
				{
					/*String didYouMean = CallDidYouMean(externalReq.getQuery(),profileCategory);
					if( StringUtils.isNotBlank( didYouMean ) )
					{							
						externalReq.setQuery( didYouMean );
						panelrequest = getUrlForAptusAppSilverSearch(externalReq,map,apiVersion,profileCategory,user);
						pubDetails = aptussearchfacetsservice.search(panelrequest);							
						if(pubDetails !=null && pubDetails.getSearchHits() != null && pubDetails.getSearchHits().size() > 0 ||
							(pubDetails !=null && pubDetails.getSearchHitCountHigher().size()>0 && pubDetails.getSearchHitCountHigher().get(0).getCount() >0))
						{
							searchResult = new SearchResultWithFacets();	
							if(null != pubDetails.getSearchHits())
							 {							
								 ArrayList<ProductInformation> productlist = apptusServiceHelper.
										 operateOnApptusProduct(pubDetails.getSearchHits().get(0).getProducts(),
												 user, start, apiVersion,false);
								 //This code commented, to make use of "apptusServiceHelper.operateOnApptusProduct()" method
								 //ArrayList<ProductInformation> productlist = operateOnAptusSecondSilverSearch(pubDetails,externalReq,apiVersion,user);
								 searchResult.setProductlist(productlist);
							 }	
							searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());							
							searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount());
							 if(pubDetails.getFacets()!=null)
								 searchResult.setFacets(operateonFacets(pubDetails.getFacets(),user));
						}
						else if(pubDetails !=null && pubDetails.getSearchHits() == null && pubDetails.getSearchHitCount().get(0).count > 0 ||
									(pubDetails !=null && pubDetails.getSearchHitCountHigher().size()>0 && pubDetails.getSearchHitCountHigher().get(0).getCount() >0))
						{
							searchResult = new SearchResultWithFacets();
							searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount());
							searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount());
						}
						else						
							searchResult = new SearchResultWithFacets();							
					}
					else					*/
						searchResult = new SearchResultWithFacets();
						 if(pubDetails !=null && pubDetails.getSearchHitCount() != null && 
								  pubDetails.getSearchHitCount().get(0) !=null)
						searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount()>0?
								  pubDetails.getSearchHitCount().get(0).getCount():0);
						 if(pubDetails !=null && pubDetails.getSearchHitCountHigher() != null && 
								  pubDetails.getSearchHitCountHigher().get(0) !=null)
						searchResult.setHigherresultSize(pubDetails.getSearchHitCountHigher().get(0).getCount()>0?
								  pubDetails.getSearchHitCountHigher().get(0).getCount():0);
				}
				pubDetails=null;
			} /*catch (JsonParseException e) {			
				e.printStackTrace();
			} catch (JsonMappingException e) {			
				e.printStackTrace();
			} catch (IOException e) {			
				e.printStackTrace();
			}*/catch (Exception e) {			
				e.printStackTrace();
			}
		} else
			searchResult = new SearchResultWithFacets();						
		return searchResult;
	}
	
	
	public SearchResultWithFacets getOnlyFacetsForAptus(SearchRequest externalReq, Double apiVersion, String profileCategory,User user
			,SearchJsonApptusRequestBean reqbean)
	{
		SearchResultWithFacets searchResult = new SearchResultWithFacets();
		SearchPanel panelrequest = null;
		AptusSecondSilverSearchWithFacets pubDetails = null;
		if (StringUtils.isNotBlank(externalReq.getQuery()))
		{
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			 try {
				 int window = externalReq.getWindowSize() == null?12:externalReq.getWindowSize();				
				 int start = ((externalReq.getPageNumber() == null ?(1*window):((externalReq.getPageNumber()+1)*window)) - window) +1;
				 panelrequest = getUrlForAptusAppSilverSearch(externalReq, map, apiVersion, profileCategory, user);	
				 
				 //write code to add facets arguments.
				 String facetsfilter = getfacetsfilter( reqbean,  user);
				 if(StringUtils.isNotBlank(facetsfilter))
				  panelrequest.setFacets(facetsfilter);
				 
				 	pubDetails 	= aptusOnlyFacetsService.search(panelrequest);				  
				 
					 if(pubDetails!=null && pubDetails.getFacets()!=null)
					 {
					  searchResult.setFacets(operateonFacets(pubDetails.getFacets(),user));
					  if(pubDetails !=null && pubDetails.getSearchHitCount() != null && 
							  pubDetails.getSearchHitCount().get(0) !=null)
					  searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount()>0?
							  pubDetails.getSearchHitCount().get(0).getCount():0);
					 }
					else					
					 {
						searchResult = new SearchResultWithFacets();	
						searchResult.setFacets(new ArrayList<FacetList>());
						if(pubDetails !=null && pubDetails.getSearchHitCount() != null && 
								  pubDetails.getSearchHitCount().get(0) !=null)
						  searchResult.setResultSize(pubDetails.getSearchHitCount().get(0).getCount()>0?
								  pubDetails.getSearchHitCount().get(0).getCount():0);
					 }
				
				pubDetails=null;
			
			}catch (Exception e) {			
				e.printStackTrace();
			}
		} else
			searchResult = new SearchResultWithFacets();						
		return searchResult;
	}
	
	
	
	public String getfacetsfilter(SearchJsonApptusRequestBean reqbean, User user)
	{
		 String facetsfilter = "";
		 if(StringUtils.isNotBlank(reqbean.getLanguage_facet()))
		 {
			 String a[] = reqbean.getLanguage_facet().split(",");
			 int count =0;
			 for(String data:a)
			 {
				 if(StringUtils.isNotBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + ", language:"+data;
				 }
				 else if(StringUtils.isBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + "language:"+data;
				 }
				 else
				 {
					 facetsfilter = facetsfilter + "|"+data;
				 }
				 count++;
			 }
		 }
		 
		 if(StringUtils.isNotBlank(reqbean.getAvgrate_Min_facet()) && StringUtils.isNotBlank(reqbean.getAvgrate_Max_facet()) )
		 {
			 if(StringUtils.isNotBlank(facetsfilter))
			 {
				 facetsfilter = facetsfilter + ", avgrate:["+reqbean.getAvgrate_Min_facet()+","+reqbean.getAvgrate_Max_facet()+"]";
			 }
			 else
			 {
				 facetsfilter = facetsfilter + "avgrate:["+reqbean.getAvgrate_Min_facet()+","+reqbean.getAvgrate_Max_facet()+"]";
			 }
		 }
		 
		 if(StringUtils.isNotBlank(reqbean.getCategoryids_facet()))
		 {
			 String a[] = reqbean.getCategoryids_facet().split(",");
			 int count =0;
			 for(String data:a)
			 {
				 if(StringUtils.isNotBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + ", categoryids_"+user.getCountrycode()+"_f:"+data;
				 }
				 else if(StringUtils.isBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + "categoryids_"+user.getCountrycode()+"_f:"+data;
				 }
				 else
				 {
					 facetsfilter = facetsfilter + "|"+data;
				 }
				 count++;
			 }
		 }
		 
		 
		 if(StringUtils.isNotBlank(reqbean.getSeriesAsit_facet()))
		 {
			 String a[] = reqbean.getSeriesAsit_facet().split(",");
			 int count =0;
			 for(String data:a)
			 {
				 if(data!=null)
					 data = data.replace(":", "\\:");
				 
				 if(StringUtils.isNotBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + ", seriesAsit:"+data;
				 }
				 else if(StringUtils.isBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + "seriesAsit:"+data;
				 }
				 else
				 {
					 facetsfilter = facetsfilter + "|"+data;
				 }
				 count++;
			 }
		 }
		 
		 
		 if(StringUtils.isNotBlank(reqbean.getAuthors_facet()))
		 {
			 String a[] = reqbean.getAuthors_facet().split(",");
			 int count =0;
			 for(String data:a)
			 {
				 if(StringUtils.isNotBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + ", authors:"+data;
				 }
				 else if(StringUtils.isBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + "authors:"+data;
				 }
				 else
				 {
					 facetsfilter = facetsfilter + "|"+data;
				 }
				 count++;
			 }
		 }
		 
		 if(StringUtils.isNotBlank(reqbean.getFormattype_facet()))
		 {
			 String a[] = reqbean.getFormattype_facet().split(",");
			 int count =0;
			 for(String data:a)
			 {
				 if(StringUtils.isNotBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + ", formattype:"+data;
				 }
				 else if(StringUtils.isBlank(facetsfilter) && count ==0)
				 {
					 facetsfilter = facetsfilter + "formattype:"+data;
				 }
				 else
				 {
					 facetsfilter = facetsfilter + "|"+data;
				 }
				 count++;
			 }
		 }
		 
		 return facetsfilter;
	}
		
	public SearchPanel getUrlForAptusAppSilverSearch(SearchRequest externalReq,Map<String , Integer> map, Double apiVersion,  String profileCategory, 
												User user)
	{		
		List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd( externalReq.getPageNumber(), externalReq.getWindowSize() );
		SearchPanel	searchpanel = new SearchPanel("" + windowstart_end.get(0), "" + windowstart_end.get(1));		
		searchpanel.setSearchPhase( externalReq.getQuery() );
		if(user != null && !StringUtils.isBlank( user.getUserCountry() ) )
		{					
			searchpanel.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));			
			searchpanel.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));
		}
		if(user !=null && user.getExcludePubs()!=null )
			searchpanel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()));
		if(!StringUtils.isBlank( user.getCountrycode() ))			
			searchpanel.setMarket_filter( user.getCountrycode() );
		if(StringUtils.equalsAnyIgnoreCase(profileCategory, PROFILE_CATEGORIES.KIDS.name()))
		{			
			String	category_filter = apptusServiceHelper.getParentCategoryFilter(user);
			if( !StringUtils.isBlank(category_filter))
			searchpanel.setParent_category_filter(category_filter);
		 }		 		
		 if(externalReq.getProductFormat() != null && !externalReq.getProductFormat().equals(ProductFormatEnum.UN_DEFINED) 
			&& (externalReq.getProductFormat().getFormatNo() >0 &&  externalReq.getProductFormat().getFormatNo() <=2))		 
			 searchpanel.setFormat_filter("" + externalReq.getProductFormat().getFormatNo());			
		 searchpanel.setSortBy( FilterUtils.buildSortByFilter(externalReq.getSortOn() ));			
		 
		 List<String>	subscriptions = apptusServiceHelper.getSubscriptionFilterApptusAttributeName(externalReq, map, user);
		 if( !StringUtils.isBlank( subscriptions.get(0) ) )
			 searchpanel.setSubscription_filter( subscriptions.get(0) );
		 if( !StringUtils.isBlank( subscriptions.get(1) ) )
			 searchpanel.setSubscription_filter_1( subscriptions.get(1) );
		 
		 searchpanel.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
		 
		 if(user != null && user.getProfile() != null && user.getProfile().getProfileid() != null)
		 {
			 searchpanel.setSessionKey( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): UUID.randomUUID().toString() );
			 searchpanel.setCustomerKey( "" + user.getProfile().getProfileid() );			 
		 }
		 else if(user != null && user.getCustomerid() != null)
		 {
			 searchpanel.setSessionKey( ( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): "" + user.getCustomerid() ) );
			 searchpanel.setCustomerKey( "" + user.getCustomerid() );			 
		 }
		 return searchpanel;
	}
	
	
	
	
	public AdvancedSearchWrapper getAdvancedSearchResultsForAptus(SearchRequest externalReq, Double apiVersion, String profileCategory,User user)
	{
		AdvancedSearchZone panelrequest = null;
		AdvancedSearch pubDetails = null;
		AdvancedSearchWrapper wrapper = new AdvancedSearchWrapper();
		if (StringUtils.isNotBlank(externalReq.getQuery()))
		{
			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			 try {
				 //int window = externalReq.getWindowSize() == null?12:externalReq.getWindowSize();				
				 //int start = ((externalReq.getPageNumber() == null ?(1*window):((externalReq.getPageNumber()+1)*window)) - window) +1;
				 panelrequest = getUrlForAptusAppAdvancedSearch(externalReq, map, apiVersion, profileCategory, user);				   				 
				 pubDetails 	= aptusAdvancedSearchService.search(panelrequest);
				 if(pubDetails!=null)
				 {
					 //
					 if(pubDetails.getAutocomplete()!=null && pubDetails.getAutocomplete().size()>0
							 && pubDetails.getAutocomplete().get(0).getCompletions() != null 
							 && pubDetails.getAutocomplete().get(0).getCompletions().size()>0)
					 {
						 ArrayList<Completions> list = (ArrayList<Completions>) pubDetails.getAutocomplete().get(0).getCompletions();
						 ArrayList<String> terms = new ArrayList<>();
							if(list!= null && list.size() >0 )
							{						
								for(Completions comp:list){
									if(comp.getText().indexOf("|") != -1)				
										terms.add(WordUtils.capitalize(comp.getText().replaceAll("\\|", ", ")));				
									else
										terms.add(WordUtils.capitalize(comp.getText()));
								}
							}	
							
							wrapper.setAutoComplete(terms);
					 }
					 else
						 wrapper.setAutoComplete(new ArrayList<String>());
					 
					 if(pubDetails.getAutocompleteAuthor()!=null && pubDetails.getAutocompleteAuthor().size()>0
							 && pubDetails.getAutocompleteAuthor().get(0).getCompletions() != null 
							 && pubDetails.getAutocompleteAuthor().get(0).getCompletions().size()>0)
					 {
						 /*ArrayList<Completions> list = (ArrayList<Completions>) pubDetails.getAutocompleteAuthor().get(0).getCompletions();
						 ArrayList<String> terms = new ArrayList<>();
							if(list!= null && list.size() >0 )
							{						
								for(Completions comp:list){
									if(comp.getText().indexOf("|") != -1)				
										terms.add(comp.getText().replaceAll("\\|", ", "));				
									else
										terms.add(comp.getText());
								}
							}	
							
							wrapper.setAutoCompleteAuthors(terms);*/
							
							ArrayList<Completions> list = (ArrayList<Completions>) pubDetails.getAutocompleteAuthor().get(0).getCompletions();
							 ArrayList<AutoCompleteSeries> autoCompleteSerieslist = new ArrayList<AutoCompleteSeries>();
							 int count =0;
								if(list!= null && list.size() >0 )
								{						
									for(Completions comp:list){
										String text = "";
										if(comp.getText().indexOf("|") != -1)				
											text=comp.getText().replaceAll("\\|", ", ");				
										else
											text=comp.getText();
										if(!text.equalsIgnoreCase("") && comp.getText().indexOf("|")==-1)
											{
												
												AutoCompleteSeries series = new AutoCompleteSeries();
												series.setId("search_authors_"+text);
												series.setTerms(WordUtils.capitalize(text));
												series.setPosition(count);
												series.setHaschild(0);
												series.setShowvolume(0);
												series.setAllowsorting(0);
												series.setType("Authors");
												autoCompleteSerieslist.add(series);
												count++;
											}
										
									}
								}	
								
								wrapper.setAutoCompleteAuthors(autoCompleteSerieslist);
					 }
					 else
						 wrapper.setAutoCompleteAuthors(new ArrayList<AutoCompleteSeries>());
					 
					 if(pubDetails.getAutocompleteNarrators()!=null && pubDetails.getAutocompleteNarrators().size()>0
							 && pubDetails.getAutocompleteNarrators().get(0).getCompletions() != null 
							 && pubDetails.getAutocompleteNarrators().get(0).getCompletions().size()>0)
					 {
						 /*ArrayList<Completions> list = (ArrayList<Completions>) pubDetails.getAutocompleteNarrators().get(0).getCompletions();
						 ArrayList<String> terms = new ArrayList<>();
							if(list!= null && list.size() >0 )
							{						
								for(Completions comp:list){
									if(comp.getText().indexOf("|") != -1)				
										terms.add(comp.getText().replaceAll("\\|", ", "));				
									else
										terms.add(comp.getText());
								}
							}	
							
							wrapper.setAutoCompleteNarrators(terms);*/
						 
						 ArrayList<Completions> list = (ArrayList<Completions>) pubDetails.getAutocompleteNarrators().get(0).getCompletions();
						 ArrayList<AutoCompleteSeries> autoCompleteSerieslist = new ArrayList<AutoCompleteSeries>();
						 int count =0;
							if(list!= null && list.size() >0 )
							{						
								for(Completions comp:list){
									String text = "";
									if(comp.getText().indexOf("|") != -1)				
										text=comp.getText().replaceAll("\\|", ", ");				
									else
										text=comp.getText();
									if(!text.equalsIgnoreCase("") && comp.getText().indexOf("|")==-1)
										{
											
											AutoCompleteSeries series = new AutoCompleteSeries();
											series.setId("search_narrators_"+text);
											series.setTerms(WordUtils.capitalize(text));
											series.setPosition(count);
											series.setHaschild(0);
											series.setShowvolume(0);
											series.setAllowsorting(0);
											series.setType("Narrators");
											autoCompleteSerieslist.add(series);
											count++;
										}
									
								}
							}	
							
							wrapper.setAutoCompleteNarrators(autoCompleteSerieslist);
					 }
					 else
						 wrapper.setAutoCompleteNarrators(new ArrayList<AutoCompleteSeries>());
					 
					 
					 if(pubDetails.getAutocompleteSeries()!=null && pubDetails.getAutocompleteSeries().size()>0
							 && pubDetails.getAutocompleteSeries().get(0).getCompletions() != null 
							 && pubDetails.getAutocompleteSeries().get(0).getCompletions().size()>0)
					 {
						 ArrayList<Completions> list = (ArrayList<Completions>) pubDetails.getAutocompleteSeries().get(0).getCompletions();
						 ArrayList<AutoCompleteSeries> autoCompleteSerieslist = new ArrayList<AutoCompleteSeries>();
						 int count =0;
							if(list!= null && list.size() >0 )
							{						
								for(Completions comp:list){
									String text = "";
									if(comp.getText().indexOf("|") != -1)				
										text=comp.getText().replaceAll("\\|", ", ");				
									else
										text=comp.getText();
									if(!text.equalsIgnoreCase("") && comp.getText().indexOf("|")==-1)
										{
											
											AutoCompleteSeries series = new AutoCompleteSeries();
											series.setId("s_"+text);
											series.setTerms(WordUtils.capitalize(text));
											series.setPosition(count);
											series.setHaschild(0);
											series.setShowvolume(1);
											series.setAllowsorting(0);
											series.setType("Series");
											autoCompleteSerieslist.add(series);
											count++;
										}
									
								}
							}	
							
							wrapper.setAutoCompleteSeries(autoCompleteSerieslist);
					 }
					 else
						 wrapper.setAutoCompleteSeries(new ArrayList<AutoCompleteSeries>());
					 
					 if(pubDetails.getSearchHitCount()!=null && pubDetails.getSearchHitCount().size()>0)
					 {
						 if(pubDetails.getSearchHitCount().get(0)!=null && pubDetails.getSearchHitCount().get(0).getCount()>0)
							 wrapper.setSearchCount(pubDetails.getSearchHitCount().get(0).getCount());
						 else
							 wrapper.setSearchCount(0);
					 }
					 
					 if(pubDetails.getProductSuggestions()!=null && pubDetails.getProductSuggestions().size()>0)
					 {
						 ArrayList<ProductInformation> productlist = new ArrayList<ProductInformation> ();
						 int start =0;
						 if(pubDetails.getProductSuggestions().get(0)!=null && pubDetails.getProductSuggestions().get(0).getProducts()!=null)
						 {
							 productlist=apptusServiceHelper.operateOnApptusProduct((ArrayList<ProductsAptus>)
									 pubDetails.getProductSuggestions().get(0).getProducts(), user, start, apiVersion,false);
						 }
						 
						 wrapper.setProductSuggestions(productlist);
					 }
					 else
					 {
						 wrapper.setProductSuggestions(new  ArrayList<ProductInformation>());
					 }
					 
					 
					 
					 
					 if(pubDetails.getCategorySuggestions()!=null && pubDetails.getCategorySuggestions().size()>0
							 && pubDetails.getCategorySuggestions().get(0).getCategoryList() != null 
							 && pubDetails.getCategorySuggestions().get(0).getCategoryList().size()>0 
							 && user!=null && user.getProfile()!=null && 
							 !user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
					 {
						 ArrayList<categoryList> list = (ArrayList<categoryList>) pubDetails.getCategorySuggestions().get(0).getCategoryList();
						 ArrayList<AutoCompleteSeries> autoCompleteSerieslist = new ArrayList<AutoCompleteSeries>();
						 int count =0;
							if(list!= null && list.size() >0 )
							{						
								for(categoryList comp:list){

											
											EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(comp.getKey()));
										
											if(cat!=null && comp.getParentKey().contains("root"))
											{
												AutoCompleteSeries series = new AutoCompleteSeries();
												series.setTerms(cat.getCategoryName());
												series.setPosition(count);
												series.setShowvolume(0);
												series.setAllowsorting(1);
												/*if(cat.getParentCatId().intValue() ==0)
												{*/
													series.setHaschild(1);
													series.setType(ApptusBookGroupsEnum.TOP_TIER_TOP_CATEGORY_TYPE.getGroupid());
													series.setId(ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+cat.getCategoryId());
												//}
												/*else
												{
													series.setParentId(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+cat.getParentCatId());
													series.setId(ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+cat.getCategoryId());
													series.setType(ApptusBookGroupsEnum.SUB_CATEGORY_TOP_CATEGORY_TYPE.getGroupid());
													
													if(cat.getSubCategories()!=null && cat.getSubCategories().size()>0)
														series.setHaschild(1);
													else
														series.setHaschild(0);
												}*/
												
												autoCompleteSerieslist.add(series);
												count++;
											}
											
										
									
								}
							}	
							
							wrapper.setCategorySuggestions(autoCompleteSerieslist);
					 }
					 else
						 wrapper.setCategorySuggestions(new ArrayList<AutoCompleteSeries>());
					 
					 
					 
					 /*if(pubDetails.getProductSuggestionsSeries()!=null && pubDetails.getProductSuggestionsSeries().size()>0)
					 {
						 ArrayList<ProductInformation> productlist = new ArrayList<ProductInformation> ();
						 int start =0;
						 if(pubDetails.getProductSuggestionsSeries().get(0)!=null && pubDetails.getProductSuggestionsSeries().get(0).getProducts()!=null)
						 {
							 productlist=apptusServiceHelper.operateOnApptusProduct((ArrayList<ProductsAptus>)
									 pubDetails.getProductSuggestionsSeries().get(0).getProducts(), user, start, apiVersion);
						 }
						 
						 wrapper.setProductSuggestionsSeries(productlist);
					 }
					 else
					 {
						 wrapper.setProductSuggestionsSeries(new  ArrayList<ProductInformation>());
					 }*/
					 
					 if(pubDetails.getPromotionalListSuggestions()!=null && pubDetails.getPromotionalListSuggestions().size()>0
							 && pubDetails.getPromotionalListSuggestions().get(0).getCategoryList() != null 
							 && pubDetails.getPromotionalListSuggestions().get(0).getCategoryList().size()>0)
					 {
						 ArrayList<categoryList> list = (ArrayList<categoryList>) pubDetails.getPromotionalListSuggestions().get(0).getCategoryList();
						 ArrayList<AutoCompleteSeries> autoCompleteSerieslist = new ArrayList<AutoCompleteSeries>();
						 String url = applicationpropconfig.getTopPanelUrlNew();
						 TopPanelList vo = null;
							Map<String , Integer> map1 = InMemoryCache.subAgeLimits;
							
							if(profileCategory.equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
								url = url + "&profile=CHILD&subscriptionorder="+map1.get(user.getSubscription().getType());
							else
								url = url + "&profile=PARENT&subscriptionorder="+map1.get(user.getSubscription().getType());
							
							url = url +"&country="+user.getCountrycode();
							
							try {
								if(profileCategory.equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
									vo = cacheBridgeTopPanels.callNestTopPanelsUrl(url, "CHILD",user.getCountrycode());
								else
									vo = cacheBridgeTopPanels.callNestTopPanelsUrl(url, "PARENT",user.getCountrycode());
							} catch (Exception e) {			
								e.printStackTrace();
							}
						 
							
							HashMap<String, TopPanelVO> cat = getTopPanelDataFromHelper(vo);
						 
						 
						 int count =0;
							if(list!= null && list.size() >0 )
							{						
								for(categoryList comp:list){
									
										
										TopPanelVO data = cat.get(comp.getKey());

											if(data!=null)
											{
												AutoCompleteSeries series = new AutoCompleteSeries();
												series.setTerms(data.getTitle());
												series.setPosition(count);
												series.setShowvolume(0);
												series.setAllowsorting(1);
												
													series.setHaschild(1);
													series.setType(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TYPE.getGroupid());
													series.setId(data.getGroupid());
											
												
												autoCompleteSerieslist.add(series);
												count++;
											}
											
										
									
								}
							}	
							
							wrapper.setPromotionalListSuggestions(autoCompleteSerieslist);
					 }
					 else
						 wrapper.setPromotionalListSuggestions(new ArrayList<AutoCompleteSeries>());
					 
				 }

			}catch (Exception e) {			
				e.printStackTrace();
			}
		} else
			wrapper = new AdvancedSearchWrapper();
		
		return wrapper;
	}
	
	public HashMap<String, TopPanelVO> getTopPanelDataFromHelper (TopPanelList list)
	{
		HashMap<String, TopPanelVO> map = new HashMap<>();
		if(list!=null && list.getTopPanelList()!=null && list.getTopPanelList().size()>0)
		{
			for(TopPanelVO vo:list.getTopPanelList())
			{
				map.put("L"+vo.getListid(), vo);
			}
		}
		return map;
	}
	
	public AdvancedSearchZone getUrlForAptusAppAdvancedSearch(SearchRequest externalReq, Map<String, Integer> map,
			Double apiVersion, String profileCategory, User user) {
		//List<Integer> windowstart_end = apptusServiceHelper.getWindowStartAndEnd(externalReq.getPageNumber(),
				//externalReq.getPageSize());
		AdvancedSearchZone advancedSearchZone = new AdvancedSearchZone();
		advancedSearchZone.setSearchPhase(externalReq.getQuery().replaceAll("&", "%26"));
		advancedSearchZone.setSearchPrefix(externalReq.getQuery().replaceAll("&", "%26"));
		if (user != null && !StringUtils.isBlank(user.getUserCountry())) {
			advancedSearchZone.setInclude_filter(FilterUtils.buildCountryIncludeFilter(user.getUserCountry()));
			advancedSearchZone.setExclude_filter(FilterUtils.buildCountryExcludeFilter(user.getUserCountry()));
		}
		if (user != null && user.getExcludePubs() != null)
			advancedSearchZone.setPublisher_filter(FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()));
		if (!StringUtils.isBlank(user.getCountrycode()))
			advancedSearchZone.setMarket_filter(user.getCountrycode());
		if (StringUtils.equalsAnyIgnoreCase(profileCategory, PROFILE_CATEGORIES.KIDS.name())) {
			String category_filter = apptusServiceHelper.getParentCategoryFilter(user);
			if (!StringUtils.isBlank(category_filter))
				advancedSearchZone.setParent_category_filter(category_filter);
		}
		if (externalReq.getProductFormat() != null
				&& !externalReq.getProductFormat().equals(ProductFormatEnum.UN_DEFINED)
				&& (externalReq.getProductFormat().getFormatNo() > 0
						&& externalReq.getProductFormat().getFormatNo() <= 2))
			advancedSearchZone.setFormat_filter("" + externalReq.getProductFormat().getFormatNo());
		advancedSearchZone.setSortBy(FilterUtils.buildSortByFilter(externalReq.getSortOn()));

		/*List<String> subscriptions = apptusServiceHelper.getSubscriptionFilterApptusAttributeName(externalReq, map,
				user);
		if (!StringUtils.isBlank(subscriptions.get(0)))
			searchpanel.setSubscription_filter(subscriptions.get(0));
		if (!StringUtils.isBlank(subscriptions.get(1)))
			searchpanel.setSubscription_filter_1(subscriptions.get(1));*/
		
		if (!StringUtils.isBlank(user.getCountrycode()))
		advancedSearchZone.setROOT_CATEGORY("categoryids_"+user.getCountrycode()+":'root'");
		
		if (!StringUtils.isBlank(user.getCountrycode()))
			advancedSearchZone.setCategory_Filter_1("countrycode:'"+user.getCountrycode()+"'");
		
		String subscriptionfilter = FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType())  );
		advancedSearchZone.setSubscription_filter(subscriptionfilter);

		advancedSearchZone.setMarket(apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));

		if (user != null && user.getProfile() != null && user.getProfile().getProfileid() != null) {
			advancedSearchZone.setSessionKey(
					(user.getAuthToken() != null) ? user.getAuthToken().getUuid() : UUID.randomUUID().toString());
			advancedSearchZone.setCustomerKey("" + user.getProfile().getProfileid());
		} else if (user != null && user.getCustomerid() != null) {
			advancedSearchZone.setSessionKey(
					((user.getAuthToken() != null) ? user.getAuthToken().getUuid() : "" + user.getCustomerid()));
			advancedSearchZone.setCustomerKey("" + user.getCustomerid());
		}
		return advancedSearchZone;
	}

	
	
	
			
	public BooksForBookGroupsApptusResponseWrapper getDataForRecommendZoneForTopList(BooksForBookGroupRequestBean reqBean, Double apiVersion,User user
			,BooksForBookGroupsApptusResponseWrapper proddetails) throws Exception
	{
		// Recommend books Scenario
		if(reqBean.getBookgroupid() != null && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED.getGroupid())
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_RECOMMEND_URL_TO_BE_FIRED.getGroupid()))
		{
			RecommendBasedOnCustomer	panelrequest = new RecommendBasedOnCustomer();	
			ProductFormatEnum	format = ProductFormatEnum.getFormat( reqBean.getType() );
			if( format == ProductFormatEnum.E_BOOK || format == ProductFormatEnum.AUDIO_BOOK )
				panelrequest.setFormat_filter( "" + format.getFormatNo() );	
			List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd( ( !StringUtils.isBlank( reqBean.getPagenumber() )?Integer.valueOf( reqBean.getPagenumber()):0 ), reqBean.getRows() );			
			panelrequest.setWindow_first("" + windowstart_end.get(0));
			panelrequest.setWindow_last( "" + windowstart_end.get(1) );
			panelrequest.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
			panelrequest.setCustomerKey( apptusServiceHelper.getApptusCustomerKey(user));
			panelrequest.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
			if( !StringUtils.isBlank( user.getUserCountry() ) ){				
				panelrequest.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));				
				panelrequest.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));					
			}
			panelrequest.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()) );
			if( !StringUtils.isBlank( user.getCountrycode() ) )
				panelrequest.setMarket_filter(user.getCountrycode());
			String	childfilter = apptusServiceHelper.getChildFilterFromCategorys( user );
			if( !StringUtils.isBlank( childfilter ) )
				panelrequest.setChild_filter(childfilter);

			//DEBA FILTER LIBRARY BOOKS FROM RECOMMEND FOR YOU 
			panelrequest = getFilterForBooksFromLibrary(user,panelrequest);

			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			panelrequest.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType()) ) );						
			try {
				ArrayList<ProductsAptus> list = aptusrecommendbasedoncustomer.genericPanelSearch(panelrequest);
				if( list.size() > 0 )
				{
					ArrayList<ProductInformation> recommendbooklist = apptusServiceHelper.operateOnApptusProduct(list,user, windowstart_end.get( 0 ),apiVersion,false);
					proddetails.setBooks(recommendbooklist);
				}else{
					proddetails.setBooks(Collections.emptyList());
				}
			}
			catch (Exception e) {
				logger.error("Error while fetching Recommend zone" + e.getMessage());
				e.printStackTrace();
			}
		}
		return proddetails;
	}
	
	
	public RecommendBasedOnCustomer getFilterForBooksFromLibrary (User user,RecommendBasedOnCustomer panelrequest)
	{
		if(user !=null && user.getProfile()!=null && user.getProfile().getProfileid()!=null)
		{
			String url = applicationpropconfig.getLibraryUrl()+user.getProfile().getProfileid()+"&limit="+applicationpropconfig.getLibraryMaxSize();
			BookIdData data = null;
			try {
				data = hservice.callLibraryBooksIdList(url);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			if(data!=null && data.getData()!=null && data.getData()!=null && data.getData().getBookidlist()!=null &&
					data.getData().getBookidlist().size()>0 && data.getData().getBookidlist().size()<=
					Integer.valueOf(applicationpropconfig.getLibraryMaxSize()))
			{
				String bookid_filter_new ="";
				String related_bookid_filter_new ="";
				 int count =0;
				for(Integer aut:data.getData().getBookidlist()){						
					//aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
					if(count ==0)
					{
						bookid_filter_new = bookid_filter_new + " NOT bookid:'"+ aut + "'";
						related_bookid_filter_new = related_bookid_filter_new + " NOT relatedproductid:'"+ aut + "'";
					}
					 else
					 {
						 bookid_filter_new = bookid_filter_new + " AND  NOT bookid:'"+ aut + "'";	
						 related_bookid_filter_new = related_bookid_filter_new + " AND  NOT relatedproductid:'"+ aut + "'";	
					 }
					count++;
				 }
				panelrequest.setBookid_filter_new( bookid_filter_new);
				panelrequest.setRelated_Bookid_filter(related_bookid_filter_new);
			}
		}
		
		return panelrequest;
	}
	
	public BooksForBookGroupsApptusResponseWrapper getDataForTopSellers(BooksForBookGroupRequestBean reqBean, Double apiVersion,User user
			,BooksForBookGroupsApptusResponseWrapper proddetails) throws Exception
	{
		// Recommend books Scenario
		if(reqBean.getBookgroupid() != null && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopSeller.getGroupid())
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_TopSeller.getGroupid()))
		{
			TopSellers	panelrequest = new TopSellers();	
			ProductFormatEnum	format = ProductFormatEnum.getFormat( reqBean.getType() );
			if( format == ProductFormatEnum.E_BOOK || format == ProductFormatEnum.AUDIO_BOOK )
				panelrequest.setFormat_filter( "" + format.getFormatNo() );	
			List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd( ( !StringUtils.isBlank( reqBean.getPagenumber() )?Integer.valueOf( reqBean.getPagenumber()):0 ), reqBean.getRows() );			
			panelrequest.setWindow_first("" + windowstart_end.get(0));
			panelrequest.setWindow_last( "" + windowstart_end.get(1) );
			panelrequest.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
			panelrequest.setCustomerKey( apptusServiceHelper.getApptusCustomerKey(user));
			panelrequest.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
			if( !StringUtils.isBlank( user.getUserCountry() ) ){				
				panelrequest.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));				
				panelrequest.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));					
			}
			if(user.getExcludePubs()!=null)
			panelrequest.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()) );
			if( !StringUtils.isBlank( user.getCountrycode() ) )
				panelrequest.setMarket_filter(user.getCountrycode());
			/*String	childfilter = apptusServiceHelper.getChildFilterFromCategorys( user );
			if( !StringUtils.isBlank( childfilter ) )
				panelrequest.setChild_filter(childfilter);*/

			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			panelrequest.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType()) ) );
			
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist[2];
			if(StringUtils.isNotBlank(id) && Integer.valueOf(id)>0)
			{
				panelrequest.setSelected_category( FilterUtils.buildCategoryId(user.getCountrycode(), "" + id ));	
				EbCategories cat = cacheBridge1.getCategoryCache(user.getCountrycode()).get(Integer.valueOf(id));
					if(cat !=null && StringUtils.isNotBlank(cat.getPrimarylanguage()))
					{
						String[] b = cat.getPrimarylanguage().split(",");
						String	language_filter_new  = "";
						if(b!= null && b.length>0){
							byte	count =0;
							for(String aut:b){						
								aut = URLEncoder.encode(HtmlUtils.htmlUnescape(aut),"UTF-8");
								if(count ==0)
									language_filter_new = language_filter_new + "language:'"+ aut + "'";
								 else
									 language_filter_new = language_filter_new + " OR language:'"+ aut + "'";							
								count++;
							 }
						 }
						if( !StringUtils.isBlank( language_filter_new ))
							panelrequest.setLanguage_filter(language_filter_new);
					}
			}
			try {
				
				Map<String, List<ProductListWithCount>> proDetails = topSellerService.genericPanelSearch(panelrequest);
				proddetails = apptusServiceHelper.getDataBasedOnUpcomingTopSellersBooks(proDetails, reqBean, user,false);
			}
			catch (Exception e) {
				logger.error("Error while fetching Recommend zone" + e.getMessage());
				e.printStackTrace();
			}
		}
		return proddetails;
	}
	
	
	public BooksForBookGroupsApptusResponseWrapper getDataForUpComing(BooksForBookGroupRequestBean reqBean, Double apiVersion,User user
			,BooksForBookGroupsApptusResponseWrapper proddetails) throws Exception
	{
		// Recommend books Scenario
		if(reqBean.getBookgroupid() != null && reqBean.getBookgroupid().startsWith(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_UPCOMING.getGroupid())
				&& reqBean.getBookgroupid().contains(ApptusBookGroupsEnum.TOP_TIER_TOP_LIST_UPCOMING.getGroupid()))
		{
			UpComming	panelrequest = new UpComming();	
			ProductFormatEnum	format = ProductFormatEnum.getFormat( reqBean.getType() );
			if( format == ProductFormatEnum.E_BOOK || format == ProductFormatEnum.AUDIO_BOOK )
				panelrequest.setFormat_filter( "" + format.getFormatNo() );	
			List<Integer>	windowstart_end = apptusServiceHelper.getWindowStartAndEnd( ( !StringUtils.isBlank( reqBean.getPagenumber() )?Integer.valueOf( reqBean.getPagenumber()):0 ), reqBean.getRows() );			
			panelrequest.setWindow_first("" + windowstart_end.get(0));
			panelrequest.setWindow_last( "" + windowstart_end.get(1) );
			panelrequest.setSessionKey( apptusServiceHelper.getApptusSessionKey(user) );
			panelrequest.setCustomerKey( apptusServiceHelper.getApptusCustomerKey(user));
			panelrequest.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
			if( !StringUtils.isBlank( user.getUserCountry() ) ){				
				panelrequest.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));				
				panelrequest.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));					
			}
			if(user.getExcludePubs()!=null)
			 panelrequest.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()) );
			
			if( !StringUtils.isBlank( user.getCountrycode() ) )
				panelrequest.setMarket_filter(user.getCountrycode());
			/*String	childfilter = apptusServiceHelper.getChildFilterFromCategorys( user );
			if( !StringUtils.isBlank( childfilter ) )
				panelrequest.setChild_filter(childfilter);*/
			
			
			String idlist[] = reqBean.getBookgroupid().split(ApptusBookGroupsEnum.ID_SEPERATOR.getGroupid());
			String id = idlist[2];
			panelrequest.setParent_category_filter( "listids:'L"+id+"'" );
			panelrequest.setRelevance_sort( "category_key('category_rank_values,L"+id+"') asc ");
			
			if(user.getProfile() != null && user.getProfile().getCategory() != null &&
					user.getProfile().getCategory().equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))
			{ 
				EbCategories kidCategory = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));				
				if(kidCategory!= null && kidCategory.getSubCategories() !=null){
				   byte	count =0;
				   String	author_filter = "";				   
				   for(EbCategories subcategory:kidCategory.getSubCategories())
					{
						 if(count == 0)
							 author_filter = author_filter  + FilterUtils.buildCategoryId( user.getCountrycode(), "" + subcategory.getCategoryId());
						 else
							 author_filter = author_filter + " OR " + FilterUtils.buildCategoryId( user.getCountrycode(), "" + subcategory.getCategoryId());							 
						 count++;
					 }
				   if( !StringUtils.isBlank( author_filter ))
					   panelrequest.setChild_filter(author_filter);
				 }
			}		
			

			Map<String , Integer> map = InMemoryCache.subAgeLimits;
			panelrequest.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType()) ) );						
			try {
				Map<String, List<ProductListWithCount>> proDetails = upCommingService.genericPanelSearch(panelrequest);
				proddetails = apptusServiceHelper.getDataBasedOnUpcomingTopSellersBooks(proDetails, reqBean, user,true);
			}
			catch (Exception e) {
				logger.error("Error while fetching Recommend zone" + e.getMessage());
				e.printStackTrace();
			}
		}
		return proddetails;
	}
	
	
	public AutoCompleteWrapper getAutoComplete(AutoCompleteJsonRequestBean reqBean, Double apiVersion, User user) throws Exception
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getAutoComplete :: start :: AutoCompleteWrapper ::: "+reqBean);		
		String	qStr	=	reqBean.getKeyword();
		if( qStr != null ){
			qStr	=	StringUtils.stripStart(qStr, " ");
			qStr	=	StringUtils.stripEnd(qStr, " ");
			reqBean.setKeyword(qStr);
		}		
		SearchResult 		result 		= new SearchResult();		
		List<Completions> 	list 		= new ArrayList<Completions>();		
		if(null != user.getProfile() && null != user.getProfile().getCategory())
		{
			String category = user.getProfile().getCategory();
			if(category.equalsIgnoreCase(PROFILE_CATEGORIES.KIDS.name()))			
				list = CallAutoCompleteNewKids(reqBean.getKeyword(),user);
			else			
				list = CallAutoCompleteNew(reqBean.getKeyword(),user);			
		}else		
			list = CallAutoCompleteNew(reqBean.getKeyword(),user);		
				
		ArrayList<String> terms = new ArrayList<>();
		if(list!= null && list.size() >0 )
		{						
			for(Completions comp:list){
				if(comp.getText().indexOf("|") != -1)				
					terms.add(comp.getText().replaceAll("\\|", ", "));				
				else
					terms.add(comp.getText());
			}
		}		
		AutoCompleteWrapper wrapper = new AutoCompleteWrapper();
		if(list!= null && list.size() >0 )
			wrapper.setTerms(terms);		
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getBooksFromKeywordSearchDevice :: Stop :: BookGroupBooksWrapper ::: "+result.getResultSize());
		list		 =null;			
		return wrapper;
	}
	
	
	public AutoCompleteWrapper getSearchHistory(User user) throws Exception
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getSearchHistory :: start :: SearchHistoryData ::: ");		
	
		List<Phrases> 	list 		= new ArrayList<Phrases>();		
		
		list = callSearchHistory(user);		
				
		ArrayList<String> terms = new ArrayList<>();
		if(list!= null && list.size() >0 )
		{						
			for(Phrases comp:list){
				if(comp.getText().indexOf("|") != -1)				
					terms.add(comp.getText().replaceAll("\\|", ", "));				
				else
					terms.add(comp.getText());
			}
		}		
		AutoCompleteWrapper wrapper = new AutoCompleteWrapper();
		if(list!= null && list.size() >0 )
			wrapper.setTerms(terms);		
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getSearchHistory :: Stop ::");
		list		 =null;			
		return wrapper;
	}
	
	public AutoCompleteWrapper getTopSearches(User user) throws Exception
	{
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getSearchHistory :: start :: SearchHistoryData ::: ");		
	
		List<Phrases> 	list 		= new ArrayList<Phrases>();		
		
		list = callTopSearches(user);		
				
		ArrayList<String> terms = new ArrayList<>();
		if(list!= null && list.size() >0 )
		{						
			for(Phrases comp:list){
				if(comp.getText().indexOf("|") != -1)				
					terms.add(comp.getText().replaceAll("\\|", ", "));				
				else
					terms.add(comp.getText());
			}
		}		
		AutoCompleteWrapper wrapper = new AutoCompleteWrapper();
		if(list!= null && list.size() >0 )
			wrapper.setTerms(terms);		
		if (logger.isTraceEnabled())logger.trace("SearchDaoImpl  :: getSearchHistory :: Stop ::");
		list		 =null;			
		return wrapper;
	}
	
	
	public ArrayList<Completions> CallAutoCompleteNewKids(String query, User user) throws Exception{
		//String tempurl = config.getAutoCompleteUrlNewKids();
		//StringBuffer tempurl = new StringBuffer(aptusconfig.getAutoCompleteUrlNewKids());
		AutoCompleteForKidsPanel	panel = new AutoCompleteForKidsPanel();		 
		 
		if(user!=null && user.getUserCountry()!=null && !user.getUserCountry().equalsIgnoreCase("")){
			panel.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));
		 	panel.setExclude_filter( FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ) );			
		}	
		if(user !=null && user.getExcludePubs()!=null )
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ));			
		if(user.getCountrycode()!=null && !user.getCountrycode().equalsIgnoreCase(""))
			panel.setMarket_filter( user.getCountrycode() );
		
		EbCategories kidscategories = hazelCastDependentService.getKidsParentCategory(cacheBridge1.getCategoryCache(user.getCountrycode()));
		StringBuilder	child_category_filter = new StringBuilder("");
		if(kidscategories != null){			
			child_category_filter.append( " (" + FilterUtils.buildCategoryId(user.getCountrycode(), "" + kidscategories.getCategoryId() ) );
			for(EbCategories sub: kidscategories.getSubCategories()){
				child_category_filter.append( " OR " + FilterUtils.buildCategoryId(user.getCountrycode(), "" + sub.getCategoryId() ) );
			}		
			child_category_filter.append( ") ");
			if( !StringUtils.isBlank(child_category_filter.toString()))
				panel.setChild_category_filter( child_category_filter.toString() );
		}
		panel.setSessionKey( ( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): "" + user.getCustomerid() ) );
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()));
		panel.setSearchPrefix( query );
		panel.setWindow_first(1 + "");
		panel.setWindow_last("" + 10);
		panel.setCustomerKey( (user.getProfile() != null && 
							  user.getProfile().getProfileid() != null && 
							  user.getProfile().getProfileid() > 0 )?""+user.getProfile().getProfileid():null );						
		return autocompleteforkidsservice.search(panel);
	}
	
	public ArrayList<Completions> CallAutoCompleteNew(String query,User user) throws Exception{
		//StringBuffer tempurl = new StringBuffer(aptusconfig.getAutoCompleteUrlNew());
		AutoCompletePanel	panel = new AutoCompletePanel();		
		if( !StringUtils.isBlank( user.getUserCountry() ) )
		{
			panel.setInclude_filter( FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));
			panel.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));			
		}
		if(user !=null && user.getExcludePubs()!=null)
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter( user.getExcludePubs() ));	
		if(user.getCountrycode()!=null && !user.getCountrycode().equalsIgnoreCase(""))
			panel.setMarket_filter( user.getCountrycode() );
		panel.setSessionKey( ( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): UUID.randomUUID().toString() ) );
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()) );
		panel.setSearchPrefix( query );
		panel.setWindow_first( "" + 1 );
		panel.setWindow_last( "" + 10 );		
		panel.setCustomerKey( (user.getProfile() != null && user.getProfile().getProfileid() != null && user.getProfile().getProfileid() > 0 )?""+user.getProfile().getProfileid():"" );		
		return autocompleteservice.search(panel);
	}
	
	
	
	public ArrayList<Phrases> callSearchHistory(User user) throws Exception{
		SearchHistoryPanel	panel = new SearchHistoryPanel();		
		panel.setSessionKey( ( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): UUID.randomUUID().toString() ) );
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()) );
		panel.setWindow_first( "" + 1 );
		panel.setWindow_last( "" + 10 );		
		panel.setCustomerKey( (user.getProfile() != null && user.getProfile().getProfileid() != null && user.getProfile().getProfileid() > 0 )?""+user.getProfile().getProfileid():"" );		
		return searchHistoryService.search(panel);
	}
	
	
	public ArrayList<Phrases> callTopSearches(User user) throws Exception{
		TopSearchPanel	panel = new TopSearchPanel();		
		panel.setSessionKey( ( ( user.getAuthToken() != null )?user.getAuthToken().getUuid(): UUID.randomUUID().toString() ) );
		panel.setMarket( apptusServiceHelper.getMarketPanelArgument(user.getCountrycode()) );
		panel.setWindow_first( "" + 1 );
		panel.setWindow_last( "" + 10 );		
		panel.setCustomerKey( (user.getProfile() != null && user.getProfile().getProfileid() != null && user.getProfile().getProfileid() > 0 )?""
		+user.getProfile().getProfileid():"" );		
		if(user.getUserCountry()!=null && !user.getUserCountry().equalsIgnoreCase(""))
		{					
			panel.setInclude_filter(FilterUtils.buildCountryIncludeFilter( user.getUserCountry() ));			
			panel.setExclude_filter(FilterUtils.buildCountryExcludeFilter( user.getUserCountry() ));			
		}
		if(user.getExcludePubs()!=null)
			panel.setPublisher_filter( FilterUtils.buildPublisherExclusionFilter(user.getExcludePubs()));		
		if(user.getCountrycode()!=null && !user.getCountrycode().equalsIgnoreCase(""))
			panel.setMarket_filter(user.getCountrycode() );
				
		Map<String , Integer> map = InMemoryCache.subAgeLimits;
		panel.setSubscription_filter(FilterUtils.buildSubscriptionFilter("1", "" + map.get(user.getSubscription().getType())) );
		
		return topSearchService.search(panel);
	}
	
	
	public String getBookGroupidBySlug(String slugname, String countrycode){		
		String bookGroupId = null;
		Map<Integer,EbCategories> categories = cacheBridge1.getCategoryCache(countrycode);
		EbCategories category = null;
		if(categories!=null)
			category = categories.values().stream().filter(c->c.getSlugName().equals(slugname)).findFirst().orElse(null);
		if(category!=null){
			if(category.getParentCatId()==0 && category.getSubCategories()!=null && !category.getSubCategories().isEmpty())
				bookGroupId = ApptusBookGroupsEnum.TOP_TIER_CATEGORIES.getGroupid()+category.getCategoryId();
			//else if(category.getParentCatId()>0 && category.getSubCategories()!=null && !category.getSubCategories().isEmpty())
			else if(category.getParentCatId()>0)
				bookGroupId = ApptusBookGroupsEnum.SUB_CATEGORY.getGroupid()+category.getCategoryId();
		}
		return bookGroupId;
	}
	
	/*public SearchRequest setSearchRequest(String token, User user){
		SearchRequest searchRequest = new SearchRequest();		
		Integer customerid = user.getCustomerid();
		Integer profileid = user.getProfile()!=null?user.getProfile().getProfileid(): 0;		
		searchRequest.setCustomerid(customerid);		
		searchRequest.setProfileid(profileid);
		return searchRequest;
	}*/

}
	

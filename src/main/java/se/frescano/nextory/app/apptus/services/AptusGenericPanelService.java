package se.frescano.nextory.app.apptus.services;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.request.GenericPanel;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.api.response.ProductListWithCount;

@DependsOn("applicationpropconfig")
@Service
public class AptusGenericPanelService implements TagExtractor<String>{

	private Logger logger = LoggerFactory.getLogger(AptusGenericPanelService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder genericpanelUriBuilder = null;
	
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;
	
	
	private final String GENERIC_PANEL_APP="generic_pannel_app";
		
	//@PostConstruct
	public UriComponentsBuilder init(){
		return genericpanelUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getGenericUrl() + GENERIC_PANEL_APP)
			      /*.query("arg.authors_filter={authors_filter}")
			      .query("arg.bookid_filter_new={bookid_filter_new}")
			      .query("arg.child_filter={child_filter}")
			      .query("arg.exclude_filter={exclude_filter}")
			      .query("arg.facets={facets}")
			      .query("arg.format_filter={format_filter}")
			      .query("arg.include_filter={include_filter}")
			      .query("arg.locale={locale}")
			      .query("arg.market_filter={market_filter}")
			      .query("arg.parent_category_filter={parent_category_filter}")
			      .query("arg.presentation_attributes={presentation_attributes}")
			      .query("arg.publisher_filter={publisher_filter}")
			      .query("arg.relevance_sort={relevance_sort}")
			      .query("arg.search_attributes={search_attributes}")
			      .query("arg.search_phrase={search_phrase}")
			      .query("arg.selected_category={selected_category}")
			      .query("arg.subscription_filter={subscription_filter}")
			      .query("arg.variants_per_product={variants_per_product}")
			      .query("arg.window_first={window_first}")
			      .query("arg.window_last={window_last}")
			      .query("market={market}")		      
			      .query("sessionKey={sessionKey}")
			      .query("customerKey={customerKey}")*/;
	}
	
	public Map<String, List<ProductListWithCount>> genericPanelSearch(GenericPanel panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS GENERIC SEARCH REQUEST--->" + uriComponents.toUri());	 				
		try{	
			//For TEsting
			//String	parma = "arg.format_filter=formattype:'1'&arg.authors_filter= NOT%20categoryids_SE:'13'%20AND%20NOT%20categoryids_SE:'14'%20AND%20NOT%20categoryids_SE:'15'%20AND%20NOT%20categoryids_SE:'16'%20AND%20NOT%20categoryids_SE:'17'%20AND%20NOT%20categoryids_SE:'18'%20AND%20NOT%20categoryids_SE:'19'%20AND%20NOT%20categoryids_SE:'36'%20AND%20NOT%20categoryids_SE:'39'%20AND%20NOT%20categoryids_SE:'40'%20AND%20NOT%20categoryids_SE:'41'%20AND%20NOT%20categoryids_SE:'42'%20AND%20NOT%20categoryids_SE:'43'%20AND%20NOT%20categoryids_SE:'44'%20AND%20NOT%20categoryids_SE:'51'%20AND%20NOT%20categoryids_SE:'54'%20AND%20NOT%20categoryids_SE:'55'%20AND%20NOT%20categoryids_SE:'266'%20AND%20NOT%20categoryids_SE:'107'&arg.relevance_sort=s_topE%20desc,%20relevance&arg.window_first=1&arg.window_last=12&market=SE&sessionKey=72b2b0dd-76ee-489a-8ebb-6ed5cc47c381&customerKey=336616&arg.exclude_filter=%20NOT%20exclude_restriction:'SE'%20OR%20exclude_restriction:'NIL'&arg.subscription_filter=subscriptionorder:['1','2']";
			/*Mono<String> result = webclient.get().uri( uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono( String.class );	*/
			String result = abstractresttemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, String.class,this);
			return Book2GoUtil.objectMapper.readValue(result ,new TypeReference<Map<String, List<ProductListWithCount>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);
			/*e.printStackTrace();*/
		}		
		return null;
	}

	@Override
	public CallState extract(String rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getGenericUrl() + GENERIC_PANEL_APP);
		Tag panel = Tag.of("panel", "genericpanel"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;	
		
	}
}

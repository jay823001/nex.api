package se.frescano.nextory.app.apptus.services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.ProductsAptus;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse4;
import se.frescano.nextory.apptus.request.RecommendBasedOnCustomer;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class RecommendBasedOnCustomerService implements TagExtractor<ApptusFinalResponse4>{

	private Logger logger = LoggerFactory.getLogger(RecommendBasedOnCustomerService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder uriBuilder;
	
	@Autowired
	private AbstractRestTempleteClient abstractRestTempleteClient;
	
	//@PostConstruct
	public UriComponentsBuilder init(){
		return uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getRecommendBasedOnCustomer() )
			      /*.query("arg.child_filter={child_filter}")
			      .query("arg.exclude_filter={exclude_filter}")
			      .query("arg.facets={facets}")
			      .query("arg.format_filter={format_filter}")
			      .query("arg.include_filter={include_filter}")
			      .query("arg.market_filter={market_filter}")
			      .query("arg.publisher_filter={publisher_filter}")		      
			      .query("arg.selected_category={selected_category}")
			      .query("arg.subscription_filter={subscription_filter}")
			      .query("arg.variants_per_product={variants_per_product}")		      
			      .query("arg.window_first={window_first}")
			      .query("arg.window_last={window_last}")
			      .query("market={market}")		      
			      .query("sessionKey={sessionKey}")
			      .query("customerKey={customerKey}")*/;
	}
	
	public ArrayList<ProductsAptus> genericPanelSearch(RecommendBasedOnCustomer panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS RECOMMEND BASED ON CUSTOMER REQUEST--->" + uriComponents.toUri());	 				
		try{			
			/*Mono<ApptusFinalResponse4> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono( ApptusFinalResponse4.class );			
			result.flatMapMany(response ->{
				if( response.getRecommendBasedOnCustomer() != null && response.getRecommendBasedOnCustomer().get(0) != null
						&& response.getRecommendBasedOnCustomer().get(0).getProducts() != null ){
					return Mono.just( response.getRecommendBasedOnCustomer().get(0).getProducts() );
				}
				return Mono.just(new ArrayList<ProductsAptus>(0));
			});
			return new ArrayList<ProductsAptus>(0);*/
			ApptusFinalResponse4 response = abstractRestTempleteClient.exchange(uriComponents.toUriString(), HttpMethod.GET, ApptusFinalResponse4.class,this);
			if( response.getRecommendBasedOnCustomer() != null && response.getRecommendBasedOnCustomer().get(0) != null
					&& response.getRecommendBasedOnCustomer().get(0).getProducts() != null ){
				return  response.getRecommendBasedOnCustomer().get(0).getProducts() ;
			}
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);			
		}		
		return new ArrayList<ProductsAptus>(0);
	}

	@Override
	public CallState extract(ApptusFinalResponse4 rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime, applicationpropconfig.getRecommendBasedOnCustomer());
		Tag panel = Tag.of("panel", "customerrecommended"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;
	}
		      
}

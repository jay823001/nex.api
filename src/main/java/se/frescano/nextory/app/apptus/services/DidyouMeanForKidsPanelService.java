package se.frescano.nextory.app.apptus.services;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.AptusDidYouMean;
import se.frescano.nextory.apptus.request.DidYouMeanForKidsPanel;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class DidyouMeanForKidsPanelService implements TagExtractor<AptusDidYouMean>{

private Logger logger = LoggerFactory.getLogger(AptusSearchService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder 	searchUriBuilder = null;
 	
	@Autowired
	private AbstractRestTempleteClient abstractRestTempleteClient;
	@PostConstruct
	public void init(){
		searchUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path( applicationpropconfig.getDidYouMeanUrlKids());	
	}
	
	/*Function<AptusDidYouMean,CallState> fn = new Function<AptusDidYouMean, CallState>() {
		@Override
		public CallState apply(AptusDidYouMean arg0) {
			// TODO Auto-generated method stub
			return null;
		}
	};*/
	
	public String search(DidYouMeanForKidsPanel panel){				
		
		UriComponents uriComponents = searchUriBuilder.replaceQueryParams(panel).build();
	
		logger.info("APPTUS DID YOU MEAN REQUEST--->" + uriComponents.toUri());	 				
		try{
			/*Mono<AptusDidYouMean> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AptusDidYouMean.class);			
			result.flatMapMany(response ->{
				if( response.getDidYouMean().get(0).getCorrections().size() >0 ){
					return Mono.just( response.getDidYouMean().get(0).getCorrections().get( 0 ).getText() );
				}
				return null;*/
				AptusDidYouMean result = abstractRestTempleteClient.exchange(uriComponents.toUriString(), HttpMethod.GET, AptusDidYouMean.class,this);
				if( result.getDidYouMean().get(0).getCorrections().size() >0 ){
					return /*Mono.just(*/ result.getDidYouMean().get(0).getCorrections().get( 0 ).getText() ;
				}
			return null;
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUriString(), e);
		}
		return null;
	}

	@Override
	public CallState extract(AptusDidYouMean rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getDidYouMeanUrlKids());
		Tag panel = Tag.of("panel", "didyoumeankids"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;
	}
}

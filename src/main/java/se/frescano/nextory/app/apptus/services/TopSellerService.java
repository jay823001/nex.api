package se.frescano.nextory.app.apptus.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.ProductsAptus;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse4;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse5;
import se.frescano.nextory.apptus.request.RecommendBasedOnCustomer;
import se.frescano.nextory.apptus.request.TopSellers;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.api.response.ProductListWithCount;

@Service
public class TopSellerService implements TagExtractor<String>{

	private Logger logger = LoggerFactory.getLogger(TopSellerService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder uriBuilder;
	
	@Autowired
	private AbstractRestTempleteClient abstractRestTempleteClient;
	
	//@PostConstruct
	public UriComponentsBuilder init(){
		return uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path("/clusters/w8F5A89AF/panels/topsellcategory");
	}
	
	public Map<String, List<ProductListWithCount>> genericPanelSearch(TopSellers panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS RECOMMEND BASED ON CUSTOMER REQUEST--->" + uriComponents.toUri());	 				
		try{			
			String result = abstractRestTempleteClient.exchange(uriComponents.toUriString(), HttpMethod.GET, String.class,this);
			return Book2GoUtil.objectMapper.readValue(result ,new TypeReference<Map<String, List<ProductListWithCount>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);			
		}		
		return null;
	}

	@Override
	public CallState extract(String rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime, "/clusters/w8F5A89AF/panels/topsellcategory");
		Tag panel = Tag.of("panel", "topsellcategory"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;
	}
		      
}

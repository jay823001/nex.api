package se.frescano.nextory.app.apptus.controller;

import java.util.ArrayList;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.annotation.Timed;
import se.frescano.nextory.app.api.spring.method.resolver.AppRequestBeanInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserInfo;
import se.frescano.nextory.app.api.spring.method.resolver.AppUserToken;
import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.exception.TokenNotValidException;
import se.frescano.nextory.app.product.request.AutoCompleteJsonRequestBean;
import se.frescano.nextory.app.product.request.BooksForBookGroupRequestBean;
import se.frescano.nextory.app.product.request.MainJsonRequestBean;
import se.frescano.nextory.app.product.request.ReportClickApptusRequestBean;
import se.frescano.nextory.app.product.request.SearchHistoryRequestBean;
import se.frescano.nextory.app.product.request.SearchJsonApptusRequestBean;
import se.frescano.nextory.app.product.response.AdvancedSearchWrapper;
import se.frescano.nextory.app.product.response.AutoCompleteWrapper;
import se.frescano.nextory.app.product.response.BooksForBookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.product.response.ProductInformation;
import se.frescano.nextory.app.product.response.SearchWithFacetsApptusResponseWrapper;
import se.frescano.nextory.app.request.APIController;
import se.frescano.nextory.app.request.APITokenRequestBean;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.BookGroupsApptusResponseWrapper;
import se.frescano.nextory.app.response.JsonWrapper;
import se.frescano.nextory.endpoints.SearchCounter;
import se.frescano.nextory.spring.method.model.User;

@RestController
@Timed
public class ApptusApiController extends APIController{
	
	private Logger logger = LoggerFactory.getLogger(ApptusApiController.class);
	
	@Autowired
	private  ApptusApiService apptusApiService;
	
	@Autowired
	private Validator validator;
		
	@RequestMapping(value =	{ APIJSONURLCONSTANTS.APTTUS_GROUPS}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public JsonWrapper mainApi(@AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @AppRequestBeanInfo APITokenRequestBean tokenbean,
											 @PathVariable("api_version")Double apiVersion,HttpServletRequest request, 
											 MainJsonRequestBean reqBean, BindingResult result) {
		setDefaultParams(reqBean, apiVersion, authtoken, user, tokenbean);	
		Set<ConstraintViolation<MainJsonRequestBean>> violations = validator.validate(reqBean);
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		try{  					
			BookGroupsApptusResponseWrapper wrapper = apptusApiService.getBookGroupsDetails( reqBean, user, apiVersion);			
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?new APIJsonWrapper(wrapper,apiVersion,user.getWarning()):new APIJsonWrapper(wrapper,apiVersion));
		}catch (TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		} catch (APIException e){
			logger.error("API Exception while getting Book Groups " + e.getErrorCodes() + e.getMessage());
			return handleErrorPageAsBean(e.getErrorCodes(),apiVersion,reqBean.getLocale());
		} catch (Exception e){
			logger.error("Error while getting Book Groups " + ErrorCodes.INTERNAL_ERROR, e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,apiVersion,reqBean.getLocale());
		}
	}
	
	@RequestMapping(value =	{ APIJSONURLCONSTANTS.APPTUS_BOOKS}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public Object booksforbookgroup(@AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,
														HttpServletRequest request, BooksForBookGroupRequestBean reqBean, BindingResult result
														,@AppRequestBeanInfo APITokenRequestBean tokenbean) {
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);		
		Set<ConstraintViolation<BooksForBookGroupRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
	    
		try{  										
			BooksForBookGroupsApptusResponseWrapper wrapper = apptusApiService.getBooksForBookGroup(reqBean, user, apiVersion);
			
			//This is to exclude json attributes for > 6.2 version
			if( apiVersion > CONSTANTS.API_VERSION_6_2 )
			//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_booksofbookgroupfilter_exclude_gt_6_2);		
			
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", json_booksofbookgroupfilter_exclude_gt_6_2)
							:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_booksofbookgroupfilter_exclude_gt_6_2));
			
			//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", null)
							:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null));
		}catch (TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		} catch (APIException e){
			logger.error("API Exception while getting Books for Book Group " + e.getErrorCodes() + e.getMessage());
			return handleErrorPageAsBean(e.getErrorCodes(),apiVersion,reqBean.getLocale());
		} catch (Exception e){
			logger.error("Error while getting Books for Book Group " + ErrorCodes.INTERNAL_ERROR, e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,apiVersion,reqBean.getLocale());
		}
		//return null;
	}
	
	@Autowired
	SearchCounter searchCounter;
	
	@RequestMapping(value={APIJSONURLCONSTANTS.APPTUS_SEARCH}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Object searchBooks( @AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,SearchJsonApptusRequestBean reqBean, 
			BindingResult result,HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean ) throws Exception{
		
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<SearchJsonApptusRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		try{ 							
			searchCounter.handleMessage("success");
			BooksForBookGroupsApptusResponseWrapper wrapper = apptusApiService.getBooksFromKeywordSearchDevice(reqBean,apiVersion,user);			
			if(wrapper.getBooks() == null || wrapper.getBooks().isEmpty()){				
				wrapper.setBooks(new ArrayList<ProductInformation>());
			}
			wrapper.setStatus(ErrorCodes.OK.getMessage());			
			if (logger.isTraceEnabled())logger.trace("APPTUS API CONTROLLER  :: searchProduct :: end :: wrappertBean ::: "+wrapper);
			
			if( apiVersion > CONSTANTS.API_VERSION_6_2 )
				//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_gt_6_2);
				return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
						?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", json_searchbooksfilter_exclude_gt_6_2)
								:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_gt_6_2));
			
			//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", null)
							:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null));
		}catch(TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("APPTUS API CONTROLLER -> searchProduct ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting Search Results "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
			//throw e;
		}
	}
	
	
	@RequestMapping(value={APIJSONURLCONSTANTS.APPTUS_SEARCH_FACETS}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Object searchBookswithFacets( @AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,SearchJsonApptusRequestBean reqBean, 
			BindingResult result,HttpServletRequest request, @AppRequestBeanInfo APITokenRequestBean tokenbean ) throws Exception{
		
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<SearchJsonApptusRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		try{ 							
			searchCounter.handleMessage("success");
			SearchWithFacetsApptusResponseWrapper wrapper = apptusApiService.getBooksFromSearchWithFactes(reqBean,apiVersion,user);			
			if(wrapper.getBooks() == null || wrapper.getBooks().isEmpty()){				
				wrapper.setBooks(new ArrayList<ProductInformation>());
			}
			wrapper.setStatus(ErrorCodes.OK.getMessage());			
			if (logger.isTraceEnabled())logger.trace("APPTUS API CONTROLLER  :: searchProduct :: end :: wrappertBean ::: "+wrapper);
			
			if( apiVersion > CONSTANTS.API_VERSION_6_2 )
				//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_facets_fields);	
				return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
						?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", json_searchbooksfilter_exclude_facets_fields)
								:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_facets_fields));
			
			//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", null)
							:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null));
		}catch(TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("APPTUS API CONTROLLER -> searchProduct ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting Search Results "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
			//throw e;
		}
	}
	
	
	@RequestMapping(value={APIJSONURLCONSTANTS.APPTUS_ONLY_FACETS}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Object onlyFacets( @AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,SearchJsonApptusRequestBean reqBean, 
			BindingResult result,HttpServletRequest request, @AppRequestBeanInfo APITokenRequestBean tokenbean ) throws Exception{
		
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<SearchJsonApptusRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		try{ 							
			searchCounter.handleMessage("success");
			SearchWithFacetsApptusResponseWrapper wrapper = apptusApiService.getOnlyFactes(reqBean,apiVersion,user);			
			if(wrapper.getBooks() == null || wrapper.getBooks().isEmpty()){				
				wrapper.setBooks(new ArrayList<ProductInformation>());
			}
			wrapper.setStatus(ErrorCodes.OK.getMessage());			
			if (logger.isTraceEnabled())logger.trace("APPTUS API CONTROLLER  :: searchProduct :: end :: wrappertBean ::: "+wrapper);
			
			if( apiVersion > CONSTANTS.API_VERSION_6_2 )
				//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_facets_fields);	
				return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
						?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", json_searchbooksfilter_exclude_facets_fields)
								:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_facets_fields));
			
			//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", null)
							:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null));
		}catch(TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("APPTUS API CONTROLLER -> searchProduct ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting Search Results "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
			//throw e;
		}
	}
	
	@RequestMapping(value={APIJSONURLCONSTANTS.APPTUS_ADVANCED_SEARCH}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Object advancedSearchBooks( @AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,SearchJsonApptusRequestBean reqBean, 
			BindingResult result,HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception{
		
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<SearchJsonApptusRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		try{ 							
			searchCounter.handleMessage("success");
			AdvancedSearchWrapper wrapper = apptusApiService.getAdvancedSearch(reqBean,apiVersion,user);			
			/*if(wrapper.getBooks() == null || wrapper.getBooks().isEmpty()){				
				wrapper.setBooks(new ArrayList<ProductInformation>());
			}*/
			wrapper.setStatus(ErrorCodes.OK.getMessage());			
			if (logger.isTraceEnabled())logger.trace("APPTUS API CONTROLLER  :: searchProduct :: end :: wrappertBean ::: "+wrapper);
			
			if( apiVersion > CONSTANTS.API_VERSION_6_2 )
				//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_gt_6_2);
				return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
						?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", json_searchbooksfilter_exclude_gt_6_2)
								:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", json_searchbooksfilter_exclude_gt_6_2));
			
			//return getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion,user.getWarning()), "productdetails_filter", null)
							:getJsonMappingfilter(new APIJsonWrapper(wrapper,apiVersion), "productdetails_filter", null));
		}catch(TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("APPTUS API CONTROLLER -> searchProduct ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting Search Results "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
			//throw e;
		}
	}
	
	@RequestMapping(value={APIJSONURLCONSTANTS.APPTUS_REPORT_CLICKS}, method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody JsonWrapper reportClicks( @AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,ReportClickApptusRequestBean reqBean, 
			BindingResult result,HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean ) throws Exception{
	
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		APIJsonWrapper apiProductWrapper = new APIJsonWrapper();
		Set<ConstraintViolation<ReportClickApptusRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
		try{ 			
			apptusApiService.reportClicksToApptus(reqBean,apiVersion, user);
			apiProductWrapper.setEmptyData();			
			if (logger.isTraceEnabled())logger.trace("APPTUS API CONTROLLER  :: APPTUS_REPORT_CLICKS :: end :: wrappertBean ::: "+apiProductWrapper);
			return apiProductWrapper;
		}catch(TokenNotValidException e){
			if( logger.isTraceEnabled() )logger.trace( "User with token (" + reqBean.getToken() + ") got expired");
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("APPTUS API CONTROLLER -> APPTUS_REPORT_CLICKS ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting Search Results "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
			//throw e;
		}		
	}
	
	@RequestMapping(value =	{ APIJSONURLCONSTANTS.AUTOCOMPLETE}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody JsonWrapper autoComplete(@AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,HttpServletRequest request, 
			AutoCompleteJsonRequestBean reqBean, BindingResult result,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
	
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<AutoCompleteJsonRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
	    
		try{  			
			AutoCompleteWrapper wrapper = apptusApiService.getAutoComplete(reqBean, apiVersion,user);
			if(wrapper.getTerms() == null || wrapper.getTerms().isEmpty()){			
				wrapper.setTerms(new ArrayList<String>());
			}
			wrapper.setStatus(ErrorCodes.OK.getMessage());		
			if (logger.isTraceEnabled())logger.trace("DeviceSearchController  :: autoComplete :: end :: wrappertBean ::: "+wrapper);
			//return new APIJsonWrapper(wrapper,apiVersion);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?new APIJsonWrapper(wrapper,apiVersion,user.getWarning()):new APIJsonWrapper(wrapper,apiVersion));
		}catch(TokenNotValidException e){
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("DeviceSearchController -> autoComplete ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting the autoComplete "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
		}
	}
	
	
	@RequestMapping(value =	{ APIJSONURLCONSTANTS.SEARCH_HISTORY}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody JsonWrapper searchHistory(@AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,HttpServletRequest request, 
			SearchHistoryRequestBean reqBean, BindingResult result,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
	
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<SearchHistoryRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
	    
		try{  			
			AutoCompleteWrapper wrapper = apptusApiService.getSearchHistory(user);
			if(wrapper.getTerms() == null || wrapper.getTerms().isEmpty()){			
				wrapper.setTerms(new ArrayList<String>());
			}
			wrapper.setStatus(ErrorCodes.OK.getMessage());		
			if (logger.isTraceEnabled())logger.trace("DeviceSearchController  :: SEARCH_HISTORY :: end :: wrappertBean ::: "+wrapper);
			//return new APIJsonWrapper(wrapper,apiVersion);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?new APIJsonWrapper(wrapper,apiVersion,user.getWarning()):new APIJsonWrapper(wrapper,apiVersion));
		}catch(TokenNotValidException e){
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("DeviceSearchController -> SEARCH_HISTORY ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting the SEARCH_HISTORY "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
		}
	}
	
	
	@RequestMapping(value =	{ APIJSONURLCONSTANTS.TOP_SEARCHES}, method = RequestMethod.GET, produces ={ MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody JsonWrapper topSearches(@AppUserToken UserAuthToken authtoken, @AppUserInfo User user, @PathVariable("api_version")Double apiVersion,HttpServletRequest request, 
			SearchHistoryRequestBean reqBean, BindingResult result,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
	
		setDefaultParams(reqBean, apiVersion, authtoken, user,tokenbean);
		Set<ConstraintViolation<SearchHistoryRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
	    
		try{  			
			AutoCompleteWrapper wrapper = apptusApiService.getTopSearches(user);
			if(wrapper.getTerms() == null || wrapper.getTerms().isEmpty()){			
				wrapper.setTerms(new ArrayList<String>());
			}
			wrapper.setStatus(ErrorCodes.OK.getMessage());		
			if (logger.isTraceEnabled())logger.trace("DeviceSearchController  :: SEARCH_HISTORY :: end :: wrappertBean ::: "+wrapper);
			//return new APIJsonWrapper(wrapper,apiVersion);
			return ((user!=null&&user.getWarning()!=null && user.getWarning().getCode()>0)
					?new APIJsonWrapper(wrapper,apiVersion,user.getWarning()):new APIJsonWrapper(wrapper,apiVersion));
		}catch(TokenNotValidException e){
			return handleErrorPageAsBean(e,apiVersion,reqBean.getLocale());
		}catch (APIException e) {
			logger.error("DeviceSearchController -> SEARCH_HISTORY ",e);
			if(e.getErrorCodes()!=null)
				return handleErrorPageAsBean(e.getErrorCodes(),reqBean.getLocale());
			else 
				throw e;
		}catch (Exception e) {
			logger.error("Error while getting the SEARCH_HISTORY "+ErrorCodes.INTERNAL_ERROR,e);
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
		}
	}

}

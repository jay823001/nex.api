package se.frescano.nextory.app.apptus.services;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.AptusThird;
import se.frescano.nextory.apptus.request.RelatedBookZonePanel;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class ApptusRelatedBookZoneService implements TagExtractor<AptusThird>{

private Logger logger = LoggerFactory.getLogger(ApptusRelatedBookZoneService.class);
	
	/*@Autowired
	private	WebClient webclient;
	*/
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	//private UriComponentsBuilder uriBuilder = null;
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;
	
		
	/*@PostConstruct
	public void init(){
		uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getRelatedUrl());
	}*/
	
	/*final TagExtractor<AptusThird> tagrelatedEx  = new TagExtractor<AptusThird>() {
		@Override
		public CallState extract(AptusThird rsp,long starttime,Throwable ex) {
			CallState callState = new CallState("AUTO_COMPLETE", starttime,aptusConfiguration.getAutoCompleteUrlNew());
			Tag panel = Tag.of("panel", "searchpanel"); 
			if(rsp!=null){
				callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
			}else if(ex!=null){
				if(ex instanceof RestException)
					callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
				else
					callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}else{
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}
		  return callState;	
		}
	};*/
	
	public AptusThird search(RelatedBookZonePanel panel){				
		
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() );
		UriComponents uriComponents = uriBuilder.path(applicationpropconfig.getRelatedUrl()).replaceQueryParams(panel).build();

		logger.info("APPTUS Related REQUEST--->" + uriComponents.toUri());	 				
		try{
			/*Mono<AptusThird> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AptusThird.class);			
			return result.block();*/
			return abstractresttemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, AptusThird.class,this);
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUriString(), e);
			/*e.printStackTrace();*/
		}
		//result1.subscribe(TestSubscribe::handleResponse);
		return null;
	}
	
	@Override
	public CallState extract(AptusThird rsp,long starttime,Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getRelatedUrl());
		Tag panel = Tag.of("panel", "relatedbookpanel"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	  return callState;	
	}
}

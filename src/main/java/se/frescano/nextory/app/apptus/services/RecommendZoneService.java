package se.frescano.nextory.app.apptus.services;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse2;
import se.frescano.nextory.apptus.request.RecommendZone;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class RecommendZoneService implements TagExtractor<ApptusFinalResponse2>{

	private Logger logger = LoggerFactory.getLogger(RecommendZoneService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
		
	private UriComponentsBuilder genericpanelUriBuilder;
	
	@Autowired
	private AbstractRestTempleteClient abstractRestTempleteClient;
	
	//@PostConstruct
	public UriComponentsBuilder  init(){
		return genericpanelUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getRecommendUrl() );
	}
	
	public ApptusFinalResponse2 genericPanelSearch(RecommendZone panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS RECOMMEND ZONE BROUGHT REQUEST--->" + uriComponents.toUri());	 				
		try{			
			/*Mono<ApptusFinalResponse2> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono( ApptusFinalResponse2.class );			
			return result.block();*/
			
			return abstractRestTempleteClient.exchange(uriComponents.toUriString(), HttpMethod.GET, ApptusFinalResponse2.class,this);
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);			
		}		
		return null;
	}

	@Override
	public CallState extract(ApptusFinalResponse2 rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getRecommendUrl());
		Tag panel = Tag.of("panel", "recommended"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;
	}
	
	
}

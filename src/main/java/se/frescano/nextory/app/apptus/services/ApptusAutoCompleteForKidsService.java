package se.frescano.nextory.app.apptus.services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.AutoCompleteHolder;
import se.frescano.nextory.apptus.model.Completions;
import se.frescano.nextory.apptus.request.AutoCompleteForKidsPanel;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class ApptusAutoCompleteForKidsService implements TagExtractor<AutoCompleteHolder>{

	private Logger logger = LoggerFactory.getLogger(ApptusAutoCompleteForKidsService.class);
	
/*	@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder uriBuilder = null;
	
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;
		
	//@PostConstruct
	public UriComponentsBuilder init(){
		return uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getAutoCompleteUrlNewKids());
	}
	
	/*final TagExtractor<AutoCompleteHolder> tagksearchEx  = new TagExtractor<AutoCompleteHolder>() {
		@Override
		public CallState extract(AutoCompleteHolder rsp,long starttime,Throwable ex) {
			CallState callState = new CallState("AUTO_COMPLETE_FOR_KIDS", starttime,aptusConfiguration.getAutoCompleteUrlNewKids());
			Tag panel = Tag.of("panel", "searchpanel"); 
			if(rsp!=null){
				callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
			}else if(ex!=null){
				if(ex instanceof RestException)
					callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
				else
					callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}else{
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}
		  return callState;	
		}
	};*/
	
	public ArrayList<Completions> search(AutoCompleteForKidsPanel panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS AUTOCOMPLETE REQUEST--->" + uriComponents.toUri());	 				
		try{
			/*Mono<AutoCompleteHolder> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AutoCompleteHolder.class);			
			AutoCompleteHolder	response = result.block();*/
			AutoCompleteHolder	response = abstractresttemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, AutoCompleteHolder.class,this);
			if( response != null && response.getAutocomplete() != null && response.getAutocomplete().size() >0 )
				return response.getAutocomplete().get(0).getCompletions();
								
			return new ArrayList<Completions>(0);
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUriString(), e);		
		}		
		return new ArrayList<Completions>(0);
	}
	@Override
	public CallState extract(AutoCompleteHolder rsp,long starttime,Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getAutoCompleteUrlNewKids());
		Tag panel = Tag.of("panel", "autocompleteforkids"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	  return callState;	
	}
}

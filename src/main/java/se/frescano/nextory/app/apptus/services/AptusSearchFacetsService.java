package se.frescano.nextory.app.apptus.services;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.AptusSecondSilverSearch;
import se.frescano.nextory.apptus.model.AptusSecondSilverSearchWithFacets;
import se.frescano.nextory.apptus.request.SearchPanel;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class AptusSearchFacetsService implements TagExtractor<AptusSecondSilverSearchWithFacets>{
	
	private Logger logger = LoggerFactory.getLogger(AptusSearchFacetsService.class);
/*	
	@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder 	searchUriBuilder = null;
	
	private final String SEARCH_ZONE = "searchzonewithfacets";
	
	//@PostConstruct
	public UriComponentsBuilder init(){
		return searchUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path( applicationpropconfig.getGenericUrl() + SEARCH_ZONE)
			      /*.query("arg.search_phrase={search_phrase}")
			      .query("arg.window_first={window_first}")
			      .query("arg.window_last={window_last}")
			      .query("arg.include_filter={include_filter}")
			      .query("arg.market_filter={market_filter}")
			      .query("arg.subscription_filter={subscription_filter}")
			      .query("arg.subscription_filter_1={subscription_filter_1}")
			      .query("arg.publisher_filter={publisher_filter}")
			      .query("arg.format_filter={format_filter}")
			      .query("arg.sort_by={sort_by}")
			      .query("arg.parent_category_filter={parent_category_filter}")
			      .query("market={market}")		      
			      .query("sessionKey={sessionKey}")
			      .query("customerKey={customerKey}")*/;	
	}
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;	
	/*final TagExtractor<AptusSecondSilverSearch> tagsearchEx  = new TagExtractor<AptusSecondSilverSearch>() {
		@Override
		public CallState extract(AptusSecondSilverSearch rsp,long starttime,Throwable ex) {
			CallState callState = new CallState(SEARCH_ZONE, starttime, aptusConfiguration.getGenericUrl() + SEARCH_ZONE);
			Tag panel = Tag.of("panel", "searchpanel"); 
			if(rsp!=null){
				callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
			}else if(ex!=null){
				if(ex instanceof RestException)
					callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
				else
					callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}else{
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}
		  return callState;	
		}
	};*/
	
	public AptusSecondSilverSearchWithFacets search(SearchPanel panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();
		logger.info("APPTUS SEARCH REQUEST--->" + uriComponents.toUri());	 				
		try{
			
			/*Mono<AptusSecondSilverSearch> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AptusSecondSilverSearch.class);*/
			AptusSecondSilverSearchWithFacets result = abstractresttemplate.exchange(uriComponents.toUriString(),HttpMethod.GET,AptusSecondSilverSearchWithFacets.class,this);
																  	 
			return result;
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);
			/*e.printStackTrace();*/
		}
		//result1.subscribe(TestSubscribe::handleResponse);
		return null;
	}
	
	@Override
	public CallState extract(AptusSecondSilverSearchWithFacets rsp,long starttime,Throwable ex) {
		CallState callState = new CallState("apptus", starttime, applicationpropconfig.getGenericUrl() + SEARCH_ZONE);
		Tag panel = Tag.of("panel", "searchpanel"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	  return callState;	
	}

}

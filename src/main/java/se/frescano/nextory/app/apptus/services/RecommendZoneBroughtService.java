package se.frescano.nextory.app.apptus.services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.ProductsAptus;
import se.frescano.nextory.apptus.model.ApptusFetchVo.ApptusFinalResponse3;
import se.frescano.nextory.apptus.request.RecommendZoneBought;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class RecommendZoneBroughtService implements TagExtractor<ApptusFinalResponse3>{

	private Logger logger = LoggerFactory.getLogger(RecommendZoneBroughtService.class);
	
/*	@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
		
	private UriComponentsBuilder genericpanelUriBuilder;
	
	@Autowired
	private AbstractRestTempleteClient abstractRestTempleteClient;
	
	//@PostConstruct
	public UriComponentsBuilder init(){
		return genericpanelUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getThoseWhoBought() )
			      /*.query("arg.author_filter={author_filter}")
			      .query("arg.category_filter={category_filter}")
			      .query("arg.child_filter={child_filter}")
			      .query("arg.exclude_filter={exclude_filter}")
			      .query("arg.format_filter={format_filter}")
			      .query("arg.include_filter={include_filter}")
			      .query("arg.market_filter={market_filter}")
			      .query("arg.product_key={product_key}")
			      .query("arg.publisher_filter={publisher_filter}")
			      .query("arg.series_filter={series_filter}")
			      .query("arg.subscription_filter={subscription_filter}")
			      .query("arg.variants_per_product={variants_per_product}")		      
			      .query("arg.window_first={window_first}")
			      .query("arg.window_last={window_last}")
			      .query("market={market}")		      
			      .query("sessionKey={sessionKey}")
			      .query("customerKey={customerKey}")*/;
	}
	
	public ArrayList<ProductsAptus> genericPanelSearch(RecommendZoneBought panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS RECOMMEND ZONE BROUGHT REQUEST--->" + uriComponents.toUri());	 				
		try{			
			/*Mono<ApptusFinalResponse3> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono( ApptusFinalResponse3.class )
					    ;
			
			result.flatMapMany(response ->{
				if( response.getThoseWhoBoughtAlsoBought() != null && response.getThoseWhoBoughtAlsoBought().get(0) != null 
					&& response.getThoseWhoBoughtAlsoBought().get(0).getProducts() != null ){
					return Mono.just( response.getThoseWhoBoughtAlsoBought().get(0).getProducts() );
				}
				return Mono.just(new ArrayList<ProductsAptus>(0));
			});*/
			ApptusFinalResponse3 response =  abstractRestTempleteClient.exchange(uriComponents.toUriString(), HttpMethod.GET, ApptusFinalResponse3.class,this);
			if( response.getThoseWhoBoughtAlsoBought() != null && response.getThoseWhoBoughtAlsoBought().get(0) != null 
					&& response.getThoseWhoBoughtAlsoBought().get(0).getProducts() != null ){
					return response.getThoseWhoBoughtAlsoBought().get(0).getProducts();
				}
			return new ArrayList<ProductsAptus>(0);
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);			
		}		
		return null;
	}

	@Override
	public CallState extract(ApptusFinalResponse3 rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime, applicationpropconfig.getThoseWhoBought());
		Tag panel = Tag.of("panel", "thosewhobought"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;
	}
}

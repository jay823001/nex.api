package se.frescano.nextory.app.apptus.services;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.request.GenericPanel;
import se.frescano.nextory.apptus.request.UpComming;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.api.response.ProductListWithCount;

@DependsOn("applicationpropconfig")
@Service
public class UpCommingService implements TagExtractor<String>{

	private Logger logger = LoggerFactory.getLogger(UpCommingService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder genericpanelUriBuilder = null;
	
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;
	
	
	private final String UPCOMMING="upcoming";
		
	//@PostConstruct
	public UriComponentsBuilder init(){
		return genericpanelUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getGenericUrl() + UPCOMMING);
	}
	
	public Map<String, List<ProductListWithCount>> genericPanelSearch(UpComming panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS UPCOMMING REQUEST--->" + uriComponents.toUri());	 				
		try{	
			String result = abstractresttemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, String.class,this);
			return Book2GoUtil.objectMapper.readValue(result ,new TypeReference<Map<String, List<ProductListWithCount>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching UPCOMMING results for URL -->" + uriComponents.toUri(), e);
			/*e.printStackTrace();*/
		}		
		return null;
	}

	@Override
	public CallState extract(String rsp, long starttime, Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getGenericUrl() + UPCOMMING);
		Tag panel = Tag.of("panel", "Upcomming"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	   return callState;	
		
	}
}

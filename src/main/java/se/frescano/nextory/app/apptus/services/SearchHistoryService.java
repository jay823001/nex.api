package se.frescano.nextory.app.apptus.services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.model.Phrases;
import se.frescano.nextory.apptus.model.SearchHistoryHolder;
import se.frescano.nextory.apptus.request.AutoCompletePanel;
import se.frescano.nextory.apptus.request.SearchHistoryPanel;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;

@Service
public class SearchHistoryService implements TagExtractor<SearchHistoryHolder>{

private Logger logger = LoggerFactory.getLogger(SearchHistoryService.class);
	
	/*@Autowired
	private	WebClient webclient;*/
	
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder uriBuilder = null;
	
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;
	
	//@PostConstruct
	public UriComponentsBuilder init(){
		return uriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getGenericUrl()+"search-history/recent-searches");
	}
	/*final TagExtractor<AutoCompleteHolder> tagksearchEx  = new TagExtractor<AutoCompleteHolder>() {
		@Override
		public CallState extract(AutoCompleteHolder rsp,long starttime,Throwable ex) {
			CallState callState = new CallState("AUTO_COMPLETE", starttime,aptusConfiguration.getAutoCompleteUrlNew());
			Tag panel = Tag.of("panel", "searchpanel"); 
			if(rsp!=null){
				callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
			}else if(ex!=null){
				if(ex instanceof RestException)
					callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
				else
					callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}else{
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}
		  return callState;	
		}
	};*/
	
	public ArrayList<Phrases> search(SearchHistoryPanel panel){				
		
		UriComponents uriComponents = init().replaceQueryParams(panel).build();

		logger.info("APPTUS SEARCH HISTORY REQUEST--->" + uriComponents.toUri());	 				
		try{
			/*Mono<AutoCompleteHolder> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono(AutoCompleteHolder.class);	
			AutoCompleteHolder	response = result.block();*/
			SearchHistoryHolder	response = abstractresttemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, SearchHistoryHolder.class,this);
			if( response.getRecentSearches() != null && response.getRecentSearches().size() >0 )
				return response.getRecentSearches().get(0).getPhrases();
			return new ArrayList<Phrases>(0);
		}catch(Exception e){
			logger.error("Exception occured while fetching search history results for URL -->" + uriComponents.toUriString(), e);			
		}		
		return new ArrayList<Phrases>(0);
	}
	
	@Override
	public CallState extract(SearchHistoryHolder rsp,long starttime,Throwable ex) {
		CallState callState = new CallState("apptus", starttime,applicationpropconfig.getGenericUrl()+"search-history/recent-searches?");
		Tag panel = Tag.of("panel", "searchHistoryPanel"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	  return callState;	
	}
}

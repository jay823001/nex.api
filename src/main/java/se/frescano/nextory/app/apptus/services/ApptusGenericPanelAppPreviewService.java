package se.frescano.nextory.app.apptus.services;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.apptus.request.GenericPanelAppPreviewRequest;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.api.response.ProductListWithCount;

@DependsOn("applicationpropconfig")
@Service
public class ApptusGenericPanelAppPreviewService {

	private Logger logger = LoggerFactory.getLogger(ApptusGenericPanelAppPreviewService.class);
	
	/*@Autowired
	private	WebClient webclient;
	*/
	@Autowired
	private	ApplicationPropertiesConfig	applicationpropconfig;
	
	private UriComponentsBuilder genericpanelUriBuilder = null;
	
	private final String  GENERIC_PANEL_NAME="generic_pannel_app_preview";
		
	@PostConstruct
	public void init(){
		genericpanelUriBuilder = UriComponentsBuilder.newInstance()
			      .scheme( applicationpropconfig.getHostscheme() ).host( applicationpropconfig.getHost() )
			      .path(applicationpropconfig.getGenericUrl() +GENERIC_PANEL_NAME)
			      /*.query("arg.authors_filter={authors_filter}")
			      .query("arg.bookid_filter_new={bookid_filter_new}")
			      .query("arg.child_filter={child_filter}")
			      .query("arg.exclude_filter={exclude_filter}")
			      .query("arg.facets={facets}")
			      .query("arg.format_filter={format_filter}")
			      .query("arg.include_filter={include_filter}")
			      .query("arg.locale={locale}")
			      .query("arg.market_filter={market_filter}")
			      .query("arg.parent_category_filter={parent_category_filter}")
			      .query("arg.presentation_attributes={presentation_attributes}")
			      .query("arg.publisher_filter={publisher_filter}")
			      .query("arg.relevance_sort={relevance_sort}")
			      .query("arg.search_attributes={search_attributes}")
			      .query("arg.search_phrase={search_phrase}")
			      .query("arg.selected_category={selected_category}")
			      .query("arg.subscription_filter={subscription_filter}")
			      .query("arg.variants_per_product={variants_per_product}")
			      .query("arg.window_first={window_first}")
			      .query("arg.window_last={window_last}")
			      .query("market={market}")		      
			      .query("sessionKey={sessionKey}")
			      .query("customerKey={customerKey}")*/;
	}
	
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;	
	
	
	/*final TagExtractor<String> taggenEx  = new TagExtractor<String>() {
		@Override
		public CallState extract(String rsp,long starttime,Throwable ex) {
			CallState callState = new CallState(GENERIC_PANEL_NAME, starttime,aptusConfiguration.getGenericUrl() + GENERIC_PANEL_NAME);
			Tag panel = Tag.of("panel", "searchpanel"); 
			if(rsp!=null){
				callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
			}else if(ex!=null){
				if(ex instanceof RestException)
					callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
				else
					callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}else{
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
			}
		  return callState;	
		}
	};*/
	public Map<String, List<ProductListWithCount>> genericPanelSearch(GenericPanelAppPreviewRequest panel){				
		
		UriComponents uriComponents = genericpanelUriBuilder.replaceQueryParams(panel).build();

		logger.info("APPTUS GENERIC SEARCH REQUEST--->" + uriComponents.toUri());	 				
		try{			
			/*Mono<String> result = webclient.get().uri(uriComponents.toUriString()).
					 retrieve()
					    .onStatus(HttpStatus::is4xxClientError  , clientResponse ->Mono.error(new Exception()))
					    .onStatus(HttpStatus::is5xxServerError  , clientResponse ->Mono.error(new Exception()))
					    .bodyToMono( String.class );	*/
			String result = abstractresttemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, String.class);
			return Book2GoUtil.objectMapper.readValue(result ,new TypeReference<Map<String, List<ProductListWithCount>>>() {});
		}catch(Exception e){
			logger.error("Exception occured while fetching search results for URL -->" + uriComponents.toUri(), e);
			/*e.printStackTrace();*/
		}		
		return null;
	}
	
	/*@Override
	public CallState extract(String rsp,long starttime,Throwable ex) {
		CallState callState = new CallState(GENERIC_PANEL_NAME, starttime,aptusConfiguration.getGenericUrl() + GENERIC_PANEL_NAME);
		Tag panel = Tag.of("panel", "genericpanelpreview"); 
		if(rsp!=null){
			callState.setTags(Tags.of(panel,Tag.of("status", HttpStatus.OK.name())));
		}else if(ex!=null){
			if(ex instanceof RestException)
				callState.setTags(Tags.of(panel,Tag.of("status", ((RestException)ex).getStatus().toString())));
			else
				callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}else{
			callState.setTags(Tags.of(panel,Tag.of("status", "error")));
		}
	  return callState;	
	}*/
}

package se.frescano.nextory.app.intereceptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.response.ErrorJsonWrapper;
import se.frescano.nextory.auth.exception.AuthenticationException;
import se.frescano.nextory.auth.rest.request.SERVICE_TYPE;
import se.frescano.nextory.auth.rest.request.TokenExtractReqBean;
import se.frescano.nextory.auth.rest.response.TokenExtractResponse;
import se.frescano.nextory.auth.service.AuthenticationServiceImpl;
import se.frescano.nextory.error.handlers.AppErrorHandler;
import se.frescano.nextory.error.handlers.WebErrorHandler;
import se.frescano.nextory.util.Book2GoUtil;

/**
 * This class is used for getting the fields for the test suite.
 * If the request parameter has render = true then the fields
 * will be sent to the jsp file.
 * @author Sathish Yellanty
 */

@Service
public class ApiInterceptor extends HandlerInterceptorAdapter implements AppErrorHandler, WebErrorHandler{
	
	private ArrayList<Double> 	supportedVersions;
	//private Logger 				logger 			= LoggerFactory.getLogger(ApiInterceptor.class);
	
	private static final	String	ZIPKIN_SPAN_ID = "X-B3-SpanId";
	private static final	String	ZIPKIN_TRANSACTION_ID = "X-B3-TraceId";
	private Logger logger = LoggerFactory.getLogger(ApiInterceptor.class);
		
	@Autowired
	private	AuthenticationServiceImpl		authenticationservice;
	
	public ApiInterceptor(){
		super();
	}
	/*
	 * (non-Javadoc)
	 * @see preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	//	logger.info("After handling the request");
		MDC.clear();
		if( System.getenv("MY_POD_NAME") != null )
			response.setHeader("X-Host-Header", System.getenv("MY_POD_NAME"));
		
		super.postHandle(request, response, handler, modelAndView);
	}
		
	@Override
	public boolean preHandle(final HttpServletRequest request,
			final HttpServletResponse response, final Object handlerMethod) throws Exception {
		
		//Data Dog tracing
		Book2GoUtil.setDDTraceIds();
		String requestURI = request.getRequestURI();
		requestURI = requestURI.replaceAll(request.getContextPath(), ""); 
		Double appVersion	= Book2GoUtil.getAPIVersion(requestURI);
		String apiBasePath = Book2GoUtil.getAPIBasePath(requestURI);
		String token 	= request.getParameter("token"); 
		String deviceid = request.getParameter("deviceid");
		String locale	= request.getParameter(CONSTANTS.LOCALE_PARAMNAME);
		requestURI = requestURI.replaceAll(apiBasePath, "");
		apiBasePath = apiBasePath.replaceAll("/"+appVersion+"/", "/{api_version}/");
		String tokenType = null; 
		String countrycode = null;
		
		//This is for  future implemention, not yet used for now. 
		String	extracted_token	= request.getHeader("token");
		String	extracted_deviceid	= request.getHeader("deviceid");
		String	extracted_locale	= request.getHeader(CONSTANTS.LOCALE_PARAMNAME);
		Boolean forceAuth =  Boolean.TRUE;
		try{
			
			request.setAttribute(CONSTANTS.APIVERSION, appVersion);
				
			 if(!supportedVersions.contains( appVersion ))
					throw new APIException(ErrorCodes.API_DEPRECATED);
			 
			 if( apiBasePath.equalsIgnoreCase(APIJSONURLCONSTANTS.APP_API_V1_BASE) && appVersion>APIJSONURLCONSTANTS.APP_API_VER_6_5 )//deprecate old url format for version 6.6 and above
				 throw new APIException(ErrorCodes.API_DEPRECATED);
				 
			 
			//validate locale
			if(!APIJSONURLCONSTANTS.SALT_URL.equalsIgnoreCase(requestURI)){	
				String localenew ="";
				if(StringUtils.isNotBlank(extracted_locale))
					localenew=extracted_locale;
				else if(StringUtils.isNotBlank(locale))
					localenew=locale;
				countrycode = authenticationservice.validateLocale(appVersion, localenew, SERVICE_TYPE.APP, request); 
			}
				
			TokenExtractResponse auth_response =	null;
			
			if( StringUtils.isNotBlank( extracted_token ) && StringUtils.isNotBlank( extracted_deviceid )){

				// auth_response 	= Book2GoUtil.objectMapper.readValue(extracted_token, TokenExtractResponse.class);
				//tokenType	= ( auth_response.getTokeninfo().getType() != null)?auth_response.getTokeninfo().getType().name():null;
				//authenticationservice.validateUserLocale( auth_response.getUserinfo(), appVersion, locale, SERVICE_TYPE.APP, request);
				TokenExtractReqBean tokenRequest = new TokenExtractReqBean(SERVICE_TYPE.APP, appVersion,extracted_token,null,extracted_deviceid, 
						forceAuth, locale,countrycode);
				auth_response  = authenticationservice.authenticateUser(tokenRequest, request);				
				tokenType	= ( auth_response.getTokeninfo().getType() != null)?auth_response.getTokeninfo().getType().name():null;	
			} 
			else if(StringUtils.isNotBlank(token)){
				TokenExtractReqBean tokenRequest = new TokenExtractReqBean(SERVICE_TYPE.APP, appVersion,token,null,deviceid, forceAuth, locale,countrycode);
				auth_response  = authenticationservice.authenticateUser(tokenRequest, request);				
				tokenType	= ( auth_response.getTokeninfo().getType() != null)?auth_response.getTokeninfo().getType().name():null;				
			}
			else if(Arrays.asList(APIJSONURLCONSTANTS.SECURE_API).contains(requestURI))
				{
					Enumeration e = request.getHeaderNames();
					String headers = null;
					while (e.hasMoreElements()) {
					      headers = (String) e.nextElement();
					logger.error("API Exception INPUT_MISSING " + ":::: "+headers+ ":::: " + request.getHeader(headers) + "  ::: ");
					}
					Enumeration e1 = request.getParameterNames();
					String headers1 = null;
					while (e1.hasMoreElements()) {
					      headers1 = (String) e1.nextElement();
					logger.error("API Exception INPUT_MISSING " + ":::: "+headers1+ ":::: " + request.getParameter(headers1) + "  ::: ");
					}
					logger.error("API Exception INPUT_MISSING " + ":::: "+request.getRequestURI());
					throw new APIException(ErrorCodes.INPUT_MISSING);
					
				}
			
		}catch(AuthenticationException e){			
			response.setContentType("application/json;charset=UTF-8");
			ErrorJsonWrapper errorJsonWrapper = new ErrorJsonWrapper(e.getErrorcode(),e.getErrormessage(), appVersion);		 			
			response.getWriter().write( Book2GoUtil.objectMapper.writeValueAsString(errorJsonWrapper));
			return false;
		}catch(APIException e){
			if(e.getErrorCodes()==null)e.setErrorCodes(ErrorCodes.INVALID_TOKEN);
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write( Book2GoUtil.objectMapper.writeValueAsString( handleErrorPageAsBean(e.getErrorCodes(),appVersion,locale)));
			return false;
		}catch(Exception e){
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write( Book2GoUtil.objectMapper.writeValueAsString( handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,appVersion,locale)));
			return false;
		}
	 
		if(tokenType!=null && CONSTANTS.TOKEN_TYPE.LT.name().equals(tokenType)){ 
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write( Book2GoUtil.objectMapper.writeValueAsString( handleErrorPageAsBean(ErrorCodes.UNAUTHORIZED_API_ACCESS,appVersion, locale)) );
			return false;
		}		
		/*try{			
			if(StringUtils.isNotBlank(token)){
				MDC.put(ZIPKIN_SPAN_ID, request.getHeader( ZIPKIN_SPAN_ID )); 
				MDC.put(ZIPKIN_TRANSACTION_ID, request.getHeader( ZIPKIN_TRANSACTION_ID ));				
			}else{
				MDC.clear();
			}			 
		}catch(Exception e){ 
			return true;
		}*/				
		return true;
	}
	/*
	private Double getAPIVersionNumber(HttpServletRequest request){
		Double version 	= 0.0;
		String url 		= request.getRequestURI();
		if( StringUtils.contains(url, "/api/") ){
			url	= StringUtils.substring( url, (StringUtils.indexOf(url, ("/api/")) + ("/api/".length())), (url.length() - 1));
			url	= StringUtils.substring( url, 0,StringUtils.indexOf(url, "/"));
			Double	dv	= null;
			try{
				dv	= Double.parseDouble(url);
				version = dv;
			}catch(Exception e){
				version = 0.0;
				e.getMessage();
			}			
		}
		return version;		
	}*/
			
	public ArrayList<Double> getSupportedVersions() {
		return supportedVersions;
	}
	public void setSupportedVersions(ArrayList<Double> supportedVersions) {
		this.supportedVersions = supportedVersions;
	}
	  
}

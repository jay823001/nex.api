package se.frescano.nextory.app.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface JsonWrapper {

	@JsonIgnore
	public Double getVersion();
	
}

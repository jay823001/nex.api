package se.frescano.nextory.app.response;

import java.util.Date;
import se.frescano.nextory.app.constants.CONSTANTS;

public class ReviewAndRating {
	
	//private static final long serialVersionUID = -2779412232016353413L;	
	private Double avgRatings; 	
	private int totalReviews;
	private int bookid;
	private int profileId;
	private Date createdDate;
	private int userRating;
	private String reviewComments; 
	
	
	public int getUserRating() {
		return userRating;
	}
	public void setUserRating(int userRating) {
		this.userRating = userRating;
	}
	public String getReviewComments() {
		return reviewComments;
	}
	public void setReviewComments(String reviewComments) {
		this.reviewComments = reviewComments;
	}
	public int getProfileId() {
		return profileId;
	}
	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getTotalReviews() {
		return totalReviews;
	}
	public void setTotalReviews(int userRatings) {
		this.totalReviews = userRatings;
	}

	public void setAvgRatings(Double avgRatings) {
		this.avgRatings = avgRatings;
	}
	public Double getAvgRatings() {
		return avgRatings;
	}
	public void updateWithDefaultValues(){
		int prevTotalReviews = this.totalReviews;
		this.totalReviews = this.totalReviews+CONSTANTS.DEFAULT_PRODUCT_RATING_COUNT;
		this.avgRatings = ((this.avgRatings*prevTotalReviews)+CONSTANTS.DEFAULT_PRODUCT_RATING)/this.totalReviews;
	}
	

}

package se.frescano.nextory.app.response;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.constants.ErrorCodes;




//@JsonPropertyOrder({"error"})
//@JsonRootName(value="error")
public class ErrorJsonWrapper  implements JsonWrapper {

	@JsonProperty(value="error")
	Map<String,Object> error;
	
	Double version;
	
	public ErrorJsonWrapper(int errorcode,String message) {
		error = new HashMap<String, Object>();
		error.put("code",errorcode);
		error.put("msg",message);
	}
	/**Used for web responses*/
	public ErrorJsonWrapper(ErrorCodes errorCode){
		error = new HashMap<String, Object>();
		error.put("code",errorCode.getErrorcode());
		error.put("msg",errorCode.getI18key());
	}
	/**Used for web responses*/
	public ErrorJsonWrapper(ErrorCodes errorCode, Object reason){
		error = new HashMap<String, Object>();
		error.put("code",errorCode.getErrorcode());
		error.put("msg",errorCode.getI18key());
		error.put("reason", reason);
	}
	
	public ErrorJsonWrapper(int errorcode,String message,Double version) {
		error = new HashMap<String, Object>();
		error.put("code",errorcode);
		error.put("msg",message);
		this.version= version;
	}

	@Override
	public Double getVersion() {
		// TODO Auto-generated method stub
		return version;
	}
	
	
}

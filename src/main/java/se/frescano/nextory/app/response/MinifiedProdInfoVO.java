package se.frescano.nextory.app.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.constants.ProductFormatEnum;
import se.frescano.nextory.app.product.response.DownloadFileVo;
import se.frescano.nextory.app.product.response.SeriesInfoBean;
import se.frescano.nextory.app.product.response.SyncSettingsVo;
import se.frescano.nextory.util.Book2GoUtil;

@JsonInclude(Include.NON_NULL)
public class MinifiedProdInfoVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6363232234139215366L;
	
	@JsonProperty( value = "id")
	private int productid;
	
	@JsonProperty( value = "type")
	private int formattype;
	
	@JsonProperty( value = "title")
	private String title;
	
	@JsonProperty( value = "imageurl")
	private String imageurl;
	
	@JsonProperty( value = "relatedbookid")
	private Integer relProductid;
	
	@JsonProperty( value = "relatedbookallowedinlibrary")
	private byte relatedbookallowedinlibrary;
		
	@JsonProperty( value = "descriptionfull")
	private String descriptionbrief	= "";
		
	@JsonProperty( value = "language")
	private String language;
	
	@JsonProperty( value = "publisher")
	private String publisher;
	
	@JsonProperty( value = "duration")
	private String duration;
	
	@JsonProperty( value = "weburl")
	private String weburl;
	
	@JsonProperty( value = "availableinlib")
	private int libraryStatus;
	
	@JsonProperty(value = "libid")
	private int libid;
	
	@JsonProperty( value = "allowedinlibrary")
	private byte allowedinlibrary;
	
	@JsonProperty( value = "currentpercentage")
	private String currentpercentage;
	
	@JsonProperty( value = "allowedforstring")
	private String allowed_subscriptions;
	
	@JsonProperty( value = "displaymessage")
	private String displaymessage;
		
	@JsonIgnore
	private SeriesInfoBean sereisinfo;
	
	@JsonIgnore
	private ReviewAndRating reviewAndRatings;

	@JsonProperty( value = "authors")
	private List<String> author;
	
	@JsonProperty( value = "narrators")
	private List<String> narrator;
	
	@JsonProperty( value = "translators")
	private List<String> translator;
	
	@JsonProperty( value = "numberinseries")
	private Integer numberInSeries;
	
	//@JsonSerialize( using = ProductPopularitySerializer.class)
	//private PopularityMap<String, Integer> popularity;
	
	@JsonProperty( value = "coverratio")
	private Double coverratio;
	
	@JsonProperty( value = "avgrate")
	private Double avgrating;
	
	@JsonProperty( value = "numberofrates")
	private Double totalratings;
	
	@JsonProperty( value = "synchsettings")
	private SyncSettingsVo	syncsettings;
		
	@JsonProperty( value = "file")
	private DownloadFileVo	file;
	
	
	@JsonIgnore	
	private Date publishedDate;
	
	@JsonIgnore
	private Date 	datemodified;	
	
	@JsonIgnore
	private String coverimage;
		
	@JsonIgnore
	private ProductFormatEnum format;
	
	@JsonIgnore
	private Integer relformattype;
	
	@JsonIgnore
	private List<String> relNarrator;
				
	@JsonIgnore
	private String completed;
	
	@JsonIgnore
	private Date syncDate;
	
	@JsonIgnore
	private String position;
	
	@JsonIgnore
	private String isbn;
	
	@JsonIgnore
	private boolean showseries;
	
	@JsonIgnore
	private Integer seriebookcount;
	
	@JsonIgnore
	private String cfi;
	
	@JsonIgnore
	private String idref;
		
	/*@JsonIgnore
	private NewFormatDetailsVO downloadFormat;*/
	
	@JsonIgnore
	private double price;
	
	@JsonIgnore
	private String allowed_for;
			
	@JsonIgnore
	private Integer booklengthvalue;
	
	@JsonIgnore
	private Integer countOfbooks;
	
	@JsonIgnore
	private Date lastDate;
		
	
	@JsonIgnore
	private Integer showInVolume;
	
	@JsonIgnore
	private String sortOn;
	/*private Double requestVersion;*/
	
	@JsonIgnore
	private double imageHeight;
	
	@JsonIgnore
	private double imageWidth;
		
	@JsonProperty( value = "series")
	public String	getSeriesname(){
		if( this.sereisinfo != null && StringUtils.isNotBlank( this.sereisinfo.getSeriesName() ) )
			return this.sereisinfo.getSeriesName();
		else
			return null;
	}
	
	@JsonProperty( value = "numberinseries")
	public Integer	getSeriesnumber(){
		if( this.sereisinfo != null && this.sereisinfo.getSequence() > 0 )
			return this.sereisinfo.getSequence();
		else
			return null;
	}
	
	@JsonProperty( value = "pubdate")
	public String	getPublishedDateStr(){
		if( this.publishedDate != null )
			return Book2GoUtil.dateFormatForVer5( this.publishedDate.getTime());
		else
			return "";
	}
	
	@JsonProperty( value = "datemodified")
	public String	getModifiedDateStr(){
		if( this.datemodified != null )
			return Book2GoUtil.dateFormatForVer5( this.datemodified.getTime());
		else
			return "";
	}
	
	@JsonProperty( value = "avgrate")
	public Double	getAverageRatting(){
		if( this.reviewAndRatings != null && this.reviewAndRatings.getAvgRatings() != null)
			return this.reviewAndRatings.getAvgRatings();
		else
			return null;
	}
	
	@JsonProperty( value = "numberofrates")
	public Integer	getNumberOfRates(){
		if( this.reviewAndRatings != null && this.reviewAndRatings.getTotalReviews() > 0 )
			return this.reviewAndRatings.getTotalReviews();
		else
			return null;
	}
	
	@JsonProperty( value = "userrate")
	public Integer	getUserRates(){
		if( this.reviewAndRatings != null && this.reviewAndRatings.getUserRating() > 0 )
			return this.reviewAndRatings.getUserRating();
		else
			return 0;
	}
	
	public Double getAvgrating() {
		return avgrating;
	}
	public void setAvgrating(Double avgrating) {
		this.avgrating = avgrating;
	}
	
	public double getImageHeight() {
		return imageHeight;
	}
	public void setImageHeight(double imageHeight) {
		this.imageHeight = imageHeight;
	}
	public double getImageWidth() {
		return imageWidth;
	}
	public void setImageWidth(double imageWidth) {
		this.imageWidth = imageWidth;
	}
		
	public Integer getNumberInSeries() {
		return numberInSeries;
	}
	public void setNumberInSeries(Integer numberInSeries) {
		this.numberInSeries = numberInSeries;
	}
	/*public Double getRequestVersion() {
		return requestVersion;
	}
	public void setRequestVersion(Double requestVersion) {
		this.requestVersion = requestVersion;
	}*/
	public Integer getShowInVolume() {
		return showInVolume;
	}
	public void setShowInVolume(Integer showInVolume) {
		this.showInVolume = showInVolume;
	}
	public String getSortOn() {
		return sortOn;
	}
	public void setSortOn(String sortOn) {
		this.sortOn = sortOn;
	}
	public SeriesInfoBean getSereisinfo() {
		return sereisinfo;
	}
	public void setSereisinfo(SeriesInfoBean sereisinfo) {
		this.sereisinfo = sereisinfo;
	}
	public Date getLastDate() {
		return lastDate;
	}
	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}
	public Integer getCountOfbooks() {
		return countOfbooks;
	}
	public void setCountOfbooks(Integer countOfbooks) {
		this.countOfbooks = countOfbooks;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Integer getBooklengthvalue() {
		return booklengthvalue;
	}
	public void setBooklengthvalue(Integer booklengthvalue) {
		this.booklengthvalue = booklengthvalue;
	}	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCoverimage() {
		return coverimage;
	}
	public void setCoverimage(String coverimage) {
		this.coverimage = coverimage;
	}
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	
	public int getFormattype() {
		return formattype;
	}
	public void setFormattype(int formattype) {
		this.formattype = formattype;
	}
	public ProductFormatEnum getFormat() {
		return format;
	}
	public void setFormat(ProductFormatEnum format) {
		this.format = format;
	}
	public Integer getRelProductid() {
		return relProductid;
	}
	public void setRelProductid(Integer relProductid) {
		this.relProductid = relProductid;
	}
	public Integer getRelformattype() {
		return relformattype;
	}
	public void setRelformattype(int relformattype) {
		this.relformattype = relformattype;
	}
	public List<String> getAuthor() {
		return author;
	}
	public void setAuthor(List<String> author) {
		this.author = author;
	}
	public List<String> getNarrator() {
		return narrator;
	}
	
	public List<String> getRelNarrator() {
		return relNarrator;
	}
	public void setRelNarrator(List<String> relNarrator) {
		this.relNarrator = relNarrator;
	}
	/*public se.frescano.nextory.app.product.response.PopularityMap<String, Integer> getPopularity() {
		return popularity;
	}
	public void setPopularity(se.frescano.nextory.app.product.response.PopularityMap<String, Integer> popularity) {
		this.popularity = popularity;
	}*/
	
	/*@SuppressWarnings("unchecked")
	public void setPopularity(Integer popularity) {
		se.frescano.nextory.app.product.response.PopularityMap<String, Integer> popularityMap = new se.frescano.nextory.app.product.response.PopularityMap<String, Integer>();
		popularityMap.put("popularity", popularity);
		this.put("popularity", popularity);
	}*/
	public Date getPublishedDate() {
		return publishedDate;
	}
	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}
	public String getDescriptionbrief() {
		return descriptionbrief;
	}
	public void setDescriptionbrief(String descriptionbrief) {
		this.descriptionbrief = descriptionbrief;
	}
	public int getLibraryStatus() {
		return libraryStatus;
	}
	public void setLibraryStatus(int libraryStatus) {
		this.libraryStatus = libraryStatus;
	}
	public int getLibid() {
		return libid;
	}
	public void setLibid(int libid) {
		this.libid = libid;
	}
	public String getCurrentpercentage() {
		return currentpercentage;
	}
	public void setCurrentpercentage(String currentpercentage) {
		this.currentpercentage = currentpercentage;
	}
	public String getCompleted() {
		return completed;
	}
	public void setCompleted(String completed) {
		this.completed = completed;
	}
	public Date getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Date datemodified) {
		this.datemodified = datemodified;
	}
	
	/*public NewFormatDetailsVO getDownloadFormat() {
		return downloadFormat;
	}
	public void setDownloadFormat(NewFormatDetailsVO downloadFormat) {
		this.downloadFormat = downloadFormat;
	}*/
	public Date getSyncDate() {
		return syncDate;
	}
	public void setSyncDate(Date syncDate) {
		this.syncDate = syncDate;
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getAllowed_for() {
		return allowed_for;
	}
	public void setAllowed_for(String allowed_for) {
		this.allowed_for = allowed_for;
	}
	public String getAllowed_subscriptions() {
		return allowed_subscriptions;
	}
	public void setAllowed_subscriptions(String allowed_subscriptions) {
		this.allowed_subscriptions = allowed_subscriptions;
	}
	public boolean isShowseries() {
		return showseries;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public void setShowseries(boolean showseries) {
		this.showseries = showseries;
	}
	public Integer getSeriebookcount() {
		return seriebookcount;
	}
	public void setSeriebookcount(Integer seriebookcount) {
		this.seriebookcount = seriebookcount;
	}
	
	public byte getRelatedbookallowedinlibrary() {
		return relatedbookallowedinlibrary;
	}
	public void setRelatedbookallowedinlibrary(byte relatedbookallowedinlibrary) {
		this.relatedbookallowedinlibrary = relatedbookallowedinlibrary;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MinifiedProdInfoVO [productid=");
		builder.append(productid);
		builder.append(", title=");
		builder.append(title);
		builder.append(", coverimage=");
		builder.append(coverimage);
		builder.append(", author=");
		builder.append(author);
		builder.append(", narrator=");
		builder.append(narrator);
		builder.append(", translator=");
		builder.append(translator);
		builder.append(", formattype=");
		builder.append(formattype);
		builder.append(", format=");
		builder.append(format);
		builder.append(", relProductid=");
		builder.append(relProductid);
		builder.append(", relformattype=");
		builder.append(relformattype);
		builder.append(", relNarrator=");
		builder.append(relNarrator);
		//builder.append(", popularity=");
		//builder.append(popularity);
		builder.append(", publishedDate=");
		builder.append(publishedDate);
		builder.append(", descriptionbrief=");
		builder.append(descriptionbrief);
		builder.append(", libraryStatus=");
		builder.append(libraryStatus);
		builder.append(", libid=");
		builder.append(libid);
		builder.append(", currentpercentage=");
		builder.append(currentpercentage);
		builder.append(", completed=");
		builder.append(completed);
		builder.append(", datemodified=");
		builder.append(datemodified);
		builder.append(", syncDate=");
		builder.append(syncDate);
		builder.append(", position=");
		builder.append(position);
		builder.append(", showseries=");
		builder.append(showseries);
		builder.append(", seriebookcount=");
		builder.append(seriebookcount);
		/*builder.append(", downloadFormat=");
		builder.append(downloadFormat);*/
		builder.append(", price=");
		builder.append(price);
		builder.append(", allowed_for=");
		builder.append(allowed_for);
		builder.append(", allowedinlibrary=");
		builder.append(allowedinlibrary);
		builder.append(", allowed_subscriptions=");
		builder.append(allowed_subscriptions);
		builder.append(", language=");
		builder.append(language);
		builder.append(", publisher=");
		builder.append(publisher);
		builder.append(", booklengthvalue=");
		builder.append(booklengthvalue);
		builder.append(", countOfbooks=");
		builder.append(countOfbooks);
		builder.append(", lastDate=");
		builder.append(lastDate);
		builder.append(", sereisinfo=");
		builder.append(sereisinfo);
		builder.append("]");
		return builder.toString();
	}
	public String getCfi() {
		return cfi;
	}
	public void setCfi(String cfi) {
		this.cfi = cfi;
	}
	public String getIdref() {
		return idref;
	}
	public void setIdref(String idref) {
		this.idref = idref;
	}
	
	public ReviewAndRating getReviewAndRatings() {
		
		return reviewAndRatings;
	}
	
	public void setReviewAndRatings(ReviewAndRating reviewAndRatings) {
		this.reviewAndRatings = reviewAndRatings;
	}
	public Double getCoverratio() {
		return coverratio;
	}
	public void setCoverratio(Double coverratio) {
		this.coverratio = coverratio;
	}
	
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getWeburl() {
		return weburl;
	}
	public void setWeburl(String weburl) {
		this.weburl = weburl;
	}
	public byte getAllowedinlibrary() {
		return allowedinlibrary;
	}
	public void setAllowedinlibrary(byte allowedinlibrary) {
		this.allowedinlibrary = allowedinlibrary;
	}
	public String getDisplaymessage() {
		return displaymessage;
	}
	public void setDisplaymessage(String displaymessage) {
		this.displaymessage = displaymessage;
	}
	public List<String> getTranslator() {
		return translator;
	}
	public void setTranslator(List<String> translator) {
		this.translator = translator;
	}
	public void setNarrator(List<String> narrator) {
		this.narrator = narrator;
	}
	public SyncSettingsVo getSyncsettings() {
		return syncsettings;
	}
	public void setSyncsettings(SyncSettingsVo syncsettings) {
		this.syncsettings = syncsettings;
	}
	public DownloadFileVo getFile() {
		return file;
	}
	public void setFile(DownloadFileVo file) {
		this.file = file;
	}
	public Double getTotalratings() {
		return totalratings;
	}
	public void setTotalratings(Double totalratings) {
		this.totalratings = totalratings;
	}
	
	
}

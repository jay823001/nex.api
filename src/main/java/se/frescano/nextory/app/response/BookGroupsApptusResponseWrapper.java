package se.frescano.nextory.app.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.product.response.BookGroupVOApptus;
import se.frescano.nextory.app.request.APIWrapperBean;

//@JsonSerialize(using=BookGroupApptusConvertor.class)
@JsonInclude(Include.NON_NULL)
public class BookGroupsApptusResponseWrapper extends APIWrapperBean {
	
	
	@JsonProperty( value = "bookgroups", required =true)
	private  List<BookGroupVOApptus> bookGroups= null;
	
	@JsonIgnore
	private Double apiversion;
	
	@JsonIgnore
	private String type;
	
	@JsonProperty( value = "bookgroupcount")
	private Integer size;
	
	@JsonIgnore
	private Integer start;
	
	

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Double getApiversion() {
		return apiversion;
	}

	public void setApiversion(Double apiversion) {
		this.apiversion = apiversion;
	}

	public List<BookGroupVOApptus> getBookGroups() {
		return bookGroups;
	}

	public void setBookGroups(List<BookGroupVOApptus> bookGroups) {
		this.bookGroups = bookGroups;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}

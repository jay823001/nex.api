package se.frescano.nextory.app.response;

import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.request.APIWrapperBean;
/*import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

@XStreamAlias("rsp")
*/public class ErrorWrapper extends APIWrapperBean{
	
	

//	@XStreamConverter(ErrorConvertor.class)
//	@XStreamAlias("err")
	public ErrorCodes errorcode;
	
	public ErrorCodes getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(ErrorCodes errorcode) {
		this.errorcode = errorcode;
	}

	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ErrorWrapper [errorcode=");
		builder.append(errorcode);
		builder.append("]");
		return builder.toString();
	}
}

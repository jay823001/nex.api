package se.frescano.nextory.app.response;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import se.frescano.nextory.app.exception.APIDataOverRideException;
import se.frescano.nextory.app.request.APIWrapperBean;


public class APIJsonWrapper implements JsonWrapper{

	
	public APIJsonWrapper() {
		super();		
	}
	public APIJsonWrapper(Object data) {
		super();
		this.data = data; 
	}
	
	public APIJsonWrapper(Object data,Double version) {
		super();
		this.data = data;
		this.version= version;
		if(data instanceof APIWrapperBean)
			((APIWrapperBean)data).setApiVersion(version);
	}
	
	
	public APIJsonWrapper(Object data,Double version, Object warning) {
		super();
		this.data = data;
		this.version= version;
		this.warning= warning;
		if(data instanceof APIWrapperBean)
			((APIWrapperBean)data).setApiVersion(version);
	}
	
	

	Object data = null;
	Double version;
	@JsonInclude(Include.NON_NULL)
	Object warning =null;

	public Double getVersion() {
		return version;
	}

	public void setVersion(Double version) {
		this.version = version;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	public Object getWarning() {
		return warning;
	}
	public void setWarning(Object warning) {
		this.warning = warning;
	}
	public void setEmptyData() throws APIDataOverRideException {
		if(this.data ==null){
			data = new HashMap<String,String>();
		}else{
			throw new APIDataOverRideException(" The data is already set and cannot be set to empty data");
		}
	}
}


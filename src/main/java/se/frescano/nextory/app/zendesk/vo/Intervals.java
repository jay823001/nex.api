package se.frescano.nextory.app.zendesk.vo;

public class Intervals implements Comparable<Object> {
	private String end_time;

	private String start_time;

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	@Override
	public String toString() {
		return "Intervals [start_time = " + start_time + ", end_time = " + end_time + "]";
	}

	@Override
	public int compareTo(Object o) {
		Intervals interval = (Intervals) o;
		return start_time.compareTo(interval.start_time);
	}
}

package se.frescano.nextory.app.zendesk.vo;

import java.util.Arrays;

public class SchedulePayload 
{
    private Schedules[] schedules;

    private String url;

    public Schedules[] getSchedules ()
    {
        return schedules;
    }

    public void setSchedules (Schedules[] schedules)
    {
        this.schedules = schedules;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "SchedulePayload [schedules = "+Arrays.toString(schedules)+", url = "+url+"]";
    }
}

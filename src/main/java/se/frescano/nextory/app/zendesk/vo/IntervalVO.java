/**
 * 
 */
package se.frescano.nextory.app.zendesk.vo;

public class IntervalVO {

	private long endTime;
	private long startTime;
	private double startHour;
	private double endHour;

	public IntervalVO(double startHour, double endHour, long startTime, long endTime) {
		this.startHour = startHour;
		this.endHour = endHour;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setStartHour(double startHour) {
		this.startHour = startHour;
	}

	public void setEndHour(double endHour) {
		this.endHour = endHour;
	}

	public long getEndTime() {
		return endTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public double getStartHour() {
		return startHour;
	}

	public double getEndHour() {
		return endHour;
	}

	@Override
	public String toString() {
		return "IntervalVO [endTime=" + endTime + ", startTime=" + startTime + ", startHour=" + startHour + ", endHour="
				+ endHour + "]";
	}

}

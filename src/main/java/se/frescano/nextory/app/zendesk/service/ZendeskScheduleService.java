package se.frescano.nextory.app.zendesk.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.zendesk.vo.Holiday;
import se.frescano.nextory.app.zendesk.vo.Holidays;
import se.frescano.nextory.app.zendesk.vo.IntervalVO;
import se.frescano.nextory.app.zendesk.vo.Intervals;
import se.frescano.nextory.app.zendesk.vo.SchedulePayload;
import se.frescano.nextory.app.zendesk.vo.Schedules;
import se.frescano.nextory.app.zendesk.vo.ZendeskDateResponseVO;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.util.InMemoryCache;

@DependsOn("applicationpropconfig")
@Service("zendeskScheduleService")
public class ZendeskScheduleService/* extends AbstractRestClient*/{

	/*@Autowired
	private AbstractRestClient abstractRestClient;*/
	
	@Autowired
	private	AbstractRestTempleteClient	abstractresttemplate;
	
	@Autowired
	private ApplicationPropertiesConfig	applicationpropconfig;
	
	private static final Logger logger = LoggerFactory.getLogger(ZendeskScheduleService.class);

	/**
	 * An interval represents an active business-hours period.Interval times are
	 * expressed as the number of minutes since the start of the week. Sunday is
	 * considered the first day of the week.For instance, 720 is equivalent to
	 * Sunday at noon (12 * 60)
	 * 
	 * @see <a href=
	 *      "https://developer.zendesk.com/rest_api/docs/core/schedules">Zendesk
	 *      Schedules</a>
	 * @return <tt>true</tt> if the day start_time and end_time is absent
	 * 
	 */
	
	@PostConstruct
	public void initializeRestClient(){		
	}
	
	private  Map<Integer, List<IntervalVO>> getBusinessHours(SchedulePayload schedulePayload, int dayOfWeek) 
	{		
		List<Intervals> intervalList = new ArrayList<Intervals>();
		Map<Integer, List<IntervalVO>> weekDayIntervalMap = new LinkedHashMap<Integer, List<IntervalVO>>();
		final int BASE_START_TIME = 1440;

		Schedules[] schedules = schedulePayload.getSchedules();
		if (null != schedules) 
		{
			for (Schedules schedule : schedules) 
			{
				Intervals[] intervals = schedule.getIntervals();
				if (null != intervals) 
				{
					intervalList.addAll(Arrays.asList(intervals));
					Collections.sort(intervalList);
					if (logger.isTraceEnabled())
					{
						logger.trace(" Sorted Interval List " + intervalList);
					}
					if(logger.isDebugEnabled())
						logger.debug(intervalList.toString());
					//System.out.println(intervalList);
					for (Intervals interval : intervalList) 
					{
						int intervalStartTime = Integer.parseInt(interval.getStart_time());
						int intervalEndTime = Integer.parseInt(interval.getEnd_time());
						int minutes = intervalStartTime - BASE_START_TIME;
						int weekday = ((minutes) / (24 * 60)) + 1;
						double starthour = (double)(intervalStartTime - weekday * BASE_START_TIME) / 60;
						double endhour = (double)(intervalEndTime - weekday * BASE_START_TIME) / 60;
						if(logger.isDebugEnabled())
							logger.debug("week " + weekday + " starthour " + starthour + " end hour " + endhour);
						//System.out.println("week " + weekday + " starthour " + starthour + " end hour " + endhour);
						List<IntervalVO> intervalVOList = weekDayIntervalMap.get(weekday);
						if (null != intervalVOList) 
						{
							intervalVOList.add(new IntervalVO(starthour, endhour, intervalStartTime, intervalEndTime));
							weekDayIntervalMap.put(weekday, intervalVOList);
						} else 
						{
							intervalVOList = new ArrayList<IntervalVO>();
							intervalVOList.add(new IntervalVO(starthour, endhour, intervalStartTime, intervalEndTime));
							weekDayIntervalMap.put(weekday, intervalVOList);
						}

					}
				}
			}
		}
		
		/**
		 * Need to handle scenario like if there are no schedule then what
		 * status to be updated will get updated data?
		 */
		return weekDayIntervalMap;
	}

	private static int getTodaysDayOfWeek() 
	{
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		
		return dayOfWeek - 1;
	}
	/**
	 * Example Replace every occurence of {} with value
	 * @param url
	 * @param patternStart
	 * @param patternEnd
	 * @param values
	 * @return
	 */
	public static String getPathVariableURL(String url, String patternStart,String patternEnd, Map<String, String> pathVariable) 
	{
		StringBuilder builder = new StringBuilder(url);
		for (Entry<String,String> entry : pathVariable.entrySet()) 
		{
			int start;
			String pattern = patternStart + entry.getKey() + patternEnd;
			String value = entry.getValue();
			while ((start = builder.indexOf(pattern)) != -1) 
			{
				builder.replace(start, start + pattern.length(), value);
			}
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param type
	 * @param username
	 * @param password
	 * @param url
	 * @param customHeaders
	 * @param queryParams
	 * @return
	 * @throws Exception 
	 * @throws RestException 
	 */
	public <T extends Object> T getZendeskResponse(Class<T> type, String url,Map<String, Object> customHeaders,
			HashMap<String, String> queryParams) throws Exception, RestException{
		/*Response response = null;
		try{*/
			/*response = get(url, customHeaders, queryParams);
			T payload = getEntity(response, type);*/
		String username = applicationpropconfig.getZENDESK_USER_NAME();
		String password = applicationpropconfig.getZENDESK_PWD();   //  "d3^QzB%dUReN";	
		T payload = abstractresttemplate.exchange(url, HttpMethod.GET, null, username, password, type);
		return payload;		
	}

	public static Date getNextDay(Date dt)
	{
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 1);
		return c.getTime();
	}

	public ZendeskDateResponseVO getBusinessHours() throws Exception, RestException{
		DateTime todaysdate = DateTime.now().withTimeAtStartOfDay();
		if(logger.isTraceEnabled()){
			logger.trace(" Time Start of the Day ::"+todaysdate);
			logger.trace(" zenDesk Time Start  of the Day ::"+todaysdate);
		}
		
		if(InMemoryCache.zendeskDateResponseVO==null  ||  InMemoryCache.zendeskDate.isBefore(todaysdate)){
			if(logger.isTraceEnabled())logger.trace("Business Hours not in memorycache...Adding ::"+todaysdate);
			InMemoryCache.zendeskDateResponseVO = loadBusinessHours();
			InMemoryCache.zendeskDate = todaysdate;
		}  
		return InMemoryCache.zendeskDateResponseVO;	
	}
	
	private ZendeskDateResponseVO loadBusinessHours() throws Exception, RestException
	{
		String dateFormat =  applicationpropconfig.getZENDESK_DATE_FORMAT();  		//    "yyyy-MM-dd HH:mm a";
		String zendeskScheduleURL = applicationpropconfig.getZENDESK_SCHEDULE_URL(); //  "https://nextory.zendesk.com/api/v2/business_hours/schedules.json";
		String sampleUrl = "https://nextory.zendesk.com/api/v2/business_hours/schedules/{id}/holidays/{id1}.json";
		String zendeskHolidaysURL = applicationpropconfig.getZENDESK_HOLIDAY_URL();  //  "https://nextory.zendesk.com/api/v2/business_hours/schedules/{id}/holidays.json";
		Map<String,String> pathVariable= new HashMap<String,String>();
		pathVariable.put("id","78892");
		pathVariable.put("id1","78892");
		zendeskHolidaysURL =getPathVariableURL(zendeskHolidaysURL,"{","}",pathVariable);
		sampleUrl =getPathVariableURL(sampleUrl,"{","}",pathVariable);
		int value = getTodaysDayOfWeek();
		//System.out.println("DayOfWeek:"+value);
		Holidays holidays = getZendeskResponse(Holidays.class,zendeskHolidaysURL,null,null);
		
		SchedulePayload schedulePayload =getZendeskResponse(SchedulePayload.class, zendeskScheduleURL ,null,null);
		
		Map<Integer, List<IntervalVO>> weekDayIntervalMap = getBusinessHours(schedulePayload, value);
		
		ZendeskDateResponseVO zendeskDateResponseVO = getAPIResponse(dateFormat, value, weekDayIntervalMap, holidays);
		//Long timeInMillis= getDateTimeInMillis(dateFormat,new Date(),9,00,Calendar.AM);
		//Date date = getDateFromMillis(dateFormat,timeInMillis);
		//System.out.println(date);
		return zendeskDateResponseVO;	
	}

	private static ZendeskDateResponseVO getAPIResponse(String dateFormat, int value,
			Map<Integer, List<IntervalVO>> weekDayIntervalMap, Holidays holidays) 
	{
		ZendeskDateResponseVO zendeskDateResponseVO= new ZendeskDateResponseVO();
		boolean isTodayHoliday = isTodayHoliday(dateFormat,holidays);		
		List<IntervalVO> currentDayIntervalVOList = weekDayIntervalMap.containsKey(value)?weekDayIntervalMap.get(value):null;
		if(null == currentDayIntervalVOList || isTodayHoliday)
		{
			zendeskDateResponseVO.setOpenToday(false);
			zendeskDateResponseVO.setTodayStartTimeMillis("");
			zendeskDateResponseVO.setTodayEndTimeMillis("");
			List<IntervalVO> nextDayIntervalVOList = weekDayIntervalMap.containsKey(value+1)?weekDayIntervalMap.get(value+1):null;
			if(null == nextDayIntervalVOList)
			{
				zendeskDateResponseVO.setNextDayStartTimeMillis("");
				zendeskDateResponseVO.setNextDayEndTimeMillis("");
			}else
			{
				for(IntervalVO intervalVO: nextDayIntervalVOList)
				{
					zendeskDateResponseVO.setOpenToday(true);
					int startHour = (int)intervalVO.getStartHour();
					int startMin = (int)((intervalVO.getStartHour() - Math.floor(intervalVO.getStartHour()))*60);
					int endHour = (int)intervalVO.getEndHour();
					int endMin = (int)((intervalVO.getEndHour() - Math.floor(intervalVO.getEndHour()))*60);
					zendeskDateResponseVO.setNextDayStartTimeMillis(getDateTimeInMillis(dateFormat,getNextDay(new Date()),startHour,startMin).toString());
					zendeskDateResponseVO.setNextDayEndTimeMillis(getDateTimeInMillis(dateFormat,getNextDay(new Date()),endHour,endMin).toString());
				}
			}
		}else
		{
			for(IntervalVO intervalVO: currentDayIntervalVOList)
			{
				zendeskDateResponseVO.setOpenToday(true);
				int startHour = (int)intervalVO.getStartHour();
				int startMin = (int)((intervalVO.getStartHour() - Math.floor(intervalVO.getStartHour()))*60);
				int endHour = (int)intervalVO.getEndHour();
				int endMin = (int)((intervalVO.getEndHour() - Math.floor(intervalVO.getEndHour()))*60);
				zendeskDateResponseVO.setTodayStartTimeMillis(getDateTimeInMillis(dateFormat,new Date(),startHour,startMin).toString());
				zendeskDateResponseVO.setTodayEndTimeMillis(getDateTimeInMillis(dateFormat,new Date(),endHour,endMin).toString());
			}
			List<IntervalVO> nextDayIntervalVOList = weekDayIntervalMap.containsKey(value+1)?weekDayIntervalMap.get(value+1):null;
			if(null == nextDayIntervalVOList)
			{
				zendeskDateResponseVO.setNextDayStartTimeMillis("");
				zendeskDateResponseVO.setNextDayEndTimeMillis("");
			}else
			{
				for(IntervalVO intervalVO: nextDayIntervalVOList)
				{
					zendeskDateResponseVO.setOpenToday(true);
					int startHour = (int)intervalVO.getStartHour();
					int startMin = (int)((intervalVO.getStartHour() - Math.floor(intervalVO.getStartHour()))*60);
					int endHour = (int)intervalVO.getEndHour();
					int endMin = (int)((intervalVO.getEndHour() - Math.floor(intervalVO.getEndHour()))*60);
					zendeskDateResponseVO.setNextDayStartTimeMillis(getDateTimeInMillis(dateFormat,getNextDay(new Date()),startHour,startMin).toString());
					zendeskDateResponseVO.setNextDayEndTimeMillis(getDateTimeInMillis(dateFormat,getNextDay(new Date()),endHour,endMin).toString());
				}
			}
		}
		return zendeskDateResponseVO;
	}

	private static Long getDateTimeInMillis(String dateFormat,Date date,int hour, int min) 
	{
		Calendar calendar = Calendar.getInstance();
		//SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, min);
		calendar.set(Calendar.SECOND, 0);
		if(logger.isDebugEnabled())
			logger.debug("Date:"+date+" Time in Millis:"+calendar.getTimeInMillis());
		//System.out.println("Date:"+date+" Time in Millis:"+calendar.getTimeInMillis());
		return calendar.getTimeInMillis();
	}


	private static Date getDateFromString(String dateFormat,String date) 
	{
		Date convertedDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		try
		{
			convertedDate = sdf.parse(date);
		}catch(ParseException parseException)
		{
			parseException.printStackTrace();
		}
		return convertedDate;
	}

	private static boolean isTodayHoliday(String dateFormat,Holidays holidays)
	{
		String[] dateTimePart = dateFormat.split(" ");
		boolean status = false;
		if(null == holidays || null == holidays.getHolidays())
		{
			status = false;
		}else
		{
			for(Holiday holiday : holidays.getHolidays())
			{
				Date startDate = getDateFromString(dateTimePart[0],holiday.getStart_date()); 
				Date endDate = getDateFromString(dateTimePart[0],holiday.getEnd_date());
				Date today = new Date();
				if(today.equals(startDate) || today.equals(endDate) || (today.after(startDate) && today.before(endDate))) 
				{
					status = true;
					break;
				}			
			}
		}
		return status;
	}
}

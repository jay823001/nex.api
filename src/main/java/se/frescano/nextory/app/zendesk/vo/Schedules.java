package se.frescano.nextory.app.zendesk.vo;

import java.util.Arrays;

public class Schedules
{
    private String id;

    private String time_zone;

    private String updated_at;

    private String name;

    private String created_at;

    private Intervals[] intervals;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTime_zone ()
    {
        return time_zone;
    }

    public void setTime_zone (String time_zone)
    {
        this.time_zone = time_zone;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public Intervals[] getIntervals ()
    {
        return intervals;
    }

    public void setIntervals (Intervals[] intervals)
    {
        this.intervals = intervals;
    }

    @Override
    public String toString()
    {
        return "Schedules [id = "+id+", time_zone = "+time_zone+", updated_at = "+updated_at+", name = "+name+", created_at = "+created_at+", intervals = "+Arrays.toString(intervals)+"]";
    }
}
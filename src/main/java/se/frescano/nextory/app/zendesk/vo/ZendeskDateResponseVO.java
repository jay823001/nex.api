package se.frescano.nextory.app.zendesk.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZendeskDateResponseVO {
	
	@JsonProperty( value = "isOpenToday")
	private boolean isOpenToday;
	
	@JsonProperty( value = "todayStartTimeMillis")
	private String todayStartTimeMillis;
	
	@JsonProperty( value = "todayEndTimeMillis")
	private String todayEndTimeMillis;
	
	@JsonProperty( value = "nextDayStartTimeMillis")
	private String nextDayStartTimeMillis;
	
	@JsonProperty( value = "nextDayEndTimeMillis")
	private String nextDayEndTimeMillis;

	public boolean isOpenToday() {
		return isOpenToday;
	}

	public void setOpenToday(boolean isOpenToday) {
		this.isOpenToday = isOpenToday;
	}

	public String getTodayStartTimeMillis() {
		return todayStartTimeMillis;
	}

	public void setTodayStartTimeMillis(String todayStartTimeMillis) {
		this.todayStartTimeMillis = todayStartTimeMillis;
	}

	public String getTodayEndTimeMillis() {
		return todayEndTimeMillis;
	}

	public void setTodayEndTimeMillis(String todayEndTimeMillis) {
		this.todayEndTimeMillis = todayEndTimeMillis;
	}

	public String getNextDayStartTimeMillis() {
		return nextDayStartTimeMillis;
	}

	public void setNextDayStartTimeMillis(String nextDayStartTimeMillis) {
		this.nextDayStartTimeMillis = nextDayStartTimeMillis;
	}

	public String getNextDayEndTimeMillis() {
		return nextDayEndTimeMillis;
	}

	public void setNextDayEndTimeMillis(String nextDayEndTimeMillis) {
		this.nextDayEndTimeMillis = nextDayEndTimeMillis;
	}

	@Override
	public String toString() {
		return "ZendeskDateResponseVO [isOpenToday=" + isOpenToday + ", todayStartTimeMillis=" + todayStartTimeMillis
				+ ", todayEndTimeMillis=" + todayEndTimeMillis + ", nextDayStartTimeMillis=" + nextDayStartTimeMillis
				+ ", nextDayEndTimeMillis=" + nextDayEndTimeMillis + "]";
	}

}

package se.frescano.nextory.app.zendesk.vo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Holidays {

	private List<Holiday> holidays = new ArrayList<Holiday>();
	private String url;

	/**
	 * 
	 * @return The holidays
	 */
	@JsonProperty("holidays")
	public List<Holiday> getHolidays() {
		return holidays;
	}

	/**
	 * 
	 * @param holidays
	 *            The holidays
	 */
	@JsonProperty("holidays")
	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	/**
	 * 
	 * @return The url
	 */
	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	/**
	 * 
	 * @param url
	 *            The url
	 */
	@JsonProperty("url")
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Holidays [holidays=" + holidays + ", url=" + url + "]";
	}

}
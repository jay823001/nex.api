package se.frescano.nextory.app.salt.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.request.APIWrapperBean;

public class BookCountWrapper extends APIWrapperBean{
	
	@JsonProperty( value = "ebookcount")
	private String ebookcount;
	
	@JsonProperty( value = "audiobookcount")
	private String audiobookcount;
	
	@JsonProperty( value = "totalbookcount")
	private String totalbookcount;
	
	public String getEbookcount() {
		return ebookcount;
	}
	public void setEbookcount(String ebookcount) {
		this.ebookcount = ebookcount;
	}
	public String getAudiobookcount() {
		return audiobookcount;
	}
	public void setAudiobookcount(String audiobookcount) {
		this.audiobookcount = audiobookcount;
	}
	public String getTotalbookcount() {
		return totalbookcount;
	}
	public void setTotalbookcount(String totalbookcount) {
		this.totalbookcount = totalbookcount;
	}
	
	
}

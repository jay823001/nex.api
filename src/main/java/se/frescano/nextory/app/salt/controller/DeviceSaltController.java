package se.frescano.nextory.app.salt.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.nextory.app.api.spring.method.resolver.AppRequestBeanInfo;
import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIDataOverRideException;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.request.APIController;
import se.frescano.nextory.app.request.APITokenRequestBean;
import se.frescano.nextory.app.request.SaltRequestBean;
import se.frescano.nextory.app.request.ServerStatusRequestBean;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.JsonWrapper;
import se.frescano.nextory.app.salt.response.BookCountWrapper;
import se.frescano.nextory.app.salt.response.SaltWrapper;
import se.frescano.nextory.util.InMemoryCache;

@RestController
public class DeviceSaltController extends APIController {

	Logger logger = LoggerFactory.getLogger(DeviceSaltController.class);
	
	@Autowired
	private Validator validator;
	
	@Autowired
	CacheManager cacheManager;
	
	 //json converter
	//@RequestMapping(value = {APIJSONURLCONSTANTS.V5.SALT_URL,APIJSONURLCONSTANTS.V5_1.SALT_URL}, method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
	@RequestMapping(value = {APIJSONURLCONSTANTS.SALT_URL}, method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
	public JsonWrapper  getSaltJson(@PathVariable("api_version")Double apiVersion, SaltRequestBean requestBean,
			BindingResult result, HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
		
		
		//Long d = System.currentTimeMillis();
		Collection<String> caches = cacheManager.getCacheNames();		
		for (Iterator<String> iterator = caches.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			if(StringUtils.startsWith(string, "nest.category"))
				cacheManager.getCache(string).clear();
		}
		
		setHeaderParams(requestBean,tokenbean);	
		Set<ConstraintViolation<SaltRequestBean>> violations = validator.validate(requestBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, requestBean.getLocale());
	    }
		
	    
		/*ErrorCodes errorCodes = validate(logger, requestBean, result, request);
		if (!errorCodes.equals(ErrorCodes.OK))
			return handleErrorPageAsBean(errorCodes,apiVersion,requestBean.getLocale());
		if(logger.isTraceEnabled())logger.trace("request value for SALT_URL" + requestBean);*/

		
		SaltWrapper salt = new SaltWrapper();
		String saltval = InMemoryCache.salt;
		salt.setSalt(saltval); // need to relook
		salt.setStatus(ErrorCodes.OK.getMessage());
		if(logger.isTraceEnabled())logger.trace("salt value from Inmemory :: " + salt);
		//ModelAndView mav = new ModelAndView("bookXmlView", "xstreamresponse",	salt);
		
		return new APIJsonWrapper(salt);
	}
	//json Convertter
	//@RequestMapping(value = { APIJSONURLCONSTANTS.V5.PING_VER,APIJSONURLCONSTANTS.V5_1.PING_VER}, method = RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	@RequestMapping(value = { APIJSONURLCONSTANTS.PING_VER}, method = RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	public JsonWrapper getServerStatusJson(@PathVariable("api_version")Double apiVersion, ServerStatusRequestBean reqBean,
			BindingResult result, HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
		if (logger.isTraceEnabled())
			logger.trace( ": DeviceSaltController  :: getServerStatus :: start :: requestBean ::: "
					+ reqBean);
		setHeaderParams(reqBean,tokenbean);	
		Set<ConstraintViolation<ServerStatusRequestBean>> violations = validator.validate(reqBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, reqBean.getLocale());
	    }
	    
		/*	ErrorCodes errorCodes = validate(logger, reqBean, result, request);
		if (!errorCodes.equals(ErrorCodes.OK)) {
			if (logger.isInfoEnabled())
				logger.info(" DeviceSaltController  :: validation failed ");
			return handleErrorPageAsBean(errorCodes,reqBean.getLocale());
		}*/
		APIJsonWrapper apiProductWrapper = new APIJsonWrapper();
		try {
			//ErrorWrapper apiProductWrapper = new ErrorWrapper();
			//apiProductWrapper.setData("");
			apiProductWrapper.setEmptyData();
			if (logger.isTraceEnabled())
				logger.trace( ": DeviceSaltController  :: getServerStatus :: end :: wrapperBean ::: "
						+ apiProductWrapper);
		} catch (APIException e) {
			logger.error("@@@@@@@@@@@@@@ APIException -> getServerStatus  "
					+ e.getMessage());
			return handleErrorPageAsBean(e.getErrorCodes() != null ? e
					.getErrorCodes() : ErrorCodes.AUTHENTICATION_ERROR,reqBean.getLocale());
		}catch (APIDataOverRideException e) {
			logger.error("@@@@@@@@@@@@@@ APIDataOverRideException -> getServerStatus  " + e.getMessage());
			return handleErrorPageAsBean(ErrorCodes.INTERNAL_ERROR,reqBean.getLocale());
		}
		return apiProductWrapper;
	}

    //json Converter 
	//@RequestMapping(value = { APIJSONURLCONSTANTS.V5.BOOK_COUNT_API,APIJSONURLCONSTANTS.V5_1.BOOK_COUNT_API}, method = RequestMethod.GET)
	@RequestMapping(value = {APIJSONURLCONSTANTS.BOOK_COUNT_API}, method = RequestMethod.GET)
	public JsonWrapper bookCountJson(@PathVariable("api_version")Double apiVersion, SaltRequestBean requestBean,
			BindingResult result, HttpServletRequest request,@AppRequestBeanInfo APITokenRequestBean tokenbean) throws Exception {
		if (logger.isTraceEnabled())
			logger.trace( ": DeviceSaltController  :: bookCount :: start :: requestBean ::: "
					+ requestBean);
		/*ErrorCodes errorCodes = validate(logger, requestBean, result, request);
		if (!errorCodes.equals(ErrorCodes.OK)){
			if (logger.isInfoEnabled())
				logger.info(" DeviceSaltController  :: validation failed ");
			return handleErrorPageAsBean(errorCodes,requestBean.getLocale());
		}*/
		setHeaderParams(requestBean,tokenbean);	
		Set<ConstraintViolation<SaltRequestBean>> violations = validator.validate(requestBean);
	     
	    if (violations != null && violations.size() > 0)
	    {
	        return handleErrorPageAsBean(ErrorCodes.INPUT_MISSING, apiVersion, requestBean.getLocale());
	    }
		BookCountWrapper wrapper = new BookCountWrapper();
		wrapper.setTotalbookcount(InMemoryCache.totalBookCount);
		wrapper.setEbookcount(InMemoryCache.eBookCount);
		wrapper.setAudiobookcount(InMemoryCache.audioBookCount);
		wrapper.setStatus(ErrorCodes.OK.getMessage());
		if (logger.isTraceEnabled())
			logger.trace( ": DeviceSaltController  :: bookCount :: end :: wrapperBean ::: "
					+ wrapper);
		return new APIJsonWrapper(wrapper);
	}
}

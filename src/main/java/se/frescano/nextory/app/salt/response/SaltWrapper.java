package se.frescano.nextory.app.salt.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import se.frescano.nextory.app.request.APIWrapperBean;


public class SaltWrapper extends APIWrapperBean{

	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SaltWrapper [salt=");
		builder.append(salt);
		builder.append("]");
		return builder.toString();
	}

	@JsonProperty( value = "salt")
	private String salt;

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	
	
	
}
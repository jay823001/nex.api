package se.frescano.nextory.customer;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookIdData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1561118954140805946L;
	
	@JsonProperty("data")
	private BookIdList data;

	
	@JsonProperty("data")
	public BookIdList getData() {
		return data;
	}

	
	@JsonProperty("data")
	public void setData(BookIdList data) {
		this.data = data;
	}
	
	
	

}

package se.frescano.nextory.customer;

import java.io.Serializable;
import java.util.List;

public class BookIdList implements Serializable{
	
	private static final long serialVersionUID = 4899169658741802364L;
	/**
	 * 
	 */
	
	private List<Integer> bookidlist;

	public List<Integer> getBookidlist() {
		return bookidlist;
	}

	public void setBookidlist(List<Integer> bookidlist) {
		this.bookidlist = bookidlist;
	}
	
	
	
}

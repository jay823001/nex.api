package se.frescano.nextory.error.handlers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

public interface WebErrorHandler {

	static final Logger logger = LoggerFactory.getLogger(AppErrorHandler.class);	
	
	default void sendErrorMsg(HttpServletResponse response, WEBErrorCodeEnum errorCode) {
		GenericAPIResponse apiResponse	=	new GenericAPIResponse(HttpStatus.FORBIDDEN,
				errorCode,errorCode.getMessage() );	 		
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		try {
			response.getWriter().write(Book2GoUtil.objectMapper.writeValueAsString(apiResponse));
		} catch (IOException e) { 
			logger.error("sendErrorMsg::Failed to write error msg ",e.getMessage());
		}
	}
}

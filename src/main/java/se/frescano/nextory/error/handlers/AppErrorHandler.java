package se.frescano.nextory.error.handlers;

import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.TokenNotValidException;
import se.frescano.nextory.app.response.ErrorJsonWrapper;
import se.frescano.nextory.util.Book2GoUtil;

public interface AppErrorHandler {

	static final Logger logger = LoggerFactory.getLogger(AppErrorHandler.class);	 
	
	default ErrorJsonWrapper handleErrorPageAsBean(ErrorCodes errorCode,String language){
		logger.info("handleErrorPageAsBean >> ErrorCodes :: {}", errorCode);
		 String key=errorCode.getI18key();
		 Locale locale=null;
		 /*if(!StringUtils.isBlank(language))
			 locale=new Locale(language);*/
		 if(!StringUtils.isBlank(language))
			 locale=  stringToLocale(language);
		 String errormsg=Book2GoUtil.getLocalizedString(locale,key);
		 if(org.apache.commons.lang3.StringUtils.isBlank(errormsg))
			 errormsg = errorCode.getMessage();
		ErrorJsonWrapper errorJsonWrapper = new ErrorJsonWrapper(errorCode.getErrorcode(),errormsg);
		return errorJsonWrapper;
	}
	
	default ErrorJsonWrapper handleErrorPageAsBean(ErrorCodes errorCode ,Double apiversion,String localeString){
		 
		logger.info("handleErrorPageAsBean >> ErrorCodes with apiversion :: {}/{}", errorCode,apiversion);
				
		 String key=errorCode.getI18key();
		 Locale locale=null;
		 if(!StringUtils.isBlank(localeString))
			 locale=  stringToLocale(localeString);
		 String errormsg=Book2GoUtil.getLocalizedString(locale,key);
		 if(org.apache.commons.lang3.StringUtils.isBlank(errormsg))
			 errormsg = errorCode.getMessage(); 
		ErrorJsonWrapper errorJsonWrapper = new ErrorJsonWrapper(errorCode.getErrorcode(),errormsg,apiversion);
		return errorJsonWrapper;
	}
	
	default ErrorJsonWrapper handleErrorPageAsBean(TokenNotValidException te ,Double apiversion, String language){
		ErrorCodes errorCode = te.getErrorCodes(); 
		List<String> args = te.getArgs();
		logger.info("handleErrorPageAsBean >> ErrorCodes with apiversion :: {}/{}", errorCode,apiversion);
				
		String key=errorCode.getI18key();
		Locale locale=null;
		/*if(!StringUtils.isBlank(language))
			locale=new Locale(language);*/
		 if(!StringUtils.isBlank(language))
			 locale=  stringToLocale(language);
		String errormsg=Book2GoUtil.getLocalizedString(locale,key);
		if(org.apache.commons.lang3.StringUtils.isBlank(errormsg))
			errormsg = errorCode.getMessage();
		else if(args!=null && args.size()>0){
			MessageFormat mf = new MessageFormat(errormsg); 
			errormsg = mf.format(args.toArray());
		}  
		ErrorJsonWrapper errorJsonWrapper = new ErrorJsonWrapper(errorCode.getErrorcode(),errormsg,apiversion);
		return errorJsonWrapper;
	}
	
	default Locale stringToLocale(String s) {
		StringTokenizer tempStringTokenizer = new StringTokenizer(s,"_");
		String l ="";
		String c = "";
		if(tempStringTokenizer.hasMoreTokens())
			l = (String) tempStringTokenizer.nextElement();
		if(tempStringTokenizer.hasMoreTokens())
			c = (String) tempStringTokenizer.nextElement();
		if(StringUtils.isBlank(c) || StringUtils.isBlank(l))
			return null;
		return new Locale(l,c);
	}
}

package se.frescano.nextory.auth.exception;

public class AuthenticationException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6773490647536912247L;
	private int 	errorcode;
	private String	errormessage;
	
	public AuthenticationException(int 	errorcode, String	errormessage){
		this.errorcode = errorcode;
		this.errormessage = errormessage;
	}
	
	public int getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
	public String getErrormessage() {
		return errormessage;
	}
	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}
	
	

}

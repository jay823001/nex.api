package se.frescano.nextory.auth.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Base64;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import io.micrometer.core.instrument.Tags;
import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.constants.APIJSONURLCONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.CONSTANTS.WEB_TOKEN_TYPE;
import se.frescano.nextory.app.constants.ErrorCodes;
import se.frescano.nextory.app.exception.APIException;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.auth.exception.AuthenticationException;
import se.frescano.nextory.auth.rest.request.BlueShiftUserExtractReqBean;
import se.frescano.nextory.auth.rest.request.SERVICE_TYPE;
import se.frescano.nextory.auth.rest.request.TokenExtractReqBean;
import se.frescano.nextory.auth.rest.response.AuthenticationResponseStatus;
import se.frescano.nextory.auth.rest.response.TokenExtractResponse;
import se.frescano.nextory.cache.service.CacheBridgeMarket;
import se.frescano.nextory.endpoints.CallState;
import se.frescano.nextory.endpoints.TagExtractor;
import se.frescano.nextory.market.model.MarketListVo;
import se.frescano.nextory.restclients.AbstractRestTempleteClient;
import se.frescano.nextory.restclients.RestException;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.web.api.exception.InSecuredRequestException;
import se.frescano.nextory.web.api.exception.WebApiException;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;

@DependsOn("applicationpropconfig")
@Service("authenticationservice")
public class AuthenticationServiceImpl implements AuthenticationService{

	public static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
	
	/*@Value("${nextory.authentication.service.endpoint}")*/
	private String AUTHENTICATION_SERVICE;
	
	
	private String BLUESHIFT_AUTHENTICATION_SERVICE;
	
	@Autowired
	private CacheBridgeMarket cacheBridgeMarket;
	
	@Autowired
	private AbstractRestTempleteClient		abstractresttemplate;	
	
	@Autowired
	private ApplicationPropertiesConfig		applicationpropconfig;
	/*@Autowired
	Timer authTimer;
	*/
	
	@PostConstruct
	public void init(){
		this.AUTHENTICATION_SERVICE	= applicationpropconfig.getAUTHENTICATION_SERVICE();
		this.BLUESHIFT_AUTHENTICATION_SERVICE = applicationpropconfig.getBLUESHIFT_AUTHENTICATION_SERVICE();
	}
	
	final TagExtractor<TokenExtractResponse> tagEx  = new TagExtractor<TokenExtractResponse>() {
		@Override
		public CallState extract(TokenExtractResponse rsp,long starttime,Throwable ex) {
			CallState callState = new CallState("auth", starttime, AUTHENTICATION_SERVICE);
			if(rsp!=null){
				callState.setTags(Tags.of("status", rsp.getStatus()));
			}else if(ex!=null){
				if(ex instanceof RestException)
					callState.setTags(Tags.of("status", ((RestException)ex).getStatus().toString()));
				else
					callState.setTags(Tags.of("status", "error"));
			}else{
				callState.setTags(Tags.of("status", "error"));
			}
		  return callState;	
		}
	};
	
	public TokenExtractResponse authenticateUser(final TokenExtractReqBean authenticationrequest, final HttpServletRequest request) {
		//TokenExtractResponse	auth_response = authrestclient.extractUserToken(Book2GoUtil.objectMapper.writeValueAsString( authenticationrequest) );
		TokenExtractResponse	auth_response = null;
		try {
			logger.debug("Outgoing Auth-Request :: {}", authenticationrequest);
			
			auth_response =   abstractresttemplate.exchange( AUTHENTICATION_SERVICE , HttpMethod.POST, authenticationrequest, null, null, TokenExtractResponse.class,tagEx);
			
			logger.debug("Incoming Auth-Response :: {}",auth_response);
		}
		catch (Exception e) {
			logger.error("Unkown exception in Auth-Request ",e);
		}
		
		if( auth_response == null )
			throw new APIException(ErrorCodes.INVALID_TOKEN);
		if( StringUtils.equalsAnyIgnoreCase(auth_response.getStatus(), AuthenticationResponseStatus.FAILED.name()) ){
			if(auth_response.getError().getErrorcode()==WEBErrorCodeEnum.ACCESS_TOKEN_EXPIRED.getCode())
				throw new WebApiException(WEBErrorCodeEnum.ACCESS_TOKEN_EXPIRED);
			else if(auth_response.getError().getErrorcode()==ErrorCodes.INVALID_TOKEN.getErrorcode())
				throw new APIException(ErrorCodes.INVALID_TOKEN);
			else
				throw new AuthenticationException( auth_response.getError().getErrorcode(), auth_response.getError().getMessage() );
		}
		if( auth_response.getTokeninfo() == null || auth_response.getUserinfo() == null )
			throw new APIException(ErrorCodes.INVALID_TOKEN);
			
		UserAuthToken	authtoken	= auth_response.getTokeninfo();
		User			user		= auth_response.getUserinfo();
		request.setAttribute(CONSTANTS.AUTH_TOKEN, authtoken);
		request.setAttribute(CONSTANTS.USER_INFO, user);
		//validateLocale( user, authenticationrequest.getApiversion(), 
		//		authenticationrequest.getLocale(), authenticationrequest.getServiceType(), request);
		return auth_response;
	}
	
	@SuppressWarnings("null")
	public String validateLocale(Double apiversion, String locale, SERVICE_TYPE requestedby,
			HttpServletRequest request) throws Exception {
		//boolean	locale_error = false;
		//Locale	localeObj	 = null;
		if( requestedby == SERVICE_TYPE.WEB && apiversion >= CONSTANTS.WEB_API_VERSION_4_0
				&& StringUtils.isBlank(locale) )
			throw new WebApiException(WEBErrorCodeEnum.INPUT_LOCALE_MISSING);
		else if( requestedby == SERVICE_TYPE.APP && apiversion >= APIJSONURLCONSTANTS.APP_API_VER_6_4 
			&& StringUtils.isBlank(locale) )
			throw new APIException(ErrorCodes.INPUT_LOCALE_MISSING);	
		else if( ( requestedby == SERVICE_TYPE.WEB && apiversion < CONSTANTS.WEB_API_VERSION_4_0) ||
				(requestedby == SERVICE_TYPE.APP && apiversion < APIJSONURLCONSTANTS.APP_API_VER_6_4)){ 
			 locale = CONSTANTS.DEFAULT_LOCALE;
		}	 
		try{
			if(StringUtils.isNotBlank(locale))
				LocaleUtils.toLocale(locale);
		}catch (Exception e) {
			logger.error("Failed to derive locale from string locale", e.getMessage());
			if(requestedby == SERVICE_TYPE.WEB)
				throw new WebApiException(WEBErrorCodeEnum.INPUT_LOCALE_INVALID);
			else if(requestedby == SERVICE_TYPE.APP)
				throw new APIException(ErrorCodes.INPUT_LOCALE_INVALID);
			else
				throw new Exception(ErrorCodes.INPUT_LOCALE_INVALID.getMessage());
		}		
		 
		//Fetch market id by locale 
		MarketListVo market=null;
		String countrycode = null;
		try {
			market = cacheBridgeMarket.getMarketFromNestList(locale);
			if(market==null || market.getMarket_id()==null)
				throw new Exception(ErrorCodes.INPUT_LOCALE_INVALID.getMessage());
			countrycode = market.getCountryCode();
		} catch (Exception e) {
			logger.error(" error while getting the market for {} ",locale);
			if(requestedby == SERVICE_TYPE.WEB)
				throw new WebApiException(WEBErrorCodeEnum.INPUT_LOCALE_INVALID);
			else if(requestedby == SERVICE_TYPE.APP)
				throw new APIException(ErrorCodes.INPUT_LOCALE_INVALID);
		}
		logger.debug("Market by locale :: {}", market);		
		 
		request.setAttribute(CONSTANTS.COUNTRY_CODE, countrycode);
	    return countrycode;
	}
	public boolean authenticateBasicAuth(String authCredentials, String nextoryReqAuthUsername, String nextoryReqAuthPassword) {
		if (null == authCredentials)
			throw new WebApiException(WEBErrorCodeEnum.AUTHENTICATION_FALIED_EXCEPTION,"Basic Authorization Failed !" );
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		}catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();
		boolean authenticationStatus = nextoryReqAuthUsername.equals(username) && nextoryReqAuthPassword.equals(password);
		if(!authenticationStatus)
			throw new WebApiException(WEBErrorCodeEnum.AUTHENTICATION_FALIED_EXCEPTION,"Basic Authorization Failed !" );
		return authenticationStatus;
	}
	
	public void verifyOrigin(HttpServletRequest httpReq, HttpServletResponse httpResp, WEB_TOKEN_TYPE tokenType, String refreshTokenHeader) throws InSecuredRequestException, MalformedURLException{
		// STEP 1: Verifying Same Origin with Standard Headers 
        //Try to get the source from the "Origin" header
        String source = httpReq.getHeader("Origin");
        String reason = null;
        if (StringUtils.isBlank(source)) {
            //If empty then fallback on "Referer" header
            source = httpReq.getHeader("Referer");
            //If this one is empty too then we trace the event and we block the request (recommendation of the article)...
            if (StringUtils.isBlank(source)) {
            	reason = "Error: ORIGIN and REFERER request headers are both absent/empty so we block the request !";
            	logger.error(reason);
                throw new InSecuredRequestException(reason); 
            }
        }
        
        //Compare the source against the expected target origin     
       //  STEP 2: Verifying CSRF token using "Double Submit Cookie" approach 
        //If CSRF token cookie is absent from the request then we provide one in response but we stop the process at this stage.
        //Using this way we implement the first providing of token
        if(WEB_TOKEN_TYPE.REFRESH==tokenType){
	        Cookie tokenCookie = null;
	        Cookie[] cookies = httpReq.getCookies() ;
	        if (cookies != null) {  
	            tokenCookie = Arrays.stream(httpReq.getCookies()).filter(c -> c.getName().equals(refreshTokenHeader)).findFirst().orElse(null); 	        	
	        } 
	      
	        String tokenFromHeader = httpReq.getHeader(refreshTokenHeader);  
	        if(!StringUtils.isBlank(tokenFromHeader)){
	            //If the cookie is present then we pass to validation phase
	            //Get token from the custom HTTP header (part under control of the requester)
	        	//logger.info("Checking cookie token :: {}", tokenCookie.getValue());
	            //If empty then we trace the event and we block the request
	            if (tokenCookie == null || StringUtils.isBlank(tokenCookie.getValue())) {
	            	reason = "Error: Token provided via Cookie is absent/empty so we block the request !";
	                logger.error(reason);
	                throw new InSecuredRequestException(reason); 
	            } else  if (!tokenFromHeader.equals(tokenCookie.getValue())) {
	                //Verify that token from header and one from cookie are the same
	                //Here is not the case so we trace the event and we block the request
	            	reason = "Error: Token provided via HTTP Header and via Cookie are not same so we block the request !";
	            	logger.error(reason);
	                throw new InSecuredRequestException(reason); 
	            } 
	        } 
        }  
	}
	
	
	public TokenExtractResponse authenticateBlueShiftUser(Integer customerid) {
		//TokenExtractResponse	auth_response = authrestclient.extractUserToken(Book2GoUtil.objectMapper.writeValueAsString( authenticationrequest) );
		BlueShiftUserExtractReqBean authenticationrequest = new BlueShiftUserExtractReqBean(
				SERVICE_TYPE.WEB,(customerid!=null?customerid:0));
		TokenExtractResponse	auth_response = null;
		try {
			logger.debug("Outgoing Auth-Request :: {}", authenticationrequest);
			
			auth_response =   abstractresttemplate.exchange( BLUESHIFT_AUTHENTICATION_SERVICE , HttpMethod.POST,authenticationrequest, null, null, TokenExtractResponse.class,tagEx);
			
			logger.debug("Incoming Auth-Response :: {}",auth_response);
		}
		catch (Exception e) {
			logger.error("Unkown exception in Auth-Request ",e);
		}
		
		if( auth_response == null )
			throw new APIException(ErrorCodes.INVALID_TOKEN);
		if( StringUtils.equalsAnyIgnoreCase(auth_response.getStatus(), AuthenticationResponseStatus.FAILED.name()) ){
			if(auth_response.getError().getErrorcode()==WEBErrorCodeEnum.ACCESS_TOKEN_EXPIRED.getCode())
				throw new WebApiException(WEBErrorCodeEnum.ACCESS_TOKEN_EXPIRED);
			else if(auth_response.getError().getErrorcode()==ErrorCodes.INVALID_TOKEN.getErrorcode())
				throw new APIException(ErrorCodes.INVALID_TOKEN);
			else
				throw new AuthenticationException( auth_response.getError().getErrorcode(), auth_response.getError().getMessage() );
		}
		if( auth_response.getTokeninfo() == null || auth_response.getUserinfo() == null )
			throw new APIException(ErrorCodes.INVALID_TOKEN);
			
		/*UserAuthToken	authtoken	= auth_response.getTokeninfo();
		User			user		= auth_response.getUserinfo();
		request.setAttribute(CONSTANTS.AUTH_TOKEN, authtoken);
		request.setAttribute(CONSTANTS.USER_INFO, user);*/
		//validateLocale( user, authenticationrequest.getApiversion(), 
		//		authenticationrequest.getLocale(), authenticationrequest.getServiceType(), request);
		return auth_response;
	}
	
	@Override
	public String toString() {
		StringBuilder	sb = new StringBuilder();
		sb.append("Authentication condif [");
		if( AUTHENTICATION_SERVICE != null )
			sb.append(" AUTHENTICATION_SERVICE = ").append( AUTHENTICATION_SERVICE );
		sb.append("]");		
		return sb.toString();
	}
}

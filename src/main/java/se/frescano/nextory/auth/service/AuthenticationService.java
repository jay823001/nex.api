package se.frescano.nextory.auth.service;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;

import se.frescano.nextory.auth.rest.request.SERVICE_TYPE;
import se.frescano.nextory.auth.rest.request.TokenExtractReqBean;
import se.frescano.nextory.auth.rest.response.TokenExtractResponse;
import se.frescano.nextory.spring.method.model.User;

public interface AuthenticationService {

	public TokenExtractResponse authenticateUser(TokenExtractReqBean authenticationrequest, HttpServletRequest request) throws JsonProcessingException, Exception;
	
	public String validateLocale(Double apiversion, String locale, SERVICE_TYPE requestedby,
			HttpServletRequest request) throws Exception;
}

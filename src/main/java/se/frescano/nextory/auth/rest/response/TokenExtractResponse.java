package se.frescano.nextory.auth.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.spring.method.model.User;

@JsonInclude(JsonInclude.Include.NON_NULL) 
public class TokenExtractResponse extends ResponseStatusBean{

	private UserAuthToken	tokeninfo;
	private User			userinfo;
	
	public TokenExtractResponse(){		
	}
	
	public TokenExtractResponse(String	status, UserAuthToken	tokeninfo, User		userinfo){
		super(status, null);
		this.tokeninfo	= tokeninfo;
		this.userinfo	= userinfo;
	}
	
	public TokenExtractResponse(String	status, ErrorDetailsBean	error){
		super(status, error);
	}

	public UserAuthToken getTokeninfo() {
		return tokeninfo;
	}

	public void setTokeninfo(UserAuthToken tokeninfo) {
		this.tokeninfo = tokeninfo;
	}

	public User getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(User userinfo) {
		this.userinfo = userinfo;
	}
	
}

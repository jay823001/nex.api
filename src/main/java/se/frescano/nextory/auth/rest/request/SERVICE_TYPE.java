package se.frescano.nextory.auth.rest.request;

import org.apache.commons.lang3.StringUtils;

public enum SERVICE_TYPE {

	APP,
	WEB;
	
	private	String	service;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
	
	public static SERVICE_TYPE getServiceFrom(String service){
		for(SERVICE_TYPE servicetype : values()){
			if( StringUtils.equalsAnyIgnoreCase(service, servicetype.name()))
				return servicetype;
		}
		return null;
	}
}

package se.frescano.nextory.auth.rest.request;

import org.codehaus.plexus.util.StringUtils;

import se.frescano.nextory.app.constants.CONSTANTS;



public class TokenExtractReqBean extends AppRequestBean{

	//@NotNull(message="api.INPUT_MISSING")
	private String	auth_token;
	
	//@NotNull(message="api.INPUT_MISSING")
	private String	salt_key;
	
	//@NotNull(message="api.INPUT_MISSING")
	private String	requestedby;
	
	//This field to support token backward compatibility
	//@NotNull(message="api.INPUT_MISSING")
	private Double	apiversion;
	
	/*private boolean	checkExpiry;*/
	private boolean	force_authenticate;
	
	private String	locale;
	//private String webTokenType;
	private String refreshToken;
	private String countrycode;
	
 
	public TokenExtractReqBean(SERVICE_TYPE requestBy, Double version,  String accessToken, String refreshToken, String	salt_key,
			Boolean forceAuth, String locale, String countrycode) {
		this.auth_token = accessToken;
		this.refreshToken = refreshToken;
		this.requestedby= requestBy.name();
		this.apiversion = version; 
		this.force_authenticate = forceAuth;
		this.locale=locale;
		this.salt_key	= salt_key;
		this.countrycode=countrycode;
	}

	
	public String getAuth_token() {
		return auth_token;
	}
	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}
	
	public String getSalt_key() {
		return salt_key;
	}
	public void setSalt_key(String salt_key) {
		this.salt_key = salt_key;
	}
	
	public boolean isForce_authenticate() {
		return force_authenticate;
	}
	public void setForce_authenticate(boolean force_authenticate) {
		this.force_authenticate = force_authenticate;
	}
	public Double getApiversion() {
		return apiversion;
	}
	public void setApiversion(Double apiversion) {
		this.apiversion = apiversion;
	}

	public String getRequestedby() {
		return requestedby;
	}

	public void setRequestedby(String requestedby) {
		this.requestedby = requestedby;
	}

	 

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	 

	/*public String getWebTokenType() {
		return webTokenType;
	}

	public void setWebTokenType(String webTokenType) {
		this.webTokenType = webTokenType;
	}
*/

	public String getRefreshToken() {
		return refreshToken;
	}


	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}	
	
	public SERVICE_TYPE getServiceType() {
		SERVICE_TYPE serviceType = null;
		try{
			if(StringUtils.isNotBlank(this.requestedby))
				serviceType = SERVICE_TYPE.valueOf(this.requestedby);
		}catch(Exception e){}
		return serviceType;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TokenExtractReqBean [");
		if (auth_token != null) {
			builder.append("auth_token=");
			builder.append(auth_token);
			builder.append(", ");
		}
		if (salt_key != null) {
			builder.append("salt_key=");
			builder.append(salt_key);
			builder.append(", ");
		}
		if (requestedby != null) {
			builder.append("requestedby=");
			builder.append(requestedby);
			builder.append(", ");
		}
		if (apiversion != null) {
			builder.append("apiversion=");
			builder.append(apiversion);
			builder.append(", ");
		}
		builder.append("force_authenticate=");
		builder.append(force_authenticate);
		builder.append(", ");
		if (locale != null) {
			builder.append("locale=");
			builder.append(locale);
			builder.append(", ");
		}
		/*if (webTokenType != null) {
			builder.append("webTokenType=");
			builder.append(webTokenType);
			builder.append(", ");
		}*/
		if (refreshToken != null) {
			builder.append("refreshToken=");
			builder.append(refreshToken);
		}
		builder.append("]");
		return builder.toString();
	}


	public String getCountrycode() {
		return countrycode;
	}


	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

}

package se.frescano.nextory.auth.rest.response;

public class ErrorDetailsBean {

	private int 	errorcode;
	private String	message;
	
	public ErrorDetailsBean(){
		
	}
	
	public ErrorDetailsBean(int errorcode, String message){
		this.errorcode	= errorcode;
		this.message	= message;
	}
	
	public int getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}

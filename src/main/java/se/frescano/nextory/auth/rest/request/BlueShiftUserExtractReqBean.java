package se.frescano.nextory.auth.rest.request;

public class BlueShiftUserExtractReqBean extends AppRequestBean{

	
	
	//@NotNull(message="api.INPUT_MISSING")
	private String	requestedby;
	
	//@NotNull(message="api.INPUT_MISSING")
		private Integer	customerid;
	
	
	
 
	public BlueShiftUserExtractReqBean(SERVICE_TYPE requestBy, Integer customerid) {
		this.requestedby= requestBy.name();
		this.customerid =customerid;
	}




	public String getRequestedby() {
		return requestedby;
	}




	public void setRequestedby(String requestedby) {
		this.requestedby = requestedby;
	}




	



	public Integer getCustomerid() {
		return customerid;
	}




	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}




	@Override
	public String toString() {
		return "BlueShiftUserExtractReqBean [requestedby=" + requestedby + ", customerid=" + customerid + "]";
	}
	
	

	
	

}

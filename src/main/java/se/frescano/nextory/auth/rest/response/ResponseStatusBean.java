package se.frescano.nextory.auth.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL) 
public class ResponseStatusBean {

	private String	status;	
	private ErrorDetailsBean	error;
	
	public ResponseStatusBean(){
		
	}
	
	public ResponseStatusBean(String	status, ErrorDetailsBean	error){
		this.status	=	status;
		this.error= 	error;
			
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public ErrorDetailsBean getError() {
		return error;
	}

	public void setError(ErrorDetailsBean error) {
		this.error = error;
	}
		
}

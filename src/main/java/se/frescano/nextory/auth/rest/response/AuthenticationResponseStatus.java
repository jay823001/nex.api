package se.frescano.nextory.auth.rest.response;

public enum AuthenticationResponseStatus {
	SUCCESS,
	FAILED;
}

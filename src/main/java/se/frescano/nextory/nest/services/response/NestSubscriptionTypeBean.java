package se.frescano.nextory.nest.services.response;

public class NestSubscriptionTypeBean {

	private String	subscriptionType;
	private int		subscriptionSequence;
	public String getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public int getSubscriptionSequence() {
		return subscriptionSequence;
	}
	public void setSubscriptionSequence(int subscriptionSequence) {
		this.subscriptionSequence = subscriptionSequence;
	}
	
	
}

package se.frescano.nextory.nest.services.response;

import java.util.List;

public class NestSubscriptionTypes {

	private List<NestSubscriptionTypeBean>	subscriptionTypeList;
	private String	status;
	private int		code;
	private String	description;
	public List<NestSubscriptionTypeBean> getSubscriptionTypeList() {
		return subscriptionTypeList;
	}
	public void setSubscriptionTypeList(
			List<NestSubscriptionTypeBean> subscriptionTypeList) {
		this.subscriptionTypeList = subscriptionTypeList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}

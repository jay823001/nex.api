package se.frescano.nextory.blueshift.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import se.frescano.ApplicationPropertiesConfig;

/*@PropertySource("classpath:properties/database-${spring.profiles.active}.properties")*/
@DependsOn("applicationpropconfig")
@Component("blueShiftConfiguration")
public class BlueShiftConfiguration {

	@Autowired
	private ApplicationPropertiesConfig	applicationpropconfig;
	
	//@Value("${blueshift.api.url}") 
	private String blueshiftUrl;
	
	//@Value("${blueshift.view}") 
	private String blueshiftView;
	
	//@Value("${blueshift.add}") 
	private String blueshiftAdd;
	
	//@Value("${blueshift.purchase}") 
	private String blueshiftPurchase;
	
	//@Value("${blueshift.mode}") 
	private String blueshiftMode;
	
	//@Value("${blueshift.EVENT_API_KEY}") 
	private String blueshiftApiKey;
	
	@PostConstruct
	public void inti(){
		this.blueshiftUrl	= applicationpropconfig.getBlueshiftUrl();
		this.blueshiftView	= applicationpropconfig.getBlueshiftView();
		this.blueshiftAdd	= applicationpropconfig.getBlueshiftAdd();
		this.blueshiftPurchase = applicationpropconfig.getBlueshiftPurchase();
		this.blueshiftMode = applicationpropconfig.getBlueshiftMode();
		this.blueshiftApiKey = applicationpropconfig.getBlueshiftApiKey();
	}
	
	public String getBlueshiftUrl() {
		return blueshiftUrl;
	}

	public void setBlueshiftUrl(String blueshiftUrl) {
		this.blueshiftUrl = blueshiftUrl;
	}

	public String getBlueshiftView() {
		return blueshiftView;
	}

	public void setBlueshiftView(String blueshiftView) {
		this.blueshiftView = blueshiftView;
	}

	public String getBlueshiftAdd() {
		return blueshiftAdd;
	}

	public void setBlueshiftAdd(String blueshiftAdd) {
		this.blueshiftAdd = blueshiftAdd;
	}

	public String getBlueshiftPurchase() {
		return blueshiftPurchase;
	}

	public void setBlueshiftPurchase(String blueshiftPurchase) {
		this.blueshiftPurchase = blueshiftPurchase;
	}

	public String getBlueshiftMode() {
		return blueshiftMode;
	}

	public void setBlueshiftMode(String blueshiftMode) {
		this.blueshiftMode = blueshiftMode;
	}
	
	public String getBlueshiftApiKey() {
		return blueshiftApiKey;
	}
	
	public void setBlueshiftApiKey(String blueshiftApiKey) {
		this.blueshiftApiKey = blueshiftApiKey;
	}
	
	@Override
	public String toString() {
		StringBuilder	sb = new StringBuilder();
		sb.append("Blueshif configurations [");		
		if( blueshiftUrl != null )
			sb.append("blueshiftUrl = ").append( blueshiftUrl ).append(",");
		if( blueshiftView != null )
			sb.append("blueshiftView = ").append( blueshiftView ).append(",");
		
		if( blueshiftAdd != null )
			sb.append("blueshiftAdd = ").append( blueshiftAdd ).append(",");
		
		if( blueshiftPurchase != null )
			sb.append("blueshiftPurchase = ").append( blueshiftPurchase ).append(",");
		if( blueshiftMode != null )
			sb.append("blueshiftMode = ").append( blueshiftMode ).append(",");
		
		if( blueshiftApiKey != null )
			sb.append("blueshiftApiKey = ").append( blueshiftApiKey ).append(",");
		
		sb.append("]");
		return sb.toString();
	}
	
	
}

package se.frescano.nextory.blueshift.config;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.frescano.nextory.listener.Publisher;
import se.frescano.nextory.notification.NotificationEnum;
import se.frescano.nextory.notification.NotificationEvent;
import se.frescano.nextory.util.Book2GoUtil;

/*
 * Created by Deba
 * Reference : http://springinpractice.com/2008/11/15/aop-101-speeding-up-springs-javamailsenderimpl-with-aop
 * Any new method added to this class should start with the nomenclature of CallAptus..
 * Note: All http rest api methods which are  Asynchronous in nature qualifies to get into this class
 * */

@Service
public class BlueShiftNotificationApiCall{
		
	public static final Logger	_logger	= LoggerFactory.getLogger(BlueShiftNotificationApiCall.class);
	
	@Autowired
	private Publisher publisher;
	
	protected void postCall( Map<String,Object> postBody,String url){
		try{
			publisher.publishEvent(new NotificationEvent(NotificationEnum.BLUE_SHIFT, postBody));
			if(_logger.isTraceEnabled())_logger.trace(Book2GoUtil.objectMapper.writeValueAsString( postBody ));
		}catch (Exception e) {
			_logger.warn("Exception while posting the notification to the blueshift ",e);
		}		
	}	
}

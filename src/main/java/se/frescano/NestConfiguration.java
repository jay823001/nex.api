package se.frescano;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@PropertySource("classpath:properties/database-${spring.profiles.active}.properties")
@Component("nestConfiguration")
//@ConfigurationProperties("nest")
public class NestConfiguration {

	@Value("${nest.subscriptiontypes}")
	private String	subscriptiontypes;
	
	@Value("${nest.categories}")
	private String	categories;
	
	@Value("${nest.markets}")
	private String	markets;

	@Value("${nest.campaigntypes}")
	private String	campaigntypes;
	
	public String getSubscriptiontypes() {
		return subscriptiontypes;
	}

	public void setSubscriptiontypes(String subscriptiontypes) {
		this.subscriptiontypes = subscriptiontypes;
	}

	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getMarkets() {
		return markets;
	}

	public void setMarkets(String markets) {
		this.markets = markets;
	}
	
	
	public String getCampaigntypes() {
		return campaigntypes;
	}

	public void setCampaigntypes(String campaigntypes) {
		this.campaigntypes = campaigntypes;
	}

	@Override
	public String toString() {
		StringBuilder	str = new StringBuilder("subscriptiontypes =").append( subscriptiontypes)
				.append(", categories = ").append( categories).append(", markets =").append( markets)
				.append(", getcampaigntypes = ").append( campaigntypes );
		return str.toString();
	}
	
}

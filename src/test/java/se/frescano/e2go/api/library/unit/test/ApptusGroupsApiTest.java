package se.frescano.e2go.api.library.unit.test;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.product.request.MainJsonRequestBean;
import se.frescano.nextory.app.product.response.BookGroupVOApptus;
import se.frescano.nextory.app.request.APITokenRequestBean;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.app.response.BookGroupsApptusResponseWrapper;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.service.SubscriptionTypeVO;
import se.frescano.nextory.util.Book2GoUtil;
import se.frescano.nextory.util.BookGroupViewEnum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class ApptusGroupsApiTest {
	
	@Autowired 
	private ApptusApiService service;
	
	@Test
	public void testGroupsAPiRelated2() throws Exception
	{
		User user = new User();
		user.setCustomerid(486749);
		user.setMembertype(MemberTypeEnum.MEMBER_EMPLOYEE);
		
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setType("PREMIUM");
		vo.setCountrycode("SE");
		vo.setSubId(7);
		vo.setSubName("Familj Plus");
		vo.setRank((short) 3);
		vo.setIsFamilySubscription(true);
		vo.setMaxChildProfiles(3);
		
		user.setSubscription(vo);
		user.setCountrycode("SE");
		user.setUserCountry("SE");
		
		Profile prf = new Profile();
		prf.setProfileid(231042);
		prf.setParentid(486749);
		prf.setIsparent(true);
		prf.setProfilename("debashis.das@frescano.se");
		prf.setColorindex(0);
		prf.setCategory("all");
		prf.setCreateddate(new Date());
		prf.setLoginkey("F2907DD8A537EE64C60665F0C9D4C5B4");
		prf.setStatus("active");
		user.setProfile(prf);
		
		Double apiversion = new Double("6.5");
		
		
		MainJsonRequestBean reqBean = new MainJsonRequestBean();
		
		APITokenRequestBean tokenbean = new APITokenRequestBean();
		tokenbean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		tokenbean.setVer2(false);
		tokenbean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		tokenbean.setAppId("201");
		tokenbean.setOsinfo("11.4");
		tokenbean.setModel("Simulator");
		
		UserAuthToken userauthtoken = new UserAuthToken();
		userauthtoken.setUuid("f77e8f50-73c2-436f-b9e9-e491953a0561");
		userauthtoken.setType(CONSTANTS.TOKEN_TYPE.PT);
		

		reqBean.setApiVersion(new Double("6.5"));
		reqBean.setAppId("201");
		//reqBean.setAuthors_facet(null);
		reqBean.setAuthToken(userauthtoken);
		//reqBean.setAvgrate_Max_facet(null);
		//reqBean.setAvgrate_Min_facet(null);
		//reqBean.setCategoryids_facet(null);
		reqBean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		//reqBean.setIncludenotallowedbooks("0");
		//reqBean.setLanguage_facet(null);
		reqBean.setLocale("sv_SE");
		reqBean.setModel("Simulator");
		reqBean.setOsinfo("11.4");
		reqBean.setOsinfo("11.4");
		reqBean.setPagenumber("1");
		reqBean.setBookid(10001293);
		reqBean.setView("related2");
		//reqBean.setSeriesAsit_facet(null);
		//reqBean.setSort("default");
		//reqBean.setSubscriptionorder_facet(null);
		reqBean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		//reqBean.setType("0");
		reqBean.setVer2(false);
		reqBean.setVersion("201");
		//reqBean.setFormattype_facet("1");
		
		BookGroupsApptusResponseWrapper wrapper = service.getBookGroupsDetails(reqBean,user,apiversion);
		
		int count =0;
		for(BookGroupVOApptus data:wrapper.getBookGroups())
		{
			System.out.println(data.getViewby());
			System.out.println(data.getId());
			System.out.println(data.getId());
			System.out.println(Book2GoUtil.getStrippedDescription(data.getTitle()));
			assertEquals(data.getViewby(), BookGroupViewEnum.RELATED_BOOK_2.getView());
			if(data.getId().contains("search_"))
				count++;
			
		}
		
		assertTrue("sucess", count>0);
		
	}
	
}

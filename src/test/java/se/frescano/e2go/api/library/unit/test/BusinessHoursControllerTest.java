package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class BusinessHoursControllerTest {
	
	 private final String businesshoursPath = "http://localhost:9000/api/6.4/businesshours?";
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void businesshoursTest() throws Exception {
		 System.out.println(":::::::::::::::::::: businesshoursTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
	        String urlContent = "appId=200&model=Google%2BPixel&version=2.4.3&deviceid=dj2o1GwuDX4&osinfo=Android%208.0.0&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(businesshoursPath+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("todayStartTimeMillis"));
			        assertNotNull(jsonObject.get("todayEndTimeMillis"));
			        assertNotNull(jsonObject.get("isOpenToday"));
			        assertNotNull(jsonObject.get("openToday"));
			        assertNotNull(jsonObject.get("nextDayEndTimeMillis"));
			        assertNotNull(jsonObject.get("nextDayStartTimeMillis"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: businesshoursTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
		
}

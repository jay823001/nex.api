package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class ApptusInternalControllerTest {
	
	 private final String basePath = "http://localhost:9000/api/internal/1.0/preview/list/details?";
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getBookForBookGroupPreviewTest() throws Exception {
		 System.out.println(":::::::::::::::::::: getBookForBookGroupPreviewTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 
	        String urlContent = "bookgroupid=tttl_Nyheter&pagenumber=0&rows=12&subscriptiontype=5&countrycode=SE&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(basePath+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	            JSONObject jsonObject = resp.getJSONObject("data");
	            assertEquals(UnitTestUtil.jsonMimeType, mimeType);
	            assertNotNull(jsonObject);
		        assertNotNull(jsonObject.get("bookcount"));
		        assertNotNull(resp.get("status"));
	            	           
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: getBookForBookGroupPreviewTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

}

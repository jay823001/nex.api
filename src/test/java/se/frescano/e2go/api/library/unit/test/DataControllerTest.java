package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class DataControllerTest {

	 private final String basePath = "http://localhost:9000/api/staticdata/";
	 private final String newLsitPath = "NewList?";
	 private final String topEbookAndAudioBookPath = "apptus/topEbookAndAudioBook?";
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void newListTest() throws Exception {
		 System.out.println(":::::::::::::::::::: newListTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+newLsitPath;
	        String urlContent = "countrycode=SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONArray resp = null;
	        
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONArray(writer.toString());
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        
			        if(resp.length() > 0) {
				        for (int i = 0; i < resp.length(); i++) {
			                JSONObject json = (JSONObject) resp.get(i);
			                assertNotNull(json.get("productid"));
					        assertNotNull(json.get("title"));
					        assertNotNull(json.get("isbn"));
					        assertNotNull(json.get("coverUrl"));
					        assertNotNull(json.get("relProductid"));
					        assertNotNull(json.get("productFormat"));
					        assertNotNull(json.get("description"));
				        }
			        }
			        
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: newListTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings({ "deprecation" })
	 @Test
	 public void topEbookAndAudioBookTest() throws Exception {
		 System.out.println(":::::::::::::::::::: topEbookAndAudioBookTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+topEbookAndAudioBookPath;
	        final HttpUriRequest request = new HttpGet(path);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONArray jsonSEArray = resp.getJSONArray("SE");
	            JSONArray jsonFIArray = resp.getJSONArray("FI");
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
		        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
		        assertNotNull(resp);
		        
		        if(jsonSEArray.length() > 0) {
			        for (int i = 0; i < jsonSEArray.length(); i++) {
		                JSONObject json = (JSONObject) jsonSEArray.get(i);
		                assertNotNull(json.get("productid"));
				        assertNotNull(json.get("title"));
				        assertNotNull(json.get("isbn"));
				        assertNotNull(json.get("coverUrl"));
				        assertNotNull(json.get("relProductid"));
				        assertNotNull(json.get("productFormat"));
				        assertNotNull(json.get("description"));
			        }
		        }
		        
		        if(jsonFIArray.length() > 0) {
			        for (int i = 0; i < jsonFIArray.length(); i++) {
		                JSONObject json = (JSONObject) jsonFIArray.get(i);
		                assertNotNull(json.get("productid"));
				        assertNotNull(json.get("title"));
				        assertNotNull(json.get("isbn"));
				        assertNotNull(json.get("coverUrl"));
				        assertNotNull(json.get("relProductid"));
				        assertNotNull(json.get("productFormat"));
				        assertNotNull(json.get("description"));
			        }
		        } 
			       
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: topEbookAndAudioBookTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
}

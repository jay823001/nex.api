package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeviceLibraryControllerVer2Test {
	 private final String basePath = "http://localhost:9000/api/6.4/";
	 private final String libraryActivePath = "library/active?";
	 private final String libraryInActivePath = "library/inactive?";
	 private final String modifyLibraryPath = "library/modifylibrary?";
	 private final String synchsettingsPath = "library/synchsettings?";
	 private final String librarysynPath = "library-sync?";//get & post
	 
	 private static String path      = "http://10.91.0.174:9095/api/web/3.0/authenticate";
	 private static String loginPath = "http://10.91.0.174:9095/api/web/3.0/login";
	 private static String apiloginPath = "http://10.91.0.174:9095/api/6.4/login?";
	 public static String authKey = null;
	 public static String nx_rt = null;
	 public static String nx_at = null;
	 public static String token = null;
	 
	 @SuppressWarnings("deprecation")
	 @BeforeClass
	 public static void getAuthKey() throws JSONException {
		 System.out.println(":::::::::::::::::::: getAuthKey Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		    HttpClient client = HttpClientBuilder.create().build();
	        HttpPost post = new HttpPost(path);
	        HttpPost loginpost = new HttpPost(loginPath);
	        
	        List<NameValuePair> arguments = new ArrayList<>(3);
	        arguments.add(new BasicNameValuePair("password", "993E7E104A6190CA17F2F9E96A47758C"));
	        arguments.add(new BasicNameValuePair("email", "testsandra.freij@hotmail.com"));
	        arguments.add(new BasicNameValuePair("serverdetails", "EB47DB1AB63689E010D28582DD3EC58F"));
	        List<NameValuePair> loginarguments = new ArrayList<>(3);
	        loginarguments.add(new BasicNameValuePair("password", "A9D135B24A7C30319A823D14C93EA4F4"));
	        loginarguments.add(new BasicNameValuePair("email", "testjoanna.lysen@gmail.com"));
	        loginarguments.add(new BasicNameValuePair("serverdetails", "236D928B599BF6C2BB82F295C748F80E"));
	        
	        try {
	        	 
	        	loginpost.setEntity(new UrlEncodedFormEntity(loginarguments));
	            HttpResponse loginresp = client.execute(loginpost);
	            Header[] allHeaders = loginresp.getAllHeaders();
	            for(Header hdr : allHeaders) {
	            	
	            	if(hdr.getName().equalsIgnoreCase("nx-at")){
	            		nx_at = hdr.getValue();
	            	} else if(hdr.getName().equalsIgnoreCase("nx-rt")){
	            		nx_rt = hdr.getValue();
	            	}
	            }
	            
	            post.setEntity(new UrlEncodedFormEntity(arguments));
	            HttpResponse response = client.execute(post);
	            StringWriter writer = new StringWriter();
	            IOUtils.copy(response.getEntity().getContent(), writer);
	            JSONObject resp = new JSONObject(writer.toString());
	            authKey = resp.getJSONObject("data").getString("authkey");
	            assertNotNull(authKey);
	            
	            String urlcontent = "password=ED6063AAE5EF86AC040F01B17489B069&appId=200&checksum=4668DCCF67FBF4598F1A39E0C9F10206&model=Google%2BPixel&version=2.4.3"
	            					+ "&deviceid=pixel&osinfo=Android%208.0.0&username=testRobin.aspman@gmail.com&locale=en";
			    	final HttpUriRequest request = new HttpGet(apiloginPath+urlcontent);
			        HttpResponse webloginresp = UnitTestUtil.executeRequest(request);
			        writer = new StringWriter();
			        IOUtils.copy(webloginresp.getEntity().getContent(), writer);
			        JSONObject webresp = new JSONObject(writer.toString());
			        token = webresp.getJSONObject("data").getString("token");
			        assertNotNull(token);
			        System.out.println(":::::::::::::::::::: getAuthKey Testcase ended ::::::::::::::::::::::::::::::::::::::::::: "+token);  
	
	          } catch (IOException e) {
	            e.printStackTrace();
	        }
	 }
	 
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void libraryActivePathTest() throws Exception {
		 System.out.println(":::::::::::::::::::: libraryActiveTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+libraryActivePath;
	        String urlContent = "appId=200&model=Google%2BPixel&version=2.6.2&deviceid=pixel&osinfo=Android%208.1.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("books"));
			        assertNotNull(jsonObject.get("bookcount"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: libraryActiveTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void libraryInActiveTest() throws Exception {
		 System.out.println(":::::::::::::::::::: libraryInActiveTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+libraryInActivePath;
	        String urlContent = "libid=3428934&appId=200&action=active&model=Google%2BPixel&formattype=1&id=10039620&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("books"));
			        assertNotNull(jsonObject.get("bookcount"));
		            JSONArray jsonArray = jsonObject.getJSONArray("books");
		            
		            if(jsonArray.length() > 0) {
				        for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
					        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
					        assertNotNull(resp);
					        assertNotNull(jsonObject);
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.get("type"));
					        assertNotNull(json.get("imageurl"));
					        assertNotNull(json.get("weburl"));
					        assertNotNull(json.get("relatedbookid"));
					        assertNotNull(json.get("descriptionfull"));
					        assertNotNull(json.get("pubdate"));
					        assertNotNull(json.get("authors"));
					        assertNotNull(json.get("libid"));
					        assertNotNull(json.get("allowedinlibrary"));
					        assertNotNull(json.get("relatedbookallowedinlibrary"));
					        assertNotNull(json.get("coverratio"));
					        assertNotNull(json.get("userrate"));
					        assertNotNull(json.get("availableinlib"));
					        assertNotNull(json.get("synchsettings"));
					        assertNotNull(json.get("datemodified"));
//					        assertNotNull(json.get("file"));
				        }
		            }
			        
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: libraryInActiveTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void addModifyLibraryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: addModifyLibraryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+modifyLibraryPath;
	        String urlContent = "libid=0&appId=200&action=add&model=Google%2BPixel&formattype=1&id=10056227&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONObject json = jsonObject.getJSONObject("books");
	            
				        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
				        assertNotNull(resp);
				        assertNotNull(jsonObject);
		                assertNotNull(json.getLong("id"));
				        assertNotNull(json.getString("title"));
				        assertNotNull(json.get("type"));
				        assertNotNull(json.get("imageurl"));
				        assertNotNull(json.get("weburl"));
				        assertNotNull(json.get("relatedbookid"));
				        assertNotNull(json.get("descriptionbrief"));
				        assertNotNull(json.get("pubdate"));
				        assertNotNull(json.get("authors"));
				        assertNotNull(json.get("libid"));
				        assertNotNull(json.get("allowedinlibrary"));
				        assertNotNull(json.get("relatedbookallowedinlibrary"));
				        assertNotNull(json.get("coverratio"));
				        assertNotNull(json.get("numberofrates"));
				        assertNotNull(json.get("avgrate"));
				        assertNotNull(json.get("synchsettings"));
				        assertNotNull(json.get("datemodified"));
				        assertNotNull(json.get("file"));
				        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: addModifyLibraryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 @SuppressWarnings("deprecation")
	 @Test
	 public void activeModifyLibraryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: activeModifyLibraryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+modifyLibraryPath;
	        String urlContent = "libid=3428937&appId=200&action=active&model=Google%2BPixel&formattype=1&id=10056227&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	            JSONObject json = jsonObject.getJSONObject("books");
				        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
				        assertNotNull(resp);
				        assertNotNull(jsonObject);
		                assertNotNull(json.getLong("id"));
				        assertNotNull(json.getString("title"));
				        assertNotNull(json.get("type"));
				        assertNotNull(json.get("imageurl"));
				        assertNotNull(json.get("weburl"));
				        assertNotNull(json.get("relatedbookid"));
				        assertNotNull(json.get("descriptionbrief"));
				        assertNotNull(json.get("pubdate"));
				        assertNotNull(json.get("authors"));
				        assertNotNull(json.get("libid"));
				        assertNotNull(json.get("allowedinlibrary"));
				        assertNotNull(json.get("relatedbookallowedinlibrary"));
				        assertNotNull(json.get("coverratio"));
//				        assertNotNull(json.get("numberofrates"));
//				        assertNotNull(json.get("avgrate"));
				        assertNotNull(json.get("synchsettings"));
				        assertNotNull(json.get("datemodified"));
				        assertNotNull(json.get("file"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: activeModifyLibraryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void inactiveModifyLibraryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: inactiveModifyLibraryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+modifyLibraryPath;
	        String urlContent = "libid=3428934&appId=200&action=inactive&model=Google%2BPixel&formattype=1&id=10039620&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	            resp = new JSONObject(writer.toString());
	            
	            
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: inactiveModifyLibraryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void deleteModifyLibraryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: deleteModifyLibraryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+modifyLibraryPath;
	        String urlContent = "libid=3428934&appId=200&action=delete&model=Google%2BPixel&formattype=1&id=10039620&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: deleteModifyLibraryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void resetModifyLibraryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: resetModifyLibraryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+modifyLibraryPath;
	        String urlContent = "libid=3428934&appId=200&action=reset&model=Google%2BPixel&formattype=1&id=10039620&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: resetModifyLibraryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void synchsettingsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: synchsettingsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+synchsettingsPath;
	        String urlContent = "libid=3428934&appId=200&format=9&model=Google%2BPixel&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	            resp = new JSONObject(writer.toString());

	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONObject json = jsonObject.getJSONObject("syncsettings");
	            JSONObject settingsjson = json.getJSONObject("setting");
				        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
				        assertNotNull(resp);
				        assertNotNull(jsonObject);
		                assertNotNull(json.getLong("completed"));
				        assertNotNull(json.getString("position"));
				        assertNotNull(json.get("bookmarks"));
				        assertNotNull(json.get("highlights"));
				        assertNotNull(json.get("pages"));
				        assertNotNull(json.get("playersetting"));
				        assertNotNull(settingsjson);
				        assertNotNull(settingsjson.get("fontname"));
				        assertNotNull(settingsjson.get("fontsize"));
				        assertNotNull(settingsjson.get("linespacing"));
				        assertNotNull(settingsjson.get("foreground"));
				        assertNotNull(settingsjson.get("background"));
				        assertNotNull(settingsjson.get("theme"));
				        assertNotNull(settingsjson.get("brightness"));
				        assertNotNull(settingsjson.get("transitiontype"));
				        assertNotNull(settingsjson.get("lockrotation"));
				        assertNotNull(settingsjson.get("doublepaged"));
				        assertNotNull(settingsjson.get("allow3g"));
				        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: synchsettingsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void postLibrarysynTest() throws Exception {
		 System.out.println(":::::::::::::::::::: postLibrarysynTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	String path = basePath+librarysynPath;
	 	
		    String jsonrequest = getJsonString(token);
		 //   String prettyJson = toPrettyFormat(jsonrequest);

	        final HttpPost request = new HttpPost(path);
		 	request.setHeader("Content-Type", "application/json;charset=UTF-8");
		 	 request.setEntity(new StringEntity(/*prettyJson*/jsonrequest, "UTF-8"));
	        
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: postLibrarysynTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 private String getJsonString(String token) {
				
			String json = "{\"appId\" : \"201\", \"locale\" :\"sv_SE\", \"token\":  \""+token+"\" , "
							+ "\"osinfo\": \"ios9.2.1\",\"model\": \"Simulator64\",\"version\": \"1.1\",\"deviceid\":\"pixel\",\"syncdateforhighlights\": \"2017-02-27 21:56:16 +0530\",\"syncdateforbookmarks\": \"2017-02-27 21:56:16 +0530\","
							+ "\"libid\": \"3428934\",\"position\": \"0.612121\",\"formatid\": \"9\", \"percentage\": \"0.6121673\",\"currentpercentage\": \"0.6121673\",\"cfi\" : \"4[epub_Hatadochalskad]/2/32,/1:0,/1:94\","
							+ "\"idref\" : \"epub_Hatadochalskad-7\",\"syncdate\": \"2017-02-27 21:56:16 +0530\","
  
        					+ "\"setting\": {\"fontname\" : \"serif\",\"fontsize\" : 2,\"playerspeed\" : 1,\"linespacing\" : -1,\"foreground\" : -1,\"background\" : -1,"
        					+ "\"theme\" : 0,\"brightness\" : \"1\",\"transistiontype\" : null,\"lockrotation\" : 0,\"doublepaged\" : 1,\"allowthreeg\" : null,\"globalpagination\" : 1,"
        					+ "\"synctimestamp\" : null,\"margin\" : 0.2,\"justified\" : 0,\"readerversion\" : 1},"
        					+ "\"highlights\" : [{\"chapterindex\" : 14, \"formattype\" : 105,\"startindex\" : 997,\"startoffset\" : 183,\"endindex\" : 997,\"endoffset\" : 198,"
        					+ "\"pagepositioninchapter\" : \"0.0\",\"pagepositioninbook\" : \"0.0\",\"color\" : 15656590,\"text\" : \"operationssalen\",\"note\" : \"\",\"isnote\" : 0,"
        					+ "\"cfi\" : \"/4[epub_Hatadochalskad]/2/32,/1:0,/1:94\",\"idref\" : \"epub_Hatadochalskad-7\"}],"
        					+ "\"bookmarks\" : [{\"formattype\" : 105, \"chapterindex\" : 7, \"pagepositioninchapter\" : \"0.104575\",\"pagepositioninbook\" : \"0.263213\",\"chaptername\" : null,"
        					+ "\"contentheight\" : null,\"bookname\" : null,\"title\" : \"title\",\"type\" : \"0\",\"datetime\" : \"2017-02-27 21:56:16 +0530\",\"code\" : \"\",\"cfi\" : \"/4[epub_Hatadochalskad]/2/32,/1:0,/1:94\","
        					+ "\"idref\" : \"epub_Hatadochalskad-7\"},"
        					+ "{\"formattype\" : 105,\"chapterindex\" : 14,\"pagepositioninchapter\" : \"0.7564102\",\"pagepositioninbook\" : \"0.5465337132003798\",\"chaptername\" : null,"
        					+ "\"contentheight\" : null,\"bookname\" : null,\"title\" : \"Måndagen den 10 maj 2010\",\"type\" : \"0\",\"datetime\" : \"2017-02-27 21:56:16 +0530\","
        					+ "\"code\" : \"\",\"cfi\" : \"/4[epub_Hatadochalskad]/2/32,/1:0,/1:94\",\"idref\" : \"epub_Hatadochalskad-7\" } ]}";
			
			return json;
		}
	 
	 
/*	 @Test
	 public void testPrettyPrint()
	 {
	     String compactJson = "{\"playerID\":1234,\"name\":\"Test\",\"itemList\":[{\"itemID\":1,\"name\":\"Axe\",\"atk\":12,\"def\":0},{\"itemID\":2,\"name\":\"Sword\",\"atk\":5,\"def\":5},{\"itemID\":3,\"name\":\"Shield\",\"atk\":0,\"def\":10}]}";
	     System.out.println("**************************token***************************"+token);
//	     String prettyJson = toPrettyFormat(compactJson);
	    
	     String jsonrequest = getJsonString(token);
	     String prettyJson = toPrettyFormat(jsonrequest);
	     System.out.println("Compact:\n" + jsonrequest);
	     System.out.println("Pretty:\n" + prettyJson);
	 }*/
	 
	 /*public static String toPrettyFormat(String jsonString) 
	  {
	      
		 JsonParser parser = new JsonParser();
	      JsonObject json = parser.parse(jsonString).getAsJsonObject();

	      Gson gson = new GsonBuilder().setPrettyPrinting().create();
	      String prettyJson = gson.toJson(json);

	      return prettyJson;
	  }*/
	 
}

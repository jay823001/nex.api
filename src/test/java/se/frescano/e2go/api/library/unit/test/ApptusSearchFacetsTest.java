package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.product.request.SearchJsonApptusRequestBean;
import se.frescano.nextory.app.product.response.AutoCompleteWrapper;
import se.frescano.nextory.app.product.response.SearchWithFacetsApptusResponseWrapper;
import se.frescano.nextory.app.request.APITokenRequestBean;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.app.request.UserAuthToken;
import se.frescano.nextory.apptus.model.Facets.FacetList;
import se.frescano.nextory.apptus.model.Facets.Value;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.service.SubscriptionTypeVO;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class ApptusSearchFacetsTest {
	
	@Autowired 
	private ApptusApiService service;
	
	@Test
	public void testdebaproducthistory() throws Exception
	{
		User user = new User();
		user.setCustomerid(1234);
		Profile prf = new Profile();
		prf.setProfileid(123);
		user.setProfile(prf);
		AutoCompleteWrapper wrapper = service.getSearchHistory(user);
		assertEquals(Boolean.TRUE, Boolean.TRUE);
	}
	
	
	@Test
	public void testSearchFacets() throws Exception
	{
		User user = new User();
		user.setCustomerid(486749);
		user.setMembertype(MemberTypeEnum.MEMBER_EMPLOYEE);
		
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setType("PREMIUM");
		vo.setCountrycode("SE");
		vo.setSubId(7);
		vo.setSubName("Familj Plus");
		vo.setRank((short) 3);
		vo.setIsFamilySubscription(true);
		vo.setMaxChildProfiles(3);
		
		user.setSubscription(vo);
		user.setCountrycode("SE");
		user.setUserCountry("SE");
		
		Profile prf = new Profile();
		prf.setProfileid(231042);
		prf.setParentid(486749);
		prf.setIsparent(true);
		prf.setProfilename("debashis.das@frescano.se");
		prf.setColorindex(0);
		prf.setCategory("all");
		prf.setCreateddate(new Date());
		prf.setLoginkey("F2907DD8A537EE64C60665F0C9D4C5B4");
		prf.setStatus("active");
		user.setProfile(prf);
		
		Double apiversion = new Double("6.5");
		
		
		SearchJsonApptusRequestBean reqBean = new SearchJsonApptusRequestBean();
		
		APITokenRequestBean tokenbean = new APITokenRequestBean();
		tokenbean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		tokenbean.setVer2(false);
		tokenbean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		tokenbean.setAppId("201");
		tokenbean.setOsinfo("11.4");
		tokenbean.setModel("Simulator");
		
		UserAuthToken userauthtoken = new UserAuthToken();
		userauthtoken.setUuid("f77e8f50-73c2-436f-b9e9-e491953a0561");
		userauthtoken.setType(CONSTANTS.TOKEN_TYPE.PT);
		

		reqBean.setApiVersion(new Double("6.5"));
		reqBean.setAppId("201");
		reqBean.setAuthors_facet(null);
		reqBean.setAuthToken(userauthtoken);
		reqBean.setAvgrate_Max_facet(null);
		reqBean.setAvgrate_Min_facet(null);
		reqBean.setCategoryids_facet(null);
		reqBean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		reqBean.setIncludenotallowedbooks("0");
		reqBean.setLanguage_facet(null);
		reqBean.setLocale("sv_SE");
		reqBean.setModel("Simulator");
		reqBean.setOsinfo("11.4");
	 
		reqBean.setPagenumber("1");
		reqBean.setPagesize("12");
		reqBean.setQuery("stieg");
		reqBean.setSeriesAsit_facet(null);
		reqBean.setSort("default");
		reqBean.setSubscriptionorder_facet(null);
		reqBean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		reqBean.setType("0");
		reqBean.setVer2(false);
		reqBean.setVersion("201");
		reqBean.setFormattype_facet("1");
		
		SearchWithFacetsApptusResponseWrapper wrapper = service.getBooksFromSearchWithFactes(reqBean, apiversion, user);
		System.out.println(wrapper.getBookCount());

		for(FacetList list: wrapper.getFacets())
		{
			System.out.println("####################"+list.getAttribute());
			if(list!=null && list.getValues()!=null)
			for(Value val: list.getValues())
			{
				System.out.println(val.getText()+"-----------------"+ val.getCount()
				+"----------------------------"+val.getCategoryname()+"------------------"+val.isSelected());
			}
			System.out.println("####################");
		}
		
		assertEquals(Boolean.TRUE, Boolean.TRUE);
	}
	
	
	@Test
	public void testSearchFacetswithAuthors() throws Exception
	{
		User user = new User();
		user.setCustomerid(486749);
		user.setMembertype(MemberTypeEnum.MEMBER_EMPLOYEE);
		
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setType("PREMIUM");
		vo.setCountrycode("SE");
		vo.setSubId(7);
		vo.setSubName("Familj Plus");
		vo.setRank((short) 3);
		vo.setIsFamilySubscription(true);
		vo.setMaxChildProfiles(3);
		
		user.setSubscription(vo);
		user.setCountrycode("SE");
		user.setUserCountry("SE");
		
		Profile prf = new Profile();
		prf.setProfileid(231042);
		prf.setParentid(486749);
		prf.setIsparent(true);
		prf.setProfilename("debashis.das@frescano.se");
		prf.setColorindex(0);
		prf.setCategory("all");
		prf.setCreateddate(new Date());
		prf.setLoginkey("F2907DD8A537EE64C60665F0C9D4C5B4");
		prf.setStatus("active");
		user.setProfile(prf);
		
		Double apiversion = new Double("6.5");
		
		
		SearchJsonApptusRequestBean reqBean = new SearchJsonApptusRequestBean();
		
		APITokenRequestBean tokenbean = new APITokenRequestBean();
		tokenbean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		tokenbean.setVer2(false);
		tokenbean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		tokenbean.setAppId("201");
		tokenbean.setOsinfo("11.4");
		tokenbean.setModel("Simulator");
		
		UserAuthToken userauthtoken = new UserAuthToken();
		userauthtoken.setUuid("f77e8f50-73c2-436f-b9e9-e491953a0561");
		userauthtoken.setType(CONSTANTS.TOKEN_TYPE.PT);
		

		reqBean.setApiVersion(new Double("6.5"));
		reqBean.setAppId("201");
		reqBean.setAuthors_facet("Stieg Trenter");
		reqBean.setAuthToken(userauthtoken);
		reqBean.setAvgrate_Max_facet(null);
		reqBean.setAvgrate_Min_facet(null);
		reqBean.setCategoryids_facet(null);
		reqBean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		reqBean.setIncludenotallowedbooks("0");
		reqBean.setLanguage_facet(null);
		reqBean.setLocale("sv_SE");
		reqBean.setModel("Simulator");
		reqBean.setOsinfo("11.4");
	 
		reqBean.setPagenumber("1");
		reqBean.setPagesize("12");
		reqBean.setQuery("stieg");
		reqBean.setSeriesAsit_facet(null);
		reqBean.setSort("default");
		reqBean.setSubscriptionorder_facet(null);
		reqBean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		reqBean.setType("0");
		reqBean.setVer2(false);
		reqBean.setVersion("201");
		reqBean.setFormattype_facet("1");
		
		SearchWithFacetsApptusResponseWrapper wrapper = service.getBooksFromSearchWithFactes(reqBean, apiversion, user);
		System.out.println(wrapper.getBookCount());

		for(FacetList list: wrapper.getFacets())
		{
			System.out.println("####################"+list.getAttribute());
			if(list!=null && list.getValues()!=null)
			for(Value val: list.getValues())
			{
				System.out.println(val.getText()+"-----------------"+ val.getCount()
				+"----------------------------"+val.getCategoryname()+"------------------"+val.isSelected());
			}
			System.out.println("####################");
		}
		
		assertEquals(Boolean.TRUE, Boolean.TRUE);
	}
	

	@Test
	public void testSearchFacetswithSeries() throws Exception
	{
		User user = new User();
		user.setCustomerid(486749);
		user.setMembertype(MemberTypeEnum.MEMBER_EMPLOYEE);
		
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setType("PREMIUM");
		vo.setCountrycode("SE");
		vo.setSubId(7);
		vo.setSubName("Familj Plus");
		vo.setRank((short) 3);
		vo.setIsFamilySubscription(true);
		vo.setMaxChildProfiles(3);
		
		user.setSubscription(vo);
		user.setCountrycode("SE");
		user.setUserCountry("SE");
		
		Profile prf = new Profile();
		prf.setProfileid(231042);
		prf.setParentid(486749);
		prf.setIsparent(true);
		prf.setProfilename("debashis.das@frescano.se");
		prf.setColorindex(0);
		prf.setCategory("all");
		prf.setCreateddate(new Date());
		prf.setLoginkey("F2907DD8A537EE64C60665F0C9D4C5B4");
		prf.setStatus("active");
		user.setProfile(prf);
		
		Double apiversion = new Double("6.5");
		
		
		SearchJsonApptusRequestBean reqBean = new SearchJsonApptusRequestBean();
		
		APITokenRequestBean tokenbean = new APITokenRequestBean();
		tokenbean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		tokenbean.setVer2(false);
		tokenbean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		tokenbean.setAppId("201");
		tokenbean.setOsinfo("11.4");
		tokenbean.setModel("Simulator");
		
		UserAuthToken userauthtoken = new UserAuthToken();
		userauthtoken.setUuid("f77e8f50-73c2-436f-b9e9-e491953a0561");
		userauthtoken.setType(CONSTANTS.TOKEN_TYPE.PT);
		

		reqBean.setApiVersion(new Double("6.5"));
		reqBean.setAppId("201");
		reqBean.setAuthors_facet("Stieg Trenter");
		reqBean.setAuthToken(userauthtoken);
		reqBean.setAvgrate_Max_facet(null);
		reqBean.setAvgrate_Min_facet(null);
		reqBean.setCategoryids_facet(null);
		reqBean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		reqBean.setIncludenotallowedbooks("0");
		reqBean.setLanguage_facet(null);
		reqBean.setLocale("sv_SE");
		reqBean.setModel("Simulator");
		reqBean.setOsinfo("11.4");
	 
		reqBean.setPagenumber("1");
		reqBean.setPagesize("12");
		reqBean.setQuery("stieg");
		reqBean.setSeriesAsit_facet("Harry Friberg");
		reqBean.setSort("default");
		reqBean.setSubscriptionorder_facet(null);
		reqBean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		reqBean.setType("0");
		reqBean.setVer2(false);
		reqBean.setVersion("201");
		reqBean.setFormattype_facet("1");
		
		SearchWithFacetsApptusResponseWrapper wrapper = service.getBooksFromSearchWithFactes(reqBean, apiversion, user);
		System.out.println(wrapper.getBookCount());

		for(FacetList list: wrapper.getFacets())
		{
			System.out.println("####################"+list.getAttribute());
			if(list!=null && list.getValues()!=null)
			for(Value val: list.getValues())
			{
				System.out.println(val.getText()+"-----------------"+ val.getCount()
				+"----------------------------"+val.getCategoryname()+"------------------"+val.isSelected());
			}
			System.out.println("####################");
		}
		
		assertEquals(Boolean.TRUE, Boolean.TRUE);
	}
	
	
	@Test
	public void testOnlyFacetswithSeries() throws Exception
	{
		User user = new User();
		user.setCustomerid(486749);
		user.setMembertype(MemberTypeEnum.MEMBER_EMPLOYEE);
		
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setType("PREMIUM");
		vo.setCountrycode("SE");
		vo.setSubId(7);
		vo.setSubName("Familj Plus");
		vo.setRank((short) 3);
		vo.setIsFamilySubscription(true);
		vo.setMaxChildProfiles(3);
		
		user.setSubscription(vo);
		user.setCountrycode("SE");
		user.setUserCountry("SE");
		
		Profile prf = new Profile();
		prf.setProfileid(231042);
		prf.setParentid(486749);
		prf.setIsparent(true);
		prf.setProfilename("debashis.das@frescano.se");
		prf.setColorindex(0);
		prf.setCategory("all");
		prf.setCreateddate(new Date());
		prf.setLoginkey("F2907DD8A537EE64C60665F0C9D4C5B4");
		prf.setStatus("active");
		user.setProfile(prf);
		
		Double apiversion = new Double("6.5");
		
		
		SearchJsonApptusRequestBean reqBean = new SearchJsonApptusRequestBean();
		
		APITokenRequestBean tokenbean = new APITokenRequestBean();
		tokenbean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		tokenbean.setVer2(false);
		tokenbean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		tokenbean.setAppId("201");
		tokenbean.setOsinfo("11.4");
		tokenbean.setModel("Simulator");
		
		UserAuthToken userauthtoken = new UserAuthToken();
		userauthtoken.setUuid("f77e8f50-73c2-436f-b9e9-e491953a0561");
		userauthtoken.setType(CONSTANTS.TOKEN_TYPE.PT);
		

		reqBean.setApiVersion(new Double("6.5"));
		reqBean.setAppId("201");
		reqBean.setAuthors_facet("Stieg Trenter");
		reqBean.setAuthToken(userauthtoken);
		reqBean.setAvgrate_Max_facet(null);
		reqBean.setAvgrate_Min_facet(null);
		reqBean.setCategoryids_facet(null);
		reqBean.setDeviceid("1869C1C1-77D5-4AEA-B5F1-E1CE8045768B");
		reqBean.setIncludenotallowedbooks("0");
		reqBean.setLanguage_facet(null);
		reqBean.setLocale("sv_SE");
		reqBean.setModel("Simulator");
		reqBean.setOsinfo("11.4");
	 
		reqBean.setPagenumber("1");
		reqBean.setPagesize("12");
		reqBean.setQuery("stieg");
		reqBean.setSeriesAsit_facet("Harry Friberg");
		reqBean.setSort("default");
		reqBean.setSubscriptionorder_facet(null);
		reqBean.setToken("f77e8f50-73c2-436f-b9e9-e491953a0561");
		reqBean.setType("0");
		reqBean.setVer2(false);
		reqBean.setVersion("201");
		reqBean.setFormattype_facet("1");
		
		SearchWithFacetsApptusResponseWrapper wrapper = service.getBooksFromSearchWithFactes(reqBean, apiversion, user);
		System.out.println(wrapper.getBookCount());
		
		SearchWithFacetsApptusResponseWrapper wrapper1 = service.getOnlyFactes(reqBean, apiversion, user);
		System.out.println(wrapper1.getBookCount());

		for(FacetList list: wrapper.getFacets())
		{
			System.out.println("####################"+list.getAttribute());
			if(list!=null && list.getValues()!=null)
			for(Value val: list.getValues())
			{
				System.out.println(val.getText()+"-----------------"+ val.getCount()
				+"----------------------------"+val.getCategoryname()+"------------------"+val.isSelected());
			}
			System.out.println("####################");
		}
		
		for(FacetList list: wrapper1.getFacets())
		{
			System.out.println("####################"+list.getAttribute());
			if(list!=null && list.getValues()!=null)
			for(Value val: list.getValues())
			{
				System.out.println(val.getText()+"-----------------"+ val.getCount()
				+"----------------------------"+val.getCategoryname()+"------------------"+val.isSelected());
			}
			System.out.println("####################");
		}
		
		assertEquals(wrapper.getFacets().size(), wrapper1.getFacets().size());
	}
	
}

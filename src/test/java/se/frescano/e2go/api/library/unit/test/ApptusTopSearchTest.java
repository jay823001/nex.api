package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.MemberTypeEnum;
import se.frescano.nextory.app.product.response.AutoCompleteWrapper;
import se.frescano.nextory.app.request.Profile;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.service.SubscriptionTypeVO;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class ApptusTopSearchTest {
	
	@Autowired 
	private ApptusApiService service;
	
	@Test
	public void testTopSearches() throws Exception
	{
		User user = new User();
		user.setCustomerid(486749);
		user.setMembertype(MemberTypeEnum.MEMBER_EMPLOYEE);
		
		SubscriptionTypeVO vo = new SubscriptionTypeVO();
		vo.setType("PREMIUM");
		vo.setCountrycode("SE");
		vo.setSubId(7);
		vo.setSubName("Familj Plus");
		vo.setRank((short) 3);
		vo.setIsFamilySubscription(true);
		vo.setMaxChildProfiles(3);
		
		user.setSubscription(vo);
		user.setCountrycode("SE");
		user.setUserCountry("SE");
		
		Profile prf = new Profile();
		prf.setProfileid(231042);
		prf.setParentid(486749);
		prf.setIsparent(true);
		prf.setProfilename("debashis.das@frescano.se");
		prf.setColorindex(0);
		prf.setCategory("all");
		prf.setCreateddate(new Date());
		prf.setLoginkey("F2907DD8A537EE64C60665F0C9D4C5B4");
		prf.setStatus("active");
		user.setProfile(prf);
		
		
		AutoCompleteWrapper wrapper = service.getTopSearches(user);
		System.out.println("TOP SEARCHES ################");
		for(String data:wrapper.getTerms())
			System.out.println(data);
		assertTrue(wrapper.getTerms().size()>0);
	}
	
	
	
	
}

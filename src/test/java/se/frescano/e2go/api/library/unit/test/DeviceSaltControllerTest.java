package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class DeviceSaltControllerTest {
	
	 private final String basePath = "http://localhost:9000/api/";
	 private final String deviceSaltPath = "6.4/salt?";
	 private final String deviceServerStatusPath = "6.4/server-status?";
	 private final String deviceBookCountPath = "6.4/book-count?";
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getSaltJsonTest() throws Exception {
		 System.out.println(":::::::::::::::::::: getSaltJsonTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+deviceSaltPath;
	        String urlContent = "appId=200&model=Google%2BPixel&version=2.4.3&deviceid=dj2o1GwuDX4&osinfo=Android%208.0.0&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			        assertNotNull(resp.getJSONObject("data").get("salt"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: getSaltJsonTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getServerStatusJsonTest() throws Exception {
		 System.out.println(":::::::::::::::::::: getServerStatusTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 
		 String path = basePath+deviceServerStatusPath;
	        String urlContent = "appId=200&model=Google%2BPixel&version=2.4.3&deviceid=dj2o1GwuDX4&osinfo=Android%208.0.0&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: getServerStatusTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getBookCountTest() throws Exception {
		 System.out.println(":::::::::::::::::::: getBookCountTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 
		 	String path = basePath+deviceBookCountPath;
	        String urlContent = "appId=201&version=1.1&model=Simulator64&deviceid=dj2o1GwuDX4&osinfo=Android%208.0.0&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("ebookcount"));
			        assertNotNull(jsonObject.get("audiobookcount"));
			        assertNotNull(jsonObject.get("totalbookcount"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: getBookCountTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 

}

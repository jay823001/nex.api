package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.app.request.Profile;

import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.product.response.AutoCompleteWrapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class SearchWithFaceTest {

	@Autowired 
	private ApptusApiService service;
	
	//private User user;
	
	@Test
	public void testdebaproducthistory() throws Exception
	{
		User user = new User();
		user.setCustomerid(1234);
		Profile prf = new Profile();
		prf.setProfileid(123);
		user.setProfile(prf);
		AutoCompleteWrapper wrapper = service.getSearchHistory(user);
		assertEquals(Boolean.TRUE, Boolean.TRUE);
	}
}

package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeviceLibraryControllerTest {
	
	 private final String basePath = "http://localhost:9000/api/6.4/library-download-log?";
	 
	 private static String loginPath = "http://10.91.0.174:9095/api/6.4/login?";
	 public static String token = null;
	 
	 @SuppressWarnings("deprecation")
	 @BeforeClass
	 public static void getAuthKey() throws JSONException {
		 System.out.println(":::::::::::::::::::: getAuthKey Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		    String urlcontent = "password=A9D135B24A7C30319A823D14C93EA4F4&appId=200&checksum=61E51DC16B270C27610A9F2E21A604BA"
		    					+ "&model=Google%2BPixel&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&username=testjoanna.lysen@gmail.com&locale=en";
	        try {
	        	 // Login call
	        	final HttpUriRequest request = new HttpGet(loginPath+urlcontent);
	            HttpResponse loginresp = UnitTestUtil.executeRequest(request);
	            StringWriter writer = new StringWriter();
	            IOUtils.copy(loginresp.getEntity().getContent(), writer);
	            JSONObject resp = new JSONObject(writer.toString());
	            token = resp.getJSONObject("data").getString("token");
	            assertNotNull(token);
	
	          } catch (IOException e) {
	        	  assertFalse(true);
	        }
		System.out.println(":::::::::::::::::::: getAuthKey Testcase ended :::::::::::::::::::::::::::::::::::::::::::"+token);  

	 }
	 
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void libraryDownLoadLogTest_PT_Token() throws Exception {
		 System.out.println(":::::::::::::::::::: libraryDownLoadLogTest_PT_Token Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	
	        String urlContent = "libid=1570907&appId=200&model=Google%2BPixel&logid=1054519&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&status=COMPLETED&token="+token;
	        final HttpUriRequest request = new HttpGet(basePath+urlContent);

	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			        assertNotNull(resp.getJSONObject("data").get("logid"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: libraryDownLoadLogTest_PT_Token Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void libraryDownLoadLogTest_StartedStatus() throws Exception {
		 System.out.println(":::::::::::::::::::: libraryDownLoadLogTest_StartedStatus Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 	
	        String urlContent = "libid=1570907&appId=200&model=Google%2BPixel&logid=1054519&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&status=STARTED&token="+token;
	        final HttpUriRequest request = new HttpGet(basePath+urlContent);

//	        String userPassword = "tele2" + ":" + "tele2pass";
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);

	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			        assertNotNull(resp.getJSONObject("data").get("logid"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: libraryDownLoadLogTest_StartedStatus Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
			
}

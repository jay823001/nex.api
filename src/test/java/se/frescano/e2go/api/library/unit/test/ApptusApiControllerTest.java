package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

public class ApptusApiControllerTest {
	
	 private final String basePath = "http://localhost:9000/api/6.4";
	 private final String aptusGroupsPath = "/groups?";
	 private final String aptusBooksForBookgroupPath = "/booksforbookgroup?";
	 private final String aptusBookInfoPath = "/bookinfo?";
	 private final String aptusSearchbooksPath = "/searchbooks?";
	 private final String aptusReportClickPath = "/reportclick?";
	 private final String aptusAutoCompletePath = "/autocomplete?";
	 public static String token = null;
	 private static String loginPath = "http://10.91.0.174:9095/api/6.4/login?";

	 
	 @SuppressWarnings("deprecation")
	 @BeforeClass
	 public static void getToken() throws JSONException {
		 System.out.println(":::::::::::::::::::: getToken Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		    String urlcontent = "password=A9D135B24A7C30319A823D14C93EA4F4&appId=200&checksum=61E51DC16B270C27610A9F2E21A604BA"
		    					+ "&model=Google%2BPixel&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&username=testjoanna.lysen@gmail.com&locale=en";
	        try {
	        	 // Login call
	        	final HttpUriRequest request = new HttpGet(loginPath+urlcontent);
	            HttpResponse loginresp = UnitTestUtil.executeRequest(request);
	            StringWriter writer = new StringWriter();
	            IOUtils.copy(loginresp.getEntity().getContent(), writer);
	            JSONObject resp = new JSONObject(writer.toString());
	            token = resp.getJSONObject("data").getString("token");
	            assertNotNull(token);
	
	          } catch (IOException e) {
	            e.printStackTrace();
	        }
		System.out.println(":::::::::::::::::::: getToken Testcase ended :::::::::::::::::::::::::::::::::::::::::::"+token);  

	 }
	 
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusGroupsMainTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusGroupsMainTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusGroupsPath;
	        String urlContent = "view=main&appId=200&model=Google%2BPixel&version=2.6&deviceid=pixel&osinfo=Android%208.1.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("bookgroups"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusGroupsMainTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusGroupsLevel2Test() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusGroupsLevel2Test Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusGroupsPath;
	        String urlContent = "view=level2&bookgroupid=ttc_1&appId=200&model=Google%2BPixel&version=2.6&deviceid=pixel&osinfo=Android%208.1.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("bookgroups");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonArray);
			        if(jsonArray.length() > 0) {
			        for (int i = 0; i < jsonArray.length(); i++) {
		                JSONObject json = (JSONObject) jsonArray.get(i);
				        assertNotNull(json.getString("parentid"));
				        assertNotNull(json.getString("id"));
				        assertNotNull(json.getString("type"));
				        assertNotNull(json.getInt("haschild"));
				        assertNotNull(json.getInt("position"));
				        assertNotNull(json.getInt("allowsorting"));
			        }
			      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusGroupsLevel2Test Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusGroupsSeriesTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusGroupsSeriesTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusGroupsPath;
	        String urlContent = "view=series&appId=200&pagesize=12&model=Google%2BPixel&version=2.6&deviceid=pixel&osinfo=Android%208.1.0&locale=sv_SE&pagenumber=0&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("bookgroups");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			        
			        
			        assertNotNull(jsonObject);
			        assertNotNull(jsonArray);
			        assertNotNull(jsonObject.getInt("bookgroupcount"));
			        
			        if(jsonArray.length() > 0) {
			        for (int i = 0; i < jsonArray.length(); i++) {
		                JSONObject json = (JSONObject) jsonArray.get(i);
				        assertNotNull(json.getString("title"));
				        assertNotNull(json.getString("id"));
				        assertNotNull(json.getString("type"));
				        assertNotNull(json.getInt("haschild"));
				        assertNotNull(json.getInt("position"));
				        assertNotNull(json.getInt("allowsorting"));
				        assertNotNull(json.get("covers"));
				        assertNotNull(json.getInt("showvolume"));
			        }
			      }
	                
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusGroupsSeriesTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusGroupsRelatedTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusGroupsRelatedTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusGroupsPath;
	        String urlContent = "view=related&bookid=10001275&appId=200&model=Google%2BPixel&version=2.6&deviceid=pixel&locale=sv_SE&osinfo=Android%208.1.0&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("bookgroups"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusGroupsRelatedTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookgroupTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookgroupTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=tttl_Nyheter&sort=default&type=0&rows=12&version=2.6&locale=sv_SE&deviceid=pixel&osinfo=Android%208.1.0&pagenumber=0"
	        					+ "&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("books");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        if(jsonArray.length() > 0) {
			        	assertNotNull(jsonObject.get("bookcount"));
				        for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getInt("type"));
					        assertNotNull(json.getString("imageurl"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.getString("descriptionbrief"));
					        assertNotNull(json.get("pubdate"));
					        assertNotNull(json.get("authors"));
					        assertNotNull(json.getInt("coverratio"));
					        assertNotNull(json.getInt("allowedinlibrary"));
					        assertNotNull(json.getInt("relatedbookallowedinlibrary"));
					        assertNotNull(json.getString("esalesticket"));
					        assertNotNull(json.get("rank"));
					        assertNotNull(json.getInt("position"));

				        }
				      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookgroupTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookgroupMainCategoryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookgroupMainCategoryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=ttc_1&sort=default&type=0&rows=12&version=2.6&deviceid=pixel&locale=sv_SE&osinfo=Android%208.1.0&pagenumber=0&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("books");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        
			        if(jsonArray.length() > 0) {
				        for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getInt("type"));
					        assertNotNull(json.getString("imageurl"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.getString("descriptionbrief"));
					        assertNotNull(json.get("pubdate"));
					        assertNotNull(json.get("authors"));
//					        assertNotNull(json.getInt("availableinlib"));
					        assertNotNull(json.getInt("allowedinlibrary"));
					        assertNotNull(json.getInt("relatedbookallowedinlibrary"));
					        assertNotNull(json.getString("esalesticket"));
					        assertNotNull(json.get("rank"));
					        assertNotNull(json.getInt("position"));
					        
				        }
				      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookgroupMainCategoryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }  
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookgroupSubCategoryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookgroupSubCategoryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=sc_11&sort=default&type=0&rows=12&version=2.6&locale=sv_SE&deviceid=pixel&osinfo=Android%208.1.0&pagenumber=0&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("books");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        
			        if(jsonArray.length() > 0) {
				        for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getInt("type"));
					        assertNotNull(json.getString("imageurl"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.getString("descriptionbrief"));
					        assertNotNull(json.get("pubdate"));
					        assertNotNull(json.get("authors"));
//					        assertNotNull(json.getInt("availableinlib"));
					        assertNotNull(json.getInt("allowedinlibrary"));
					        assertNotNull(json.getInt("relatedbookallowedinlibrary"));
					        assertNotNull(json.getString("esalesticket"));
					        assertNotNull(json.get("rank"));
					        assertNotNull(json.getInt("position"));
					        
				        }
				      }
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookgroupSubCategoryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

/*	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookGroupsMaincategoryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookgroupMaincategoryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=ttc_1&sort=default&type=0&rows=12&version=2.6&deviceid=c0xBWSTC8Uc&osinfo=Android%208.1.0"
	        		          + "&pagenumber=0&locale=sv_SE&token=00f45b9a-aefe-4515-8901-5f36dd4e5872";	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookgroupMaincategoryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    } 
	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookGroupsSubcategoryTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookgroupSubcategoryTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=sc_11&sort=default&type=0&rows=12&version=2.6&deviceid=c0xBWSTC8Uc&osinfo=Android%208.1.0"
	        		          + "&pagenumber=0&locale=sv_SE&token=00f45b9a-aefe-4515-8901-5f36dd4e5872";	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookgroupSubcategoryTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }*/ 
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookGroupsRelatedBookSeriesTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookGroupsRelatedSeriesTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=r_10027879_author&sort=default&type=0&rows=12&version=2.6&locale=sv_SE&deviceid=pixel&osinfo=Android%208.1.0&pagenumber=0&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("books");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        
			        if(jsonArray.length() > 0) {
				        for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getInt("type"));
					        assertNotNull(json.getString("imageurl"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.getString("descriptionbrief"));
					        assertNotNull(json.get("pubdate"));
					        assertNotNull(json.get("authors"));
//					        assertNotNull(json.getInt("availableinlib"));
					        assertNotNull(json.getInt("allowedinlibrary"));
					        assertNotNull(json.getInt("relatedbookallowedinlibrary"));
					        assertNotNull(json.getString("esalesticket"));
					        assertNotNull(json.get("rank"));
					        assertNotNull(json.getInt("position"));
					        
				        }
				      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookGroupsRelatedSeriesTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBooksForBookGroupsRelatedBookRecommendedTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBooksForBookGroupsRelatedBookRecommendedTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBooksForBookgroupPath;
	        String urlContent = "appId=200&model=Google%2BPixel&bookgroupid=r_10027879_rec&sort=default&type=0&rows=12&version=2.6&locale=sv_SE&deviceid=pixel&osinfo=Android%208.1.0&pagenumber=0&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());

	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("books");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        
			        if(jsonArray.length() > 0) {
				        for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getInt("type"));
					        assertNotNull(json.getString("imageurl"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.getString("descriptionbrief"));
					        assertNotNull(json.get("pubdate"));
					        assertNotNull(json.get("authors"));
					        assertNotNull(json.getInt("availableinlib"));
					        assertNotNull(json.getInt("allowedinlibrary"));
					        assertNotNull(json.getInt("relatedbookallowedinlibrary"));
					        assertNotNull(json.getString("esalesticket"));
					        assertNotNull(json.get("rank"));
					        assertNotNull(json.getInt("position"));
					        
				        }
				      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBooksForBookGroupsRelatedBookRecommendedTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    } 
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusBookInfoTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusBookInfoTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusBookInfoPath;
	        String urlContent = "appId=200&model=Google%2BPixel&id=10039620&version=2.6&deviceid=pixel&osinfo=Android%208.1.0&locale=sv_SE&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());

	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONObject json = jsonObject.getJSONObject("books");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        
			        if(json != null) {
			                assertNotNull(json.getLong("id"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getInt("type"));
					        assertNotNull(json.getString("imageurl"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.getLong("relatedbookallowedinlibrary"));
					        assertNotNull(json.getString("descriptionfull"));
					        assertNotNull(json.getString("publisher"));
					        assertNotNull(json.getString("duration"));
					        assertNotNull(json.getInt("availableinlib"));
					        assertNotNull(json.getInt("libid"));
					        assertNotNull(json.getInt("allowedinlibrary"));
					        assertNotNull(json.getString("allowedforstring"));
					        assertNotNull(json.getString("displaymessage"));
					        assertNotNull(json.get("authors"));
					        assertNotNull(json.get("narrators"));
					        assertNotNull(json.get("translators"));
					        assertNotNull(json.getInt("avgrate"));
					        assertNotNull(json.getInt("numberofrates"));
					        assertNotNull(json.getString("datemodified"));
					        assertNotNull(json.getString("pubdate"));
					        assertNotNull(json.getInt("userrate"));
				      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusBookInfoTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 @SuppressWarnings("deprecation")
		 @Test
		 public void aptusSearchBooksTest() throws Exception {
			 System.out.println(":::::::::::::::::::: aptusSearchBooksTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
			 String path = basePath+aptusSearchbooksPath;
		        String urlContent = "appId=200&query=calendar%20girl&model=Google%2BPixel&sort=default&type=0&version=2.6&deviceid=pixel&locale=sv_SE&osinfo=Android%208.1.0&pagenumber=0&token="+token;	        
		        final HttpUriRequest request = new HttpGet(path+urlContent);
		        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
		        JSONObject resp = null;
		        try {
		        	StringWriter writer = new StringWriter();
		            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
		            resp = new JSONObject(writer.toString());
		        	
		            JSONObject jsonObject = resp.getJSONObject("data");
		            JSONArray jsonArray = jsonObject.getJSONArray("books");
		  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
				        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
				        assertNotNull(resp);
				        assertNotNull(resp.getJSONObject("data"));
				        assertNotNull(jsonObject);
				        if(jsonArray.length() > 0) {
				        	assertNotNull(jsonObject.getInt("bookcount"));
				        	assertNotNull(jsonObject.getInt("notallowedbookcount"));
					        for (int i = 0; i < jsonArray.length(); i++) {
				                JSONObject json = (JSONObject) jsonArray.get(i);
				                assertNotNull(json.getLong("id"));
						        assertNotNull(json.getString("title"));
						        assertNotNull(json.getInt("type"));
						        assertNotNull(json.getString("imageurl"));
						        assertNotNull(json.getString("weburl"));
						        assertNotNull(json.getLong("relatedbookid"));
						        assertNotNull(json.getString("descriptionbrief"));
						        assertNotNull(json.getString("pubdate"));
						        assertNotNull(json.get("authors"));
	//					        assertNotNull(json.getInt("availableinlib"));
						        assertNotNull(json.getInt("allowedinlibrary"));
						        assertNotNull(json.getInt("relatedbookallowedinlibrary"));
						        assertNotNull(json.getString("esalesticket"));
						        assertNotNull(json.get("rank"));
						        assertNotNull(json.getInt("position"));
						        
					        }
					      }
				        
				} catch (JSONException e) {
					e.printStackTrace();
				}	       
	
		        System.out.println(":::::::::::::::::::: aptusSearchBooksTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
		    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusReportClickTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusReportClickTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusReportClickPath;
	        String urlContent = "esalesticket=Oy9zZWFyY2gtem9uZS1kZWJhL3NlYXJjaC1oaXRzOyM7cHJvZHVjdF9rZXk7MTM3Nzs5Nzg5MTEzMDc1OTM4O09CSkVDVElWRSQ7Tk9ORTpOT05FOzQ3Ow"
	        					+ "&appId=200&model=Google%2BPixel&id=10027879&version=2.6&deviceid=pixel&locale=sv_SE&osinfo=Android%208.1.0&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusReportClickTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void aptusAutoCompleteTest() throws Exception {
		 System.out.println(":::::::::::::::::::: aptusAutoCompleteTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+aptusAutoCompletePath;
	        String urlContent = "appId=200&model=Google%2BPixel&keyword=spi&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;	        
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("terms"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: aptusAutoCompleteTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

}

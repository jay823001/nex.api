package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class UnitTestUtil {
	
	private String path = "http://localhost:9095/api/web/3.0/authenticate";
	public static Double webversion = 4.0;
	public static String authKey = null;
	final public static String jsonMimeType = "application/json";
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getAuthKey() throws JSONException {
		 System.out.println(":::::::::::::::::::: getAuthKey Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		    HttpClient client = HttpClientBuilder.create().build();
	        HttpPost post = new HttpPost(path);
	        
	        // Create some NameValuePair for HttpPost parameters
	        List<NameValuePair> arguments = new ArrayList<>(3);
	        arguments.add(new BasicNameValuePair("password", "7A607C5C5E3E7F6D5001BE7DAD08F60C"));
	        arguments.add(new BasicNameValuePair("email", "test_8Janpaulina@e2go.se"));
	        arguments.add(new BasicNameValuePair("serverdetails", "EB47DB1AB63689E010D28582DD3EC58F"));
	        try {
	            post.setEntity(new UrlEncodedFormEntity(arguments));
	            HttpResponse response = client.execute(post);
	            
	            StringWriter writer = new StringWriter();
	            IOUtils.copy(response.getEntity().getContent(), writer);
	            JSONObject resp = new JSONObject(writer.toString());
	            authKey = resp.getJSONObject("data").getString("authkey");
	            assertNotNull(authKey);
	            
	          } catch (IOException e) {
	            e.printStackTrace();
	        }
		
	 }
	 
	 public static String getKey() throws JSONException {
		 //new UnitTestUtil().getAuthKey();
		 System.out.println("::::::::::::::::: authKey is :::::::::::::::::::::"+authKey);
		 return authKey;
	 }
	 
	public static HttpResponse executeRequest(final HttpUriRequest request)
			throws IOException, ClientProtocolException {
		final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		return httpResponse;
	}
	
	/*private static String getJsonString() {
		String password = "7A607C5C5E3E7F6D5001BE7DAD08F60C";
		String email = "test_8Janpaulina@e2go.se";
		String serverdetails = "EB47DB1AB63689E010D28582DD3EC58F";
		String json = (String) ("{\"email\" : \""+email+"\", \"password\" : \""+password+"\", \"serverdetails\" : \""+serverdetails+"\"}");
		return json;
	}
	
	private static HttpPost getHttpPostObject(String path) throws UnsupportedEncodingException, IOException {
		HttpPost post = new HttpPost(path);
		
//		byte[] encodeBase64 = Base64.encodeBase64(userPassword.getBytes());
//		post.addHeader("Authorization", "Basic " + new String(encodeBase64));
		post.setHeader("Content-Type", "application/json");
		post.setHeader("Accept", "application/json");
		post.setHeader("X-Stream" , "true");
		
		return post;
	}
   
	static HttpResponse executeHttpRequest(String jsonData,  HttpPost httpPost)  throws UnsupportedEncodingException, IOException
    {
        httpPost.setEntity(new StringEntity(jsonData));
        
        final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(httpPost);
        return httpResponse;
    } */
}

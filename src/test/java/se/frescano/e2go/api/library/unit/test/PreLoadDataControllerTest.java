package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class PreLoadDataControllerTest {

	 private final String basePath = "http://localhost:9000/api/web/"+UnitTestUtil.webversion;
	 private final String subscriptions = "/subscriptions?";
	 private final String bookformats = "/bookformats?";
	 private final String categories = "/categories?";
	 private final String mycollabs = "/mycollabs?";
	 private final String errorcodes = "/errorcodes?"; 
	 private final String validations = "/validations?"; 
	 private final String systemproperties = "/systemproperties?";
	 private final String pageMeta = "/page-meta?"; 
	 private final String cmsurls = "/cms-urls?"; 
	 private final String cmsPage = "/cms-page?"; 

	 @SuppressWarnings("deprecation")
	 @Test
	 public void subscriptionsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: subscriptionsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+subscriptions;
	        String urlContent = "countrycode=SE&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        if(UnitTestUtil.webversion == 4.0) {
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("defaultsubid"));
			        JSONArray jsonArray = jsonObject.getJSONArray("subscriptions");
			        if(jsonArray.length() > 0) {
			        	for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
					        assertNotNull(json.get("offerprice"));
					        assertNotNull(json.get("subid"));
					        assertNotNull(json.getString("subname"));
					        assertNotNull(json.getInt("subprice"));
					        assertNotNull(json.getInt("sublimit"));
					        assertNotNull(json.get("status"));
					        assertNotNull(json.get("subscriptiontype"));
					        assertNotNull(json.get("ranking"));
					        assertNotNull(json.get("isfamilysubscription"));
					        assertNotNull(json.get("maxchildprofiles"));
					        assertNotNull(json.get("maxrootlogin"));
					        assertNotNull(json.get("maxchildlogin"));
					        assertNotNull(json.get("maxconsumers"));
					        assertNotNull(json.get("countrycode"));
				        }
			        }
			        }
			        
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: subscriptionsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void bookformatsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: bookformatsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+bookformats;
	        String urlContent = "locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        if(UnitTestUtil.webversion == 4.0) {
			        assertNotNull(jsonObject);
			        
			        JSONArray jsonArray = jsonObject.getJSONArray("formats");
			        if(jsonArray.length() > 0) {
			        	for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
					        assertNotNull(json.get("formatid"));
					        assertNotNull(json.get("formatname"));
				        }
			        }
			        }
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: bookformatsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void categoriesTest() throws Exception {
		 System.out.println(":::::::::::::::::::: categoriesTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+categories;
	        String urlContent = "locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        if(UnitTestUtil.webversion == 4.0) {
			        assertNotNull(jsonObject);
			       
			        JSONArray jsonArray = jsonObject.getJSONArray("categories");
			        if(jsonArray.length() > 0) {
			        	for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
					        assertNotNull(json.get("categoryid"));
					        assertNotNull(json.get("parentcatid"));
					        assertNotNull(json.get("categoryname"));
					        assertNotNull(json.get("isactive"));
					        assertNotNull(json.get("slugname"));
					        assertNotNull(json.get("popular"));
					        assertNotNull(json.get("position"));
				        }
			          }
			        }
			} catch (JSONException e) {
				e.printStackTrace();
			}	       
	        System.out.println(":::::::::::::::::::: categoriesTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void mycollabsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: mycollabsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+mycollabs;
	        String urlContent = "locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        if(UnitTestUtil.webversion == 4.0) {
			        assertNotNull(jsonObject);
			       
			        JSONArray jsonArray = jsonObject.getJSONArray("collabs");
			        if(jsonArray.length() > 0) {
			        	for (int i = 0; i < jsonArray.length(); i++) {
			                JSONObject json = (JSONObject) jsonArray.get(i);
					        assertNotNull(json.get("collabid"));
					        assertNotNull(json.get("companyname"));
					        assertNotNull(json.get("expireson"));
					        assertNotNull(json.get("title"));
					        assertNotNull(json.get("subtitle"));
					        assertNotNull(json.get("description"));
					        assertNotNull(json.get("picture"));
					        assertNotNull(json.get("status"));
					        assertNotNull(json.get("link"));
				        }
			          }
			        }
	  		
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: mycollabsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void errorcodesTest() throws Exception {
		 System.out.println(":::::::::::::::::::: errorcodesTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+errorcodes;
	        String urlContent = "locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONArray jsonArray = resp.getJSONArray("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonArray);
			        if(jsonArray.length() > 0) {
			        for (int i = 0; i < jsonArray.length(); i++) {
			        JSONObject json = (JSONObject) jsonArray.get(i);
			        assertNotNull(json.get("code"));
			        assertNotNull(json.get("name"));
			        assertNotNull(json.get("message"));
			        }
			      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: errorcodesTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void validationsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: validationsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+validations;
	        String urlContent = "locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: validationsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void systempropertiesTest() throws Exception {
		 System.out.println(":::::::::::::::::::: systempropertiesTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+systemproperties;
	        String urlContent = "locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: systempropertiesTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void pageMetaTest() throws Exception {
		 System.out.println(":::::::::::::::::::: pageMetaTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+pageMeta;
	        String urlContent = "pageUrl=/e-bocker/&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: pageMetaTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void cmsurlsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: cmsurlsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+cmsurls;
	        String urlContent = "pageUrl=/e-bocker/&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: cmsurlsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

	 @SuppressWarnings("deprecation")
	 @Test
	 public void cmsPageTest() throws Exception {
		 System.out.println(":::::::::::::::::::: cmsPageTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+cmsPage;
	        String urlContent = "pageUrl=/e-bocker/&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: cmsPageTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

}

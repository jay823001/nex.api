package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeviceFetchProductsControllerTest {
	
	 private final String basePath = "http://localhost:9000/api/6.4";
	 private final String bookRatingPath = "/ratebook?";
	 private static String loginPath = "http://10.91.0.174:9095/api/6.4/login?";
	 public static String token = null;
	 
	 @SuppressWarnings("deprecation")
	 @BeforeClass
	 public static void getToken() throws JSONException {
		 System.out.println(":::::::::::::::::::: gettoken Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		    String urlcontent = "password=A9D135B24A7C30319A823D14C93EA4F4&appId=200&checksum=61E51DC16B270C27610A9F2E21A604BA"
		    					+ "&model=Google%2BPixel&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&username=testjoanna.lysen@gmail.com&locale=en";
	        try {
	        	 // Login call
	        	final HttpUriRequest request = new HttpGet(loginPath+urlcontent);
	            HttpResponse loginresp = UnitTestUtil.executeRequest(request);
	            StringWriter writer = new StringWriter();
	            IOUtils.copy(loginresp.getEntity().getContent(), writer);
	            JSONObject resp = new JSONObject(writer.toString());
	            token = resp.getJSONObject("data").getString("token");
	            assertNotNull(token);
	
	          } catch (IOException e) {
	        	  assertFalse(true);
	        }
		System.out.println(":::::::::::::::::::: gettoken Testcase ended :::::::::::::::::::::::::::::::::::::::::::"+token);  

	 }
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void bookRatingTest() throws Exception {
		 System.out.println(":::::::::::::::::::: bookRatingTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+bookRatingPath;
	        String urlContent = "appId=200&rating=5&model=Google%2BPixel&id=10039620&version=2.4.3&deviceid=pixel&osinfo=Android%208.0.0&locale=sv_SE&token="+token;
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            JSONObject jsonObject = resp.getJSONObject("data");
	            
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("avgrate"));
			        assertNotNull(jsonObject.get("numberofrates"));
			        
			} catch (JSONException e) {
				assertFalse(true);
			}	       

	        System.out.println(":::::::::::::::::::: bookRatingTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

}

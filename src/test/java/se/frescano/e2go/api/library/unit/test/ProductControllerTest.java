package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class ProductControllerTest {
	
	 private final String basePath = "http://localhost:9000/fetch/productimage?";
	 
	 
	 @SuppressWarnings("deprecation")
	 @Test
	 public void bookidTest() throws Exception {
		 System.out.println(":::::::::::::::::::: bookidTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 
	        
	        String urlContent = "bookid=10001275&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(basePath+urlContent);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getString("bookid"));
			        assertNotNull(resp.getString("description"));
			        assertNotNull(resp.getString("lastupdateddate"));
			        assertNotNull(resp.get("coverimage"));
			        assertTrue(resp.getString("status").equals("success"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: bookidTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
}

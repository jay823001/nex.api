package se.frescano.e2go.api.library.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

public class ApptusControllerTest {
	
	 private final String basePath = "http://localhost:9000/api/web/"+UnitTestUtil.webversion;
	 private final String bocker = "/bocker?";
	 private final String categoryViewAll = "/categoryViewAll?";
	 private final String book = "/book?";
	 private final String search = "/search?";
	 private final String suggest = "/suggest?"; 
	 private final String categorySpecificLoadStep = "/categorySpecificLoadStep?"; 
	 private final String MaincategorySpecificLoadStep = "/MaincategorySpecificLoadStep?";
	 private static String path      = "http://10.91.0.174:9095/api/web/3.0/authenticate";
	 private static String loginPath = "http://10.91.0.174:9095/api/web/3.0/login";
	 public static String authKey = null;
	 public static String nx_rt = null;
	 public static String nx_at = null;
	 
	 @SuppressWarnings("deprecation")
	 @BeforeClass
	 public static void getAuthKey() throws JSONException {
		 System.out.println(":::::::::::::::::::: getAuthKey Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		    HttpClient client = HttpClientBuilder.create().build();
	        HttpPost post = new HttpPost(path);
	        HttpPost loginpost = new HttpPost(loginPath);
	        
	        List<NameValuePair> arguments = new ArrayList<>(3);
	        arguments.add(new BasicNameValuePair("password", "993E7E104A6190CA17F2F9E96A47758C"));
	        arguments.add(new BasicNameValuePair("email", "testsandra.freij@hotmail.com"));
	        arguments.add(new BasicNameValuePair("serverdetails", "EB47DB1AB63689E010D28582DD3EC58F"));
	        List<NameValuePair> loginarguments = new ArrayList<>(3);
	        loginarguments.add(new BasicNameValuePair("password", "A9D135B24A7C30319A823D14C93EA4F4"));
	        loginarguments.add(new BasicNameValuePair("email", "testjoanna.lysen@gmail.com"));
	        loginarguments.add(new BasicNameValuePair("serverdetails", "236D928B599BF6C2BB82F295C748F80E"));
	        
	        try {
	        	 // Login call
	        	loginpost.setEntity(new UrlEncodedFormEntity(loginarguments));
	            HttpResponse loginresp = client.execute(loginpost);
	            Header[] allHeaders = loginresp.getAllHeaders();
	            for(Header hdr : allHeaders) {
	            	if(hdr.getName().equalsIgnoreCase("nx-at")){
	            		nx_at = hdr.getValue();
	            	} else if(hdr.getName().equalsIgnoreCase("nx-rt")){
	            		nx_rt = hdr.getValue();
	            	}
	            }
	            assertNotNull(nx_rt);
	            post.setEntity(new UrlEncodedFormEntity(arguments));
	            HttpResponse response = client.execute(post);
	            StringWriter writer = new StringWriter();
	            IOUtils.copy(response.getEntity().getContent(), writer);
	            JSONObject resp = new JSONObject(writer.toString());
	            authKey = resp.getJSONObject("data").getString("authkey");
	            assertNotNull(authKey);
	          } catch (IOException e) {
	            e.printStackTrace();
	        }
	 }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getBockerDetailsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: getBockerDetailsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+bocker;
	        String urlContent = "countrycode=SE&locale=sv_SE&authkey="+authKey;
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("bookgroups");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject.get("toptitles"));
			        assertNotNull(jsonObject);
			        assertNotNull(jsonArray);
			        
			        if(UnitTestUtil.webversion == 4.0 && jsonArray.length() > 0) {
			        for (int i = 0; i < jsonArray.length(); i++) {
			        	
		                JSONObject json = (JSONObject) jsonArray.get(i);
		                assertNotNull(json.getInt("bookgroupid"));
		                assertNotNull(json.getString("name"));
		                assertNotNull(json.getString("slug"));
		                assertNotNull(json.getBoolean("iscategory"));
		                assertNotNull(json.getBoolean("isparentcategory"));
		                assertNotNull(json.get("formattype"));
		                assertNotNull(json.getString("sorton"));
		                assertNotNull(json.getJSONArray("books"));
			        }
			      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: getBockerDetailsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void categoryViewAllTest() throws Exception {
		 System.out.println(":::::::::::::::::::: categoryViewAllTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+categoryViewAll;
	        String urlContent = "catslug=bocker&subcatslug=topplista_ljudbocker&format=&windowsize=50&toplist=&sorton=&pagenumber=1&subscription=Familj&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	            JSONObject jsonObject = resp.getJSONObject("data");
	           
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			       
			       if(jsonObject.getJSONArray("books") != null && UnitTestUtil.webversion == 4.0) {
			    	    JSONArray jsonArray = jsonObject.getJSONArray("books");
			    	    assertNotNull(jsonArray);
				        assertNotNull(jsonObject.getInt("bookgroupid"));
				        assertNotNull(jsonObject.getString("name"));
				        assertNotNull(jsonObject.get("formattype"));
				        assertNotNull(jsonObject.getString("sorton"));
				        assertNotNull(jsonObject.get("parentid"));
				        assertNotNull(resp.getString("status"));
				        if(jsonArray.length() > 0) {
				        for (int i = 0; i < jsonArray.length(); i++) {
				        	
			                JSONObject json = (JSONObject) jsonArray.get(i);
			                assertNotNull(json.getLong("bookid"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.getLong("isbn"));
					        assertNotNull(json.getString("coverimg"));
					        assertNotNull(json.getString("weburl"));
					        assertNotNull(json.getLong("relatedbookid"));
					        assertNotNull(json.get("formattype"));
					        assertNotNull(json.get("rank"));
					        assertNotNull(json.getInt("relevance"));
				        }
				      }
				   } else {
					    assertNotNull(resp.getString("status"));
				        assertNotNull(jsonObject.get("parentid"));
				   }

			} catch (JSONException e) {
				e.printStackTrace();
			}	       
	        System.out.println(":::::::::::::::::::: categoryViewAllTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	private void setRequestHeaders(final HttpUriRequest request) {
		request.setHeader("nx-at", nx_at);
		request.addHeader(new BasicHeader("Cookie", "nx-rt="+nx_rt+"; nx-at="+nx_at+"; path="+basePath));
		request.setHeader("Referer", "localhost");
		request.setHeader("Content-Type", "application/json");
		request.setHeader("Accept", "application/json");
		request.setHeader("X-Stream" , "true");
	}
//	@Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void MaincategorySpecificLoadStepTest() throws Exception {
		 System.out.println(":::::::::::::::::::: MaincategorySpecificLoadStepTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+MaincategorySpecificLoadStep;
	        String urlContent = "subscription=STANDARD&format=-1&windowsize=50&pagenumber=1&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("bookgroups");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.get("toptitles"));
			        assertNotNull(jsonArray);
			        
			        if(UnitTestUtil.webversion == 4.0 && jsonArray.length() > 0) {
			        for (int i = 0; i < jsonArray.length(); i++) {
			        	
		                JSONObject json = (JSONObject) jsonArray.get(i);
		                assertNotNull(json.getInt("bookgroupid"));
		                assertNotNull(json.getString("name"));
		                assertNotNull(json.getString("slug"));
		                assertNotNull(json.getBoolean("iscategory"));
		                assertNotNull(json.getBoolean("isparentcategory"));
		                assertNotNull(json.get("formattype"));
		                assertNotNull(json.getString("sorton"));
		                assertNotNull(json.getJSONArray("books"));
			        }
			      }
			        
			        
	  			 
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: MaincategorySpecificLoadStepTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void categorySpecificLoadStepTest() throws Exception {
		 System.out.println(":::::::::::::::::::: categorySpecificLoadStepTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+categorySpecificLoadStep;
	        String urlContent = "subscription=STANDARD&pagenumber=2&format=1&windowsize=50&categoryslug=barnbocker&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(resp.getJSONObject("data"));
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: categorySpecificLoadStepTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
	 
	 @SuppressWarnings("deprecation")
	 @Test
//	 @Ignore
	 public void searchSuggestsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: searchSuggestsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+suggest;
	        String urlContent = "keyword=HY&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	            
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONArray jsonArray = jsonObject.getJSONArray("completions");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        assertNotNull(jsonObject.getString("keyword"));
			        assertNotNull(jsonObject.get("books"));
			        assertNotNull(jsonArray);
			        
			        if(UnitTestUtil.webversion == 4.0 && jsonArray.length() > 0) {
			        for (int i = 0; i < jsonArray.length(); i++) {
		                JSONObject json = (JSONObject) jsonArray.get(i);
				        assertNotNull(json.getString("text"));
				        assertNotNull(json.getString("ticket"));
			        }
			      }
			        
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: searchSuggestsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void searchBooksTest() throws Exception {
		 System.out.println(":::::::::::::::::::: searchBooksTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+search;
	        String urlContent = "type=search&keyword=allt-du-onskar-kristin-emilsson&subscription=STANDARD&pagenumber=1&windowsize=10&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        if(UnitTestUtil.webversion == 4.0) {
			        assertNotNull(jsonObject.getString("searchon"));
			        assertNotNull(jsonObject.getString("esalesticket"));
			        assertNotNull(jsonObject.getString("keyword"));
			        assertNotNull(jsonObject.getInt("bookcount"));
			        assertNotNull(jsonObject.get("books"));
			        assertNotNull(jsonObject.get("toptitles"));
			        }
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: searchBooksTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }
	 
//	 @Ignore
	 @SuppressWarnings("deprecation")
	 @Test
	 public void getBookDetailsTest() throws Exception {
		 System.out.println(":::::::::::::::::::: getBookDetailsTest Testcase started :::::::::::::::::::::::::::::::::::::::::::");  
		 String path = basePath+book;
	        String urlContent = "isbn=9789129705287&esalesticket=&locale=sv_SE";
	        final HttpUriRequest request = new HttpGet(path+urlContent);
	        setRequestHeaders(request);
	        final HttpResponse httpResponse = UnitTestUtil.executeRequest(request);
	        JSONObject resp = null;
	        try {
	        	StringWriter writer = new StringWriter();
	            IOUtils.copy(httpResponse.getEntity().getContent(), writer);
	            resp = new JSONObject(writer.toString());
	        	
	            JSONObject jsonObject = resp.getJSONObject("data");
	            JSONObject json = jsonObject.getJSONObject("bookdetails");
	  			 final String mimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
			        assertEquals(UnitTestUtil.jsonMimeType, mimeType);
			        assertNotNull(resp);
			        assertNotNull(jsonObject);
			        if(UnitTestUtil.webversion == 4.0) {
			        assertNotNull(jsonObject.get("authorbooks"));
			        assertNotNull(jsonObject.get("seriesbooks"));
			        assertNotNull(jsonObject.getJSONArray("recomondedbooks"));
			        
			        if(json != null) {
			                assertNotNull(json.getLong("bookid"));
					        assertNotNull(json.getString("coverimg"));
					        assertNotNull(json.get("relatedbook"));
					        assertNotNull(json.getString("isbn"));
					        assertNotNull(json.getString("title"));
					        assertNotNull(json.get("authors"));
					        assertNotNull(json.get("narattors"));
					        assertNotNull(json.get("translators"));
					        assertNotNull(json.getString("publisher"));
					        assertNotNull(json.getString("description"));
					        assertNotNull(json.get("formattype"));
					        assertNotNull(json.get("publishdate"));
					        assertNotNull(json.get("bookallowedfor"));
					        assertNotNull(json.getString("language"));
					        assertNotNull(json.getString("sequence"));
					        assertNotNull(json.get("seriesname"));
					        assertNotNull(json.get("bookLengthValue"));
					        assertNotNull(json.get("ratings"));

				      }
	              }
			} catch (JSONException e) {
				e.printStackTrace();
			}	       

	        System.out.println(":::::::::::::::::::: getBookDetailsTest Testcase ended :::::::::::::::::::::::::::::::::::::::::::");
	    }

}

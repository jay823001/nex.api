package se.frescano.nextory.web.apptus.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.databind.ObjectMapper;

import se.frescano.ApplicationPropertiesConfig;
import se.frescano.nextory.app.apptus.service.ApptusApiService;
import se.frescano.nextory.app.constants.CONSTANTS;
import se.frescano.nextory.app.product.request.MainJsonRequestBean;
import se.frescano.nextory.app.response.APIJsonWrapper;
import se.frescano.nextory.app.response.BookGroupsApptusResponseWrapper;
import se.frescano.nextory.auth.service.AuthenticationServiceImpl;

import se.frescano.nextory.spring.method.model.User;
import se.frescano.nextory.startup.service.StartUpBean;
import se.frescano.nextory.util.InMemoryCache;
import se.frescano.nextory.web.api.exception.WEBAPIExceptionHandler;
import se.frescano.nextory.web.api.exception.WebApiException;
import se.frescano.nextory.web.api.interceptor.WebAuthInterceptor;

import se.frescano.nextory.web.api.request.WebApiRequestBean;
import se.frescano.nextory.web.api.response.GenericAPIResponse;
import se.frescano.nextory.web.api.spring.method.resolver.CurrentWebUserMethodArgumentResolver;
import se.frescano.nextory.web.api.util.WEBErrorCodeEnum;
import se.frescano.nextory.web.api.validator.BookGroupsRequestValidator;
import se.frescano.nextory.web.api.validator.BooksBookGroupRequestValidator;
import se.frescano.nextory.web.api.validator.SearchBooksRequestBeanValidator;
import se.frescano.nextory.web.api.validator.SearchSuggestRequestBeanValidator;





@WebAppConfiguration
@RunWith(SpringRunner.class)
//@WebMvcTest(WebRegistrationAPIController.class)
@ContextConfiguration(classes = {ApptusWebController.class,WebAuthInterceptor.class,GenericAPIResponse.class,
		ObjectMapper.class,CurrentWebUserMethodArgumentResolver.class, APIJsonWrapper.class})
//@ContextConfiguration(classes={ NextoryUserAppConfig.class, ApplicationPropertiesConfig.class,NextoryMVCAdapter.class })
@ActiveProfiles("test")
public class ApptusWebControllerTest {
	public static final Logger logger = LoggerFactory.getLogger(ApptusWebControllerTest.class);
	
	private MockMvc mvc;

    @Autowired
    ApptusWebController apptusWebController;
    
    @MockBean
    private ApplicationPropertiesConfig appConfig;
    
    @Autowired
    WebAuthInterceptor authInterceptor;
   
    @MockBean
    private AuthenticationServiceImpl authService;
    
   
    
    @MockBean
	private	ApptusApiService	apptusApiService;
    
    private String basicAuth;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Autowired
    GenericAPIResponse apiResponse;
    
    @Autowired
    APIJsonWrapper apiJSONResponse;
    
    @MockBean
    private CurrentWebUserMethodArgumentResolver webUserResolver;
    
    @MockBean
    private HttpServletRequest httpRequest;
    
    @MockBean
	private SearchBooksRequestBeanValidator searchRequestValidator;
	
    @MockBean
	private SearchSuggestRequestBeanValidator suggestRequestValidator;
	
    @MockBean
	private BookGroupsRequestValidator bookGroupsRequestValidator;

    @MockBean
	private BooksBookGroupRequestValidator booksRequestValidator;
    
	@Before
	public void setUp() throws Exception {
		
				MockitoAnnotations.initMocks(this);
				//authInterceptor = new WebAuthInterceptor();
				 
				/*authInterceptor.setNextoryReqAuthUsername("test");
				authInterceptor.setNextoryReqAuthPassword("test11");
				authInterceptor.setACCESS_TOKEN_HEADER("nx-at");
				authInterceptor.setREFRESH_TOKEN_HEADER("nx-rt");*/
				/*appConfig.setNextoryReqAuthPassword("test");
				appConfig.setNextoryReqAuthPassword("test123");
				appConfig.setACCESS_TOKEN_HEADER("nx-at");
				appConfig.setREFRESH_TOKEN_HEADER("nx-rt");*/
				authInterceptor.setAuthenticationservice(authService);
				authInterceptor.setApplicationpropconfig(appConfig);
				
				// webUserResolver = new CurrentWebUserMethodArgumentResolver();
				//webRegistrationAPIController = new WebRegistrationAPIController();
				apptusWebController.setBookGroupsRequestValidator(bookGroupsRequestValidator);
				apptusWebController.setBooksRequestValidator(booksRequestValidator);
				apptusWebController.setSearchRequestValidator(searchRequestValidator);
				apptusWebController.setSuggestRequestValidator(suggestRequestValidator);
				 StartUpBean.initDefaultUser();
				 this.mvc = MockMvcBuilders.standaloneSetup(apptusWebController)
						 .addInterceptors(authInterceptor)
						 .setControllerAdvice(new WEBAPIExceptionHandler())
						 .setCustomArgumentResolvers(webUserResolver)
						 .build();
				 basicAuth = "Basic "+Base64.getEncoder().encodeToString("test:test".getBytes()); 
				 when(appConfig.getACCESS_TOKEN_HEADER()).thenReturn("nx-at");
				 when(appConfig.getREFRESH_TOKEN_HEADER()).thenReturn("nx-rt");
				when(appConfig.getNextoryReqAuthUsername()).thenReturn("test");
				when(appConfig.getNextoryReqAuthPassword()).thenReturn("test11");
				InMemoryCache.isProductionEnv = false;
				 logger.info("Before");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMainApi_failed_basic_auth() throws Exception {
		int statusCode = 403;
		
		when(authService.authenticateBasicAuth(basicAuth, "test", "test11"))
		.thenThrow(new WebApiException(WEBErrorCodeEnum.AUTHENTICATION_FALIED_EXCEPTION,"Basic Authorization Failed !" ));
		
		InMemoryCache.isProductionEnv = true;//to force basic authentication
		RequestBuilder request = MockMvcRequestBuilders
				.get("/api/web/product/6.0/groups")
				.header("locale", "sv_SE")
				.header("Authorization", basicAuth)
				.param("view", "related2")
				.param("pagesize", "5")
				.param("pagenumber", "1")
				.param("bookid", "10061006")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE);
		
		MvcResult mvcResult = mvc.perform(request).andReturn();
		MockHttpServletResponse mResponse = mvcResult.getResponse();
		apiResponse = mapper.readValue(mResponse.getContentAsString(), GenericAPIResponse.class);
		
		 assertNotNull(apiResponse);
		 assertEquals(apiResponse.getStatus(), statusCode);
		 assertNotNull(apiResponse.getError());
		 assertEquals(apiResponse.getError().getCode(), 3000);
	}
	
	@Test
	public void testMainApi_viewmain() throws Exception {
		String locale= "sv_SE",countrycode="SE";
		
		String responseStr = "{\"bookgroups\":[{\"id\":\"tttl_BookTips\",\"type\":\"Top-Tier Top-List\",\"title\":\"Snackisar\",\"haschild\":0,\"position\":0,\"allowsorting\":0},{\"id\":\"tttl_Nyheter\",\"type\":\"Top-Tier Top-List\",\"title\":\"Nyheter\",\"haschild\":0,\"position\":1,\"allowsorting\":0},{\"id\":\"tttl_dynamic_111\",\"type\":\"Top-Tier Top-List\",\"title\":\"Jull\u00E4ngtan\",\"haschild\":0,\"position\":2,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_111/christmas-decor-1-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_111_Large/christmas-decor-1-tablet.jpg\",\"description\":\"\u00C4n k\u00E4nns julen lite l\u00E5ngt borta, men den n\u00E4rmar sig med stormsteg! F\u00F6rbered dig med mysig jull\u00E4sning.\",\"label\":\"Jul\"},{\"id\":\"tttl_dynamic_124\",\"type\":\"Top-Tier Top-List\",\"title\":\"Crossover\",\"haschild\":0,\"position\":3,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_Rec\",\"type\":\"Top-Tier Top-List\",\"title\":\"Rekommenderas f\u00F6r dig\",\"haschild\":0,\"position\":4,\"allowsorting\":0},{\"id\":\"tttl_dynamic_9\",\"type\":\"Top-Tier Top-List\",\"title\":\"Sp\u00E4nning i vinterm\u00F6rkret\",\"haschild\":0,\"position\":5,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_9/excitement-in-the-hammock-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_9_Large/excitement-in-the-hammock-tablet.jpg\",\"description\":\"Pulsh\u00F6jande deckare och iskall sp\u00E4nning. Det spelar inte n\u00E5gon roll vilket v\u00E4der det \u00E4r.\\r\\nDu kommer \u00E4nd\u00E5 bara vilja l\u00E4sa.\",\"label\":\"Sp\u00E4nning\"},{\"id\":\"tttl_TopB\",\"type\":\"Top-Tier Top-List\",\"title\":\"Popul\u00E4ra b\u00F6cker\",\"haschild\":0,\"position\":6,\"allowsorting\":0},{\"id\":\"tttl_dynamic_57\",\"type\":\"Top-Tier Top-List\",\"title\":\"Krinoliner och kronor\",\"haschild\":0,\"position\":7,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_57/crowns-and-croinolines-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_57_Large/crowns-and-croinolines-tablet.jpg\",\"description\":\"Att l\u00E4sa kan vara att dr\u00F6mma sig bort. Kanske till en annan tid med frasiga kjolar och st\u00E5tliga kungar eller vadmal och skyttegravar.\",\"label\":\"Historiska romaner\"},{\"id\":\"tttl_dynamic_146\",\"type\":\"Top-Tier Top-List\",\"title\":\"Historiska deckare!\",\"haschild\":0,\"position\":8,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_115\",\"type\":\"Top-Tier Top-List\",\"title\":\"Kvinnliga f\u00F6rebilder\",\"haschild\":0,\"position\":9,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_68\",\"type\":\"Top-Tier Top-List\",\"title\":\"Engelska nyheter\",\"haschild\":0,\"position\":10,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_130\",\"type\":\"Top-Tier Top-List\",\"title\":\"B\u00F6cker du borde ha l\u00E4st\",\"haschild\":0,\"position\":11,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_123\",\"type\":\"Top-Tier Top-List\",\"title\":\"In the empire business\",\"haschild\":0,\"position\":12,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_60\",\"type\":\"Top-Tier Top-List\",\"title\":\"F\u00F6r dig som har br\u00E5ttom\",\"haschild\":0,\"position\":13,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_60/in-a-hurry-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_60_Large/in-a-hurry-tablet.jpg\",\"description\":\"Timmarna vi tillbringar fram och tillbaka till jobbet kan vara olidligt tr\u00E5kiga. Liva upp resan med en kortis - en bok du slukar p\u00E5 en eller ett par dagars pendling.\",\"label\":\"Kortisar\"},{\"id\":\"tttl_dynamic_110\",\"type\":\"Top-Tier Top-List\",\"title\":\"Lustfyllt\",\"haschild\":0,\"position\":14,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_80\",\"type\":\"Top-Tier Top-List\",\"title\":\"Livs\u00F6den\",\"haschild\":0,\"position\":15,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_76\",\"type\":\"Top-Tier Top-List\",\"title\":\"Leve romantiken!\",\"haschild\":0,\"position\":16,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_76/feelgood-ferris-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_76_Large/feelgood-ferris-tablet.jpg\",\"description\":\"S\u00E4tt guldkant p\u00E5 den gr\u00E5 vardagen med romance.\\r\\nStora k\u00E4nslor, lite trassel, en del pirr men framf\u00F6rallt massor av k\u00E4rlek!\",\"label\":\"Romance\"},{\"id\":\"tttl_dynamic_117\",\"type\":\"Top-Tier Top-List\",\"title\":\"B\u00F6cker om b\u00F6cker\",\"haschild\":0,\"position\":17,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_108\",\"type\":\"Top-Tier Top-List\",\"title\":\"Kritikerrosade deckare 2018\",\"haschild\":0,\"position\":18,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_78\",\"type\":\"Top-Tier Top-List\",\"title\":\"Brottsplatser\",\"haschild\":0,\"position\":19,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_78/hebrides-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_78_Large/hebrides-tablet.jpg\",\"description\":\"B\u00E4sta platsen f\u00F6r en riktigt bra deckare \u00E4r en karg milj\u00F6 - en enslig \u00F6, Australiens inland, de norrl\u00E4ndska v\u00E4garna. Sp\u00E4nningen \u00E4r outh\u00E4rdlig och milj\u00F6n f\u00F6rst\u00E4rker bara den k\u00E4nslan!\",\"label\":\"Sp\u00E4nning\"},{\"id\":\"tttl_dynamic_98\",\"type\":\"Top-Tier Top-List\",\"title\":\"Urban Fantasy\",\"haschild\":0,\"position\":20,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_93\",\"type\":\"Top-Tier Top-List\",\"title\":\"Hitta dig sj\u00E4lv\",\"haschild\":0,\"position\":21,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_56\",\"type\":\"Top-Tier Top-List\",\"title\":\"Ett gr\u00F6nare liv\",\"haschild\":0,\"position\":22,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_56/greener-autumn-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_56_Large/greener-autumn-tablet.jpg\",\"description\":\"Mustiga grytor, kr\u00E4miga grat\u00E4nger och en massa annat gott! V\u00E4lj bort eller minska p\u00E5 k\u00F6ttintaget. H\u00E4r hittar du kokb\u00F6ckerna som hj\u00E4lper dig p\u00E5 traven.\",\"label\":\"Mat\"},{\"id\":\"tttl_dynamic_59\",\"type\":\"Top-Tier Top-List\",\"title\":\"Andra v\u00E4rldskriget\",\"haschild\":0,\"position\":23,\"allowsorting\":0,\"label\":\"Fakta\"},{\"id\":\"tttl_dynamic_87\",\"type\":\"Top-Tier Top-List\",\"title\":\"Late Bloomers!\",\"haschild\":0,\"position\":24,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_87/books-you-should-have-read-phone.png\",\"imagelargeurl\":\"https://storage.googleapis.com/acpt-nest-cover-images/panels/tttl_dynamic_87_Large/books-you-should-have-read-tablet.png\",\"description\":\"Det \u00E4r aldrig f\u00F6r sent f\u00F6r att b\u00F6rja leva!\\r\\nEn del s\u00E4ger att livet b\u00F6rjar vid pensionen. Vi s\u00E4ger att livet b\u00F6rjar exakt n\u00E4r du vill det.\",\"label\":\"Romaner\"},{\"id\":\"tttl_dynamic_67\",\"type\":\"Top-Tier Top-List\",\"title\":\"Film \u00E4r b\u00E4st som bok\",\"haschild\":0,\"position\":25,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_63\",\"type\":\"Top-Tier Top-List\",\"title\":\"Dystopier\",\"haschild\":0,\"position\":26,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_54\",\"type\":\"Top-Tier Top-List\",\"title\":\"Tillbaka till Hogwarts\",\"haschild\":0,\"position\":27,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_54/harry-potter-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_54_Large/harry-potter-tablet.jpg\",\"description\":\"Hoppa p\u00E5 Hogwartsexpressen och njut av J.K. Rowlings magiska v\u00E4rld.\",\"label\":\"Magisk\"},{\"id\":\"tttl_dynamic_29\",\"type\":\"Top-Tier Top-List\",\"title\":\"Allm\u00E4nbildning deluxe\",\"haschild\":0,\"position\":28,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_11\",\"type\":\"Top-Tier Top-List\",\"title\":\"Bara v\u00E4ldigt bra romaner\",\"haschild\":0,\"position\":29,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_25\",\"type\":\"Top-Tier Top-List\",\"title\":\"B\u00F6cker att somna till\",\"haschild\":0,\"position\":30,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_25/books-to-fall-asleep-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_25_Large/books-to-fall-asleep-tablet.jpg\",\"description\":\"Tankar som flyger, jobb som pressar eller bara ren och sk\u00E4r s\u00F6mnl\u00F6shet. H\u00E4r \u00E4r b\u00F6ckerna som hj\u00E4lper dig att somna n\u00E4r f\u00E5ren inte r\u00E4cker till.\",\"label\":\"S\u00F6mn\"},{\"id\":\"tttl_dynamic_15\",\"type\":\"Top-Tier Top-List\",\"title\":\"Seriem\u00F6rdare\",\"haschild\":0,\"position\":31,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_16\",\"type\":\"Top-Tier Top-List\",\"title\":\"M\u00E4nniskans b\u00E4sta v\u00E4n\",\"haschild\":0,\"position\":32,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_96\",\"type\":\"Top-Tier Top-List\",\"title\":\"Medan talmannen jobbar\",\"haschild\":0,\"position\":33,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_26\",\"type\":\"Top-Tier Top-List\",\"title\":\"Bli ditt b\u00E4sta jag\",\"haschild\":0,\"position\":34,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_10\",\"type\":\"Top-Tier Top-List\",\"title\":\"Feelgood f\u00F6r alla\",\"haschild\":0,\"position\":35,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_10/summery-feelgood-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_10_Large/summery-feelgood-tablet.jpg\",\"description\":\"Sorgligt, romantiskt, roligt och definitivt mysigt! H\u00E4r \u00E4r massor av feelgood att sjunka ner i soffan med.\",\"label\":\"MYS\"},{\"id\":\"tttl_dynamic_12\",\"type\":\"Top-Tier Top-List\",\"title\":\"Filosofera i h\u00F6stm\u00F6rkret\",\"haschild\":0,\"position\":36,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_85\",\"type\":\"Top-Tier Top-List\",\"title\":\"Sommarstugemordet\",\"haschild\":0,\"position\":37,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_58\",\"type\":\"Top-Tier Top-List\",\"title\":\"Sekter och sektledare\",\"haschild\":0,\"position\":38,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"tttl_dynamic_27\",\"type\":\"Top-Tier Top-List\",\"title\":\"Vindlande \u00E4ventyr\",\"haschild\":0,\"position\":39,\"allowsorting\":0,\"imageurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_27/promo-roadtrip-phone.jpg\",\"imagelargeurl\":\"https://storage.googleapis.com/nest-cover-images/panels/tttl_dynamic_27_Large/promo-roadtrip-tablet.jpg\",\"description\":\"P\u00E5 bilresan kan det l\u00E4tt bli l\u00E5ngtr\u00E5kigt. Vi har valt ut ett g\u00E4ng sp\u00E4nnande och vindlande \u00E4ventyr som vi tror passar hela familjen.\",\"label\":\"RESA\"},{\"id\":\"tttl_dynamic_161\",\"type\":\"Top-Tier Top-List\",\"title\":\"ajazlabel\",\"haschild\":0,\"position\":40,\"allowsorting\":0,\"label\":\"\"},{\"id\":\"ttc_1\",\"type\":\"Top-Tier Category\",\"title\":\"Deckare\",\"haschild\":1,\"position\":41,\"allowsorting\":1},{\"id\":\"ttc_25\",\"type\":\"Top-Tier Category\",\"title\":\"Sp\u00E4nning\",\"haschild\":1,\"position\":42,\"allowsorting\":1},{\"id\":\"ttc_39\",\"type\":\"Top-Tier Category\",\"title\":\"Barnb\u00F6cker\",\"haschild\":1,\"position\":43,\"allowsorting\":1},{\"id\":\"ttc_79\",\"type\":\"Top-Tier Category\",\"title\":\"Feelgood & Romance\",\"haschild\":1,\"position\":44,\"allowsorting\":1},{\"id\":\"ttc_107\",\"type\":\"Top-Tier Category\",\"title\":\"Biografier & memoarer\",\"haschild\":1,\"position\":45,\"allowsorting\":1},{\"id\":\"ttc_139\",\"type\":\"Top-Tier Category\",\"title\":\"Sk\u00F6nlitteratur\",\"haschild\":1,\"position\":46,\"allowsorting\":1},{\"id\":\"ttc_172\",\"type\":\"Top-Tier Category\",\"title\":\"Ton\u00E5r & Young Adult\",\"haschild\":1,\"position\":47,\"allowsorting\":1},{\"id\":\"ttc_176\",\"type\":\"Top-Tier Category\",\"title\":\"Fantasy & Science Fiction\",\"haschild\":1,\"position\":48,\"allowsorting\":1},{\"id\":\"ttc_200\",\"type\":\"Top-Tier Category\",\"title\":\"Erotik\",\"haschild\":1,\"position\":49,\"allowsorting\":1},{\"id\":\"ttc_204\",\"type\":\"Top-Tier Category\",\"title\":\"Personlig utveckling\",\"haschild\":1,\"position\":50,\"allowsorting\":1},{\"id\":\"ttc_238\",\"type\":\"Top-Tier Category\",\"title\":\"Fakta\",\"haschild\":1,\"position\":51,\"allowsorting\":1},{\"id\":\"ttc_281\",\"type\":\"Top-Tier Category\",\"title\":\"Samh\u00E4lle & politik\",\"haschild\":1,\"position\":52,\"allowsorting\":1},{\"id\":\"ttc_302\",\"type\":\"Top-Tier Category\",\"title\":\"Serier & humor\",\"haschild\":1,\"position\":53,\"allowsorting\":1},{\"id\":\"ttc_308\",\"type\":\"Top-Tier Category\",\"title\":\"Livsstil\",\"haschild\":1,\"position\":54,\"allowsorting\":1},{\"id\":\"ttc_323\",\"type\":\"Top-Tier Category\",\"title\":\"L\u00E4ttl\u00E4st\",\"haschild\":1,\"position\":55,\"allowsorting\":1}]}";
		BookGroupsApptusResponseWrapper bgRespWarapper = mapper.readValue(responseStr,BookGroupsApptusResponseWrapper.class);
		APIJsonWrapper expectedResp = new APIJsonWrapper(bgRespWarapper);
		
		when(authService.authenticateBasicAuth(basicAuth, "test", "test11"))
		.thenThrow(new WebApiException(WEBErrorCodeEnum.AUTHENTICATION_FALIED_EXCEPTION,"Basic Authorization Failed !" ));
		
		when(authService.validateLocale(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any() )).thenReturn(countrycode);
		
		Method method = ApptusWebController.class.getDeclaredMethod("mainApi", WebApiRequestBean.class, 
				User.class, HttpServletRequest.class,Double.class,MainJsonRequestBean.class, BindingResult.class);
		//MethodParameter methodParam = mockMethodParameter(MethodParameter.class);
		MethodParameter methodParam = new MethodParameter(method,1);
		
		when(webUserResolver.supportsParameter(methodParam)).thenReturn(true);
		when(webUserResolver.resolveArgument(Mockito.eq(methodParam), Mockito.any(), Mockito.any(),Mockito.any())).thenReturn(InMemoryCache.defaultUser);
		
	
		when(apptusApiService.getBookGroupsDetails(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(bgRespWarapper);
		RequestBuilder request = MockMvcRequestBuilders
				.get("/api/web/product/6.0/groups")
				.header("locale", "sv_SE")
				.header("Authorization", basicAuth)
				.param("view", "main")
				.param("pagesize", "5")
				.param("pagenumber", "1")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.accept(MediaType.APPLICATION_JSON_UTF8_VALUE);
		
		MvcResult mvcResult = mvc.perform(request).andReturn();
		
		MockHttpServletResponse mResponse = mvcResult.getResponse();
		System.out.println(mResponse.getContentLength());
		System.out.println(mResponse.getContentAsString());
		apiJSONResponse = mapper.readValue(mResponse.getContentAsString(), APIJsonWrapper.class);
		System.out.println(apiJSONResponse.getData());
		/*verify(webUserResolver).supportsParameter(methodParam);
		verify(webUserResolver).resolveArgument(Mockito.eq(methodParam), Mockito.any(), Mockito.any(),Mockito.any());*/
		
		 assertNotNull(apiJSONResponse);
		Map mappedData = (Map) apiJSONResponse.getData();
		 BookGroupsApptusResponseWrapper wrapper = mapper.convertValue(mappedData,BookGroupsApptusResponseWrapper.class);
		 assertNotNull(wrapper);
		 assertTrue(wrapper.getBookGroups().size()>0);
		 assertEquals(expectedResp, apiJSONResponse);
		 
	}

	@Test
	public void testBooksforbookgroup() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testSearchBooks() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testSearchBookswithFacets() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testOnlyFacets() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testAdvancedSearchBooks() {
		fail("Not yet implemented"); // TODO
	}

}

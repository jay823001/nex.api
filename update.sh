export PATH=$PATH:/usr/lib/google-cloud-sdk/bin/
docker build -t gcr.io/plasma-set-92411/nex-product:$1 .
gcloud docker -- push gcr.io/plasma-set-92411/nex-product:$1
kubectl set image deployment/nex-product\
   nex-product=gcr.io/plasma-set-92411/nex-product:$1


==============NEW Profile Table ================
CREATE TABLE profiles (
profileid int not null AUTO_INCREMENT,
parentid int not null,
isparent boolean not null default true, 
profilename varchar(100),
colorindex smallint,
category ENUM('ALL','KIDS') not null,
createddate datetime ,
loginkey varchar(200) not null,
status ENUM('ACTIVE','INACTIVE','DELETED') not null,
constraint uk_loginkey unique key(loginkey),
constraint pk_profileid 
primary key (profileid),
constraint fk_parentid 
foreign key(parentid) 
references customerinfo(customerid) 
on update cascade
on delete cascade
);
================ profiles inserts ==============
insert into profiles(parentid, profilename, colorindex, loginkey,createddate, status) 
   select distinct customerid, IFNULL( NULLIF(firstname,''), IFNULL( NULLIF(lastname,''),email )) 
   , 0,UPPER(MD5(CONCAT(UUID(),'WFNQlLCH', customerid))),  createdon, IF(isactive, 'ACTIVE','INACTIVE') 
   from customerinfo where member_type_code not in (100002, 101001) ;
   
  insert into maxidoftables(tablename, maxid) 
select 'profiles',max(profileid) from profiles ;

================== Alter api_auth_tokens================
ALTER table api_auth_tokens add column  tokentype ENUM('LT','PT') not null default 'PT' after token;
ALTER table api_auth_tokens add column profileid int not null default 0 after user_id;

ALTER TABLE api_auth_tokens DROP PRIMARY KEY,
ADD PRIMARY KEY (user_id, profileid,tokentype,deviceid) ;

==================api_auth_tokens update query================
update IGNORE api_auth_tokens t
INNER JOIN profiles p ON t.user_id=p.parentid
set t.profileid=p.profileid

===============Alter subscription_master==========
ALTER table subscription_master 
	add column ranking TINYINT not null default 1,
	add column is_family_subscription boolean not null default 0,
	add column max_child_profiles int not null default 0,
	add column max_root_login int not null default 3,
	add column max_child_login int not null default 0;
					
====Subscription master alters/updates==========

update subscription_master set subscriptionplanname='SILVER PLUS', subscription_type='BASE', subscriptionprice=99,book_price_limit=46, is_family_subscription=0,max_child_profiles=0,max_root_login=3,max_child_login=0, lastupdatedon=now(),ranking=10 where subscriptionid =3;

update subscription_master set subscriptionplanname='SILVER', subscription_type='BASE', subscriptionprice=119,is_family_subscription=0,max_child_profiles=0,max_root_login=3,max_child_login=0, lastupdatedon=now(),ranking=10 where subscriptionid =5;

update subscription_master set subscriptionplanname='GULD', subscription_type='STANDARD', subscriptionprice=169,is_family_subscription=0,max_child_profiles=0,max_root_login=3,max_child_login=0, lastupdatedon=now(),ranking=20 where subscriptionid =6;

update subscription_master set subscriptionplanname='FAMILY', subscription_type='PREMIUM', subscriptionprice=199,is_family_subscription=1,max_child_profiles=3,max_root_login=1,max_child_login=1, lastupdatedon=now(),ranking=30 where subscriptionid =7;

=====subscription Migrations=============
update customer2subscriptionmap set SUBSCRIPTIONID = 3 where SUBSCRIPTIONID = 5 ;
update customer2subscriptionmap set SUBSCRIPTIONID = 7 where SUBSCRIPTIONID in ( 6,4);

update customer2subscriptionmap set NEXT_SUBSCRIPTIONID = 3 where NEXT_SUBSCRIPTIONID=5;
update customer2subscriptionmap set NEXT_SUBSCRIPTIONID = null ,  SUBSCRIPTIONID  = 7 where NEXT_SUBSCRIPTIONID=6;

********************************************END*************************************
INSERT INTO   test_properties (propkey, value, comments, updatedon, prop_categoryid, datatype) VALUES ('KIDS_CATEGORY_FILTER', 'barnbocker', 'Category for kid\'s releated book\'s', '2017-08-07 22:44:53', '3', 'String');
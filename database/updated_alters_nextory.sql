mysqldump -uroot  -pMyNewPass nextory_new  test_properties >  "/app/tmp/dump/test_properties.sql"

drop database nextory_new;

mysqldump -d -uroot -pMyNewPass nextory_new > "/tmp/mj/nextory.sql"

create schema nextory_new;
--Creating Blank Schema
mysql -uroot -ppassword nextory_new < "D:\DB Dump\final\nextory.sql" 

SET foreign_key_checks = 0;
drop table customerlibrary;
drop table app_library_sync_bookmarks_log;
drop table app_library_sync_highlight_log;
drop table app_library_sync_master_log;
drop table app_library_sync_paging_log;
drop table app_library_sync_setting_log;
drop table downloadtoken;
drop table contributor;
drop table relations;
drop table app_library_track_log;
drop table product;
SET foreign_key_checks = 1;


--Run at source DB
delete from e2go_customer_comments where customerid not in (select customerid from eb_customerinfo);
delete from e2go_customer_comments where customerid not in (select customerid from eb_customerinfo);
delete from eb_giftcard_redeem_log where order_details_id not in (select id from eb_orderdetails);
delete from e2go_customer_library_limit where customerid not in ( select customerid from eb_customerinfo);
delete from eb_customer_temp_password where customerid not in ( select customerid from eb_customerinfo);


update eb_customerlibrary set status ='INACTIVE' where status in ('REFUND','U_DOWNLOAD','U_INACTIVE');
ALTER TABLE `app_library_sync_bookmarks_log` ADD INDEX `blibid` (`lib_id` ASC);
ALTER TABLE `app_library_sync_highlight_log` ADD INDEX `hlibid` (`lib_id` ASC);
ALTER TABLE `app_library_sync_master_log` ADD INDEX `hlibid` (`lib_id` ASC);
ALTER TABLE `app_library_sync_setting_log` ADD INDEX `slibid` (`lib_id` ASC);
ALTER TABLE `eb_customerlibrary` ADD INDEX `formattype` (`formattype` ASC);

delete from eb_customerlibrary where formattype = 5;
DELETE FROM app_library_sync_bookmarks_log WHERE lib_id IN (select id  from eb_customerlibrary where status ='DELETED');
DELETE FROM app_library_sync_bookmarks_log WHERE lib_id IN (select id  from eb_customerlibrary where status ='REMOVED');
delete from eb_giftcard_redeem_log where giftcardid not in (select id from eb_giftcard);
delete from e2go_member_freedays_log where customerid not in (select customerid from eb_customerinfo);
create TEMPORARY TABLE temp select lib_id,count(lib_id) as count from app_library_sync_bookmarks_log group by lib_id having count > 50;

DELETE FROM app_library_sync_bookmarks_log WHERE lib_id IN (SELECT lib_id FROM temp);

DROP TEMPORARY TABLE temp;
-- END


rename table nextory_new.prod_eb_properties to prod_properties;
insert into voucher select * from nextory.eb_voucher;
insert into vatinfo select * from nextory.eb_vatinfo;
insert into userroles select * from nextory.eb_userroles;
insert into search_analytics select * from nextory.search_analytics;
insert into scheduler_log select * from nextory.eb_scheduler_log;
insert into refundtransactionlog select * from nextory.eb_refundtransactionlog;
alter table admin_logged_in_as_user drop primary key;
insert into admin_logged_in_as_user select * from nextory.admin_logged_in_as_user;
insert into api_auth_tokens select * from nextory.api_auth_tokens;
insert into app_banner_details select * from nextory.app_banner_details;
insert into app_device_log select * from nextory.app_device_log;
insert into automatic_mails select * from nextory.nextory_automatic_mails;
insert into category select * from nextory.eb_categories;

insert into campaigncode select * from nextory.eb_campaigncode;
insert into campaign_voucher_master select * from nextory.eb_campaign_voucher_master;
insert into camp_voucher_map select * from nextory.eb_camp_voucher_map;
insert into campaign_channelmaster select * from nextory.eb_campaign_channelmaster;
insert into campaign_voucher_member_type_map select * from nextory.eb_campaign_voucher_member_type_map;
  
insert into subscription_master select * from nextory.eb_subscription_master;
insert into subscription_change_log select * from nextory.eb_subscription_change_log;
insert into customer2subscriptionmap select * from nextory.eb_customer2subscriptionmap;

insert into customerinfo select 
customerid,firstname,lastname,password,salt,ismember,member_type_code,prev_mem_code,intial_mem_code,email,gender,city,land,mobile,
telephonenumber,address1,postcode,isactive,sub_newsletter,createdon,first_app_loggedin ,lastloggedin,updatedby,updatedon,mail_sent_date  
,user_from,referred_by,marketing_src from nextory.eb_customerinfo;


insert into customer_library_limit select * from nextory.e2go_customer_library_limit;
insert into customer_comments select * from nextory.e2go_customer_comments;
insert into customer_temp_password select * from nextory.eb_customer_temp_password;
insert into delete_customer_log select * from nextory.e2go_delete_customer_log;
insert into downloadformat_map select * from nextory.e2go_downloadformat_map;
insert into elib_2go_price select * from nextory.elib_2go_price;
insert into finacial_report_dump select * from nextory.finacial_report_dump;
insert into forgotpassword select * from nextory.eb_forgotpassword;
insert into giftcard select id,giftcardname,price,validity_in_months,subscriptionid,comments,generated_by,createddate,updateddate,isactive 
from nextory.eb_giftcard;
insert into giftcard_pur_details select * from nextory.eb_giftcard_pur_details;
insert into member_freedays_log select * from nextory.e2go_member_freedays_log;
insert into membertype_master select * from nextory.nx_membertype_master;
insert into payment_response_dump  select * from nextory.payex_response_dump;
insert into product_list select * from nextory.eb_product_list;
INSERT INTO product_list_map (map_id,product_list_id,provider_productid,position,adjust_factor,createddate,bookid) select n.*,0 from 
nextory.eb_product_list_map as n;
insert into orders select * from nextory.eb_ordertable;
insert into orderdetails select 
id,orderid,qty,productid,formattype,title,priceoftheproduct,moms,purchaseprice,purchaseprice_without_moms,admin_price_rep,admin_mom_rep,
ctc_price,points_required,isbook,promotionid,elib_order_ref,consumedpoints,purchasedby,iscampaignapplied,isgiftcardapplied,
old_productid,status from nextory.eb_orderdetails;

insert into customer2ccmap select * from nextory.eb_customer2ccmap;


insert into giftcard_redeem_log select * from nextory.eb_giftcard_redeem_log;
ALTER TABLE transaction MODIFY orderid  int(11);
insert into transaction select * from nextory.eb_transaction;
alter table prod_properties modify value varchar(750);
insert into prod_properties select * from nextory.prod_eb_properties;
insert into admin_giftcard_log select * from nextory.eb_admin_giftcard_log;
insert into download_library_log select * from nextory.e2go_download_library_log;
insert into customer_refer_friend select * from nextory.eb_customer_refer_friend;

insert into maxidoftables values ('customerinfo','122405') ;
insert into maxidoftables values ('customer_comments','419') ;
insert into maxidoftables values ('giftcard_pur_details','7400') ;
insert into maxidoftables values ('maxoflibid','538195') ;
insert into maxidoftables values ('orderdetails','372236') ;
insert into maxidoftables values ('orders','366403') ;

update maxidoftables set maxid = (select max(customerid) from nextory.eb_customerinfo) where tablename='customerinfo';
update maxidoftables set maxid = (select max(id) from nextory.e2go_customer_comments) where tablename='customer_comments';
update maxidoftables set maxid = (select max(id) from nextory.eb_giftcard_pur_details) where tablename='giftcard_pur_details';
update maxidoftables set maxid = (select max(id) from nextory.eb_customerlibrary) where tablename='maxoflibid';
update maxidoftables set maxid = (select max(id) from nextory.eb_orderdetails) where tablename='orderdetails';
update maxidoftables set maxid = (select max(orderid) from nextory.eb_ordertable) where tablename='orders';


mysqldump -uroot  -pMyNewPass nextory_new  test_properties >  "/tmp/mj/test_properties.sql"
//take latest table insert scripts from accpt server
INSERT INTO `test_properties` VALUES (1,'elibdataupdatefrom','2016-09-01 03:00:00','used for data import from elib1','2016-08-09 14:06:40',1,'date'),(2,'PAYNOVA_NOTIFY_URL','http://130.211.74.42/payex/notify','URL on which Paynova notifies','2015-09-07 10:25:30',3,'string'),(3,'PAYNOVA_REDIRECT_OK_URL','http://130.211.74.42/payex/notify?','URL to which Paynova redirects on success','2015-09-07 10:25:50',3,'string'),(4,'PAYNOVA_REDIRECT_CANCEL_URL','http://130.211.74.42/payex/cancel?','Url to which paynova redirects on failure','2015-09-07 10:26:03',3,'string'),(5,'PER_POINT_PRICE','49','Per point price ... ','2015-05-19 07:51:10',0,'int'),(6,'LIMIT_TO_SHOW_PRICE','50','price limit to show the price for member','2015-05-27 11:46:59',1,'int'),(7,'PAYNOVA_CUSTOMERID','640799','paynova customer id','2011-01-01 00:00:00',3,'int'),(8,'PAYNOVA_SECRETKEY','J2P2ChF2vTTP77FD','paynova secret key','2011-01-01 00:00:00',3,'string'),(9,'PDF med Adobe-kryptering','hjalp/format/pdf-med-adobe-kryptering','','2011-04-18 11:03:25',4,'string'),(10,'Mobipocket','hjalp/format/mobipocket','','2011-04-18 11:03:25',4,'string'),(11,'PDF med vattenmÃƒÂ¤rke','hjalp/format/pdf-med-vattenmarke','','2011-04-18 11:03:25',4,'string'),(12,'EPUB med Adobe-kryptering','hjalp/format/epub-med-adobe-kryptering','','2011-04-18 11:03:25',4,'string'),(13,'EPUB med vattenmÃƒÂ¤rke','hjalp/format/epub-med-vattenmarke','','2011-04-18 11:03:25',4,'string'),(14,'Ljudbok, MP3','hjalp/format/ljudbok-mp3','','2015-06-09 14:30:44',4,'string'),(15,'Ljudbok, iPod (AAC)','hjalp/format/ljudbok-ipod-aac','','2011-04-18 11:03:25',4,'string'),(16,'Ljudbok, MP3 (5-minutersspÃƒÂ¥r)','hjalp/format/ljudbok-mp3-5-minutersspar','','2011-04-18 11:03:25',4,'string'),(17,'Ljudbok,P3 (en fil)','hjalp/format/ljudbok-mp3-en-fil','','2011-04-18 11:03:25',4,'string'),(18,'Ljudbok, iPod (en AAC-fil)','hjalp/format/ljudbok-ipod-en-aac-fil','','2011-04-18 11:03:25',4,'string'),(19,'PAYMENT_OPTION_SLUG','betalsatt-new','Holds the name of the FAQ in the menu','2011-04-10 00:00:00',4,'string'),(20,'SECURITY_SLUG','sakerhet-new','Holds the name of the FAQ in the menu','2011-04-10 00:00:00',4,'string'),(21,'FORGOT_PASSWORD_LINK','http://130.211.74.42/konto/andra-losenord?id={0}&authkey={1}','Url lik for forgot password rest','2015-09-01 05:24:17',1,'string'),(22,'ELIB_PRICE_LIMIT','128','Max price of the elib product to be imported1','2015-09-28 10:58:16',2,'int'),(23,'IS_RE_MASS_INDEXING_REQUIRED','false','PriceCategory reindexing ','2015-05-27 12:24:25',1,'boolean'),(24,'RETAILER_ID','949','Elib Service id','2011-06-13 16:11:00',1,'int'),(25,'ACTIVATION_KEY','ljudboksgruppen','Activation key','2011-06-13 16:11:01',1,'string'),(26,'KEYCODE','jd3Rh57h!h','keycode','2011-06-13 16:11:04',1,'string'),(27,'AUDIOBOOK_DOWNLOAD_FORMAT_ID','300','Audio book default download format','2011-06-17 11:33:12',1,'int'),(28,'PAYNOVA_LANG','SWE','language to be sent to paynova','2011-06-17 11:33:20',3,'string'),(29,'PAYNOVA_THEME','Ljudbok','theme to be sent to paynova','2011-06-17 11:33:21',3,'string'),(30,'BOOK2GO_URL','http://localhost:8080','book2go application url','2016-03-11 09:49:27',1,'string'),(31,'BOOK2GO_CONTEXT_URL','http://localhost:8080/','book2go application context url','2016-02-15 07:26:48',1,'string'),(32,'ELIB_IMAGES_DOWNLOAD_LOCATION','/app/nextory/www-data/e2go_img_downloader/images/','Images to download from Elib','2015-04-24 10:27:28',0,'string'),(33,'DISC_TRANS_LAST_RUN_ON','2011-11-17 05:27:59','Discount transactions run date','2011-01-01 00:00:00',2,'date'),(34,'ELIB_FEE_UPDATE_LAST_RUN','2012-12-11 03:56:52','ELIBFee modification ran on','2011-01-01 00:00:00',2,'date'),(35,'DISCOUNT_UPDATE_LAST_RUN','2011-01-01','Discount update ran on','2011-01-01 00:00:00',2,'date'),(36,'BASE_IMAGE_URL','https://104.155.59.96/nextory/','URLlocation for the images','2015-09-18 10:30:45',0,'string'),(37,'AUDIOBOOK_STREAMING_FORMAT_ID','31','Audio book for streaming format','2011-01-01 00:00:00',1,'int'),(38,'GIFTCARD_LOCATION','/app/nextory/app-data/attachments/giftcard','giftcard pdf location','2015-06-19 11:08:25',0,'string'),(39,'DEVELOPMENT_MODE','false','Is the app running on Dev mode ? No orders to the Elib and Qiozk','2012-01-01 00:00:00',1,'boolean'),(40,'SERVER_URL','http://104.199.22.40/','Server url for external systems','2016-02-15 07:27:28',1,'string'),(41,'EBOOK_FORMAT_PRIORITY','105,102,101','Priority of the Ebooks to be allowed for download.','2012-01-01 00:00:00',2,'string'),(42,'CUSTOMER_SERVICE_MAIL','undservice@e2go.se','QUERY SEND TO THIS MAIL BY CUSTOMER','2015-06-17 14:29:13',4,'string'),(43,'GULD_ID','2','Id in the database for Gold, Donot edit this','2015-05-27 12:29:42',1,'int'),(44,'SILVER_ID','3','Id in the database for Silver,Donot Edit this','2012-01-01 00:00:00',1,'int'),(45,'REPORT_STORAGE_LOCATION','/app/nextory/app-data/reports','store e2go reports','2015-04-24 10:28:54',0,'string'),(46,'DOWNLOAD_IPHONE_APP_URL','http://itunes.apple.com/in/app/e2go/id515270408?mt=8','download iphone app url','2012-01-01 00:00:00',4,'string'),(47,'SYSTEM_ADMIN_MAIL','rammohan.gudditi@gmail.com','internal error msg send to system admin','2015-04-24 10:38:59',0,'string'),(48,'EBOOK_DOWNLOAD_FORMAT','100','elib ebook download format','2014-12-06 17:41:02',1,'int'),(49,'SERVICE_KEY','Axcv01OsBDJmeUz','elib service key1','2015-08-04 14:36:32',1,'string'),(50,'PAYNOVA_URL','https://testpaygate.paynova.com/mpi/paynova.mpi?version=1','URL for payment gate site','2012-01-01 00:00:00',3,'string'),(51,'KLARNA_ID','18809','Klarna Eid','2012-01-01 00:00:00',3,'int'),(52,'KLARNA_KEY','nIv8BUnTG6teXbT','Klarna Key','2012-01-01 00:00:00',3,'string'),(53,'KLARNA_MODE','LIVE','KLARNA Mode','2012-01-01 00:00:00',3,'string'),(54,'QIOZK_RUN_MODE','true','test purchase for qiozk','2012-01-01 00:00:00',3,'boolean'),(55,'AUDIOBOOK_DWONLOAD_FORMAT_PRIORITY','301,302,303,306,401','audiobook download formats sperate by comma','2012-01-01 00:00:00',3,'string'),(56,'PAYNOVA_MIN_AMOUNT_FOR_RESERVE','1','Min amount for reserve Message','2012-01-01 00:00:00',3,'float'),(57,'PAYNOVA_REDIRECT_APP_OK_URL','http://104.155.59.96/nextory/bank_resp_app?','Response page from bank for App','2015-04-28 12:10:02',0,'string'),(58,'PAYNOVA_REDIRECT_APP_CANCEL_URL','http://104.155.59.96/nextory/bank_resp_app/cancel?','Response page from bank for App','2015-04-28 12:10:12',0,'string'),(59,'FACEBOOK_APP_ID','250693735072504','Face book id for e2go.se','2012-01-01 00:00:00',1,'int'),(60,'BANNER_STORE_LOCATION','/var/e2go/e2go/vardebanner/','banner storage location','2012-01-01 00:00:00',4,'string'),(61,'BANNER_PREFIX_URL','http://87.237.215.211/banner/','Banner Prefix Url','2013-07-19 07:18:18',4,'string'),(62,'UNLIMITED_ID','4','unlimited subscrption id','2013-10-01 00:00:00',1,'int'),(63,'TEST_CUSTOMER_ID','4','TEST_CUSTOMER_ID','2013-10-01 00:00:00',1,'int'),(64,'NEW_PRODUCT_SLUG_NAME','nyheter','new products for email','2013-10-01 00:00:00',1,'string'),(65,'INDEX_URL','http://87.237.215.211:30080/e2go/indexing','index url','2013-10-01 00:00:00',1,'string'),(66,'TOP_EBOOKS_SLUG_NAME','topplista-e-bocker','top ebooks slug name','2013-10-01 00:00:00',1,'string'),(67,'TOP_AUDIOBOOKS_SLUG_NAME','topplista-ljudbocker','top audio book slug name','2013-10-01 00:00:00',1,'string'),(68,'FEATURED_BOOKS_SLUG_NAME','vi-rekommenderar','featured books slug name','2013-10-01 00:00:00',1,'string'),(69,'FEATURED_AUDIO_BOOKS_SLUG_NAME','vi-rekommenderar-audio','featured books slug name','2013-10-01 00:00:00',1,'string'),(70,'PHP_IMAGE_RESIZE_URL','/coverimg','php image url prefix','2015-07-15 07:50:48',0,'string'),(71,'RECRUIT_FRIEND_FREE_DAYS','20','Free 14 daysTrail for Friend','2015-07-31 07:41:10',1,'int'),(72,'REFERER_FREE_DAYS_AFTER_PAYMENT','6','Refer wiil get 7 day free if friend subscribe','2015-06-23 07:08:56',1,'int'),(73,'TRAIL_PERIOD_MEMBER','14','trail period for first time','2016-02-15 08:09:37',1,'int'),(74,'ENVIRONMENT_SMG','TestÂ Server','e2go environment','2015-06-10 14:41:52',1,'string'),(75,'GIFTCARD_REDEEM_FREE_DAYS','7','Free Days when the user redeems the giftcard','2014-04-01 00:00:00',1,'int'),(76,'NO_OF_UNLIMITED_DOWNLOADS_ALERT','5','threshold for alerting the admin about the user downloads','2015-05-19 08:02:28',0,'int'),(77,'SITE_MAP_TEMP_LOCATION','/app/nextory/app-data/sitemap-temp','temp location for storing the sitemap','2015-04-24 10:33:09',0,'String'),(78,'SITE_MAP_DESTINATION','/app/nextory/www-data/wordpress','destination for storing the sitemap','2015-04-24 10:33:35',0,'String'),(79,'CRASH_LOG_DUMP','/app/nextory/logs/','location for storing the crash logs','2015-05-28 06:35:11',3,'String'),(80,'PAYEX_ENV','TEST','payex enviroment\nTEST/PROD ','2014-06-18 13:58:35',3,'String'),(81,'PAYEX_ACCID','60023085','payex AccountID ','2014-06-18 13:58:36',3,'String'),(82,'PAYEX_ENCRY','79Z7n993uvF2BDp275mW','payex Encryption ','2014-06-18 13:58:37',3,'String'),(83,'EXCEL_EXPORT_LOCATION','/app/nextory/app-data/excel','excel storeLocation ','2015-04-24 10:35:47',0,'String'),(84,'UNLIMITED_BOOKS_DAILY_DOWNLOAD_LIMIT','1','one day max books download limit ','2016-07-20 13:36:59',3,'int'),(85,'UNLIMITED_BOOKS_MONTHLY_DOWNLOAD_LIMIT','2','30 days max books download limit','2016-07-20 13:37:15',3,'int'),(86,'EMAIL_TEMPLATE_BANNER_PREFIX_URL','http://87.237.215.211/banner/new_books.jpg','banner update for email templates','2014-10-22 10:29:00',3,'String'),(87,'REPORT_TEMPLATE_LOCATION','/app/nextory/app-data/excel/templates/','report templates','2015-04-24 10:36:07',0,'String'),(88,'IMAGE_LOADER_URL','http://localhost:8090/img-download/downloadimage.php','internal service to download images','2015-08-11 11:54:04',1,'string'),(89,'NEST_APP_ID','150824','NEERAJ','2015-05-19 07:52:45',0,'int'),(90,'NEST_SECRET_KEY','g2h5LL6XHm','TESTING','2015-05-19 07:51:44',0,'string'),(91,'NEST_URL','http://10.240.0.5:8082/nestver2/api/ver1/order/createorder','','2015-04-29 16:38:57',0,'string'),(92,'NEST_SYNCH_URL','http://10.240.0.5:8082/nestver2/api/ver1/store/productdetails','','2015-04-24 10:32:05',0,'string'),(93,'ELIB_IMPORT_LOC','/app/nextory/app-data/elib_dump/','location to import xml file','2015-04-24 10:23:13',0,'String'),(94,'BOCKER_PAGE_BOOK_COUNT','36','BOCKER_PAGE_BOOK_COUNT','2015-05-08 10:40:10',0,'int'),(95,'NEST_PROD_STATUS_SYNC_URL','http://10.240.0.5:8082/nestver2/api/ver1/sync/productstatus','','2015-06-05 08:29:41',0,'string'),(96,'SWEDISH_CHARACTERS',' Ã¡Ã Ã¢Ã¥Ã£Ã¤Ã¦?Ã§?Ã©Ã¨ÃªÃ«?Ã­Ã¬Ã®Ã¯?Ã±?Ã³Ã²Ã´Ã¸ÃµÃ¶?ÃŸÃºÃ¹Ã»Ã¼Ã¿???/,','swedish characters','2015-06-10 15:28:53',1,'string'),(97,'JOB_DASHBOARD_SERVER_URL','http://frescano-monitoring.appspot.com/api/notify/','dashboard URL','2015-06-24 06:52:50',1,'String'),(98,'BATCH_URL','http://10.240.0.5:8084/nextory.batch/','Batch URL','2015-07-10 10:25:52',1,'String'),(99,'BLOCK_ORDERS','true','if value is true it will block recurssive payments and cacelled members cron','2015-08-06 08:45:40',1,'boolean'),(100,'NEST_PRICE_LIMIT','0','','2015-09-02 11:24:17',1,'int'),(101,'ALL_EMAIL_LOCATION','/app/nextory/app-data/email-templates','storage of all emails','2015-11-17 00:00:00',0,'String'),(102,'REPORT_SENDS_USER_MAILS','rammohan.gudditi@gmail.com,vijaya.souri@frescano.se','reports send user mails','2015-11-17 00:00:00',0,'String'),(103,'NEXTORY_SYSTEM_PROPERTY_URL','http://localhost:8080/systemproperty','UPDATE INMEMORY CACHE DATA IN CLIENT SERVER','2015-11-17 00:00:00',0,'String'),(104,'APP_CONCURRENT_USERS_COUNT','3','','2016-02-29 00:00:00',0,'int'),(105,'NEST_BOOK_DOWNLOAD_URL','http://10.240.0.5:8082/nestver2/api/ver1/publisher/download','URL for new publisher contracts','2016-04-21 11:20:09',1,'string'),(106,'DEPLOY_LOCATION','ACCEPTANCE',NULL,'2016-05-03 08:12:01',0,'String'),(107,'ADYEN_USER','ws@Company.FrescanoGroupAB','User Id for Adyen Integration','2016-06-28 15:16:13',3,'String'),(108,'ADYEN_PASSWORD','FRESCANOPASSWORD12345','Password for Adyen Integration','2016-06-28 15:16:13',3,'String'),(109,'MERCHANT_ADYEN','FrescanoGroupABSE','Merchant Name for Adyen Integration','2016-06-28 15:16:13',3,'String'),(110,'ADYEN_URL','https://pal-test.adyen.com/pal/adapter/httppost','URL for Adyen Payment Gateway','2016-06-28 15:16:14',3,'String'),(111,'ADYEN_CAPTURE_URL','https://pal-test.adyen.com/pal/servlet/Payment/v10/capture','CAPTURE URL for Adyen Payment Gateway','2016-06-28 15:16:14',3,'String'),(112,'ADYEN_RECURRING_URL','https://pal-test.adyen.com/pal/servlet/Recurring/v10/listRecurringDetails','RECURRING URL for Adyen Payment Gateway','2016-06-28 15:16:14',3,'String'),(113,'ADYEN_CANCEL_URL','https://pal-test.adyen.com/pal/servlet/Payment/v10/cancel','CANCEL URL for Adyen Payment Gateway','2016-06-28 15:16:14',3,'String'),(114,'ADYEN_REFUND','https://pal-test.adyen.com/pal/servlet/Payment/v10/refund','URL for Adyen Payment Gateway','2016-06-28 15:16:14',3,'String'),(115,'MAIL_SENDER_NAME','kundservice@nextory.se','from whom the mail is coming','2016-07-01 11:08:45',0,'String'),(116,'ADYEN_CSE_KEY','10001|8E8A9925D9CD9C1E3D3508AF09A58163A722DE04DEAD0CFCB145B765ED9C719132D99421FA08C476D0E1033A730FD9211CC36CDB4355226EF228E65C30C9EA22C5EC1183B006B221A6704559E2F9B02BFF2BDB7E7278BD778EE28A7E870D51E5CCF473002B06A5AFC250AC1CA4911784E7FED2A65B17098388E1404C6F73268317A04A0251F6D1D75D2B3455F112CE92364432AAC460342CEDEB375CAE7ABFE5A66ED9157C3D2724B116DAC8F39ED1EDE20758447512633E2C39753EFBBEBB647C857600B1938260833A0053E5FE9297867DA8F5B84D4C20694AE61A7F3CFB3417D09D4C56BB31F6E2E2E8A4C64A605BE0EE60B9ADF9E1CDED22C931000D72B1','CSE Token for Adyen Payment','2016-07-07 12:15:17',3,'String'),(117,'BUSINESS_ADMIN_MAIL','rammohan.gudditi@gmail.com','Business notification emails to Business Admin','2016-07-20 12:33:24',0,'string'),(118,'NEST_PRODUCT_URL','http://10.240.0.5:8082/nestver2/app/ver2/fetch/product?fromdate=','Product Status sync with Nest to Nextory','2016-07-29 12:36:09',0,'String'),(119,'PRODUCT_UPDATE_DATE','2016-08-12 13:05:02','Date to run Job to sync with Nest to Nextory','2016-07-29 12:36:21',0,'String'),(120,'ORDER_INTERVAL_FOR_POPULARITY','17','','2016-09-20 15:44:29',1,'int'),(121,'IMG_AS_PRODUCTID','true','determines if provider_productid or bookid to be used for image url','2016-10-07 12:20:40',1,'boolean'),(122,'MIXPANEL_TOKEN','7db5c263c86c1b649c83545f03334fad','Toekn to Integrate Mixpanel','2016-10-14 13:12:08',3,'String');

update test_properties set value ='http://104.199.22.40/'  where propkey ='BOOK2GO_CONTEXT_URL';
update test_properties set value = 'http://104.199.22.40/img-download/downloadimage.php' where propkey ='IMAGE_LOADER_URL';
update test_properties set value = 'http://104.199.22.40/systempropery' where propkey ='NEXTORY_SYSTEM_PROPERTY_URL';

update prod_properties set value ='https://www.nextory.se/'  where propkey ='BOOK2GO_CONTEXT_URL';
update prod_properties set value = 'http://localhost:8090/img-download/downloadimage.php' where propkey ='IMAGE_LOADER_URL';
update prod_properties set value = 'http://127.0.0.1:9090/systemproperty' where propkey ='NEXTORY_SYSTEM_PROPERTY_URL';

  insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(173,'ZENDESK_DATE_FORMAT','yyyy-MM-dd HH:mm a','Zendesk Date format',now(),3,"String");

  insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(174,'ZENDESK_USER_NAME','admin@frescano.se','Zendesk User Name',now(),3,"String");

insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(175,'ZENDESK_PWD','d3^QzB%dUReN','Zendesk Password',now(),3,"String");

insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(176,'ZENDESK_SCHEDULE_URL','https://nextory.zendesk.com/api/v2/business_hours/schedules.json','Zendesk Schedule URL',now(),3,"String");

insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(177,'ZENDESK_HOLIDAY_URL','https://nextory.zendesk.com/api/v2/business_hours/schedules/{id}/holidays.json','Zendesk Holiday URL',now(),3,"String");


--Prod Tables
  insert into prod_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(173,'ZENDESK_DATE_FORMAT','yyyy-MM-dd HH:mm a','Zendesk Date format',now(),3,"String");

  insert into prod_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(174,'ZENDESK_USER_NAME','admin@frescano.se','Zendesk User Name',now(),3,"String");

insert into prod_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(175,'ZENDESK_PWD','d3^QzB%dUReN','Zendesk Password',now(),3,"String");

insert into prod_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(176,'ZENDESK_SCHEDULE_URL','https://nextory.zendesk.com/api/v2/business_hours/schedules.json','Zendesk Schedule URL',now(),3,"String");

insert into prod_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(177,'ZENDESK_HOLIDAY_URL','https://nextory.zendesk.com/api/v2/business_hours/schedules/{id}/holidays.json','Zendesk Holiday URL',now(),3,"String");


INSERT INTO test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) 
values
(178,"TOPLIST_COUNT_READ_DAYS", 14 ,"Top read count days",now(), 0, "String");

INSERT INTO prod_eb_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) 
values
(178,"TOPLIST_COUNT_READ_DAYS", 14 ,"Top read count days",now(), 0, "String");


----------------------------------------Above keys Already in Production -------------------------------------------------------------


 insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(180,'RULEMAILER_KEY','12c60e86-cab6a6e-5f457ca-be5cf02-f36122a8564','Token to Integrate RuleMailer',now(),3,"String");

insert into prod_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values
('RULEMAILER_KEY','12c60e86-cab6a6e-5f457ca-be5cf02-f36122a8564','Token to Integrate RuleMailer',now(),3,"String");

-----------------21-11-2016 (LakshmiNarayana)--------
db.toplist.update({},{$set : {"showindevice":"ALL"}},false,true);
db.toplist.update({"_id":22},{$set : {"showindevice":"WEB"}});
db.toplist.update({"_id":23},{$set : {"showindevice":"WEB"}});

----------------12-12-2016 (LakshmiNarayana)for NEX-2083.........
INSERT INTO `nextory_new`.`test_properties` (`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('P12_FILE_LOC', 'frescano-cloud-project-27d48456fe09.p12', 'Goolge cloude store certificate', '2016-12-12 13:51:38', '3', 'String');
ALTER TABLE `customer_campaign_activation_details`
	CHANGE COLUMN `subscriptiontype` `subscriptiontype` VARCHAR(50) NOT NULL AFTER `voucher_code`;
ALTER TABLE `customer_campaign_activation_details`
	DROP FOREIGN KEY `FK__customerinfo`;
ALTER TABLE `customer_campaign_activation_details`
	ADD COLUMN `identifier_id` VARCHAR(50) NOT NULL AFTER `createdby`;
------------------13-Feb-2017 (Harish) for NEX-2389-----------------
INSERT INTO `nextory_new`.`test_properties` (`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('RULEMAILER_CRITICAL_KEY', '8715c454-9cc1f5c-ed1f63c-82f409d-b38f136b273', 'Separate rulemailer API-key for critical emails', now(), '3', 'String');

---------------15-02-2017(LakshmiNarayana) for NEX-NEX-2407 -------------------------------
ALTER TABLE `customerinfo`
	ADD COLUMN `educate_mailsent` VARCHAR(500) NULL DEFAULT '' AFTER `is90perc_mailsent`;

Update customerinfo SET educate_mailsent='Education 1| Education 2 | Education 3| Education 4| Education 5|' Where (prev_mem_code > 400000 OR ( prev_mem_code > 100000 AND prev_mem_code < 200000))
AND member_type_code in (203002, 201002, 202002, 201007,304001, 302012, 302013) AND (updatedon >= Date('2016-12-15') AND updatedon <= DATE_SUB(NOW(), INTERVAL 2 DAY));

UPDATE `nextory_new`.`trigger_method_mapper` SET `params`='TriggermailSubject, WinbackCampaignVoucher' WHERE  `triggername`='RULEMAILER_MEMBER_CHURN_WINBACK_OFFER';



----------- 21-03-2017(NEX-2541 fix)  ---------------------------
 alter table collabs modify link varchar(1000);
 
 -----------24-03-2017(NEX-2544)------------------------
 ALTER TABLE `customerinfo`
	ADD COLUMN `tm_dailup_action` ENUM('ACCEPTED_OFFER','NO_THANKS_FOR_OFFER','DO_NOT_DISTURB') NULL DEFAULT NULL AFTER `mailsent_once`,
	ADD COLUMN `tm_dailup_action_date` DATETIME NULL DEFAULT NULL AFTER `tm_dailup_action`;
	
insert into nextory_new.test_properties (id,propkey,value,comments,updatedon,prop_categoryid,
datatype) values
(193,'NEST_SUBSCRIPTIONTYPE_URL','http://130.211.74.42:8083/nestver2/app/ver2/fetch/subscriptiontype',
'Fetch Subscription types',now(),3,"String");

--------------13-07-2017(NEX-2951)--------------
ALTER TABLE `download_library_log`
	CHANGE COLUMN `log_id` `log_id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	ADD PRIMARY KEY (`log_id`);
	
CREATE TABLE IF NOT EXISTS `customer_cancel_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cancel_reason` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table nextory_new.customer_cancel_reasons: ~6 rows (approximately)
/*!40000 ALTER TABLE `customer_cancel_reasons` DISABLE KEYS */;
INSERT INTO `customer_cancel_reasons` (`id`, `cancel_reason`, `created_date`, `updated_date`) VALUES
	(1, 'Jag har inte tid att utnyttja tjänsten', '2017-07-20 18:33:44', '2017-07-20 18:33:45'),
	(2, 'Tjänsten är inte tillräckligt prisvärd', '2017-07-20 18:34:10', '2017-07-20 18:34:11'),
	(3, 'Jag hittar inte de böcker jag vill läsa', '2017-07-20 18:34:25', '2017-07-20 18:34:26'),
	(4, 'Appen har inte fungerat som den ska', '2017-07-20 18:34:38', '2017-07-20 18:34:38'),
	(5, 'Jag har valt en annan tjänst', '2017-07-20 18:34:49', '2017-07-20 18:34:50'),
	(6, 'Annat', '2017-07-20 18:35:09', '2017-07-20 18:35:09');
	
CREATE TABLE IF NOT EXISTS `winback_campaign_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_cancel_reason` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `winback_offer` varchar(50) NOT NULL,
  `mail_tigger_name` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  KEY `id` (`id`),
  KEY `FK_winback_campaign_map_customer_cancel_reasons` (`customer_cancel_reason`),
  CONSTRAINT `FK_winback_campaign_map_customer_cancel_reasons` FOREIGN KEY (`customer_cancel_reason`) REFERENCES `customer_cancel_reasons` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

---- Added by Ram on 02-08-2017
ALTER TABLE customerinfo  ADD COLUMN user_country varchar(4) null DEFAULT 'SE' AFTER tm_dailup_action_date ;

	-----Added by Harish : Aug 30th 2017 for getting the categories from NEST URL at startup-------------
	INSERT INTO test_properties (propkey, value, comments, updatedon, prop_categoryid, datatype) 
VALUES ('NEST_BASE_URL', 'http://104.199.254.5/nest', 'NEST Base URL for all NEST references', NOW(), '3', 'String');

delete from test_properties where propkey='KIDS_CATEGORY_FILTER';

CREATE TABLE IF NOT EXISTS `cancelled_customer_offer_map` (
  `customerid` int(11) NOT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `updatedon` datetime NOT NULL,
  PRIMARY KEY (`customerid`),
  CONSTRAINT `FK__customerinfo` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `nextory_new`.`test_properties` (`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('TM_OFFER_FIXED_CAMPAIGN_VOUCHER', 'FPC2017', 'Fixed campaign applicable for cancelled member\'s', '2017-09-15 18:34:08', '3', 'String');
INSERT INTO `nextory_new`.`test_properties` (`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('TM_OFFER_DISCOUNT_CAMPAIGN_VOUCHER', 'DPC2017', 'Discount campaign applicable for cancelled member\'s', '2017-09-15 18:34:08', '3', 'String');

ALTER TABLE `customer_comments` DROP PRIMARY KEY;
/*
INSERT INTO `winback_campaign_map` (`id`, `customer_cancel_reason`, `age`, `winback_offer`, `mail_tigger_name`, `created_date`, `updated_date`, `created_by`, `updated_by`) VALUES
	(1, 4, 30, 'winback50', 'Did not work #1: Post-churn', '2017-07-20 16:21:49', '2017-07-20 16:21:50', 7700, 7700),
	(2, 4, 45, 'winback9kr', 'Did not work #2: Post-churn', '2017-07-20 16:22:52', '2017-07-20 16:22:53', 7700, 7700),
	(3, 4, 75, 'winback9kr', 'Did not work #3: Post-churn', '2017-07-20 16:23:39', '2017-07-20 16:23:40', 7700, 7700),
	(4, 1, 14, 'winback50', 'No time #1: Post-churn', '2017-07-20 16:25:16', '2017-07-20 16:25:17', 7700, 7700),
	(5, 1, 30, 'winback9kr', 'No time #2: Post-churn', '2017-07-20 16:25:53', '2017-07-20 16:25:54', 7700, 7700),
	(6, 1, 60, 'winback9kr', 'No time #3: Post-churn', '2017-07-20 17:28:50', '2017-07-20 17:28:51', 7700, 7700),
	(7, 3, 30, 'winback50', 'Can\'t find books #1: Post-churn', '2017-07-20 17:30:52', '2017-07-20 17:30:55', 7700, 7700),
	(8, 3, 45, 'winback9kr', 'Can\'t find books #2: Post-churn', '2017-07-20 17:31:52', '2017-07-20 17:31:53', 7700, 7700),
	(9, 3, 75, 'winback9kr', 'Can\'t find books #3: Post-churn', '2017-07-20 17:32:46', '2017-07-20 17:32:47', 7700, 7700),
	(10, 2, 14, 'winback50', 'Not good value #1: Post-churn', '2017-07-20 17:33:51', '2017-07-20 17:33:52', 7700, 7700),
	(11, 2, 30, 'winback9kr', 'Not good value #2: Post-churn', '2017-07-20 17:36:06', '2017-07-20 17:36:07', 7700, 7700),
	(12, 2, 60, 'winback9kr', 'Not good value #3: Post-churn', '2017-07-20 17:36:48', '2017-07-20 17:36:48', 7700, 7700),
	(13, 5, 30, 'winback50', 'Other provider #1: Post-churn', '2017-07-20 18:10:18', '2017-07-20 18:10:18', 7700, 7700),
	(14, 5, 45, 'winback9kr', 'Other provider #2: Post-churn', '2017-07-20 18:11:07', '2017-07-20 18:11:08', 7700, 7700),
	(15, 5, 75, 'winback9kr', 'Other provider #3: Post-churn', '2017-07-20 18:13:38', '2017-07-20 18:13:38', 7700, 7700),
	(16, 6, 30, 'winback50', 'Other #1: Post-churn', '2017-07-20 18:14:52', '2017-07-20 18:14:52', 7700, 7700),
	(17, 6, 45, 'winback9kr', 'Other #2: Post-churn', '2017-07-20 18:15:23', '2017-07-20 18:15:23', 7700, 7700),
	(18, 6, 75, 'winback9kr', 'Other #3: Post-churn', '2017-07-20 18:15:44', '2017-07-20 18:15:45', 7700, 7700); */
	
-----Added by Harish : Sept 21st 2017 for 10 year 10% campaign-------------
alter table campaign_voucher_master add column dynamic_type boolean not null default 0;
alter table campaign_voucher_master add column voucher_pattern varchar(50);
-----Added by Ram : 11th October 2017 for 10 year 10% campaign-------------
alter table campaign_voucher_master add column trial_days int(11);
update campaign_voucher_master set trial_days=14 where camp_id=862 and camp_name='Hyresgastforeningen';
-------Added by Ram : Auto-login on click of   upgrade link from App-------------
CREATE TABLE `web_access_tokens` (
  `token` varchar(200) NOT NULL,
  `customerid` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  UNIQUE KEY `WAT_UK_customerinfo` (`customerid`),
  CONSTRAINT `WAT_FK_customerinfo` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
---------By Harish on Oct 26th 2017 for NEX-3258 :: Custom error messages for session expired tokens--------
ALTER TABLE api_auth_tokens ADD COLUMN pwd_updateddate DATETIME ;

---------- Signup and Visitor onboarding in app --------------------------------------------------------------

ALTER TABLE api_auth_tokens ADD COLUMN account_type varchar(45);

 SET SQL_SAFE_UPDATES = 0;
 // For Existing Visitor users
 update api_auth_tokens set account_type='VISITOR' where dirty_flag = 0 and user_id in 
 (select customerid from customerinfo where member_type_code < 200000 );
 
 // For Existing Member users
  update api_auth_tokens set account_type='MEMBER' where dirty_flag = 0 and user_id in 
 (select customerid from customerinfo where member_type_code > 200000 and member_type_code < 400000 );
 
 // For Existing NON_MEMBER users
  update api_auth_tokens set account_type='NON_MEMBER' where dirty_flag = 1 and user_id in 
 (select customerid from customerinfo where member_type_code > 400000);
 
  // For Existing CANCELLED_MEMBER users
  update api_auth_tokens set account_type='CANCELLED_MEMBER' where dirty_flag = 1 and user_id in 
 (select customerid from customerinfo where member_type_code in(201003,202017,203017,203028,301003,302003,304003,304017,304028));
 
 
 	----------------TELE2 Project----------------
create table partner_account ( 
accountid int(10) not null primary key AUTO_INCREMENT,
display_name varchar(200) not null,
account_name char(50) not null,
password varchar(50) not null,
salt char(10) not null default 'WFLQERJF10',
free_days int(3),
status boolean not null default true,
billcycle ENUM ('MONTHLY','WEEKLY') not null default 'MONTHLY',
billday int(3) not null default 1,
created_on datetime not null default current_timestamp,
CONSTRAINT UC_account_name UNIQUE (account_name)
);
 
INSERT INTO partner_account(display_name,account_name,password,free_days,billday) 
values('Tele2 Sweden','tele2','3E544020ECF6D3C0DE68161E9FDDCD5F',30, 28);
 
 create table partner_customer ( 
		accountid int(10) not null,
		customerid int(10) not null,
		optInApproved boolean not null default true,
		status ENUM('ACTIVE','CANCELED','SUSPENDED','INACTIVE') not null default 'ACTIVE',
		 reason varchar(200),
		 primary key(accountid,customerid),
		 constraint partner_customer_accountid_fk foreign key(accountid) 
		 references partner_account(accountid) on delete cascade,
		 constraint partner_customer_customerid_fk foreign key(customerid) 
		 references customerinfo(customerid) on delete cascade,
		);
  alter table customer2ccmap modify orderid int default null;
-- #Member Transitions alters------------------------------------------------------------------------------------------------------
ALTER TABLE customerinfo ADD COLUMN previous_state varchar(150) not null ;
ALTER TABLE customerinfo ADD COLUMN current_state varchar(150) not null after previous_state;
ALTER TABLE customerinfo ADD COLUMN user_event enum('PURCHASE','CARDUPDATE','G_REDEEM','C_REDEEM','F_REDEEM','D_REDEEM','CANCELLED','CHARGE','NA') default 'NA' after current_state;
ALTER TABLE customerinfo ADD COLUMN system_event enum('EXPIRYDUE','NOCARDINFO','PARKED','NA') default 'NA' after user_event;

create table transition_missing(id int(11) not null primary key  auto_increment,customerid int(11) null,
previous_state varchar(500) not null,current_state varchar(500) not null,user_event varchar(500) null,system_event varchar(500) null,trigger_name varchar(500) null,result varchar(100) null,source varchar(50) null);

--------------------------TELE2 PHASE 2 alters------------------
 create table partner_subscription (  
		accountid int(10) not null,
		subscription_name varchar(50) not null, 
		subscription_price float not null,
		subscriptionid int(10) not null,
		status enum('ACTIVE','INACTIVE') not null default 'ACTIVE', 
		 constraint partner_subscription_accountid_fk foreign key(accountid) 
		 references partner_account(accountid) on delete cascade,
		 constraint partner_subscription_subid_fk foreign key(subscriptionid) 
		 references subscription_master(subscriptionid) on delete cascade,
		 CONSTRAINT UC_subscription_name UNIQUE (subscription_name)		 
		);
   create table partner_campaign (  
		campid int(10) not null primary key AUTO_INCREMENT,
		accountid int(10) not null,
		campaigncode varchar(50) not null,
		campaigntype enum('FREE','FIXED','DISCOUNT') not null default 'DISCOUNT',
		period int(5) not null default 1,
		period_units enum('MONTH','DAY','YEAR'),
		status enum('ACTIVE','INACTIVE') not null default 'ACTIVE', 
		price float, 
		isdefault boolean not null default false,
		 constraint partner_campaign_accountid_fk foreign key(accountid) 
		 references partner_account(accountid) on delete cascade ,
		  CONSTRAINT UC_partnercampaign_campcode UNIQUE (campaigncode)	
		);
	create table partner_customer_campaignmap(
		customerid int(10) not null,
		campid int(10) not null,
		createddate datetime not null default current_timestamp,
		constraint partner_campmap_customerid_fk foreign key(customerid) 
		 references customerinfo(customerid) on delete cascade,
		 constraint partner_campmap_campid_fk foreign key(campid) 
		 references partner_campaign(campid) on delete cascade
	);	
		
  insert into partner_subscription(accountid, subscription_name, subscription_price,subscriptionid) 
  values(1,'SILVER', 119,5),(1,'GOLD', 169,6),(1,'FAMILY',199,7);
  insert into partner_campaign (accountid,campaigncode,campaigntype,period,price,isdefault) values(1,'TELE2DCC','DISCOUNT',-1,20,true);
  insert into partner_campaign (accountid,campaigncode,campaigntype,period,period_units,price,isdefault) values(1,'TELE2FCC','FREE',1,'MONTH',0,false);
  
  alter table customerinfo 
  add accountid int(10),
  add CONSTRAINT  customer_accountid_fk foreign key(accountid) references partner_account(accountid) ON DELETE SET NULL,
  add ext_reason varchar(200);
---------------------TELE2 PHASE 2 ends------------------
---------------------04-JUNE-2018---- start--------------
Update prod_properties SET value='https://nextory.se/html/trustly_payment_template.html' where propkey='TRUSTLY_TEMPLATE_URL';
---------------------------end---------------------------
 
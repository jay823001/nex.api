
-- TABLE FOR bart_publisher
DROP TABLE IF EXISTS `bart_publisher_fee`;
CREATE TABLE `bart_publisher_fee` (
  `pub_fee_id` int(11) NOT NULL AUTO_INCREMENT,
  `elib_price_from` float NOT NULL COMMENT 'Price received from Elib',
  `elib_fee` float NOT NULL COMMENT 'The Fee which is charged by Elib based on elib price',
  `percentage` tinyint(4) DEFAULT NULL COMMENT 'Decides if the elib fee is charged as percentage',
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE' COMMENT 'Activate or inactivates a publisher price',
  PRIMARY KEY (`pub_fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';


DROP TABLE IF EXISTS `bart_publisher`;
CREATE TABLE `bart_publisher` (
  `pub_id` INT NOT NULL AUTO_INCREMENT,
  `publisher_name` VARCHAR(70) NOT NULL,
  `created_date` DATETIME NOT NULL,
  `updated_date` DATETIME NULL,
  PRIMARY KEY (`pub_id`))
COMMENT = 'Table with the publisher info.';

  
-- table for bart_campaign_price
DROP TABLE IF EXISTS `bart_campaign_price`;
  CREATE TABLE `bart_campaign_price` (
  `campaign_price_id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `campaign_price` float NOT NULL,
  `campaign_start_date` datetime NOT NULL,
  `campaign_end_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(45) NOT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`campaign_price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `bart_publisher_payment_map`
--
DROP TABLE IF EXISTS `bart_publisher_payment_map`;
CREATE TABLE IF NOT EXISTS `bart_publisher_payment_map` (
  `publisher_id` int(11) NOT NULL,
  `matrix_id` int(11) NOT NULL,
  `payment_model_id` int(11) NOT NULL,
  `format_id` enum('E_BOOK','AUDIO_BOOK') NOT NULL,
  `distributor` enum('NEST','ELIB') NOT NULL DEFAULT 'NEST',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `description` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bart_publisher_payment_map`
--

-- table for bart_payment_model_meta
DROP TABLE IF EXISTS `bart_payment_model_meta`;
CREATE TABLE `bart_payment_model_meta` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_model_name` varchar(45) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `levels` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `bart_matrix_price_details`
--

DROP TABLE IF EXISTS `bart_matrix_price_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bart_matrix_price_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matrix_id` int(11) NOT NULL,
  `age_from` int(11) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `percentage` tinyint(1) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `level` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `age_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_matrix_id_idx` (`matrix_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bart_matrix_price_meta`
--

DROP TABLE IF EXISTS `bart_matrix_price_meta`;
CREATE TABLE IF NOT EXISTS `bart_matrix_price_meta` (
  `matrix_id` int(11) NOT NULL AUTO_INCREMENT,
  `matrix_name` varchar(65) NOT NULL,
  `levels` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`matrix_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bart_matrix_price_details`
--
ALTER TABLE `bart_matrix_price_details`
  ADD CONSTRAINT `fk_matrix_id` FOREIGN KEY (`matrix_id`) REFERENCES `bart_matrix_price_meta` (`matrix_id`) ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

DROP TABLE IF EXISTS `bart_payment_model_details`;
CREATE TABLE `bart_payment_model_details` (
  `payment_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `perc_book_read_from` int(11) NOT NULL,
  `amount` float NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  UNIQUE KEY `level_UNIQUE` (`level`,`payment_id`),
  KEY `payment_id_idx` (`payment_id`),
  CONSTRAINT `payment_id` FOREIGN KEY (`payment_id`) REFERENCES `bart_payment_model_meta` (`payment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `bart_payment_model_details` CHANGE `amount` `payment` FLOAT NOT NULL; 

ALTER TABLE `bart_campaign_price` ADD `filename` VARCHAR(100) not null;

--
-- Table structure for table `bart_import_campaign_price_failed`
--

DROP TABLE IF EXISTS `bart_import_campaign_price_failed`;
CREATE TABLE IF NOT EXISTS `bart_import_campaign_price_failed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lineno` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `errormessage` varchar(500) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `description` varchar(1000) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `uploaded` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;

ALTER TABLE `bart_payment_model_meta` CHANGE `status` `status` ENUM('ACTIVE','INACTIVE','PENDING') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'PENDING';
ALTER TABLE `bart_payment_model_details` CHANGE `perc_book_read_from` `perc_book_read_to` INT(11) NOT NULL; 
ALTER TABLE `bart_campaign_price` CHANGE `filename` `filename` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `bart_publisher_fee` CHANGE `elib_price_from` `elib_price_to` FLOAT NOT NULL COMMENT 'Price received from Elib'; 

-- sai 10 apr 2015

ALTER TABLE `bart_import_campaign_price_failed` CHANGE `description` `description` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL; 
ALTER TABLE `bart_import_campaign_price_failed` CHANGE `startdate` `startdate` DATE NULL DEFAULT NULL;
ALTER TABLE `bart_import_campaign_price_failed` CHANGE `enddate` `enddate` DATE NULL DEFAULT NULL; 
ALTER TABLE `bart_import_campaign_price_failed` CHANGE `productid` `productid` INT(11) NULL DEFAULT NULL; 

-- sai 14 apr 2015
ALTER TABLE `bart_matrix_price_details` DROP COLUMN `age_from` ;

ALTER TABLE `bart_campaign_price` ADD `ISBN` INT(50) NULL AFTER `campaign_price_id`; 
--changed bart_genralcampign table
DROP TABLE IF EXISTS `bart_genralcampaign`;
CREATE TABLE `bart_genralcampaign` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(45) NOT NULL,
  `pub_id` int(11) NOT NULL,
  `publisher` varchar(45) NOT NULL,
  `discount` int(11) NOT NULL,
  `camp_start_date` datetime NOT NULL,
  `camp_end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `booktype` enum('E_BOOK','AUDIO_BOOK') NOT NULL,
  `percentage` tinyint(4) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;


ALTER TABLE `bart_import_campaign_price_failed` ADD `ISBN` BIGINT(30) NULL AFTER `lineno`; 
--Change ISBN to hold string values
ALTER TABLE `bart_campaign_price` CHANGE `ISBN` `ISBN` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `bart_import_campaign_price_failed` CHANGE `ISBN` `ISBN` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- vijay may 5 2015 
ALTER TABLE `bart_genralcampaign` CHANGE COLUMN `camp_end_date` `camp_end_date` DATETIME NOT NULL  ;

-- sai 8 apr 2015bart_publisher_payment_map
ALTER TABLE `bart_publisher_payment_map` ADD PRIMARY KEY (`publisher_id`, `format_id`) ;
--vijay may 12 2015
ALTER TABLE `bart_publisher_payment_map` ADD COLUMN `contract_start_date` DATETIME NULL  AFTER `updated_date` ;

ALTER TABLE `bart_matrix_price_details` CHANGE `description` `description` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `bart_publisher_payment_map` ADD COLUMN `contract_start_date` DATETIME NULL  AFTER `updated_date` ;

-- Sathish
alter table  bart_publisher_payment_map add column  `formatname` varchar(45)  NOT NULL after format_id;
update bart_publisher_payment_map set formatname='E-book' where format_id='E_BOOK';
update bart_publisher_payment_map set formatname='Audiobook' where format_id='AUDIO_BOOK';
ALTER TABLE `bart_publisher_payment_map` DROP PRIMARY KEY, ADD PRIMARY KEY (`publisher_id`, `formatname`);
alter table  bart_publish


-- this may give an error if format_id already exists

ALTER TABLE `bart_publisher_payment_map` ADD COLUMN `format_id` ENUM('E_BOOK','AUDIO_BOOK') NULL AFTER `formatname`;
update bart_publisher_payment_map set format_id ='E_BOOK' where formatname='E-book';
update bart_publisher_payment_map set format_id ='AUDIO_BOOK' where formatname='Audiobook';

ALTER TABLE `bart_publisher_payment_map` 
CHANGE COLUMN `format_id` `format_id` ENUM('E_BOOK','AUDIO_BOOK') NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`publisher_id`, `format_id`);

ALTER TABLE `bart_publisher_payment_map` 
DROP COLUMN `formatname`;


-- sai 15 jun 2015
CREATE  TABLE `bart_publisher_price_model_meta` (
  `pub_price_id` INT NOT NULL AUTO_INCREMENT ,
  `price_model_name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(45) NULL ,
  `created_date` DATETIME NOT NULL ,
  `updated_date` DATETIME NULL ,
  PRIMARY KEY (`pub_price_id`) );
  
  CREATE  TABLE `bart_publisher_price_model_details` (
  `pub_price_id` INT NOT NULL ,
  `level` INT NOT NULL ,
  `price_to` INT NOT NULL ,
  `value` INT NOT NULL ,
  `ispercent` TINYINT NOT NULL ,
  `description` VARCHAR(45) NULL ,
  `created_date` DATETIME NOT NULL ,
  `updated_date` DATETIME NULL ,
  PRIMARY KEY (`pub_price_id`, `level`) ,
  INDEX `fk_pub_price_id_idx` (`pub_price_id` ASC) ,
  CONSTRAINT `fk_pub_price_id`
    FOREIGN KEY (`pub_price_id` )
    REFERENCES `bart_publisher_price_model_meta` (`pub_price_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
ALTER TABLE `bart_publisher_payment_map` ADD COLUMN `pub_price_id` INT NULL  AFTER `format_id` ;

ALTER TABLE `bart_publisher_payment_map` 
  ADD CONSTRAINT `fk_pub_price_id_`
  FOREIGN KEY (`pub_price_id` )
  REFERENCES `bart_publisher_price_model_meta` (`pub_price_id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_pub_price_id__idx` (`pub_price_id` ASC) ;

ALTER TABLE `bart_publisher_price_model_meta` ADD COLUMN `status` ENUM('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE'  AFTER `updated_date` ;

ALTER TABLE `bart_publisher_price_model_meta` CHANGE COLUMN `description` `description` VARCHAR(100) NULL DEFAULT NULL  ;
ALTER TABLE `bart_publisher_price_model_details` CHANGE COLUMN `description` `description` VARCHAR(100) NULL DEFAULT NULL  ;

-- Bart Publisher Association Revisions (New Alter Script Added on 5th January 2015)
ALTER TABLE `bart_publisher_payment_map`
DROP PRIMARY KEY,
ADD COLUMN `assc_id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (assc_id);

ALTER TABLE `bart_publisher_payment_map` ADD COLUMN `assc_status` ENUM('NOTAPPROVED','APPROVED') NOT NULL DEFAULT 'NOTAPPROVED' AFTER pub_price_id, 
ADD COLUMN `assc_revision` int(11) NOT NULL DEFAULT 1  AFTER `assc_status`,
ADD COLUMN `created_by` INT(11) NOT NULL DEFAULT 0  AFTER `assc_revision`,
ADD COLUMN `approved_by` INT(11) NULL   AFTER `created_by` ,
ADD COLUMN `approved_date` DATETIME NULL   AFTER `approved_by` ,
ADD COLUMN `contract_end_date` DATETIME NULL AFTER `contract_start_date`;

ALTER TABLE `bart_publisher_payment_map` ADD UNIQUE KEY `uk_bart_publisher_payment_map` (`publisher_id`, `format_id`,`assc_revision`);

-- Spring 6 changes related to Bart Admin( check with sprint 6 alters)

ALTER TABLE `bart_publisher_payment_map` ADD COLUMN net_price_derived_from varchar(45);
  
update bart_publisher_payment_map set net_price_derived_from="10,20,30";

CREATE  TABLE `bart_fixed_price` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `productid` INT(11) NOT NULL ,
  `isbn` VARCHAR(45) NOT NULL ,
  `fixed_price` FLOAT(20) NOT NULL ,
  `start_date` DATETIME NOT NULL ,
  `end_date` DATETIME NULL ,
  `createddate` DATETIME NOT NULL ,
  `status` ENUM('ACTIVE','INACTIVE') NOT NULL ,
  PRIMARY KEY (`id`) );
  
  -- Bart Publisher existing records update with Approved status (Rammohan - on 22-01-2016)
  update bart_publisher_payment_map set `assc_status`='APPROVED' ,`approved_by`=0 , assc_revision=1;
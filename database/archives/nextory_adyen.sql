
ALTER TABLE `eb_ordertable` CHANGE `provider` `provider` ENUM('ADYEN','PAYNOVA','PAYEX','KLARNA','E2GO','NOTSELECTED') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `eb_transaction` CHANGE `provider` `provider` ENUM('ADYEN','PAYNOVA','PAYEX') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'ADYEN';

ALTER TABLE `eb_customerinfo` CHANGE `user_from` `user_from` ENUM('NEXTORY','E2GO','BOOKCHEQUE') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'NEXTORY';

ALTER TABLE `eb_ordertable` CHANGE `provider` `provider` ENUM('NEXTORY','ADYEN','PAYNOVA','PAYEX','KLARNA','E2GO','NOTSELECTED') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-- Test Envrionment System Properties
INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_USER','ws@Company.FrescanoGroupAB','User Id for Adyen Integration',now(),3,'String');

INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_PASSWORD','FRESCANOPASSWORD12345','Password for Adyen Integration',now(),3,'String');

INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('MERCHANT_ADYEN','FrescanoGroupABSE','Merchant Name for Adyen Integration',now(),3,'String');

INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_URL','https://pal-test.adyen.com/pal/adapter/httppost','URL for Adyen Payment Gateway',now(),3,'String');


INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_CAPTURE_URL','https://pal-test.adyen.com/pal/servlet/Payment/v10/capture','CAPTURE URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_RECURRING_URL','https://pal-test.adyen.com/pal/servlet/Recurring/v10/listRecurringDetails','RECURRING URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_CANCEL_URL','https://pal-test.adyen.com/pal/servlet/Payment/v10/cancel','CANCEL URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_REFUND','https://pal-test.adyen.com/pal/servlet/Payment/v10/refund','URL for Adyen Payment Gateway',now(),3,'String');

-- Production Envrionment System Properties
INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_USER','ws@Company.FrescanoGroupAB','User Id for Adyen Integration',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_PASSWORD','FRESCANOPASSWORD12345','Password for Adyen Integration',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('MERCHANT_ADYEN','FrescanoGroupABSE','Merchant Name for Adyen Integration',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_URL','https://f982fa0b313bc793-FrescanoGroupAB.pal-live.adyenpayments.com/pal/adapter/httppost','URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_CAPTURE_URL','https://f982fa0b313bc793-FrescanoGroupAB.pal-live.adyenpayments.com/pal/servlet/Payment/v12/capture','CAPTURE URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_RECURRING_URL','https://f982fa0b313bc793-FrescanoGroupAB.pal-live.adyenpayments.com/pal/servlet/Recurring/v12/listRecurringDetails','RECURRING URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_CANCEL_URL','https://f982fa0b313bc793-FrescanoGroupAB.pal-live.adyenpayments.com/pal/servlet/Payment/v12/cancel','CANCEL URL for Adyen Payment Gateway',now(),3,'String');

INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_REFUND','https://f982fa0b313bc793-FrescanoGroupAB.pal-live.adyenpayments.com/pal/servlet/Payment/v12/refund','REFUND URL for Adyen Payment Gateway',now(),3,'String');
----------------for sender mail refrence--------------- Sprint 17 alters
INSERT INTO `test_eb_properties` (`propkey`,`value`,`comments`,`updatedon`,`prop_categoryid`,`datatype`) VALUES( 'MAIL_SENDER_NAME','kundservice@nextory.se','from whom the mail is coming',now() ,0,'String');
INSERT INTO `prod_eb_properties` (`propkey`,`value`,`comments`,`updatedon`,`prop_categoryid`,`datatype`) VALUES( 'MAIL_SENDER_NAME','kundservice@nextory.se','from whom the mail is coming',now() ,0,'String');

---sprint 18 alters----
ALTER TABLE `test_eb_properties` CHANGE `value` `value` VARCHAR(750) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `prod_eb_properties` CHANGE `value` `value` VARCHAR(750) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
INSERT INTO `test_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_CSE_KEY','10001|8E8A9925D9CD9C1E3D3508AF09A58163A722DE04DEAD0CFCB145B765ED9C719132D99421FA08C476D0E1033A730FD9211CC36CDB4355226EF228E65C30C9EA22C5EC1183B006B221A6704559E2F9B02BFF2BDB7E7278BD778EE28A7E870D51E5CCF473002B06A5AFC250AC1CA4911784E7FED2A65B17098388E1404C6F73268317A04A0251F6D1D75D2B3455F112CE92364432AAC460342CEDEB375CAE7ABFE5A66ED9157C3D2724B116DAC8F39ED1EDE20758447512633E2C39753EFBBEBB647C857600B1938260833A0053E5FE9297867DA8F5B84D4C20694AE61A7F3CFB3417D09D4C56BB31F6E2E2E8A4C64A605BE0EE60B9ADF9E1CDED22C931000D72B1','CSE Token for Adyen Payment',now(),3,'String')
INSERT INTO `prod_eb_properties`(`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('ADYEN_CSE_KEY','10001|DB7C97720201F9ABE9A580275D98D33EAE1C9D2CA8D73468D0E83B4624DA567E2D14A3696B44A67783DEA8C12E4EA36A3A927AA0959F1C50DB04BB17E3896AC9253930CF06939E4345E6F06331A4E8612A83A2A1B739D7DFBFCFF710EFB2DA4F1505B1D58E9FB0D0CBB74556B6372169819E9ABCD4B7632B42CF5F2A14142EA7F171FABB0CE7589863DDB14C12BAE316C6E1DAD660CB231C2F6888CBC7A1BD66AC68F5D58DA38C88D121E8F83BABE08FE66C0B8168B8D8C04FE8AF577714843C61EEEED2FD7C17C56DC28C426B702B82D1B642EC1B9A71E1F92998122D2246919D8E3BEF4F164AF17DC89FDB557F0B766A2742540CD2EDBDBAC769F9FD896481','CSE Token for Adyen Payment',now(),3,'String')
----real_password  cloumn fix -varchar changed 20 to 45 -------
ALTER TABLE `eb_customerinfo` CHANGE COLUMN `real_password` `real_password` VARCHAR(45) NULL DEFAULT NULL  ;

---sprint 19 alters -----
INSERT INTO `nx_membertype_master`(`member_type_code`,`membertype`,`comments`)
VALUES(100005,'NONMEMBER_PREVIOUS_EMPLOYEE','memberemployee after next subacription rundate exceeds current date turns into nonmember employee');

-- prod env
INSERT INTO prod_eb_properties(propkey,value,comments,updatedon,datatype) 
values('BUSINESS_ADMIN_MAIL','anna.ek@frescano.se,rammohan.gudditi@frescano.se','Business notification emails to Business Admin',now() ,'string');

--test env
INSERT INTO test_eb_properties(propkey,value,comments,updatedon,datatype) 
values('BUSINESS_ADMIN_MAIL','rammohan.gudditi@frescano.se,vijaya.souri@frescano.se','Business notification emails to Business Admin',now() ,'string');


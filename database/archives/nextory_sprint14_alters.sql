-- concurrent login alters

UPDATE `api_auth_tokens` SET `deviceid` = 0 WHERE `deviceid` IS NULL;

ALTER TABLE `api_auth_tokens` CHANGE COLUMN `deviceid` `deviceid` varchar(45) NOT NULL;

ALTER TABLE `api_auth_tokens` DROP PRIMARY KEY, ADD PRIMARY KEY (user_id,deviceid);
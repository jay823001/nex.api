ALTER TABLE `bart_publisher` 
ADD COLUMN `export_to` ENUM('EXCEL', 'ELIB') NOT NULL DEFAULT 'EXCEL' COMMENT '' AFTER `updated_date`;

--nex741
	CREATE  TABLE `bart_fixed_price` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `productid` INT(11) NOT NULL ,
  `isbn` VARCHAR(45) NOT NULL ,
  `fixed_price` FLOAT(20) NOT NULL ,
  `start_date` DATETIME NOT NULL ,
  `end_date` DATETIME NULL ,
  `createddate` DATETIME NOT NULL ,
  `status` ENUM('ACTIVE','INACTIVE') NOT NULL ,
  PRIMARY KEY (`id`) );
  
  ALTER TABLE `bart_publisher_payment_map` ADD COLUMN net_price_derived_from varchar(45);
  
  update bart_publisher_payment_map set net_price_derived_from="10,20,30";
  
  ALTER TABLE `eb_product_price_log` ADD COLUMN `fixedprice` float DEFAULT NULL after `matrixprice`;
-- SUDHEER 20151026

CREATE TABLE `eb_campaign_voucher_member_type_map` (
  `id` INT(11) NOT NULL COMMENT '',
  `camp_id` INT(11) NOT NULL COMMENT '',
  `parent_member_type` INT(11) NULL COMMENT '',
  `child_member_type` INT(11) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '');
ALTER TABLE `eb_campaign_voucher_member_type_map` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '' ;


-- For all member types
insert into eb_campaign_voucher_member_type_map ( camp_id , parent_member_type , child_member_type) values
(3,100000,0),(3,200000,0),(3,300000,0),(3,400000,0),
(5,100000,0),(5,200000,0),(5,300000,0),(5,400000,0),
(6,100000,0),(6,200000,0),(6,300000,0),(6,400000,0),
(22,100000,0),(22,200000,0),(22,300000,0),(22,400000,0),
(23,100000,0),(23,200000,0),(23,300000,0),(23,400000,0),
(26,100000,0),(26,200000,0),(26,300000,0),(26,400000,0),
(27,100000,0),(27,200000,0),(27,300000,0),(27,400000,0),
(29,100000,0),(29,200000,0),(29,300000,0),(29,400000,0),
(30,100000,0),(30,200000,0),(30,300000,0),(30,400000,0),
(31,100000,0),(31,200000,0),(31,300000,0),(31,400000,0),
(32,100000,0),(32,200000,0),(32,300000,0),(32,400000,0),
(34,100000,0),(34,200000,0),(34,300000,0),(34,400000,0),
(39,100000,0),(39,200000,0),(39,300000,0),(39,400000,0),
(40,100000,0),(40,200000,0),(40,300000,0),(40,400000,0),
(41,100000,0),(41,200000,0),(41,300000,0),(41,400000,0),
(42,100000,0),(42,200000,0),(42,300000,0),(42,400000,0),
(43,100000,0),(43,200000,0),(43,300000,0),(43,400000,0),
(47,100000,0),(47,200000,0),(47,300000,0),(47,400000,0),
(49,100000,0),(49,200000,0),(49,300000,0),(49,400000,0),
(55,100000,0),(55,200000,0),(55,300000,0),(55,400000,0),
(76,100000,0),(76,200000,0),(76,300000,0),(76,400000,0),
(77,100000,0),(77,200000,0),(77,300000,0),(77,400000,0);
-- only new and nonmembers
insert into eb_campaign_voucher_member_type_map ( camp_id , parent_member_type , child_member_type) values
(1,100000,0),(3,400000,0),
(2,100000,0),(3,400000,0),
(4,100000,0),(3,400000,0),
(7,100000,0),(3,400000,0),
(8,100000,0),(3,400000,0),
(9,100000,0),(3,400000,0),
(10,100000,0),(3,400000,0),
(11,100000,0),(3,400000,0),
(12,100000,0),(3,400000,0),
(13,100000,0),(3,400000,0),
(14,100000,0),(3,400000,0),
(15,100000,0),(3,400000,0),
(16,100000,0),(3,400000,0),
(17,100000,0),(3,400000,0),
(18,100000,0),(3,400000,0),
(19,100000,0),(3,400000,0),
(20,100000,0),(3,400000,0),
(21,100000,0),(3,400000,0),
(24,100000,0),(3,400000,0),
(25,100000,0),(3,400000,0),
(28,100000,0),(3,400000,0),
(33,100000,0),(3,400000,0),
(35,100000,0),(3,400000,0),
(36,100000,0),(3,400000,0),
(38,100000,0),(3,400000,0),
(44,100000,0),(3,400000,0),
(45,100000,0),(3,400000,0),
(46,100000,0),(3,400000,0),
(48,100000,0),(3,400000,0),
(50,100000,0),(3,400000,0),
(51,100000,0),(3,400000,0),
(52,100000,0),(3,400000,0),
(53,100000,0),(3,400000,0),
(54,100000,0),(3,400000,0),
(56,100000,0),(3,400000,0),
(57,100000,0),(3,400000,0),
(60,100000,0),(3,400000,0),
(61,100000,0),(3,400000,0),
(62,100000,0),(3,400000,0),
(63,100000,0),(3,400000,0),
(64,100000,0),(3,400000,0),
(65,100000,0),(3,400000,0),
(66,100000,0),(3,400000,0),
(67,100000,0),(3,400000,0),
(68,100000,0),(3,400000,0),
(69,100000,0),(3,400000,0),
(70,100000,0),(3,400000,0),
(71,100000,0),(3,400000,0),
(72,100000,0),(3,400000,0),
(73,100000,0),(3,400000,0),
(74,100000,0),(3,400000,0),
(75,100000,0),(3,400000,0),
(78,100000,0),(3,400000,0);

update eb_customerinfo set member_type_code = 100003 where member_type_code = 400000 ;


 update eb_customerinfo  i ,eb_customer2subscriptionmap m   set member_type_code=100004 
  where m.customerid = i.customerid
  and next_subscription_run_date < '2015-08-24'
  and member_type_code > 400000  ;
  
  -- sudheer 27102015
  
  update eb_customerinfo set member_type_code = 100002 where member_type_code = 100000;
  update eb_customerinfo set prev_mem_code = 100002 where prev_mem_code = 100000;
  update eb_customerinfo set intial_mem_code = 100002 where intial_mem_code = 100000;
  
  -- Mongo updates
  db.nx_membership_change_log.update({"mem_type_code_current" : 100000},{$set:{"mem_type_code_current" : 100002}},{multi:true});
  db.nx_membership_change_log.update({"mem_type_code_intial" : 100000},{$set:{"mem_type_code_intial" : 100002}},{multi:true});
  db.nx_membership_change_log.update({"mem_type_code_new" : 100000},{$set:{"mem_type_code_new" : 100002}},{multi:true});
  db.nx_membership_change_log.update({"mem_type_code_old" : 100000},{$set:{"mem_type_code_old" : 100002}},{multi:true});
  db.nx_membership_change_log.update({"mem_type_code_previous" : 100000},{$set:{"mem_type_code_previous" : 100002}},{multi:true});
		
  
  insert into nx_membertype_master values (100004, 'E2GO_NONMEMBER');
  update nx_membertype_master set member_type_code= 100003 where member_type_code=400000;
  update nx_membertype_master set member_type_code= 100002 where member_type_code=100000;
  
  -- Rammohan 24102015 
  
alter table nx_membertype_master add column comments varchar(5000) after membertype;

update nx_membertype_master set comments='New user or the user already just registered their mail id with NEXTORY or E2GO' where membertype='VISITOR_NO_ACTION';
update nx_membertype_master set comments='Customer purchased gift card from NEXTORY & not taken subscription.' where membertype='VISITOR_GIFTCARD_BUYER';                                                                    
update nx_membertype_master set comments='Became Non Member before 24-Aug-2015 and treating them as VISITOR.' where membertype='E2GO_NONMEMBER';                                                                           
update nx_membertype_master set comments='Purchased Books with E2GO system and became NONMEMBER before 24-Aug-2015 and treating them as VISITOR.' where membertype='NONMEMBER_PIECE_BUYER';                                                        

update nx_membertype_master set comments='Customer taken 14 days Trail subscription and still in the trail period.' where membertype='FREE_TRIAL_MEMBER';                                                                                
update nx_membertype_master set comments='New Member redeemed the Gift card and not yet registered the card details for the next payment' where membertype='FREE_GIFTCARD_NOCARDINFO';                        
update nx_membertype_master set comments='New Member redeemed the campaign, and still Campaign free member' where membertype='FREE_CAMPAIGN_MEMBER';                                                                        
update nx_membertype_master set comments='New Member redeemed the Gift card and registered their card details for the next payment' where membertype='FREE_GIFTCARD_MEMBER';                                        
update nx_membertype_master set comments='New Member redeemed the Gift card and cancelled their subscription before gift card period over.' where membertype='FREE_GIFTCARD_CANCELLED';                  
update nx_membertype_master set comments='New Member redeemed the Campaign and cancelled their subscription before free period over.' where membertype='FREE_CAMPAIGN_CANCELLED';                        
update nx_membertype_master set comments='New free subscriber - registered card will be expired by end of the current month.' where membertype='FREE_CARD_EXPRIYDUE';

 update nx_membertype_master set comments='Active Member paying monthly subscription' where membertype='MEMBER_PAYING';                                                                                                                                    
 update nx_membertype_master set comments='Paying member - registered card will be expired by end of the current month.' where membertype='MEMBER_CARD_EXPIRYDUE';                                                         
 update nx_membertype_master set comments='Paying member redeemed the gift card and still in the gift period.' where membertype='MEMBER_GIFTCARD_EXISTING';                                                                           
 update nx_membertype_master set comments='Paying member redeemed the Campaign and still in the free period.' where membertype='MEMBER_CAMPAIGN_EXISTING';
 update nx_membertype_master set comments='Active paying member cancelled their subscription before next subscription.' where membertype='MEMBER_PAYING_CANCELLED';
 update nx_membertype_master set comments='Paying member redeemed the gift card and cancelled the subscription before gift period over ' where membertype='MEMBER_GIFTCARD_CANCELLED';
 update nx_membertype_master set comments='Paying member redeemed the campaign and cancelled the subscription before free period over ' where membertype='MEMBER_CAMPAIGN_CANCELLED';
 
 
 update nx_membertype_master set comments='Trail Member cancelled their subscription before trail period over.' where membertype='NONMEMBER_PREVIOUS_TRIAL';                                                                           
 update nx_membertype_master set comments='New Gift card member cancelled their subscription or not registered the card details and became nonmember on next subscription date. ' where membertype='NONMEMBER_PREVIOUS_GIFTCARD';                                                                           
 update nx_membertype_master set comments='New free campaign member cancelled their subscription or payment fails and became nonmember on next subscription date.' where membertype='NONMEMBER_PREVIOUS_CAMPAIGN';
 update nx_membertype_master set comments='Paying member cancelled their subscription or payment fails and became nonmember on next subscription date' where membertype='NONMEMBER_PREVIOUS_MEMBER';
 update nx_membertype_master set comments='Paying member became nonmember because of registered card expired.' where membertype='NONMEMBER_CARD_EXPIRED';        


























  
  
  
  

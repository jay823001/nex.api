ALTER TABLE `eb_customerlibrary` CHANGE COLUMN `download_Count` `download_Count` INT(3) NULL  ;
ALTER TABLE `eb_giftcard` CHANGE COLUMN `points` `points` FLOAT NULL  ;
ALTER TABLE `eb_giftcard_pur_details` CHANGE COLUMN `points` `points` FLOAT NULL  ;



--sathish (24/03/2015)
ALTER TABLE `eb_customerlibrary` 
DROP COLUMN `old_productid`,
CHANGE COLUMN `validupto` `validupto` DATETIME NULL ,
CHANGE COLUMN `elib_order_ref` `order_ref` VARCHAR(45) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `orderid` `orderid` INT(11) NULL ;



ALTER TABLE `eb_customerlibrary` CHANGE COLUMN `elib_order_id` `provider_order_id` VARCHAR(45) NULL;
ALTER TABLE `eb_customerlibrary` CHANGE COLUMN `elib_order_date` `order_date` VARCHAR(45) NULL;

ALTER TABLE `eb_downloadtoken` 
DROP COLUMN `old_download_formatid`,
CHANGE COLUMN `challenge` `file_checksum` VARCHAR(250) NULL DEFAULT NULL ,
CHANGE COLUMN `elib_order_ref` `nest_order_ref` VARCHAR(45) NOT NULL ;

ALTER TABLE `eb_downloadtoken` DROP COLUMN `nest_order_ref`;

ALTER TABLE `eb_downloadtoken` DROP COLUMN `sessionid`;



insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('NEST_APP_ID',123,'',now(),1,'int');
insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('NEST_SECRET_KEY','test','',now(),1,'string');
insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('NEST_URL','http://192.168.0.114:8009/Nest/order/createOrder','',now(),1,'string');




 -- this is duplicate field, its already stored in the eb_customerlibrary;
ALTER TABLE `eb_downloadtoken` CHANGE COLUMN `ordercreatedate` `ordercreatedate` datetime NULL;
-- sai 6 apr 2015
ALTER TABLE `elib_product` ADD COLUMN `price` FLOAT NULL  AFTER `imagename` ;




--ALTER TABLE `eb_customerinfo` CHANGE COLUMN `membertype` `membertype`
 --ENUM('NONMEMBER','MEMBER','SPMEMBER','PARTNERMEMBER','BLOGGER','CMEMBER','TRAIL_MEMBER',
--'VISITOR','EMPLOYEE','CANCELLED_MEMBER','CAMPAIGN_MEMBER','CAMPAIGN_EXISTING',
--'PIECE_USER','CANCELLED_CAMPAIGN','PRESENTKORT_MEMBER') NOT NULL;



--ALTER TABLE `eb_customerinfo` ADD COLUMN `initial_membertype` ENUM('VISITOR_NO_ACTION','VISITOR_GIFTCARD_BUYER','MEMBER_TRIAL',	'MEMBER_GIFTCARD_NEW',	'MEMBER_CAMPAING_NEW',	'MEMBER_PAYING','MEMBER_CARD_EXPIRY_DUE',
--	'MEMBER_GIFTCARD','MEMBER_GIFTCARD_EXISTING','MEMBER_CAMPAIGN_EXISTING','MEMBER_EMPLOYEE','CANCELLED_AS_MEMBER','CANCELLED_AS_GIFTCARD',
--	'CANCELLED_AS_CAMPAIGN','NONMEMBER_PREVIOUS_TRIAL','NONMEMBER_PREVIOUS_GIFTCARD','NONMEMBER_PREVIOUS_CAMPAIGN','NONMEMBER_PREVIOUS_MEMBER',
--	'NONMEMBER_CARD_EXPIRED','NONMEMBER_PIECE_BUYER') NULL 
--AFTER `membertype` , ADD COLUMN `previous_membertype` ENUM('VISITOR_NO_ACTION','VISITOR_GIFTCARD_BUYER','MEMBER_TRIAL',	'MEMBER_GIFTCARD_NEW',	'MEMBER_CAMPAING_NEW',	'MEMBER_PAYING','MEMBER_CARD_EXPIRY_DUE',
--	'MEMBER_GIFTCARD','MEMBER_GIFTCARD_EXISTING','MEMBER_CAMPAIGN_EXISTING','MEMBER_EMPLOYEE','CANCELLED_AS_MEMBER','CANCELLED_AS_GIFTCARD',
--	'CANCELLED_AS_CAMPAIGN','NONMEMBER_PREVIOUS_TRIAL','NONMEMBER_PREVIOUS_GIFTCARD','NONMEMBER_PREVIOUS_CAMPAIGN','NONMEMBER_PREVIOUS_MEMBER',
--	'NONMEMBER_CARD_EXPIRED','NONMEMBER_PIECE_BUYER') NULL  AFTER `initial_membertype` ;




	
ALTER TABLE `eb_customerlibrary` ADD COLUMN `last_updated_date` DATETIME NULL  AFTER `elib_price` ;


ALTER TABLE `elib_product` ADD COLUMN `price_updatedon` datetime NULL  AFTER `price` ;

alter table elib_product change column product_status product_status enum('ACTIVE','A_INACTIVE','HIGH_PRICE','NO_FORMAT','P_INACTIVE','P_DELETED','DATA_MISSING','ERROR','UNKNOWN','P_UPCOMING','A_OMITTED','PARKED') NOT NULL  default 'PARKED';
 insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('ELIB_IMPORT_LOC','g:\\temp\\elib-json\\','location to import xml file',now(),2,'String');
 

ALTER TABLE `elib_product` 
CHANGE COLUMN `product_status` `product_status` ENUM('ACTIVE','A_INACTIVE','HIGH_PRICE','NO_FORMAT','P_INACTIVE','P_DELETED','DATA_MISSING','ERROR','UNKNOWN','P_UPCOMING','A_OMITTED','PARKED') NOT NULL DEFAULT 'ACTIVE' ;

ALTER TABLE `elib_public_identifiers` CHANGE COLUMN `id` `id` VARCHAR(30) NOT NULL DEFAULT '' ;


delete from test_eb_properties where propkey in ('ELIB_DOWNLOAD_CONTENT','ELIB_AUDIO_DOWNLOAD_LOCATION','QIOZK_DOWNLOAD_CONTENT','QIOZK_LAST_UPDATE_ON','QIOZK_PRICE_LIMIT','RULEMAILER_KEY','RULE_MAILER_GOLD','RULE_MAILER_SILVER','RULE_MAILER_NON_MEMBER','RULE_MAILER_NON_REGISTERED','QIOZK_API_KEY','BASE_IMAGE_URL_EMAGAZINE','BOOKCHEQUE_SERVICE_KEY','BOOKCHEQUE_RETAILER_ID','SEND_TO_KINDLE_DOWNLOAD_LOCATION','PRESENTKART_MIN_RPICE','PRESENTKART_MAX_RPICE','MENUS','CUSTOM_POST_FAQ','CUSTOM_POST_GUIDE','MENU_HEADER','GIFTCARD_PAGE','HELP_PAGE','BLI_MEDLEM','ORDER_TC_POST','SUBSCRIPTION_TC_POST','FAQ_MENU_NAME','CUSTOM_POST_FORMAT','GUIDE_MENU_NAME','CUSTOM_POST_CAMPAIGN','MAIL_HELP','MAIL_KOP_VILLKOR','MAIL_NEDLAMDDNINGSGUIDE','MAIL_FAQ_HOW_DO_I_DOWNLOAD_E_MAGAZINE','MAIL_FAQ_HOW_DO_I_DOWNLOAD_E_BOOK','MAIL_FAQ_HOW_DO_I_DOWNLOAD_AUDIO_BOOK','NEDLADDNINGSGUIDE_PAGE','REDEEM_GIFTCARD_PAGE','CONTACT_US_TITLE');

INSERT INTO `test_eb_properties` (`propkey`,`value`,`comments`,`updatedon`,`prop_categoryid`,`datatype`)
VALUES ('NEST_SYNCH_URL','http://localhost:8081/nest-comp/store/productDetails','',now(),1,'string');

--merge from D1.2
--ALTER TABLE `eb_customerinfo` CHANGE COLUMN `membertype` `membertype`
 ---ENUM('NONMEMBER','MEMBER','TRAIL_MEMBER',
--'VISITOR','EMPLOYEE','CANCELLED_MEMBER','CAMPAIGN_MEMBER','CAMPAIGN_EXISTING',
--'PIECE_USER','CANCELLED_CAMPAIGN','MEMBER_GIFTCARD_NEW',
--'VISITOR_GIFTCARD_BUYER','MEMBER_GIFTCARD','MEMBER_GIFTCARD_EXISTING','CANCELLED_AS_GIFTCARD') NOT NULL;

INSERT INTO `test_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('BOCKER_PAGE_BOOK_COUNT', '10', 'BOCKER_PAGE_BOOK_COUNT', '2015-03-31 00:00:00', '1', 'int');

alter table eb_customerlibrary change column order_date order_date datetime ;

--sudheer 050515
ALTER TABLE `eb_customerinfo` ADD COLUMN `member_type_code` INT(6) NULL  AFTER `membertype` ;

update eb_customerinfo set member_type_code=300000 where membertype=	'EMPLOYEE';
update eb_customerinfo set member_type_code=304001 where membertype=	'MEMBER'  ;
update eb_customerinfo set member_type_code=203002 where membertype=	'TRAIL_MEMBER' ;
update eb_customerinfo set member_type_code=304003 where membertype=	'CANCELLED_MEMBER';
update eb_customerinfo set member_type_code=202002 where membertype=	'CAMPAIGN_MEMBER'   ;
update eb_customerinfo set member_type_code=302002 where membertype=	'CAMPAIGN_EXISTING' ;
update eb_customerinfo set member_type_code=100000 where membertype=	'VISITOR'	    ;
update eb_customerinfo set member_type_code=400000 where membertype=	'PIECE_USER'	 ;
update eb_customerinfo set member_type_code=202003 where membertype=	'CANCELLED_CAMPAIGN'   ;
update eb_customerinfo set member_type_code=101001 where membertype=    'VISITOR_GIFTCARD_BUYER'	;
update eb_customerinfo set member_type_code=201002 where membertype=	'MEMBER_GIFTCARD_NEW'	 ;
update eb_customerinfo set member_type_code=205007 where membertype=	'MEMBER_GIFTCARD'	;
update eb_customerinfo set member_type_code=301002 where membertype=	'MEMBER_GIFTCARD_EXISTING'   ;
update eb_customerinfo set member_type_code=201003 where membertype=	'CANCELLED_AS_GIFTCARD'	  ;

update eb_customerinfo info , eb_customer2subscriptionmap map set member_type_code= 504005 where map.customerid =info.customerid
and membertype=	'NONMEMBER' ;

update eb_customerinfo set member_type_code=400000 where membertype='NONMEMBER'  ;


ALTER TABLE `eb_customerinfo` ADD COLUMN `prev_mem_code` INT(6) NULL  AFTER `member_type_code` , ADD COLUMN `intial_mem_code` INT(6) NULL  AFTER `prev_mem_code` ;


ALTER TABLE `eb_customerinfo` DROP COLUMN `membertype` ; 


 --ALTER TABLE `eb_scheduler_log` CHANGE COLUMN `` `job_name` VARCHAR(45) NULL  ;
 
 ALTER TABLE `eb_scheduler_log` ADD COLUMN `job_name1` VARCHAR(45) NULL  AFTER `id` ;
 update eb_scheduler_log set  job_name1 = job_name ;

ALTER TABLE `eb_scheduler_log` DROP COLUMN `job_name` ;
ALTER TABLE `eb_scheduler_log` CHANGE COLUMN `job_name1` `job_name` VARCHAR(45) NULL  ;



--------------------------
alter table  elib_product add column  `format` varchar(45) NOT NULL default '123' after formattype;
update elib_product set format='E_BOOK' where formattype=1;
update elib_product set format='AUDIO_BOOK' where formattype=2;
alter table  elib_product drop column  `formatname`;


CREATE TABLE `eb_downloadtoken_archive` (
  `id` int(11) NOT NULL ,
  `customerlibid` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `format` int(11) DEFAULT NULL,
  `part` int(11) DEFAULT NULL,
  `name` varchar(350) DEFAULT NULL,
  `size` varchar(250) DEFAULT NULL,
  `sizetype` varchar(45) DEFAULT NULL,
  `token` varchar(1000) NOT NULL,
  `file_checksum` varchar(250) DEFAULT NULL,
  `ordercreatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_library_downloadlinks` (`customerlibid`)
) ENGINE=InnoDB AUTO_INCREMENT=145247 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;



insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('NEST_PROD_STATUS_SYNC_URL','http://146.148.9.23/nest/api/ver1/sync/productstatus','',now(),0,'string');

alter table  elib_product add column  `createddate_elib` datetime NULL  after createddate;
update elib_product set createddate_elib = createddate;

--insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('SWEDISH_CHARACTERS',' áàâåãäæąçćéèêëęíìîïłñńóòôøõöśßúùûüÿźż?/,','swedish characters',now(),1,'string');
--insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('REPLACE_SWEDISH_CHARACTERS','-aaaaaaaacceeeeeiiiilnnoooooossuuuuyzz','replace swedish charactes',now(),1,'string');

--insert into prod_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('SWEDISH_CHARACTERS',' áàâåãäæąçćéèêëęíìîïłñńóòôøõöśßúùûüÿźż?/,','swedish characters',now(),1,'string');
--insert into prod_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('REPLACE_SWEDISH_CHARACTERS','-aaaaaaaacceeeeeiiiilnnoooooossuuuuyzz','replace swedish charactes',now(),1,'string');

INSERT INTO `test_eb_properties` (`propkey`, `value`, `comments`, `prop_categoryid`, `datatype`,`updatedon`) 
VALUES ('BATCH_URL', 'http://localhost:8081/nextory_batch/', 'Batch URL', '1', 'String',now());

--------neeraj 17th June 2015------
ALTER TABLE `eb_camp_voucher_map` CHANGE `voucher_code` `voucher_code` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL; 

INSERT INTO `test_eb_properties` (`propkey`, `value`, `comments`, `prop_categoryid`, `datatype`,`updatedon`) 
VALUES ('JOB_DASHBOARD_SERVER_URL', 'http://frescano-monitoring.appspot.com/api/notify/', 'dashboard URL', '1', 'String',now());

ALTER TABLE `elib_product` CHANGE COLUMN `product_status` `product_status` ENUM('ACTIVE','A_INACTIVE','HIGH_PRICE','NO_FORMAT','P_INACTIVE','P_DELETED','DATA_MISSING','ERROR','UNKNOWN','P_UPCOMING','A_OMITTED','PARKED','L_INACTIVE') NOT NULL DEFAULT 'ACTIVE'  ;

update elib_product set product_status='A_OMITTED' where product_status='HIGH_PRICE';

ALTER TABLE `eb_customerlibrary` ADD COLUMN `read_status` ENUM('STARTED','ONGOING','DONE') NULL  AFTER `last_updated_date` ;

-- sai 13 jul 2015
ALTER TABLE `elib_relation` ADD COLUMN `doneby` INT NOT NULL DEFAULT 0 COMMENT '' AFTER `c_productid`;

-- Feroz 21 july 2015
ALTER TABLE `eb_customerlibrary` ADD COLUMN `read_completed_date` DATE NULL DEFAULT NULL  AFTER `read_status` ;

ALTER TABLE `eb_userroles` CHANGE COLUMN `customer_role` `customer_role` ENUM('ROLE_ADMIN','ROLE_CS','ROLE_SUPER') NOT NULL  ;

-- sai 26 jul 2015
ALTER TABLE `eb_customerinfo` 
CHANGE COLUMN `password` `password` VARCHAR(45) NULL DEFAULT NULL COMMENT '' ;

ALTER TABLE `eb_customerinfo` 
CHANGE COLUMN `salt` `salt` VARCHAR(8) NULL DEFAULT NULL COMMENT '' ;

create table UserConnection (userId varchar(255) not null,
	providerId varchar(255) not null,
	providerUserId varchar(255),
	rank int not null,
	displayName varchar(255),
	profileUrl varchar(512),
	imageUrl varchar(512),
	accessToken varchar(512) not null,
	secret varchar(512),
	refreshToken varchar(512),
	expireTime bigint,
	primary key (userId, providerId, providerUserId));
create unique index UserConnectionRank on UserConnection(userId, providerId, rank);


insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('BLOCK_ORDERS','true','',now(),1,'boolean');

ALTER TABLE `app_library_sync_master_log` ADD COLUMN `fixforverios4` TINYINT(1) NOT NULL DEFAULT 0  AFTER `syncdateForHighlights` ;


-- insert into test_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('NEST_PRICE_LIMIT','0','',now(),1,'int');

insert into  prod_eb_properties (propkey,value,comments,updatedon,prop_categoryid,datatype) values('NEST_PRICE_LIMIT','0','',now(),1,'int');

--
CREATE TABLE `eb_product_price_log` (
  `productid` int(11) NOT NULL,
  `elibprice` float DEFAULT NULL,
  `pubprice` float DEFAULT NULL,
  `campprice` float DEFAULT NULL,
  `gencampprice` float DEFAULT NULL,
  `matrixprice` float DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `finalprice` float DEFAULT NULL,
  `campaignname` varchar(100) DEFAULT NULL,
  `publisheddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `bart_publisher` CHANGE COLUMN `updated_date` `updated_date` DATETIME NULL  , ADD COLUMN `last_run_date` DATETIME NOT NULL DEFAULT '2015-08-24 00:00:00'  AFTER `updated_date` , ADD COLUMN `destination` ENUM('EXCEL','WEB_SERVICE') NOT NULL DEFAULT 'EXCEL'  AFTER `last_run_date` , ADD COLUMN `frequency` ENUM('MANUAL') NOT NULL DEFAULT 'MANUAL'  AFTER `destination` ;

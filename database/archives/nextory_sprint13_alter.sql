
//for firstime app login
ALTER TABLE `eb_customerinfo` ADD COLUMN `first_app_loggedin` DATETIME NULL DEFAULT NULL  AFTER `createdon` ;

update eb_customerinfo info , api_auth_tokens api set info.first_app_loggedin = api.createddate where  info.customerid = api.user_id ;


-- Deb 31052016 NEX-1137 TMP Password Changes

delimiter $$

CREATE TABLE `eb_customer_temp_password` (
  `customerid` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(45) DEFAULT NULL,
  `real_password` varchar(20) DEFAULT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`customerid`)
) ENGINE=InnoDB AUTO_INCREMENT=7701 DEFAULT CHARSET=utf8$$




-- Top List Alters

ALTER TABLE `eb_product_list_map` ADD COLUMN `adjust_factor` INT(11) NULL  DEFAULT 0 AFTER `position` ;

ALTER TABLE eb_product_list ADD COLUMN `show_in_category_page` TINYINT(1) DEFAULT 0 AFTER `isactive`;

ALTER TABLE eb_product_list ADD column mode ENUM('MANUAL','AUTO') after show_in_category_page;

-- Clean up existing product list for new top list change
delete from eb_product_list where subscription_type in('BASE','STANDARD');

delete from eb_product_list_map where product_list_id not in(select product_list_id from eb_product_list);

delete from eb_product_list where isactive=false;

ALTER TABLE `eb_product_list` CHANGE COLUMN `subscription_type` `subscription_type` ENUM('PREMIUM','STANDARD','BASE') NULL;

update eb_product_list set subscription_type=null where subscription_type='PREMIUM';


UPDATE eb_product_list set mode='AUTO' where  slug in('topplista-e-bocker','topplista-ljudbocker');

-- UPDATE eb_product_list set mode='AUTO' where  slug='Nyheter'  and show_in_category_page=true;

UPDATE eb_product_list set mode='AUTO' where  category_id is not null and category_id > 0 and show_in_category_page=false;

UPDATE eb_product_list set mode='MANUAL' where  slug='vi-rekommenderar';

UPDATE eb_product_list set mode='MANUAL' where  slug='Nyheter'  and show_in_category_page=false;

delete from eb_product_list_map where product_list_id not in(select product_list_id from eb_product_list where mode='MANUAL')

ALTER TABLE `eb_product_list_map`  ADD CONSTRAINT `FK_KEY_LIST_ID` FOREIGN KEY (`product_list_id`) REFERENCES `eb_product_list` (`product_list_id`);

update eb_product_list set show_in_home_page=true where isactive=true;

update eb_product_list set displayname='Deckare' where slug='deckare';

update eb_customerlibrary lib set status = 'DELETED' where  status in ('OFFLINE_READER' ,'U_DOWNLOAD','INACTIVE','E2GO_ORDERED') and storagelocation ='ELIB' or  storagelocation is null;

-- alters 
ALTER TABLE eb_customerlibrary MODIFY COLUMN book_status_code  int(2) NULL DEFAULT 0;
update eb_customerlibrary set book_status_code =1 where order_date is not null;
update eb_customerlibrary set book_status_code =0 where order_date is null;


--Sathish for allowing null values in storage location
ALTER TABLE eb_customerlibrary MODIFY storagelocation varchar(45) null;
update eb_customerlibrary set storagelocation =null where status ='E2GO_ORDERED';

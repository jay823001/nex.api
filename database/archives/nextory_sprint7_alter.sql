-- sudheer 28102015
ALTER TABLE `eb_subscription_master` 
DROP COLUMN `max_point_carry_forward`,
ADD COLUMN `book_price_limit` FLOAT NOT NULL DEFAULT 0 COMMENT '' AFTER `isactive`,
ADD COLUMN `subscription_type` ENUM('PREMIUM', 'STANDARD', 'BASE') NOT NULL DEFAULT 'BASE' COMMENT '' AFTER `book_price_limit`;

ALTER TABLE `eb_subscription_master` 
DROP COLUMN `subscription_hierarchy`,
ADD COLUMN `subscription_type` ENUM('PREMIUM', 'STANDARD', 'BASE') NOT NULL DEFAULT 'BASE' COMMENT '' AFTER `book_price_limit`;
-- ALTER TABLE `eb_customer2subscriptionmap` CHANGE COLUMN `prev_subscriptionid` `next_subscriptionid` INT(11) NULL DEFAULT NULL COMMENT '' ;

--Rammohan 28102015
UPDATE `eb_subscription_master` SET `isactive`='0', `subscription_type`='PREMIUM' WHERE `subscriptionplanname`='Unlimited';

INSERT INTO `eb_subscription_master` (`subscriptionplanname`, `subscriptionprice`, `points`, `offerprice`, `validity`, `createddate`, `isactive`, `book_price_limit`, `subscription_type`) 
VALUES ('BAS', '99', '0', '99', '0', now(), '1', '46', 'BASE');


INSERT INTO `eb_subscription_master` (`subscriptionplanname`, `subscriptionprice`, `points`, `offerprice`, `validity`, `createddate`, `isactive`, `book_price_limit`, `subscription_type`) 
VALUES ('STANDARD', '199', '0', '199', '0', now(), '1', '128', 'STANDARD');

INSERT INTO `eb_subscription_master` (`subscriptionplanname`, `subscriptionprice`, `points`, `offerprice`, `validity`, `createddate`, `isactive`, `book_price_limit`, `subscription_type`) 
VALUES ('PREMIUM', '249', '0', '249', '0', now(), '1', '200', 'PREMIUM');

--sudheer 20151102
ALTER TABLE `elib_product` 
ADD COLUMN `allowed_for` ENUM('BASE', 'STANDARD', 'PREMIUM') NULL COMMENT '' AFTER `price_updatedon`;


ALTER TABLE `eb_product_list` ADD COLUMN `subscription_type`  ENUM('PREMIUM','STANDARD','BASE') NOT NULL DEFAULT 'PREMIUM'  ;

--sudheer 20151105
ALTER TABLE `eb_customer2subscriptionmap` 
ADD COLUMN `next_subscriptionid` INT(11) NULL COMMENT '' AFTER `next_subscription_run_date`;

--sudheer 20151106
ALTER TABLE `eb_customerlibrary` 
ADD COLUMN `added_as` ENUM('PREMIUM', 'STANDARD', 'BASE') NULL DEFAULT NULL COMMENT '' AFTER `read_completed_date`,
ADD COLUMN `allowed_for` ENUM('PREMIUM', 'STANDARD', 'BASE') NULL COMMENT '' AFTER `added_as`,
ADD COLUMN `allowed_confirmation` TINYINT(1) NULL COMMENT '' AFTER `allowed_for`;

UPDATE eb_customerlibrary set added_as ='PREMIUM' ;


UPDATE eb_customerlibrary lib , elib_product prod SET lib.allowed_for = CASE 
WHEN prod.price <= 46  THEN 'BASE' 
WHEN prod.price <= 128 THEN 'STANDARD' 
WHEN prod.price <= 200 THEN 'PREMIUM' 
WHEN prod.price > 200 THEN 'PREMIUM'
END  WHERE lib.productid  = prod.productid and lib.allowed_for is null ;

UPDATE eb_customerlibrary set allowed_for = added_as where   allowed_for is null;



--Neeraj 5th Nov 2015
UPDATE `eb_product_list` SET `slug` = 'skonlitteratur' WHERE `eb_product_list`.`product_list_id` = 9; 


ALTER TABLE `eb_customerlibrary` 
CHANGE COLUMN `purchase_type` `purchase_type` ENUM('PIECE', 'UNLIMITED', 'PREMIUM', 'STANDARD', 'BASE') NULL DEFAULT 'PIECE' COMMENT '' ;

insert into nx_membertype_master values (201010, 'FREE_GIFTCARD_UPGRADE_MEMBER','free giftcard nocardinfo or member upgrades subscription');

INSERT INTO `prod_eb_properties` (`id`, `propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ('157', 'ALL_EMAIL_LOCATION', '/app/nextory/app-data', 'storage of all emails', '2015-11-17', 'String');

CREATE TABLE `nextory_automatic_mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(45) DEFAULT NULL,
  `filename` varchar(45) DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8$$;

ALTER TABLE `nextory_automatic_mails` CHANGE COLUMN `filename` `filename` VARCHAR(100) NULL DEFAULT NULL  ;
ALTER TABLE `nextory_automatic_mails` CHANGE COLUMN `subject` `subject` VARCHAR(100) NULL DEFAULT NULL  ;
ALTER TABLE `nextory_automatic_mails` CHANGE COLUMN `comments` `comments` VARCHAR(300) NULL DEFAULT NULL  ;

INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('1', 'Välkommen till Unlimited', 'Valkommen till Unlimited.eml', 'Email to the user when user complete the transaction using prova unlimited ');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('2', 'Ditt abonnemang är uppsagt', 'Ditt abonnemang ar uppsagt!.eml', 'paying subscriber cancels subscription before next subscription date, mail will triggered immedeatelty to the customer');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('3', 'Ditt abonnemang är nu avslutat', 'Ditt abonnemang ar nu avslutat.eml', 'Notifying user saying subscription is terminated  due to non payment (After 7 days of failure transaction )');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('4', 'Ditt kort går ut - uppdatera nu!', 'Ditt kort gar ut - uppdatera nu!.eml', 'Email reminder to subscribed user 2 days before user registred  credit card expiry.');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('5', 'Du har väl inte glömt bort ditt gratis abonnemang?', 'Du har val inte glomt bort ditt gratis abonnemang.eml', 'Email sending to trail member when user didn’t add the books  into library after  7 days ');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('6', 'Har du glömt ditt lösenord?', 'Har du glomt ditt losenord.eml', 'For the Forgot password request, mail to customer with password reset link  ');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('7', 'Något har blivit fel med din betalning', 'Nagot har blivit fel med din betalning.eml', 'While jobs runs on the next sybscription date , If payament fails, this mail trigger (First mail to customer about payment failure)');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('8', 'Påminnelse kortuppgifter', 'Paminnelse kortuppgifter.eml', 'Email reminder to subscribed user 7 days before user registred  credit card expiry ');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('9', 'Tack för att du besöker Nextory', 'Tack for att du besoker Nextory.eml', 'Email to new customers who tries to make the transaction with nextory but not complete it for whatever reason');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('10', 'Tack för att du värvade en vän!', 'Tack for att du varvade en van!.eml', 'when customers referred friend charged with first payment then customer will receive this email with some days added to his subscription');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('11', 'Uppdatera kortuppgifterna hos Nextory', 'Uppdatera kortuppgifterna hos Nextory.eml', 'Email reminder to subscribed user 30 days before user registred  credit card expiry ');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('12', 'Vi har inte mottagit din betalning', 'Vi har inte mottagit din betalning.eml', 'Email after 3 days of Recursive payment failure due to invalid  credit card details. ');
INSERT INTO  `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('13', 'Viktig information om kortuppgifter', 'Viktig information om kortuppgifter.eml', 'Email reminder to subscribed user 14 days before user registred  credit card expiry ');

--NEERAJ 27 Nov 2015
ALTER TABLE `eb_customerinfo` ADD `marketing_src` VARCHAR(45) NULL ; 





-- RAM 01-DEC-2015
ALTER TABLE `eb_customerlibrary` CHANGE COLUMN `status` `status` ENUM('ACTIVE','INACTIVE','E2GO_ORDERED','OFFLINE_READER','REFUND','DELETED','U_DOWNLOAD','U_INACTIVE','REMOVED') NOT NULL  ;


update eb_subscription_master set subscriptionplanname='PREMIUM 2' where subscriptionid=4;

-- Mongo changes
db.nx_customer_search_behaviour_log.renameCollection( "nx_customer_search_behaviour_log_old");

-- Mongo changes
db.nextory_price_behaviour_log.renameCollection( "nextory_price_behaviour_log_old");
db.nx_reading_behaviour_log.update({},{$set:{"added_as" : "PREMIUM","allowed_for" : "PREMIUM","subTypeEnum":"PREMIUM"}},{multi:true});



ALTER TABLE `eb_product_list` 
ADD COLUMN `category_id` INT(11) NULL COMMENT '' AFTER `der_cat`,
ADD COLUMN `position` INT(11) NOT NULL DEFAULT 0 COMMENT '' AFTER `category_id`;

ALTER TABLE `eb_campaign_voucher_master` 
ADD COLUMN `source` VARCHAR(25) NULL COMMENT '' AFTER `multiple_reduction_allow`,
ADD COLUMN `channel` VARCHAR(30) NULL COMMENT '' AFTER `source`;

ALTER TABLE `eb_campaign_voucher_master` 
CHANGE COLUMN `channel` `channel_id` INT(11) NULL DEFAULT 0 COMMENT '' ;



CREATE  TABLE `eb_campaign_channelmaster` (
  `channel_id` INT NOT NULL AUTO_INCREMENT ,
  `channel_name` VARCHAR(45) NOT NULL ,
  `created_by` INT NULL ,
  `createddate` VARCHAR(45) NOT NULL ,
  `updateddate` VARCHAR(45) NULL ,
  `isactive` TINYINT NOT NULL DEFAULT false ,
  PRIMARY KEY (`channel_id`) );
  
--for failed order dumpxcel to table
  CREATE  TABLE  `finacial_report_dump` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `fakturanumber` DOUBLE NULL ,
  `amount` VARCHAR(45) NULL ,
  `status` VARCHAR(45) NULL ,
  `source` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) );
  
  db.nx_membership_change_log.update({},{$set:{"comments" : null}},{multi:true});


-- Babu Added two fields in campaign table
ALTER TABLE `eb_campaign_voucher_master` 
ADD COLUMN `header_text` VARCHAR(11) NOT NULL ,
ADD COLUMN `tag_text` VARCHAR(11) NOT NULL;

  ---all mails---
INSERT INTO `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('14', 'Orderbekräftelse Presentkort', 'Har ar din orderbekraftelse.eml ', 'This is the gift card that you have ordered from Nextory');
INSERT INTO `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('15', 'Här är presentkortet som du har beställt från Nextory', 'Har ar presentkortet som du har bestallt fran Nextory.eml', 'This is the gift card that you have ordered from Nextory');
INSERT INTO `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('16', ' bjuder dig på  dagar obegränsad läsning', ' bjuder dig pa  dagar obegransad lasning.eml', 'friend invite you on days xxxx of unlimited reading');
INSERT INTO `nextory_automatic_mails` (`id`, `subject`, `filename`, `comments`) VALUES ('17', 'Hitta tillbaka till dina böcker', 'Hitta tillbaka till dina bocker.eml', 'Thank you for trying our service. Your subscription with us is now closed. register to back ');



UPDATE  `nextory_automatic_mails` SET `filename`='Kumar bjuder dig pa 20 dagar obegransad lasning.eml' WHERE `id`='16';
UPDATE  `nextory_automatic_mails` SET `filename`='Presentkort Nextory.eml', subject='Presentkort Nextory' WHERE `id`='15';

--20160122--
ALTER TABLE `eb_campaign_voucher_master` CHANGE COLUMN `displayprice` `displayprice` float NULL DEFAULT 0.0 ;
ALTER TABLE `eb_campaign_voucher_master` CHANGE COLUMN `header_text` `header_text` VARCHAR(200) NOT NULL ;
ALTER TABLE `eb_campaign_voucher_master` CHANGE COLUMN `tag_text` `tag_text` VARCHAR(200) NOT NULL;

-- INSERT INTO `test_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ( 'REPORT_SENDS_USER_MAILS', 'sudheer@frescano.se,hari@frescano.se', 'reports send user mails', '2015-11-17', 'String');
INSERT INTO `prod_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ( 'REPORT_SENDS_USER_MAILS', 'sudheer@frescano.se,hari@frescano.se,daniel@frescano.se', 'reports send user mails', '2016-02-17', 'String');

--INSERT INTO `test_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ( 'NEXTORY_SYSTEM_PROPERTY_URL', 'http://127.0.0.1:9090/systemproperty', 'UPDATE INMEMORY CACHE DATA IN CLIENT SERVER', '2015-11-17', 'String');
INSERT INTO `prod_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ( 'NEXTORY_SYSTEM_PROPERTY_URL', 'http://127.0.0.1:9090/systemproperty', 'UPDATE INMEMORY CACHE DATA IN CLIENT SERVER', '2016-02-17', 'String');



-- sudheer 190022016

ALTER TABLE `eb_customerlibrary` 
ADD COLUMN `book_status_code` INT(2) NULL DEFAULT 0 COMMENT '' AFTER `allowed_confirmation`;

ALTER TABLE `eb_customerlibrary_dump_log` 
ADD COLUMN `book_status_code` INT(2) NULL DEFAULT 0 COMMENT '' AFTER `allowed_confirmation`;

 update eb_customerlibrary set book_status_code = 0 where status ='OFFLINE_READER' ;
 update eb_customerlibrary set book_status_code = 0 where status ='INACTIVE' ;
 update eb_customerlibrary set book_status_code = -1 where status ='E2GO_ORDERED' ;
 
 -- Rammohan 29th Feb 2016
UPDATE `api_auth_tokens` SET `deviceid` = 0 WHERE `deviceid` IS NULL;

ALTER TABLE `api_auth_tokens` CHANGE COLUMN `deviceid` `deviceid` varchar(45) NOT NULL;

ALTER TABLE `api_auth_tokens` DROP PRIMARY KEY, ADD PRIMARY KEY (user_id,deviceid);

-- INSERT INTO `test_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ( 'APP_CONCURRENT_USERS_COUNT', 3, '', '2016-02-29', 'int');
INSERT INTO `prod_eb_properties` (`propkey`, `value`, `comments`, `updatedon`, `datatype`) VALUES ( 'APP_CONCURRENT_USERS_COUNT', 3, '', '2016-02-29', 'int');

ALTER TABLE `api_auth_tokens` CHANGE COLUMN `createddate` `createddate` DATETIME NOT NULL;

ALTER TABLE `api_auth_tokens` CHANGE COLUMN `updateddate` `updateddate` DATETIME NULL;

CREATE TABLE IF NOT EXISTS `eb_category_publisher_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `for_publisher` varchar(150) NOT NULL DEFAULT 'ALL',
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`),
  KEY `cat_id_2` (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


--Sathish
 create index product_index on eb_customerlibrary (productid);
 

 --sathish
 
 
 drop table eb_category_publisher_mapping;

CREATE TABLE `eb_category_publisher_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `for_publisher` varchar(150) NOT NULL DEFAULT 'ALL',
  `association_on` varchar(45) DEFAULT NULL,
  `relation_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`),
  CONSTRAINT `FK_KEY_CAT_PUB` FOREIGN KEY (`cat_id`) REFERENCES `eb_categories` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;



CREATE TABLE `eb_categories_special_mapping` (
  `id` INT NOT NULL,
  `category_id` INT NOT NULL,
  `relation_identifier_id` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_eb_category_sp_map_idx` (`category_id` ASC),
  CONSTRAINT `FK_eb_category_sp_map`
    FOREIGN KEY (`category_id`)
    REFERENCES `eb_categories` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



 ALTER TABLE `eb_categories` 
ADD COLUMN `position` INT(2) NOT NULL DEFAULT 0 COMMENT '' AFTER `popular`;

INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`,`position`) VALUES ('49', '0', 'Harlequin', '1', 'harlequin', '0',7);

update  eb_categories set position = category_id where category_id <=6 ;

update  eb_categories set position = category_id+1 where category_id > 6 and parent_cat_id = 0 ;





update  eb_categories set position = 1 where parent_cat_id = 1 and category_name='0-3 år';              
update  eb_categories set position = 2 where parent_cat_id = 1 and category_name='3-6 år';              
update  eb_categories set position = 3 where parent_cat_id = 1 and category_name='6-9 år';              
update  eb_categories set position = 4 where parent_cat_id = 1 and category_name='9-12 år' ;            
update  eb_categories set position = 5 where parent_cat_id = 1 and category_name='12-15 år';            
update  eb_categories set position = 1 where parent_cat_id = 6 and category_name='Filosofi';            
update  eb_categories set position = 2 where parent_cat_id = 6 and category_name='Religion';            
update  eb_categories set position = 1 where parent_cat_id = 9 and category_name='Aforismer & Citat';   
update  eb_categories set position = 2 where parent_cat_id = 9 and category_name='Humor';               
update  eb_categories set position = 3 where parent_cat_id = 9 and category_name='Tecknade Serier';    
update  eb_categories set position = 1 where parent_cat_id = 14 and category_name='Pedagogik';           
update  eb_categories set position = 2 where parent_cat_id = 14 and category_name='Psykologi' ;          
update  eb_categories set position = 1 where parent_cat_id = 18 and category_name='Dramatik'   ;         
update  eb_categories set position = 2 where parent_cat_id = 18 and category_name='Litterära Essäer'  ;  
update  eb_categories set position = 3 where parent_cat_id = 18 and category_name='Lyrik'   ;            
update  eb_categories set position = 4 where parent_cat_id = 18 and category_name='Lättläst' ;           
update  eb_categories set position = 5 where parent_cat_id = 18 and category_name='Noveller' ;           
update  eb_categories set position = 6 where parent_cat_id = 18 and category_name='Romaner' ;            
update  eb_categories set position = 1 where parent_cat_id = 20 and category_name='Humaniora'   ;        
update  eb_categories set position = 2 where parent_cat_id = 20 and category_name='Språk och Ordböcker' ;
update  eb_categories set position = 3 where parent_cat_id = 20 and category_name='Litteraturvetenskap'; 







INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('50', '49', 'Bestseller', '1', 'bestseller', '0');
INSERT INTO `	` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('51', '49', 'Spänning', '1', 'spänning', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('52', '49', 'Historisk', '1', 'historisk', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('53', '49', 'Läkarromaner', '1', 'läkarromaner', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('54', '49', 'Paranormal Romance', '1', 'paranormal_romance', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('55', '49', 'Passion', '1', 'passion', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('56', '49', 'Prélude', '1', 'prelude', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('57', '49', 'Romantik', '1', 'romantik', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('58', '49', 'Silk', '1', 'silk', '0');
INSERT INTO `eb_categories` (`category_id`, `parent_cat_id`, `category_name`, `isactive`, `slug_name`, `popular`) VALUES ('59', '49', 'Övrigt', '1', 'teen', '0');


INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (1,'49', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (2,'50', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (3,'51', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (4,'52', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (5,'53', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (6,'54', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (7,'55', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (8,'56', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (9,'57', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (10,'58', 'Förlaget Harlequin');
INSERT INTO `eb_category_publisher_mapping` (id,`cat_id`, `for_publisher`) VALUES (11,'59', 'Förlaget Harlequin');

UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='1';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='2';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='3';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='4';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='5';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='6';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='7';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='8';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='9';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='10';
UPDATE `eb_category_publisher_mapping` SET `association_on`='relation' WHERE `id`='11';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='1';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='2';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='3';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='4';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='5';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='6';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='7';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='8';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='9';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='10';
UPDATE `eb_category_publisher_mapping` SET `relation_type`='6' WHERE `id`='11';




INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (1, 50, 'Bestseller');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (2, 51, 'Black Rose');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (3, 50, 'HQ Bestseller');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (4, 52, 'Historisk');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (5, 53, 'Läkarromaner');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (6, 54, 'Paranormal');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (7, 55, 'Passion');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (8, 56, 'Prélude');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (9, 57, 'Romantik');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (10,58, 'Silk');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (11, 51, 'Spänning');

INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (12, 57, 'Spice');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (13, 57, 'Ängeln');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (14, 57, 'E-books Skönlitteratur');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (15, 57, 'Romaner');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (16, 59, 'TEEN');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (17, 59, 'Skönlitteratur');


INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (18, 49, 'Bestseller');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (19, 49, 'Black Rose');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (20, 49, 'HQ Bestseller');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (21, 49, 'Historisk');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (22, 49, 'Läkarromaner');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (23, 49, 'Paranormal');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (24, 49, 'Passion');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (25, 49, 'Prélude');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (26, 49, 'Romantik');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (27, 49, 'Silk');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (28, 49, 'Spänning');

INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (29, 49, 'Spice');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (30, 49, 'Ängeln');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (31, 49, 'E-books Skönlitteratur');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (32, 49, 'Romaner');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (33, 49, 'TEEN');
INSERT INTO `eb_categories_special_mapping` (`id`, `category_id`, `relation_identifier_id`) VALUES (34, 49, 'Skönlitteratur');




'Bestseller' => 'Bestseller';
'Black Rose' => 'Spänning';
'HQ Bestseller' => 'Bestseller';
'Historisk' => 'Historisk';
'Läkarromaner' => 'Läkarromaner';
'Paranormal' => 'Paranormal Romance';
'Passion' => 'Passion';
'Prélude' => 'Prélude';
'Romantik' => 'Romantik';
'Silk' => 'Silk';
'Spänning' => 'Spänning';
'Spice' => 'Övrigt';
'Ängeln' => 'Övrigt';
'E-books Skönlitteratur' => 'Övrigt';
'Romaner' => 'Övrigt'; 
'TEEN' => 'Övrigt';
'Skönlitteratur' => 'Romantik'



--sathish 15032016 - changes to the category mappings
update  eb_categories_special_mapping set category_id = 59 where  id = 12 and relation_identifier_id ='Spice';
update  eb_categories_special_mapping set category_id = 59 where  id = 13 and relation_identifier_id ='Ängeln' ;
update  eb_categories_special_mapping set category_id = 59 where  id = 14 and relation_identifier_id ='E-books Skönlitteratur'  ;
update  eb_categories_special_mapping set category_id = 59 where  id = 15 and relation_identifier_id ='Romaner'  ;
update  eb_categories_special_mapping set category_id = 57 where  id = 17 and relation_identifier_id ='Skönlitteratur'  ;
 
--
ALTER TABLE `app_library_sync_master_log` 
ADD COLUMN `last_cur_perc` DOUBLE NULL COMMENT '' AFTER `position`;

update app_library_sync_master_log set last_cur_perc = high_percentage ;
update app_library_sync_master_log set last_cur_perc =  TRUNCATE(last_cur_perc , 2)  where last_cur_perc <1 ;



 
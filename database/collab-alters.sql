
CREATE TABLE `collabs` (
  `collabid` int(11) NOT NULL,
  `companyname` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `link` varchar(100) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(5) NOT NULL,
  `expireson` datetime NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` int(5) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED','') NOT NULL DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `collabs`
  ADD PRIMARY KEY (`collabid`);

ALTER TABLE `collabs`
  MODIFY `collabid` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `collabs` ADD `position` INT(5) NOT NULL AFTER `status`;



  
INSERT INTO `test_properties` (`id`, `propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES (184, 'COLLAB_PICTURE_LOCATION', '/app/nextory/www-data/collabimg', NULL, now(), 0, NULL);
INSERT INTO `test_properties` (`id`, `propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES (185, 'COLLAB_PICTURE_HTTP_LINK', '/collabimg', NULL, now(), '0', '0');


alter table collabs change column picture picture VARCHAR(100);
alter table collabs change column description description VARCHAR(1000);

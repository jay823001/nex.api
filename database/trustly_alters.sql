# test environment
insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(186,'TRUSTLY_NOTIFICATION_URL','https://acp104.nextory.se/api/staticdata/trustly/notification',
'Nextory notification service URL to receive trustly notifications',now(),3,"String");

insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(187,'TRUSTLY_USERNAME','nextory','Nextory merchant username to access trustly api',now(),3,"String");

insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(188,'TRUSTLY_PASSWORD','d4935453-4c59-4c9e-848c-93256005e661','Nextory merchant password to access trustly api',now(),3,"String");
insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(189,'TRUSTLY_SUCCESS_URL','http://130.211.74.42:8092/trustly/success','Nextory Success Url',now(),3,"String");
insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(190,'TRUSTLY_FAIL_URL','http://130.211.74.42:8092/trustly/fel','Nextory Failure Url',now(),3,"String");
insert into test_properties (id,propkey,value,comments,updatedon,prop_categoryid,datatype) values
(191,'TRUSTLY_TEMPLATE_URL','https://acp104.nextory.se/api/staticdata/trustly/template','Trustly template url to build final payment page',now(),3,"String");
ALTER TABLE `orders`
 CHANGE COLUMN `provider` `provider` ENUM('PAYNOVA','ADYEN','PAYEX','KLARNA','E2GO','NOTSELECTED','NEXTORY','TRUSTLY') NULL DEFAULT NULL AFTER `paymentmode`;
 
ALTER TABLE `transaction`
 CHANGE COLUMN `lasttxnstatus` `lasttxnstatus` ENUM('FAILED','EXPIRED','SUCCESS','PARKED') NOT NULL DEFAULT 'SUCCESS' COLLATE 'latin1_swedish_ci' AFTER `createddate`;
ALTER TABLE `transaction`
 CHANGE COLUMN `provider` `provider` ENUM('ADYEN','PAYNOVA','PAYEX','TRUSTLY') NOT NULL DEFAULT 'ADYEN' COLLATE 'latin1_swedish_ci' AFTER `failed_mail_sent_date`;  


ALTER TABLE orders ADD COLUMN `trustly_notification_received` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '' AFTER `ordersource`;

ALTER TABLE transaction ADD COLUMN `trustly_other_data` VARCHAR(1000) NULL COMMENT ''  AFTER `provider`;

ALTER TABLE orders CHANGE COLUMN status status ENUM('SUCCESS','FAILED','WAIT','REFUND','REFUND_WAIT','REFUND_FAILED','REVERSE_FAILED','CAPTURE_FAILED','AUTHORIZED','PARKED','SUSPENDED') NOT NULL ;
-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: nextory_new
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_giftcard_log`
--

DROP TABLE IF EXISTS `admin_giftcard_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_giftcard_log` (
  `giftcard_purchase_id` int(11) NOT NULL,
  `issuedby` enum('HAND','EMAIL') NOT NULL,
  `comment` varchar(45) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `sent` tinyint(1) NOT NULL,
  PRIMARY KEY (`giftcard_purchase_id`),
  CONSTRAINT `gift_card_pur_details_gift_Card` FOREIGN KEY (`giftcard_purchase_id`) REFERENCES `giftcard_pur_details` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_logged_in_as_user`
--

DROP TABLE IF EXISTS `admin_logged_in_as_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_logged_in_as_user` (
  `customerid` int(11) NOT NULL,
  `hashkey` varchar(100) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedon` datetime DEFAULT NULL,
  `islinkActive` tinyint(4) NOT NULL,
  PRIMARY KEY (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `api_auth_tokens`
--

DROP TABLE IF EXISTS `api_auth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_auth_tokens` (
  `user_id` int(11) NOT NULL,
  `token` varchar(45) NOT NULL,
  `deviceid` varchar(45) NOT NULL DEFAULT 'NULL',
  `appid` varchar(45) NOT NULL,
  `model` varchar(45) NOT NULL,
  `version` varchar(45) NOT NULL,
  `createddate` datetime NOT NULL,
  `updateddate` datetime DEFAULT NULL,
  `dirty_flag` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`deviceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_banner_details`
--

DROP TABLE IF EXISTS `app_banner_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_banner_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appbannerid` int(11) NOT NULL DEFAULT '0',
  `img_url` varchar(250) DEFAULT NULL,
  `content` text,
  `colour` varchar(20) DEFAULT NULL,
  `createddate` datetime NOT NULL,
  `resolution` enum('RS_320','RS_640','RS_768','RS_1536') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_device_log`
--

DROP TABLE IF EXISTS `app_device_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_device_log` (
  `customerid` int(11) NOT NULL,
  `deviceid` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `version` varchar(50) NOT NULL,
  `osInfo` varchar(45) DEFAULT NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_library_sync_bookmarks_log`
--

DROP TABLE IF EXISTS `app_library_sync_bookmarks_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_library_sync_bookmarks_log` (
  `lib_id` int(11) NOT NULL,
  `format` int(11) NOT NULL,
  `chapter_index` int(11) DEFAULT NULL,
  `page_position_in_chapter` varchar(20) DEFAULT NULL,
  `page_position_in_book` varchar(20) DEFAULT NULL,
  `chapter_name` varchar(45) DEFAULT NULL,
  `content_height` varchar(20) DEFAULT NULL,
  `book_name` varchar(150) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `datetime` varchar(25) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  KEY `lib_customer_FK_idx` (`lib_id`),
  KEY `blibid` (`lib_id`),
  CONSTRAINT `lib_customer_FK` FOREIGN KEY (`lib_id`) REFERENCES `customerlibrary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_library_sync_highlight_log`
--

DROP TABLE IF EXISTS `app_library_sync_highlight_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_library_sync_highlight_log` (
  `lib_id` int(11) NOT NULL,
  `chapter_index` int(11) DEFAULT NULL,
  `start_index` int(11) DEFAULT NULL,
  `start_offset` int(11) DEFAULT NULL,
  `end_index` int(11) DEFAULT NULL,
  `end_offset` int(11) DEFAULT NULL,
  `page_position_in_chapter` varchar(20) DEFAULT NULL,
  `page_position_in_book` varchar(20) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `text` text,
  `note` text,
  `isnote` int(11) DEFAULT NULL,
  KEY `hlibid` (`lib_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_library_sync_master_log`
--

DROP TABLE IF EXISTS `app_library_sync_master_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_library_sync_master_log` (
  `lib_id` int(11) NOT NULL,
  `percentage` varchar(45) NOT NULL,
  `high_percentage` varchar(45) DEFAULT NULL,
  `position` varchar(45) NOT NULL,
  `last_cur_perc` double DEFAULT NULL,
  `per_sync_date` varchar(45) NOT NULL,
  `det_sync_date` varchar(45) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `syncdateForBookmarks` varchar(45) DEFAULT NULL,
  `syncdateForHighlights` varchar(45) DEFAULT NULL,
  `fixforverios4` tinyint(1) NOT NULL,
  KEY `libid_FK_customer_sync_idx` (`lib_id`),
  KEY `hlibid` (`lib_id`),
  CONSTRAINT `libid_FK_customer_sync` FOREIGN KEY (`lib_id`) REFERENCES `customerlibrary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_library_sync_paging_log`
--

DROP TABLE IF EXISTS `app_library_sync_paging_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_library_sync_paging_log` (
  `lib_id` int(11) NOT NULL,
  `chapter_index` int(11) DEFAULT NULL,
  `no_of_pages_in_chapter` int(11) DEFAULT NULL,
  `font_name` varchar(45) DEFAULT NULL,
  `font_size` int(11) DEFAULT NULL,
  `line_spacing` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `vertical_gap_ratio` varchar(20) DEFAULT NULL,
  `horizontal_gap_ratio` varchar(20) DEFAULT NULL,
  `portrait` int(11) DEFAULT NULL,
  `double_paged_for_landscape` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_library_sync_setting_log`
--

DROP TABLE IF EXISTS `app_library_sync_setting_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_library_sync_setting_log` (
  `lib_id` int(11) NOT NULL,
  `font_name` varchar(45) DEFAULT NULL,
  `font_size` int(11) DEFAULT '3',
  `player_speed` int(11) DEFAULT NULL,
  `line_spacing` int(11) DEFAULT '-1',
  `foreground` int(11) DEFAULT '-1',
  `background` int(11) DEFAULT '-1',
  `theme` int(11) DEFAULT '0',
  `brightness` varchar(20) DEFAULT '0',
  `transition_type` int(11) DEFAULT '2',
  `lock_rotation` int(11) DEFAULT '0',
  `double_paged` int(11) DEFAULT '1',
  `allow3g` int(11) DEFAULT '1',
  `globalpagination` int(11) DEFAULT '0',
  `syncTimestamp` varchar(45) DEFAULT NULL,
  KEY `lib_id_FK_sync_Setting_idx` (`lib_id`),
  KEY `slibid` (`lib_id`),
  CONSTRAINT `lib_id_FK_sync_Setting` FOREIGN KEY (`lib_id`) REFERENCES `customerlibrary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_library_track_log`
--

DROP TABLE IF EXISTS `app_library_track_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_library_track_log` (
  `lib_id` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  UNIQUE KEY `lib_id_UNIQUE` (`lib_id`),
  CONSTRAINT `lib_id_FK_app_library_track` FOREIGN KEY (`lib_id`) REFERENCES `customerlibrary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `automatic_mails`
--

DROP TABLE IF EXISTS `automatic_mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `automatic_mails` (
  `id` int(11) NOT NULL DEFAULT '0',
  `subject` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `filename` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `comments` varchar(300) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `c3p0.testtable`
--

DROP TABLE IF EXISTS `c3p0.testtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c3p0.testtable` (
  `a` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `camp_voucher_map`
--

DROP TABLE IF EXISTS `camp_voucher_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camp_voucher_map` (
  `voucher_id` int(11) NOT NULL,
  `camp_id` int(11) NOT NULL,
  `voucher_code` varchar(50) NOT NULL,
  `redeem_by` int(11) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL,
  `redeem_date` datetime DEFAULT NULL,
  PRIMARY KEY (`voucher_id`),
  UNIQUE KEY `voucher_code_UNIQUE` (`voucher_code`),
  KEY `camp_id_FK_idx` (`camp_id`),
  CONSTRAINT `camp_id_FK` FOREIGN KEY (`camp_id`) REFERENCES `campaign_voucher_master` (`camp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_channelmaster`
--

DROP TABLE IF EXISTS `campaign_channelmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_channelmaster` (
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `channel_name` varchar(45) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `createddate` varchar(45) NOT NULL,
  `updateddate` varchar(45) DEFAULT NULL,
  `isactive` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_voucher_master`
--

DROP TABLE IF EXISTS `campaign_voucher_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_voucher_master` (
  `camp_id` int(11) NOT NULL,
  `camp_name` varchar(45) NOT NULL,
  `camp_slugname` varchar(45) NOT NULL,
  `noofvouchers` int(4) NOT NULL,
  `noofoccurence` int(1) NOT NULL,
  `frequency` enum('YEAR','MONTH','WEEK','DAY') NOT NULL,
  `displayprice` float DEFAULT '0',
  `subscription_id` int(2) NOT NULL,
  `credit_card_required` tinyint(4) NOT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `expiry_date` date NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `allow_for_members` tinyint(2) NOT NULL DEFAULT '0',
  `applicable_to` enum('PRODUCTS','SUBSCRIPTION') DEFAULT 'SUBSCRIPTION',
  `multiple_reduction_allow` tinyint(1) DEFAULT NULL,
  `source` varchar(25) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `header_text` varchar(200) NOT NULL,
  `tag_text` varchar(200) NOT NULL,
  PRIMARY KEY (`camp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_voucher_member_type_map`
--

DROP TABLE IF EXISTS `campaign_voucher_member_type_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_voucher_member_type_map` (
  `id` int(11) NOT NULL DEFAULT '0',
  `camp_id` int(11) NOT NULL,
  `parent_member_type` int(11) DEFAULT NULL,
  `child_member_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaigncode`
--

DROP TABLE IF EXISTS `campaigncode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigncode` (
  `campaigncodeid` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `value` varchar(45) NOT NULL DEFAULT '0',
  `ispercentage` tinyint(1) NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createddate` datetime NOT NULL,
  `updateby` int(11) DEFAULT '0',
  `updatedate` datetime DEFAULT NULL,
  `islimtedusage` tinyint(1) NOT NULL DEFAULT '1',
  `ismultiplereductions` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`campaigncodeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `parent_cat_id` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `slug_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(2) NOT NULL DEFAULT '0',
  `for_publisher` varchar(150) CHARACTER SET latin1 NOT NULL DEFAULT 'ALL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contributor`
--

DROP TABLE IF EXISTS `contributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contributor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookid` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_bookid_idx` (`bookid`),
  CONSTRAINT `category_bookid` FOREIGN KEY (`bookid`) REFERENCES `product` (`bookid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=92662 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer2ccmap`
--

DROP TABLE IF EXISTS `customer2ccmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer2ccmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `campaigncodeid` int(10) NOT NULL,
  `orderid` int(11) NOT NULL,
  `consumed` tinyint(1) DEFAULT NULL,
  `consumedon` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id_FK_customer_info_customer2ccmap_idx` (`customerid`),
  KEY `camapigncode_id_FK_camp_code_idx` (`campaigncodeid`),
  KEY `order_id_FK_order_idx` (`orderid`),
  CONSTRAINT `customer_id_FK_customer_info_customer2ccmap` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_id_FK_order` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer2subscriptionmap`
--

DROP TABLE IF EXISTS `customer2subscriptionmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer2subscriptionmap` (
  `customerid` int(11) NOT NULL,
  `subscriptionid` int(11) NOT NULL,
  `transactionid` varchar(45) DEFAULT NULL,
  `updatedon` date DEFAULT NULL,
  `subscribedon` date DEFAULT NULL,
  `last_subscription_run_date` date NOT NULL,
  `next_subscription_run_date` date NOT NULL,
  `next_subscriptionid` int(11) DEFAULT NULL,
  PRIMARY KEY (`customerid`),
  KEY `subscription_id_FK_subscription_master_idx` (`subscriptionid`),
  CONSTRAINT `subscription_id_FK_subscription_master` FOREIGN KEY (`subscriptionid`) REFERENCES `subscription_master` (`subscriptionid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_comments`
--

DROP TABLE IF EXISTS `customer_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_comments` (
  `id` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `comments` varchar(150) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_FK_customer_ID_idx` (`customerid`),
  CONSTRAINT `customer_FK_customer_ID` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_library_limit`
--

DROP TABLE IF EXISTS `customer_library_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_library_limit` (
  `customerid` int(11) NOT NULL,
  `daily_limit` int(11) NOT NULL,
  `monthly_limit` int(11) NOT NULL,
  `createddate` datetime NOT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  KEY `customer_FK_customer_library_idx` (`customerid`),
  CONSTRAINT `customer_FK_customer_library` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_refer_friend`
--

DROP TABLE IF EXISTS `customer_refer_friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_refer_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refered_email` varchar(100) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `refered_by` int(11) NOT NULL,
  `createdate` datetime NOT NULL,
  `redeemed` tinyint(4) NOT NULL DEFAULT '0',
  `token` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_temp_password`
--

DROP TABLE IF EXISTS `customer_temp_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_temp_password` (
  `customerid` int(11) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `real_password` varchar(20) DEFAULT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  KEY `customer_id_FK_customer_info_temp_password_idx` (`customerid`),
  CONSTRAINT `customer_id_FK_customer_info_temp_password` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customerinfo`
--

DROP TABLE IF EXISTS `customerinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerinfo` (
  `customerid` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `salt` varchar(8) DEFAULT NULL,
  `ismember` tinyint(1) DEFAULT NULL,
  `member_type_code` int(6) DEFAULT NULL,
  `prev_mem_code` int(6) DEFAULT NULL,
  `intial_mem_code` int(6) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `land` varchar(45) DEFAULT NULL,
  `mobile` varchar(35) DEFAULT NULL,
  `telephonenumber` varchar(35) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `postcode` varchar(6) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `sub_newsletter` enum('SUBSCRIBED','UN_SUBSCRIBED','NONE') NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `first_app_loggedin` datetime DEFAULT NULL,
  `lastloggedin` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `mail_sent_date` datetime DEFAULT NULL,
  `user_from` enum('E2GO','BOOKCHEQUE','NEXTORY') DEFAULT NULL,
  `referred_by` int(11) DEFAULT NULL,
  `marketing_src` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`customerid`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customerlibrary`
--

DROP TABLE IF EXISTS `customerlibrary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerlibrary` (
  `id` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `format` varchar(45) NOT NULL,
  `storagelocation` varchar(45) DEFAULT NULL,
  `order_ref` varchar(45) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `errorcode` int(11) NOT NULL,
  `retry` int(11) NOT NULL,
  `provider_order_id` varchar(45) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','E2GO_ORDERED','OFFLINE_READER','DELETED','REMOVED') NOT NULL,
  `purchase_type` enum('PIECE','UNLIMITED','PREMIUM','STANDARD','BASE') DEFAULT NULL,
  `download_Count` int(3) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `read_status` enum('STARTED','ONGOING','DONE') DEFAULT NULL,
  `read_completed_date` datetime DEFAULT NULL,
  `pub_ass_id` int(11) NOT NULL,
  `added_as` enum('PREMIUM','STANDARD','BASE') DEFAULT NULL,
  `allowed_for` enum('PREMIUM','STANDARD','BASE') DEFAULT NULL,
  `allowed_confirmation` tinyint(1) DEFAULT NULL,
  `book_status_code` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_bookid_idx` (`bookid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `delete_customer_log`
--

DROP TABLE IF EXISTS `delete_customer_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delete_customer_log` (
  `customerid` int(11) NOT NULL,
  `updatedby` int(11) NOT NULL,
  `updatedate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `download_library_limit`
--

DROP TABLE IF EXISTS `download_library_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `download_library_limit` (
  `log_id` int(11) NOT NULL,
  `lib_id` int(11) NOT NULL,
  `download_start_date` datetime NOT NULL,
  `download_percentage` varchar(45) DEFAULT NULL,
  `download_end_date` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `comments` int(11) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `lib_id _FK_download_limit_idx` (`lib_id`),
  CONSTRAINT `lib_id?_FK_download_limit` FOREIGN KEY (`lib_id`) REFERENCES `customerlibrary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `download_library_log`
--

DROP TABLE IF EXISTS `download_library_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `download_library_log` (
  `log_id` int(11) NOT NULL DEFAULT '0',
  `lib_id` int(11) NOT NULL,
  `download_start_date` datetime NOT NULL,
  `download_percentage` varchar(45) DEFAULT NULL,
  `download_end_date` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `comments` varchar(750) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `downloadformat_map`
--

DROP TABLE IF EXISTS `downloadformat_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadformat_map` (
  `e2go_formatid` int(11) NOT NULL,
  `provider` enum('ELIB','QIOZK','E2GO') NOT NULL,
  `formatid` int(11) NOT NULL,
  `formatname` varchar(45) NOT NULL,
  `isallowedfordevice` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `downloadtoken`
--

DROP TABLE IF EXISTS `downloadtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloadtoken` (
  `id` int(11) NOT NULL,
  `customerlibid` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `format` int(11) DEFAULT NULL,
  `part` int(11) DEFAULT NULL,
  `name` varchar(350) DEFAULT NULL,
  `size` varchar(250) DEFAULT NULL,
  `sizetype` varchar(45) DEFAULT NULL,
  `token` varchar(1000) NOT NULL,
  `file_checksum` varchar(250) DEFAULT NULL,
  `ordercreatedate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `download_customer_foriegnkey_idx` (`customerlibid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elib_2go_price`
--

DROP TABLE IF EXISTS `elib_2go_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elib_2go_price` (
  `productid` int(11) NOT NULL,
  `pub_dis_id` int(11) NOT NULL,
  `effective_2go_price` float NOT NULL,
  `promotionid` int(11) DEFAULT NULL,
  `promotionname` varchar(150) DEFAULT NULL,
  `isautomatic` tinyint(1) NOT NULL DEFAULT '0',
  `validfrom` datetime DEFAULT NULL,
  `validto` datetime DEFAULT NULL,
  `createddate` date NOT NULL,
  `lastupdateddate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `finacial_report_dump`
--

DROP TABLE IF EXISTS `finacial_report_dump`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finacial_report_dump` (
  `id` int(11) NOT NULL DEFAULT '0',
  `fakturanumber` double DEFAULT NULL,
  `amount` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `source` varchar(45) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forgotpassword`
--

DROP TABLE IF EXISTS `forgotpassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forgotpassword` (
  `customerid` int(11) NOT NULL,
  `hashKey` varchar(100) NOT NULL,
  `salt` varchar(45) NOT NULL,
  `createdon` datetime NOT NULL,
  `consumed` tinyint(1) NOT NULL,
  PRIMARY KEY (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `giftcard`
--

DROP TABLE IF EXISTS `giftcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giftcard` (
  `id` int(11) NOT NULL,
  `giftcardname` varchar(45) NOT NULL,
  `price` float NOT NULL,
  `validity_in_months` int(11) NOT NULL,
  `subscriptionid` int(11) DEFAULT '0',
  `comments` varchar(60) DEFAULT NULL,
  `generated_by` enum('USER','ADMIN') DEFAULT NULL,
  `createddate` date NOT NULL,
  `updateddate` date DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `giftcardname_UNIQUE` (`giftcardname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `giftcard_pur_details`
--

DROP TABLE IF EXISTS `giftcard_pur_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giftcard_pur_details` (
  `id` int(11) NOT NULL,
  `giftcardid` int(10) NOT NULL,
  `voucher` varchar(45) NOT NULL,
  `price` float NOT NULL,
  `months` int(11) NOT NULL,
  `moms` float NOT NULL,
  `points` float DEFAULT NULL,
  `purchasedby` int(11) DEFAULT NULL,
  `purchasedon` date DEFAULT NULL,
  `purchasedorderno` int(11) NOT NULL,
  `validupto` date DEFAULT NULL,
  `redeemedby` int(11) DEFAULT NULL,
  `redeemedon` date DEFAULT NULL,
  `redeemedfor` int(11) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','CONSUMED','PARTIAL') NOT NULL,
  `generatedby` enum('USER','ADMIN') NOT NULL,
  `order_details_id` int(11) DEFAULT NULL,
  `internal_price` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gift_card_id_idx` (`giftcardid`),
  CONSTRAINT `gift_card_id` FOREIGN KEY (`giftcardid`) REFERENCES `giftcard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `giftcard_redeem_log`
--

DROP TABLE IF EXISTS `giftcard_redeem_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giftcard_redeem_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcardid` int(11) NOT NULL,
  `order_details_id` int(11) NOT NULL,
  `redeem_price` float NOT NULL,
  `redeemed_on` datetime NOT NULL,
  `actual_price` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gift_card_id_idx` (`giftcardid`),
  KEY `order_details_id_idx` (`order_details_id`),
  CONSTRAINT `FK_gift_card_id` FOREIGN KEY (`giftcardid`) REFERENCES `giftcard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_details_id` FOREIGN KEY (`order_details_id`) REFERENCES `orderdetails` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxidoftables`
--

DROP TABLE IF EXISTS `maxidoftables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxidoftables` (
  `tablename` varchar(45) NOT NULL,
  `maxid` int(11) NOT NULL,
  PRIMARY KEY (`tablename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxoflibid`
--

DROP TABLE IF EXISTS `maxoflibid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxoflibid` (
  `libid` int(11) NOT NULL,
  PRIMARY KEY (`libid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_freedays_log`
--

DROP TABLE IF EXISTS `member_freedays_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_freedays_log` (
  `id` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `noofoccurence` int(11) NOT NULL,
  `frequency` enum('YEAR','MONTH','WEEK','DAY') NOT NULL,
  `created_date` datetime NOT NULL,
  `comments` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id_FK_customer_info_idx` (`customerid`),
  CONSTRAINT `customer_id_FK_customer_info` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `membertype_master`
--

DROP TABLE IF EXISTS `membertype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membertype_master` (
  `member_type_code` int(11) NOT NULL,
  `membertype` varchar(45) CHARACTER SET latin1 NOT NULL,
  `comments` varchar(5000) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderdetails`
--

DROP TABLE IF EXISTS `orderdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL,
  `orderid` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  `formattype` int(11) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `priceoftheproduct` float DEFAULT NULL,
  `moms` int(11) NOT NULL,
  `purchaseprice` float NOT NULL DEFAULT '0',
  `purchaseprice_without_moms` float NOT NULL,
  `admin_price_rep` float NOT NULL DEFAULT '0',
  `admin_mom_rep` float NOT NULL DEFAULT '0',
  `ctc_price` float NOT NULL DEFAULT '0',
  `points_required` float NOT NULL DEFAULT '0',
  `isbook` tinyint(1) NOT NULL DEFAULT '1',
  `promotionid` int(11) DEFAULT NULL,
  `elib_order_ref` varchar(45) NOT NULL,
  `consumedpoints` float NOT NULL DEFAULT '0',
  `purchasedby` enum('FREE','PURCHASE','PARTIAL') NOT NULL,
  `iscampaignapplied` tinyint(1) NOT NULL,
  `isgiftcardapplied` tinyint(1) NOT NULL,
  `old_bookid` int(11) DEFAULT NULL,
  `status` enum('REFUND') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Order_id_Fk_idx` (`orderid`),
  KEY `Bookid_Fk_idx` (`bookid`),
  CONSTRAINT `Order_id_Fk` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL,
  `fakturano` varchar(20) NOT NULL,
  `customerid` int(11) NOT NULL,
  `orderdate` datetime NOT NULL,
  `orderamount` float NOT NULL,
  `orderamountwithmoms` float NOT NULL,
  `paymentmode` enum('POLLETS','BANK_POLLETS','BANK','WAIT','MOBILE','CCARD_BANK','INTERNET_BANK','CC_PREAUTH_BANK','PREAUTH_BANK','VOUCHER','BOOKCHEQUE','UNLIMITED','GIFT_CARD','AGREEMENT') NOT NULL,
  `provider` enum('PAYNOVA','ADYEN','PAYEX','KLARNA','E2GO','NOTSELECTED','NEXTORY') DEFAULT NULL,
  `status` enum('SUCCESS','FAILED','WAIT','REFUND','REFUND_WAIT','REFUND_FAILED','REVERSE_FAILED','CAPTURE_FAILED','AUTHORIZED') NOT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `totalpoints` float DEFAULT NULL,
  `consumedpoints` float DEFAULT NULL,
  `campagincode` int(11) DEFAULT NULL,
  `giftcardid` int(11) DEFAULT NULL,
  `purchasedAsMember` tinyint(1) NOT NULL,
  `providerreference` varchar(45) DEFAULT NULL,
  `ordersource` enum('WEB','ANDROID','IOS') NOT NULL DEFAULT 'WEB',
  PRIMARY KEY (`orderid`),
  UNIQUE KEY `fakturano_UNIQUE` (`fakturano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_response_dump`
--

DROP TABLE IF EXISTS `payment_response_dump`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_response_dump` (
  `customerid` int(11) NOT NULL,
  `orderno` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `payex_service` varchar(100) NOT NULL,
  `raw_dump` varchar(10000) NOT NULL,
  `dump_date` datetime NOT NULL,
  `failed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prod_eb_properties`
--

DROP TABLE IF EXISTS `prod_eb_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_eb_properties` (
  `id` int(11) NOT NULL,
  `propkey` varchar(250) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL,
  `comments` varchar(150) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `prop_categoryid` int(11) DEFAULT NULL,
  `datatype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `propkey_UNIQUE` (`propkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `bookid` int(11) NOT NULL,
  `isbn` varchar(15) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  `book_type` enum('E_BOOK','AUDIO_BOOK') DEFAULT NULL,
  `publisherid` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subtitle` varchar(250) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `imagename` varchar(45) DEFAULT NULL,
  `coverimage` varchar(90) DEFAULT NULL,
  `product_status` enum('ACTIVE','INACTIVE','UPCOMING') DEFAULT NULL,
  `book_length` int(11) DEFAULT NULL,
  `allowed_for` enum('BASE','STANDARD','PREMIUM') DEFAULT NULL,
  `description` text,
  `original_publication_date` datetime DEFAULT NULL,
  `published_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`bookid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_list`
--

DROP TABLE IF EXISTS `product_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_list` (
  `product_list_id` int(11) NOT NULL,
  `slug` varchar(45) NOT NULL,
  `displayname` varchar(45) NOT NULL,
  `product_count` int(3) NOT NULL DEFAULT '0',
  `formatid` int(3) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_category_page` tinyint(1) DEFAULT '0',
  `mode` enum('MANUAL','AUTO') DEFAULT NULL,
  `show_in_home_page` tinyint(1) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL,
  `der_cat` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `subscription_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_list_map`
--

DROP TABLE IF EXISTS `product_list_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_list_map` (
  `map_id` int(11) NOT NULL,
  `product_list_id` int(3) NOT NULL,
  `bookid` int(11) NOT NULL,
  `position` int(3) NOT NULL,
  `adjust_factor` int(11) DEFAULT '0',
  `createddate` datetime NOT NULL,
  `provider_productid` int(11) DEFAULT NULL,
  PRIMARY KEY (`map_id`),
  KEY `productList_FK_idx` (`product_list_id`),
  KEY `bookid_FK_product_map_idx` (`bookid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publisher_details`
--

DROP TABLE IF EXISTS `publisher_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher_details` (
  `publisherid` int(11) NOT NULL,
  `publisher_name` varchar(150) DEFAULT NULL,
  `distributor_name` varchar(70) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`publisherid`),
  UNIQUE KEY `publisher_name_UNIQUE` (`publisher_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `refundtransactionlog`
--

DROP TABLE IF EXISTS `refundtransactionlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refundtransactionlog` (
  `id` int(11) NOT NULL DEFAULT '0',
  `orderid` int(11) NOT NULL,
  `orderamount` float NOT NULL,
  `refundamount` float NOT NULL,
  `refunddate` datetime NOT NULL,
  `totalconsumedpoints` float DEFAULT '0',
  `refundpoints` float DEFAULT '0',
  `refundedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `bookid` int(11) NOT NULL,
  `relationtype` int(11) NOT NULL,
  `identifiertype` varchar(45) DEFAULT NULL,
  `identifierid` varchar(250) DEFAULT NULL,
  `relationtype_name` varchar(45) DEFAULT NULL,
  `c_isbn` int(11) DEFAULT NULL,
  `createdby` enum('ELIB','NEXTORY','PUBLIT') NOT NULL,
  `volume` int(11) DEFAULT NULL,
  KEY `Bookid_FK_idx` (`bookid`),
  CONSTRAINT `Bookid_FK_relations` FOREIGN KEY (`bookid`) REFERENCES `product` (`bookid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scheduler_log`
--

DROP TABLE IF EXISTS `scheduler_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduler_log` (
  `id` int(11) NOT NULL DEFAULT '0',
  `job_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `status` enum('RUNNING','COMPLETED','FAILED') CHARACTER SET latin1 NOT NULL,
  `completed_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_analytics`
--

DROP TABLE IF EXISTS `search_analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_analytics` (
  `keyword` varchar(150) CHARACTER SET utf8 NOT NULL,
  `formattype` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `searchedon` datetime DEFAULT NULL,
  `suggestion` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `status` enum('SUCCESS','FAILED','UPDATED','DELETED') CHARACTER SET utf8 NOT NULL,
  `failed_count` int(11) DEFAULT NULL,
  `is_suggestion_type` enum('KEYWORD','CATEGORYLIST','TOPLIST') CHARACTER SET utf8 NOT NULL DEFAULT 'KEYWORD'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscription_change_log`
--

DROP TABLE IF EXISTS `subscription_change_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_change_log` (
  `id` int(11) NOT NULL DEFAULT '0',
  `customerid` int(11) NOT NULL,
  `sub_id_from` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `sub_id_to` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `points` float DEFAULT NULL,
  `action` enum('ACTIVATE','INACTIVATE') CHARACTER SET latin1 NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `comments` varchar(150) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscription_master`
--

DROP TABLE IF EXISTS `subscription_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_master` (
  `subscriptionid` int(20) NOT NULL AUTO_INCREMENT,
  `subscriptionplanname` varchar(20) DEFAULT NULL,
  `subscriptionprice` float NOT NULL,
  `points` float NOT NULL,
  `offerprice` float NOT NULL,
  `validity` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL,
  `lastupdatedon` datetime DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  `book_price_limit` float NOT NULL DEFAULT '0',
  `subscription_type` enum('PREMIUM','STANDARD','BASE') NOT NULL DEFAULT 'BASE',
  PRIMARY KEY (`subscriptionid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_properties`
--

DROP TABLE IF EXISTS `test_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `propkey` varchar(250) CHARACTER SET latin1 NOT NULL,
  `value` varchar(750) CHARACTER SET latin1 NOT NULL,
  `comments` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `updatedon` datetime NOT NULL,
  `prop_categoryid` int(11) unsigned NOT NULL DEFAULT '0',
  `datatype` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `customerid` int(11) NOT NULL,
  `transactionid` varchar(45) CHARACTER SET latin1 NOT NULL,
  `expire_day` int(11) DEFAULT '0',
  `expire_month` int(11) DEFAULT NULL,
  `expire_year` int(11) DEFAULT NULL,
  `card_holder_name` varchar(80) CHARACTER SET latin1 DEFAULT NULL,
  `card_Details` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `orderid` int(11) NOT NULL DEFAULT '0',
  `expriy_soon_mail_count` int(11) NOT NULL DEFAULT '0',
  `failed_mail_count` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime DEFAULT NULL,
  `lasttxnstatus` enum('FAILED','EXPIRED','SUCCESS') CHARACTER SET latin1 NOT NULL DEFAULT 'SUCCESS',
  `updateddate` datetime DEFAULT NULL,
  `failed_mail_sent_date` datetime DEFAULT NULL,
  `provider` enum('ADYEN','PAYNOVA','PAYEX') CHARACTER SET latin1 NOT NULL DEFAULT 'ADYEN',
  PRIMARY KEY (`customerid`),
  KEY `orderid` (`orderid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userroles`
--

DROP TABLE IF EXISTS `userroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userroles` (
  `customerid` int(11) NOT NULL,
  `customer_role` enum('ROLE_ADMIN','ROLE_CS','ROLE_SUPER','ROLE_APP') NOT NULL,
  PRIMARY KEY (`customerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vatinfo`
--

DROP TABLE IF EXISTS `vatinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vatinfo` (
  `vatid` int(11) NOT NULL,
  `producttype` int(11) NOT NULL,
  `vatpercentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voucher`
--

DROP TABLE IF EXISTS `voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher` (
  `voucherid` int(11) NOT NULL AUTO_INCREMENT,
  `voucher` varchar(45) NOT NULL,
  `validfrom` datetime NOT NULL,
  `validto` datetime NOT NULL,
  `redeemedby` int(11) DEFAULT NULL,
  `vouchervalue` float NOT NULL,
  `subscriptionid` int(11) NOT NULL,
  `no_occurence` int(11) DEFAULT NULL,
  `frequency` enum('YEAR','MONTH','WEEK','DAY') NOT NULL,
  `redeemedon` datetime DEFAULT NULL,
  `voucherstatus` enum('ACTIVE','INACTIVE') NOT NULL,
  `mail_sent` tinyint(1) NOT NULL,
  `months_redeemed` int(11) DEFAULT NULL,
  `generatedfor` varchar(45) NOT NULL,
  PRIMARY KEY (`voucherid`),
  UNIQUE KEY `voucher_UNIQUE` (`voucher`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-16  5:32:24

alter table campaign_voucher_master add column isfixedcampaign tinyint(2) after tag_text;
alter table campaign_voucher_master add column isdiscountcampaign tinyint(2) after isfixedcampaign;
alter table campaign_voucher_master add column campaign_price float default 0 after isdiscountcampaign;
alter table customer2subscriptionmap add column discount_end_date DATETIME null after next_subscription_run_date ;

select max(camp_id) from campaign_voucher_master; - 458

insert into campaign_voucher_master (camp_id,camp_name,camp_slugname,noofvouchers,noofoccurence,frequency,
subscription_id,credit_card_required,expiry_date,created_date,updated_by,updated_date,active, multiple_reduction_allow,source,channel_id,header_text,tag_text,isfixedcampaign,isdiscountcampaign,campaign_price) values(459,'FIXED_PRICE_CAMPAIGN','fixedpricecampaign',1,30,'DAY',6,0,'2017-12-31',now(),27220,now(),1,1,'OFFLINE',11,'FIXED PRICE CAMPAIGN','FIXED PRICE CAMPAIGN',1,0,9);

insert into campaign_voucher_master (camp_id,camp_name,camp_slugname,noofvouchers,noofoccurence,frequency,
subscription_id,credit_card_required,expiry_date,created_date,updated_by,updated_date,active, multiple_reduction_allow,source,channel_id,header_text,tag_text,isfixedcampaign,isdiscountcampaign,campaign_price) values(460,'DISCOUNT_PRICE_CAMPAIGN','discountpricecampaign',1,90,'DAY',6,0,'2017-12-31',now(),27220,now(),1,1,'OFFLINE',11,'DISCOUNT PRICE CAMPAIGN','DISCOUNT PRICE CAMPAIGN',0,1,50);

select max(voucher_id) from camp_voucher_map; - 41853


insert into camp_voucher_map (voucher_id,camp_id,voucher_code,status) values (41854,459,'FPC01','ACTIVE');
insert into camp_voucher_map (voucher_id,camp_id,voucher_code,status) values (41855,460,'DPC01','ACTIVE');


insert into campaign_voucher_member_type_map (camp_id,parent_member_type,child_member_type) values (459,400000,0);
insert into campaign_voucher_member_type_map (camp_id,parent_member_type,child_member_type) values (460,400000,0);


 insert into membertype_master values (203011, 'FREE_TRANSACTION_FAILED','Free campaign or giftcard member transaction failed on subscription charge date');
  insert into membertype_master values (304011, 'MEMBER_TRANSACTION_FAILED','Paying member transaction failed on subscription charge date');
   insert into membertype_master values (404011, 'NONMEMBER_TRANSACTION_FAILED','Paying member transaction failed on final attempt');
   insert into membertype_master values (302012, 'MEMBER_FIXED_PRICE_CAMPAIGN','Nonmember activate again with Fixed price campaign');
    insert into membertype_master values (302013, 'MEMBER_DISCOUNTED_PRICE_CAMPAIGN','Nonmember activate again with discounted price campaign');
	 insert into membertype_master values (302014, 'MEMBER_FIXED_CAMPAIGN_TRANSACTION_FAILED','payment failed for fixed price campaign users on subscription charge date');
	 insert into membertype_master values (302015, 'MEMBER_DISCOUNTED_CAMPAIGN_TRANSACTION_FAILED','payment failed for discounted price campaign users on subscription charge date');
	 
	 
	 CREATE TABLE `customer_campaign_activation_details` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`customerid` INT(11) NOT NULL,
	`voucher_code` VARCHAR(45) NOT NULL,
	`subscriptiontype` INT(11) NOT NULL,
	`status` VARCHAR(50) NULL DEFAULT NULL,
	`comment` VARCHAR(500) NULL DEFAULT NULL,
	`activation_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`createddate` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00',
	`createdby` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `customerid` (`customerid`),
	CONSTRAINT `FK__customerinfo` FOREIGN KEY (`customerid`) REFERENCES `customerinfo` (`customerid`)
);



INSERT INTO `prod_properties` (`propkey`, `value`, `comments`, `updatedon`, `prop_categoryid`, `datatype`) VALUES ('P12_FILE_LOC', 'frescano-cloud-project-27d48456fe09.p12', 'Goolge cloude store certificate', '2016-12-12 13:51:38', '3', 'String');
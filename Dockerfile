FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install -y wget openjdk-8-jdk
RUN update-ca-certificates -f
CMD java -version
RUN echo "Europe/Stockholm"  > /etc/timezone
CMD date

FROM maven:3-jdk-8

#RUN mkdir -p /usr/src/app
WORKDIR /app

ADD . /app

RUN mvn clean install -DskipTests
#COPY /app/target/nex-api-0.0.1-SNAPSHOT.jar /app/target/nex-api.jar

#COPY src/main/resources/dd-java-agent/dd-java-agent.jar /app

EXPOSE 9000/tcp

ENV DD_AGENT_HOST $DD_AGENT_SERVICE_HOST
ENV DD_AGENT_PORT $DD_AGENT_SERVICE_PORT

#ENTRYPOINT ["java", "-jar", "/app/target/nex-api.jar"]

ENTRYPOINT ["java","-Xmx2g","-Xms1g","-javaagent:/app/src/main/resources/dd-java-agent/dd-java-agent.jar","-jar", "/app/target/nex-api-0.0.1-SNAPSHOT.jar"]
